# ttoolboxes

This toolbox provides functions for the 
- computation of the joint spectral radius of a finite set of matrices using the modified invariant polytope algorithm, as well as 
- functions for the work with multiple, multivariate, stationary subdivision schemes.

Hosting is on GitLab. Download the latest realease from:
https://gitlab.com/tommsch/ttoolboxes

## Installation
Call in the folder where `ttoolboxes.m` is located the command
```matlab
ttoolboxes install
```
You can also add all folders of the ttoolboxes by hand to the Matlab path, 
but this adds a lot of unnecessary folders too.

## Joint spectral radius example
```matlab
T = {randn(3), randn(3)};
ipa( T );
```

## Subdivision example
```matlab
S = getS( '1_daubechies', 2 )
blf( S );
```

## Included third party libraries
This package includes a copy of
- "The JSR Toolbox" by Raphael Jungers.
- SeDuMi
- Feta algorithm by Claudia Möller


## Citations
- If you use the ipa-package, please cite:
  - N. Guglielmi, V. Yu. Protasov, Exact computation of joint spectral characteristics of linear operators, Comput. Math. 13 (2013).
  - N. Guglielmi, V. Yu. Protasov, Invariant polytopes of linear operators with applications to regularity of wavelets and of subdivisions, SIAM J. Matrix Anal. Appl. 37 (2016)
  - T. Mejstrik, Algorithm 1011: Improved Invariant Polytope Algorithm and Applications, ACM Trans. Math. Softw., 46 (2020) 3, 29.
  - T. Mejstrik, V. Yu. Protasov, Elliptic polytope and Lyapunov norms of linear operators, SIMAX, submitted for publication.
  - T. Mejstrik, U. Reif, Feta flavoured Ipa, ACM Toms, submitted for publication.

- If you use the subdivision-package, please cite
  - M. Charina, T Mejstrik, Multiple multivariate subdivision schemes: Matrix and operator approaches, J. Comp. Appl. Math. (2018)
  - T. Mejstrik, Algorithm 1011: Improved Invariant Polytope Algorithm and Applications, ACM Trans. Math. Softw., 46 (2020) 3, 29, pages. doi:10.1145/3408891
  
