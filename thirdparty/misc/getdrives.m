function [ ret ] = getdrives();
% getdrives returns the drive letters of mounted filesystems
% On unix the function unconditionally returns '\'
% 
% Written by: tommsch, 2024-09-15

    if( ~ispc() );
        ret = {'/'};
        return; end;
    
    ret = {};
    for letter = 'a':'z';
        if( exist(['' letter ':\'], 'dir') == 7 )
            ret{end+1} = [letter ':\']; end; end;  %#ok<AGROW>
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*MCCD>  % MCC, use of cd function
