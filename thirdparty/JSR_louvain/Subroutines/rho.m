function r = rho( M, eigs_flag );
% [ r ] = rho( M, eigs_flag )
% 
% Computes the spectral radius of matrix M
% if M is a cell of matrices, r is a row vector with 
% the spectral radius of each M{i}

%            2020_09_07 - tommsch - Whether a cell array of matrices contains sparse matrices, now is decided solely by M{1}
%            2023_09_05 - tommsch - Bugfix for large matrices
% Changelog: 


if( nargin <= 1 );
    eigs_flag = size( M, 1 ) >= 500; end;

if( iscell(M) );
    r = zeros( size(M) );
    if( issparse(M{1}) );
        opts.disp = 0;
        for i = 1:numel( r );
            if( any(~isfinite(M{i}(:))) );
                r = nan;
            else;
                r(i) = abs( eigs( M{i}, 1, 'LM', opts ) ); end; end;
    elseif( eigs_flag );
        for i = 1:numel( r );
            if( any(~isfinite(M{i}(:))) );
                r = nan;
            else;
                r(i) = abs( eigs( M{i}, 1 ) ); end; end;
    else;
        for i = 1:numel( r );
            if( any(~isfinite(M{i}(:))) );
                r = nan;
            else;
                r(i) = max( abs(eig(M{i})) ); end; end; end;
else;
    if( any(~isfinite(M(:))) );
        r = nan;
        return; end;
    if( issparse(M) );
        opts.disp = 0;
        r = abs( eigs( M, 1, 'LM', opts ) );
    elseif( eigs_flag );
        r = abs( eigs( M, 1 ) );
    else;
        r = max( abs(eig(M)) ); end; end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>