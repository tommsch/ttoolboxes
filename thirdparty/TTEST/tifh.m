function out = tifh( truth, yesh, noh )
% Ternary if operator
% out = truth & yes : no ;
% Input:
%    yes, no         function handles
%
% E.g.: tifh( true, @()1, @()2 )
    
if( truth );
    if( isa(yesh,'function_handle') );
        out = yesh();
    else;
        out = yesh; end;
else;
    if( isa(noh,'function_handle') );
        out = noh();
    else;
        out = noh; end; end;
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
