classdef node < handle
    
    properties
        col  % col codes property of the node:
        % col = 1 - "ordinary" node
        % col = 2 - 1-bounded node
        % col = 3 - covered node
        % col = 3 + i - child of generator edge resp. to J{i}
        % col = 8 - strictly 1-bounded node
        fuz  % number of generator edges in the path between the node and root
        nodenrm  % norm of node
        child  % cell which contains the children
        edgecnt  % vector, entry i contains the number of J{i}-edges in the subtree (!) of the node
        m
        n
    end
        
    methods
        function [ K ] = node( a, b, c, m, n );
            K = K@handle();
            K.col = a;
            K.fuz = b;
            K.nodenrm = c;
            K.child = cell( 1, m + n );
            K.edgecnt = zeros( 1, n );
            K.m = m;
            K.n = n;
        end      

        function K = setChild( K, i, I );
            assert( isa(I,'node'), 'VerifyOutput:OutOfBounds', 'Child of a node has to be an object node!' );
            K.child{node.transformIndex(i,K.m)}=I;
        end
                
        function[N]=getNode(root,indexvecNachlev0)
            lev=1;
            N=root;
            while ~isempty(N) && lev<=length(indexvecNachlev0)
                i  =indexvecNachlev0(lev);
                N = N.child{node.transformIndex(i,root.m)};
                lev=lev+1;
            end
        end           
        
        function [ db ] = depthBelow( K );
            db=0;
            for i=1:length(K.child)
                Child_i=K.child{i};
                if ~isempty(Child_i)
                    db=max(db,depthBelow(Child_i)+1);
                end
            end
        end
        
        function [ nsub ] = cntNodesSubtree( K, nsub ); 
            if nargin==1
                nsub = 0; 
            end
            
            nsub = nsub + 1;
            
            if ~all(cellfun('isempty',K.child)) % K is not a leaf
                for i = 1:K.m+K.n
                    if ~isempty(K.child{i})
                        nsub = cntNodesSubtree(K.child{i},nsub);
                    end
                end
            end
            
        end
        
        function [ K ] = deleteChild( K );
            K.child = cell( 1, K.m + K.n );
        end
        
        function K = increaseEdgecnt( root, indexvecNachlev0, k )
            K=root;
            for i=1:length(indexvecNachlev0)
                K.edgecnt(-k)=K.edgecnt(-k)+1;
                K=K.child{node.transformIndex(indexvecNachlev0(i),root.m)};
                assert( ~isempty(K), 'VerifyOutput:OutOfBounds', 'The input index vector does not correspond to a node in the tree!' );
            end
        end
        
        function K = decreaseEdgecnt( root, KK, indexvecNachlev0 )
            K=root;
            for i=1:length(indexvecNachlev0)
                K.edgecnt=K.edgecnt-KK.edgecnt;
                K=K.child{node.transformIndex(indexvecNachlev0(i),root.m)};
                assert( ~isempty(K), 'VerifyOutput:OutOfBounds', 'The input index vector does not correspond to a node in the tree!' );
            end
        end
        
        function [v,K] = isInTree(root,indexvecNachlev0)
            K=root;
            v=1;
            for i=1:length(indexvecNachlev0)
                K=K.child{node.transformIndex(indexvecNachlev0(i),root.m)};
                if isempty(K)
                    v=0;
                    break  
                end 
            end
        end
        
        function [] = plotSubtree( K, blackwhite );
            field1 = 'mySymbol';
            value1 = {'ok','ok','ok','sk','vk','dk','pk','hk'};
            field2 = 'myColour';
            if( blackwhite==0 );
                value2={'r',[53/255,214/255,35/255],'w','b','c','m','y',[53/255,214/255,35/255]};
            else;
                value2={'k',[153/255,153/255,153/255],'w','k','k','k','k',[153/255,153/255,153/255]}; end;
            field3 = 'myMarkersize';
            value3 = {8,8,8,8,8,10,12,12};
            field4 = 'myLinewidth';
            value4 = {1,1,1.5,1,1,1,1,1};
            PlotProperties = struct(field1,value1,field2,value2,field3,value3,field4,value4);
            db = depthBelow(K);
            %figure
            axis ij
            tsum = 0;
            for i = 1:db
                tsum = tsum + 1/2*K.m^(-i-1); end;
            
            xlim( [-tsum*1.2 tsum*1.2] );
            ylim( [-0.5 db + 1] );
 
            set( gca,'XTick',[] )
            
            if( db <16 );
                set( gca, 'YTick',0:db );
            elseif( db <26 );
                if( mod(db,2)==0 );
                    set( gca, 'YTick',0:2:db );
                else;
                    set( gca, 'YTick',0:2:db+1 ); end;
            else;
                set( gca, 'YTick',0:5:db ); end;
            
            hold on
            tOld = 0;
            lev = 0;
            plot3( tOld, lev, 1, PlotProperties(K.col).mySymbol, ...
                   'MarkerFaceColor',PlotProperties(K.col).myColour, ...
                   'MarkerSize',PlotProperties(K.col).myMarkersize, ...
                   'LineWidth',PlotProperties(K.col).myLinewidth );
            
            for i = 1:K.m + K.n
                C = K.child{i};
                if( ~isempty(C) );
                    plotNextLevel(C,i,tOld,lev+1); end; end;
            
            function [] = plotNextLevel( C, i, tOld, lev );
                if( i<=K.m );
                    diff = (-1/2+(i-1)/(K.m-1))*K.m^(-lev-1);
                    diff = sign(diff) * max( abs(diff), 0.0001 * rand ) * sign(randn);
                    tNew = tOld + diff;
                else;
                    tNew = tOld; end;
                
                  plot( [tOld,tNew], [lev-1,lev], 'k' );  % plot edge to K
                  hold on
                  % plot node K
                  plot3( tNew, lev, C.col, PlotProperties(C.col).mySymbol,'MarkerFaceColor', ...
                                           PlotProperties(C.col).myColour,'MarkerSize', ...
                                           PlotProperties(C.col).myMarkersize,'LineWidth', ...
                                           PlotProperties(C.col).myLinewidth );

                  a = 1;
                  b = K.m + K.n;
                  for h = a:b
                      if ~isempty(C.child{h})
                        plotNextLevel( C.child{h}, h, tNew, lev+1 ); end; end;
            end   
        end
    end
    methods (Static)
        function [ j ] = transformIndex( i, m )
            if i < 0
                j=m-i;
            else
                j=i;
            end
        end
    end   
end
            
       
