function [ rho ] = feta( M, varargin )
% Written by Claudia Möller, 2010-2015
% For more information about the algorithm, see my thesis:
% Möller, Claudia <http://tuprints.ulb.tu-darmstadt.de/view/person/M=F6ller=3AClaudia=3A=3A.html>
%   A New Strategy for Exact Determination of the Joint Spectral
%   Radius. Technische Universität, Darmstadt [Dissertation], (2015)
%   document_url: http://tuprints.ulb.tu-darmstadt.de/4603/1/DissMoellerGenehmigt.pdf

%               2022-02-02, tommsch, totally refactored, removed v-norms and live plotting capabilities
% Changelog: 

clf;


[verbose, varargin]         = parsem( {'verbose','v'}, varargin, 1 );
[p, varargin]               = parsem( {'p','norm'}, varargin, 'p' );  % string specifying the norm, see mynorm.m: % p= '2Norm', 'infNorm', 'poly'
[it, varargin]              = parsem( {'it','iteration'}, varargin, 5 );  % needed for p='poly', number of iterations
[MaxGreenNorm, varargin]    = parsem( {'MaxGreenNorm','maxgreennorm'}, varargin, 0 );  % safe the biggest norm leading to a 1-bounded leaf
[LimRadComput, varargin]    = parsem( {'LimRadComput','limradcomput'}, varargin, 1/17 );  % parameter for ball vector computation (see thesis)
[LimRadConstr, varargin]    = parsem( {'LimRadConstr','limradconstr'}, varargin, 1/100 );   % parameter for ball vector computation (see thesis)
[cleaning, varargin]        = parsem( {'cleaning','clean'}, varargin, 0 );   % set cleaning==1 to reduce the ball vectors after every multiplication (becomes slow!)
[DisableEdgecnt, varargin]  = parsem( {'DisableEdgecnt','disableedgecnt','disableedgecount'}, varargin, 0 );  % if DisableEdgecnt==1, the number of negative nodes is not tracked (faster but less information about the resulting tree)
[NormApproach, varargin]    = parsem( {'NormApproach','normapproach','normapp'}, varargin, 'BallVec' );   % type of norm estimation that is used: 'BoundEig', 'BallVec' (see thesis)
[SafetyConst, varargin]     = parsem( {'SafetyConst','eps'}, varargin, 1e-7 );   % node detected as 1-bounded if its norm is smaller than 1-SafetyConst
[Max_k0, varargin]          = parsem( {'Max_k0','maxk0'}, varargin, 10 );  % parameter for ball vector computation (see thesis)
[maxlevel, varargin]        = parsem( {'maxlevel','maxit','iteration'}, varargin, 150 );  % maximal search level
[ScaleMaxK, varargin]       = parsem( {'ScaleMaxK','scalemaxk'}, varargin, [] );  % if ScaleMaxK=[] then OptimiseEdges.m will not be executed
[neglimit, varargin]        = parsem( {'neglimit','maxgreennorm'}, varargin, 3 );   % maximal number of generator edges in a path

M = parsematrixset( M );
J = findsmp( M, 'v',verbose-1 );
M = cat( 3, M{:} );

[startlevel, varargin] = parsem( {'startlevel'}, varargin, max( [3 numel(J{end})+1] ) );  % negative children forbidden up to startlevel

parsem( varargin, 'test' );

h_nrm = feta_setnorm( M, p, it, ScaleMaxK );

calltic = tic;
[rho, MaxGreenNorm, Node] = CallTree( M, J, maxlevel, startlevel, neglimit, SafetyConst, Max_k0, ...
                                      cleaning, DisableEdgecnt, LimRadComput, LimRadConstr, NormApproach, MaxGreenNorm, h_nrm );
%gr = Node2Graph( Node );
toc( calltic );
fprintf( 'The maximal norm detected as 1-bounded was %15.12g\n', MaxGreenNorm );

RootNode = Node{1};
RootNode.plotSubtree( 0 );

fprintf( 'Number of nodes in the tree: %i\n', RootNode.cntNodesSubtree );
fprintf( 'Number of negative nodes (as a vector, i-th entry refers to generator J{i}): %i', RootNode.edgecnt );


end
%#ok<*NOSEL,*NOSEMI,*ALIGN>

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.

