function[cp]=CoeffProdGen(WW,a,Tup,fuz,I)

d=size(WW{1},2); % space dimension
C=zeros(d^fuz,fuz);
I_col=I(I<0);
for j=1:fuz
    W=WW{-I_col(j)};
    for i=1:d^fuz
        C(i,j)=W(a(j),Tup(i,j));
    end
end
cp=prod(C,2);

