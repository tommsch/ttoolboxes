function [ C ] = multiply( A, B, h_nrm, cleaning );

if( nargin<=3 );
    cleaning = 0; end;  %% XX

% Generate for vectors of balls or vectors of matrices a new vector which
% contains all products

if( isnumeric(A) && isnumeric(B) );
    C = A * B;
else;
    if( isnumeric(A) );
        A = ball( A, 0, h_nrm );
    elseif isnumeric(B)
        B = ball( B, 0, h_nrm );
    else;
        % do nothing
        end;

    C = ball.empty( numel(A)*numel(B), 0 );
    for i = 1:numel( A );
        for j = 1:numel( B );
            C(j+(i-1)*numel( B )) = A(i)*B(j); end; end; end;

% Clean ball vector -> eliminate those who are contained in others
if( cleaning==1 );
    elim = [];
    disp( 'Cleaning ballvector from redundant entries.' );
    numel( C );
    Nontriv = find( [C.rad] );
    for i = 1:numel( C );
        for j = setdiff( Nontriv, i );
            if( C(i)<C(j) );
                elim = [elim,i];
                Nontriv = setdiff( Nontriv, i );
                break; end; end; end;
    disp( ['Eliminated ',num2str(length(elim)),' balls of ',num2str(numel(C)),' in total.'] );
    C = C(setdiff( 1:numel(C), elim )); end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.

      