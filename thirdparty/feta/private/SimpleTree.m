function [ val, MaxGreenNorm, Node ] = SimpleTree( A, J, maxlevel, SafetyConst, MaxGreenNorm, h_nrm );

R = [];
m = size( A, 3 );  % m is number of matrices in family A
n = 0;
Depth = 0;
val = 0;
% Start with analysing I=[1],A1
lev = 1;
lev0 = length( R );  % lev0 is the level of this tree's root, lev0=0 iff root=id
B = cell( maxlevel, 1 );
Node = cell( maxlevel+1, 1 );  % Node{lev} will contain the last visited node from level lev-1.
I=zeros(1,maxlevel+lev0);
if any(R<0)
   disp('Root must be a positive index vector!')
   return
end
I(1:lev0) = R;
RootMatrix = GetMat( A, R );  % If R=[], then RootMatrix=id
Node{1} = node( 1, 0, 10, m, n );  % This is the node describing the root.
B{lev} = A(:,:,1)*RootMatrix; 
I(lev0+1) = 1;

if isempty(J)==0
    JInd=J{1};
else
    JInd=[];
end
cnt = 0;  % cnt counts how often the while-loop is executed
while true
    cnt = cnt+1;
    N = h_nrm( B{lev} );  % Calculate I's norm

    %%%%%%%%%%%%%
    
    if isequal(JInd,I(1:lev+lev0)) || N<1-SafetyConst %Test if I is (strictly) 1-bounded
        MaxGreenNorm = max( MaxGreenNorm, N );  % Track the maximal norm leading to 1-bounded in order to notice numerical problem
        if N<1-SafetyConst
            Node{lev+1}=node(8,0,N,m,n);
        else
            Node{lev+1}=node(2,0,N,m,n);
        end
        Node{lev}.setChild(I(lev+lev0),Node{lev+1});

        %Find out where to track back
        k=find(0<I(lev0+1:lev0+lev) & I(lev0+1:lev0+lev)<m , 1, 'last' );
        if isempty(k) %we found a leaf I=[m,m,m,m,...]
            disp('Finite simple tree exists')
            disp(['Depth of tree is ',num2str(Depth)])
            val=1;
            break
        end
        %Go back to level k, go into next branch
        lev=k;
        if isequal(k,1) %i.e. we backtrack down to level 1 
            B{lev}=A(:,:,I(k+lev0)+1)*RootMatrix;
        else
            B{lev}=A(:,:,I(k+lev0)+1)*B{lev-1};
        end
        I(lev+lev0)=I(k+lev0)+1;
    else %if I is not 1-bounded
         Node{lev+1}=node(1,0,N,m,n);
         Node{lev}.setChild(I(lev+lev0),Node{lev+1});
        if lev>=maxlevel %maximum level reached

            disp('Maximum level reached - no result.')
            val=0;
            break %Algorithm terminates with "no result"
        else

            % Go to child [I,1]
            lev=lev+1; %augment level
            Depth=max(Depth,lev);  % adapt depth of tree
            B{lev}=A(:,:,1)*B{lev-1};  % calculate coded matrix product
            I(lev+lev0) = 1;  % set I to the desired child
        end
    end
end
disp(['Number of nodes in the tree: ',cnt+1]) % +1 for root