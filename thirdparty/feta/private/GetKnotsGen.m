function [ K ] = GetKnotsGen( A, it, K );
L = K;
for i = 1:it
    for k = 1:size( A, 3 );
        L = [L A(:,:,k)*K]; end;
    K = transpose( unique(L','rows') );
    C = convhulln( K' );
    K = K(:,unique( C )); end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
