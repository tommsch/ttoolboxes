function[ A_I ] = GetMat( A, I );

    A_I = eye( size(A(:,:,1)) );
    for i = 1:length(I)
        A_I = A(:,:,I(i))*A_I; end;
    
end
    
function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.

