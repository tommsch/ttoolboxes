
function[B_up,B_down,B_right,B_left]=getBoundsEigValArbitraryk0(lambda,k0)

r=abs(lambda);
phi=angle(lambda);
phi=mod(phi,2*pi);
if k0==0
   B_right=ones(size(lambda));
else
    B_right=zeros(size(lambda));
end
B_left=zeros(size(lambda));
B_up=zeros(size(lambda));
B_down=zeros(size(lambda));

for i=1:length(lambda)
    if abs(r(i)-1)<10^(-13) %eigenvalue with modulus 1 -> skip numerical inaccurancy + avoid infinite loop
        B_right(i)=1;
        B_left(i)=-1;
        if abs(sin(phi(i)))<10^(-13) %lambda(i) real
            if cos(phi(i)) >= 0 % if lambda(i) positive
                B_left(i)=1;
            end
        else
            B_up(i)=1;
            B_down(i)=-1;
        end 
    else
        k=k0+1; % k0=0 gives bound B_right=1 but no other bounds
        while 1
              B_right(i)=max(B_right(i),(r(i)^k)*cos(phi(i)*k));
              B_left(i)=min(B_left(i),(r(i)^k)*cos(phi(i)*k));
              if abs(sin(phi(i)))< 10^(-13) %lambda(i) real (regard seperately to avoid infinite loop)
                  BoundRad=min([B_right(i),abs(B_left(i))]);
              else   
                  B_up(i)=max(B_up(i),(r(i)^k)*sin(phi(i)*k));
                  B_down(i)=min(B_down(i),(r(i)^k)*sin(phi(i)*k));
                  BoundRad=min([B_up(i),abs(B_down(i)),B_right(i),abs(B_left(i))]);
              end
              k=k+1;
              if r(i)^k <= BoundRad
                 break %since abs(cos(phi(i)*k))<=1 and abs(sin(phi(i)*k))<=1 for all phi,k
              end
        end
    end
end

