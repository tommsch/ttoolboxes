function[Tplus]=TPlus(T,S,U,V,CC,Tup)
% In Tplus, all Matrices T_(s_1,...,s_i) (with i variable from loop in TArray) will be stored.
% s_1,...,s_i can independently take values between 1 and d (space dimension).
% Tup is a matrix whose rows contain all possibilities for vectors
% s_1,...,s_fuz (NOT s_1,...,s_i)
% input T is the array containing the Matrices T_(s_1,...,s_(i-1))

% Remark: In case of CC=[] (i.e. no complex couple of Eigenvalues), a Matrix
% T_(s_1,...,s_i) is calculated via (S*V)(:,s_i)*(U*T_(s_1,...,s_(i-1)))(s_i,:).
% This product is stored in Tplus.
% Otherwise, SubsCC sums up Matrices of complex conjugated couples and
% replaces them in Tplus.

i=ndims(T)-1; % i is variable from loop in TArrayGen, implicitly given in the size of T. If i=1, then T is just a matrix.
d=length(S(:,1));
% create an array TPlus of the appropriate dimensions
Thelp=cell(1,d);%(length(U(1,:)))
for r=1:d
    Thelp{r}=T;
end
Tplus=cat(ndims(T)+1,Thelp{:}); %initialisation of Tplus

dimsize=repmat(d,1,i-1);

% Make Tup a matrix that stores all possibilities for vectors
% s_1,...,s_i (before: s_1,...,s_fuz)
Tup=unique(Tup(:,1:i),'rows');

% Fill Tplus with matrices corresponding to tupels s_1,...,s_i
for k=1:length(Tup(:,1))
    tup=Tup(k,:); %current tupel s_1,...,s_i
    tupCell=num2cell(tup); %current tupel as cell
    tupbeg=tup(1:end-1); %corresponing tupel s_1,...,s_(i-1)
    tupbegCell=num2cell(tupbeg); %corresponing tupel as cell

    % Calculate the Matrix corresponding to tup and store it in Tplus (using the Matrix corresponding to tup(1:end-1) stored in T)
    H2=S*V;
    if ndims(T)-2==0 %i.e. length(tup)==1 and length(tupbeg)==0
        H1=U*T;
        Tplus(:,:,tup)=H2(:,tup)*H1(tup,:);
    elseif ndims(T)-2==1 %i.e. length(tup)==2 and length(tupbeg)==1
        H1=U*T(:,:,tupbeg);
        Tplus(:,:,sub2ind([dimsize,d],tupCell{:}))=H2(:,tup(end))*H1(tup(end),:);
    else
        H1=U*T(:,:,sub2ind(dimsize,tupbegCell{:}));
        Tplus(:,:,sub2ind([dimsize,d],tupCell{:}))=H2(:,tup(end))*H1(tup(end),:);
    end
end

% Replace matrices of complex conjugated couples by sum and difference
TupBegin=unique(Tup(:,1:end-1));
if isempty(TupBegin)
    tmp=[];
    Tplus=SubsCC(Tplus,dimsize,d,tmp,CC);
else
    for i = 1:length(TupBegin(:,1))
        tmp=TupBegin(i,:);
        Tplus=SubsCC(Tplus,dimsize,d,tmp,CC);
    end
end