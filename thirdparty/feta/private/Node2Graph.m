function [ gr ] = Node2Graph( child );
    gr = zeros( numel(child) );
    gr = Node2Graph_worker( 1, gr, child );
    
end

function [ gr ] = Node2Graph_worker( depth, gr, child );
    for j = 1:numel( child );
        if( ~isempty(child{j}) && ~isempty(child{j}.child) );
            gr(depth, j) = 1;
            imagesc(gr);
            drawnow;
            gr = Node2Graph_worker( depth+1, gr, child{j}.child );
            
        end;
    end
end

