function [ val ] = CombAllValGen( W, fuz, Tarray, Tup, I, h_nrm );

a=ones(1,fuz); %a is a vector that contains in a(k) an index that refers to a row of W{J_{i_k}}
d=size(W{1},2); %space dimension

dimsize=repmat(d,1,fuz);
Parray=zeros(size(Tarray)); %Initialisation
j=1;
val=0;
while j <= fuz
      cp=CoeffProdGen(W,a,Tup,fuz,I); %calculate the scalars that are multiplied with T_(s_1,...,s_fuz)
      %Over all tupels tup of Tup:
      %Multiply T_tup with the coefficient product cp_tup (depending on a)
      
      for i=1:length(cp) %length(cp) is equal to number of rows of Tup which is d^fuz
          if fuz > 1
            tupCell=num2cell(Tup(i,:));
            Ind=sub2ind(dimsize,tupCell{:});
          else
            Ind=Tup(i,:);
          end
          Parray(:,:,Ind)=cp(i)*Tarray(:,:,Ind);
      end
      %Summands cp_tup*T_tup are summed along tup
      for i=3:ndims(Parray)
          Parray = sum(Parray,i);
      end
      val = max( val, h_nrm(Parray) );
   
      if val >= 1
         break
      end
      j=1;
      while j <= fuz
          if a(j)< 2^d
            a(j)=a(j)+1;
            break
          else
            a(j)=1;
            j=j+1;
          end
      end
end
    

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.

