function [ h_nrm ] = feta_setnorm( A, p, it, ScaleMaxK )
% Written by: Clauda Möller
% Changed by: tommsch, 2022
       
switch p;
    case {'ipa'};
        VV = makepolytope( A );
        h_nrm = @( T ) polytopenorm( T, VV, 'c','matrix', 'output',2 );
        
    case {'poly','p','polytope'};
         disp( 'Computation of F and K has to be done.' );
        if( isequal(ScaleMaxK,0) );
            ScaleMaxK = []; end;
        
        if( isempty(ScaleMaxK) );
            disp( 'The polytope norm is computed with default ScaleMaxK=[], i.e. no optimisation of convex hull.' );
        else;
            disp( ['The number of vertices for the unit ball of the polytope norm is limited to ',num2str(ScaleMaxK*length(A(1,:,1))*2),'. To change this, modify parameter ScaleMaxK.'] ); end;
        
        [ F, K ] = GetEdgesFacesPolynorm( A, it, ScaleMaxK );
        h_nrm = @( T ) max( max( abs( F * T * K ) ) );
    case {'1Norm','1',1}
        h_nrm = @( T ) norm( T, 1 );
    case {'2Norm','2',2}
        h_nrm = @( T ) norm( T, 2 );
    case {'infNorm','inf',inf}
        h_nrm = @( T ) norm( T, inf );
%   case {'w1Norm','w1'};
%        h_nrm = @( T ) norm( WMat * T * WMatInv, 1 );
%    case {'w2Norm','w2'};
%        h_nrm = @( T ) norm( WMat * T * WMatInv, 2 );
%    case {'wInfNorm','winf'};
%        h_nrm = @( T ) norm( WMat * T * WMatInv, inf );
    otherwise;
        error( 'moeller:norm', 'Choice of p is not defined.' ); end;
    


end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
