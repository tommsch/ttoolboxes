
function [ FNorm ] = setnorm( A, fuz, I, U, V, W, p, CC )

if fuz==0 % i.e. I codes singleton {M}
    disp('fuz==0 not expected in setnorm.m')
    return
else
    d=length(A(:,1,1));
    Tup=IndexComb(d,fuz); % matrix whose rows contain all vectors of length fuz with entries from {1,...,d}
    
    Tarray=TArrayGen(A,I,U,V,CC,Tup,fuz);
    
    val=CombAllValGen(W,fuz,Tarray,Tup,I,p);

    FNorm=val;
end



