function [ k ] = Decision( J, I, fuzcnt, neglimit, startlevel, maxlevel, OldDec, lev )
%%% "fuzcnt==neglimit" means that no more generator edges are allowed in the
%branch
%%% "OldDec==1" means that we already decided to choose simple edges (should
%be last choice)
%%% "lev+1<startlevel" means that we are too low in the tree to
% choose generator edges
if fuzcnt==neglimit || OldDec == 1 || lev+1<startlevel || lev+1==maxlevel
    k=1;

else %specify here special decision strategies (depending on A and J)
  
    %Insert generator edge if some powers of J are prefix %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    k=1;
    for i=1:length(J)
        c=floor((startlevel-1)/length(J{i}));
        assert( c>0, 'VerifyOutput:OutOfBounds', 'Error: One of the generators is to long to be suffix in the current startlevel. Please increase startlevel.' );
        if isequal(I(end-c*length(J{i})+1:end),repmat(J{i},1,c)) %&& isequal(c,0)==0    
            k=-i;
            INeg=I(I<0);
            if ~isempty(INeg) && INeg(end)==k %never use the generator edge that is last in the current path
                k=1;
            end
            break
        end
    end
    if isequal(OldDec,k) || (isempty(I)==0 && isequal(I(end),k))  % Do not try the same generator edge again and again
        k=1;
    end
  
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     %Try one after the other
%     if OldDec==0
%         k=-1;
%     else
%         k=OldDec-1; 
%     end
%     if isempty(I)==0 & I(end)==k % Avoid [I,k,k], contains no new information if k<0
%         k=k+1;
%     end
%     if k==-n-1
%             k=1;
%     end

    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end

end