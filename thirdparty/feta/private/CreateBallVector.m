function [ BV ] = CreateBallVector( AJ, LimRadConstr, LimRadComput, Max_k0, h_nrm );

[AInf, lambda, T, AbsSubdom, IndexDom] = DetermineLimitProperties( AJ );

IndexSmall = setdiff( 1:length(lambda), IndexDom );
NormSmall = zeros( 1, length(IndexSmall) );
for i = 1:length( IndexSmall );
     NormSmall = h_nrm( T(:,:,IndexSmall(i)) ); end;

% Determine k0 based on LimRadConstr
if( isempty(AbsSubdom) );  % AbsSubdom empty: only eigenvalues with abs 1, IndexDom empty: weak generator
    disp(' Warning: Generator matrix close to identity.' );
    k0 = 0;
else;
    k0 = ceil( log(LimRadConstr/sum(NormSmall))/log(AbsSubdom) ); end;

if( k0 > Max_k0 );
    disp( ['Found k0 = ',num2str(k0),' bigger than Max_k0 = ',num2str(Max_k0),'.'] );
    disp( ['Setting k0 = ',num2str(Max_k0),' (for this generator).'] );
    k0 = Max_k0;
    disp( ['Adapting the radius of the limit ball: LimRadConstr= ',num2str(LimRadConstr),'.'] ); end;

% Instead of using a limit ball with radius LimRadConstr, we compute the
% more exact radius r_k0:
% Compute radius r_k0 that describes the distance of A^k0 to AInf. Then A^k
% < AInf for all k > k0
AbsLambdaSmall = abs( lambda(IndexSmall).^k0 );
r_k0 = sum( AbsLambdaSmall.*NormSmall );  % This is a closer bound for k0 than LimRadConstr 
disp( ['Using r_k0=',num2str(r_k0),' instead of LimRadConstr=',num2str(LimRadConstr),'.'] );

% create vector that contains balls (AJ^k,0) for k<=k0 and (AInf,r_k0)
BV = ball.empty( k0+2, 0 );
BV(1) = ball( eye(size(AJ)), 0, h_nrm );
for i = 1:k0
    BV(i+1) = AJ * BV(i); end;
BV(k0+2) = ball( AInf, r_k0, h_nrm );

if( LimRadComput <= r_k0 );
    disp( 'LimRadComp < radius of limit ball' );
    disp( 'Eliminating the trivial balls that are contained in limit ball.' );
    % Clean ball vector of those balls (A^k,0) that are contained in (AInf,r_k0)
    KeepList = [];
    for i = 1:k0+1;
        if( ~(BV(i)<BV(end)) );
           KeepList = [KeepList i]; end; end;
    KeepList=[KeepList,k0+2];

    BV = BV(KeepList);
    if( length(KeepList) < k0+2 );
        disp( ['BV successfully reduced to ',num2str(length(KeepList)-1),' balls with radius 0 plus 1 ball with radius r_k0 (last entry).'] );
        disp( ['(In comparison to ' num2str(k0+1),' balls with radius 0 plus 1 ball with radius r_k0.)'] );
    else;
        disp( 'Reduction of ballvector not possible.' ); end; end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
