function [ rho, MaxGreenNorm, Node ] = CallTree( A, J, maxlevel, startlevel, neglimit, SafetyConst, Max_k0, ...
                               cleaning, DisableEdgecnt, ...
                               LimRadComput, LimRadConstr, NormApproach, MaxGreenNorm, h_nrm );
                           
if( isempty(NormApproach) );
    NormApproach = 'BallVec'; end;

%%% necessary for 'BoundEig'

k0 = 0;

if( neglimit > 1 && k0>0 );
    disp( 'Warning: Due to neglimit > 1, k0 is set to 0.' );
    k0 = 0; end;

AJ1000 = [];
AJPow = [];
U = [];
V = [];
CC = [];
W = [];
BallVec = [];
%d = size( A(:,:,1), 1 );

%%%%%%%%% Scale matrix family s.t. rho(A_J{1})=1 %%%%%%%%%%%%%%%%%%%%%%%%%%%
A_J = GetMat( A ,J{1} );
[~, Dsc] = eig( A_J ); %use Vsc later on, scaling does not change eigenvectors

AbsD = abs( diag(Dsc) );
[~, i] = max( AbsD );
lambda_max = Dsc(i,i);

alpha = abs( lambda_max )^(-1/length( J{1} ));

disp( 'A is scaled s.t. generator matrix has spectral radius 1.' );
A = alpha*A;
% Check if transformation lead to either dominant eigenvalue(s) 1 or a
% complex conjugated couple
if( isequal(NormApproach,BallVec) );
    [AInf, lambda, T, AbsSubdom, IndexDom, change] = DetermineLimitProperties( GetMat(A,J{1}) );  %#ok<ASGLU>
    if( change==1 );
        disp( 'Rescale' );
        A = -A; end; end;

%%%%%%%%%%% Compute a vector of balls for all generators in J
if( isequal(NormApproach,'BallVec') );
    BallVec = cell( 1, length(J) );
    for i = 1:length( J );
        % Get limit matrices and ball vectors of the scaled generator matrices
        % (ball vector contains balls (A^0,0),...,(A^k0,0),(AInf,LimRadComp) with k0 determined depending on LimRadComstr)
        disp( ['Determining ballvector for J = [', num2str(J{i}(:))',']'] );
        A_J = GetMat(A, J{i} );
        assert( LimRadConstr<=LimRadComput, 'VerifyOutput:OutOfBounds', 'Error: LimRadConstr must be chosen <= LimRadComput!' );
        BV = CreateBallVector( A_J, LimRadConstr, LimRadComput, Max_k0, h_nrm );
        BV = ReduceBallVector( BV, LimRadComput, h_nrm );
        BallVec{i} = BV; end;
    
elseif( isequal(NormApproach,'BoundEig') );
    V = cell( 1, length(J) );
    U = cell( 1, length(J) );
    W = cell( 1, length(J) );
    CC = cell( 1, length(J) );
    AJ1000 = cell( 1, length(J) );
    AJPow = cell( length(J), k0 );
    for i = 1:length( J );
        % Get eigenvalues of the scaled generator matrices
        A_J = GetMat( A, J{i} );
        [X, D] = eig( A_J );
        V{i} = X;
        lambda = diag( D )';
        U{i} = inv( X );
        % Calculate CC (Indizes of complex conjugated couples in lambda)
        CC{i} = CompConj( lambda );
        % Calculate extreme values of any power of lambda
        [Lmin, Lmax] = LMinMaxBounds( lambda, CC{i}, k0 );
        W{i} = EstPowVal( Lmin, Lmax );
        AJ1000{i} = A_J^1000;
        AJPow{i,1} = eye( size(A_J) );
        for j = 2:k0
            AJPow{i,j} = A_J * AJPow{i,j-1}; end; end;
else;
    error( 'VerifyOutput:OutOfBounds', 'Error: The choice for NormApproach is unknown.' ); end;
    


if( neglimit==0 );
    disp( 'Looking for a simple tree (no generator edges)' );
    assert( length(J)==1, 'VerifyOutput:OutOfBounds', 'Error: Size of J is too big. There cannot be a simple tree for the scaled family if J is not dominant.' );
    assert( ~h_nrm(GetMat(A,J{1}))>1, 'VerifyOutput:OutOfBounds', 'Error:Reconsider your choice of norm. There cannot be a simple tree for the scaled family if norm(A_J) is not equal to one.' );
    disp('The family was scaled (J non-empty).')
    [res, MaxGreenNorm, Node] = SimpleTree( A, J, maxlevel, SafetyConst, MaxGreenNorm, h_nrm );
else;
    disp( 'Looking for a set-valued tree' );
    tball = tic;
    if( isequal(NormApproach,'BallVec') );
        [res, MaxGreenNorm, Node] = BallVecTree( A, J, maxlevel, startlevel, neglimit, SafetyConst, DisableEdgecnt, BallVec, cleaning, MaxGreenNorm, h_nrm );        
    else;
        [res, MaxGreenNorm, Node] = BoundEigTree( A, J, maxlevel, startlevel, neglimit, SafetyConst, DisableEdgecnt, k0, AJ1000, AJPow, U, V, W, CC, MaxGreenNorm, h_nrm ); end;
                   
    toc( tball ); end;


if( res==1 );
    disp( ['jsr=',num2str(1/alpha)] );
    rho = 1/alpha;
else;
    rho = [];
    disp( 'No result' ); end;

end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
