
function[Tup]=IndexComb(d,fuz)
% Generates matrix Tup whose rows contain all vectors of length fuz with
% entries from {1,...,d}

Ind_mult=repmat(1:d,1,fuz);
Tup = combnk(Ind_mult,fuz);
Tup = unique(Tup,'rows');
