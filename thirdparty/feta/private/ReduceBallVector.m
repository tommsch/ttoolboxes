function [ BV ] = ReduceBallVector( BV, LimRadComput, h_nrm );

if( LimRadComput < BV(end).rad );
    return; end;

NumbTrivBall = numel(BV)-1;
TrivBallInd = 1:numel(BV)-1;
MaxNumbContainedInd = 0;
IndexNewMidpoint = 0;
ContainedForNewMidpoint = [];
disp( 'Reducing ballvector - please wait' );
for i = TrivBallInd
    if( BV(end) < ball( BV(i).mid, LimRadComput, h_nrm ) );
        I = [1:i-1,i+1:NumbTrivBall];
        ContainedInd = [];
        for j = I;
            if( BV(j) < ball( BV(i).mid, LimRadComput, h_nrm ) );
                ContainedInd = [ContainedInd j]; end; end;
        
        if( length(ContainedInd) > MaxNumbContainedInd );
            MaxNumbContainedInd = length( ContainedInd );
            IndexNewMidpoint = i;
            ContainedForNewMidpoint = ContainedInd; end; end; end;

%Reduce BV: Take balls with radius 0 for k=0,...,new_k0 -1 (i.e.
%BV(1:new_k0)) and then ball with middle point A^(new_k0) and radius r_k0.
%All higher powers are contained by choice of new_k0.
if( ~isempty(ContainedForNewMidpoint) );
    NewTrivBallInd = setdiff( TrivBallInd, ContainedForNewMidpoint );
    BV = [BV(NewTrivBallInd) ball( BV(IndexNewMidpoint).mid, LimRadComput, h_nrm )];
    disp( ['BV successfully reduced to ',num2str(length(NewTrivBallInd)),' balls with radius 0 plus 1 ball with radius LimRadComput (last entry).'] );
    disp( ['(In comparison to ' num2str(NumbTrivBall),' balls with radius 0 plus 1 ball with radius LimRadConstr.)'] );
else;
    disp('Reduction of ballvector not possible.'); end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.

        