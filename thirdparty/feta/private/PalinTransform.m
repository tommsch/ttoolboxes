function[E,JDom,alpha]=PalinTransform(A,J)
% A is family of 2 palindromic matrices
% J contains a generator pair J1,J2

%%% Check if number of matrices = 2
assert( size(A,3)<=2, 'VerifyOutput:OutOfBounds', 'Error: palin=1, but A consists of more than 2 matrices.' );

%%% Check if J contains generator pair:
assert( isequal(length(J),2), 'VerifyOutput:OutOfBounds', 'Error: If palin=1, J has to consist of a generator pair.' );
                

J1 = J{1};
J2 = J{2};
ell = length(J1);
assert( length(J2)==ell, 'VerifyOutput:OutOfBounds', 'Error: If palin=1, J has to consist of a generator pair.' );
% if J1 and J2 are inverse and m=2, J1+J2==[3,3,...,3]
assert( ~any(J1+J2-repmat(3,1,ell)), 'VerifyOutput:OutOfBounds', 'Error: If palin=1, J has to consist of a generator pair.' );

R=zeros(size(A(:,:,1)));
d=length(A(1,:,1));
for i=1:d
    R(i,d-i+1)=1;
end

assert( ~any(abs(R*A(:,:,1)*R-A(:,:,2)))>10^-14, 'VerifyOutput:OutOfBounds', 'Error: If palin=1, the matrices must be palindromic.' );

A_J=GetMat(A,J{1});
[~,Dsc] = eig(A_J);
[lambda_max,~] = max(abs(diag(Dsc)));
alpha=lambda_max^(-1/length(J{1}));
A(:,:,1)=alpha*A(:,:,1);


E(:,:,1)=A(:,:,1)*R;
E(:,:,2)=R*A(:,:,1);



JJ=zeros(1,2*length(J1));
for i=1:2:length(J1)
   JJ(i)=J2(i);
   JJ(i+length(J1))=J1(i);
end
for i=2:2:length(J1)
   JJ(i)=J1(i);
   JJ(i+length(J1))=J2(i);
end
JDom{1}=JJ;

end
