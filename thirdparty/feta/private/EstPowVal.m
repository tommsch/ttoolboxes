function[W] = EstPowVal(Lmin,Lmax)
% Generates a matrix W whose rows contain all combinations of upper and
% lower bound for the eigenvalue powers:
% The column j of W will contain the extreme value that powers of j-th
% eigenvalue can reach. Each row corresponds to one combination of extreme values.

n=length(Lmin); %number of eigenvalues (space dimension)

% Initialise W with rows containing all vectors of length n with entries
% in 0,1
W=zeros(2^n,n);
for j=2:2^(n)
    v=dec2bin(j-1)-'0';
    diff=(n)-length(v);
    if diff > 0
        v=[zeros(1,diff),v];
    end
    W(j,:)=v;
end

% Fill in the upper and lower bounds of the eigenvalue 
% corresponding to the current column.
for i=1:n
    W(find(W(:,i)==0),i)=Lmin(i);
    W(find(W(:,i)==1),i)=Lmax(i);
end