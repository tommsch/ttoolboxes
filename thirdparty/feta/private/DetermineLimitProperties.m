function [AInf, lambda, T, AbsSubdom, IndexDom, change ] = DetermineLimitProperties( A );

change = 0;
d = length(A(1,:));
% Determine V,U s.t. A = V*D*U
[V, D] = eig( A );  % the columns of V contain the right eigenvectors of A
U = inv( V );
lambda = diag( D )';

r = abs( lambda );
phi = mod( angle(lambda), 2*pi );
IndexDom = find( abs(r-1)<10^(-13) );  % Search for eigenvalues close to 1 in modulus
if( isempty(IndexDom) );
   disp( 'J is a weak generator' );
   IndexDom = find( max(abs(lambda))==abs(lambda) ); end;
assert( max(abs(imag(lambda(IndexDom))))<=10^(-13), ...  % complex dominant eigenvalue
        'VerifyOutput:OutOfBounds', ['Error: Generator Matrix with complex dominant eigenvalue detected! (Imaginary part ',num2str(max(abs(sin(phi(find(abs(sin(phi(IndexDom))))>10^(-13)))))),'.) Only real and positive leading eigenvalues are allowed.'] );
    
if( any(cos(phi(IndexDom))< 0) );  % Negative real leading eigenvalue detected, family has to be scaled by -1
   change = 1; end;

if( length(IndexDom)==d );
    AbsSubdom = [];
else;
    AbsSubdom = max( r(setdiff(1:d,IndexDom)) ); end;  % AbsSubdom is the absolute value of the subdominant eigenvalue

disp( ['Subdominant eigenvalue of scaled generator = ' num2str(AbsSubdom)] );
% partition of A via the eigenvalues*matrices: A=sum(lambda(i)*T(:,:,i))
T = zeros( d, d, d );
for i = 1:d
    T(:,:,i) = V(:,i)*U(i,:); end;

AInf = sum( T(:,:,IndexDom), 3 );  % This is because AInf=sum(lim k (lambda(i))^k*T(:,:,i))and lambda(i)^k=1 for i from IndexDom and lambda(i)^k=0 else.

2;

end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
