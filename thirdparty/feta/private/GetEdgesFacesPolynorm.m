function [ F, K ] = GetEdgesFacesPolynorm( A, it, ScaleMaxK );
% Get edges K and faces F of the polytope  
% If ScaleMaxK=[] then OptimiseEdges will not be executed.
    d = length( A(1,:,1) );
    K = [zeros( d, 1 ) eye( d ) ones( d, 1 )];
    [F, K] = PolytopNormGen( A, K, it, ScaleMaxK );

    disp(['Number of facets:', num2str(size(F,1))])
    disp(['Number of vertices:', num2str(size(K,2))])

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.

    