function[CC]=CompConj(lambda)
% Generates a matrix whose rows contain the indices of complex conjugated
% couples of eigenvalues

CC=[];
% Find indices of complex eigenvalues
J=find(abs(lambda-real(lambda))>=10^-13);
% Search for complex conjugated couples
while length(J)>1    
    k=find(abs(lambda(J(1)) + lambda(J) - 2*real(lambda(J(1))))< 10^-13);
    if length(k)>1
        [r,k]=min(abs(lambda(J(1)) + lambda(J) - 2*real(lambda(J(1)))));
    end
    if isempty(k)
        break
    end
    % Each row of CC shall contain the indices of a couple
    if angle(lambda(J(1)))>0
        CC=[CC;J(1),J(k)];
    else
        CC=[CC;J(k),J(1)];
    end
    J=J([2:k-1,k+1:end]);
end