function [ v ] = fixnorm( I, k0, A, J, fuz, AJPow, h_nrm )

% output v contains the maximal value of norms that is reached by powers up to k0 of
% generator matrices.

v = 0; % initalisation so that setnorm.m will be called if k0=0 or fuz=0 
if( k0 <= 0 || fuz==0 );
    return; end ;

% cut the index "in pieces" that are seperated by the negative entries, calculate product matrix to each completely positive piece
B=cell(1,fuz+1);
b = find(I<0); % b contains the positions of negative entries in I (b is not empty since fuz~=0.)
s = I(1:b(1)-1);
B{1}=GetMat(A,s);
for j = 2:fuz
    s = I(b(j-1)+1:b(j)-1);
    B{j} = GetMat( A, s ); end;

s = I(b(end)+1:end);
B{fuz+1} = GetMat( A, s );


% case dinstinction just for speed-up in case fuz=1
if fuz==1
    for j = 1:k0
        v = max( h_nrm(B{2}*AJPow{-I(b),j}*B{1}), v );
        if( v>1 );
            break; end; end;
else;
    % the rows of kPoss contain all possible combinations of powers between 0 and k0
    % example: k0=3, fuz=2 i.e. kPoss contains
    % 0,0;0,1;0,2;0,3;1,0;1,1;1,2;1,3;...
    Ind_mult=repmat(0:k0,1,fuz);
    kPoss=combnk(Ind_mult,fuz);
    kPoss=unique(kPoss,'rows');
    %Calculate norm for any combination of powers up to k0
    for i=1:length(kPoss(:,1))
        P=eye(size(A(:,:,1)));
        APow=cell(1,fuz);
        for j=1:fuz
            APow{j}=GetMat(A,repmat(J{-I(b(j))},1,kPoss(i,j)));
            P = APow{j}*B{j}*P;
        end
        P=B{fuz+1}*P;
        v = max( h_nrm(P), v );
        if( v>1 );
            break; end; end; end;

end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.

