function[Tarray]=TArrayGen(A,I,U,V,CC,Tup,fuz)
b = find(I<0); % b contains the positions of negative entries in I (b is not empty if TArray is called.)

% cut the index in pieces, calculate product matrix to each piece
B=cell(1,fuz+1);
s = I(1:b(1)-1);
B{1}=GetMat(A,s);
for j=2:fuz
    s=I(b(j-1)+1:b(j)-1);
    B{j}=GetMat(A,s);
end
s=I(b(end)+1:end);
B{fuz+1}=GetMat(A,s);


T=B{1};
for i=1:fuz
    S=B{i+1};
    T=TPlus(T,S,U{-I(b(i))},V{-I(b(i))},CC{-I(b(i))},Tup);
end
Tarray=T;
