function [ nc ] = Normcheck( I, A, fuz, AJ1000, h_nrm );

nc = 0;  % initalisation so, that setnorm.m will be called if fuz=0 
if( fuz==0 );
    return; end;

% cut the index "in pieces" that are seperated by the negative entries, calculate product matrix to each completely positive piece
B=cell(1,fuz+1);
b = find(I<0); % b contains the positions of negative entries in I (b is not empty since fuz~=0.)
s = I(1:b(1)-1);
B{1}=GetMat(A,s);
for j = 2:fuz
    s=I(b(j-1)+1:b(j)-1);
    B{j}=GetMat(A,s);
end
s=I(b(end)+1:end);
B{fuz+1}=GetMat(A,s);


P=B{1};
for j=2:fuz+1
    P=B{j}*P; %calculate special case k=0
end
PNorm = h_nrm( P );
if( PNorm>1 );
    nc = PNorm;
else;
    Q = eye( size(A(:,:,1)) );
    for j = 1:fuz
        Q = AJ1000{-I(b(j))}*B{j}*Q; end;  % calculate special case k=1000
    
    Q = B{fuz+1}*Q;
    nc = max( PNorm, h_nrm(Q) ); end;
    
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.

