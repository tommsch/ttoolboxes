function [ F, K ] = PolytopNormGen( A, K, it, varargin );
if( nargin>4 );
    disp('Problem in PolytopNormGen: Number of arguments not correct.')
elseif( nargin==4 );
    ScaleMaxK = varargin{1};
else;
    ScaleMaxK = []; end;

K = GetKnotsGen( A, it, K );
disp( 'K is determined.' );
if( isempty(ScaleMaxK)==0 );
    disp( 'Optimization of edges.' );
    K = OptimiseEdges( K, ScaleMaxK ); end;  % decrease number of potential edges (and therewith number of faces)

K = [K,-K];

disp( 'Computing inequalities of hyperplanes.' );
[B, b] = vert2con( K' );

disp( 'Normalising Hyperplanes.' );
F = B./repmat( b, 1, length(B(1,:)) );  % norm hyperplanes Bx=b s.t. right side = 1

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.

    