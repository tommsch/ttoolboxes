function [ val, MaxGreenNorm, Node ] = BoundEigTree( A, J, R, p, maxlevel, startlevel, neglimit, SafetyConst, ...
                                 DisableEdgecnt, k0, AJ1000, AJPow, ... 
                                 U, V, W, CC, MaxGreenNorm, h_nrm )
                             
R = [];
StartDelChildren = cell( maxlevel, 1 );
StartDelChildren{1} = 1;
val = 0;
m = size( A, 3 );  % m is number of matrices in family A
n = length( J );  % n is number of generators
lev0 = length(R);  % lev0 is the level of this tree's root, lev0=0 iff root=id
B = cell( maxlevel, 1 );
Node = cell( maxlevel+1, 1 );  % Node{lev} will contain the last visited node from level lev-1.
I = zeros( 1, maxlevel+lev0 );
Ind_minus = [];
PMS = cell( 1, neglimit + 1 );
if( any(R<0) );
   disp( 'Root must be a positive index vector!' );
   return; end;

I(1:lev0) = R;
RootMatrix = GetMat( A, R );  % If R=[], then RootMatrix=id
Node{1} = node( 1, 0, 10, m, n );  % This is the node describing the root.

lev = 1;  % lev is the tree level
fuz = 0;  % fuz is number of generator edges in a path
PMS{1} = A(:,:,1)*RootMatrix;  % In PMS, the prefix/middle/suffix-matrices are stored (NOT as balls to avoid inaccurate bounds of norm)
I(lev0+1) = 1;
CurrDepth = 1;  % current depth of tree (regards only the current path)
Depth = 0;  % depth of tree (is actualized whenever a leaf is found)
OldDec = 0;

GoToFuz = 0;  % GoToFuz=1 means that it is not necessary to analyse if a node is white or green (happens when deleting a generator edge -> initial node was known to be red.)
cnt = 0;  % cnt counts how often the while-loop is executed
while 1
    cnt=cnt+1;
    if mod(cnt,200)==0 %&& lev < 498
        disp(['Current Depth of tree: ',num2str(Depth)])
        disp(['Depth of current path: ',num2str(CurrDepth)])
        disp(['First entry m in current node: ',num2str(find(I(lev0:lev0+lev)==m,1,'first'))])
    end   
    iswhite=0;
    N=10; % Set value of norm to default >1 (N is not calculated if node is white.)
    if GoToFuz==0
        OldDec=0;
     %%%%%%%%% Zum Debuggen %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%          
        k=find(I(lev0+1:lev0+lev)<0); %Find generator edges in the current path

        if  isempty(k) && ~isempty(Ind_minus)
            disp('Achtung, berechnung von Ind_minus stimmt nicht')
        elseif ~isempty(k)
            if ~isequal(k,Ind_minus)
                disp('Achtung, berechnung von Ind_minus stimmt nicht')
            end
        else
        end
        if isempty(k) && fuz~= 0 
            disp('Achtung, Berechnung von fuz stimmt nicht')
        else
            if fuz~=length(k)
                disp('Achtung, Berechnung von fuz stimmt nicht')
            end
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if fuz==0  % I codes singleton
            N = h_nrm( PMS{1} );  % Calculate I's norm
        else;  % I codes denumberable set
            if( isequal(I(lev0+Ind_minus(end)+1:lev0+lev),J{-I(Ind_minus(end)+lev0)}) );  % Test if I is covered
                iswhite = 1;
            else;
                   %N>1 (as initialized) is true
                  
                   nc = Normcheck( I(1:lev0+lev), A, fuz, AJ1000, h_nrm );

                    if nc <1
                        N = fixnorm( I(1:lev0+lev), k0, A, J, fuz, AJPow, h_nrm );  % Here we regard I(1:lev0+lev), i.e. including root R!
                        if N<1-SafetyConst
                            N = max( N, setnorm(A, fuz, I(1:lev0+lev), U, V, W, p, CC) ); %Here we regard I(1:lev0+lev), i.e. including root R!
                        end
                    end
            end
        end
    end

    %%%%%%%%%%%%%

    if iswhite==1 || N<1-SafetyConst %if I is covered or (strictly) 1-bounded
        if N<1-SafetyConst
           MaxGreenNorm=max(MaxGreenNorm,N); end;  % Track the maximal norm leading to 1-bounded in order to notice numerical problem
        
        if( iswhite==1 );
            Node{lev+1}=node(3,fuz,N,m,n);
        else;
            Node{lev+1}=node(2,fuz,N,m,n); end;
        
        Node{lev}.setChild(I(lev+lev0),Node{lev+1});

        % Find out where to track back
        k = find(0<I(lev0+1:lev0+lev) & I(lev0+1:lev0+lev)<m , 1, 'last' );
        Depth = max(Depth,CurrDepth);
       
        if isempty(k) %we found a leaf I=[m,m,m,m,...]
            val=1;
            if isempty(R)
                tmp='[ ]';
            else
                tmp=num2str(R);
            end
            disp(['Finite set-valued tree with root ',tmp,' exists'])
            disp(['Depth of tree is ',num2str(Depth)])
            break
        end
        %Go back to level k, go into next branch
        lev=k;
        CurrDepth=lev;
        I(lev+lev0)=I(k+lev0)+1;
        if isequal(k,1) %i.e. we backtrack down to level 1
            fuz=0;
            Ind_minus=[];
            PMS{1}=A(:,:,I(lev0+1))*RootMatrix;
        else
            Ind_minus=find(I(lev0+1:lev0+lev)<0); %Find generator edges in the current path
            if isempty(Ind_minus)
                fuz=0; %No generetor edges -> set fuz back to 0
            else
                fuz=length(Ind_minus); %Set fuz to number of generator edges
            end
            
            if fuz==0
                PMS{fuz+1}=GetMat(A,I(1:lev0+lev));
                B{lev} = ball( PMS{fuz+1}, 0, h_nrm );
            else
                PMS{fuz+1} = GetMat( A, I(lev0+Ind_minus(end)+1:lev0+lev) ); end; end;
        
    else  % I is neither covered nor 1-bounded
        if fuz>0
            afterneg=lev-Ind_minus(end);
        else
            afterneg=0;
        end
         if lev>=maxlevel || afterneg > ceil(maxlevel/3) %maximum level reached or too far below last generator child
            Node{lev+1}=node(1,fuz,N,m,n);
            Node{lev}.setChild(I(lev0+lev),Node{lev+1});

            %%%%%%%%%%%%%%%%%%%%
            if fuz==0 % no generator edge in the path -> no possibility to go back and try a different choice
                disp('Maximum level reached - no result.')
                disp(['Search ended in node ',num2str(I)])
                break %Algorithm terminates with "no result"
            else % go back to last generator edge and try a different choice
                kk=Ind_minus(end);
                OldDec=I(kk+lev0);
                Node{kk}.deleteChild; % Node{kk} is the node in level kk-1. Its children have to be deleted

                % Go back to inital node of deleted generator edge
                Ind_minus=Ind_minus(1:end-1);
                PMS{fuz+1}=[];
                fuz=fuz-1;  % Deleting decreased number of generator edges
                lev=kk-1;
                CurrDepth=lev;
                GoToFuz=1;  % We do not need to check everything again: Node was neither green nor white.
            end
         else  % maximum level not yet reached
            Node{lev+1}=node(1,fuz,N,m,n);
            Node{lev}.setChild(I(lev+lev0),Node{lev+1});

            k = Decision( J, I(1:lev+lev0), fuz, neglimit, startlevel, maxlevel, OldDec, lev );  % decide whether simple children or generator child of I is going to be analysed. Root=I(1:lev0) is also taken into account, important in dominant generator case
            if( k<0 ); % made decision to analyse generator child [I,k]
               lev=lev+1;  % augment level
               fuz=fuz+1;  % increased number of generator edges in path
               I(lev+lev0)=k;  % set I to the desired child (= end node of generator edge)
               Ind_minus=[Ind_minus,lev];  % add index of negative entry to Ind_minus
               PMS{fuz+1}=eye(size(RootMatrix));  % PMS does not change, initialized for the next multiplication
               Node{lev+1}=node(3-k,fuz,10,m,n);
               Node{lev}.setChild(k,Node{lev+1});

               if DisableEdgecnt ~= 1
                  Node{1}.increaseEdgecnt(I(lev0+1:lev0+lev),k);
               end
            end
            %analyse simple children, starting here with [I,1]
            lev=lev+1; %augment level
            StartDelChildren{lev}=StartDelChildren{lev-1};
            CurrDepth=lev; %max(Depth,lev); %adapt depth of tree
            GoToFuz=0;
            PMS{fuz+1}=A(:,:,1)*PMS{fuz+1};
            I(lev+lev0)=1; %set I to the desired child
         end
    end
end
disp(['The while loop was executed ',num2str(cnt),' times.'])


end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.



