function[Tplus]=SubsCC(Tplus,dimsize,n,tmp,CC)
% Replace matrices corresponding to conjugated couples:
% If a,b are indices of complex conjugated eigenvalues (i.e. [a b] or [b
% a]) is a row in CC, then all matrices T_(s_1,...,s_(i-1),a) resp.
% T_(s_1,...,s_(i-1),b) are to be replaced by
% T_(s_1,...,s_(i-1),a)+T_(s_1,...,s_(i-1),b) resp.
% i*(T_(s_1,...,s_(i-1),a)-T_(s_1,...,s_(i-1),b))

% By tmp, a fixed tupel s_1,...,s_(i-1) is given.

if isempty(CC)==0
    for j=1:length(CC(:,1)) %for each complex conjugated couple do:
        if isempty(tmp) % i.e. i=1
            Tmp1=Tplus(:,:,CC(j,1));
            Tmp2=Tplus(:,:,CC(j,2));
            Tplus(:,:,CC(j,1))=Tmp1+Tmp2;
            Tplus(:,:,CC(j,2))=sqrt(-1)*(Tmp1-Tmp2);
        else % i.e. i.1   
            tmp1=num2cell([tmp,CC(j,1)]);
            tmp2=num2cell([tmp,CC(j,2)]);
            Tmp1=Tplus(:,:,sub2ind([dimsize,n],tmp1{:}));
            Tmp2=Tplus(:,:,sub2ind([dimsize,n],tmp2{:}));
            Tplus(:,:,sub2ind([dimsize,n],tmp1{:}))=Tmp1+Tmp2;
            Tplus(:,:,sub2ind([dimsize,n],tmp2{:}))=sqrt(-1)*(Tmp1-Tmp2);
        end
    end
end