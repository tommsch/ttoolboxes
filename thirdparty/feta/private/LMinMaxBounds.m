function[Lmin,Lmax]=LMinMaxBounds(lambda, CC, k0)
%Check that all eigenvalues are <=1
if any(lambda >=1+(10^-13))
    disp('STOP Generator matrix with eigenvalue > 1 detected.') 
    disp('Please correct the scaling of your matrix family.') 
    disp('Probably, you have to change the order in J (J{1} is assumed to have the biggest eigenvalue.)')
    return
end

%Find indizes of complex eigenvalues
K=find(abs(lambda-real(lambda))>=10^-13);
if isequal(size(CC,1),length(K)/2)==0
    disp('A generator matrix has at least complex eigenvalue without conjugated partner. Our implementation does not account for this case.')
    return
end

[Bk0_up,Bk0_down,Bk0_right,Bk0_left]=getBoundsEigValArbitraryk0(lambda, k0);

Lmin=Bk0_left;
Lmax=Bk0_right;

if size(CC)>0
    for k=1:length(CC(:,1))
        Lmax(CC(k,2))=Bk0_up(CC(k,1));
        Lmin(CC(k,2))=Bk0_down(CC(k,1));
    end
end
