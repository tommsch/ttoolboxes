function[K]=OptimiseEdges(K,ScaleMaxK)
MaxNumberOfK=ScaleMaxK*length(K(:,1));  %value i.e. 4*dimension of space
AbsMat=1000*eye(length(K(1,:)));
for i=1:length(K(1,:))
    R=K(:,i+1:end);
    r=K(:,i);
    AbsMat(i,i+1:end)=max(abs(R-repmat(r,1,length(R(1,:)))),[],1);
end
KIndices=1:length(K(1,:));
KeepInK=KIndices;
AbsMat=AbsMat+AbsMat';
AbsMatReduced=AbsMat;
while length(KeepInK)> MaxNumberOfK
    [a,IndRow]=min(AbsMatReduced); %a is a vector containing the minimal distance in each column of R
    [a,i]=min(a); % a is the minimal distance of points in K
    KeepInK=[KeepInK(1:IndRow(i)-1),KeepInK(IndRow(i)+1:end)];
    AbsMatReduced=AbsMat(KeepInK,KeepInK);
end
K=K(:,KeepInK);