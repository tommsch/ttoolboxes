function[AD]=DiagonalizeGen(A,Vsc)
Usc=inv(Vsc);

m=size(A,3);
for i=1:m
    AD(:,:,i)=Usc*A(:,:,i)*Vsc;
end