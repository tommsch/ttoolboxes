classdef ball
           
    properties
        mid
        rad
        nrm
        h_nrm  %handle for norm
    end

    methods
        function B = ball( m, r, h_nrm )
            if( nargin==1 );
                r = 0; end;
            B.mid = m;
            B.rad = r;
            B.nrm = h_nrm( m );
            B.h_nrm = h_nrm;
        end

        function n = ballnorm( B )
            n = 0;
            for i = 1:numel( B )
                n = max( n, B(i).nrm + B(i).rad ); end;
        end

        function C = mtimes( A, B )
            if( isnumeric(A) && isscalar(A) );
                m = A*B.mid;
                r = A*B.rad;
                C = ball( m, r, B.h_nrm );
            elseif( isnumeric(B) && isscalar(B) );
                C = mtimes( B, A );
            else;
                if( isnumeric(A) );
                    A = ball( A, 0, B.h_nrm );
                elseif( isnumeric(B) );
                    B = ball( B, 0, A.h_nrm ); end;
                m = A.mid*B.mid;
                r = A.rad*B.nrm + B.rad*A.nrm + B.rad*A.rad;
                C = ball( m, r, A.h_nrm );
            end
        end
        
        function [ C ] = minus( A, B );
            if( isnumeric(A) );
                A = ball( A, 0, B.h_nrm );
            elseif( isnumeric(B) );
                B = ball( B, 0, A.h_nrm ); end;
            m = A.mid - B.mid;
            r = A.rad - B.rad;
            C = ball( m, r, A.h_nrm );
        end
        
        function v = lt( A, B )
            if( ~isnumeric(A) && ~isnumeric(B) );
                C = minus( B, A );
                if( C.nrm > C.rad )  % mynorm(B.mid - A.mid,type) + A.rad > B.rad
                   v = 0;
                else;
                   v = 1; end;
            elseif( isnumeric(B) );
                v = 0;
            elseif( isnumeric(A) );
                v = lt( ball(A, 0, B.h_nrm), B ); end;
        end

    end
end

