function [ ret ] = has_gurobi();
% Crude check whether gurobi is installed
% [ ret ] = has_gurobi();
%
% Written by: tommsch, 2024

    persistent ret_;
    if( isempty(ret_) );
        ret_ = isequal( exist('gurobi', 'file'), 3 ); end;
    ret = ret_;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
