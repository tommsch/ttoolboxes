function [ ret ] = uninstall_gurobi( option );
% (experimental) Checks whether gurobi is installed.
% If so removes Gurobi from the Matlab path
% A subsequent call adds Gurobi to the Matlab path
%
% Written by: tommsch, 2024-10-10

%               tommsch, 2024-10-10,    Added function uninstall_gurobi (experimental)
% Changelog:    

    persistent gurobi_path;

    if( nargin == 0 );
        option = 'switch'; end;

    p = which( 'gurobi.m' );
    p = fileparts( p );
    path_check = ismember( p, strsplit(path, pathsep) );

    switch option;
        case 'switch';
            if( isempty(p) );
                option = 'install';
            elseif( path_check );
                option = 'uninstall';
            else;
                option = 'install';
                fprintf( 'I do not know whether to install or uninstall Gurobi. You are probably in the Gurobi folder.\nI decided to install Gurobi.\n' );
                end;
        otherwise;
            end; % do nothing
    
    switch option;
        case {'load','install','setup'};
            if( ~isempty(gurobi_path) );
                addpath( gurobi_path );
                gurobi_path = [];
                fprintf( 'Gurobi is <strong>added</strong> to the Matlab path: %s\nNote: The Matlab path is not saved.\n', gurobi_path );
            else;
                [~, old_path] = evalc( 'cwd( ''gurobi_setup'' );' );
                clean_path = onCleanup( @() cd(old_path) );
                ls = dir( 'gurobi_setup.m' );
                if( ~isempty(ls) );
                    gurobi_setup();
                    fprintf( 'Gurobi was <strong>installed</strong> using gurobi_setup().\n' );
                else;
                    fprintf( 2, 'Could not find nor install Gurobi.' ); end; end;

        case {'unload','uninstall','unsetup','remove','desetup'};
            if( path_check );
                fprintf( 'Gurobi is <strong>removed</strong> from the Matlab path: %s\nRun this function again to add Gurobi to the Matlab path.\nNote: The Matlab path is not saved.\n', p );
                rmpath( p );
                gurobi_path = p;
            else;
                fprintf( 'Gurobi is <strong>not</strong> on the Matlab path. Thus I cannot remove it.\n' ); end;

        case {'check','test'};
            if( isempty(p) );
                fprintf( 'Gurobi is not installed.\n' );
                ret = false;
                return; end;

            if( ~path_check );
                fprintf( 'Gurobi is not installed, but still can be found on the path. Your are probably in the directory of Gurobi.\n' ); end;

            model.A = sparse( [1 1 0; 0 1 1] );
            model.obj = [1 2 3];
            model.modelsense = 'Max';
            model.rhs = [1 1];
            model.sense = ['<' '<'];
            params.outputflag = 0;
            
            check = false;
            try;
                err = lasterror();  %#ok<LERR>
                result = gurobi( model, params );
                if( isequal(result.x,[1 0 1].') );
                    check = true; end;
            catch me;  %#ok<NASGU>
                lasterror( err ); end;  %#ok<LERR>
            if( check );
                ret = true;
                fprintf( 'Gurobi works.' );
            else;
                ret = false;
                fprintf( 'Gurobi seems to be installed, but is not working.' ); end;


        otherwise;
            error( 'uninstall_gurobi:option', 'Wrong option given.' ); end;
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*MCCD>

