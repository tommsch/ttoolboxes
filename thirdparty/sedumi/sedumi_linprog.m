function [ x_m, fval_m, exitflag_m, output_m, lambda_m ] = sedumi_linprog( varargin );
% wrapper around sedumi, to be used with the matlab linprog interface

    [f_m, A_m, b_m, Aeq_m, beq_m, lb_m, ub_m, options_m] = parse_input( varargin{:} );
    
    [A_s, b_s, c_s, K_s, pars_s, verbose] = input_matlab2sedumi( f_m, A_m, b_m, Aeq_m, beq_m, lb_m, ub_m, options_m );  %#ok<ASGLU>
    
    [msg_s, x_s, y_s, info_s] = evalc( 'sedumi( A_s, b_s, c_s, K_s, pars_s );' );
    
    if( ~success( info_s ) && verbose>=1 );
        fprintf( '%s\n', msg_s ); end;
    
    [x_m, fval_m, exitflag_m, output_m, lambda_m] = output_sedumi2matlab( msg_s, x_s, y_s, info_s, c_s, numel(f_m) );
end

function [ f, A, b, Aeq, beq, lb, ub, options ] = parse_input( f, A, b, Aeq, beq, lb, ub, options );

    % parse input
    if( nargin <= 3 );
        Aeq = [];
        beq = []; end;
    if( nargin <= 5 );
        lb = [];
        ub = []; end;
    if( nargin <= 7 || isempty(options) );
        options = struct; end;

    %m = max( [size(A, 1), size(b, 1)] );
    %meq = max( [size(Aeq, 1), size(beq, 1)] );
    n = max( [size(A, 2), size(Aeq, 2), size(lb, 1), size(ub, 1)] );

    % make matlab variables
    if( isempty(lb) );
        lb = -inf( n, 1 ); end;
    if( isrow(lb) );
        lb = lb.'; end;

    if( isempty(ub) );
        ub = inf( n, 1 ); end;
    if( isrow(ub) );
        ub = ub.'; end;

    if( isempty(A) );
        A = zeros( 0, n ); end;
    if( isempty(Aeq) );
        Aeq = zeros( 0, n ); end;

    if( isempty(b) );
        b = zeros( 0, 1 ); end;
    if( isrow(b) );
        b = b.'; end;

    if( isempty(beq) );
        beq = zeros( 0, 1 ); end;
    if( isrow(beq) );
        beq = beq.'; end;
    
    if( isrow(f) );
        f = f.'; end;

    % check variables
    assert( isequal( size(A, 1), size(b, 1) ), 'sedumi:arg', 'Dimensions of ''A'' and ''b'' not compatible.' );
    assert( isequal( size(Aeq, 1), size(beq, 1) ), 'sedumi:arg', 'Dimensions of ''Aeq'' and ''beq'' not compatible.' );
    assert( isequal( size(A, 2), size(Aeq, 2), size(lb, 1), size(ub, 1) ), 'sedumi:arg', 'Dimensions of ''A'', ''Aeq'', ''lb'' and ''ub'' not compatible.' );

end

function [ A_s, b_s, c_s, K_s, pars_s, verbose ] = input_matlab2sedumi( f_m, A_m, b_m, Aeq_m, beq_m, lb_m, ub_m, options_m );
    [pars_s, verbose] = input_matlab2sedumi_options( options_m );
    [A_s, b_s, c_s, K_s] = input_matlab2sedumi_lp( f_m, A_m, b_m, Aeq_m, beq_m, lb_m, ub_m );
end

function [ pars_s, verbose ] = input_matlab2sedumi_options( options_m );  %#ok<INUSD>

    [~, options_m] = evalc( 'struct( options_m );' );

    verbose = 1;
    pars_s = struct;
    if( isfield(options_m, 'Display') );
        if( strcmpi(options_m.Display,'final') );
            % do nothing
        elseif( strcmpi(options_m.Display,  'off') );
            verbose = 0;
            pars_s.fid = 0;
        elseif( strcmpi(options_m.Display,'iter') );
            verbose = 2;
            pars_s.err = 1; end; end;
    
    if( isfield(options_m, 'Diagnostics') );    
        pars_s.errors = 1; 
        pars_s.vplot = 0; end;
    
    if( isfield(options_m, 'Algorithm') );
        if( ~strcmpi(options_m.Algorithm,'dual-simplex') );
            warning( 'sedumi_linprog:opt', 'SeDuMi only supports dual simplex algorithm. The option ''Algorithm'' is ignored.' ); end; end;
    
    if( isfield(options_m, 'MaxIterations') );
        pars_s.maxiter = options_m.MaxIterations; end;
    
    if( isfield(options_m, 'MaxIter') );
        pars_s.maxiter = options_m. MaxIter; end;    
    
    if( isfield(options_m, 'OptimalityTolerance') );
        pars_s.eps = options_m.OptimalityTolerance; end;
    
    if( isfield(options_m, 'TolX') );
        pars_s.eps = options_m.TolX; end;
    
    if( isfield(options_m, 'PlotFcns') );
        warning( 'sedumi_linprog:opt', 'SeDuMi only supports the SeDuMi built in plot functions. Specific arguments for ''PlotFcns'' are ignored.' );
        pars_s.vplot = 1; end;      
    
    if( isfield(options_m, 'Preprocess') );
        if( strcmpi(options_m.Display, 'none') );
            pars_s.sdp = 0; end; end;
    
    expect( ~isfield(options_m, 'ConstraintTolerance'), 'sedumi_linprog:opt', 'The option ''ConstrainTolerance'' is not possible with this wrapper, and thus, ignored.' );
    expect( ~isfield(options_m, 'TolCon'), 'sedumi_linprog:opt', 'The option '' TolCon'' is not possible with this wrapper, and thus, ignored.' );
    expect( ~isfield(options_m, 'MaxTime'), 'sedumi_linprog:opt', 'The option ''MaxTime'' is not possible with this wrapper, and thus, ignored.' );
    
    
end    

function  [ A_s, b_s, c_s, K_s ] = input_matlab2sedumi_lp( f_m, A_m, b_m, Aeq_m, beq_m, lb_m, ub_m );
 
    % make working variables
    m_m = size( A_m, 1 );
    meq_m = size( Aeq_m, 1 );
    n_m = size( ub_m, 1 );

    
    % make lower bounded slack variables
    idx_l = lb_m==-inf;
    idx_l1 = find( idx_l );
    idx_l2 = setdiff( 1:numel(idx_l), idx_l1 );
    %idx_l = [idx_l1 idx_l2];
    I_l = eye( n_m );
    I_l(idx_l1,:) = [];
    
    % make upper bounded slack variables
    idx_u = ub_m==inf;
    idx_u1 = find( idx_u );
    idx_u2 = setdiff( 1:numel(idx_u), idx_u1 );
    %idx_u = [idx_u1 idx_u2];
    I_u = eye( n_m );
    I_u(idx_u1,:) = [];
    
    n_l = numel( idx_l2 );
    n_u = numel( idx_u2 );
    
    % make new LP
    A_s = [[ -A_m      zeros(m_m,n_l)        zeros(m_m,n_u)     -eye(m_m);        ]
           [  Aeq_m    zeros(meq_m,n_l)      zeros(meq_m,n_u)    zeros(meq_m,m_m) ]
           [  I_l     -eye(n_l)              zeros(n_l,n_u)      zeros(n_l,m_m)   ]
           [ I_u      zeros(n_u,n_l)         eye(n_u)            zeros(n_u,m_m)   ]] ;
    
    b_s = [-b_m; beq_m; lb_m(idx_l2); ub_m(idx_u2)];
    c_s = [f_m; zeros(n_l + n_u + m_m, 1)];
    
    K_s.f = n_m;
    K_s.l = n_l + n_u + m_m;
end

function [ flag ] = success( info );
    flag = false;
    try;
        if( info.feasratio<0.9 );
            return;
        elseif( info.pinf || info.dinf );
            return;
        elseif( info.numerr ~= 0 );
            return; end;
    catch me;  %#ok<NASGU>
        return; end;
    
    flag = true;

end

function [ x_m, fval_m, exitflag_m, output_m, lambda_m ] = output_sedumi2matlab( msg_s, x_s, y_s, info_s, c_s, m_m );

    output_m.iterations = nan;
    output_m.algorithm = 'sedumi';

    if( isempty(x_s) );
        x_m = zeros( m_m, 0 ); 
        fval_m = [];
        exitflag_m = -10;
    else;
        x_m = x_s( 1:m_m );
        fval_m = c_s( 1:m_m )' * x_m;

        exitflag_m = 1;
        if( info_s.feasratio < 0.9 );
            exitflag_m = 3; end;
        if( info_s.pinf == 1 );
            exitflag_m = -2; end;
        if( info_s.dinf == 1 );
            exitflag_m = -3; end;
        if( info_s.pinf == 1 && info_s.dinf == 1 );
            exitflag_m = -5; end;
        if( info_s.numerr == 1 );
            exitflag_m = -7; 
        elseif( info_s.numerr == 2 );
            exitflag_m = -4; end; end;

    output_m.algorithm = 'Sedumi with ttoolboxes wrapper';
    output_m.iterations = info_s.iter;
    output_m.feasratio = info_s.feasratio;
    output_m.pinf = info_s.pinf;
    output_m.dinf = info_s.dinf;
    output_m.numerr = info_s.numerr;
    output_m.r0 = info_s.r0;
    output_m.timing = info_s.timing;
    output_m.wallsec = info_s.wallsec;
    output_m.cpusec = info_s.cpusec;
    output_m.x = x_s;
    output_m.y = y_s;
    output_m.msg = msg_s;
    output_m.eps_probably_used_value = 1e-8;  % these values are taken from the documentation
    output_m.bigeps_probably_used_value = 1e-3;
       
    lambda_m = [];
    
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
