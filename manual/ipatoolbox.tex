% !TeX encoding = UTF-8 Unicode
% !TeX root = manual.tex
% !TeX spellcheck = en_GB


\chapter{\texttt{ipa}-toolbox}

This toolbox implements functions to compute the join spectral radius ($\JSR$) using the modified invariant-polytope algorithm.

\subsection*{Dependencies}
The toolbox depends on the \texttt{m}- and the \texttt{tmisc}-toolbox and partly on the \texttt{subdivision}-toolbox
but also runs without the latter.
The toolbox furthermore depends on the \texttt{Gurobi}-Solver
and the Matlab \texttt{Parallel Toolbox}.

If the \texttt{Gurobi}-solver is not installed, Matlab-functions will be used as a fall-back 
and the algorithm runs magnitudes slower.
If the \texttt{Parallel Toolbox} is not installed, the algorithm will run single-threaded.

\subsection*{Important functions}
\begin{param}
\item[findsmp%
\footnote{Copyright for algorithm \texttt{'genetic'} by~\cite{BC11}.}%
]
Searches for s.m.p.-candidates using various algorithms.

\item[invariantsubspace%
\footnote{Copyright for algorithm \texttt{'perm'} and \texttt{'basis} by~\cite{Jung2014} under the 3-clause BSD License.}%
]%
Searches for invariant subspaces of matrices using various algorithms.

\item[gallery\_matrixset]%
Returns sets of matrices, mostly used for \texttt{ipa}.

\item[polytopenorm]
Computes the norm of a vector or matrix with respect to a (cone, symmetric or elliptic) polytope.

\item[ipa]%
Computes the joint spectral radius.

\end{param}

\subsection*{Remaining functions}
\begin{param}
%\item[addcombination] Constructs short cycles from long cycles.
\item[blockjsr] Returns the JSR of block diagonal matrices, given the JSR of the blocks.
\item[chooseval] Selects highest values of a vector.
\item[codecapacity] Returns matrices whose JSR is related to their capacity, given forbidden differences.
\item[complexleadingmatrix] Constructs a matrix with complex leading eigenvalue
\item[computepolytopenorm] Computes the Minkowski-norm.
\item[daubechiesmatrix\footnote{Copyright for algorithm \texttt{'jung'} by~\cite{Jung2014} under the 3-clause BSD License. Copyright for algorithm \texttt{'gugl'} by Nicola Guglielmi.}] Constructs matrices whose $\JSR$ is related to the Daubechies' wavelets regularity.
\item[estimatejsr] Rough estimate of the $\JSR$.
\item[estimatepolytopenorm] Estimates the Minkowski-norm.
\item[extravertex] Finds vertices such that given polytope has non-empty interior.
\item[findsmp\_old]%
\footnote{Copyright for algorithm \texttt{'genetic'} by~\cite{BC11}. Copyright for algorithm \texttt{'gripenberg'} by~\cite{Jung2014} under the 3-clause BSD License.}
{\bfseries Deprecated} 
Old implementation of \texttt{findsmp}
\item[intersectinterval] Intersects intervals.
\item[invariantsubspace\_old]%
\footnote{Copyright for algorithm \texttt{'perm'} and \texttt{'basis} by~\cite{Jung2014} under the 3-clause BSD License.}%
%
{\bfseries Deprecated} Old implementaiton of \texttt{invariantsubspace}.
\item[leadingeigenvector] Returns all leading eigenvectors of a matrix.
\item[makecandidate] Mostly a helper function for \texttt{ipa}
\item[makeorderinggraph] {\bfseries (buggy)} Constructs the graph corresponding to a partially ordered set.
\item[makepolytope] Constructs a polytope which approximates the unit ball of the set of matrices M
\item[makepositive] Multiplies arrays with a scalar such that its first non-zero entry is positive and real.
\item[matrixenum] Returns sets of integer matrices with given digit set. Replaced the function \verb|binarymatrix|.
\item[polytopenorm\_old]
{\bfseries Deprecated} 
Old implementation of \texttt{polytopenorm}.
\item[paritionatepolytope] Partitions points in $\RR^s$ into clusters of nearby points.
\item[preprocessmatrix]%
Simplifies sets of matrices while preserving its $\JSR$.
\item[problemtype] Mostly a helper function for \texttt{ipa}
\item[reducelength] Removes periodics and cycles vectors such that they have smallest lexicographic value.
\item[removecombination] Constructs a minimal set of cycles.
\item[tbuildproduct\_fast\footnote{Uses code from~\cite{Jung2014}. Copyright: 3-clause BSD License.}] Constructs a product of matrices, less overhead, less features
\item[ipa\_domain] {\bfseries Deprecated, \color{red} untested} Computes the jsr of a set of continuous set of matrices
\item[rhot] Computes the spectral radius of a matrix
\end{param}

\subsection*{Functions taken from others}
\begin{param}
\item[tcellDivide\footnoteref{footnote_jsrtoolbox}] Divides matrices in a cell.
\item[genNecklacest\footnoteref{footnote_jsrtoolbox}] Generation of all necklaces.
\item[tgraphSCC\footnoteref{footnote_jsrtoolbox}] Finds the strongly connected components of graph.
\item[jointTriangult\footnoteref{footnote_jsrtoolbox}] Searches for invariant subspaces of matrices.
\item[ipa\_zeroJsr\footnoteref{footnote_jsrtoolbox}] Decides if the JSR of a set of matrices is equal to zero.
\item[liftproductt\footnoteref{footnote_jsrtoolbox}] Computes all products of matrices of a given length.
\item[liftsemidefinitet\footnoteref{footnote_jsrtoolbox}] Computes semi-definite liftings of matrices.
\item[tpermtriangual\footnoteref{footnote_jsrtoolbox}] Searches for invariant subspaces of matrices.
\end{param}

\noindent
All functions with prefix \texttt{ipa\_} are subroutines of \texttt{ipa} and are not documented here, since they are subject to big changes, whenever the main function \texttt{ipa} is changed.


\section{\texttt{findsmp}}\label{findsmp}
This function searches for 
spectral maximizing and spectral minimizing products\footnote{s.min.p's are experimental}, 
in the following s.m.p.-candidates.
Certain algorithms are available.

\subsection*{Syntax}
\begin{param}
\item 
\texttt{[ cand, nearlycand, info ] = findsmp( T, [algorithm], ['smaxp'|'sminp'], [options] )}
\end{param}


\subsection*{Note}
\begin{itemize}
\item The Gripenberg type algorithms are parallelised, the genetic algorithm is not.
\item All Gripenberg type algorithms return true lower and upper bounds for the $\JSR$/$\LSR$.
\item There is a bug in the Genetic algorithm and the returned upper bound for the $\JSR$ is sometimes wrongly normalized.
\item See the \texttt{help} of \texttt{findsmp} for the options for the genetic algorithm.
\end{itemize}

\subsection*{Example Usage}
\begin{param}
\item[{\texttt{[ c, nc, info ] = findsmp( {[1 -1; 3 -2], [1 3; -1 -1]}, 'maxsmpdepth', 15 )}}]
\item[{\texttt{[ c, nc, info ] = findsmp( {[1 -1; 3 -2], [1 3; -1 -1]}, 'gripenberg' )}}]
\end{param}


\section{\texttt{gallery\_matrixset}}\label{gallery_matrix}
This function provides example-sets of matrices.
It is useful for testing algorithms and other purposes. 
It also makes use of Matlab's \texttt{gallery}.
\subsection*{Syntax}
\begin{param}
\item \texttt{[ val ] = gallery\_matrixset( what, dim, N, k, [options] )}
\end{param}


\subsection*{Example Usage}
\begin{param}
\item[{gallery\_matrixset('rand\_gauss',5,2,100,'rho')}]
\item[{gallery\_matrixset('mejstrik\_119')}]
\end{param}


\section{\texttt{invariantsubspace}}\label{invariantsubspace}
Searches for invariant subspaces of matrices $M\in\texttt{M}$, 
i.e.\ a change of basis $B$ such that all matrices $B^{-1}MB$, $M\in\texttt{M}$ have block-triangular form.
The function uses three different algorithms: \texttt{permTriangul}, \texttt{jointTriangul} from~\cite{Jung2014} and an implementation of~\cite{CP17}.
The returned matrices still may have invariant subspaces which can or cannot be found using this function.
\subsection*{Syntax}
\begin{param}
\item \texttt{[ Mret, B ] = invariantsubspace( M, ['type'], [options] )}
\end{param}


\subsection*{Example Usage}
\begin{param}
\item[{\texttt{[ M, B ] = invariantsubspace( \{[1 0 ; 1 2], [3 -1; -1 3]\}, 'basis', 'verbose', 2 )}}]
\end{param}


\section{\texttt{ipa}}\label{ipa}
Computes the $\JSR$ of a set of square-matrices \texttt{M}.
\subsection*{Syntax}
\begin{param}
\item \texttt{[ JSR, info, allinfo ] = ipa( M, [options] ) }
\end{param}


\subsection{Important options}
This is a list of the most important options (which should be sufficient for the standard-user). In a later section all available options are listed.
\begin{param}


\item['delta',val] double, \defval{1}\\Accuracy. For $\texttt{delta}<1$ the algorithm is faster, but returns only bounds for the $\JSR$.

\item['invariantsubspace',string] string, \defval{\texttt{'auto'}}\\  \texttt{string} is one of the following:
\texttt{'none'}, \texttt{'perm'}, \texttt{'basis'}, \texttt{'trans'}, \texttt{'auto'}. See the documentation of \nameref{invariantsubspace} for more information.

\item['balancing',val] \defval{0}\\Controls balancing, `-1` means auto.

\item['ordering',cell] cell array of matrices of column vectors, \defval{empty}\\ Orderings of s.m.p.-candidates.    

\item['plot',string] string, \defval{\texttt{'none'}}
    \begin{param}
    \item['norm'] Plots intermediate norms
    \item['polytope'] Plots the constructed polytope
    \item['L'] Plots the number of vertices left to compute (at the moment)
    \end{param}

\item['verbose',val] integer, \defval{1}\\Verbose level. If $\texttt{verbose}<0$ the algorithm suppresses error-messages (not recommended!).
\end{param}


\subsection{Example Usage}\label{ipa_example_usage}
The algorithm (in the optimal case) does not need to be called with any parameters, i.e. a call of the form \texttt{ipa(A)}, where \texttt{A} is the cell array of matrices whose $\JSR$ shall be computed, is sufficient.
Nevertheless, in some examples the algorithm does not work as expected and manual interaction is necessary.

{\bfseries \color{red}The examples in this section may be outdated, and the options may have other names.}

For our examples in this section we make use of the function~\nameref{gallery_matrixset} which returns example matrices. Some options used for these examples are described in detail only in the help text in the source code, type \texttt{help ipa>fullhelp} in Matlab to display it.
\begin{itemize} 

\item This example shows how to specify the cyclic-roots. 
We use for the example the set of matrices\\
\texttt{A=gallery\_matrixset('rand\_pm1',3,2,'seed',100)}, i.e.\ 
$\texttt{A}=\{\texttt{A}_1,\texttt{A}_2\}$,
\begin{equation*}
\texttt{A}_1=\left[\begin{array}{rrr}
 1& 1& 1\\
 0& 1&-1\\
 1& -1&-1\\
\end{array}\right],
\quad
\texttt{A}_2=\left[\begin{array}{rrr}
 0& 0& 1\\
-1&-1& 0\\
 1& 1&-1\\
\end{array}\right].
\end{equation*}


The set $\texttt{A}$ has an s.m.p.\ $A_1 A_2^8$, which can be computed with \texttt{ipa(A)}.

The command \texttt{ipa(A,'ordering',\{[2]'\})} starts the algorithm with a wrong s.m.p.. The algorithm finds better s.m.p.-candidates and restarts several times. Note that automatic extra-vertices are disabled, if we specify an ordering.

If we want to add extra-vertices we can do it in two ways.
\begin{param}
\item[{ipa(A,'extravertex',\{[.1 0 0]'\})}]~
\begin{itemize}
\item This command specifies only the extra-vertex. The s.m.p.-candidates and nearly-s.m.p.s are computed automatically.
\end{itemize}

\item[{ipa(A,'ordering',\{[1 2 2 2 2 2 2 2 2]', []'\},'smpflag',[0 2],'v0',...\newline
\{[0.058585928823279  -0.687551547968790   0.723768303968635]', [.1 0 0]'\})}]~\\
 This command specifies vectors of the cyclic-roots. The downside is, that one needs to give the exact eigenvalues of all s.m.p.-candidates and nearly-s.m.p.s.
\begin{itemize}
\item The option \texttt{'smpflag',[0 2]} specifies that we want two cyclic-roots. The first is the root corresponding to an s.m.p.-candidate (number \texttt{0}), the second root corresponds to an extra-vertex (number \texttt{2}). If we also want to specify a root for a nearly-s.m.p., we have to use the number \texttt{1}.

\item The option \texttt{'v0',\{[0.05859  -0.68755   0.72377]' [.1 0 0]'\}} specifies the eigenvectors/vectors used to start the cyclic-root. They must be given as a cell array of column vectors.

\item If one also wants to specify the dual-leading eigenvectors, this has to be done using the option \texttt{'v0s'}.

\item The option \texttt{'ordering',\{[1 2 2 2 2 2 2 2 2]', []'\}} specifies the orderings of the cyclic-roots.
The first is the ordering of the s.m.p.. 

Note that $(i)$ \emph{orderings of extra-vertices MUST be empty},
$(ii)$ \emph{orderings MUST be given as column vectors}, and $(iii)$ are written in reversed polish notation, i.e.\ the ordering \texttt{1 2 3} is the product $\texttt{A}_3\texttt{A}_2\texttt{A}_1$.

If there is more than one ordering corresponding to a vector \texttt{v0}, it must be given as a matrix. 
E.g.: \texttt{'ordering',\{[1 2 2 2; 1 2 2 0]'\}} means that 
$\texttt{A}_2^3\texttt{A}_1 \texttt{v0}_1=\texttt{A}_2^2\texttt{A}_1 \texttt{v0}_1=\texttt{v0}_1$.
\end{itemize}



\end{param}

\item This example presents balancing, automatic extra-vertices and approximate computation options.
We use for the example the set of Daubechies-matrices 
\texttt{D7=gallery\_matrixset('daub',7)}. 

\begin{param}

\item[{ipa(D7)}] Just starting the algorithm computes that this set has two s.m.p.s: $\texttt{D7}_1$ and $\texttt{D7}_2$. 

\item[{ipa(D7,'balancingvector',[1 1.02 .01 .01 .01 .01 .01])}] Uses the given balancing vector to balance the trees. This option is useful when one wants to prove the invariance of the invariant polytope by hand.

\item[{ipa(D7,'nobalancing')}] This command disables the balancing (and the algorithm applied to this example will not terminate). Using verbose level to 4, \texttt{ipa(D7,'nobalancing','verbose',4)}, we see that the third line of numbers does not stop to grow. This line corresponds to the number of added vertices to the third cyclic-tree.

\item[{ipa(D7,'autoextravertex',0)}] This command disables the automatic extra-vertices. Since the polytope for these matrices is very flat, the LP-program fails to compute the Minkowski-norm and reports all vertices to be outside (This can be seen by the fact, the the computed norms are always $\infty$).

\item[{ipa(D7,'nobalancing','delta',.99999)}] This command multiplies the matrices prior computing the cyclic-tree by $0.99999$. Thus the algorithm will terminate, although we did not balance the trees. Clearly, the returned value is not exact but an interval.

\item[{ipa(D7,'epspolytope',-.1)}] This command influences when a vertex is considered to be inside of the polytope. A negative value means, that even points which are outside are considered to be inside. The algorithm automatically increases the value of \texttt{epspolytope} whenever there are no vertices left which can get children until the value is bigger than \texttt{epslinprog}.

This option speeds up the computation of the invariant polytope at the beginning, but in total leads to much bigger polytopes and a slowdown of the algorithm. For approximate computation of the $\JSR$, the option \texttt{'delta'} is preferable.
\end{param}

\item This example presents invariant subspace options. We use for the example the random boolean matrices,
\texttt{B=gallery\_matrixset('rand\_bool',4,2,43)}.
\begin{param}
\item[ipa(B)] finds two invariant subspaces. 
\item[ipa(B,'invariantsubspace','none')] disables the search for invariant subspaces. In some cases, the search for invariant subspaces may take a long time, especially when the number of matrices is big or the dimension is high.
\end{param}

\item This example presents some plotting options.
We use for the example a random set of 10 non-negative matrices with spectral radius 1 and dimension 20,\\
\texttt{T=gallery\_matrixset('rand\_gauss',6,2,'rho','seed',100)}.


\begin{param}
\item[ipa(T,'plot','norm')] Plots the computed norms of vertices and colours them according whether they have children or not.\\
It is also possible to plot the norms using the string \texttt{'info\_norm'}, but then the plot does not look as interesting.

\item[ipa(T,'plot','L')] Plots the number of added vertices and the number of remaining vertices to compute. The graph usually has the shape of a Gaussian.

\item[ipa(T,'plot','tree')] Plots the graphs of the cyclic trees.\\
To change the labels of the vertices, change the code in \texttt{ipa\_plotoutput}.


\item[ipa(T,'plot','polytope')] Plots the polytope/cone. If the dimension is higher than 3, a random subset of directions is chosen to be plotted in each iteration.

\item[ipa(T,'plot','info\_normest')] Plots the estimated Minkowski-norms.\\
The prefix \texttt{'info\_'} allows to plot any data contained in the \texttt{info}-struct. Fields in the sub-struct \texttt{cyclictree} can be addressed directly. All others need to be called with their full name. Thus the option \texttt{'plot','info\_normest'} is equivalent to\\
\texttt{'plot','info\_cyclictree.normest'}

\item[ipa(T,'plot','info\_normest\_norm','fastnorm',0)] Plots the real norms against the estimated Min\-kow\-ski-norms. We have to add the option \texttt{'fastnorm',0} since otherwise the Minkowski norms of some points would not be computed.\\
If one wants to plot more data from the \texttt{info}-struct, the variables to be plotted  have to be separated with an underscore~\texttt{\_}.

\item[ipa(T,'plot','info\_normest\_norm\_rho','fastnorm',0)] Plots the estimated norms against the real norms against the spectral radii.

\item[ipa(T,'plot','info\_normest\_L')] Plots the estimated norms and the number of processed vertices.
If the variables are not compatible in size or format, the algorithm tries to plot them anyhow.



\end{param}

\end{itemize}



%%%%%%%%%%%%%%%%%%%
% EOF
%%%%%%%%%%%%%%%%%%%