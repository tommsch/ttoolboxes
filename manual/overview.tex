% !TeX encoding = UTF-8 Unicode
% !TeX root = manual.tex
% !TeX spellcheck = en_GB


\chapter{Overview over the included toolboxes}
This is the documentation for the most important functions of the toolboxes
\emph{cell}, \emph{double}, \emph{m}, \emph{sequence}, \emph{subdivision}, \emph{ipa}, \emph{tmisc} and \emph{tsym} (summarized as the \emph{ttoolboxes}).
The documentation for all functions (including those in this manual) can be read with the Matlab-command 
\texttt{help name}, where \texttt{name} is the name of a function, e.g.\ \texttt{help ipa}. 

The \emph{ttoolboxes} can be downloaded from 
\href{https://gitlab.com/tommsch/ttoolboxes}{\emph{gitlab.com/tommsch/ttoolboxes}}.

\section{Naming/calling conventions and data-format}
\begin{itemize}
\item Most functions expect vectors in column format, contrary to Matlab's default. I.e. if you want the two column vectors 
$\left[\begin{array}{c c c}1&2&3\end{array}\right]^T$,
$\left[\begin{array}{c c c}7&8&9\end{array}\right]^T$
written in one matrix, all of the toolboxes`s function`s expect them as%
\footnote{Internally most of the functions also use this data-format. 
Since sparse arrays in Matlab do not work well with this data-format, 
in a future release the function \texttt{ipa} may change its data-format 
to Matlab's default.}
\begin{equation*}
\left[\begin{array}{c c c}1&7\\2&8\\3&9\end{array}\right].
\end{equation*}


\item Most of the functions use \emph{name-value} pairs to pass options, e.g. \texttt{ipa(...,'plot','norm')}, where \texttt{'plot'} is a \emph{name} and \texttt{'norm'} is a \emph{value}.
Some names expect no value, in which case the value can be omitted or given. 
If used without a value or with the value $1$ the option is enabled, if used with the value $0$, the option is disabled. 
Other arguments behind options which do not expect a value lead to undefined behaviour.

\item Optional arguments are written inside of square brackets in this documentation, with the exception when they are \emph{Options}, i.e.\ all options are optional.

\item All function-names/names/values/etc.\ are singular and written in lower-case, with the only exception when a corresponding mathematical symbol uses an upper-case letter.

\item Since Matlab (nearly) has no types for variables, we denote parameters which shall be whole numbers with the type \texttt{integer}, even if they are \texttt{double}s in reality.

\item The variables \texttt{idx} and \texttt{val} in the source-code are used only locally. They are only valid for some lines of code. They are re-used, since Matlab has no scope for variables.

\item The letters $\texttt{XX}$ in the source-code, indicates things which should be changed.
\end{itemize}

\section{Test-drivers}
Apart from the described example-usages in this documentation and in the \text{help} of each function, 
one can run the test suite by calling
\texttt{runttests} from the folder \texttt{/unittest}
which test every function included in the toolboxes. 
The test suite succeeded on the following architectures 
(i.e.\ the \emph{ttoolboxes} are expected to run on the following architectures):

For version \emph{v2.2024.09.02}, the test suite succeeded on the architectures
\begin{itemize}
\item
AMD Ryzen 5 3600, 32GB RAM,\\
Windows 10 LTSC, version 21H2\\
Matlab R2023b\\
with and without Gurobi v10.0.2
\end{itemize}

For version \emph{10 September 2020}, the test suite succeeded on the architectures
\begin{itemize}
\item
AMD Ryzen 5 3600, 32GB RAM,\\
Windows 10 LTSC, version 21H2\\
Matlab R2023b\\
with and without Gurobi v10.0.2
\end{itemize}

For version \emph{10 September 2020}, the test suite succeeded on the architectures
\begin{itemize}
\item 
AMD Ryzen 5 3600, 32GB RAM,\\
Windows 10 LTSC, version 1809\\
Matlab R2018a\\
Gurobi v8.1.1

\item 
Intel Core i7-8650U@1.90GHz\\
Linux 4.15.0-99, Ubuntu 18.04\\
Matlab R2019a\\
without Gurobi

\item Intel Core i5-4670S@3.8GHz, 8GB RAM\\
Linux 4.15.0-38,
Ubuntu 16.04.5 LTS\\
Matlab R2017a\\
with and without Gurobi v8.0.1

\item Intel Xeon IvyBridge-Ep E5-2650v2@2.6GHz, 64GB RAM\\
Linux 3.10.0, CentOS Linux 7\\
Matlab R2016b/R2017b/R2018b\\
with and without Gurobi v7.5.1/Gurobi v8.0.1

\item 
Intel Core i5-760@2.8GHz, 8GB RAM,\\
Windows 7 SP1\\
Matlab R2017a/R2018a\\
with and without Gurobi v8.0.1

\item
Intel Core i7@2.5GHz, 16GB RAM\\
OSX 10.10.5 (Yosemite), macOS 10.14.1 (Mojave)\\
Matlab R2017b\\
Gurobi v8.0.1

\end{itemize}

The toolboxes do not run on Matlab versions before and including R2015b.

\section{Summary of toolboxes}
\begin{itemize}
\item \texttt{cell}-toolbox: Functions for scalar operations on cell arrays. 
\item \texttt{experimental}: Experimental stuff
\item \texttt{ipa}-toolbox: Functions to compute the $\JSR$
\item \texttt{m}-toolbox: Functions which generalize Matlab functions to $n$-arrays for arbitrary $n\in\NN_0$, plus some \emph{very} useful functions
\item \texttt{math}: Math functions
\item \texttt{sequence}-toolbox: Class for tensor valued, finitely supported (up to a constant) sequences in $\ZZ^s$
\item \texttt{subdivision}-toolbox: Functions for the work with multiple subdivision schemes
\item \texttt{tmisc}-toolbox: Very useful functions and barely useful functions
\item \texttt{tsym}-toolbox: Functions which Matlab did not implement for \texttt{double}s but did implement for \texttt{sym}
\item \texttt{utility}: Utility functions with no connection to mathematics
\end{itemize}

\section{Summary of included third party software}
\begin{itemize}
\item \texttt{SeDuMi} Optimization over symmetric cones. Only works on Windows.
\item \texttt{The JSR toolbox} Gathers and compares the methods for the joint spectral radius computation.
\item \texttt{feta} Algorithm for computing the joint spectral radius using the \emph{finite expressible tree algorithm}\cite{MR2014}.
\item \texttt{misc} Some functions which do not belong a full framework.
\item \texttt{TTEST} Only some helper files from the \verb|TTEST| framework.
\end{itemize}

\section{Documentation of functions}
{\color{red}
This manual only gives an overview of (nearly) all included functions. 
Functions not mentioned here, but present in the \emph{ttoolboxes} are 
implicitly either experimental or deprecated,
which means they may change or even get removed in a future release.

{\bfseries The most up to date documentation of the functions is included in the help texts of the source files.}
}


%%%%%%%%%%%%%%%%%%%
% EOF
%%%%%%%%%%%%%%%%%%%
