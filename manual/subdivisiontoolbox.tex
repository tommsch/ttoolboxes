% !TeX encoding = UTF-8 Unicode
% !TeX root = manual.tex
% !TeX spellcheck = en_GB


\chapter{\texttt{subdivision}-toolbox}

This toolbox implements functions for the work with multiple subdivision schemes. 
\section{Overview}
\subsection*{Dependencies}
The toolbox depends on the \texttt{m}-, \texttt{tmisc}- and \texttt{sequence}-toolbox.
The toolbox furthermore depends on the Matlab
\texttt{Parallel Toolbox} and \texttt{Symbolic Math Toolbox}.
Furthermore it uses functions from the Matlab \texttt{Signal Processing Toolbox} but also runs without the latter.

\subsection*{Important functions}
\begin{param}
\item[blf] Plots the basic limit function of a multiple subdivision scheme.
\item[constructOmega] Constructs the set $\Omega_C$ of a multiple subdivision scheme~\cite{CM18}.    
\item[constructV] Constructs a basis for the space ${V}_k(\Omega)$~\cite{CM18}.
\item[constructVt] Constructs a basis for the space $\tilde{V}_k(\Omega)$~\cite{CM18}.
\item[getS] Returns subdivision operators in this toolboxs format.
\item[restrictmatrix] Restricts matrices to a subspace.
\item[tile] Plots the attractor of a multiple subdivision scheme.
\item[transitionmatrix] Constructs transition matrices.
\end{param}

\subsection{Remaining functions}
\begin{param}
\item[characteristic] Returns the characteristic function of an index set.
\item[checktile] {\bfseries\color{red} buggy!} Computes if a pair dilation-matrix/digit-set generates a tile (i.e. an attractor with area 1).
\item[compresscoordinates] Returns an array representing the graph of a function.
\item[constructdigit] Constructs the usual digit set $M[0,1)^s\cap \ZZ^s$. 
\item[constructordering] Constructs the data-type \texttt{ordering}.
\item[constructU] Constructs a basis for the space $U$~\cite{CP17}.
\item[daubechiesmask] Returns the mask coefficients for Daubechies' wavelet.
\item[differencescheme] {\bfseries\color{red} buggy!} Computes the masks of difference schemes.
\item[dimVVt] Computes the dimension of the spaces $V_k(\Omega)$ and $\tilde{V}_k(\Omega)$~\cite{CM18}.
\item[findperiod] Searches for periodics in sequences.
\item[isodering] Determines if input is {ordering}.
\item[isS] Determines if input are {subdivision operator}s.
\item[isT] Determines if input are transition matrices.
\item[multiplyS] Concatenates subdivision operators.
%\item[mask2symbol] Computes the symbol of a mask.
\item[normalizeS] Normalizes the values of a mask.
\item[num2ordering] Computes number expansions for multiple multivariate number systems.
\item[ordering2double] Takes a vector of numbers an returns an integer having this number representation in backwards ordering.
\item[ordering2num] Computes numbers corresponding for multivariate multiple number systems.
\item[ordering2vector] Converts an ordering to a vector of certain length.
\item[peter] {\bfseries Deprecated} Removes randomly columns of arrays.
\item[supp] Computes the support of a mask.
\item[symbol2mask] Computes the masks from given symbols.
\item[ssregularity] {\bfseries \color{red} untested} Computes the convergence/Hoelder regularity of a subdivision scheme
\item[sumrule] {\bfseries \color{red} untested} Checks whether a subdivision schemes satisfies sum rules
\item[tilearea] Tests heuristically if an attractor is a tile (for dimension $1$ and $2$).
\item[vector2ordering] Wrapper function for \texttt{findperiod}.
\end{param}


\section{\texttt{getS}}
This function returns a finite set of \emph{subdivision operators}. This is a cell array, each row describing one subdivision operator. Each row has the entries

\subsection*{Syntax}
\begin{param}
\item[{[ S ] = getS( dim || cellarray || name || list, [options] )}]
\end{param}


\subsection*{Example Usage}
\begin{param}
\item[getS(2)] Returns all unnamed subdivision operators of dimension $2$. The returned set may be empty, if there are no unnamed subdivision operators.
\item[getS('2\_butterfly')] Returns the butterfly scheme.
\item[getS('1\_all')] Returns all named subdivision operator of dimension 1.
\item[{getS('a',[.25 .5 .25;.5 1 .5;.25 .5 .25],'M',[2 0; 0 2],'n','tensorlinear');}] The first bivariate tensor-product B-spline.
\end{param}


\section{\texttt{blf}}
This function plots the basic limit function of multiple subdivision schemes.
    
\subsection*{Syntax}
\begin{param}
\item[{[ c, PM, xyzv, oo ] = blf( [oo], S, [options] )}]
\end{param}


\section{\texttt{tile}}
Plots the attractor corresponding to a multiple subdivision scheme. I.e.\ for given \texttt{S} and \texttt{oo}, the set defined by
\begin{equation*}
\texttt{M}_{\texttt{oo}_1}^{-1}\texttt{D}_{\texttt{oo}_1}+
\texttt{M}_{\texttt{oo}_1}^{-1}\texttt{M}_{\texttt{oo}_{2}}^{-1}\texttt{D}_{\texttt{oo}_2}+
\texttt{M}_{\texttt{oo}_1}^{-1}\texttt{M}_{\texttt{oo}_{2}}^{-1}\texttt{M}_{\texttt{oo}_{3}}^{-1}\texttt{D}_{\texttt{oo}_3}+
\cdots.
\end{equation*}

\subsection*{Syntax}
\begin{param}
\item[{[ Q, oo ] = tile( [oo], S, [options] )}]
\end{param}

\subsection*{Input}
\begin{param}
\item[{[oo]}] ordering, optional\\
The ordering in which the subdivision operators are applied
\item[S] subdivision operators, mandatory\\
The subdivision operators
\end{param}

\subsection*{Options}
\begin{param}
\item['digit']                    \defval{false}\\Computes iterated digit sets instead of the attractor.

\item['iteration',val]             integer, \defval{depends on \texttt{S}}\\
Computation stops after \texttt{iteration} many iterations.

\item['maxiteration',val]          integer, \defval{50}\\Maximum number of iterations.

\item['numpoint',val]              integer, \defval{30000}\\Number of points in the output-sequence to be computed.

\item['plot',cellarray]            cell array or scalar, \defval{\texttt{\{\}}}\\
Arguments passed to \texttt{plotm}, e.g.\ \texttt{\{'Color','red'\}}.
If $\texttt{plot}=0$, nothing is plotted.

\item['round',val]                integer or $1\times 2$-vector, \defval{\texttt{1e-2, 1e-12}}\\
If \texttt{round} is an integer, the same round value is used in each iteration.
If \texttt{round} is a vector, the round values are linearly interpolated.

\item['start',array]              $dim$-array, \defval{$\delta_0$}\\The starting set.

\item['supertile',n]              integer\\Computes the supertile $K=\cup_{j}(M_j^{-1}K+D_j)$ instead of the attractor. 

\item['verbose',val]              integer, \defval{1}\\Verbose level.

\end{param}

\subsection*{Output}
\begin{param}
\item[Q] $dim\times N$ matrix\\The computed attractor.
\item[oo] vector\\Ordering used. Equals \texttt{oo} (Input) if given.
\end{param}
    
\subsection*{Example Usage}
\begin{param}
\item[{tile('2\_frayed\_squares','round',[.1 1e-2])}]
\item[{tile('1\_cantor','digit')}]
\item[{tile([getS('2\_rand'); getS('2\_rand')],'supertile','iteration',10)}]   
\end{param} 

\section{\texttt{constructOmega}}
Constructs the set $\Omega_C$ as described in~\cite{CM18}.

\subsection*{Syntax}
\begin{param}
    \item[{[ Om ] = constructOmega( S, [options] )}]
\end{param}

\subsection*{Example Usage}
\begin{param}
    \item[{constructOmega('2\_butterfly','Omega',[2;2],'stable')}]
\end{param}   


\section{\texttt{constructVt}}
Constructs a basis for the space $\tilde{V}_k(\Omega)$, as described in~\cite{CM18}.

\subsection*{Syntax}
\begin{param}
\item[{[ V, Om, Xmuf ] = constructVt( Om, [k] , [options])}]
\end{param}


\subsection*{Note}
\begin{itemize}
    \item The functions \texttt{constructV} and \texttt{constructU} construct the spaces ${V}_k(\Omega)$ and $U_k(S)$ as described in~\cite{CM18}. They have nearly the same interface as \texttt{constructVt}, so they are not described in this manual. See the \texttt{help} of these functions for more informations.
    
    \item If one wants to use the matrix-approach for the characterization of convergence of subdivision schemes, the set $\Omega$ has to fulfil two assumptions. $(i)$ $\dim \tilde{V}_k(\Omega)=\dim V_k(\Omega)$, $(ii)$ $X_\mu(\Omega)$ is non-empty for all $|\mu|=k+1$. The function \texttt{dimVVt} may be used to check these two conditions.    
    
\end{itemize}

\subsection*{Example Usage}
\begin{param}
    \item[{Om = constructOmega( '2\_butterfly' ); constructV( Om, 1 )}]
\end{param}


\section{\texttt{restrictmatrix}}
Restricts matrices to a subspace and checks whether the subspace is invariant or not, 
i.e.\ with the notation from below, the function computes 
\begin{equation}
\texttt{TT}=
\left[\begin{array}{c c}\texttt{TA}&\ast\\\texttt{NULL}&\texttt{TR}\end{array}\right]=
\texttt{BASIS}^{-1}\cdot\texttt{T}\cdot\texttt{BASIS}.
\end{equation}
\subsection*{Syntax}
\begin{param}
\item[{[ TA, TT, TR, NULL, BASIS ] = restrictmatrix( T, A, [options] )}]
\end{param}

\subsection*{Example Usage}
\begin{param}
\item[{restrictmatrix([1 1 0; 0 1 1; 1 0 1],[1 -1 0; 0 1 -1]')}]
\end{param}

\section{\texttt{transitionmatrix}}
Constructs transition matrices 
$T_{d, \Omega}=(\texttt{a}(\alpha-\texttt{M}\beta+d))_{\alpha, \beta\in\Omega}$ 
where 
$\texttt{a}\in\texttt{S}$ are subdivision masks, 
$M\in\texttt{S}$ are dilation matrices, 
$d\in\texttt{D}\in\texttt{S}$, $D\simeq\ZZ^s/\texttt{M}\ZZ^s$, are digit sets and $\Omega\subseteq\ZZ^s$ is an invariant set for these matrices~\cite{CM18}.
\subsection*{Syntax}
\begin{param}
\item[{[ T, Om, Vt ] = transitionmatrix( S, [options] )}]
\end{param}


\subsection*{Example Usage}
\begin{param}
\item[{vdisp( transitionmatrix( {1/4*[1 4 3]', 2} ) )}]
\end{param}

\section{Example showing how to use the \texttt{subdivision}-toolbox}

\begin{example}\label{ex_subdivision}
This example computes the Hölder regularity of the stationary subdivision scheme given by the subdivision operator 
$S=(a,M)$, 
with mask $a=\frac{1}{4}\left[\begin{array}{cccc}1 & 1& 3&3\end{array}\right]$ 
and dilation $M=2$.
We first generate the cell array of subdivision operators
\begin{verbatim}
S = getS( 'a',1/4*[1;1;3;3], 'M',2 );
   %There is a bug in some Matlab versions which throws an error for the command disp(S). 
   %Thus the semi-colon is important.
\end{verbatim}
To plot the basic limit function we call
\begin{verbatim}
blf( S );
\end{verbatim}
We next construct the transition matrices and restrict them to the invariant subspace $V_0$. 
We also construct the space $\tilde{V}_0$. To apply the joint spectral radius approach to compute the regularity of subdivision-schemes, the dimension of $V_0(\Omega)$ and $\tilde{V}_0(\Omega)$ must coincide.
\begin{verbatim}
[ T, Om ] = transitionmatrix( S );  % compute transition matrices and the set Omega
plotm( Om, 'x' )                    % plot the set Omega
V0 = constructV( Om, 0 )            % Construct space orthogonal to polynomial sequences
V0t = constructVt( Om, 0 );         % Construct space of differences of delta
assert( size(V0,2)==size(V0t,2) );  % Test ifV0==V0bar
TV0 = restrictmatrix( T, V0 ); 
vdisp( TV0 );  % restrict transition matrices and display them
\end{verbatim}
The last step is to compute the Hölder regularity using the modified invariant polytope algorithm.
\begin{verbatim}
[JSR, type] = ipa( TV0 );  
al = -log( JSR )/log( 2 )  % Hoelder regularity. The 2 comes from the dilation M=2.
\end{verbatim}

\end{example}

%%%%%%%%%%%%%%%%%%%
% EOF
%%%%%%%%%%%%%%%%%%%