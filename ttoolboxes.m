function [ varargout ] = ttoolboxes( varargin );
% [ output ] = ttoolboxes( [option] )
% Install the ttoolboxes
%
% Options:
%       'base'              returns the folder where ttoolboxes are installed
%       'demofolder'        switches to demo folder (may work only when ttoolboxes are already installed)
%       'install'           installs ttoolboxes
%       'path'              adds all toolboxes to the Matlab path, without asking, and saves the Matlab path
%       'thanks'            Print out acknowledgements
%       'uninstall'         Removes ttoolboxes from path
%       'ver'               returns the version of the ttoolboxes as a string
%
% Output: Depends on the input
%
% Written by: tommsch, 2019

%               2021-10-12, tommsch: changed meaning of testflag, now default is: no tests
%               2021-10-12, tommsch: changed default value of path
%               2022-09-09, tommsch: Added full SeDuMi support for Matlab and Octave - but may not work on Linux
%               2022-03-10, tommsch: Behaviour change of option 'path'
%               2022-03-30, tommsch: Added option: thanks
%               2022-05-13, tommsch: Removed legacy types CONEFUNCT, MINKFUNCT, etc...
%               2023-03-30, tommsch: Removed function `estimatepolytopenorm`
%               2023-05-31, tommsch: Major release number change. Renamed function `tjsr` to `ipa`
%               2023-06-09, tommsch: Added toolboxes: `utility`,`math`
%                                    added functions: `scale_screen`, 'figure_lazy', 'subplot_lazy', 'repr'
%                                    added experimental toolbox: `optperron`, with `gallery_mp`, `gallery_graph`, ...
%               2024-02-28, tommsch: Added option 'demofolder'
%               2024-08-01, tommsch, Added option 'base'
%                                    removed option 'test'/testflag (was actually already removed before)
%               2024-10-10, tommsch, Moved is* functions to new toolbox `is`
%               2024-10-23, tommsch, New toolbox: `latex`
% Changelog:    

    if( nargin == 0 );
        help ttoolboxes;
        return; end;
    
    [ opt ] = parse_input( varargin{:} );

    if( opt.install );
        ttoolboxes_install( opt ); end;

    if( opt.thanks );
        ttoolboxes_thanks(); end;

    if( opt.base );
        d = ttoolboxes_base();
        if( nargout == 0 );
            fprintf( 'ttoolboxes base dir: %s\n', d );
        else;
            varargout{1} = d; end; end;

    if( opt.ver );
        ver = ttoolboxes_ver();
        if( nargout == 0 );
            fprintf( 'ttoolboxes version: %s\n', ver );
        else;
            varargout{1} = ver; end; end;

    if( opt.demofolder );
        ttoolboxes_demofolder(); end;

end

function ttoolboxes_thanks();
    fprintf( [
        'Thanks to:\n' ...
        '    Maria Charina,' ...
        '    Raphael Jungers,' ...
        '    Peter Laskawiec,' ...
        '    Vladimir Yu. Protasov,' ...
        '    Aaron Pumm,' ...
        '    Ulrich Reif\n' ...
        ]);
end


function [ directory ] = ttoolboxes_base();
    fname = mfilename( 'fullpath' );
    directory = fileparts( fname );
end

function [ ver ] = ttoolboxes_ver();
    ver = '2.2025.01.28';
end

function ttoolboxes_demofolder();
    fn = which( 'ttoolboxes' );
    d = fileparts( fn );
    cd( d );
    cd( 'demo' );
end

function [ opt ] = parse_input( varargin );

    % parse input
    assert( numel(varargin) >= 1, 'ttoolboxes:option', 'Possible options: ''path'', ''ver'', ''install'', ''uninstall'', ''thanks''.' );

    [opt.thanks, varargin] = parse_keyword( {'thank','thanks','thx'}, varargin );
    [opt.path, varargin] = parse_keyword( {'path'}, varargin );
    [opt.experimental, varargin] = parse_keyword( {'experimental'}, varargin );
    if( opt.experimental );
        warning( 'ttoolboxes:experimental', 'The option ''experimental'' is removed. Experimental stuff is always installed now. Removed this option from your command line.' ); end;
    [opt.install, varargin] = parse_keyword( {'install'}, varargin );
    [opt.uninstall, varargin] = parse_keyword( {'uninstall','remove'}, varargin );
    if( opt.uninstall );
        opt.path = -1; end;
    if( opt.path ~= 0 );
        opt.install = true; end;

    [opt.base, varargin] = parse_keyword( {'base','basepath','installdir','dir','basedir','installpath'}, varargin );
    [opt.ver, varargin] = parse_keyword( {'version','ver'}, varargin );
    [opt.demofolder, varargin] = parse_keyword( {'demofolder','ver'}, varargin );

    assert( isempty(varargin), 'ttoolboxes:input', 'ttoolboxes: Could not parse input.' );
end

function [ found, varargin ] = parse_keyword( keywords, varargin );
    if( ~iscell(keywords) );
        keywords = {keywords}; end;
    varargin = varargin{1};
    idx = false( size(varargin) );
    for kw_ = keywords; kw = kw_{1};
        idx = idx | strcmpi( varargin, kw );
        idx = idx | strcmpi( varargin, [ '-' kw] );
        idx = idx | strcmpi( varargin, [ '--' kw] );
        end;
    varargin = varargin(~idx);
    found = any( idx );

end

function ttoolboxes_install( opt );

    fprintf( ['=====================================================================================\n', ...
              'ttoolboxes version %s\n', ...
              '=====================================================================================\n'], ttoolboxes_ver() );

    % pre-processing
    %%%%%%%%%%%%%%%%
    olddir = pwd;
    cleandir = onCleanup( @() cd(olddir) );
    if( exist(fullfile(pwd, 'ttoolboxes'), 'file')~=2 );
        cd( fileparts(which('ttoolboxes')) ); end;

    % set path
    %%%%%%%%%%%%%%%%
    if( opt.path >= 0 ); path2 = 1; else; path2 = opt.path; end;  % Is necessary since `ttoolboxes.m` is on the path when this function is executed, and thus the root folder of ttoolboxes would not get added to the Matlab path
    % ttoolboxes stuff
    writepath( 'ttoolboxes.m',          'ttoolboxes',                   path2,    true,                         '' );  % Here `path2` was used instead of `opt.path`, but I do not understand why
    writepath( '@cell/ldivide.m',       'cell',                         opt.path, true,                         'ttoolboxes/cell' );
    writepath( '@cell/ldivide.m',       'cell',                         opt.path, true,                         'ttoolboxes/cell' );
    writepath( 'cart2sphm.m',           'm',                            opt.path, true,                         'ttoolboxes/m' );
    writepath( 'totient.m',             'math',                         opt.path, true,                         'ttoolboxes/math' );
    writepath( '@sequence/sequence.m',  'sequence',                     opt.path, true,                         'ttoolboxes/sequence' );
    writepath( 'transitionmatrix.m',    'subdivision',                  opt.path, true,                         'ttoolboxes/subdivision' );
    writepath( 'isanyof.m',             'is',                           opt.path, true,                         {'ttoolboxes/is'} );
    writepath( 'findsmp.m',             'ipa',                          opt.path, true,                         'ttoolboxes/ipa' );
    writepath( 'latexprod.m',           'latex',                        opt.path, true,                         'ttoolboxes/latex' );
    writepath( 'matrixinfo.m',          'tmisc',                        opt.path, true,                         'ttoolboxes/tmisc' );
    writepath( 'symmin.m',              'tsym',                         opt.path, true,                         'ttoolboxes/tsym' );
    writepath( 'scale_screen.m',        'utility',                      opt.path, true,                         'ttoolboxes/utility' );
    writepath( 'experimental_dummy.m',  'Experimental stuff',           opt.path, true,                         'ttoolboxes/experimental' );
    writepath( 'statematrix.m',         'Experimental filter',          opt.path, true,                         'ttoolboxes/experimental/filter' );
    writepath( 'iskone.m',              'Experimental kone',            opt.path, true,                         'ttoolboxes/experimental/kone' );
    writepath( 'gallery_mp.m',          'Experimental optperron',       opt.path, true,                         'ttoolboxes/experimental/perron' );
    writepath( 'gallery_signal.m',      'Experimental signal',          opt.path, true,                         'ttoolboxes/experimental/signal' );

    % thirdparty stuff
    writepath( 'RunLength_M.m',         'Misc. from third parties',     opt.path, true,                         {'thirdparty/misc','thirdparty'} );
    writepath( 'sedumi_linprog.m',      'thirdparty: sedumi',           opt.path, ismatlab,                     {'thirdparty/sedumi','thirdparty/sedumi/matlab','thirdparty/sedumi/matlab/conversion','thirdparty/sedumi/matlab/o_win'} );
    writepath( 'sedumi_linprog.m',      'thirdparty: sedumi',           opt.path, ~ismatlab,                    {'thirdparty/sedumi','thirdparty/sedumi/octave'} );
    writepath( 'gurobi_linprog.m',      'thirdparty: Gurobi Wrapper',   opt.path, true,                         {'thirdparty/gurobi'} );
    writepath( 'jsr_pathcomplete.m',    'thirdparty: JSR Louvain',      opt.path, true,                         {'thirdparty/JSR_louvain/Methods','thirdparty/JSR_louvain','thirdparty/JSR_louvain/Benchmark/','thirdparty/JSR_louvain/Pre-processing','thirdparty/JSR_louvain/Subroutines'} );
    writepath( 'feta.m',                'thirdparty: Feta algorithm',   opt.path, true,                         {'thirdparty/feta'} );
    writepath( 'tifh.m',                'thirdparty: TTEST',            opt.path, true,                         {'thirdparty/TTEST'} );

    % shims
    writepath( 'pdist2.m',              'shim: Stastistic Toolbox',     opt.path, ~check_toolbox('statistic'),                  {'shim/statistic'} );
    writepath( 'upsample.m',            'shim: Signal Toolbox',         opt.path, ~check_toolbox('signal'),                     {'shim/signal'} );
    writepath( '@named/named.m',        'shim: named class',            opt.path, ~ismatlab || verLessThan('matlab', '9.11'),   {'shim/oldversion'} );  %#ok<VERLESSMATLAB>
    writepath( '@named/named.m',        'shim: named class',            opt.path, ismatlab && ~verLessThan('matlab', '9.11'),   {'shim/R2021b'} );  %#ok<VERLESSMATLAB>

    
    postprocess()

    savepath_t();
    fprintf( '\nNew Matlab path definitions saved.\n');

end

function postprocess();
    if( ~ismatlab );
        currentdir = pwd;
        cleandir = onCleanup( @() pwd(currentdir) );
        cd( 'thirdparty' ); cd( 'sedumi' ); cd( 'octave' );
        try;
            install_sedumi();
        catch me;
            fprintf( 2, 'Error: SeDuMi could not be installed. Thrown Error:\n' );
            disp( me );
            disp( me.stack(1) );
            end; end;
end

function writepath( file, name, pathflag, doflag, relpath );

% Input:
%   file        string - name of file, is used to checked whether the toolbox is already installed
%   relpath     string OR cell array of strings, relative paths which shall be added to the path
%   name        string, name of the toolbox as it is displayed by this function
%   doflag      bool, if true, nothing happens
%   pathflag    integer, -1 toolbox is removed from the path
%                         0 only toolboxes which seem not be installed are added to path
%                         1 all toolboxes are added to path, and put at the beginning
%                       
    if( ~doflag );
        return; end;
    
    if( ~iscell(relpath) );
        relpath = {relpath}; end;

    if( ~isempty(file) && ~iscell(file) );
        file = {file}; end;
    
    if( ~isempty(file) );
        file_exist = cellfun( @(x) exist(x, 'file') == 2, file ); end;
    if( pathflag >= 1 || pathflag <= -1 );
        % remove from path
        for i = 1:numel( relpath );
            p = fullfile( pwd, relpath{i} );
            evalc( 'rmpath( p );' );  % This functions throws warnings without id on octave
            if( pathflag <= -1 );
                fprintf( 'Removed from matlab-path: %s\n', p ); end; end; end;
    if( pathflag >= 0 && ~isempty( file ) && ~all( file_exist ) || ...
        pathflag >= 0 && isempty( file ) || ...
        pathflag >= 1 ...
      );
        % add to path
        if( isempty( file ) || ...
            isequal( exist(fullfile(relpath{1}, file{1}), 'file'), 2 ) ...
          );
            for i = 1:numel( relpath );
                p = fullfile( pwd, relpath{i} );
                evalc( 'addpath( p );' );  % This functions throws warnings without id on octave
                fprintf( 'Added to matlab-path: %s\n', p ); end;
        else;
            fprintf( 2, 'ERROR: Missing files. Toolbox %s not added to path.\n', name ); end;
    else;
        %fprintf( 'toolbox already installed: %s\n', name );
        end;

end

%%

function savepath_t();
    persistent warning_shown;
    
    if( ~ismatlab );
        try;
            ret = savepath();  %#ok<MCSVP>
            if( ret == 0 );
                raise_error_and_goto_catch;
            else;
                fprintf( 'New path definition is saved.\n' ); end;
        catch me;  %#ok<NASGU>
            fprintf( 2, 'Saving the new path definitions failed.' ); end;
    else;
        olddir = pwd;
        cleardir = onCleanup( @() cd(olddir) );
        try;
            if( isunix && ~ismac );
                cd( '~' );
            elseif( ismac );
                cd( '$home' );
            elseif( ispc );
                [~, res] = system( 'echo  %USERPROFILE%' );
                res = strtrim( res );
                cd( res );
            else;
                fatal_error; end;
            fprintf( 'Working directory temporarily changed to users home directory.\n' ); 
        catch me;  %#ok<NASGU>
            end;
            
        if( numel(which( 'pathdef.m', '-all' )) > 1 && isempty(warning_shown) );
            warning_shown = true;
            fprintf( 2, ['Multiple ''pathdef.m'' files are found.\n'...
                         '  This means, that the path may be saved in the wrong file, and after a new start of Matlab,\n' ...
                         '  the ''ttoolboxes'' may not be useable without installing it again.\n' ...
                         '  Sorry for the inconvenience, but this is a problem raised by Matlab/Your system administrator.\n'] ); end;
        pd = which( 'pathdef.m' );
        [fid, errmsg] = fopen( pd, 'w' );  % check if we have write access to pathdef.m
        if( ~isempty(errmsg) && strcmp(errmsg, 'Permission denied') || ...
            fid == -1 ...
          );
            fprintf( 2, 'You do not have write permission to ''pathdef.m'', and thus, the new path definitions cannot be saved.' );
        else;
            fclose( fid );
            savepath_status = savepath();  %#ok<MCSVP>
            if( savepath_status ~= 0 );
                fprintf( 2, 'Saving the new path definitions failed.' );
            fprintf( 'New path definition is saved.\n' );
            end; end; end;
end

function [ ret ] = ismatlab;
    ret = ~exist( 'OCTAVE_VERSION', 'builtin' );
end

function [ flag ] = check_toolbox( name )
switch name;
    case 'octave';      flag = exist( 'OCTAVE_VERSION', 'builtin' );
    case 'matlab';      flag = ~exist( 'OCTAVE_VERSION', 'builtin' );
    case 'statistic';   flag = isequal( exist('pdist2', 'file'), 2 );
    case 'signal';      flag = isequal( exist('upsample', 'file'), 2 );
    otherwise; fatal_error; end;
end

function dummy; end  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*MCCD,*MCHLP>  % MCC, use of functions: cd, help

