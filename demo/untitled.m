    fprintf( 2, 'This demo is not tested regularly, and may contain code which does not work with the current version.\n' );
while( true );
    clc
    %ct = ct + 1
    
    rng( ct )
    clf; hold on;
    %imagesc(real(log(M1)), 'XData',[min(Nx) max(Nx)], 'YDATA',[min(Ny) max(Ny)])
    %plotm( [cos(0:.1:2*pi);sin(0:.1:2*pi)], 'r-' )
    dim = 3;
    R = gallery_matrix( 'orthogonal','dim',dim );
    VV = R*rand(  dim, dim+1 );  %VV(:,1) = [0;1];
    %VV(:,2) = [-1;1];
    nrm = sqrt(sum(VV.^2,1));
    VV = VV./nrm;
    
    c = center_kone( VV, 'sdp2' );
    mn = min( sum(c.*VV,1) );
    if( mn >= 0);
        break; end;
end;
fprintf( 'mn: %f\n', mn );
fprintf( 'maxangle: %f\n', acosd(1 - max( pdist( VV', 'cosine' ) )) )
%norm(c)
if( numel(c) == 3 );
    view(c); end;
axis( [-1 1 -1 1] )
axis equal


