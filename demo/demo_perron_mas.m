fprintf( 2, 'This demo is not tested regularly, and may contain code which does not work with the current version.\n' );
ct = ct + 1  %#ok<NOPTS>
ct = 40;
rng( ct );
verbose = 1;

plotgraph = true;
dim = 9;
threshold = .3;
while( true );
    fprintf( '.' );
    
    [S, co] = gallery_graph( 'lattice2',dim, 'sparse',1, 'oriented',1, 'omitselfloops', 'noisolated' );
    S = full( S );
    % S(1:1+size(S, 1):end) = 0;  % remove self loops
    if( ~digraph(S).isdag );
        break;
    else;
        threshold = threshold*1.1; end; end;
dim = size( S, 1 );
fprintf( '\n' );

% upper bound on size of acyclic subgraph
val = S;
val(1:1+size(val, 1):end) = 0;
N = nnz( triu(val+val') );

%N = nnz( S );
%rho( double(S) )
%leadingeigenvector( double(S), 'nonnegative', 'nocell' )

plotm( 'clf','all' );
set( gcf, 'color','r' );
clc

%cleanpro = scoped_profile();

fig = 0;
a = 0;
b = N;
Xold = [];
while( true );
    m = floor( (a+b)/2 );
    [X, la] = optperron( 'X',Xold, 'ex','time', 'dim',dim, 'min', 'supp',S, 'sumlb',m, 'ub',1, 'v',verbose-1, 'method','all,mas,nomonotone', 'zerohandling','decompose' );
    
    Xg = digraph( X );
    Sg = digraph( S );
    ac = Xg.isdag;
    
    if( verbose>=2 || ac );
        
        fig = fig + 1;
        subplot_lazy( fig );
        if( plotgraph || ~isempty(co) || dim<10 || verbose>=2 );
            if( ~isempty(co) );
                h = plot( Sg, 'XData',co(1,:), 'YData',co(2,:) );
            else;
                h = plot( Sg ); end;
            highlight( h, Xg, 'EdgeColor','r', 'LineWidth',1.5 );
        else;
            imagesc( S + X ); end;
        title( [num2str(N) ' to ' num2str(nnz(X)) ' (' num2str(m) '), acyclic: ' num2str(ac) ', rho: ' num2str(rho(X))] ); 
        drawnow; end;
    
    if( ac );
        fprintf( 'feasible: %i (%i)\n', nnz(X), m );
        a = max( [ceil((a+b)/2), a+1, nnz(X)] );
    else;
        Xold = X;
        fprintf( 'infeasible: %i (%i)\n', nnz(X), m );
        b = min( floor((a+b)/2), b-1 ); end;
    
    if( b-a < .45 );
        break; end;    
end
set(gcf,'color','g');
fprintf( 'Done\n' );
clear cleanpro

[v, ~] = eig( S );
v = normalizematrix( v, 'positive', 1 );
nnz( all( isAlways(v>=-eps), 1 ) )

[v, ~] = eig( X );
v = normalizematrix( v, 'positive', 1 );
nnz( all( isAlways(v>=-eps), 1 ) )

%#ok<*NOSEL,*NOSEMI,*ALIGN>
