    fprintf( 2, 'This demo is not tested regularly, and may contain code which does not work with the current version.\n' );
A0=cell([1 2]); A0{1}=[1 -1;0 1]; A0{2}=[10/9 0;-10/9 10/9]; A = A0./max( rho(A0) );
dim = size( A0{1}, 1 );
tau = 1;  % step size, h
dwell = 0.1;  % m, M

w = [dwell tau dwell tau];

T = {expm(A{2}*dwell), expm(A{1}*tau), expm(A{1}*dwell), expm(A{2}*tau)};
G = [0 0 1 1;1 1 0 0;1 1 0 0;0 0 1 1];

[c, nc, findsmp_nfo] = findsmp( T, G, 'toa', w, 'v',2, 'maxsmpdepth',20 );
sigma = log( findsmp_nfo.jsrbound(1) )

At = cell( size(A) );
for i = 1:numel( A );
    At{i} = A{i} - sigma*eye( dim ); end;

Tt = {expm( At{2}*dwell ), expm( At{1}*tau ), expm( At{1}*dwell ), expm( At{2}*tau )};
rho( Tt )

figure( 1 );
[r, nfot] = ipa( Tt, G,  'maxnormerror',inf, 'maxtime',60*30, 'maxnumvertex',50000, 'maxremainingvertex',200000, 'sym',0, 'balancing',0, 'maxsmpdepth',4, 'admissiblealgorithm','r', 'plot','polytopeallimages' );

Pt = ipa_nfo( nfot, 'polytope' );
for i = 1:2;
    val = polytopenorm( At{i}^2, Pt{i}, 'mat', nfot.blockvar{1}.cyclictree.algorithm );
    val = val(2,:);
    nrmt(i) = val; end;  %#ok<SAGROW>

% figure(3);
% for i = 1:2;
%     subplot_lazy( i );
%     plotm( Pt{i}, 'r-', 'funct','p', 'hull',1 ); alpha .5
%     plotm( abs(At{i}^2*Pt{i}/nrmt(i)), 'k--', 'funct','p', 'hull',1, 'hold','on' ); alpha .5; end;

nrmt
val = max(nrmt)*dwell^2/8;
h = val*(1+1/val)^2;
acc = -1/dwell*log( 1 - h )



function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
 %#ok<*NOPTS>

