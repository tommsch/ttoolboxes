clc
    fprintf( 2, 'This demo is not tested regularly, and may contain code which does not work with the current version.\n' );

ct = ct + 1
rng( ct );
dim = 10;
%T = gallery_matrixset( 'noise',0, 'rho' );
%A = T{1};
%B = T{2};

A = gallery_matrix( 'randn', 'dim',dim, 'noise',0, 'rho' );
B = gallery_matrix( 'randn', 'dim',dim, 'noise',0, 'rho' );

cmx = findsmp( {A,B}, 'max' );
cmn = findsmp( {A,B}, 'min' );
cmx{1}'
cmn{1}'

clf; hold on;
eA = eig( A );
eB = eig( B );
plot( real(eA), imag(eA), 'ro' );
plot( real(eB), imag(eB), 'bo' );
x = 0:.001:(2*pi+.001);
plotm( [sin(x);cos(x)], 'k-' );
axis equal
diff = 0.001;
v = eig( A );
t = 0;
step = 0.001;
acc = 1000;
t = t + step;

method = 'product';

scale = 1;
while( true );
    switch method;
        case 'product';
            [nom, denom] = ratt( t );
            oo = randorder( nom, denom );
            X = buildproduct_fast( {A,B}, oo );
            scale = numel( oo );
        case 'harmonic';
            X = pinv( (1-t)*pinv(A) + t*pinv(B) );
        case 'linear';
            X = A*(1-t) + B*t;
        case 'geometric';
            warning( 'off',  'MATLAB:logm:nonPosRealEig' );
            X = expm( (1-t)*logm(A) + t*logm(B) );
        otherwise; fatal_error; end;
        
    v(:,end+1) = eig( X ).^(1/scale);
    d = norm( v(:,end) - v(:,end-1), inf );
    step = clamp( 1/(acc*dim*10*d^1.3), 1/(acc^2), 1/acc );
    t = t + step;
    
    if( t>1 );
        break; end;
    
    plot( real(v(:,end)), imag(v(:,end)), '.', 'color',clamp([cos(t*pi) sin(t*pi) t^2], 0, 1) );
    plot( t, max(abs(v(:,end))), '.', 'color',clamp([cos(t*pi) sin(t*pi) t^2], 0, 1) );
    drawnow;
    
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
