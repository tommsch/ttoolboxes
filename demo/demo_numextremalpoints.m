function [ ratio, H, P ] = demo_numextremalpoints( varargin );
% [ ratio, H, P ] = demo_numextremalpoints( [options] )
% generates random points and their convex hull
% Options:
%   'N',val         number of points
%   'dim',val       the dimension
%   'norm',val      the norm to use.
%   'diameter',val  points with norm>diameter are removed prior computing the convex hull
%   'dist',val      distribution of generated points
%                   val= 'n','g','normal','gauss':  Normal distribution
%                        'e','equal':               Equally distributed points in [0,1]^dim
%                        'i','integer':             Equally distributed (scaled) Integers
%   'verbose',val   The verbose level. Default = 1;
%   'noplot'        surpresses plot output
%   'seed',val      Seed for the random-number generator
%
% Output:
%   ratio           #P/#H
%   P               all generated points
%   H               the convex hull 
%
% E.g.: demo_numextremalpoints( 'dim',3 )
%
% Written by: tommsch, 2017
    fprintf( 2, 'This demo is not tested regularly, and may contain code which does not work with the current version.\n' );

    [DIST, varargin] =      parsem( 'dist', varargin, 'g' );
    [NORM, varargin] =      parsem( 'norm', varargin, 2 );
    [dim, varargin]  =      parsem( 'dim', varargin, 3 );
    [N, varargin] =         parsem( 'N', varargin, dim^4 );
    [diam, varargin] =      parsem( 'diameter', varargin, 1 );
    [verbose, varargin] =   parsem( 'verbose', varargin, 1 );
    [seed, varargin] =      parsem( 'seed', varargin, 0 );
    [noplot, varargin] =    parsem( 'noplot', varargin, 0 );
    if( seed~=0 );
        rng(seed);
        rand(1000); end;  % call randi(10000) to start Mersenne Twister
    parsem( varargin, 'test' );

    switch DIST;
        case {'g','n','normal','gauss'};    P = randn( dim, N );
        case {'e','equal'};                 P = 2*rand( dim, N ) - 1;
        case {'i','integer'};               P = 2*randi( N, dim, N )./N-1;
        otherwise; error( 'Unkown distribution.' ); end

    switch NORM;
        case 1;     P = P(:,sum( abs(P),1) < diam );
        case 2;     P = P(:,sqrt( sum(P.^2, 1)) < diam );
        case inf;   P = P(:,max( abs(P), [], 1) < diam );
        otherwise;  P = P(:,sum( abs(P).^NORM, 1).^(1/NORM) < diam ); end;

    assert( size(P, 2)>=dim, 'ttoolboxes:input', 'To less points. Either increase ''N'' or increase ''diam'' or change ''dist''.' );

    %plotm(P,'.');
    vprintf( 'Compute convex hull.\n','imp',[1 verbose]);
    idx = convhulln( P.' );
    H = P(:,unique( idx(:,1) ));  % points on the surface
    if( ~noplot && dim<=3 );
        if( dim==3 );
            trisurf( idx,P(1,:), P(2,:), P(3,:) );  end;
        if( dim==2 );
            plot( P(1,idx(1:end)), P(2,idx(1:end)) ); end;
        axis equal; end;

    ratio = size( P, 2 )/size( H, 2 );
    vprintf( 'Ratio: %g\n', ratio, 'imp',[1 verbose] );

end




function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
