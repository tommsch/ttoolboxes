function RESULT = demo_matrixenum( oldnum, maxnum );
    %#ok<*NASGU>
    %#ok<*AGROW>
    %very interesting: 6548, 6551, 6552, 7127, 7131, 7292, 7366
    %problem_binary2 = [-1];
    problem_binary3 = [44765 44779 44891 44893 52957 125802 ];  %very interesting: 7339,  28388, 14486, 32637 (needs ''addlimit''), 40887 (has infinitely many different leading eigenvectors, needs 'maxsmpdepth',1), 53085, 125813 ('maxsmpdepth',1), 147383 ('maxsmpdepth',1)
    %problem_binary3 = 3165;
    %problem_ternary2 = [-1 ];
    
    
        fprintf( 2, 'This demo is not tested regularly, and may contain code which does not work with the current version.\n' );
    clc
    %oldnum = tif( nargin==0, problem_binary2(end)+1, oldnum ); dim = 2; D = [0 1];
    oldnum = tif( nargin==0, problem_binary3(end)+1, oldnum ); dim = 3; D = [0 1];  % interesting
    %oldnum = tif( nargin==0, problem_ternary2(end)+1, oldnum ); dim = 2; D = [0 1 -1]; 
    %oldnum = -1;
    RESULT = cell( 4, 0 );;
    if( nargin<=1 );
        maxnum =numel(D)^(2*dim^2)-1; end;
    for num = oldnum:maxnum
    %for num = 90:500;
    %for num = problem_binary3;
        if( ~mod(num,1000) );
            fprintf( '\nnum: %i ', num ); end;
        fprintf( '.' );
        [A,num_t,nfo] = matrixenum( dim, 2, num, D, true, 'v',0 ); 
        if( isempty(nfo) );
            %b = get(getCurrentTask(),'ID');
            %fprintf( 'num: %i, task: %i \n\b\n', num, b );
            vdisp(num)
            vdisp(A)
            %nfo
            %[~,info] = ipa( A, 'v',0, 'plot','none', 'epssym',1e-12, 'case',tif( isequal(D,[0 1]),'P',''), 'invariantsubspace','perm','ncdelta',1, 'numcore',1 );
            [~,info] = ipa( A, 'v',1, 'plot','none','ncdelta',1,'maxsmpdepth',20 );
            RESULT(:,num) = {num;num_t;nfo;info};
            %num
            %break;
            fprintf('------------------------------\n'); 
        else;
            RESULT(:,num) = {num;num_t;nfo;[]}; end; end;


end



function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
