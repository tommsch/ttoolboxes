fprintf( 'Test scripts for invariant kone algorithms' )

%%
% Initialize variables
condset( 'counter', 0 );
res = struct( [] );  % syntax to create an empty struct

%%

counter = max( 0, counter - 1 );
dim = 3;
while( true );
    if( dim > 15 );
        dim = 3; end;
    dim = dim + .05;
    counter = counter + 1  %#ok<NOPTS>
    rng( counter )
    [T, ~, T_nfo] = gallery_matrixset( 'kone_nonneg_neg', 'num_try',inf, 'dim',floor(dim), 'J',2, 'different', 'filter',{'vandergraft',true, 'nonneg','notall', 'unitary','no', 'finite','all'} );
    if( isanyof(T_nfo.name, {'iota'}) );
        continue; end;
    if( all(T_nfo.int) );
        continue; end;
    
    tic;
    [pd_ret, K, Ks, dmin] = primal_dual_kone( T );
    time_fasttest = toc;
    if( ~isfinite(pd_ret) || isequal(pd_ret, false) );
        continue; end;


    res(end+1).counter = counter;  %#ok<SAGROW>
    idx = numel(res);

    res(idx).T = T;
    res(end).nfo_T = T_nfo;
    res(idx).fasttest_duration = time_fasttest;
    res(idx).fasttest = pd_ret;
    
    tic;
    [~, exact_nfo] = ipa( T, 'Kone', 'add','prune', 'recomputenorm',3, 'maxiter',30, 'maxvertexnum',5000, 'sym',0 );
    res(idx).exact_duration= toc;
    res(idx).exact_nfo = exact_nfo;
    
    if( isanyof( exact_nfo.log.errorcode, {ipa_errorcode.COMPLEXKONE,ipa_errorcode.NOINVARIANTKONE,ipa_errorcode.NOINVARIANTKONE_DUALTEST,ipa_errorcode.IS_NOT_A_KONE} ) );
        continue; end;

    tic;
    [~, et_nfo] = ipa( T, 'Kone', 'delta',1.01, 'recomputenorm',3, 'maxiter',30, 'invariantsubspace','none', 'maxvertexnum',3000, 'sym',0 );
    res(idx).et_duration = toc;
    res(idx).et_nfo = et_nfo;

    if( isfield( et_nfo.blockvar{1}.cyclictree, 'Mt' ) );
        tic;
        Tt = et_nfo.blockvar{1}.cyclictree.Mt;
        [pd_et_ret, K, Ks] = primal_dual_kone( Tt );
        res(idx).fasttest_Mt_duration = toc;
        res(idx).fasttest_Mt = pd_et_ret;
        end;
        
    end;
 %%
% Assess results
clc  %#ok<UNRCH>
tbl = cell( 0, 1 );
header = cell( 1, 0 );
header{end+1} = 'idx';                      idx_idx = numel( header );
header{end+1} = 'counter';                  idx_counter = numel( header );
header{end+1} = 'mat_name';                 idx_mat_name = numel( header );
header{end+1} = 'dim';                      idx_dim = numel( header );

header{end+1} = 'fasttest_duration';        idx_fasttest_duration = numel( header );
header{end+1} = 'fasttest_success';         idx_fasttest_success = numel( header );  % fasttest is always true

header{end+1} = 'exact_duration';           idx_exact_duration = numel( header );
header{end+1} = 'exact_errorcode';          idx_exact_errorcode = numel( header );
header{end+1} = 'exact_errorstr';           idx_exact_errorstr = numel( header );
header{end+1} = 'exact_success';            idx_exact_success = numel( header );

header{end+1} = 'fasttest_et_duration';     idx_fasttest_et_duration = numel( header );
header{end+1} = 'fasttest_et_success';      idx_fasttest_et_success = numel( header );

header{end+1} = 'et_duration';              idx_et_duration = numel( header );
header{end+1} = 'et_errorcode';             idx_et_errorcode = numel( header );
header{end+1} = 'et_errorstr';              idx_et_errorstr = numel( header );
header{end+1} = 'et_success';               idx_et_success = numel( header );

for idx = 1:numel( res )
    try;
        exact_errorcode = res(idx).nfo_iter.log.errorcode;  % .nfo_exact, .nfo_iter, .exact_nfo, 
        exact_errorstr = ipa_errorcode.get( exact_errorcode );
    catch me;
        exact_errorcode = nan;
        exact_errorstr = 'FAIL';
        end;
    try;
        et_errorcode = res(idx).nfo_et.log.errorcode;  % .et_nfo, .nfo_et
        et_errorstr = ipa_errorcode.get( et_errorcode );
    catch me;
        et_errorcode = nan;
        et_errorstr = 'FAIL';
        end;

    tbl{end+1,1} = [];  % increase size

    tbl{end,idx_idx} = idx;
    tbl{end,idx_counter} = res(idx).counter;
    tbl{end,idx_mat_name} = res(idx).nfo_T.name;
    tbl{end,idx_dim} = size( res(idx).T{1}, 1 );

    tbl{end,idx_fasttest_duration} = res(idx).fasttest_duration;
    tbl{end,idx_fasttest_success} = tif( isanyof( {exact_errorcode,et_errorcode}, {ipa_errorcode.VALIDATEUPPERBOUNDNORM, ipa_errorcode.NOERROR, ipa_errorcode.NOERROR_INVARIANTSUBSPACE}), 'Y', ...
                                         isanyof( {exact_errorcode}, {ipa_errorcode.COMPLEXKONE, ipa_errorcode.NOINVARIANTKONE_DUALTEST, ipa_errorcode.IS_NOT_A_KONE, ipa_errorcode.NOINVARIANTKONE}), 'N', ...
                                         '?' ...
                                       );
    
    tbl{end,idx_exact_duration} =   res(idx).iter_duration;  % .exact_duration, iter_duration, duration_exact
    tbl{end,idx_exact_errorcode} =  exact_errorcode;
    tbl{end,idx_exact_errorstr} =   exact_errorstr;
    tbl{end,idx_exact_success} = tif( isanyof( {exact_errorcode}, {ipa_errorcode.VALIDATEUPPERBOUNDNORM, ipa_errorcode.NOERROR, ipa_errorcode.NOERROR_INVARIANTSUBSPACE} ), 'Y', ...
                                      isanyof( {exact_errorcode}, {ipa_errorcode.COMPLEXKONE, ipa_errorcode.NOINVARIANTKONE_DUALTEST, ipa_errorcode.IS_NOT_A_KONE, ipa_errorcode.NOINVARIANTKONE}), 'Z', ...
                                      isanyof( {exact_errorcode}, {ipa_errorcode.MAXNUM_VERTEXREACHED, ipa_errorcode.MAXTIMEREACHED, ipa_errorcode.MAXITERATION, ipa_errorcode.MAXTREETIMEREACHED}), 'N', ...
                                      '?' ...
                                    );

    tbl{end,idx_fasttest_et_duration} = res(idx).fasttest_Mt_duration;
    if( isempty(res(idx).fasttest_Mt) ...
      );
        fasttest_et_correct = '?';
    elseif( ~isfinite(res(idx).fasttest_Mt) );
        fasttest_et_correct = '?';
    elseif( res(idx).fasttest_Mt && ...
            isanyof( {et_errorcode}, {ipa_errorcode.VALIDATEUPPERBOUNDNORM, ipa_errorcode.NOERROR, ipa_errorcode.NOERROR_INVARIANTSUBSPACE}) ...
      );
        fasttest_et_correct = 'Y';
    elseif( ~res(idx).fasttest_Mt && ...
            isanyof( {et_errorcode}, {ipa_errorcode.COMPLEXKONE, ipa_errorcode.NOINVARIANTKONE_DUALTEST, ipa_errorcode.IS_NOT_A_KONE, ipa_errorcode.NOINVARIANTKONE} ) ...
          );
        fasttest_et_correct = 'Y';
    elseif( isanyof( {et_errorcode}, {ipa_errorcode.MAXNUM_VERTEXREACHED, ipa_errorcode.MAXTIMEREACHED, ipa_errorcode.MAXITERATION, ipa_errorcode.MAXTREETIMEREACHED} ) ...
          );
        fasttest_et_correct = '?';
    else;
        fasttest_et_correct = 'N'; end;
    tbl{end,idx_fasttest_et_success} = fasttest_et_correct;

    tbl{end,idx_et_duration} =  res(idx).nfo_et_duration;  % .duration_et, .et_duration, .nfo_et_duration
    tbl{end,idx_et_errorcode} = et_errorcode;
    tbl{end,idx_et_errorstr} =  et_errorstr;
    tbl{end,idx_et_success} = tif( isanyof( {et_errorcode}, {ipa_errorcode.VALIDATEUPPERBOUNDNORM, ipa_errorcode.NOERROR, ipa_errorcode.NOERROR_INVARIANTSUBSPACE} ), 'Y', ...
                                   isanyof( {et_errorcode}, {ipa_errorcode.COMPLEXKONE, ipa_errorcode.NOINVARIANTKONE_DUALTEST, ipa_errorcode.IS_NOT_A_KONE, ipa_errorcode.NOINVARIANTKONE}), 'Z', ...
                                   isanyof( {et_errorcode}, {ipa_errorcode.MAXNUM_VERTEXREACHED, ipa_errorcode.MAXTIMEREACHED, ipa_errorcode.MAXITERATION, ipa_errorcode.MAXTREETIMEREACHED}), 'N', ...
                                   '?' ...
                                 );
    
    
    end;

tbl2 = tbl;
% sort wrt dim
[~, idx] = sort( [tbl2{:,idx_dim}] );
tbl2 = tbl2(idx,:);


% reassign error code numbers
c = unique( [tbl2{:,idx_et_errorcode}] );
yt = [];
ytl = {};
for i = 1:numel( c );
    idx = [tbl2{:,idx_et_errorcode}] == c(i);
    tbl2(idx,idx_et_errorcode) = {i};
    yt(end+1) = i;
    ytl{end+1} = ipa_errorcode.get( c(i) );
end

tbl3 = cell2table( tbl2, 'VariableNames',header )


% plotm( cell2mat( tbl2(:,[idx_dim, idx_et_errorcode]) )', 'violin',[.1  .1], '.' )
% yticklabels( ytl )
% yticks( yt )
% axis equal

%%
% Statistics 
d = d + 1;
idx_dim_n = [tbl2{:,idx_dim}] == d;
sze = nnz( idx_dim_n );
fasttest_duration = median( [tbl2{idx_dim_n,idx_fasttest_duration}] );
fasttest_success = nnz( [tbl2{idx_dim_n,idx_fasttest_success}] == 'Y' ) / nnz( [tbl2{idx_dim_n,idx_fasttest_success}] ~= '?' ) * 100;

exact_duration = median( [tbl2{idx_dim_n,idx_exact_duration}] );
exact_success = nnz( [tbl2{idx_dim_n,idx_exact_success}] == 'Y' ) / nnz( [tbl2{idx_dim_n,idx_exact_success}] ~= '?' & [tbl2{idx_dim_n,idx_exact_success}] ~= 'Z' ) * 100;

et_duration = median( [tbl2{idx_dim_n,idx_et_duration}] );
et_success = nnz( [tbl2{idx_dim_n,idx_et_success}] == 'Y' ) / nnz( [tbl2{idx_dim_n,idx_et_success}] ~= '?' & [tbl2{idx_dim_n,idx_et_success}] ~= 'Z' ) * 100;

fasttest_et_duration = median( [tbl2{idx_dim_n,idx_fasttest_et_duration}] );
fasttest_et_success = nnz( [tbl2{idx_dim_n,idx_fasttest_et_success}] == 'Y' ) / nnz( [tbl2{idx_dim_n,idx_fasttest_et_success}] ~= '?' & [tbl2{idx_dim_n,idx_fasttest_et_success}] ~= 'Z' ) * 100;
fprintf( '%i & %2.1f\\,s & %2i\\%% & %2.1f\\,s & %2i\\%% &  %2.1f\\,s & %2i\\%% \\\\\n', d, fasttest_duration, round(fasttest_success), exact_duration, round(exact_success), et_duration, round(et_success) )


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
