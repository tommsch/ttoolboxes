    fprintf( 2, 'This demo is not tested regularly, and may contain code which does not work with the current version.\n' );
clc
condset( 'ct', 1 )
ct = ct + 1  %#ok<NOPTS,SUSENS>
%ct = 1139; dim = 50; J = 2;  % something goes wrong here
%ct = 1138; dim = 10; J = 3;  % something goes wrong here
rng( ct )

verbose = 2;

J = 3;
dim = 15;

figure( 1 ); clf;
[Tt, T_orig, B_orig,limits] = make_matrix( dim, J, 'rand_int', verbose );
%Tt = cast_to( Tt, 'vpa' );
J = numel( Tt );
dim = size( Tt{1}, 1 );

T_mean = make_rand_lincomb( Tt );

%[Ub, Sb, Vb] = svd( T_mean );
[vb, db] = eig( T_mean );
%[U, Tri] = schur( T_mean );
B = vb;  % which basis to use subsequently
 
figure( 2 ); clf;
Tt_orig = in_basis( Tt, B, 'vb' );
Tt_min = make_min_matrix( Tt_orig );

figure( 3 ); clf;
first_gap_value = get_gap( Tt_min, verbose );
Tt_min(Tt_min < first_gap_value) = 0;
Tt_min = double( logical(abs(Tt_min)) );

[Tmin_perm, B_perm] = invariantsubspace( Tt_min, 'permutation' );
vb_perm = vb*B_perm;
sze_subspace = cellfun( @(x) size(x{1}, 1), Tmin_perm);
if( verbose >= 2 );
    figure( 4 );
    clf;
    in_basis( Tt_min, B_perm, 'minmatrix in vb-perm' );
    sgtitle( ['Sizes of subspaces of Tt-min:' newline vdisp(sze_subspace)] );
    end;


vb_orth = orthogonalize_basis( vb_perm, sze_subspace );

figure( 5 ); clf;
T_res = in_basis( Tt, vb_orth, 'vb-orth' );
TT_res = get_blocks( T_res, verbose );

    
plotm( 'link','c' )
caxis( limits )  %#ok<CAXIS>
colorbar
%set( groot,'ColorScale','log')

rho_orig = round( nestedcellfun( @rho, T_orig ) )  %#ok<NOPTS>
rho_TT = round( nestedcellfun( @rho, TT_res) )  %#ok<NOPTS>
%%
iB_orig = inv( B_orig );
idx=1:7; rank([iB_orig(:,idx) vb_orth(:,idx)])

%%

assert( numel(T_orig) == numel(TT_res), 'technical assertion failed.' );
r = {};
r_res = {};
for i = 1:numel( T_orig );
    r{i} = ipa( T_orig{i}, 'delta',0.99999 );  %#ok<SAGROW>
    r_res{i} = ipa( TT_res{i}, 'delta',0.99999 );  %#ok<SAGROW>
    end;
for i = 1:numel( T_orig );
    fprintf( 'Difference of %i-th block: %e\n', i, norm(r{i} - r_res{i})); end;

%%

function [ T, TT, B, limits ] = make_matrix( dim, J, what, verbose )
    if( nargin <= 3 || isempty(verbose) );
        verbose = 1; end;
    if( nargin <= 2 );
        what = {'rand_int', 'sym',4}; end;
    what = tocell( what );
    
    M = gallery_matrixset( 'dim',dim, 'J',J, 'v',0, what{:} );
    J = numel( M );
    dim = size( M{1}, 1 );

    lengths = randsum( dim );
    lengths = sort( lengths );
    clengths = cumsum( lengths );
    clengths0 = [0 clengths];
    mask = ones( dim );
    for l = 1:numel( lengths ) - 1;
        idx_c = (clengths(l) + 1):clengths(l+1);
        idx_r = 1:clengths(l);
        mask(idx_c,idx_r) = 0;
    end;
    mask = mask .* (rand( dim ) < .8);  % for sparse matrices
    %mask = mask .* mask';  % for block diagonal matrices
    %mask = eye( dim );  % for diagonalizable matrices
    
    while( true );
        while( true );
            B = gallery_matrix( 'unitary', 'dim', size(M{1}, 1), 'v',0, what{:} );
            B = normalizematrix( B, 'colnorm',2 );  % not necessary, only here to have better numerical stability during prototyping

            if( issym(M{1}) );
                B = sym( B ); end;
            if( issym(B) || ...
                cond(B) < dim ...
              );
                break; end; end;
        T = cell( 1, J );
        TT = cell( 1, numel(clengths) );
        for j = 1:J;
            T{j} = B\(mask.*M{j})*B;
            
            for k = 1:numel(clengths);
                idx_TT = clengths0(k)+1:clengths0(k+1);
                TT{k}{j} = M{j}(idx_TT, idx_TT); end; end;
        norm_test = 0;
        for j = 1:J;
            norm_test = norm_test + norm( (B*T{j}/B) .* (~mask), 1 ); end;
        bound = 1e-12 * max( cellfun( @(x) norm(x, 1), T ) );
        if( norm_test ) < bound;
            break; end; end;


    maxval = [M{:}];
    maxval = max(abs(double(maxval(:))));
    limits = [0 maxval];

    if( verbose >= 2 );
        for j = 1:J;
            subplot_lazy( j );
            cla;
            imagesc( double(mask.*M{j}) );
            axis equal
            end;
        sgtitle( ['Images of original input matrices before scrambling.' newline ...
                  'Error: ' sprintf('%.3e', norm_test) newline ...
                  'Sizes of subspaces original: ' newline vdisp(lengths) ...
                 ] ...
               ); end;

    
    
    vprintf( 'Error: %g\nSizes of subspaces original: %v\n', norm_test, lengths );
    
end

function [ T ] = cast_to( T, type );
    if( iscell(T) );
        for i = 1:numel( T );
            T{i} = cast_to( T{i}, type ); end;
    else;
        T = eval( [type '( T );'] ); end;
end

function [ T_mean ] = make_rand_lincomb( T );
    dim = size( T{1}, 1 );
    J = numel( T );
    T_mean = zeros( dim );
    if( issym(T{1}) );
        T_mean = sym( T_mean ); end;
    al = rand( 1, J ); al = al/sum(al);
    for j = 1:J
        T_mean = T_mean + al(j)*T{j}; end;
    if( issym(T{1}) );
        T_mean = vpa( T_mean ); end;
end

function [ TB ] = in_basis( T, B, verbose );
    if( nargin <= 2 || isempty(verbose) );
        verbose = 1; end;
    if( ischar(verbose) );
        name = verbose;
    else;
        name = '(unnamed basis)'; end;
    cellflag = iscell( T );
    if( ~cellflag );
        T = {T}; end;
    J = numel( T );
    TB = cell( size(T) );
    for j = 1:J;
        TB{j} = B\T{j}*B; end;

    if( verbose >= 2 );
        for j = 1:J;
            subplot_lazy( j );
            imagesc( abs(double(TB{j})) );
            axis equal;
            sgtitle( ['Matrices in new basis: ' name] );
            end; end;

    if( ~cellflag );
        TB = TB{1}; end;
end

function [ Tmin ] = make_min_matrix( T );
    dim = size( T{1} );
    J = numel( T );
    Tmin = inf( dim );
    if( issym(T{1}) );
        Tmin = sym( Tmin ); end;    
    
    for j = 1:J;
        min_idx = abs( T{j} ) < abs( Tmin );
        Tmin(min_idx) = T{j}(min_idx);
        end;
end



function [ first_gap_value ] = get_gap( Tmin, verbose )
    if( nargin <= 1 || isempty(verbose) );
        verbose = 1; end;
    
    BinWidth = 20;
    while( true );
        [counts, edges] = histcounts( log(abs(double(Tmin(:)))), 'BinWidth',BinWidth );
        %bins = histogram( log(abs(double(Tmin(:)))), 'BinWidth',BinWidth );
        first_gap_bin = find( counts == 0, 1 );
        if( isempty(first_gap_bin) );
            BinWidth = BinWidth/1.5;
        else;
            first_gap_value = exp( edges(first_gap_bin) );
            break; end; end;

    if( verbose >= 2 );
        subplot_lazy( 1 );
        imagesc( abs(double(Tmin)) );
        axis equal
        title( 'Minimum among all matrices in basis B0' )

        subplot_lazy( 2 );
        histogram( log(abs(double(Tmin(:)))), 'BinWidth',BinWidth );
        title( 'Histogram of values of upper matrix.' );    
        end;

end

function [ Bo ] = orthogonalize_basis( B, sze_subspace );
    bounds_subspace = [0 cumsum( sze_subspace )];
    Bo = zeros( size(B) );
    Bo = cast( Bo, 'like',B );
    for i = 1:numel( sze_subspace );
        idx = (bounds_subspace(i) + 1):bounds_subspace(i+1);
        Bi = B(:,idx);
        Bo(:,idx) =  orth( [real(Bi) imag(Bi)] ); end;
end

function [ TT ] = get_blocks( T, verbose );
    if( nargin <= 1 || isempty(verbose) );
        verbose = 1; end;

    cellflag = iscell( T );
    if( ~cellflag );
        T = {T} ; end;
    dim = size( T{1}, 1 );
    TTsum = zeros( [dim dim] );
    TTsum = cast( TTsum, 'like',T{1} );
    for j = 1:numel( T );
        TTsum = TTsum + abs( T{j} ); end;

    first_gap_value = get_gap( TTsum, 0 );
    TTsum(TTsum < first_gap_value) = 0;

    col_idx = zeros( dim, 1 );
    for i = 1:dim;
        val = find( TTsum(i,:), 1 );
        if( isempty(val) );
            val = dim + 1; end;
        col_idx( i ) = val; end;
    equal_idx = find( col_idx == (1:dim)' )';
    block_sizes = diff( [equal_idx dim+1] );
    cum_block_sizes = [0 cumsum( block_sizes )];
    TT = cell( 1, numel(block_sizes) );
    for k = 1:numel( TT );
        for j = 1:numel( T );
            idx = cum_block_sizes(k)+1:cum_block_sizes(k+1);
            TT{k}{j} = T{j}(idx,idx); end; end;

    if( verbose >= 2 );
        sgtitle( ['Sizes of subspaces computed:' newline vdisp(block_sizes)] ); end;
end



function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
