function [ varargout ] = demo_polytopenorm( varargin );
% [ [VV] ] = demo_polytopenorm( [options] )
% Demonstrates how estimatepolytopenorm() works
%
% Options:
%   'dim',val                     dimension used to plot
%   'seed',val                    Seed for the random-number generator
%   'N',val                       default:30, number of points of the polytope
%   'step',val                    default:0.02, Resolution
%   'polytope',val                which polytope to generate. Default=0
%                                       possible values: 'k', 'p' (default), 'r', 'c'
%   'algorithm',val               which algorithm to use
%   'removeintpts'                default: false, If set, interior points of the polytope are removed
%   'colorregion',val             default:1, Colors the regions where the points are for sure outside or inside the polytope.
%
% Output:
%   VV                            the vertices of the polytope used
%   A nice picture
%   
%
% See also: testnorm2, estimatepolytopenorm
%
% Written by: tommsch, 2018

    fprintf( 2, 'This demo is not tested regularly, and may contain code which does not work with the current version.\n' );

    [VV, varargin] =                parsem( {'VV','V'}, varargin, [] );
    [pts, varargin] =               parsem( {'points','point','pts','pt'}, varargin, [] );
    [nVV, varargin] =               parsem( {'N','n'}, varargin, [] );
    if( ~isempty(VV) );
        nVV = size( VV, 2 ); end;
    
    [opt.dim, varargin] =           parsem( {'dim','indim'}, varargin, 2 );
    [opt.verbose, varargin] =       parsem( {'verbose','v'}, varargin, 1 );
    [opt.resolution, varargin] =    parsem( {'res','resolution'}, varargin, 0.02 );
    [opt.cnum, varargin] =          parsem( {'complexresoltuion','cres'}, varargin, 20 );
    [opt.polytope, varargin] =      parsem( {'polytope'}, varargin, 'p' );
    [opt.algorithm, varargin] =     parsem( {'algorithm','alg'}, varargin, [opt.polytope 'E'] );
    [opt.removeintpts, varargin] =  parsem( {'removeintpts','rip'}, varargin, 0 );  % remove interior points
    [opt.colorregion, varargin] =   parsem( {'colorregion','colourregion','color','colour','c'}, varargin, 1 );
    
    [opt.seed, varargin] =              parsem( {'rng','seed'}, varargin, 0 ); 
    if( opt.seed );
        oldrng = rng( opt.seed );
        cleanrng = onCleanup( @() rng(oldrng) ); end;
    parsem( varargin, 'test' );

    switch lower( opt.polytope(1) );
        case {'k'};
            if( isempty(nVV) );
                if( isanyof( lower(opt.polytope), {'k','k1'} ) );
                    nVV = opt.dim + 2; 
                else;
                    nVV = opt.dim + 3; end; end;
            if( isempty(nVV) );
                nVV = opt.dim + 3; end;
            if( isempty(VV) );
                VV = randn( opt.dim, nVV )/3;
                VV = normalizematrix( VV, 'colnorm', 2 )/2 + rand( size(VV) )*.3; end;
            if( isempty(pts) );
                cobd = max( abs(VV(:)) ) + .2;
                pts = mixvector( -cobd:opt.resolution:cobd, opt.dim ); end;
            epsprog = 5e-10;

%             for i = nVV:-1:1;
%     %              plotm( VV, 'x-', 'funct','K' )
%                 nrm = polytopenorm( -VV(:,i), VV(:, [1:i-1 i+1:end]), 'k1', 'v',-1, 'debug' );
%                 if( nrm(2)<1 );
%                     VV(:,i) = []; end; end;
            
        case {'l','p'};
            if( isempty(nVV) );
                nVV = 10; end;
            if( isempty(VV) );
                VV = rand( opt.dim, nVV )/3; 
                VV = abs((normalizematrix( VV, 'colnorm', 2 ))/3 + rand( size(VV)) )*.5; end;
            if( isempty(pts) );
                cobd = max( abs(VV(:)) ) + .2;
                pts = mixvector( 0:opt.resolution:cobd, opt.dim ); 
                expect( size(pts,2)^2*8 < 2*1024*1024*1024, 'demo_polytopenorm:memory', ...
                        ['Most likely too many points are needed to compute.\n' ...
                         '  If the computer hangs, or this function fails, try to reduce the extents of the polytope,\n' ...
                         '  and/or reduce the resolution.\n'] ); 
            end;
            epsprog = 5e-10;
        case {'r'};
            if( isempty(nVV) );
                nVV = 10; end;
            if( isempty(VV) );
                VV = randn( opt.dim, nVV )/3;
                VV = normalizematrix( VV, 'colnorm', 2 )/2 + rand( size(VV) )*.3; end;
            if( isempty(pts) );
                cobd = max( abs(VV(:)) ) + .2;
                pts = mixvector( -cobd:opt.resolution:cobd, opt.dim ); end;
            epsprog = 5e-10;
        case {'c'};
            if( isempty(nVV) );
                nVV = 5; end;
            if( isempty(VV) );
                VV = randn( opt.dim, nVV )/3 + 1i*randn( opt.dim, nVV )/3; 
                VV = normalizematrix( VV, 'colnorm', 2 )/2 + rand( size(VV) )*.3; end;
            if( isempty(pts) );
                cobd = max( abs(VV(:)) ) + .2; 
                pts = mixvector( -cobd:opt.resolution:cobd, opt.dim );
                pts = pts + 1i*0.5*randn( [size(pts) opt.cnum] ); end;
            epsprog = 5e-5;
        otherwise;
            error( 'testnorm:arg', 'wrong value for ''polytope''.' ); end;

    vprintf( 'Number of points to compute: %i\n', size(pts, 2)*size(pts,3) );

    clf;
    % remove interior points
    if( opt.removeintpts );
        val = size( VV, 2 );
        plotm( VV, 'functional',opt.algorithm(1), 'r:' );
        [normval, ~] = polytopenorm( VV, VV, opt.algorithm );
        VV(:,normval(2,:) < 1-epsprog) = []; 
        plotm( VV, 'functional',opt.algorithm(1), 'g.', 'hold',2 );
        vprintf( 'Removed vertices: %i\n', val-size(VV, 2), 'imp',[1 opt.verbose] ); 
        end;
        
    if( isequal(opt.polytope(1), 'k') );
        plotm( VV, 'functional',opt.algorithm(1), 'k-', 'hold',2 ); end;
    plotm( VV, 'functional',opt.algorithm(1), 'g-', 'hull',1, 'hold',2 );
    plotm( VV, 'functional',opt.algorithm(1), 'gx', 'hold',2 );
    if( opt.dim >= 3 );
        alpha( 0.6 );
        lightangle( gca, -45, 30 );
        shading( 'interp' ); end;


    normval = polytopenorm( pts, VV, opt.algorithm, 'v',opt.verbose );
    vprintf( '\n', 'imp',[1 opt.verbose] );

    if( opt.colorregion );
        idx = normval(2,:)<1;
        normval(1,idx) = normval(2,idx) - opt.colorregion;

        idx = normval(3,:)>1;
        normval(1,idx) = normval(3,idx) + opt.colorregion; end;

    normval = normval(1,:);

    % make image
    len = round( size(pts, 2)^(1/opt.dim) );
    len = repmat( len, [1 opt.dim] );
    normval = reshape( normval, [len size(pts, 3)] );

    %clf; 
    hold on;

    image( 'XData',[min(pts(1,:)) max(pts(1,:))], 'YData',[min(pts(2,:)) max(pts(2,:))], 'CData',max(normval, [], 3), 'CDataMapping','scaled' );
    h = get( gca, 'Children' );
    colorbar;
    axis equal;
    set( gca, 'Children', h(numel(h):-1:1) );
    
    if( nargout >= 1 );
        varargout = cell( 1, nargout ); end;
    if( nargout >= 1 );
        varargout{1} = VV; end;
        

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
