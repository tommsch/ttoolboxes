fprintf( 2, 'This demo is not tested regularly, and may contain code which does not work with the current version.\n' );
%%
%#ok<*NASGU> 
% keine Interessanten Beispiele für heute (20.04.2023)
ct = ct + 1
clc
close all
minmax = 'min';
defopt = {'ub',1000};
% ct = 119; dim =  20; e = 0.1;  addopt = {};                      % beginning: ev, then :aftw, only min
 ct = 134; dim =  20; e = 0.1;  addopt = {};                      % zerohandling: decompose best
% ct = 579; dim =  20; e = 0.05; addopt = {'diffdiag',{'N',3,'>'}};
% dim =  200; e = 0;    addopt = {'rowsumlb',rand(1,dim), 'colsumlb',rand(1,dim)};              % very different results
% dim = 200; e = 0;    addopt = {'rowsumlb',[], 'colsumlb',[],'diffdiag',{'>','N',20}};    % very different results
% ct = 177; 
% dim =  50; e = 0.05; addopt = {'antidiagsum',dim, 'symmetric', 'diffdiag',{'N',10}};    % very different results
% ct = 33; dim =  50; e = 0.05; addopt = {'antidiagsum',dim, 'diffdiag',{'N',10}, 'diagsum',dim/2};    % very different results
%rng( ct )
%X1 = optperron( 'dim',dim, minmax, 'rand',e, addopt{:}, defopt{:}, 'num_try',1, 'seed',ct, 'ex','opt',  'v',1,  'figure',1, 'zerohandling','off', 'plotscaled',0  );
%rng( ct )
%X2 = optperron( 'dim',dim, minmax, 'rand',e, addopt{:}, defopt{:}, 'num_try',1, 'seed',ct, 'ex','opt',  'v',1,  'figure',2, 'zerohandling','off', 'method','ev,fw', 'plotscaled',0 );
rng( ct );
X2 = optperron( 'dim',dim, minmax, 'rand',e, addopt{:}, defopt{:}, 'num_try',1, 'seed',ct, 'ex','opt',  'v',1,  'figure',2, 'zerohandling','decompose', 'method','ev,fw', 'plotscaled',0 );
rng( ct )
X3 = optperron( 'dim',dim, minmax, 'rand',e, addopt{:}, defopt{:}, 'num_try',1, 'seed',ct, 'ex','opt',  'v',1,  'figure',3, 'zerohandling','decompose', 'method','fw', 'plotscaled',0 );
rng( ct );
X4 = optperron( 'dim',dim, minmax, 'rand',e, addopt{:}, defopt{:}, 'num_try',1, 'seed',ct, 'ex','opt',  'v',1,  'figure',4, 'zerohandling','decompose', 'method','all', 'plotscaled',0 );

rho( { X2, X3, X4} )
%%
% dim =  50;  % 243, 663, 874, 886, 1142, 1233
% dim = 100;  
dim = 50;
%close all
while( true );
    clc;
    try;
        ct = ct + 1  %#ok<NOPTS>
        %ct = 663;

        addopt = {'antidiagsum',dim, 'diffdiag',{'N',10}, 'diagsum',dim/2};
        e = 0.05;
        rng( ct );        
        X = optperron( 'dim',dim, 'max', 'rand',e, addopt{:}, 'num_try',1, 'seed',ct, 'ex','opt',  'v',1,  'figure',1, 'method','fw', 'zerohandling','decompose' );
        %rng( ct );
        %X = optperron( 'dim',dim, 'min', 'rand',e, addopt{:}, 'num_try',1, 'seed',ct, 'ex','opt',  'v',1,  'figure',2, 'method','ev', 'zerohandling','decompose' );
        %rng( ct );
        %X = optperron( 'dim',dim, 'max', 'rand',e, addopt{:}, 'num_try',1, 'seed',ct, 'ex','opt',  'v',1,  'figure',3, 'method','all', 'zerohandling','decompose' );
        
        2;
        break;
    catch me;
        2;
        end; end;

%%



while( true )
    clc;
    ct = ct + 1  %#ok<NOPTS>
    rng( ct )
    % ct = 101
    dim = 20;
    try;
        def_opt = {'ub',1000, 'diffdiag',{'<>'}};
        neg_ex = {'-stoch', '-rstoch', '-cstoch', '-dstoch', '-1norm', '-symmetric', '-bisymmetric'};
        th = rand/3;
%         minmax = tif( randi([0 1]), 'min', 'max' )
        minmax = 'min';
        %[X1, l1] = optperron(  'num_try',3, minmax, 'dim',dim, 'rand',th, def_opt{:}, neg_ex{:}, 'seed',ct, 'ex','opt', 'method','ev',  'figure',12 );
        [X2, l2] = optperron(  'num_try',3, minmax, 'dim',dim, 'rand',th, def_opt{:}, neg_ex{:}, 'seed',ct, 'ex','opt', 'method','ev,fw',  'figure',13 );
        [X3, l3] = optperron(  'num_try',3, minmax, 'dim',dim, 'rand',th, def_opt{:}, neg_ex{:}, 'seed',ct, 'ex','opt', 'method','ev,simple', 'figure',14 );
        %r1 = rho( X1 );
        r2 = rho( X2 );
        r3 = rho( X3 );
        if( ... (norm(X1-X2)>1e-9 || norm(X1-X3)>1e-9 || norm(X2-X3)>1e-9) && ...
            (norm(X2-X3)>1e-9) && ...
            ... (numel(l1)>8 || numel(l2)>6 || numel(l3)>15) || ...
            (numel(l2)>6 || numel(l3)>15) || ...
            ... (abs(r1-r2)>1e-9 || abs(r1-r3)>1e-9 || abs(r2-r3)>1e-9 ) ...
            (abs(r2-r3)>1e-9 ) ...
          );
            break; 
            end;
    catch me;
        switch me.identifier;
            case 'optperron:infeasible';
                continue;
            otherwise;
                disp( me );
                break; end; end; end;
%%
%%

clc
plotm( 'clf','all' )
ct = ct + 1;
% ct = 5018;
rng( ct )


% sol = [2;0;dim];

while( true );
    clc;
    ct = ct + 1  %#ok<NOPTS>
    % ct = 101
    try;
        rng( ct )
        dim = 100;
        def_opt = {'min', 'num_try',3, 'dim',dim, 'seed',ct};
       
        %rsum = rand(1,dim); rsum = rsum/sum(rsum) * dim;
        %csum = rand(1,dim); csum = csum/sum(csum) * dim;
        rsum = (1:dim)/dim;
        csum = (1:dim)/dim;
        ex = {'rowsum',rsum, 'colsum',csum};
%         ex = {'rand', rand/10};
        [X1, l1] = optperron(  def_opt{:}, ex{:}, 'ex','opt', 'method','all' );
        
        val = unique( l1(1,:) );
        if( numel(val)>sol(1,end) );
            sol(:,end+1) = [numel(val); ct; dim];
            %break; 
            end;
    catch me;
        switch me.identifier;
            case 'optperron:infeasible';
                continue;
            otherwise;
                disp( me );
                break; end; end; end;
    
clf; hold on;
%plot( l )
l=l1(1,:)
lim = 0;
ls = l-lim;
plot( ls(1:end-1)./(ls(2:end).^1) )    
%%
clc
plotm( 'clf','all' )
%ct = ct + 1;
% ct = 5018;
rng( ct )

def_opt = {'num_try',3, 'ub',1000};

dim = 100;

minmax = tif( randi([0 1]), 'min', 'max' );

minmax = 'max';

% dim = 20; ex = {'diffdiag', {'N',10,'<'}, 'halfplane', []}; minmax = 'max';  % 4969, 5036

% seems to have complexity O(dim^2)

 ex = {'columnsumlb', [], 'maindiagsumub', [], 'banded', [], 'rowsumlb', []}; minmax = 'max';  % ev and monotone are important
% ex = {'rowsumlb', [], 'sumub',1000, 'halfplane',[]};  % fw alone is worse then ev alone
% ex = {'columnsumlb', [], 'maindiagsumub', [], 'banded', [], 'rowsumlb', [], 'diffdiag',2}; minmax = 'min';  % 5018  % ev and monotone are important
% ex = {'antidiagsumeq', [], 'diffdiag',{'<>'}, 'posdiagdomsub', [], 'monotonedec',0 }; minmax = 'max';  % 4973 % seems to have complexity O(dim^2)
ex = {'sumub', 1000, 'rowsumlb', [], 'halfplane', []};  % ev!

% nicht spannend
%ex = {'bisymmetric', 'rowsumlb',[], 'maindiagsumeq', [], 'rowsumub',1000};  % monotone (5002)
% % not interesting anymore % ex = {'bisymmetric', 'circulant', 'sumlb',[], 'antidiagsumub',[], 'posdiagdom',[], 'halfplane',[]};
% % not interesting anymore % ex = {'circulant', 'colsumub',1};
% ex = {'diffdiag', {'<'}, 'halfplane',[], 'halfplane',[] }; minmax = 'max';  % 5034, 5035, 5039 % ev and fw
% ex = {'diffdiag', {'<>'}, 'maindiagsumeq', []};

rng( ct );
X1 = optperron(  minmax, 'dim',dim, ex{:}, def_opt{:}, 'seed',ct, 'ex','opt', 'method','all',  'figure',12 );
rng( ct );
X2 = optperron(  minmax, 'dim',dim, ex{:}, def_opt{:}, 'seed',ct, 'ex','opt', 'method','ev,fw',  'figure',13 );
rng( ct );
X3 = optperron(  minmax, 'dim',dim, ex{:}, def_opt{:}, 'seed',ct, 'ex','opt', 'method','ev,simple', 'figure',14 );
rho( {X1 X2 X3} )
%%
clc

%sol = {};
method = 'all';
for dim = 100:3:200;
    dsum = ones(1,2*dim-1);  % global minimum is 1
    ex = {'diagsum',dsum};  % global minimum is dim+2
    % dsum = [1:dim dim-1:-1:1];
    for R = 1:20;
        def_opt = {'min', 'num_try',3, 'dim',dim, 'seed',ct };
        [X, l] = optperron(  def_opt{:}, ex{:}, 'ex','time', 'method','all' );
        sol(:,end+1) = {dim; rho(X); method};
    end
end

clf; hold on;
plot(100*[sol{2,:}],'.')
plot([sol{1,:}],'r.')
%%

A = gallery_matrixset( 'randsvd', 'dim',dim, 'N',1 ); A = abs( A );
switch deal( 1 );
    case 1;
        B = gallery_matrixset( 'randsvd', 'dim',dim, 'N',1 ); B = abs( B );
    case 2;
        rp = rho_prime( A );
        rp = rp / norm( rp );
        B = A + 5*rp;
    case 3;
        B = norm(A) * randn( size(A) ) * 5;
        B = abs( B );
    end;

vA = leadingeigenvector( A, 'nonnegative' );
vA = vA{1};
vB = leadingeigenvector( B, 'nonnegative' );
vB = vB{1};
N = 100;
r = zeros( 2, 0 );
d = zeros( 2, 0 );
for t = linspace( 0, 1, N );
    X = B*t + A*(1-t);
    rX = rho( X );
    vX = leadingeigenvector( X, 'nonnegative' );
    vX = vX{1};
    dX = min( norm( vA - vX ), norm( vB - vX ) );
    r = [r [rX;dX] ];  %#ok<AGROW>
end
r(1,:) = r(1,:)./max(r(1,:));
r(2,:) = r(2,:)./norm(vA-vB);
plot( r' )

%#ok<*NOSEL,*NOSEMI,*ALIGN>