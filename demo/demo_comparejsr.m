function demo_comparejsr( varargin )
% demo_comparejsr( ['what', what], [options] );
% Tests the algorithm ipa using example-matrices.
%
% Input:
%   'what',what          string passed to gallery_matrixset
%   'dim',dim             2nd value passed to gallery_matrixset
%   'N',N               3rd value passed to gallery_matrixset
%
% Options:
%   'arg5',val      some matrices from gallery_matrixset need a fifth argument, which is this. Default: arg5=[]
%   'diary'         diary is started
%   'restart'       after finishing the test-run, the dimension is increased, and another test is started
%   anything else is passed to ipa
%
% Output:
%   Nice pictures, also saved to disk
%
% Written by: tommsch, 2017

    fprintf( 2, 'This demo is not tested regularly, and may contain code which does not work with the current version.\n' );
    
[seed, varargin] =      parsem( {'seed','rng'}, varargin, 1234 );
[what, varargin] =      parsem( 'what', varargin, [] );
[dim, varargin] =       parsem( 'dim', varargin, 2 );
[N, varargin] =         parsem( {'J','N'}, varargin, 2 );
[arg5, varargin ] =     parsem( 'arg5', varargin, [] );
[restart, varargin ] =  parsem( 'restart', varargin );
%[fname, varargin ] =    parsem( 'filename', varargin, '' );

if( isempty(what) );
    assert( numel(varargin) >= 1 );
    what = varargin{1};
    varargin(1)=[]; end;

while( true );    
    OPT = vprintf( repmat(['%v '], 1 ,size(varargin, 2)), varargin{:} );  %#ok<NBRAK>

    treetime = []; 
    numberofvertex = []; 
    treedepth = []; 
    stepbig = []; 
    JSR = [];  
    errorcode = [];  
    lengthofcandidates = [];  

    k = 0;
    
    while(true)
        fprintf( 2, 'Seed = %i\n',seed );
        if(k > 20); 
            break; end;
        
        M = gallery_matrixset( what, 'dim',dim, 'N',N, 'seed',seed, arg5 );
        seed = seed + 1;

        try;
            JSRi = [];
            [JSRi, nfo] = ipa( M, 'maxtime',3*2^(dim/1.5)+100, 'verbose',2, varargin{:}, 'extrapath',1 );
        catch me;  %#ok<NASGU>
            nfo.info.algorithm = 2; end;
        if(numel(JSRi) == 2); continue; end;
        %if(type.info.algorithm == 2); continue; end;
        %if(isempty(type.info.algorithm)); continue; end;
        %try
            if( ~isempty(JSRi) && numel(JSRi) == 1 );
                JSR(end+1) =                JSRi(1);
                lengthofcandidates(end+1) = max( cellfun(@length, nfo.blockvar{1}.cyclictree.ordering) );
                treetime(end+1) =           nfo.log.treetime;
                numberofvertex(end+1) =     nfo.log.numberofvertex;
                treedepth(end+1) =          nfo.log.iteration;
                stepbig(end+1) =            nfo.log.num_stepbig;
            else
                JSR(end+1) =                inf;
                lengthofcandidates(end+1) = inf;
                treetime(end+1) =           inf;
                numberofvertex(end+1) =     inf;
                treedepth(end+1) =          inf;
                stepbig(end+1) =            inf;
            end
        %catch me;
        %    continue; end;

        k = k + 1;
        
        
        
        
        f = gcf;
        p = uipanel( 'Parent',f, 'BorderType','none' ); 
        titlestring = [what ' N:' num2str(N) ' dim: ' num2str(dim) ' Opt: ' OPT ' time: ' datestr(now,'yyyy-mm-dd-HH-MM-SS')];
        p.Title = titlestring;
        p.TitlePosition = 'centertop';
        p.FontSize = 12;
        p.FontWeight = 'bold'; 

        subplot( 6, 2, [1 2], 'Parent',p ); 
        semilogy( [treetime;
                   median( treetime ) * ones( size(treetime) );
                   mean( treetime ) *  ones( size(treetime) )]' );
        title(['treetime: ' num2str( median(treetime), 2 ) 's, ' num2str( mean(treetime), 2 ) 's ( ' num2str( min(treetime) ) ' - ' num2str( max(treetime) ) ' )']);
        if( treetime(end) > 1.4*median(treetime) );
            text( length(treetime), median(treetime), num2str(seed) ); end;

        subplot( 6, 2, [3 4], 'Parent',p ); 
        semilogy( [numberofvertex;
                   median( numberofvertex ) * ones( size(numberofvertex) );
                   mean( numberofvertex ) * ones( size(numberofvertex) )]' )       % plot using + markers
        title( ['numberofvertices: ' num2str( median(numberofvertex), 2 ) ', ' num2str( mean(numberofvertex), 2 ) ' ( ' num2str( min(numberofvertex) ) ' - ' num2str( max(numberofvertex) ) ' )']);

        subplot( 6, 2, [5 6], 'Parent',p );    
        semilogy( [treedepth;
                   median( treedepth ) * ones( size(treedepth) );
                   mean( treedepth ) * ones( size(treedepth) )]' );
        title( ['treedepth: ' num2str( median(treedepth, 2) ) ', ' num2str( mean(treedepth), 2 ) ' ( ' num2str( min(treedepth) ) ' - ' num2str(max(treedepth)) ' )']);

        subplot( 6, 2, [7 8], 'Parent',p );
        semilogy( [lengthofcandidates;
                   median( lengthofcandidates ) * ones( size(lengthofcandidates) );
                   mean( lengthofcandidates ) * ones( size(lengthofcandidates) )]' );
        title( ['lengthofcandidates: ' num2str( median(lengthofcandidates), 2 ) ', ' num2str( mean(lengthofcandidates), 2 ) ' ( ' num2str( min(lengthofcandidates) ) ' - ' num2str( max(lengthofcandidates) ) ' )']);
        
        subplot( 6, 2, [9 10], 'Parent',p );
        semilogy( [stepbig;
                   median(stepbig)*ones(size(stepbig));
                   mean(stepbig)*ones(size(stepbig))]')
        title( ['stepbig: ' num2str( median(stepbig), 2 ) ', ' num2str( mean(stepbig), 2 ) ' ( ' num2str( min(stepbig) ) ' - ' num2str( max(stepbig) ) ' )']);

        subplot( 6, 2, 11, 'Parent',p );
        semilogy( [JSR;
                   median( JSR ) * ones( size(JSR) );
                   mean( JSR ) * ones( size(JSR) )]' );
        title( ['JSR: ' num2str( median(JSR), 2 ) ', ' num2str( mean(JSR), 2 ) ' ( ' num2str( min(JSR) ) ' - ' num2str( max(JSR) ) ' )']);

        subplot( 6, 2, 12, 'Parent',p ); 
        semilogy( [errorcode;
                   median( errorcode ) * ones( size(errorcode) );
                   mean( errorcode ) * ones( size(errorcode) )]' );
        title( 'errorcode: ' );

        drawnow;

        end;

    median( treetime )
    median( numberofvertex )
    median( treedepth )
    median( errorcode )
    
    if( restart );
        dim = dim*2;
        %N = N+2;
    else;
        break; end;
    
end

end


  

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*AGROW>
    