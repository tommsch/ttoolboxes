    fprintf( 2, 'This demo is not tested regularly, and may contain code which does not work with the current version.\n' );
if( ~exist('ct','var') ); ct = 0; end;

clc
while( true );
    ct = ct + 1;
    rng( ct )
    [G, co] = gallery_graph( 'cube',16, 'oriented',1, 'reorder',1 );
%     G = gallery_graph( 'complete',20, 'oriented',1, 'sparse',.2, 'reorder',1, 'connected',1 );
    
%     if( nnz(G) >= 1000 );
%         continue; end;
%     if( nnz(G) >= 15  );
%         break; end; 
      break;
end;
ct

if( ~isempty(co) );
    co = {'NodeLabel',{}, 'XData',co(1,:),'YData',co(2,:)};
else;
    co = {'NodeLabel',{}}; end;

fprintf( 'Size of graph: %i\nNonzero entries: %i\n', size(G, 1), nnz(G) );

subplot_lazy(1);
ph = plot( digraph(G), co{:} );
tic;
A_1 = mas1( G );
time_1 = toc;
A_1m = make_maximal( G, A_1 );
highlight( ph, digraph( A_1m ), 'EdgeColor','red' );
title( 'Random greedy algorithm' );
assert( digraph(A_1m).isdag );
drawnow;

subplot_lazy(2);
ph = plot( digraph(G), co{:} );
tic;
A_p = mas_perron( G );
A_p = logical( A_p );
time_p = toc;
A_pm = make_maximal( G, A_p );
highlight( ph, digraph( A_pm ), 'EdgeColor','red' );
title( 'Eigenvector algorithm' );
assert( digraph(A_pm).isdag );
drawnow;

 subplot_lazy(3);
 ph = plot( digraph(G), co{:} );
 tic;
 A_2 = mas2( G );
 time_2 = toc;
 A_2m = make_maximal( G, A_2 );
 highlight( ph, digraph( A_2m ), 'EdgeColor','red' );
 title( 'MAS' );
 assert( digraph(A_2m).isdag );
 drawnow;

fprintf( ['      Algorithm                 |  Time   |    #Edges  |   #Edges with post greedy optimization step\n'  ...
          '      Random greedy algorithm 1 |  %.3f  | %5i      | %5i  \n' ...
          '      Random greedy algorithm 2 |  %.3f  | %5i      | %5i  \n' ...
          '      eigenvector               |  %.3f  | %5i      | %5i  \n'], ...
         time_1, nnz(A_1), nnz(A_1m), ...
         time_2, nnz(A_2), nnz(A_2m), ...
         time_p, nnz(A_p), nnz(A_pm) );

%% 

function [ A ] = make_maximal( G, A );
% function which adds vertices to A, until A is not a subgraph of larger acyclic graph

    % search vertices which have only in OR out edges
    % and add all remaining in OR out edges
    n = size( G, 1 );
    for i = 1:n;
        Ain = nnz( A(i,:) );
        Aout = nnz( A(:,i) );
        if( Ain==0 && Aout~=0 );
            A(:,i) = G(:,i);
        elseif( Ain~=0 && Aout==0 );
            A(i,:) = G(i,:);
        else;
            % do nothing
            end; end
    
    % go through all remaining edges, and test whether we can add it such that the result is still acyclic
    A_idx = find( A );
    G_idx = find( G );
    idx = setdiff( G_idx, A_idx );
    [r, c] = ind2sub( size(G), idx );
    T = A;
    for i = 1:numel( r );
        T(r(i),c(i)) = 1;
        if( digraph(T).isdag );
            A = T; 
        else;
            T(r(i),c(i)) = 0; end; end;
end

%%

function [ A, pi ] = mas1( G );
    % simple randomized algorithm which computes at least a 1/2 approximation
    S = G;
    n = size( S, 1 );
    l = 1;
    u = n;
    pi = zeros( 1, n );
    used = zeros( 1, n );
    while( true );
        i = select_vertex( used, 'random' );
        used(i) = 1;
        win = nnz( S(:,i) );
        wout = nnz( S(i,:) );
        S(:,i) = 0;
        S(i,:) = 0;
        if( win<=wout );
            pi(i) = l;
            l = l + 1;
        else;
            pi(i) = u;
            u = u - 1; end;
        if( u<l );
            break; end; end;
    
    missing = setdiff( 1:n, pi );
    assert( isempty(missing) );
    %pi(pi==0) = missing;
    %[~,piinv] = sort( pi );
    
    idx = find( G );
    [r, c] = ind2sub( size(G), idx );
    select = pi(r) < pi(c);
    A = G;
    A(idx(~select)) = 0;
end

function [ A ] = mas2( G );
    % divides the graph in 2, and runs mas1 for each part
    % Then, joins the two subgraphs again
    n = size( G, 1 );
    co = logical( randi( [0 1], [1 n] ) );
    G1 = G(co,co);
    G2 = G(~co,~co);
    A1 = mas1( G1 );
    A2 = mas1( G2 );
    A = zeros( size(G) );
    A(co,co) = A1;
    A(~co,~co) = A2;
    
    to = nnz( G(co,~co) );
    back = nnz( G(~co,co) );
    
    if( to>=back );
        A(co,~co) = G(co,~co);
    else;
        A(~co,co) = G(~co,co); end;
end

function [ i ] = select_vertex( used, method )
    switch method;
        case {'r','random'};
            idx = find( ~used );
            i = idx( randi(numel(idx)) );
        otherwise;
            fatal_error; end;
end

%%

function [ X ] = mas_perron( G, X_start )
    % uses an LP program and the leading eigenvector of the adjacancy matrix to find a local optima
    % (see: optperron)
    X = [];
    dim = size( G, 1 );

    % upper bound on size of acyclic subgraph
    val = G;
    val(1:1+size(val, 1):end) = 0;
    N = nnz( triu(val+val') );
    
    a = 0;
    b = N;
    if( nargin<=1 );
        X_start = []; end;
    if( ~isempty(X_start) );
        assert( digraph(X_start).isdag );
        X = X_start;
        a = nnz(X_start); end;
    while( true );
        m = floor( (a+b)/2 );
        epsilon = 1e-3;
        Xnew = optperron( 'X',X_start, 'ex','opt', 'dim',dim, 'min', 'supp',{G,epsilon}, 'sumlb',m, 'ub',1, 'v',-1, 'method','all,mas,nomonotone', 'zerohandling','off', 'epsilon',epsilon );
        Xnew(Xnew<epsilon*5) = 0;

        Xg = digraph( Xnew );
        ac = Xg.isdag;


        if( ac );
            X = Xnew;
            %fprintf( 'feasible: %i (%i)\n', nnz(X), m );
            a = max( [ceil((a+b)/2), a+1, nnz(X)] );
        else;
            X_start = Xnew;
            %fprintf( 'infeasible: %i (%i)\n', nnz(X), m );
            b = min( floor((a+b)/2), b-1 ); end;

        if( b-a < .45 );
            break; end; end;
    
end

%% 


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
