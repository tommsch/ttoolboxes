function [ res ] = demo_ipa_extrapath( varargin );
    fprintf( 2, 'This demo is not tested regularly, and may contain code which does not work with the current version.\n' );
    try;        
        [res, varargin] =       parsem( {'res'}, varargin, defstruct() );
        [seed, varargin] =      parsem( {'seed','rng'}, varargin, randi(100000) );
        [filename, varargin ] = parsem( 'filename', varargin, '' );

        for dim = [16:2:24];
            for k = 1:4;
                try;
                    JSRv = [];  nfov = [];
                    JSRe = [];  nfoe = [];
                    JSRee = []; nfoee = [];
                    while( true );
                        M = gallery_matrixset( 'randn', 'rho','dim',dim, 'N',2, 'seed',seed );
                        seed = seed + 1;
                        [JSRv, nfov] = ipa( M, 'maxtime',3*2^(dim/2)+2000, 'verbose',2, 'extrapath',0, 'admissiblealgorithm','r', varargin{:} );
                        if( ~isequal(nfov.log.errorcode, 250) );  % not admissible algorithm chosen
                            break; end; end;
%                     [JSRe,  nfoe] =    ipa( M, 'maxtime',3*2^(dim/2)+2000, 'verbose',2, 'extrapath',1, 'admissiblealgorithm','r', 'oo',nfov.blockvar{1}.cyclictree.ordering, varargin{:} );
                    JSRe = JSRv;
                    nfoe = nfov;
                    [JSRee, nfoee] =   ipa( M, 'maxtime',3*2^(dim/2)+2000, 'verbose',2, 'extrapath',2, 'admissiblealgorithm','r', 'oo',nfov.blockvar{1}.cyclictree.ordering, varargin{:} );
                catch me;  %#ok<NASGU>
                    % do nothing
                    end;
                    
                res = save_res( filename, M, res, JSRv, nfov, JSRe, nfoe, JSRee, nfoee );
                dumpws -overwriteall
                plot_output( res );

                end; end
    catch me;
        disp( me ); end;

end

function [ res ] = save_res( filename, M, res, JSRv, nfov, JSRe, nfoe, JSRee, nfoee );

    res.dim(end+1) = nfov.param.dim;
    res.M{end+1} = M;
    if( ~isempty(JSRv) );
        res.JSRv{end+1} =                JSRv(1);
        res.treetimev(end+1) =           nfov.log.treetime;
        res.numberofvertexv(end+1) =     nfov.log.numberofvertex;
        res.treedepthv(end+1) =          nfov.log.iteration;
        res.stepbigv(end+1) =            nfov.log.num_stepbig;
    else;
        res.JSRv{end+1} =                inf;
        res.treetimev(end+1) =           inf;
        res.numberofvertexv(end+1) =     inf;
        res.treedepthv(end+1) =          inf;
        res.stepbigv(end+1) =            inf;
        end;
    try;
        res.algv(end+1) =                nfov.blockvar{1}.cyclictree.algorithm;
    catch me;  %#ok<NASGU>
        res.algv(end+1) =                ' '; end;
        
        

    if( ~isempty(JSRe) );
        res.JSRe{end+1} =                JSRe(1);
        res.treetimee(end+1) =           nfoe.log.treetime;
        res.numberofvertexe(end+1) =     nfoe.log.numberofvertex;
        res.treedepthe(end+1) =          nfoe.log.iteration;
        res.stepbige(end+1) =            nfoe.log.num_stepbig;
    else;
        res.JSRe{end+1} =                inf;
        res.treetimee(end+1) =           inf;
        res.numberofvertexe(end+1) =     inf;
        res.treedepthe(end+1) =          inf;
        res.stepbige(end+1) =            inf;
        end        
    try;
        res.alge(end+1) =                nfoe.blockvar{1}.cyclictree.algorithm;
    catch me;  %#ok<NASGU>
        res.alge(end+1) =                ' '; end;
    
    
    if( ~isempty(JSRee) );
        res.JSRee{end+1} =                JSRee(1);
        res.treetimeee(end+1) =           nfoee.log.treetime;
        res.numberofvertexee(end+1) =     nfoee.log.numberofvertex;
        res.treedepthee(end+1) =          nfoee.log.iteration;
        res.stepbigee(end+1) =            nfoee.log.num_stepbig;
    else;
        res.JSRee{end+1} =                inf;
        res.treetimeee(end+1) =           inf;
        res.numberofvertexee(end+1) =     inf;
        res.treedepthee(end+1) =          inf;
        res.stepbigee(end+1) =            inf;
        end        
    try;
        res.algee(end+1) =                nfoee.blockvar{1}.cyclictree.algorithm;
    catch me;  %#ok<NASGU>
        res.algee(end+1) =                ' '; end;    
   
    if( ~isempty(filename) );
        save( filename, 'res' ); end;

end

function [ res ] = defstruct();
    res.dim = [];
    res.M = {};

    res.JSRv = {};
    res.treetimev = []; 
    res.numberofvertexv = [];
    res.treedepthv = [];
    res.stepbigv = [];
    res.algv = [];
    
    res.JSRe = {};
    res.treetimee = []; 
    res.numberofvertexe = [];
    res.treedepthe = [];
    res.stepbige = [];
    res.alge = [];
    
    res.JSRee = {};
    res.treetimeee = []; 
    res.numberofvertexee = [];
    res.treedepthee = [];
    res.stepbigee = [];
    res.algee = [];    
end

function plot_output( res );
%%

    func = @mean;

    dim = unique( res.dim );

    num_vertex = [];
    ratiovertex = [];
    treetime = [];
    ct = [];
    for d = dim;
        idx = res.dim == d;
        idx = idx(1:numel( res.JSRee ));

        h = 'numberofvertex';
        he = [h 'e'];
        hv = [h 'v'];
        hee = [h 'ee'];
        val = [res.(hee)(idx); res.(he)(idx); res.(hv)(idx)];
        idx_finite = all( isfinite( val ), 1 );

        val = val(:,idx_finite);
        if( isempty(idx_finite) );
            continue; end;
        num_vertex(:,d) = func( val, 2 );
        ratiovertex(:,d) = [num_vertex(1,d)/num_vertex(3,d);
                            num_vertex(2,d)/num_vertex(3,d);
                            nan];
                        
        h = 'treetime';
        he = [h 'e'];
        hv = [h 'v'];
        hee = [h 'ee'];
        val = [res.(hee)(idx); res.(he)(idx); res.(hv)(idx)];
        val = val(:,idx_finite);
        treetime(:,d) = func(val,2);
        ratiotreetime(:,d) = [treetime(1,d)/treetime(3,d);
                            treetime(2,d)/treetime(3,d);
                            nan];
        
        ct(:,d) = size( val, 2 );
    end


    subplot_lazy(1);
    semilogy( num_vertex', 'x-' )
    title( 'number of vertices' );

    subplot_lazy(2);
    plot( ratiovertex', 'x-'  )
    title( 'ratiovertex' );
    
    subplot_lazy(3);
    semilogy( treetime', 'x-'  )
    title( 'treetime' );
    
    subplot_lazy(4);
    semilogy( ratiotreetime', 'x-'  )
    title( 'ratiotreetime' );

    %plot( ct', 'x-'  )
    %title( 'ct' );     
    
    drawnow;

end

%%
function playground  %#ok<DEFNU>
%%
res.JSRee =                [];
res.treetimere =           [];
res.numberofvertexee =     [];
res.treedepthee =          [];
res.stepbigee =            [];
%%
    for r = 1:numel(res.dim)  
        dim = res.dim(r);
        while( true );
            M = gallery_matrixset( 'randn', 'rho','dim',dim, 'N',2 );
            [JSRv, nfov] = ipa( M, 'maxtime',3*2^(dim/1.5)+2000, 'verbose',2, 'extrapath',0, 'admissiblealgorithm','r' );
            if( isequal(nfov.log.errorcode, 250) );  % not admissible algorithm chosen
                continue; end;
            break; end;

        if( ~isempty(JSRv) && numel(JSRv) == 1 );
            res.JSRv(r) =                JSRv(1);
            res.treetimev(r) =           nfov.log.treetime;
            res.numberofvertexv(r) =     nfov.log.numberofvertex;
            res.treedepthv(r) =          nfov.log.iteration;
            res.stepbigv(r) =            nfov.log.num_stepbig;
        else;
            res.JSRv(r) =                inf;
            res.treetimev(r) =           inf;
            res.numberofvertexv(r) =     inf;
            res.treedepthv(r) =          inf;
            res.stepbigv(r) =            inf; end; 
    
        plot_output( res ); end;
%%    
end

%%
  

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*AGROW>
%#ok<*NBRAK2,*NBRAK1,*NBRAK>
