B=cell([1 2]);
B{1}=[2 3 6;4 1 8;0 0 14];
B{2}=[-1 -1 0;1 -1 0;0 0 1.4142135623730951];

center = [0 0 1]';

figure( 1 )
[~,nfo_B] = ipa( B, 'Kone', 'add','prune', 'stale_norm',inf, 'center',[0 0 1]', 'sym',1 )

%%
clf
hold on
K = {center};
for i = 2:20
    
    K{i} = [B{1}*K{i-1} B{2}*K{i-1} K{i-1}];
    K{i} = normalizematrix( K{i} );
    nrm = polytopenorm( K{i}, K{i}, 'kone', 'output','ub', 'v',0 );
    K{i}(:,nrm<1) = [];

end

for i = numel( K ):-1:3
    plotm( K{i}, 'Kone', 'center',center, 'hull', 'patch',1, 'color',[i i i]/numel(K) )
end
axis equal
%alpha .5

plotm( center, 'Kone', 'center',center, 'ok', 'ms',5 )
plotm( [nfo_B.blockvar{1}.cyclictree.VV{:}], 'Kone', 'hull', 'center',center, 'k-', 'linewidth',1.5 )
plotm( [nfo_B.blockvar{1}.cyclictree.VV{:}], 'Kone', 'hull', 'center',center, 'w--', 'linewidth',1 )
plotm( [nfo_B.blockvar{1}.cyclictree.v0{:}], 'Kone', 'center',center, 'k.', 'ms',10 )
plotm( [nfo_B.blockvar{1}.cyclictree.v0{:}], 'Kone', 'center',center, 'ko', 'ms',5 )
figure( 1 )

exportfig.AXISE( [-1.2 1.2] )
exportfig.TICKS()
exportfig.LABEL()
exportfig.SZE(2,2)
exportfig.SAVE( 'direct_algorithm', [], '-png' )
