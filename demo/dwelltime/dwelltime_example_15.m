function [ res ] = dwelltime_example15( d_all, metzlerflag, res, varargin );
% Script for the Example 15 from the paper
%d_all = [2 3 4 5 6 7 8 9];  [2 5 13 34 89 233 377]
    assert( nargout == 1 );
    [opt.debug] = parsem_fast( {'debug'}, varargin );
    if( nargin <= 2 || isempty(res) );
        res = struct.empty( 0, 1 ); end;
    if( nargin <= 1 || isempty(metzlerflag) );
        metzlerflag = false; end;
    
    G = [0 0 1 1;1 1 0 0;1 1 0 0;0 0 1 1];
    
    ipa_args = {'maxnormerror',inf, 'maxtime',60*30, 'maxnumvertex',10000, 'maxremainingvertex',200000, 'maxiteration',500, 'sym',0, 'balancing',0, 'admissiblealgorithm','pr', 'maxstalenorm',100, 'testeigenplane',false, 'debug',opt.debug};
    
    for d = d_all;
    for ii = 1:11;
        n = numel( res ) + 1;
        A0 = gallery_matrixset( 'N',2, 'dim',d, 'randn' );
        if( metzlerflag )
            A0 = randomize_matrixset( A0, 'metzler' ); end;
        A = A0;
        A{1} = A{1}/norm( A{1} );
        A{2} = A{2}/norm( A{2} );
        dwell(n) = rand;
        
        range_max = inf;
        hs = linspace( 0, dwell(n), 7 );
        hs = hs( end-1:-1:2 );
        for epsilon = [0 0.01];
        for h = hs;
            w = [dwell(n) h dwell(n) h];
            T = {expm( A{2}*w(1) ), expm( A{1}*w(2) ), expm( A{1}*w(3) ), expm( A{2}*w(4) )};
            tic;
            [r, nfo_ipa] = ipa( T, G, 'toa',w, ipa_args{:}, varargin{:} );
            if( contains( nfo_ipa.blockvar{1}.cyclictree.algorithm, 'c' ) );
                continue; end;
            Pt = get_polytope( nfo_ipa );
            sigma_h = max( log( r ) );
            for i = 1:2;
                assert( numel(nfo_ipa.blockvar) == 1 );
                nrm(i) = polytopenorm( (A{i} - sigma_h*eye(d))^2, Pt{i}, 'mat', nfo_ipa.blockvar{1}.cyclictree.algorithm, 'output','ub', 'debug',opt.debug ); end;
            lb_j = log( min(r) );
            ub_j = log( max(r) ) + 1./dwell(n) .* log( 1 ./ (1 -  max( nrm(:) )./8.*h.^2) );
            range_j = ub_j - lb_j;
            time_j = toc;
            if( range_j < range_max );
                range_max = range_j;
                res(n).epsilon = epsilon;
                res(n).dim = d;
                res(n).A0 = A0;
                res(n).A = A;
                res(n).dwell = dwell(n);
                res(n).h = h;
                res(n).T = T;
                res(n).lb = lb_j;
                res(n).ub = ub_j;
                res(n).nrm = nrm;
                res(n).time = time_j;
                res(n).nfo_ipa = nfo_ipa;
                print_results( res, d_all );
                clear nfo_ipa Pt
                %save example3.mat
                end; end; end; end; end;
end

%%
function print_results( res, d_all );
%%
    fid = -1;
    if( exist( 'd_all', 'var' ) );
        filename = ['dwelltime_example15-' num2str(d_all)];
        save_close = onCleanup( @() save( [filename '.mat'], 'res' ) );
        fid = fopen( [filename '.txt'], 'a+');
        if( fid ~= -1 );
            fid_close = onCleanup( @() fclose(fid) ); end; end;
    vprintf( '\n\n  $dim$ & $\\text{ub}-\\text{lb}$ & t   \\\\  %% number of examples \n\\hline\n' );
    dims = unique( [res.dim] );
    for d = dims;
        idx = [res.dim] == d;
        if( ~any(idx) );
            continue; end;
        
        lb = log( [res(idx).lb] );
        ub = log( [res(idx).ub] );
        diff = ub - lb;
        cp_idx = imag( lb ) ~= 0 | imag( ub ) ~= 0;
        diff( cp_idx ) = [];
        time = [res(idx).time];
        str = vprintf( '$ %3i $ & $ [%3.1e - %3.1e \\pm %3.1e - %3.1e]$ & $ %6.1fs $ \\\\  %% %i \n', ...
                       d, ...
                       min(diff), mean( diff ), std( diff, 1 ), max(diff), ...
                       median( time ), ...
                       nnz(idx) ...
                     );
        if( fid ~= -1 );
            fprintf( fid, '%s', str ); end; end;
    if( fid ~= -1 );
        fprintf( fid, '\n======================\n' ); end;
end

%%
function [ P ] = get_polytope( nfo )
    assert( numel( nfo.blockvar ) == 1 );
    %assert( nfo.blockvar{1}.cyclictree.num_component == 2 );

    P = cell( 1, 2 );
    nrm = [nfo.blockvar{1}.cyclictree.norm{:}];
    nrm = nrm(2,:);
    idx_comp = [nfo.blockvar{1}.cyclictree.component_idx{:}];
    VV = [nfo.blockvar{1}.cyclictree.VV{:}];
    for i = 1:numel( P );
        %P{i} = VV(:,idx_comp == i & nrm >= 1);
        P{i} = VV(:,idx_comp == i);
   end
end



function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*NOPTS>
%#ok<*UNRCH>
%#ok<*AGROW>
