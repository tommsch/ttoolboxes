A = {1/3*[-2 0;-2 -1], 1/3*[-3 -2;0 -2]};
h = 0.1;  % discretization step
dwell = 1;  % m
G = [0 0 1 1;1 1 0 0;1 1 0 0;0 0 1 1];

w = [dwell h dwell h];
T = {expm( A{2}*w(1) ), expm( A{1}*w(2) ), expm( A{1}*w(3) ), expm( A{2}*w(4) )};
[r, nfo_ipa] = ipa( T, G, 'toa',w, 'sym',0, 'balancing',0, 'admissiblealgorithm','rp', 'plot','polytope,allpoints,images,plane', 'maxstalenorm',200, 'testeigenplane',false, 'testrho',true, 'ncdelta',1, 'maxonenorm',inf, 'delta',0.995 );

Pt = get_polytope( nfo_ipa );
sigma_h = max( log( r ) );
dim = size( T{1}, 1 );
for i = 1:2;
    assert( numel(nfo_ipa.blockvar) == 1 );
    nrmP(1:3,i) = polytopenorm( (A{i} - sigma_h*eye(dim))^2, Pt{i}, 'mat', nfo_ipa.blockvar{1}.cyclictree.algorithm ); end;  %#ok<SAGROW>
lb = log( min(r) )
ub = log( max(r) ) + 1/dwell * log( 1 / (1 - max(nrmP(2,:))/8*h^2) )

%%
% this code needs to exportfig wrapper package
% it is only used for printing out the plot from the paper
% write an email to tommsch@gmx.at to obtain it

col_blue = [0.001 0.4470 0.7410];
col_red = [0.8500 0.3250 0.0980];
col_blue_light = col_blue.^.5;
col_red_light = col_red.^.5;
col_blue_white = col_blue.^.02;
col_red_white = col_red.^.05;

import exportfig.*

figure(3); clf; 
FONTSIZE(9)
axes; hold on;
plotm( Pt{1}, '-', 'funct','r', 'hull',1, 'linewidth',2, 'color',col_blue_light ); plotm( Pt{1}, '--', 'funct','r', 'hull',1, 'linewidth',1.5, 'color',col_blue_white );
plotm( T{3}*Pt{2}/max(r), '-', 'funct','r', 'hull',1, 'color',col_red_white );     plotm( T{3}*Pt{2}/max(r), '-.', 'funct','r', 'hull',1, 'color',col_red )
plotm( T{2}*Pt{1}/max(r), '-', 'funct','r', 'hull',1, 'color',col_blue_white );    plotm( T{2}*Pt{1}/max(r), '--', 'funct','r', 'hull',1, 'color',col_blue )

AXIS( [-1.1 1.1 -1.3 1.3] );
TICKS( [-1  0 1],[ -1  0  1 ] );
SZE( 1,7, 4,4 );
exportfig.TEXT( -.6, 0, '$P_1$', 'Color',col_blue_light, 'FontWeight','bold' )
lne_blue = [-.7 -.3;-.15 -.15]; plotm( lne_blue, 'linewidth',2, 'color',col_blue_light ); plotm( lne_blue, '--', 'linewidth',1.5, 'color',col_blue_white );
exportfig.TEXT( .5, -.5, '$e^{(m-\sigma_h)A_1}P_2$', 'Color',col_red, 'FontWeight','bold' )
%lne_red = [.5 1.1;-.65 -.65]; plotm( lne_red, 'color',col_red_white ); plotm( lne_red, '-.', 'color',col_red_light );
exportfig.TEXT( .2, .5, '$e^{(h-\sigma_h)A_1}P_1$', 'Color',col_blue, 'FontWeight','bold' )
LABEL( '', '' );
POST();

axes; hold on;
plotm( Pt{2}, '-', 'funct','r', 'hull',1, 'linewidth',2, 'color',col_red_light ); plotm( Pt{2}, '-.', 'funct','r', 'hull',1, 'linewidth',1.5, 'color',col_red_white );
plotm( T{1}*Pt{1}/max(r), '-', 'funct','r', 'hull',1, 'color',col_blue_white );   plotm( T{1}*Pt{1}/max(r), '--', 'funct','r', 'hull',1, 'color',col_blue )
plotm( T{4}*Pt{2}/max(r), '-', 'funct','r', 'hull',1, 'color',col_red_white );    plotm( T{4}*Pt{2}/max(r), '-.', 'funct','r', 'hull',1, 'color',col_red )
AXIS( [-1.1 1.1 -1.3 1.3] );
TICKS( [-1  0 1], [-1  0 1] );
SZE( 7,7, 4,4 )
exportfig.TEXT( -.9, .5, '$P_2$', 'Color',col_red_light, 'FontWeight','bold' )
lne_red = [-1 -.6;.35 .35]; plotm( lne_red, 'linewidth',2, 'color',col_red_light ); plotm( lne_red, '-.', 'linewidth',1.5, 'color',col_red_white );
exportfig.TEXT( 0, .4, '$e^{(m-\sigma_h)A_2}P_1$', 'Color',col_blue, 'FontWeight','bold' )
exportfig.TEXT( -.9, -.5, '$e^{(h-\sigma_h)A_2}P_2$', 'Color',col_red, 'FontWeight','bold' )
LABEL( '', '' );
POST();

%SAVE( 'example16.pdf' )

%%
function [ P ] = get_polytope( nfo )
    assert( numel( nfo.blockvar ) == 1 );
    assert( nfo.blockvar{1}.cyclictree.num_component == 2 );
    P = cell( 1, 2 );
    nrm = [nfo.blockvar{1}.cyclictree.norm{:}];
    nrm = nrm(2,:);
    idx_comp = [nfo.blockvar{1}.cyclictree.component_idx{:}];
    VV = [nfo.blockvar{1}.cyclictree.VV{:}];
    for i = 1:numel( P );
        %P{i} = VV(:,idx_comp == i & nrm >= 1);
        P{i} = VV(:,idx_comp == i);
   end
end
%%


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*NOPTS>
