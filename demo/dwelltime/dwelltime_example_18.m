% For each step h, we give the lower and upper bounds for \sigma
% and also (not necessary but interesting)
% the switching law corresponding to the maximal growth.
% Each h gives its own switching law and we can compare them.

A = {[-1 -1  1 -1;1 -1 -1 -1; 1 1 -1 -1;1 -1 1 -1], ...
     [-1 -1 -1 -1;1 -1  1  1;-1 1 -1 -1;1 -1 1  1]};

condset( 'r', {} )
dim = size( A{1}, 1 );
G = [0 0 1 1;1 1 0 0;1 1 0 0;0 0 1 1];
dwell = 0.5;  % m, M
epsilon = 0.00;
delta = 1/(1+epsilon);
findsmp_args = {'v',2, 'maxsmpdepth',20, 'N',10000};
ipa_args = {'epsilon',epsilon, 'maxnormerror',inf, 'maxtime',60*30, 'maxnumvertex',5000, 'maxremainingvertex',20000, 'sym',0, 'balancing',0, 'maxsmpdepth',100, 'findsmp_N',5000, 'admissiblealgorithm','rp', 'stale_norm',50};


H = [0.5 0.4 0.3 0.2 0.1 0.05 0.025];
for h = H;
    [rr, dd] = rat( h );
    if( any( size(r) < [rr,dd] ) || ...
        isempty(r{rr,dd}) || ...
        ~isempty(r{rr,dd}) && min(r{rr,dd})/max(r{rr,dd}) < delta ...
      );
        fprintf( '\n\nh = %3i/%3i = %5.5f\n', rr, dd, rr/dd );
        w = [dwell h dwell h];
        T = {expm( A{2}*w(1) ), expm( A{1}*w(2) ), expm( A{1}*w(3) ), expm( A{2}*w(4) )};

    %     [c{idx}, nc{idx}, nfo_findsmp{idx}] = findsmp( T, G, 'weight', w, findsmp_args{:} );  %#ok<SAGROW>
        [r{rr,dd}, nfo_ipa{rr,dd}] = ipa( T, G, 'toa',w, ipa_args{:} );  %#ok<SAGROW>
        if(  any( nfo_ipa{rr,dd}.blockvar{1}.cyclictree.algorithm == 'c' ) );
            continue; end;

        Pt = ipa_nfo( nfo_ipa{rr,dd}, 'polytope' );
        %Pt = get_polytope( nfo_ipa{rr,dd} );
        sigma_h = max( log( r{rr,dd} ) );
        for i = 1:2;
            assert( numel(nfo_ipa{rr,dd}.blockvar) == 1 );
            nrm{rr,dd}(1:3,i) = polytopenorm( (A{i} - sigma_h*eye(dim))^2, Pt{i}, 'mat', nfo_ipa{rr,dd}.blockvar{1}.cyclictree.algorithm ); end; end;  %#ok<SAGROW>

    print_orderings( r, dwell, nfo_ipa, nrm, H, w )
end
%%
function [ P ] = get_polytope( nfo )
    assert( numel( nfo.blockvar ) == 1 );
    assert( nfo.blockvar{1}.cyclictree.num_component == 2 );
    P = cell( 1, 2 );
    nrm = [nfo.blockvar{1}.cyclictree.norm{:}];
    nrm = nrm(2,:);
    idx_comp = [nfo.blockvar{1}.cyclictree.component_idx{:}];
    VV = [nfo.blockvar{1}.cyclictree.VV{:}];
    for i = 1:numel( P );
        P{i} = VV(:,idx_comp == i & nrm >= 1);
   end
end

function print_orderings( r, dwell, nfo_ipa, nrm, H, w )
    %%
    vprintf( '\n\n$h$  & $\\varepsilon$     &   {lb}  &         {ub}          & \\# $V$   & {ordering}\\\\  %% eps & norm \n\\hline\n' );
    data_eb = [];
    data_pm = [];
    data_ud = [];
    for h = H;
        [rr, dd] = rat( h );
        if(  any( size(r) < [rr,dd] ) || ...
             isempty(nfo_ipa{rr,dd}) || ...
             any( nfo_ipa{rr,dd}.blockvar{1}.cyclictree.algorithm == 'c' ) ...
          );
            continue; end;
        lb = clamp( log( min(r{rr,dd}) ), 0, inf );  % log( max(r{rr,dd}) ); ??
        ub = log( max(r{rr,dd}) ) + 1/dwell * log( 1 / (1 - max(nrm{rr,dd}(:))/8*h^2) );  % log( max(r{rr,dd}) ) + 1/dwell * log( max(nrm{rr,dd}(:)) / (1 - max(nrm{rr,dd}(:))/8*h^2) )
        data_eb = [data_eb [h; (lb+ub)/2; (ub-lb)/2]];
        data_pm = [data_pm [h lb; h ub; nan nan]'];
        data_ud = [data_ud [h; lb; ub]];
        delta = min( r{rr,dd}/max(r{rr,dd}) ); eps = 1/delta - 1;
        %Pt = get_polytope( nfo_ipa{rr,dd} );
        Pt = ipa_nfo( nfo_ipa{rr,dd}, 'polytope' );
        if( eps == 0 );
            o = pretty_ordering( nfo_ipa{rr,dd}.blockvar{1}.cyclictree.ordering{1}', w );
        else;
            o = '?'; end;
        vprintf( '$ %5f $&$ %18.15f $&$ %8.4f $&$ %s $ \\\\ %% %5i & %i & %5.3f \n', ...
                 rr/dd, ...
                 lb, ub, ...
                 o, ...
                 max( cellfun( 'prodofsize', Pt ) ), eps, max( nrm{rr,dd}(:) ) ...
               );
    end

    clf; hold on;
    %errorbar( data_eb(1,:), data_eb(2,:), data_eb(3,:), 'LineStyle','none' )
    %plotm( data_pm, '-', 'linewidth',1 )
    plotm( data_ud([1 2],:) )
    plotm( data_ud([1 3],:) )
    plotm( 'loglog' )
    axis( [0.01 .8 0.02 30] )
    
end

function [ str ] = pretty_ordering( o, w );
    %%
    nme = ['baab';'____';'mhmh'];
    nme_last = 0;
    str = '(';
    len = 0;
    for i = 1:numel( o );
        if( nme(o(i)) ~= nme_last );
            nme_last = nme(o(i));
            if( len > 0 );
                str = [str num2str(len) '; ' ]; end;  %#ok<AGROW>
            len = 0;
        end;
        len = len + w(o(i)); end;
    str = [str num2str(len) ')'];
end



function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*NOPTS>
