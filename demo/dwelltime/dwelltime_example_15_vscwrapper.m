function dwelltime_example15_wrapper( varargin );

	cd( getenv('DATA') ) 
	
	cd matlab
	
	cd ttoolboxes
	ttoolboxes install
	cd ..

	cd ttest
	TTEST install --unattended
	cd ..

	cd ttoolboxes
	cd demo
	cd dwelltime
	dwelltime_example15( varargin{:} )
end