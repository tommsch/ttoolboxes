
clear
%%
condset( 'c_bf', {} );
G = [0 0 1 1;1 1 0 0;1 1 0 0;0 0 1 1];
for d = [2 3 4 5 7 8 11 13 17 21];
    for ii = 1:5;
        n = numel( c_bf ) + 1;
        dim(n) = d;
        A0 = gallery_matrixset( 'N',2, 'dim',5, 'integer', [-9 9] );
        A1 = randomize_matrixset( A0, 'metzler' )
        A = cell( 1, 2 );
        A{1} = A1{1}/norm( A1{1}, 1 );
        A{2} = A1{2}/norm( A1{2}, 1 );
        dwell = rand;
        h = rand * dwell;
        w = [dwell h dwell h];
        T{n} = {expm( A{2}*w(1) ), expm( A{1}*w(2) ), expm( A{1}*w(3) ), expm( A{2}*w(4) )};

        tic;
        [c, ~, nfo] = findsmp( T{n}, G, 'toa',w, 'modgrip', 'maxsmpdepth',inf, 'maxtime',10, 'delta',1, 'v',1, 'nearlycanddelta',1 );
        len = cellfun('prodofsize', c );
        t_mg(n) = toc;
        jsr_mg(n) = nfo.jsrbound(1);
        len_mg(1:2,n) = [min(len);max(len)];
        c_mg{n} = c;

        tic;
        [c, ~, nfo] = findsmp( T{n}, G, 'toa',w, 'bruteforce', 'maxtime',10, 'maxsmpdepth',inf, 'v',1 );
        len = cellfun('prodofsize', c );
        t_bf(n) = toc;
        jsr_bf(n) = nfo.jsrbound(1);
        len_bf(1:2,n) = [min(len);max(len)];
        c_bf{n} = c;

        subplot_lazy(1); plot( [jsr_bf; jsr_mg]' )
        subplot_lazy(2); semilogy( [t_bf; t_mg]' )
        legend( 'bf', 'mg' )
        subplot_lazy(3);
        plot( [min(len_mg, [], 1) - min(len_bf, [], 1)]' );  %#ok<NBRAK1>
        drawnow;

        print_results( jsr_mg, jsr_bf, t_mg, t_bf, dim );
        end; end;

%%
function print_results( jsr_mg, jsr_bf, t_mg, t_bf, dim )
%%
    vprintf( '\n\n$dim$ & mg better  &   bf better  & $t_{mg}$   & $t_{bf}$  \\\\\n\\hline\n' );
    for d = unique( dim )
        idx = dim(1:numel(t_bf)) == d;
        if( ~any(idx) );
            continue; end;
        sz = nnz( idx );
        vprintf( '$ %2i  $ & $ %4.0f\\%% $ & $ %4.0f\\%% $ & $ %4.1fs $ & $ %4.1fs $\\\\\n', ...
                 d, ...
                 100*nnz( jsr_mg(idx) > jsr_bf(idx) )/sz, 100*nnz( jsr_bf(idx) > jsr_mg(idx) )/sz,  ...
                 median(t_mg(idx)), median(t_bf(idx)) ...
               )
    end
end

%%



function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*NOPRT>
%#ok<*AGROW>
%#ok<*ASGLU>
%#ok<*NOPTS>
%#ok<*SAGROW>
