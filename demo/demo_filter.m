clc
    fprintf( 2, 'This demo is not tested regularly, and may contain code which does not work with the current version.\n' );
condset( 'ct', 0 )
ct = ct + 1
rng(ct)
fact = 4;

m = [3 101];

fs = 44100;
f = 880;
len = 2;
in = gallery_signal( 'handel', 'len',len, 'fs',44100 );
%for i = 1:3;
%    in = in + rand/i^2*gallery_signal( 'fm', [100 10 5000 5], 'len',2, 'fs',44100 ); end;
%in = in + gallery_signal( 'sin' );
soundsc( in, fs )

figure(1);
clf; 
subplot_lazy(1);
pspectrum( interp(in, fact), fact*fs, 'spectrogram','TimeResolution',0.1, 'OverlapPercent',99,'Leakage',0.85, 'FrequencyLimits',[1 20000])

figure(2);
clf; hold on;
subplot_lazy(1);
plot( interp(in(2*max(m):2*max(m)+1500), fact), 'b.-' )
subplot_lazy(2); hold on;
plot( interp(in, fact), 'b.-' )

drawnow;

clear A
A{1} = statematrix( m, 'orth' );
A{2} = statematrix( m, 'orth' );
%A = gallery_matrixset( 'signal', 'N',5 );

N = numel( m );
%r = ipa( A, 'delta',.95, 'sym',0, 'v',1 );

A = A./max( rho(A) ).*.99;
sz = size( A{1}, 1 );
%A = {.9 .9};



ga = [zeros( sz - 2*N, 1 ); ones( N, 1 ); zeros( N, 1 )];
be = [zeros( sz - N, 1 ); ones( N, 1 )];
d = 0;
%c = zeros( N, 1 ); c(1) = 1;
%b = ones( N, 1 );
%d = 0;
state = zeros( sz, 1 );
out = zeros( size(in) );

method = 'ct';
t = 0;
diff = [];
for n = 1:numel( in );
    switch method;
        case '1';
            An = A{1};
        case 'ct';
            %diff = sin(n/50000)/1000000;
            diff = rand/10000 + randn/100;
            t(n+1) = clamp( t(n) + diff, 0 , 1 );  %#ok<SAGROW>
            An = A{1}*t(n+1) + A{2}*(1-t(n+1));
        case 'rand';
            An = A{randi(2)};
        otherwise;
            fatal_error; end;
    
    
    % y = out = y
    % w = state = w
    % x = in = x
    out(:,n)     = ga'*state(:,n) + d*in(:,n);
    state(:,n+1) = An*state(:,n)  + be*in(:,n);
    
    end
    
imagesc( An >= 0 )

figure(1);
subplot_lazy( 2 );
pspectrum( interp(out, fact), fact*fs, 'spectrogram','TimeResolution',0.1, 'OverlapPercent',99,'Leakage',0.85, 'FrequencyLimits',[10 20000] )

figure(2);
%clf; hold on;
subplot_lazy(1);
plot( interp(out(2*max(m):2*max(m)+1500), fact), 'r.-' )
subplot_lazy(2); hold on;
plot( interp(out, fact), 'r.-' )
%plot( interp(in, fact), 'b.-' )

soundsc( in, fs )
pause( len+.5 )
soundsc( out, fs )

%%

clc; clf; hold on;
dim = 20;
N = gallery_matrixset( 'fiedler', 'dim',dim, 'N',2 );
J = numel( N );

for j = 1:J;
    plotm( eig(N{j}).', 'r.', 'boundary',1, 'reim', 'color',num2color(j), 'ms',10 ); end;

C = @(t) [sin(t);cos(t)];
plotm( C(0:.001:2*pi+.1), 'k-' );
%axis( [-1.1 1.1 -1.1 1.1] )
axis equal


for t = 0:.01:1;
        X = N{1}*t + N{2}*(1-t);
        e = eig( X );
        e = [e; conj(e)].';
        plot( real(e), imag(e), 'b.' );
        drawnow;
end


%sum(N(1,:).*N(2,:))

%%
clc; clf; hold on;
dim = 10;
T = gallery_matrixset( 'nilpotent', 'dim',dim, 'N',2 );
J = numel( T );
m = max( rho(T) );
if( m>0 );
    T = T./m; end;
%r = ipa( T, 'delta',.999 );
%N = N./max( r );

for j = 1:J;
    plotm( eig(T{j}).', 'r.', 'reim', 'color',num2color(j), 'ms',10 );
    plotm( eig(T{j}).', 'r.', 'boundary',1, 'reim', 'color',num2color(j), 'ms',10 ); end;

C = @(t) [sin(t);cos(t)];
plotm( C(0:.001:2*pi+.1), 'k-' );
%axis( [-1.1 1.1 -1.1 1.1] )
axis equal

for i = 1:30;
    for t = 1:50;
        len = t;
        X = buildproduct_fast( T, randi([1 numel(N)], len) );
        e = eig( X );
        e = log(abs(e)+1).*exp(1i*angle(e));
        e = [e; conj(e)].';
        plot( real(e), imag(e), 'b.' );
    end
    drawnow
end


%sum(N(1,:).*N(2,:))



function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*NBRAK>