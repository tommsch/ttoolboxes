function [S1, S2, S] = demo_checktile()
% [ S1, S2, S ] = demo_checktile()
% Checks if the area of attractors is one
% Tries to find two subdivision schemes S1,S2 whose tile area is one.
% but the scheme with ordering (S2_S1)^n has tile area bigger then one.
%
% Tests are rigorous.
%
% See also: tilearea, checktile,
%
% Written by: tommsch, 2018

 %#ok<*NOPRT>
 
     fprintf( 2, 'This demo is not tested regularly, and may contain code which does not work with the current version.\n' );
 
if(nargout~=3);
    vprintf('Function needs three output arguments.\n','cpr','err'); end;

while(true)
    
    %M1 = randi( 5 ) + 1;
    %D1 = constructdigit (M1, 'random',3 );
    %S1 = getS( {[],M1,D1} );
    S1=getS('2_rand');
    flag1=checktile(S1,'verbose',0);
    if(flag1 == 1 || isnan(flag1)); continue; end;
    fprintf('.');
    %M2=randi(5)+1;
    %D2=constructdigit(M2,'random',3);
    %S2=getS({[],M2,D2});
    S2=getS('2_rand');
    flag2=checktile(S2,'verbose',0);
    if(flag2 == 1 || isnan(flag2)); continue; end;
    fprintf(',');
    
    S=multiplyS(S1,S2);
    if(rho(S{2}^(-1))>1-100*eps); 
        fprintf(' :( ');
        continue; 
    end;
    fprintf(';');
    
    flag = checktile(S,'verbose',0);
    fprintf('%i',flag);
    
    if(isequal(flag,1)); break; end;
end
figure;
S1{2}
S1{3}
tilearea(S1,'visual','numpoint',100000);

figure;
S2{2}
S2{3}
tilearea(S2,'visual','numpoint',100000);

figure;
S{2}
S{3}
tilearea(S,'visual','numpoint',100000);

if(nargin == 1);
    S1={S1;S2;S};
end;
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
