function [ success, time, c, nc, info, jsr, t ] = demo_findsmp;
% [ratio, H, P] = demofindsmp()
% Compares various algorithm to find s.m.p.-candidates.
% In particular: Gripenberg, modified gripenberg, random modified gripenberg, brute fore, genetic and modified invariant polytope how they perform on finding s.m.p.s..
%
% Written by: tommsch, 2017

%clf; hold on;
    fprintf( 2, 'This demo is not tested regularly, and may contain code which does not work with the current version.\n' );
    J =     8;
    dim =   8;
    N =   100;
    M =    {};
    jsr_ipa = []; jsr_mg = []; jsr_rg = []; jsr_grip = []; jsr_b = []; jsr_gen=[];
    t_ipa =   []; t_mg =   []; t_rg =   []; t_grip =   []; t_b =   [];
    for n = 1:N
        n
         while( true );
             for j = 1:J
                 M{n}{j} = rand( dim )*10-5; end;
             tic; 
             [val, type] = ipa( M{n}, 'maxtime',60*60 );
             t_ipa(n) = toc;
             if( length(val) == 1 );
                 jsr_ipa(n) = val;
                 break; end; end;
        'mg',  tic; [c_mg{n},   nc_mg{n},   info_mg{n}]   = findsmp( M{n} );                                                                t_mg(n) =   toc,  jsr_mg(n) =   info_mg{n}.jsrbound(1);
        'rg',  tic; [c_rg{n},   nc_rg{n},   info_rg{n}]   = findsmp( M{n}, 'select',2 );                                                    t_rg(n) =   toc,  jsr_rg(n) =   info_rg{n}.jsrbound(1);
        'grip',tic; [c_grip{n}, nc_g{n},    info_grip{n}] = findsmp( M{n}, 'gripenberg', 'maxsmpdepth',inf, 'maxtime',180, 'delta',.99 );   t_grip(n) = toc,  jsr_grip(n) = info_grip{n}.jsrbound(1);
        'b',   tic; [c_b{n},    nc_b{n},    info_b{n}]    = findsmp( M{n}, 'bruteforce', 'maxtime',180 );                                   t_b(n) =    toc,  jsr_b(n) =    info_b{n}.jsrbound(1);
        'gen', tic; [c_gen{n},  nc_gen{n},  info_gen{n}]  = findsmp( M{n}, 'genetic', 'popsize',1000, 'maxgen',500 );                       t_gen(n) =  toc,  jsr_gen(n) =  info_gen{n}.jsrbound(1);
    end

    c=      [[]         c_mg     c_rg     c_grip     c_b      c_gen];
    nc =    [[]         nc_mg    nc_rg    nc_grip    nc_b     nc_gen];
    info =  [type       info_mg  info_rg  info_grip  info_b   info_gen];
    jsr =   [jsr_ipa'  jsr_mg'  jsr_rg'  jsr_grip'  jsr_b'   jsr_gen'];
    t =     [t_ipa'    t_mg'    t_rg'    t_grip'    t_b'     t_gen'];

    success = jsr;
    for n = 1:N
        success(n,:) = abs( jsr(n,:) - jsr_ipa(n) ) < 10e-12; end;
    success = sum(success, 1 )./N;
    time = sum(t,1)./N;
    fprintf( 'Examine the data in debug mode.\n' );
    keyboard

end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*NOPRT>
%#ok<*AGROW>
 %#ok<*ASGLU>