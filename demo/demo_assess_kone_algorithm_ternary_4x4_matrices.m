fprintf( 'Test scripts for ternary 4x4 matrices' )

%%
counter = 0;
nfo_exact = {};
res = struct( [] );  % syntax to create an empty struct
dim = 4;
J = 2;

%%
res2 = res;
for i = numel( res2 ):-1:1;
    T = res2(i).T;
    
end

%%

% counter = max( 0, counter - 1 );
while( true );
    counter = counter + 1  %#ok<NOPTS>
    rng( counter )
    [T, ~, nfo_T] = gallery_matrixset( 'dim',dim, 'J',J, 'ternary', 'different', 'filter',{'vandergraft',true, 'unitary','no', 'finite','all'} );
    %vdisp( T )
    if( num < counter );
        continue; end;
    if( numel(T) < 2 );
        continue; end;
    
    TT = invariantsubspace( T, 'all' );
    if( numel(TT) >= 2 );
        continue; end;
        
    [pd_ret, K, Ks, dmin] = primal_dual_kone( T );    
    if( ~isfinite(pd_ret) || isequal(pd_ret, false) );
        continue; end;

    tic;
    [~, nfo_exact] = ipa( T, 'Kone', 'add','prune', 'recomputenorm',3, 'maxiter',50, 'maxvertexnum',5000, 'sym',1, 'invariantsubspace',false );
    exact_duration = toc;

    res(end+1).counter = counter;  %#ok<SAGROW>
    idx = numel(res);

    res(idx).duration_exact = exact_duration;
    res(idx).T = T;
    res(end).nfo_T = nfo_T;
    res(idx).nfo_exact = nfo_exact;
    end;

%%
clear dd oo ll_idx ll sz err  %#ok<UNRCH>
for idx = numel(res):-1:1;
    if( isempty(res(idx).nfo_exact) );
        %res(idx) = [];
        continue; end;
    %fprintf( '%i/%i\n', idx, numel(res) );
    err(idx) = tswitch( res(idx).nfo_exact.log.errorcode, {ipa_errorcode.NOERROR}, 'Y', ...
                                                          {ipa_errorcode.COMPLEXKONE}, 'N', ...
                                                          {ipa_errorcode.NOINVARIANTKONE_DUALTEST, ipa_errorcode.NOINVARIANTKONE}, 'K', ...
                                                          {ipa_errorcode.MAXITERATION}, 'F', ...
                                                          '?' );
    if( isanyof( err(idx), {'?'} ) );
        err(idx)
        idx
    end;
    if( isanyof( err(idx), {'K','?','N'} ) );
        ll(idx) = 0;
        oo{idx} = [];
        sz(idx) = 0;
        continue; end;
    dd(idx) = size( res(idx).T{1}, 1 );
    if( ~isfield(res(idx), 'pruned_ordering') || ...
        isempty( res(idx).pruned_ordering ) ...
      );
        res(idx).pruned_ordering = pruned_ordering( res(idx).nfo_exact ); end;
    oo{idx} = res(idx).pruned_ordering;
    ll_idx = max( cellfun( 'prodofsize', oo{idx} ) );
    ll(idx) = tif( isempty(ll_idx), nan, ll_idx );
    sz(idx) = numel( oo{idx} );
end

fprintf( 'Success Exact Algorithm : %i\n', 2 )


%%
T1old = [];  %#ok<UNRCH>
str = {};
for n = 1:numel( res )
    if( ~isfield(res(n).nfo_exact, 'log') );
        oo_str = '?';
    else;
        switch res(n).nfo_exact.log.errorcode;
            case {ipa_errorcode.NOERROR};
                oo = pruned_ordering( res(n).nfo_exact );
                if( numel(oo) == 0 );
                    'X';
                elseif( numel(oo) == 1 && ...
                    (isequal(oo, {1}) || isequal(oo, {2}) )...
                  );
                    continue; end;
                oo_str =  sprintf( '%s', latexprod(oo, 'var','', 'newline',false) );
            case {ipa_errorcode.NOINVARIANTKONE,ipa_errorcode.IS_NOT_A_KONE}
                oo_str = 'N';
                continue;
            case {ipa_errorcode.MAXITERATION,ipa_errorcode.MAXNUM_VERTEXREACHED};
                oo_str = '?';
            otherwise;
                oo_str = num2str( res(n).nfo_exact.log.errorcode ); end; end;
    if( ~isequal( T1old, res(n).T{1} ) );
        T1old = res(n).T{1};
        mat1_str = latexbmatrix( res(n).T{1}, 'begin','\tbsm{', 'end','}', 'newline',false, 'ws',false );
        mat1_str = strrep( mat1_str, '-1','\mn' );
        mat1_str = strrep( mat1_str, '0','\zr' );
        mat1_str = strrep( mat1_str, '1','\pl' );
        if( n ~= 1 );
            str{end+1} = sprintf( '\\multicolumn{2}{c|}{\\hrulefill} & ' );
            end;
        str{end+1} = sprintf( '\\multicolumn{2}{c|}{$A_1=%s$} &', mat1_str );
        end;
    mat2_str = latexbmatrix( res(n).T{2}, 'begin','\tbsm{','end','}', 'newline',false, 'ws',false );
    mat2_str = strrep( mat2_str, '-1','\mn' );
    mat2_str = strrep( mat2_str, '0','\zr' );
    mat2_str = strrep( mat2_str, '1','\pl' );
    TT = invariantsubspace( res(n).T, 'all' );
    inv_str = tif( numel(TT) == 2, '\im', '   ' );
    str{end+1} = sprintf( '$%s$%s &$%s$ &', mat2_str, inv_str, oo_str );
end

num_col = 3;
col = cell( 1, num_col );
rem = ceil( numel(str)/num_col )*num_col - numel(str);
for i = 1:rem;
    str{end+1} = '&&'; end;

col_len = numel(str)/num_col;
for i = 1:num_col;
    col{i} = str((i-1)*col_len+1:i*col_len);
    if( i == num_col );
        for j = 1:numel( col{i} );
            col{i}{j} = strrep( col{i}{j}, 'multicolumn{2}{c|}', 'multicolumn{2}{c}' );
            idx = find( col{i}{j} == '&', 1, 'last' );
            col{i}{j}(idx) = [];
            col{i}{j}(end+1:end+2) = '\\'; end; end;
    col{i} = strvcat( col{i} );
end;
tbl = [col{:}];
fprintf( '\n' )
for i = 1:size(tbl,1);
    fprintf( '%s\n', tbl(i,:) ); end
fprintf( '\n' )


function [ oo ] = pruned_ordering( nfo );
    if( isempty(nfo) );
        oo = {};
        return; end;
    oo = nfo.blockvar{1}.cyclictree.ordering;
    VV = [nfo.blockvar{1}.cyclictree.VV{:}];
    v0 = [nfo.blockvar{1}.cyclictree.v0{:}];
    nrm = polytopenorm( v0, VV, 'kone', 'output','ub', 'v',0 );
    idx_in = nrm < 1;
    oo(idx_in) = [];
    oo = uniquecell( oo );
end

%%
 %#ok<*NOSEL,*NOSEMI,*ALIGN>
