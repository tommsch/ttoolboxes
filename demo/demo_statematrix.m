    fprintf( 2, 'This demo is not tested regularly, and may contain code which does not work with the current version.\n' );
    
clc
condset( 'ct', 1 )
ct = ct + 1
% ct = 38618

rng( ct )
[Ad, A, m, r, c] = statematrix( 'positive', 'dim',50, 'rho' );
[Bd, B, m, r, c] = statematrix( 'm',m, 'positive', 'rho' );
m
%%
% P = symamd( Ad );
% assert( numel(unique(P)) == numel(P) );
% Ads = Ad(P,P);
% Bds = Bd(P,P);

%subplot_lazy(1);
%imagesc( Ad ); colorbar;
%subplot_lazy(2);
%imagesc( Bd ); colorbar;
%plotm( 'link','c' )
%Ad = randn( 27 ); Ad = Ad/rho(Ad);
%Bd = randn( 27 ); Bd = Bd/rho(Bd);
findsmp( {Ad,Bd}, 'v',2, 'plot','mm', 'N',5000, 'delta',inf, 'maxdepth',250 )
%demo_normdecay( {Ad,Bd}, 'maxdepth',30 )
%%
% ind = min( [r c] ):max( [r c] )
% ipa( {Ad,Bd}, 'projection',ind, 'maxsmpdepth',2 )
% ipa( {Ad,Bd}, 'delta',0.999 ); 
ipa( {Ad,Bd}, 'delta',.99, 'admissiblealgorithm','p', 'extrapath',1, 'delta',.999, 'limitmatrix',1, 'testrho',0 );
%ipa( {Ad,Bd}, 'delta',.99, 'admissiblealgorithm','p', 'extrapath',1, 'delta',.999, 'extrapathmindepth',3, 'extrapathminvertex',100, 'extrapathmaxnorm',10, 'limitmatrix',1, 'testrho',0 );
feta( {Ad,Bd} )
%%
clc
m = [3 3 3 3 48];
lcmm( m )
[Ad, A, m, r, c] = statematrix( 'm',m, 'ones', 'rho' );
[Bd, B, m, r, c] = statematrix( 'm',m, 'ones', 'rho' );
%%
clf
subplot_lazy(1);
imagesc( Ad ); colorbar;
subplot_lazy(2);
imagesc( Bd ); colorbar;
subplot_lazy( [3 4 5 6]);
imagesc( buildproduct( {Ad,Bd}, 'rand',76) )

%#ok<*NOSEL,*NOSEMI,*ALIGN>
