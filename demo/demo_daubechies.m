% computes regularity of Daubechies wavelets

% r = [];
% nfo = {};
% T = {};
%%
    fprintf( 2, 'This demo is not tested regularly, and may contain code which does not work with the current version.\n' );

for d = 1:27;
    T{d} = daubechiesmatrix( d );
    c = findsmp( T{d}, 'maxsmpdepth',10, 'sym',1 );
%     [r(d), nfo{d}] = ipa( T{d}, 'smp',c, 'maxnormerr',inf );
    [r_e(d), nfo_e{d}] = ipa( T{d}, 'smp',c, 'maxnormerr',inf, 'extrapath',1 );
end


 %#ok<*SAGROW,*NOSEL>