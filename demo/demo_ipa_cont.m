clc;
fprintf( 2, 'This demo is not tested regularly, and may contain code which does not work with the current version.\n' )

%res = struct;
clear nrm
clear min
G = [0 0 1 1;1 1 0 0;1 1 0 0;0 0 1 1];
for dim = [4]; 
    for round = 1;  % 1:15;
    A = gallery_matrixset( 'randn', 'dim',dim, 'N',2, 'rho' );
    for N = [10];
        tau = 1/N;
        for dwell = [0.05];
            T = {expm(A{2}*dwell), expm(A{1}*tau), expm(A{1}*dwell), expm(A{2}*tau)};
            for delta = 1;  % [.9 .95 .975 .99];
                try;
                    start_ipa = tic;
                    [~, nfo] = ipa( T, G, 'delta',delta, 'maxnormerror',inf, 'maxtime',60*30, 'maxnumvertex',50000, 'maxremainingvertex',200000, 'sym',0, 'balancing',0 );
                    thash(T)
                    dur_ipa = toc( start_ipa );
                    if( nfo.log.errorcode<0 );
                        P = ipa_nfo( nfo, 'polytope' );  % Function missing!
                        start_nrm = tic;
                        for i = 1:2;
                            val = polytopenorm( A{i}^2, P{i}, 'mat', nfo.blockvar{1}.cyclictree.algorithm );
                            val = val(2,:);
                            nrm(i) = val; end;
                        dur_nrm = toc( start_nrm );
                    elseif( nfo.log.errorcode==0 );
                        break; 
                    else;
                        dur_nrm = inf;
                        nrm{i} = [1 1 1 1;inf inf inf inf;0 0 0 0]; end;
                    nrm
                catch me;
                    warning( 'ttoolboxes:demo', 'Error occured' );
                    break; end;
                
                res(end+1).dim = dim;
                res(end).alg = nfo.blockvar{1}.cyclictree.algorithm;
                res(end).log = nfo.log;
                res(end).N = N;
                res(end).dwell = dwell;
                res(end).delta = delta;
                res(end).time_ipa = dur_ipa;
                res(end).time_nrm = dur_nrm;
                res(end).A = thash( A, [], 8 );
                res(end).nrm = max( nrm );
                res(end).prec = -1/dwell*log( 1 - max(nrm)/(8 * N^2) )
                res(end)
                if( dur_ipa>1200 );
                    break; end; end; end; end; end;

%    save demo_ipa_cont_5 res; 
end;

thash(P)
    
%%

% XX incorporate delta into formula
% XX make polytope P_i better suited to A_i
%       use invariant polytope of {A_i^2} as extravertices for ipa for T
% XX have a look at proof, where norm of matrix comes into play

% bad code for examining results

%load demo_ipa_cont
clear min

min_bd = @(x, bd) dwell( x(x > bd) );
max_bd = @(x, bd) max( x(x < bd) );
mean_f = @(x) mean( x(isfinite(x)) );
clc

resi = res;
for i = numel( resi ):-1:1
    if( isempty(resi(i).dim) );
        resi(i) = []; end; end;
resi = resi( [resi.dim] == 9 );
%resi = resi( [resi.delta] == 0.95 );

unique( [resi.delta] )


val = [resi.time_ipa];
round( [min_bd( val, 0 ) median(val) mean_f(val) max_bd( val, inf )] )

val = [resi.nrm]; val = val(2,:);
round( [min_bd( val, 0 ) median(val) mean_f(val) max_bd( val, inf )] )


%#ok<*NOPTS>

%#ok<*DISPLAYPROG,*SAGROW>


%%
function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*NBRAK2>
