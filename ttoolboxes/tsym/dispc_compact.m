function [ str ] = dispc_compact( X );
    try;
        err = lasterror();  %#ok<LERR>
        if( false );
        elseif( ischar(X) || isstring(X) );;
            str = dispc_char( X );
        elseif( iscell(X) );
            str = dispc_cell( X );
        elseif( isnumeric(X) || islogical(X) );
            str = dispc_numeric( X );
        elseif( isstruct(X) );
            str = dispc_struct( X );
        else;
            str = dispc_fallback( X, false ); end;
        while( ~isempty(str) && strcmp( str(end), newline ) );
            str(end) = []; end;
    catch me;  %#ok<NASGU>
        lasterror( err );  %#ok<LERR>
        str = evalc( 'disp( X )' ); end;
end

%%

function [ str ] = dispc_char( X );
    str = ['''' X ''''];
end

function [ str ] = dispc_cell( X );
    if( isempty(X) );
        str = '{}';
    else;
        str = '{ ';
        if( ismatrix(X) );
            for r = 1:size( X, 1 );
                for c = 1:size( X, 2 );
                    str_n = dispc_compact( X{r,c} );
                    if( contains(str_n, newline) );
                        idx = strfind( [newline str], newline );
                        str_n = indent_by( str_n, numel(str) - idx(end) + 1, false ); end;
                    str = [str str_n ', ']; end;  %#ok<AGROW>
                str(end-1:end) = [];
                str = [str '; ']; end;  %#ok<AGROW>
        else;
            for i = 1:numel( X );
                str = [str dispc_compact(X{i}) '| ']; end; end;  %#ok<AGROW>
        str(end-1:end) = [];
        str = [str ' }']; end;
end

function [ str ] = dispc_numeric( X );
    opt.isrow = isrow( X );
    opt.iscol = iscolumn( X );
    if( isempty(X) );
        str = '[]';
    elseif( opt.isrow );
        str = ['[' dispc_fallback(X, true) ']'];
    elseif( opt.iscol );
        str = ['[' dispc_fallback(X.', true) ']^T'];
    else;
        str = ['[' dispc_fallback(X, true) ']']; 
        end;
    if( opt.isrow || opt.iscol );
        str = regexprep( str, '\\\\\\\\[ ]*Columns[ 0-9]*through[ 0-9]*\\\\\\\\', '' );
        str = regexprep( str, '[ ]*Columns[ ]+[ 0-9]+[ ]+through[ ]+[ 0-9]+[ ]*\\\\\\\\', '' );
        str = strrep( str, '\\\\', '' );
        str = strrep( str, '[  ', '[ ' );
        str = strrep( str, '  ]', ' ]' );
        end;
end

function [ str ] = dispc_struct( X );
    str = '';
    fn = fieldnames( X )';
    l = cellfun( 'length', fn );
    maxl = max( l );
    for i = 1:numel( X );
        for n_ = fn; n = n_{1};
            pre = sprintf( [repmat( ' ', 1, maxl-numel(n) ) '%s: '], n );
            str = [str pre];  %#ok<AGROW>
            str_n = dispc_compact( X(i).(n) );
            str_n = indent_by( str_n, numel(pre), false );
            str = [str str_n newline]; end;  %#ok<AGROW>
        if( numel(X) > 1 );
            str = [str '--' newline]; end; end;  %#ok<AGROW>
    if( numel(X) > 1 );
        str(end-2:end) = []; end;
end

function [ str ] = dispc_fallback( X, remove_newline );  %#ok<INUSD>
    str = evalc( 'disp( X )' );
    if( remove_newline );
        % uniformly removes blocks of whitespace
        str = strtrim( str );
        str = strrep( str, newline, '\\' );

        idx2 = strfind( str, '  ' );
        countmax = numel( idx2 );
        for i = numel(idx2)-1:-1:1
            if( idx2(i+1)-idx2(i) == 1 );
                idx2(i+1) = []; end; end;
        count2 = numel( idx2 );
        for count = countmax:-1:1;
            idx = strfind( str, repmat( ' ', 1, count ) );
            if( numel(idx) >= count2 );
                break; end; end;
        if( ~isempty(count) && count > 0 );
            str = strrep( str, repmat( ' ', 1, count ), ' ' ); end; end;
    str = remove_indent( str );
end

function [ str ] = indentstr( indent );
    str = repmat( ' ', 1, indent );
end

function [ str ] = indent_by( str, indent, firstline );
% if `firstline == false`, the first line is not indented
    istr = [newline indentstr( indent )];
    str = [newline str];
    str = strrep( str, newline, istr );
    str(1:numel(newline)) = [];
    if( ~firstline );
        str(1:indent) = []; end;
end

function [ str ] = remove_indent( str );
% removes common whitespace from the left side
    while( numel(str) && startsWith(str, newline) );
        str(1:numel(newline)) = []; end;
    str = strip( str, 'right' );

    if( ~contains( str, newline ) );
        str = strtrim( str );
        return; end;

    str = [newline str];
    idx1 = strfind( str, newline );
    for len = idx1(2):-1:0;
        idx2 = strfind( str, [newline repmat(' ', 1, len)] );
        if( numel(idx2) == numel(idx1) );
            break; end; end;
    str = strrep( str, [newline repmat(' ', 1, len+1)], newline );
    str(1:numel(newline)) = [];
    str = strip( str, 'right' );
end




function dummy; end  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
