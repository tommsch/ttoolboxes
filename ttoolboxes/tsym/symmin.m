function m = symmin( varargin );
% [ m ] = symmin( a1, ..., an );
% Input:
%   a, b        symbolic variables
% Output:
%   m           min( a1, ..., an )
%
% See also: symmax
%
% Written by: tommsch, 2020

    try; 
        if( ~any(cellfun('isclass', varargin, 'sym')) );
            m = min( varargin{:} );
            return; end;

        if( nargin == 0 );
            m = sym( inf );
            return; end;
        if( nargin == 1 );
            m = varargin{1}(1);
            for i = 2:numel( varargin{1} );
                try;
                    if( isAlways(varargin{1}(i)<m,'Unknown','error') );
                        m = varargin{1}(i); end;
                catch;
                    m = privBinaryOp( m, varargin{1}(i), 'symobj::zipWithImplicitExpansion', 'symobj::minReal');
                    m = simplify( m ); end; end;
        else;
            m = varargin{1};
            for i = 2:numel( varargin );
                try;
                    if( isAlways(varargin{i}>m,'Unknown','error') );
                        m = varargin{i}; end;
                catch;
                    m = privBinaryOp( m, varargin{i}, 'symobj::zipWithImplicitExpansion', 'symobj::minReal');
                    m = simplify( m ); end; end; end;
    catch me;  %#ok<NASGU>
        m = min( varargin{:} ); end;
end

function m = symmin_old( varargin ); %#ok<DEFNU>
% [ m ] = symmin( a1, ..., an );
% Input:
%   a, b        symbolic variables
% Output:
%   m           min( a1, ..., an )
%
% See also: symmax
%
% Written by: tommsch, 2020

    if( ~any(cellfun('isclass', varargin,'sym')) );
        m = min( varargin{:} );
        return; end;

    if( nargin == 0 );
        m = sym( -inf );
        return; end;

    m = varargin{1};
    for i = 1:numel( varargin );
        try;
            if( isAlways(varargin{i}<m,'Unknown','error') );
                m = varargin{i}; end;
        catch;
            m = privBinaryOp( m, varargin{i}, 'symobj::zipWithImplicitExpansion', 'symobj::minReal');
            m = simplify( m ); end; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
