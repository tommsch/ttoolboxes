function [ varargout ] = isdiag( A );
% checks whether an array is diagonal
% works for symbolic input too

% Changelog:    tommsch,    2024-11-22,     New function `isdiag`
    
    if( ~isa(A, 'sym') );
        varargout = cell( 1, nargout );
        [varargout{:}] = builtin( 'isdiag', A );
        return; end;
    
    if( ndims( A ) > 2 );  %#ok<ISMAT>
        ret = false;
    else;
        test = A.*~eye( size(A) ) == 0;
        ret =  ~any( test(:) ); end;
    if( nargout >= 1 );
        varargout = {ret}; end;
    
        
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
