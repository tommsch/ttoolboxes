function [ ret ] = isAlways( varargin )
% this function exists to have a uniform interface for the function isAlways

% This function is only called for non-sym arguments.
% Arguments which can be put into an array are numerical.
% But, 'NaN' can not casted to logical nevertheless, so we still need the catch block

try;
    err = lasterror();  %#ok<LERR>
    ret = logical( varargin{1} );
catch me;
    if( nargin == 3 );
        lasterror( err );  %#ok<LERR>
        switch varargin{3};
            case 'falseWithWarning';
                warning( 'sym:isAlways', 'Unable to prove ''%s''.', strtrim(evalc('disp(varargin{1})')) );
                ret = false;
            case 'trueWithWarning';  % this is a TTEST extension
                warning( 'sym:isAlways', 'Unable to prove ''%s''.', strtrim(evalc('disp(varargin{1})')) );
                ret = true;
            case 'false';
                ret = false;
            case 'true';
                ret = true;
            case 'err';
                error( 'sym:isAlways', 'Unable to prove ''%s''.', strtrim(evalc('disp(varargin{1})')) );
            otherwise;
                rethrow( me ); end;
    else;  % 'falseWithWarning';
        warning( 'sym:isAlways', 'Unable to prove ''%s''.', strtrim(evalc('disp(varargin{1})')) );
        ret = false; end; end;

end

function dummy; end  %  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
