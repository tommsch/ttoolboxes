function [ m ] = symmax( varargin );
% This function contains code from Mathworks
    try; 
        if( ~any(cellfun('isclass', varargin, 'sym')) );
            m = max( varargin{:} );
            return; end;

        if( nargin == 0 );
            m = sym( -inf );
            return; end;
        if( nargin == 1 );
            m = varargin{1}(1);
            for i = 2:numel( varargin{1} );
                try;
                    if( isAlways(varargin{1}(i) > m,'Unknown','error') );
                        m = varargin{1}(i); end;
                catch me;  %#ok<NASGU>
                    m = privBinaryOp( m, varargin{1}(i), 'symobj::zipWithImplicitExpansion', 'symobj::maxReal');
                    m = simplify( m ); end; end;
        else;
            m = varargin{1};
            for i = 2:numel( varargin );
                try;
                    if( isAlways(varargin{i} > m,'Unknown','error') );
                        m = varargin{i}; end;
                catch me;  %#ok<NASGU>
                    m = privBinaryOp( m, varargin{i}, 'symobj::zipWithImplicitExpansion', 'symobj::maxReal');
                    m = simplify( m ); end; end; end;
    catch me;  %#ok<NASGU>
        m = max( varargin{:} ); end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.

%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*NBRAK>