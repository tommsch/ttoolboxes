function [ x, ia ] = mldividem( A, b, flag );
% [ x ] = mldividem( A, b, flag );
% consistent implementation of the backslash operator (mldivide) for symbolic and double valued inputs
% 
% Input: 
%   A,b       matrice, numeric or symbolic, must have the same number of rows
%   flag      experimental
%               when set to 'qr', the important columns of A are deduced via qr decomposition
%           
% Output: 
%   x       matrix such that A*x == b, where x has a minimum number of non-zero entries and the result is nearly that of double(A)\double(b)
%   iA      index vector of important columns of A
%
% Note: 
%   This function works only for some special cases yet and needs improvement
% 
% E.g.: b = [0.2 0.8; .8 .2].'; A = [2 0; 1 1; 0 2].';
%       A\b
%       double( sym(A)\sym(b) )  % note that A\b differs from sym(A)\sym(b)
%       l0minnorm( A, b )
%       double( l0minnorm(sym(A), sym(b)) )
%
% Written by: tommsch, 2020

    if( nargin == 3 && isequal(flag, 'qr') );
        [~, ~, ia] = qr( double(A), 'vector' );
        ia = ia(1:rank( double(A) ));
        As = A(:,ia);
        y = As\b;
        x = sym( zeros(size(A, 2), size(b, 2)) );
        x(ia,:) = y;
    elseif( issym(b) && ~isscalar(A) ...
          );
        try;
            % get important columns from A
            s = warning;
            warning( 'off', 'MATLAB:nearlySingularMatrix' );
            val = double( A )\double( b );
            warning( s ) ;
            ia = val ~= 0;
            
            % compute inverse
            As = A(:,ia(:,1));
            y = As\b;
            x = sym( zeros(size(A, 2), size(b, 2)) );
            x(ia(:,1),:) = y;
            
            % make some checks
            e = eps( max(abs(val(:))) ) * numel(A);
            expect( norm(val(:) - double(x(:))) < e, 'l0minnorm:resultincostinent', 'l0minnorm:resultincostinent. Result from symbolic computation differs significantly from the double computation result.' );
            expect( norm(double(A)*double(x) - double(b)) < e, 'l0minnorm:resultwrong', 'l0minnorm:resultwrong. Result from computation is not an inverse.' );
        catch me;  %#ok<NASGU>  % if cast to double is not possible, we end up here
            x = A\b; end;
    elseif( issym(A) );
        warning( 'l0munnorm:not_implemented', 'This case is not implemented yet.' );
        x = A\b;
    else;
        ia = [];
        x = A\b; end;
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
