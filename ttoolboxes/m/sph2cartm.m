function [ X, typeout ] = sph2cartm( sco, type );
% [ X, typeout ] = sph2cartm( sco, [type] )
% Transforms hypershperical to cartesian coordinates.
%
% See also: cart2sphm
%
% Written by: tommsch, 2018

    %parse input
    dim = size( sco, 1 );
    
    if( nargin <= 1 );
        type = 'h'; 
    else;
        sph2cartm_parsetype( type ); end; 
    
    typeout = type;
    
    %handle abnormal cases
    if( nargin == 0 || isempty(sco) ); 
        X = []; 
        return; end;
    
    %check input
    type = sph2cartm_parsetype( type );
    switch type;
        case {'hsph','hsphdeg'};
            %do nothing
        case {'azel','azinc','antpat','latlong','wgs','azeldeg','azincdeg','antpatdeg','latlongdeg','wgsdeg'};
            assert( dim == 2 || dim == 3, 'sph2cartm:type', ['Type ''' type ''' only implemented for dim == 2,3 yet.'] );
        case {'wgs84','wgs84deg'};
            assert( dim == 3, '''type ''wgs84'' is only possible for dim == 3' );
        case {'hcyl','hcyldeg'}
            assert( dim ~= 1, 'sph2cartm:type', ['Type ''' type ''' does not make sense for dim == 1.'] );
        case {'hantpat', 'hantpatdeg'};
            assert( dim == 3, 'sph2cartm:type', ['Type ''' type ''' only implement for dim == 3 yet.'] );
        case {'spherinder','spherinderdeg'};
            assert( dim ~= 2, 'sph2cartm:type', ['Type ''' type ''' does not make sense for dim == 2.'] );
        otherwise
            error( 'Unkown type.' ); end;
    
    %transform coordinates to radians
    switch type;
        case {'hsphdeg','azeldeg','azincdeg','antpatdeg','latlongdeg','wgs84deg','wgsdeg'};
            sco(1:end-1,:) = sco(1:end-1,:)/180*pi;
            type(end-2:end) = [];
        case {'hcyldeg','hantpatdeg'};
            sco(1,:) = sco(1,:)/180*pi;
            type(end-2:end) = [];
        case {'spherinderdeg'};
            if( dim == 1 );
                %do nothing
            else %dim > 2
                sco(1:end-2,:) = sco(1:end-2,:)/180*pi; end;
        otherwise;
            end; %fallthrough;
            
    
    %pre-process coordinates
    switch type;
        case 'azinc';
            %transforms type 3 to type 2
            if( dim == 3 ) 
                sco(2,:) = pi/2 - sco(2,:); end;
            type = 'azel';    
        case 'antpat';
            %transforms type 4 to type 2
            sco(1,:) = pi/2 - sco(1,:);
            if( dim == 3); 
                sco(2,:) = -sco(2,:); end;
            type = 'azel';
        case 'latlong';
            %transforms type 5 to type 2
            sco(1,:) = pi/2 - sco(1,:);
            type = 'azel';
        case  'wgs';
            R = 6371000;
            sco(end,:) = sco(end,:) + R;
            sco([1 2],:) = sco([2 1],:);
            %sco(1,:) = pi/2 - sco(1,:);
            type = 'azel';
        case 'hantpat';
            %transforms type 104 to type 100
            sco(1,:) = pi/2 - sco(1,:);
            type = 'hcyl';
        otherwise;
            end; %fallthrough
    
    %transform coordinates
    switch type;
        case {'hsph'};
            X = zeros( dim,size(sco,2) );
            r = sco(end,:);
            sinval = sin(sco);
            cosval = cos(sco);
            cosval(end,:) = 1;
            for i = 1:dim
                X(i,:) = prod( [sinval(1:i-1,:); cosval(i,:)], 1 ); end;
            X = X.*r;
    
        case {'azel'};
            switch dim;
                case 2;
                    [x,y] = pol2cart( sco(1,:), sco(2,:) );
                    X = [x;y];
                case 3;
                    [x,y,z] = sph2cart( sco(1,:), sco(2,:), sco(3,:) );
                    X = [x;y;z]; 
                otherwise;
                    error( 'Fatal error. Check Code.' ); end;
        case {'wgs84'};
            a = 6378137; %Semi-major axis
            b = 6356752.314245; %Semi-minor axis
            e2 = 6.69437999014e-3; %eccentricity squared
            N = a./sqrt( 1 - e2*sin(sco(1,:)).^2 );
            x = (N + sco(3,:)) .* cos(sco(1,:)) .* cos(sco(2,:));
            y = (N + sco(3,:)) .* cos(sco(1,:)) .* sin(sco(2,:));
            z = (b^2/a^2*N + sco(3,:)) .* sin(sco(1,:));
            X = [x;y;z];
        case {'hcyl'};
            [x,y] = pol2cart( sco(1,:), sco(end,:) );
            X = [x;y; sco(2:end-1,:)];
            
        case {'spherinder'};
            error( ['Type ''' type ''' not implemented yet.'] );
            
        otherwise;
            error( 'Fatal error. Check Code.' ); end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
