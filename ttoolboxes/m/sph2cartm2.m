function [ X, typeout ] = sph2cartm2( varargin );
% [ X, typeout ] = sph2cartm2( sco, [type] )
% Transforms hypershperical to cartesian coordinates, assumes that the radius is one.
% i.e.: size(sco ) == [(dim - 1), N] yields size(X ) == [dim,N]
%
% See also: cart2sphm
%
% Written by: tommsch, 2019

    if( nargin == 1 );
        type = 'hsph'; 
    else;
        type = varargin{end}; end;
    
    sco = varargin{1};
    switch type;
        case {'wgs','wgsdeg','wgsd','wgs84','wgs84deg','wgs84d'};
            [X,typeout] = sph2cartm( [sco; zeros(1,size(sco,2))], varargin{2:end} );
        otherwise;
            [X,typeout] = sph2cartm( [sco; ones(1,size(sco,2))], varargin{2:end} ); end

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
