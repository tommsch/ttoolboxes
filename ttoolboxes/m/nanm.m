function [ d ] = nanm( varargin );
% d = nanm( a1, ..., an) 
% d = nanm([a1 ... an]) 
% Consistent behaviour of inf, with regards to multi-dimensional applications.
%
% Example: x = nanm(3);
%
% See also eyem, zerosm
%
% Written by: tommsch, 2020

    d = nan*onesm( varargin{:} );


end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
