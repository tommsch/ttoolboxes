function [ a ] = kronm( varargin )
% a = kronm( A1, ..., An )
% Kronecker product of arbitrary many matrices.
%
% A multi-argument version of kron, defined by 
%
%   kronm( A1, A2, A3 ) = kron( kron (A1, A2), A3 )
%
% and so on.  This is identical to the behaviour of
% Octave's built-in kron function, but not Matlab's.
%
% Written by: tommsch,

%            tommsch, 2023-04-17, Products of length 1 are allowed
% Changelog: 
    
    if( nargin == 1 );
        a = varargin{1};
        return; end;

    a = kron( varargin{1}, varargin{2} );
    
    for i = 3:nargin
        a = kron( a, varargin{i} ); end;
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
