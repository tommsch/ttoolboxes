function A = allm( A , dir )
% B = allm( A, [dir] )
% Recursive call of all.
% allm( A, [x1 x2 x3 ...  xn] ) = all(...(all(all(all(A, x1), x2), x3),...),xn)
% If dir is not given, then all is called recursively until the outcome is 0 or 1.
%
% Input:
%   A       the array
%   dir     the directions given as a vector. If the vector is empty, then the return value is (c ~= 0).
%
% Output 
%   B       all(...(all(all(all(c,x1),x2),x3),...),xn)
% 
% E.g.: allm([1 1 2; 0 1 1; 1 1 1])
%
% See also: all, anym
%
% Written by: tommsch, 2017

%               2020-06-07, tommsch,    Handle symbolic stuff
%                                       Bug when A is empty and direction is not given fixed.
% Changelog: 

    if( nargin == 0 || ...
        nargin == 1 && isempty( A ) ...
      );
        A = true;
        return; end;

    if( nargin == 1 );
        dir = 1:ndimsm(A); end; 

    if( issym(A) );
        A = isAlways( A ); end;

    if( isempty(dir) );
        A = (A ~= 0); end;
    for i = dir
        A = all( A, i ); end;

end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
