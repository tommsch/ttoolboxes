function [ N ] = ndimsm( X );
% [ N ] = ndimsm( X )
% Consistent behaviour of ndims, with regards to multi-dimensional applications.
% Logically, ndimsm = length( sizem(X) ), but this function is implemented without the use of sizem
%
% Note: ndimsm( [1] ) == 1 %instead of 0
%       ndimsm( [] ) == 0
%
% E.g.: ndimsm( [1; 2; 3] )
% 
%
% See also: ndims
%
% Written by: tommsch, 2017

    if( nargin == 0 || isempty(X) ) ; 
        N = 0; 
    elseif( numel(X) == 1 );
        N = 1;
    else
        N = size( X );
        val = find( N ~= 1, 1, 'last' );
        N(val+1:end) = [];
        N = numel( N ); end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

