function [ d ] = convm(varargin)
% [ d ] = convm( a1 ,a2, a3, ..., an, [options])
% [ d ] = ( ... ((a1 * a2) * a3) * ... * an )
% Consistent behaviour of conv, with regards to multi-dimensional, tensor-valued applications.
%
% Input:
%   ai                  Array
%                       
% Options:
%   'mult',val          1+x vector of integers, default = [1 ... 1], computes (a1*..*a1 ) * (a2*..*a2) * ... * (an*...*an) each of which mult(i) often
%   'outer'             computes the outer or tensor product of squeezed arrays instead
%   'normalize'         divides through "product of all sums"
%   'dim',val           integer, default = max(ndimsm(ai)), dimension of sequence
%
% E.g.: convm( [1 1], [1 1], [1 1], 'normalize' )
%       convm( [1 2 1], [1; 3; 1], 'mult', [2 1] )
%       convm( [1 2 3], [2 3 4], 'outer' )
%
% See also: conv, conv2, convn
%
% Written by: tommsch, 2017
% Uses code from: https://de.mathworks.com/matlabcentral/profile/authors/11-us

 %#ok<*ALIGN>

[mult, varargin] = parsem( 'mult', varargin, [] );
[outer, varargin] = parsem( 'outer', varargin );
[normalize, varargin] = parsem( 'normalize', varargin );
[dim, varargin] = parsem( 'dim', varargin, [] );


if( ~isempty(mult) );
    varargin = rude( mult, varargin ); end;

na = size( varargin, 2 );

if( outer );
    for i = 1:na; 
        varargin{i} = squeezem( varargin{i} ); end; end;

if( na == 1 ); 
    d = varargin{1}; 
else;
    if( outer )
        ndim = cumsum( cellfun(@ndimsm, varargin) );
        if( isempty(dim) );
            dim = ndim(end); end;
        for i = 1:na; 
            varargin{i} = permute( varargin{i}, circshift(1:ndim(end),ndim(i)-ndim(1)) ); end; end;

    if( isempty(dim) );
        dim = max( cellfun(@ndimsm, varargin) ); end;
    
    d = convm_worker( varargin{1}, varargin{2}, dim ); %initialize d
    for i = 3:na
        d = convm_worker( d, varargin{i}, dim ); end; end;

if( normalize );
    d = d/prod( cellfun(@(x) sum(x(:)), varargin) ); end;

end

function [ d ] = convm_worker( a, c, dim )
    if( isempty(a) ); 
        d = a; 
        return; end;
    if( isempty(c) ); 
        d = c; 
        return; end;
    
    try
        issyma = issym( a );
        issymc = issym( c );
    catch
        issyma = 0;
        issymc = 0; end;
    
    
    if( ~issyma && ~issymc && ~anym(a) && ~anym(c) ); 
        d = 0; 
        return; end;
    
    dimac = max( ndimsm(a), ndimsm(c) );
    if( isequal(dim,dimac) );
        if( issyma || issymc )
            val = mask2symbol( a, 'dim',dimac, 'var','z1' ) * mask2symbol( c, 'dim',dimac, 'var','z1' );
            d = symbol2mask( val, 'dim',dimac, 'sym', 'var','z1' );
        elseif( dimac == 1 );
            d = conv( a, c );
        elseif( dimac == 2 );
            d = conv2( a, c );
        else
            d = convn( a, c ); end;
    elseif( dim-dimac <= 2 )
        d = convm_tensor( a, c, dim ); 
    else
        error( 'convm:tensordim', 'Convolution not defined yet for tensor dimensions greater equal 2.' ); end;
        
            
end

function [ d ] = convm_tensor( a, c, dim )
colonpre = repcell( ':', [1 dim] );
d = zeros( [(sizem(a,1:dim)-1)+(sizem(c,1:dim)-1)+1, size(a,1+dim), size(c,2+dim)] );  %#ok<SIZEM>
for i = 1:size( a, 1+dim )
    for l = 1:size( a, 2+dim )
            d(colonpre{:},i,:) = squeeze(d(colonpre{:},i,:)) + convn(squeeze(a(colonpre{:},i,l)), squeeze(c(colonpre{:},l,:))); end; end;
end

function [ vec ] = rude( len, val )
% [ vec ] = rude( len, val ) % Run-length DEcoding
%
% len   : repeat each val corresponding len times to create vec
% val    : 1xN template array of vals to be repeated len times
%           - numericals
%           - strings
%           - cells (any contents)
% vec   : reconstruced output vector from len/val
%
% Notes:
%     len <= 0 will remove corresponding VALs
% E.g.: vec = rude( [1 2 3 0 4], [10 inf nan pi 20] )
%
% Written by : us, 18-Nov-2004
% Modified: us,    30-Nov-2004 20:42:03    / TMW FEX
% Modified: tommsch, 03-June-2020

    lx = len > 0 & ~(len == inf);
    if( ~any(lx) );
        vec = [];
        return;    end;
    assert( numel(len) == numel(val), 'rude:length', 'rl-decoder: length mismatch\nlen = %-1d\nval = %-1d', numel(len), numel(val) );
    len = len(lx);
    val = val(lx);
    val = val(:).';
    len = len(:);
    lc = cumsum( len );
    lx = zeros( 1, lc(end) );
    lx([1;lc(1:end-1)+1]) = 1;
    lc = cumsum( lx );
    vec = val(lc);
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
