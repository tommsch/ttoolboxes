function [ type ] = sph2cartm_parsetype( type )
% [ X, typeout ] = sph2cartm( sco, [type] )
% parses type  hypershperical to cartesian coordinates.

if( isempty(type) );
    type = 'h'; end;   
if( isnumeric( type ) && ...
    (type == 9 || type == 10 || type == 11 || type == 12) ...
  )
    error( 'sph2cartm:type', 'Old type used. See the help of this function for the new numbers of the types.\n'); end;
assert( ischar(type) || isstring(type), 'cast2sph:type', 'coordinate type must be given as a string.' );
type = char( type );
switch type;
    case {'hsph','h','hsphrad','hr'};             type = 'hsph';  % 1
    case {'hsphdeg','hd'};                        type = 'hsphdeg';  % -1
    case {'azel','ae','azelrad','aer'};           type = 'azel';  % 2
    case {'azeldeg','aed'};                       type = 'azeldeg';  % -2
    case {'azinc','ai','azincrad','air'};         type = 'azinc';  % 3
    case {'azincdeg','aid'};                      type = 'azincdeg';  % -3
    case {'antpat','ant','antpatrad','antr'};     type = 'antpat';  % 4
    case {'antpatdeg','antdeg','antd'};           type = 'antpatdeg';  % -4
    case {'latlong','ll','latlongrad','llr'};     type = 'latlong';  % 5
    case {'latlongdeg','lld'};                    type = 'latlongdeg';  % -5
    case {'wgs84','wgs84rad','wgs84r'};           type = 'wgs84';  % -6
    case {'wgs84deg','wgs84d'};                   type = 'wgs84deg';  % -6
    case {'wgs','wgsrad','wgsr'};                 type = 'wgs';  % -6
    case {'wgsdeg','wgsd'};                       type = 'wgsdeg';  % -7
    case {'hcyl','hc','hcylrad','hcr'};           type = 'hcyl';  % 100
    case {'hcyldeg','hcd'};                       type = 'hcyldeg';  % -100
    case {'hantpat','hant','hantpatrat','hantr'}; type = 'hantpat';  % 104
    case {'hantpatdeg','hantdeg','hantd'};        type = 'hantpatdeg';  % -104
    otherwise; error( 'sph2cartm:input', 'Wrong string for type.' ); end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
