function [ x, err ] = implicit_inverse( f, y, interval, epsilon, algorithm )
% [ x, err ] = implicit_inverse( f, y, [interval, epsilon, algorithm, hardworking] )
% returns a solution of f^{-1}(y), under the assumptions that 
%     f   is continuous and,
%     f   has a zero inside of `interval`
%
% Input:
%   f           function handle
%   y           vector of scalars
%   interval    2-element vector, interval where there exists x such that f(x) == y
%   epsilon     scalar, default = eps, Error bound - the returned value may not satisfy this bound!
%   algorithm   the algorithm to be used
%                   'bisect', 'secant', 'mueller': not recommended, since badly written and unstable
%                                                  Copyright: Gatech AE (2023). A Few Root-Finding Algorithms,  MATLAB Central File Exchange No 73705
%                                                  Licence: see at the bottom
%                   'fzero': Uses matlab function fzero, contained in the optimization toolbox
%                   'bisect_tommsch': medium fast and quite robust
%                   'all': Tries all algorithms
% Output:
%   x           vector of size equal to y, the solutions to f(x) == y
%   err         The absolute error of the solutions, may be larger than `epsilon`
%
% Note:
%  This function is experimental, and thus inside of a private folder
%
% Written by: tommsch, 01-03-2023
    
    if( nargin <= 2 || isempty(interval) );
        interval = [-1e10 1e10]; end;
    if( nargin <= 3 || isempty(epsilon) );
        epsilon = eps; end;
    if( nargin <= 4 || isempty(algorithm) );
        algorithm = 'bisect_tommsch'; end;
    
    
    
    x = zeros( size(y) );
    err = zeros( size(y) );
    for i = 1:numel( y );
        interval_i = find_start( f, y(i), interval );
        yi = y(i);
        xi = nan;
        maxerri = inf;
        
        if( any(strcmp(algorithm, {'all','all_weak','t','bisect_tommsch','bstommsch','bisecttommsch','bst','tbs'})) || ...
            strcmp(algorithm, {'all'}) || ...
            strcmp(algorithm,'all_weak') && maxerri < epsilon ...
          );
            xii = bisect_tommsch( f, yi, interval_i, epsilon );
            errii = abs(f(xii)-yi);
            if( abs(f(xii)-yi) < maxerri );
                maxerri = errii;
                xi = xii; end; end;        
        
        if( any(strcmp(algorithm, {'rf','regula_falsi','regulafalsi'})) || ...
            strcmp(algorithm, {'all'}) || ...
            strcmp(algorithm,'all_weak') && maxerri < epsilon ...
          );
            if( strcmp(algorithm,'all_weak') && maxerri < epsilon );
            else;
                xii = regula_falsi( @(x) f(x)-yi, interval_i(1), interval_i(2), 4*epsilon, 100 );
                errii = abs(f(xii)-yi);
                if( abs(f(xii)-yi) < maxerri );
                    maxerri = errii;
                    xi = xii; end; end; end;
            
        if( any(strcmp(algorithm, {'all','all_weak','fz','fzero'})) || ...
            strcmp(algorithm, {'all'}) || ...
            strcmp(algorithm,'all_weak') && maxerri < epsilon ...
          );
            opt = optimset( 'TolX',epsilon, 'Display','off' );
            try;
                xii = fzero( @(x) f(x)-yi, mean(interval_i), opt ); 
            catch me;  %#ok<NASGU>
                xii = nan; end;
            errii = abs(f(xii)-yi);
            if( abs(f(xii)-yi) < maxerri );
                maxerri = errii;
                xi = xii; end; end;
        
        if( any(strcmp(algorithm, {'all','all_weak','s','secant'})) || ...
            strcmp(algorithm, {'all'}) || ...
            strcmp(algorithm,'all_weak') && maxerri < epsilon ...
          );
            xii = SecantMethod( @(x) f(x)-yi, interval_i(1), interval_i(2), epsilon );
            errii = abs(f(xii)-yi);
            if( abs(f(xii)-yi) < maxerri );
                maxerri = errii;
                xi = xii; end; end;

        if( any(strcmp(algorithm, {'all','m','mueller'})) || ...
            strcmp(algorithm, {'all'}) || ...
            strcmp(algorithm,'all_weak') && maxerri < epsilon ...
          );
            xii = MuellerMethod( @(x) f(x)-yi, interval_i(1), mean(interval_i), interval_i(2), epsilon );
            errii = abs(f(xii)-yi);
            if( abs(f(xii)-yi) < maxerri );
                maxerri = errii;
                xi = xii; end; end;

        if( any(strcmp(algorithm, {'all','all_weak','b','bs','bisect'})) || ...
            strcmp(algorithm, {'all'}) || ...
            strcmp(algorithm,'all_weak') && maxerri < epsilon ...
          );
            xii = Bisection1D( @(x) f(x)-yi, interval_i(1), interval_i(2), epsilon );
            errii = abs(f(xii)-yi);
            if( abs(f(xii)-yi) < maxerri );
                maxerri = errii;
                xi = xii; end; end;            
        
        xi = real( xi );
        err(i) = maxerri;
        x(i) = xi;
        end;
    
end

function [ interval ] = find_start( f, y, interval )
    
    n = 2;
    while( true );
        interval_n = linspace( interval(1), interval(2), n );
        n = ceil( 1.5*(n + 1) );
        diff_signs = diff( tsign( f( interval_n ) - y ) );
        idx = find( diff_signs ~= 0 & isfinite(diff_signs) );
        if( ~isempty(idx) );
            idx = idx(1);
            interval = interval_n(idx(1):idx(1)+1); end;
        
        for i = 1:(numel( interval_n ) - 1)
            fi = f( interval_n(i) ) - y;
            fj = f( interval_n(i+1) ) - y;

            len = interval_n(2)-interval_n(1);
            mag = max( abs(interval_n(2)), abs(interval_n(1)) );
            if( len<4*eps(mag) || n > 1e7 );
                % let interval unchanged
                return;
            elseif( ~isreal(fi) || ~isreal(fj) );
                % continue;
            elseif( sign(fi) ~= sign(fj) );
                interval = interval_n(i:i+1);
                return;
                end; end; end;
end

function [ s ] = tsign( s )
    idx = ~isreal( s );
    s = sign( s );
    s(idx) = nan;
end

function [ x ] = bisect_tommsch( f, y, interval, epsilon )

    while( true );
        m = mean( interval );
        f1 = f(interval(1));
        fm = f(m);

        
        if( f1 <= y && y <= fm || ...
            fm <= y && y <= f1 ...
          );
            interval(2) = m;
        else;
            interval(1) = m; end;
%             f2 = f(interval(2)); 
%             if( fm <= y && y <= f2 ||...
%                 f2 <= y && y <= fm ...
%               );
%                 interval(1) = mid; 
%             else;
%                 x = nan;
%                 break; end;
        if( interval(2)-interval(1)<abs(4*epsilon*(abs(m)+1)) );
            break; end; end;
    
    x = m;

end

function [ c ] = regula_falsi( f, a, b, e, m );
        % a,b: endpoints of an interval where we search
        %   e: half of upper bound for relative error
        %   m: maximal number of iteration

       side = 0;
       
       % starting values at endpoints of interval
       fa = f( a );
       fb = f( b );

       for n = 1:m;
          c = real( (fa * b - fb * a) / (fa - fb) );
          if (abs(b - a) < e * abs(b + a))
             break; end;
          fc = f(c);

          if ( fc * fb > 0 );
             % fc and fb have same sign, copy c to b
             b = c;
             fb = fc;
             if (side == -1)
                fa = fa / 2; end;
             side = -1;
          elseif (fa * fc > 0);
             % fc and fa have same sign, copy c to a
             a = c;
             fa = fc;
             if ( side == +1 )
                fb = fb / 2; end;
             side = +1;
          else;
             % fc * f_ very small (looks like zero)
             break; end;
       end;
end

function Root = Bisection1D( fh, Low, High, varargin );
    %   This function uses the Bisection Method to find the root of a 
    %   nonlinear function given the following set of criteria. 
    %   - The bounds are opposite signs, or one is equal to zero. This 
    %     function will error when given bounds that are  equal in sign, as 
    %     this could imply multiple roots or none at all
    %   - The bounds will preferably not contain discontinuities, as the 
    %     direction that the bisection method should take will be unclear
    %
    % Required input arguments:
    %
    %    fh:        Function handle, either a scripted function or an anonymous
    %               function. An example of an anonymous function is given here
    %
    %                           fh = @(x) 5*sqrt(x) - x
    %
    %   Low:        The lower bound in which to look for the root
    %
    %  High:        The upper bound in which to look for the root
    %               
    %  Optional input arguments:
    %
    %  tolerance:   The accuracy that the solution must have, shown as the
    %               difference of the calculated midpoint from zero
    %
    a = fh(High);
    b = fh(Low);
    if isempty(varargin)
        tol = 1e5*eps;
    else
        tol = varargin{1};
    end
    if sign(b) == sign(a)
%         disp(['These bounds contain either no roots, a discontinuity, or multiple roots'])
%         disp('Please adjust bounds')
        Root = nan;
        return
    end
    Mid = (Low+High)/2;
    c = fh(Mid);
    if isinf(c)||isnan(c)
%         disp('Discontinuity in the within the boundaries')
        Root = nan;
        return
    end
    counter = 0;
    maxcounter = 1000;
    while abs(c)>8 * tol && counter < maxcounter;
        counter = counter + 1;
        if sign(c) ~= sign(a)
            Low = Mid;
            Mid = (Mid+High)/2;
        else
            High = Mid;
            Mid = (Mid+Low)/2;
        end
        c = fh(Mid);
        if isinf(c)||isnan(c)
%             disp('Failed to converge')
            Root = nan;
            return
        end
    end
    Root = Mid;
end

function Root = SecantMethod( fh, Guess1, Guess2, varargin )
    %   This function uses the secant method to find the root of a 
    %   nonlinear function by computing the slope between two initial guesses. 
    %   The two guesses do not need to bound the solution
    %
    % Required input arguments:
    %
    %      fh:      Function handle, either a scripted function or an anonymous
    %               function. An example of an anonymous function is given here
    %
    %                           fh = @(x) 1 - cos(x)
    %
    %   Guess1:        The lower bound in which to look for the root
    %
    %   Guess2:        The upper bound in which to look for the root
    %               
    %  Optional input arguments:
    %
    %  tolerance:   The accuracy that the solution must have, shown as the
    %               difference of the calculated midpoint from zero
    %
    G1 = min([Guess1 Guess2]);
    G2 = max([Guess1 Guess2]);
    x1 = fh(G1);
    x2 = fh(G2);
    if isempty(varargin)
        tol = 1e5*eps;
    else
        tol = varargin{1};
    end
    if isinf(x1)||isinf(x2)||isnan(x1)||isnan(x2)
%         disp('Try using different guesses, the current values yield inf or nan')
        Root = nan;
        return
    end
    err = 1;
    counter = 0;
    maxcounter = 1000;
    while ( abs(err)>tol && counter < maxcounter );
        counter = counter + 1;

        slope = real( (x2-x1)/(G2-G1) );
        if isinf(slope)||slope == 0

%             disp('Failed to converge. Either a horizontal or vertical line')
            Root = nan;
            return
        end

        tempvar = abs(slope);
        % This slope adjustment lows convergence, but helps block divergence
        % in a lot of cases. The next three lines can be commented out if the 
        % function is well behaved.
        if tempvar<0.3
            slope = real( abs(log10(tempvar))*sign(slope) );
        end
        Gnew = G2-(x2/slope);
        err = G2-Gnew;

        G1 = G2;    x1 = x2;
        G2 = Gnew;  x2 = fh(G2);

    end
    Root = Gnew;
end

function [ Root ] = MuellerMethod( fh, Guess1, Guess2, Guess3, varargin )
    %   This function uses Mueller's method to find the root of a function
    %   by using three guesses to compute the root, and can find complex roots
    %   as well. Though it requires three guesses, it converges very rapidly
    %   for high order polynomials (I tested ninth order) with roots 
    %   of multiplicity > 1, and wildly nonlinear functions like the following. 
    %   This particular nonlinear function converged in just 5 iterations with 
    %   initial guesses (0.4, 0.5, 1.6). Fzero will error in this particular 
    %   case, and the true x solutions are x = (1.380555, 5.6916, and 6.6386)
    %           
    %           fh = @(x) 4*x*cos(x)-exp(sqrt(x))*log(x);
    %
    %   The guesses do not need to bound the solution
    %       
    % Required input arguments:
    %
    %      fh:      Function handle, either a scripted function or an anonymous
    %               function. An example of an anonymous function is given here
    %
    %                           fh = @(x) 1 - cos(x)
    %
    %   Guess1, Guess2, Guess3: "Reasonable" root values guesses
    %               
    %  Optional input arguments:
    %
    %  tolerance:   The accuracy that the solution must have, shown as the
    %               difference of the calculated midpoint from zero
    %
    vec = sort([Guess1 Guess2 Guess3]);
    G1 = vec(1);    G2 = vec(2);    G3 = vec(3);
    x1 = fh(G1);    x2 = fh(G2);    x3 = fh(G3);
    if isempty(varargin)
        tol = 1e5*eps;
    else
        tol = varargin{1};
    end
    if any(isinf([G1 G2 G3 x1 x2 x3]))||any(isnan([G1 G2 G3 x1 x2 x3]))
%         disp('Try using different guesses, the current values yield inf or nan')
        Root = nan;
        return
    end
    err = 1;
    counter = 0;
    maxcounter = 1000;
    while abs(err)>tol && counter < maxcounter;
        counter = counter + 1;
        L = (G3-G2)/(G2-G1);
        D = (G3-G1)/(G2-G1);
        G = (L.^2)*x1 - (D.^2)*x2 + (L + D)*x3;
        C = L*abs(L*x1 - D*x2 + x3);
        Numerator = -2*D*x3;
        Temp = sqrt(G^2 - 4*D*C*x3);
        [~,l] = min(abs([Numerator/(G-Temp) Numerator/(G+Temp)]));
        if l == 2
            lambda = Numerator/(G+Temp);
        else
            lambda = Numerator/(G-Temp);
        end
        Gnew = G3 + lambda*(G3-G2);
        err = G3-Gnew;

        G1 = G2;    x1 = x2;
        G2 = G3;    x2 = x3;
        G3 = Gnew;  x3 = fh(Gnew);
        if any(isnan([G1 G2 G3 x1 x2 x3]))
%             disp('Convergence failed with these Guesses')
            Root = nan;
            return
        end
    end
    Root = Gnew;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

%%

% Licence for algorithms 'secant','mueller','bisect'
%%%%
% Copyright (c) 2019, Alaric Gregoire
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% * Neither the name of Georgia Institute of Technology nor the names of its
%   contributors may be used to endorse or promote products derived from this
%   software without specific prior written permission.
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
