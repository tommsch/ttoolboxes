function [ Y ] = padarraym( varargin );
% [ Y ] = padarraym( X, r, [value], [options] )
% Consistent behaviour for padarray,  with regards to multi-dimensional applications.
% Works for arrays and cells and symbolics.
% 
% Input:
%   X               the cell-array to be padded
%   r               integer, how much to pad
%                       if scalar: padds in all dimension
%                       if vector: only padds in given dimension
%   value           double, the value used for padding
%
% Options:
%   'post'          Only pad at the beginning
%   'pre'           Only pad at the end
%   (Default)       Pad everywhere
%
% Output:
%   Y           the padded cell-array
%   
% E.g.: padarraym( {1 [1 2]; sym(2) 'a'}, 1, 'VAL', 'pre' )
%
% Written by: Notlikethat (stackoverflow.com/users/3156750/notlikethat), 2014
% Modified by: tommsch, 2018

%            2020-10-16, Bugfix in the interface. If the input is empty, the output should not be empty in general
% Changelog: 

    % parse input
    if( isequal(varargin{end},'post') ); 
        post = true; 
        varargin(end) = [];
    else; 
        post = false; end;
    if( isequal(varargin{end},'pre') ); 
        pre = true;
        varargin(end) = [];
    else; 
        pre = false; end;
    X = varargin{1};
    Y = X;
    r = varargin{2};
    varargin(1:2) = [];
    if( ~isempty(varargin) );
        value = varargin{1};
    else;
        if( iscell(X) );
            value = [];
        else;
            value = 0; end; end;
    
    dim = max( numel(r), ndimsm(X) );
    if( isscalar(r) ); 
        r = r*ones( 1, dim ); end;
    r( end+1:dim ) = 0;
    if( iscolumn(r) );
        r = r.'; end;
    
    % handle degenerate cases
    if( all(r == 0) );
        return; end;
    assert( all(r  >= 0), 'padarraym:argin', '''r'' must be nonnegative.' );
    
    val = sizem( X, 1:dim );
    sz = cell( 1, numel(val) );
    for i = 1:numel( val );
        sz{i} = 1:val(i); end;
    
    if( post || pre );
        sznew = num2cell(sizem(X,1:dim) + r);
    else; %both
        sznew = num2cell( sizem(X,1:dim) + 2*r ); end;
    
    if( iscell(X) );
        Y = cellm( sznew{:} );
        [Y{:}] = deal( value );
    elseif( isa(X,'sym') );
        Y = sym( value*onesm(sznew{:}) ); 
    else;
        Y = value*onesm( sznew{:} ); end;
    
    Y(sz{:}) = X;  % hooray for comma-separated lists!
    if( ~iscell(X) );
        Y = anycast( Y, class(X) ); end;
        
    if( ~post );
        r(ndims(Y)+1:end) = [];
        Y = circshift( Y, r ); end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*SIZEM>
