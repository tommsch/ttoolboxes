function A = linspacem( A1, A2, N )
    A = cell( 1, N );
    rN = 1/(N-1);
    for n = 2:N-1
        w1 = (N - n)*rN;
        w2 = (n - 1)*rN;
        A{n} = A1 * w1 + A2 * w2;
    end
    A{1} = A1;
    A{end} = A2;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
