function [ ret ] = iotam( varargin );
    sz = [varargin{:}];
    if( numel(sz) == 1 );
        sz = [sz 1]; end;    
    n = prod( sz );
    ret = 1:n;

    ret = reshape( ret, sz );
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
