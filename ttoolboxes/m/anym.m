function [ A ] = anym( A , dir );
% B = anym( A , [dir] )
% Recursive call of any.
% anym(c,[x1 x2 x3 ...  xn]) = any(...(any(any(any(c,x1),x2),x3),...),xn)
% If dir is not given, then any is called recursevily until the outcome is 0 or 1.
%
% Input:
%   c       the sequence
%   dir     the directions given as a vector. If the vector is empty, then the return value is (c ~= 0).
%
% Output 
%   c       any(...(any(any(any(c,x1),x2),x3),...),xn)
% 
% E.g.: anym([1 1 2; 0 1 1; 1 1 1])
%
% See also: any, allm
%
% Written by: tommsch, 2017

%            2020-06-07, tommsch,   Handle symbolic stuff
%                                   Bug when A is empty and direction is not given fixed.
% Changelog:
    
    if( nargin == 0 || nargin == 1 && isempty(A) );
        A = false;
        return; end;
    
    if( nargin == 1 );
        dir = 1:ndimsm(A); end; 
    
    if( issym(A) );
        %warning( 'symbolic:sym:isAlways:TruthUnknown', 'Unable to prove whether value is nonzero or not. I assume it is nonzero.' ); 
        A = isAlways( A, 'Unknown', 'true' ); %to compute the correct result
        end;
    
    
    if( isempty(dir) ); 
        A = (A ~= 0); end;
    for i = dir
        A = any( A, i ); end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>