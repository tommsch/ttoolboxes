function [ varargout ] = sizem( A, varargin )
% [ sz ] = sizem( A, [dim, | dim1, ..., dimN] )
% [ sz1, ..., szN ] = sizem( __ )
% Consistent behaviour of size, with regards to multi-dimensional applications.
%
% Input:
%       dim                 vector, if given, only the dimensions in dim are returned
%       dimi                scalar, if given, only the dimensions dimi are returned
%
% Output:
%       sz      vector, queried sizes
%       szi     scalar, queried sizes     
%
% Remarks: 
%       Empty stuff has sizem==[]
%
% E.g.: sizem( [2 3] )
%       sizem( [2;3] )
%       sizem( [2;3],[ ], 3 )
%       sizem( zeros(2,0,1) )
%       sizem( [] )
%           
% See also: size
%
% Written by tommsch, 2018

%            2020-06-07, tommsch,   Negative dimensions are possible
%            2020-10-16, tommsch,   Interface changed to Matlab R2019b style
% Changelog:

    
    if( numel(varargin) == 0 && nargout <= 1 );
        sz = size( A );
        if( sz(end) == 1 );
            sz(end) = []; end;
        if( isequal(sz,[0 0]) );
            sz = []; end;
    else;
        if( numel(varargin) == 0 );
            dim = 1:nargout;
        else;
            dim = [varargin{:}]; end;
        idx = dim<0;
        dim(idx) = dim(idx) + ndimsm( A ) + 1;
        idx = dim <= 0;
        dim(idx) = ndimsm( A ) + 1;
        sz = zeros( 1, numel(dim) );
        for i = 1:numel( sz );
            sz(i) = size( A, dim(i) ); end; end;
    
    if( nargout > 1 );
        varargout = cell( 1, nargout );
        for i = 1:nargout;
            varargout{i} = sz(i); end;
    elseif( nargout <= 1 );
        varargout{1} = sz; end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

