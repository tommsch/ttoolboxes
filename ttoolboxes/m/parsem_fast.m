function [value, args, retname] = parsem_fast( varargin );
% [value, args, retname] =  parsem_fast name, args , [input3] )
% Parses varargin and is easier to use than Matlabs parse.
% Similar to parsem, but has only basic functionalities
%
% Input:
% ======
%   name                string or cell-array of strings. 
%                       The questionable argument. The function searches for string <name> in <args>. The return value depends on [input3] and on args.
%
%   args                string or cell array of strings
%         'help','helptext'     If contained in args, then an help is printed
%
%
%   [input3]            default value
%                           If input3 is not given: If the string can be found, and the entry in args behind the string is not a string, then this value is returned.
%                                                   Otherwise, If the string can be found the function returns 1, otherwise 0.
%                           If input3 is given:     If the string can be found, the function returns the value in <args> which is one position behind the last occurence of <name>.
%                                                   If that string cannot be found, the function returns the value which is defined input3.
%                       The last occurence of 'name' defines what happens
%
% Output:
%   value               The parsed value or the set value
%   args                input arguments without the parsed values (except 'set' or 'condset' is given).
%                       If the input argument is contained more than once, then all of those are removed in the return value.
%   retname             The parsed name. First entry  in 'name' if any of 'name' is not present in 'args'
%
%
% Written by: tommsch, 2016

    name = varargin{1};
    args = varargin{2};
    if( ~iscell(args) ); 
        args = {args}; end;
    if( ~iscell(name) ); 
        name = {name}; end;
    
    % Set default value, determine if strings are allowed for val
    if( size(varargin, 2) >= 3 );
        input3 = varargin{3};
        default = input3;
        defaultgiven = 1;
    else;
        default = 0;
        defaultgiven = 0; end;
    
    % read value for name, remove everything which is read 
    idx = [];
    if( ~isempty(name) );
        retname = name{1}; 
    else
        retname = []; end;
    for i = 1:numel( name );
        value = find( strcmp(args,name{i}) );
        if( ~isempty(value) );
            retname = name{i}; end;
        idx = [idx value]; end;  %#ok<AGROW>
    szeargs = size(args,2);  % number of elements in args
    if( ~isempty(idx) );
        for i = 1:size( idx, 2 );
            k = idx(i)+1;
            if( szeargs >= k && ...
                (~ischar(args{k}) || defaultgiven) ...
              );
                idx(end+1) = k;  %#ok<AGROW>  % save the index of val, so that it can get deleted afterwards
                value = args{k};
            elseif( defaultgiven )
                warning( 'parsem:missing', 'In the given function arguments, the option ''%s'' has most likely no ''value'' (but it should have a value).', args{idx} );
                value = 1;
            else
                value = 1; end; end;
        args(idx) = [];
    else
        value = default; end;



   
end
        

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

