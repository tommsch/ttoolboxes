function [ d, dmin ] = upsamplem( c, idx, M, fillvalue, vectorflag )
% [ d, dmin ] = upsamplem( c, idx, M, [fillvalue], [vectorflag] )
% Multivariate upsampling.
% For \alpha\in\ZZ^s: upsamplen(c,M)(\alpha) = c(\beta) for \alpha = M\beta; and fillvalue otherwise
%
% Input:
%   c           multidimensional array, the sequence to be upsampled
%   idx         vector, index of first entry in c. If idx is empty, then idx = zeros( ndimsm(c), 1 )
%   M           (dim x dim-matrix) the dilation which defines to upsample, or vector/scalar in which M is considered as a diagonal matrix with corresponding entries
%   fillvalue   scalar, default = 0, the value for the "holes"
%   vectorflag  bool, default = false, disables warning when dimension of sequence is larger than dimension of upsampling matrix
%
% Output:
%   d           the upsampled array
%   dmin        index of the first entry in d
%
% Note:
%   upsamplem may insert zeros at the boundaries. This behaviour may be changed in future releases
% 
% E.g.: [d,dmin] = upsamplem( [1 2 3; 4 5 6], [0;0], [2 1; 0 -2], Inf )
%
% Written by: tommsch, 2018

%               2020-06-07, tommsch, idx can be any vector
%                                    M can be a vector or a scalar
%                                    vectorized and totally rewritten
% Changelog:
%#ok<*ALIGN> 

    if( nargin <= 3 || isempty(fillvalue) ); 
        fillvalue = 0; end;
    if( nargin <= 4 || isempty(vectorflag) );
        vectorflag = 0; end;
    if( isempty(idx) ); 
        %idx = zeros( size(c,1), 1 ); end;  % original
        idx = zeros( ndimsm(c), 1 ); end;
    if( isrow(idx) );
        idx = idx.'; end;
    if( isscalar(M) );
        M = M*eye( numel(idx) ); end;
    if( isvector(M) );
        M = diag( M ); end;
    if( isempty(c) );
        d = c;
        dmin = idx;
        return; end;

    % error checks
    assert( nargin>= 3,                 'upsamplem:args', 'd = upsamplem(c, idx, M, [fillvalue] ). Argument(s) missing.');
    assert( isscalar(fillvalue),        'upsamplem:args', 'd = upsamplem(c, idx, M, [fillvalue] ). Optional fourth entry must be a scalar, default=0');
    assert( ismatrix(M) && issquare(M), 'upsamplem:args', 'd = upsamplem(c, idx, M, [fillvalue] ). Third entry must be a square matrix.');
    assert( isvector(idx),              'upsamplem:args', 'd = upsamplem(c, idx, M, [fillvalue] ). Second entry must be a column vector or empty.');
    
    dimo = size( M, 2 );  % the dimension in which we are working
    dim = max( ndimsm( c ), dimo );
    szi = sizem( c, 1:dim ).';
    szi(1:dimo) = [];

    if( ~vectorflag && dimo<dim );
        warning( 'upsamplem:dim', 'Dimension of sequence is larger than dimension of upsampling matrix. Set ''vectorflag'',1 when this is on purpose.' ); end;
    
    % very easy case: empty sequence
    if( isempty(c) ); 
        d = []; 
        dmin = idx; 
        return; end;
    
    if( isdiag(M) && isequal(fillvalue,0) );  % if dim == 1, we use standard upsampling 
        try;                                  %#ok<TRYNC>  % ...but only when Signal Processing Toolbox is installed
            p = [dimo 1:dimo-1 dimo+1:dim];
            if( numel(p) == 1 );
                p = [1 2]; end;
            M_t = diag( M );
            dmin_t = minm( [idx sizem( c, 1:dimo ).'+idx].*M_t, 2);  % compute coordinates of upsampled corner points
            c_t = c;
            for i = 1:numel( M_t )
                if( ~isrow(c_t) );  % if c is a row vector we do not need to upsample
                    c_t = upsample( c_t, abs(M_t(i)) );
                else
                    val = sizem( c_t );
                    c_t = vertcat(c_t, zerosm([abs(M_t(i))-1 val(2:end)]) ); end;  %#ok<AGROW>
                if( M_t(i)<0 );
                    dmin_t(i) = dmin_t(i) + 1;
                    c_t = flipud( c_t ); end;
                c_t = permute( c_t, p ); end;
            d = c_t;
            dmin = dmin_t;
            return; end; end;
        
    
    ind = supp( c, dimo, idx );  % indices of sequence, each column is one entry
    upind = M*ind;  % upsampled indices
    indmat = ind - idx + 1;  % indices for matlab, each coluMmn is one entry
    upindmat = upind - minm(upind,2) + 1;  % upsampled indices for matlab, each column is one entry
    
    % coordinates of the corner points
    dmin = minm( upind, 2 ); 
    dmax = maxm( upind, 2 );
    d = fillvalue*onesm( [dmax-dmin+1; szi] );  % output array
    colon = arrayfun( @(x) 1:size(c,x), dimo+1:dim, 'UniformOutput', false );  % indices of tensor-values
    colon = allcomb( colon{:} ).';
    cidx = num2cell( indmat, 1 );  % each cell is one coordinate
    didx = num2cell( upindmat, 1 ); 
    colidx = num2cell( colon, 1 );
    cidx = allcomb( cidx, colidx )';  % each column is one coordinate
    didx = allcomb( didx, colidx )';
    cidx = num2cell( cell2mat( cidx ), 2 );  % transform to format for sub2ind
    didx = num2cell( cell2mat( didx ), 2 );
    cidx = sub2ind( size(c), cidx{:} );  % linear indices
    didx = sub2ind( size(d), didx{:} );
    d( didx ) = anycast( c( cidx ), 'like',d );
    
end

function A = allcomb(varargin)
% ALLCOMB - All combinations
%    B = ALLCOMB(A1,A2,A3,...,AN) returns all combinations of the elements
%    in the arrays A1, A2, ..., and AN. B is P-by-N matrix where P is the product
%    of the number of elements of the N inputs. 
%    This functionality is also known as the Cartesian Product. The
%    arguments can be numerical and/or characters, or they can be cell arrays.
%
%    Examples:
%       allcomb([1 3 5],[-3 8],[0 1]) % numerical input:
%       % -> [ 1  -3   0
%       %      1  -3   1
%       %      1   8   0
%       %        ...
%       %      5  -3   1
%       %      5   8   1 ] ; % a 12-by-3 array
%
%       allcomb('abc','XY') % character arrays
%       % -> [ aX ; aY ; bX ; bY ; cX ; cY] % a 6-by-2 character array
%
%       allcomb('xy',[65 66]) % a combination -> character output
%       % -> ['xA' ; 'xB' ; 'yA' ; 'yB'] % a 4-by-2 character array
%
%       allcomb({'hello','Bye'},{'Joe', 10:12},{99999 []}) % all cell arrays
%       % -> {  'hello'  'Joe'        [99999]
%       %       'hello'  'Joe'             []
%       %       'hello'  [1x3 double] [99999]
%       %       'hello'  [1x3 double]      []
%       %       'Bye'    'Joe'        [99999]
%       %       'Bye'    'Joe'             []
%       %       'Bye'    [1x3 double] [99999]
%       %       'Bye'    [1x3 double]      [] } ; % a 8-by-3 cell array
%
%    ALLCOMB(..., 'matlab') causes the first column to change fastest which
%    is consistent with matlab indexing. Example: 
%      allcomb(1:2,3:4,5:6,'matlab') 
%      % -> [ 1 3 5 ; 1 4 5 ; 1 3 6 ; ... ; 2 4 6 ]
%
%    If one of the N arguments is empty, ALLCOMB returns a 0-by-N empty array.
%    
%    See also NCHOOSEK, PERMS, NDGRID
%         and NCHOOSE, COMBN, KTHCOMBN (Matlab Central FEX)
% Tested in Matlab R2015a and up
% version 4.2 (apr 2018)
% (c) Jos van der Geest
% email: samelinoa@gmail.com
% History
% 1.1 (feb 2006), removed minor bug when entering empty cell arrays;
%     added option to let the first input run fastest (suggestion by JD)
% 1.2 (jan 2010), using ii as an index on the left-hand for the multiple
%     output by NDGRID. Thanks to Jan Simon, for showing this little trick
% 2.0 (dec 2010). Bruno Luong convinced me that an empty input should
% return an empty output.
% 2.1 (feb 2011). A cell as input argument caused the check on the last
%      argument (specifying the order) to crash.
% 2.2 (jan 2012). removed a superfluous line of code (ischar(..))
% 3.0 (may 2012) removed check for doubles so character arrays are accepted
% 4.0 (feb 2014) added support for cell arrays
% 4.1 (feb 2016) fixed error for cell array input with last argument being
%     'matlab'. Thanks to Richard for pointing this out.
% 4.2 (apr 2018) fixed some grammar mistakes in the help and comments
    
    args = varargin ;
    idx = cellfun( @isempty, args );
    args(idx) = [];

    NC = numel(args);
    
    % default: enter arguments backwards, so last one (AN) is changing fastest
    ii = NC:-1:1;
    
    if( NC == 0 )  % no inputs
        A = zeros(0,0) ; 
    elseif( NC == 1 )  % a single input, nothing to combine
        A = args{1}(:) ; 
    else
        isCellInput = cellfun(@iscell, args) ;
        if( any(isCellInput) );
            if( ~all(isCellInput) );
                error('ALLCOMB:InvalidCellInput', 'For cell input, all arguments should be cell arrays.') ; end;
            % for cell input, we use to indices to get all combinations
            ix = cellfun(@(c) 1:numel(c), args, 'un', 0) ;
            [ix{ii}] = ndgrid( ix{ii} );  % flip using ii if last column is changing fastest

            A = cell( numel(ix{1}), NC);  % pre-allocate the output
            for k = 1:NC
                % combine
                A(:,k) = reshape( args{k}(ix{k}), [], 1 ); end;
        else
            % non-cell input, assuming all numerical values or strings
            % flip using ii if last column is changing fastest
            [A{ii}] = ndgrid( args{ii} ) ;
            % concatenate
            A = reshape( cat(NC+1,A{:}), [], NC ); end; end;

end
    

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*SIZEM>
