function d = infm(varargin)
% d = infm( a1, ..., an) 
% d = infm([a1 ... an]) 
% Consistent behaviour of inf, with regards to multi-dimensional applications.
%
% Example: x = infm(3);
%
% See also eyem, zerosm
%
% Written by: tommsch, 2020

d = inf*onesm(varargin{:});


end