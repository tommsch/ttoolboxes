function [ varargout ] = plotm( varargin )
% [ ax ] = plotm( [ax], data, [linespec], [options])  % points given
% [ ax ] = plotm( [ax], func, XYZ, [linespec], [options])  % (very experimental) function handle and evaluation points given
% Wrapper function which calls stem/plot/plot3 depending on the dimension of varargin{1}
% Also handles interval data
%
% Input:
% =============
%   [ax]        (experimental), axes, where to plot into
%   data        dim x N array OR
%               (experimental) cell array of vectors representing hyper-intervals
%   func        function handle
%   XYZ         array, points passed to `func` to generate `data`
%
%   [options]   nearly everything which can be parsed by plot/plot2/plot3/stem/stem3/scatter3/mesh3/contour3/...
%
% Specs
% =====
%   Linespec                Matlab Linespec string (e.g. 'r--' or 'xk')
%                           The color specification might not work in many cases.
%   Axisspec                Sets the axis to a different scaling. One of: 'linlin','loglin','linlog','loglog','linlinlin','loglinlin','linloglin','loglinlog','linloglog','logloglog','logx','logy','logz','logxy','logxz','logyz','logxyz','logc','loga','semilogx','semilogy','semilogz','semilogxy','semilogxz','semilogyz','semilogxyz','semilogc','semiloga'
%   Functspec               Shorthand for <'funct',value>: One of: 'Kone','Cone','kone','cone','kone*','cone*','Kone*','Cone*','minkowski','minkowski*','minkowski**','lsr'
%
% Options only for plotm
% ======================
% Not all plot options work for all dimensions
%   'alpha',value           (experimental) Sets alpha
%   'arrow',length          double, Plots arrows from the origin to the specified points. length is a scaling factor
%   'aux',val               Something, additional data - what exactly is expected depends on ''funct''
%                               'k'/'K'     center of kone
%   'average',val           (experimental) default = false, boolean, the ordinate values of all abcissa points whose distance is 0 are averaged.
%   'boundary',val          Plots the boundary (i.e. something which is around all points, but in general not convex)
%                               1 >= val >= 0:  computed with boundary (matlab function)
%                               val > 1:        points which have few neighbours are computed (only possible for dim=2)
%                               0 > val:        another strange method using Delauney triangulation (only possible for dim=2)
%   'box',dim               int, Plots the hypercube of dimension dim with volume 1
%   'clf',fignum            (experimental) whether to clf the Figure with Number fignum before plotting
%                           If fignum=='all', everything gets clfed.
%   'clf_lazy'              (experimental) whether to clf when a plotm command is issued which plots something
%   'center'                Only if `funct`=='Kone'
%   'contour'               Plots contour lines
%   'cylindrical',val       (experimental) integer, Interprets the coordinates in data as cylindrical coordinates,
%                           i.e. it shifts the first row of data to the end and calls sph2cartm for the result
%   'darkmode'              (experimental) Uses colors which look better in darkmode
%   'delauney'              (experimental) Plots the delauney triangulation
%   'equal',str             (experimental), chars, default='', Unifies axes
%                           If given, all properties (abbreviated) in str of the plots will be set to the same value
%                           str is a concatenation of the charaters: 'x', 'y', 'z', 'c' (for x, y, z, c-axis), 
%   'fig',val               Figure handle or number, to which Figure to plot. If not given, plotting is done in gcf
%   'funct',c               char, default = [], any of 'k','K','p','r','c'
%                               preprocesses the input V so that the input points correspond to some polytope which can be used to define a norm
%                               'k'         makes a kone, (i.e. makes lines connection the origin and the input), 'k' does scale the input to 2-norm equal to 1
%                               'k*'        makes dual a kone, (i.e. makes lines connection the origin and the input), 'k' does scale the input to 2-norm equal to 1
%                               'K'         rectilinear projection of kone
%                               'p',        makes a nrm w.r.t to the first orthant (i.e. adds all projections onto the axis to the input)
%                               'r'         makes a nrm w.r.t to the origin (i.e. mirrors all input points at the origin)
%                               'r*'        makes the polar set of a nrm w.r.t to the origin (i.e. mirrors all input points at the origin)
%                               'r**'       makes the bipolar set of a nrm w.r.t to the origin (i.e. mirrors all input points at the origin)
%                               'c'         makes an elliptic nrm w.r.t to the origin (i.e. transforms complex input into ellipses)
%   'height',val            Only for 1-dim plots:
%                           scalar or 1x2 vector, default=[0 1],
%                           determines the length of the lines to be plotted
%                               scalar: Line goes from 0 to val
%                               vector: Line goes from val(1) to val(2) 
%   'hyperplane',n          Plots a hyperplane for normale vector n
%   'hold',val              Sets hold temporarily to on and before returning back to the original state. In general one want 'hold',2
%                               1           only holds during the plotm command. i.e. if a plotm command would plot multiple things, all appear on the screen
%                               2, 'on'     also holds the contents currently in the figure (this is what one wants in general)
%   'hull'                  Plots the convex hull
%   'hyperplane',vector     (experimental) Plots the plane orthogonal to `vector`
%   'imp',[imp val]         1x2 vector, default=[0 0], Plots only if imp>=valif
%   'label',val             (experimental), default=enabled, adds labels to the axis
%   'link',str              chars, default='', Links xyzc-axes and camera position
%                           If given, all properties (abbreviated) in str of the plots will be linked
%                           str is a concatenation of the charaters: 'x', 'y', 'z', 'c' (for x, y, z, c-axis), 'v' (for camera view properties)
%   'patch'/'fill'          (experimental) If given, patch is used to plot convex hulls, and thus filled polygones are plotted
%   'point'                 Tries to plot the input as pointcloud
%   'proj',val              (experimental) scalar, 2-element vector or matrix, projects the plots to be pointed into a smaller subspace prior plotting
%                               scalar: generates a random a matrix, which is perturbed slightly each time plotm is called
%                    2x1-vector[out in]: generates a random matrix, projecting from R^out to R^in
%                                    0/'delete':    Deletes stored projection matrix
%                                   -1/'random':    Automatic to 3d if input is higher dimensional
%                                   -2/'old':       Uses old projection matrix (experimental): Uses a slightly perturbed projection matrix
%                                   -3: generates a new projection matrix if necessary, if a projection matrix is present from a prior call uses method -2
%                               matrix: uses the given projection matrix
%   'reim'                  Transforms `data` to [real(data); imag(data)]
%   'res',val               Hint which resolution to use in various plot types like surface plots. If val == 0, points are most likely plotted
%   'same',str              (experimental) If given, all properties (abbreviated) in str of the plots are set to the same value
%                           str is as for option 'link'
%   'scene','default'       (experimental) Sets some rendering values. Only possible value is 'default'
%   'spherical',[type off]  [int/string double], Plots points given in hyperspherical coordinates of type type (default: t=0), with offset off from the unit-sphere
%   'surface'               Plots the surface
%   'sym'                   default = false, if true, then the input is not casted to double
%   'title'                 (experimental) Title of plot
%   'thread'                (experimental) if given, plotting preprocessing is done in thread
%   'unit',str              string, default='', 
%                           Transforms last row prior of data prior further processing, depending on str. Possible values for str are:
%                           'abs', 'lin2log', 'lin2abslog', 'log2lin', 'log2abslin', 'lin2db', 'lin2absdb', 'db2lin', 'db2abslin', 'semilogx','semilogy','loglog' 
%                           axes are changed accordingly
%   'unplot',val            (experimental) unplots the last val things
%   'verbose',val           Verbose level
%   'violin',val            (experimental) 1/2/4-element vector: makes a violin plot (displaces the points randomly)
%                               1-element vector: displacment in x direction
%                               2-element vector: displacmene in x/y direction
%                               4-element vector: negative/postive displacement in x/y direction
%   'visible',bool          (experimental) if set to zero, the figure to which is plotted is set to invisible
%   'xscale',value          function handle, If given scales the x-axis values of `data`. The ticks and ticklabels are changed accordingly
%   'yscale',value          function handle, If given scales the y-axis values of `data`. The ticks and ticklabels are changed accordingly
%   'zscale',value          function handle, If given scales the z-axis values of `data`. The ticks and ticklabels are changed accordingly
%   'xkcd'                  (experimental) xkcd-ify all plots. Needs xkcdify()
%
% Options taken from Matlab
% ==========================
%   The function understands (at least it should) all options which can be passed to matlab plot functions and which make sense for the thing to be plotted.
%
%   'markersize',value      Matlabs markersize optino is inconsistent across various plotting function. plotm fixes this
%   'linewidth',value
%   'color',value           Matlab styles, OR
%                           formats which are sensible sensibleadditionally, OR
%                           'scaled'                                                                        (experimental) Uses last coordinate of `data` for colors
%                           'x','y','z','v'  (experimental)                                                 (experimental) Uses x/y/z/v values for colors
%                           'mblue', 'morange','myellow','mpurple','mgreen','mlightblue','mred'             Matlab default colors when no color specification is given
%                           
%
% Output
% ======
%   ax          (experimental), axes, where it was plotted into
%   Q           the processed input data
%
%
% Info:
% ===========
%   1-dim data:
%       Each point is plotted as a dot at height 1. 
%       The height can be changed by Option <'height',val>
%       To change the marker, the option <'Marker','something'> should be used
%
%   2-dim/3-dim data 
%       A point cloud is plotted
%
%
% E.g.: clf; hold on; load trimesh3d; plotm( [x y z]','boundary',.9); plotm( [x y z]','point','MarkerSize',5,'rx')
%       clf; plotm( randn(2, 40), '.-' )
%       clf; plotm( randn(1, 40), 'height',4 );
%
%
% See also: surfm, plot, plot3, stem
%
%
% Written by: tommsch, 2016

%               tommsch,    2019-04-01,     Added option 'hull', 'boundary' for plotm3
%                                           Added function plotm4
%               tommsch,    2019-11-25,     Refactored functions to remove code duplication
%               tommsch,    2019-12-06,     Added experimental options 'spherical','unit','imp'
%               tommsch,    2020-01-08,     Added parsing of Matlabs LineSpec
%                                           Improved plotm4
%               tommsch,    2020-01-24,     Added experimental options 'link' and 'equal'
%               tommsch,    2020-03-06,     Added experimental option 'cylindrical'
%               tommsch,    2020-04-24,     Added experimental option 'delaunay' and 'xkcd'
%               tommsch,    2020-08-25,     Added experimental option 'functional', 'proj', 'axis', 'hold'
%                                           Behaviour change in parsing of linespecs, Linespecs can now be on any position in varargin
%               tommsch,    2020-12-13,     Removed alias 'c' for 'color'
%               tommsch,    2021-01-21,     Added experimental option 'unplot'
%               tommsch,    2021-02-24,     Bugfix for non-consistent Markersizes
%               tommsch,    2021-02-26,     Added experimental options 'semilogx', 'semilogy', 'loglog'
%               tommsch,    2021-05-13,     Added experimental options 'violin'
%               tommsch,    2021-05-31,     Bugfix, changed default verbose lvl to 1
%               tommsch,    2021-06-02,     Added experimental calling option with function handle
%                                           Added experimental option 'average'
%               tommsch,    2021-07-23,     Removed experimental option 'cylindrical'
%               tommsch,    2021-11-23,     Behaviour change: Surface is plotted before Points for 3d plots
%               tommsch,    2022-02-23,     Added experimental option 'thread'
%               tommsch,    2022-03-09,     RGB triplets can be given in range [0, 255]
%               tommsch,    2022-12-16,     Bugfix, 'color' option did not work for 'hull' plots
%               tommsch,    2022-03-02,     Bugfix, linespec was parsed wrongly in some cases
%               tommsch,    2023-03-30,     Added functional types 'k','K' (for kone) (see also `ipa` and `polytopenorm`)
%               tommsch,    2023-04-16,     PROJECTION matrix is now stored inside of current figure
%               tommsch,    2024-02-04,     big refactoring
%                                           Added experimental option 'arrow'
%                                           Removed experimental option 'axis'
%                                           Added option 'darkmode' (third-party-code)
%               tommsch,    2024-04-08,     Removed possibility to plot 4d stuff
%               tommsch,    2024-09-06,     Bugfix for funct ''K''
%                                           Added experimental option ''aux''
%                                           Behaviour change of funct ''K'', now plots the stereographic projection of a kone
%               tommsch,    2024-10-14,     Added experimental option ''hyperplane''
% Changelog:    tommsch,    2024-01-01,     Added documentation for options (some of them experimental): 'clf_lazy','clf','visible','alpha','hyperplane', axisspec, functspec, 'reim','xscale','yscale','zscale','color','proj'
%               tommsch,    2024-11-27,     Behaviour change ''violin''
%               tommsch,    2024-12-04,     New experimental option ''patch''
%               tommsch,    2025-01-25,     New experimental functionals 'r*', 'r**'
%#ok<*HISTC>

% XX Add axes input/output
% XX plotting hull for funct 'l' gives very wrong results

    %% Parsing and preprocessing
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [retcode, varargin] = process_non_plotting_stuff( varargin{:} );
    if( retcode );
        return; end;

    [opt.verbose, varargin] = parsem( {'verbose','v'}, varargin, 1 );  % Verbose level
    [opt, varargin] = process_figure( 'pre', opt, varargin{:} );
    [varargin] = parse_function_handle( opt, varargin{:} );
    [opt, varargin] = parse_simple_options( opt, varargin{:} );
    [opt, varargin] = parse_varargin1_modifying_options( opt, varargin{:} );
    [opt, varargin] = parse_linespec_etc( opt, varargin{:} );  % must be last, since a linespec can easily be misinterpreted. e.g. 'funct','r' 
    [varargin] = replace_shortcuts( varargin{:} );

    [opt, varargin] = plot_meta( opt, varargin{:} );
    varargin = plot_hyperplane( opt, varargin{:} );
           
    varargin = process_projection( opt, varargin{:} );

    %% Plot  
    %%%%%%%%%%
    if( ~isempty(varargin) );
        process_figure( 'in', opt );
        Q = varargin{1};
        varargin(1) = [];
        if( iscell(Q) );
            dim = size( Q{1}, 1 );
            if( dim == 1 ); 
                plotinterval1( Q, opt, varargin{:} );
            else;
                warning( 'plotm:dim', 'no intervalplot for dim > 1.' ); end;       
        else;
            dim = size( Q, 1 );
            if( dim == 1 ); 
                plotm1( Q, opt, varargin{:} );
            elseif( dim == 2 ); 
                plotm2( Q, opt, varargin{:} ); 
            elseif( dim == 3 ); 
                plotm3( Q, opt, varargin{:} );
            else; 
                warning( 'plotm:dim', 'no ''plotm'' for dimension %i', dim ); end; end; end;
    
    %% postprocessing
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    postprocess_logaxis( opt );
    postprocess_xyzscale( opt );
    postprocess_label( opt );
    postprocess_misc( opt );
    postprocess_scene( opt );
    process_figure( 'post', opt );
    if( opt.holdglobal == 1 );
        hold on;
    elseif( opt.holdglobal <= -1 );
        hold off; end;    
    
    postprocess_view( opt, varargin{:} );
     
    if( nargout );
        varargout{1} = opt.ax; end;

end

%% preprocessing functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ retcode, varargin ] = process_non_plotting_stuff( varargin );
    retcode = 0;
    [imp, varargin] = parsem( {'imp'}, varargin, [0 0] );  % whether to plot or not
    if( imp(2) < imp(1) );
        retcode = 1;
        return; end;
    
    [unplotnum, varargin] = parsem( {'unplot','undraw'}, varargin, 0 );
    if( unplotnum );
        unplot( unplotnum );
        retcode = 1;
        return; end;
    
    [thread, varargin] = parsem( {'thread','threadplot'}, varargin, 0 );
    if( thread );
        threadplot( @() plotm( varargin{:} ), [], thread ); 
        retcode = 1;
        return; end;
end

function [ opt, varargin ] = process_figure( where, opt, varargin );

    persistent clf_lazy_flag;
    switch where;
        case 'pre';
            [opt.clf_lazy, varargin] = parsem( {'clf_lazy'}, varargin, 0 );  % whether to clf when a plotm command is issued which plots something
            [opt.clf, varargin] = parsem( {'clf'}, varargin, [] );  % whether to clf now
            [opt.holdglobal, varargin] = parsem( {'hold'}, varargin, 0 );
            
            if( isequal(opt.clf, 'all') || isequal(opt.clf, -1) );
                clfall();
            elseif( ~isequal(opt.clf_lazy, 0) );
                clf_lazy_flag = [clf_lazy_flag opt.clf_lazy(:)'];
            % if( numel(varargin) == 0 );
            %     return; end;
            elseif( ~isempty(opt.clf) );
                for i = 1:numel( opt.clf );
                    figure_lazy( opt.clf(i) );
                    clf; end; end;
            [opt.visible, varargin] = parsem( {'visible','visibility','vis'}, varargin, 'default',[] );
            
            hgflag = cellfun( @(x) allm(ishghandle(x)) && ~isnumeric(x) && ~isempty(x), varargin );
            assert( nnz(hgflag) <= 1, 'plotm:handle', 'At most one graphics handle must be given.' );
            opt.ax = [];
            if( any(hgflag) );
                hg = varargin{hgflag};
                varargin(hgflag) = [];
                if( isa( hg,  'matlab.ui.Figure' ) );
                    opt.ax = hg.CurrentAxes;
                elseif( isa( hg, 'matlab.graphics.axis.Axes' ) );
                    opt.ax = hg;
                else;
                    opt.ax = ancestor( hg, 'axes' );
                    if( iscell(opt.ax) );
                        opt.ax = opt.ax{1}; end; end; end;
            
            [fig, varargin] = parsem( {'figure','fig'}, varargin, [] );  % to which figure we shall draw
            if( ~isempty(fig) );
                if( iscell(fig) );
                    fig = figure( fig{:} );
                else;
                    fig = figure( fig ); end;
                opt.ax = fig.CurrentAxes; end;
            if( isa(opt.ax, 'matlab.ui.Figure') );
                opt.ax = opt.ax.CurrentAxes; end;
            if( isempty(opt.ax) );
                opt.ax = gca; end;
            if( isempty(opt.visible) );
                % do nothing
            elseif( isequal(opt.visible, true) || strcmpi(opt.visible, 'on') );
                set( opt.ax.Parent, 'Visible', 'On' );
            elseif( isequal(opt.visible, false) || strcmpi(opt.visible, 'off')  );
                set( opt.ax.Parent, 'Visible', 'Off' );
            else;
                error( 'plotm:visible', 'Wrong value given for option ''visible''.' ); end;
            opt.fig = ancestor( opt.ax, 'figure', 'toplevel' );
            assert( ~isempty(opt.fig), 'plotm:fig', 'Figure of given axes is empty. This is most likely a programming error.' );
            
            if( strcmp(opt.holdglobal, 'on') );
                opt.holdglobal = 2; end;

            if( opt.holdglobal );
                if( ~ishold && opt.holdglobal == 1 );
                    clear_figure_content
                    hold( opt.ax, 'on' );
                    opt.holdglobal = -1;
                elseif( ~ishold && opt.holdglobal > 1 );
                    hold( opt.ax, 'on' );
                    opt.holdglobal = -2;
                else
                    opt.holdglobal = 1; end; end;
            
        case 'in';
            if( ~isempty(clf_lazy_flag) );
                fig = opt.fig;
                idx = fig.Number == clf_lazy_flag;
                if( any(idx) );
                    clf;
                    try;
                        rmappdata( fig, 'ttoolboxes_plotm_PROJECTION' ); 
                    catch me;
                        if( ~isanyof(me.identifier, {'MATLAB:HandleGraphics:Appdata:InvalidPropertyName'}) );
                            throw me; end; end;
                    clf_lazy_flag(idx) = []; end; end;
            
        case 'post';
            if( opt.holdglobal == 1 );
                hold( opt.ax, 'on' );
            elseif( opt.holdglobal <= -1 );
                hold( opt.ax, 'off' ); end;    
        otherwise;
            fatal_error; end;
end

function [ varargin ] = parse_function_handle( opt, varargin );
    if( isempty(varargin) );
        return; end;
    if( isa(varargin{1}, 'function_handle') );
        assert( numel( varargin ) >= 2 && ( ...
                  ismatrix( varargin{2} ) && size( varargin{2}, 1 ) == nargin( varargin{1} ) || ...
                  iscell( varargin{2} ) && numel( varargin{2} ) == nargin( varargin{1} ) ), ...
            ['Second argument must be a either:' newline ...
             '  1) a matrix of column vectors of dimension equal to the number of input arguments, or' newline ...
             '  2) a cell array of vectors, where the cell array has number of input arguments elements indicating the points where to plot'] );
        if( iscell(varargin{2}) );
            XYZ = mixvector( varargin{2} ); 
        else;
            XYZ = varargin{2}; end;
        XYZ = num2cell( XYZ, 2 );
        try;
            V = feval( varargin{1}, XYZ{:} );
            varargin{1} = cell2mat( [XYZ;V] );
        catch me;
            if( opt.verbose >= 2 );
                disp( me ); end;
            % if vectorized code does not work
            XYZ = [XYZ{:}];
            V = zeros( 1, size(XYZ, 2) );
            for mm = 1:size( V, 2 );
                V(mm) = feval( varargin{1}, XYZ(:,mm) ); end; 
            if( size(V, 1) == 1 );
                varargin{1} = [XYZ;V]; 
            else;
                varargin{1} = V; end; end;
        varargin(2) = []; end;
end

function [ opt, varargin ] = parse_simple_options( opt, varargin );
    [opt.alpha, varargin] =             parsem( {'alpha'}, varargin, [] );
    [opt.boundary, varargin] =          parsem( 'boundary', varargin, [] );  % boundary
    [opt.box, varargin] =               parsem( 'box', varargin );     
    [opt.contour, varargin] =           parsem( 'contour', varargin, 0 );  % contour
    [opt.darkmode, varargin] =          parsem( {'darkmode'}, varargin );     
    [opt.delaunay, varargin] =          parsem( 'delaunay', varargin, 0 );  % delaunay triangulation
    [opt.height, varargin] =            parsem( {'height','h'}, varargin, [] );  % height
    [opt.hyperplane, varargin] =        parsem( {'hyperplane','halfspace','plane','normalplane'}, varargin );     
    [opt.hull, varargin] =              parsem( 'hull', varargin );  % hull
    [opt.label, varargin] =             parsem( {'label'}, varargin, 0 );
    [opt.logaxisflag, varargin, opt.logaxis] =  parsem( {'linlin','loglin','linlog','loglog','linlinlin','loglinlin','linloglin','loglinlog','linloglog','logloglog','logx','logy','logz','logxy','logxz','logyz','logxyz','logc','loga','semilogx','semilogy','semilogz','semilogxy','semilogxz','semilogyz','semilogxyz','semilogc','semiloga'}, varargin );
    [opt.linkaxis, varargin] =          parsem( {'link','linkaxis'}, varargin, '' );
    [opt.markersize, varargin] =        parsem( {'MarkerSize','markersize','ms'}, varargin, [] );
    [opt.origin, varargin] =            parsem( {'origin','zero'}, varargin );
    [opt.patch, varargin] =             parsem( {'patch','fill'}, varargin );
    [opt.point, varargin] =             parsem( {'point','points','pt','pts'}, varargin, 0 );  % pointcloud
    [opt.proj, varargin] =              parsem( {'proj','projection'}, varargin, [] );
    [opt.reim, varargin] =              parsem( {'reim','realimag'}, varargin );
    [opt.res, varargin] =               parsem( {'resolution','res'}, varargin, -inf );  % XX rename to .resolution
    [opt.sameaxis, varargin] =          parsem( {'same','sameaxis','equal','equalize','equalizeaxis','equalaxis'}, varargin, '' );
    [opt.surface, varargin] =           parsem( 'surface', varargin, 0 );  % surface
    
    [opt.scene, varargin] =             parsem( {'scene','lightning','lighting'}, varargin, 0 );
    [opt.title, varargin] =             parsem( {'title'}, varargin, '' );
    [opt.auxiliary_data, varargin] =    parsem( {'aux','auxiliary_data','polytopenorm_auxiliary_data'}, varargin, [] );  % (experimental) auxiliary data used for plotting, depends on the given ''funct''. The possible options will also have a specialized name too, but ''aux'' can be used for all of them
    [opt.xkcd, varargin] =              parsem( {'xkcd'}, varargin );
    [kone_center, varargin] =           parsem( {'center','kone_center'}, varargin, [] );  % (experimental) the center vector of the kone. If not given, then it is automatically computed
    if( ~isempty(kone_center) );
        assert( isempty(opt.auxiliary_data), 'If value for option ''center'' is given, then option ''auxiliary_data'' cannot be given.' );
        opt.auxiliary_data = kone_center; end;
end

function [ opt, varargin ] = parse_varargin1_modifying_options( opt, varargin );
    if( isempty(varargin) );
        return; end;

    PROJECTION = [];
    if( ~isempty(opt.proj) && isequal(opt.proj, 'old') );
        fig = opt.fig;
        PROJECTION = getappdata( fig, 'ttoolboxes_plotm_PROJECTION' );  % get PROJECTION matrix stored in figure
        assert( numel(varargin) >= 1, 'plotm:proj', 'If option <''proj'',''old''> is given, something to be plotted must be given too.' );
        assert( ~isempty(PROJECTION) && numel(varargin) >= 1, 'plotm:proj', 'No projection matrix is stored in figure.' );
        assert( size(PROJECTION, 2) == size(varargin{1}, 1), 'plotm:proj', 'Dimensions of stored projection matrix is wrong.' );
        vprintf( 'Projection Matrix:\n %v\n', PROJECTION, 'imp',[2 opt.verbose] );
        varargin{1} = PROJECTION * varargin{1};
        opt.title = [opt.title newline '(Projected from R^' num2str(size(PROJECTION, 2)) ' to R^' num2str(size(PROJECTION, 1))];
        end;
        
    if( opt.reim );
        varargin{1} = [real(varargin{1}); imag(varargin{1})]; end;

    [symflag, varargin] = parsem( {'sym'}, varargin );
    if( ~symflag && numel(varargin) >= 1 && issym(varargin{1}) );
        varargin{1} = double( varargin{1} ); end;
    
    [average, varargin] = parsem( {'average','avg'}, varargin, [] );
    if( ~isempty(average) );
        assert( isscalar(average), '''average'' must be a scalar.' );
        [xyz, ~, idxc] = unique( varargin{1}(1:end-1,:).', 'rows' );
        vmean = accumarray( idxc, varargin{1}(end,:), [], @mean );
        varargin{1} = [xyz.';vmean.']; end;
    
    [violinval, varargin] = parsem( {'violin'}, varargin, 0 );
    if( ~isequal(violinval, 0) );
        switch( numel(violinval) );
            case 1; violinval(2) = violinval(1); violinval(3:4) = 0;
            case 2; violinval(3:4) = violinval(2); violinval(1:2) = violinval(1);
            case 4; % do nothing
            otherwise; error( 'plotm:violinval', 'The argument to ''violin'' must have 1, 2 or 4 elements.' ); end;
        if( any(violinval(1:2)) );
            varargin{1}(1:end-1,:) = varargin{1}(1:end-1,:) ...
                                            + abs(randn( size(varargin{1}, 1) - 1, size(varargin{1}, 2) ))*violinval(1)...
                                            - abs(randn( size(varargin{1}, 1) - 1, size(varargin{1}, 2) ))*violinval(2); end;
        if( any(violinval(3:4)) );
            varargin{1}(end,:) = varargin{1}(end,:) ...
                                        + abs(randn( size(varargin{1}, 1) - 1, size(varargin{1}, 2) ))*violinval(3)...
                                        - abs(randn( size(varargin{1}, 1) - 1, size(varargin{1}, 2) ))*violinval(4); end; end;

                                
    [opt.unit, varargin] = parsem( {'unit'}, varargin, [] );    
    if( ~isempty(opt.unit) );
        if( isstring(opt.unit) || ischar(opt.unit) );
            opt.unit = char( opt.unit );
            switch opt.unit;
                case {''};
                    % do nothing
                case {'abs'};
                    varargin{1}(end,:) = abs( varargin{1}(end,:) );
                case {'atan'};
                    varargin{1}(end,:) = atan( varargin{1}(end,:) );
                case {'log','lin2log'};
                    varargin{1}(end,:) = log( varargin{1}(end,:) );
                case {'lin2abslog'};
                    varargin{1}(end,:) = abs( log(varargin{1}(end,:)) );
                case {'exp','log2lin'};
                    varargin{1}(end,:) = exp( varargin{1}(end,:) );
                case {'log2abslin'};
                    varargin{1}(end,:) = abs( exp(varargin{1}(end,:)) );
                case {'lin2db','lin2dB'};
                    varargin{1}(end,:) = 10*log10( varargin{1}(end,:) );
                case {'lin2absdb','lin2absdB'};
                    varargin{1}(end,:) = abs( 10*log10(varargin{1}(end,:)) );
                case {'db2lin','dB2lin'};
                    varargin{1}(end,:) = 10.^(varargin{1}(end,:)./10 );
                case {'db2abslin','dB2abslin'};
                    varargin{1}(end,:) = abs( 10.^(varargin{1}(end,:)./10) );
                otherwise
                    warning( 'plotm:unit', 'Wrong value for ''unit''. Option ignored.' ); end;
        elseif( isa( opt.unit, 'function_handle' ) && nargin(opt.unit) == 1 );
              varargin{1}(end,:) = opt.unit( varargin{1}(end,:) ); 
        else;
            error( 'plotm:unit', '''unit'' must be either a string or a unary function handle' ); end; end;

    [opt.xscale, varargin] = parsem( {'axisscale','scaleaxis','xscale','xaxisscale','axisscalex'}, varargin, [], 'postprocess',@tocell );
    [opt.yscale, varargin] = parsem( {'yscale','yaxisscale','axisscaley'}, varargin, [], 'postprocess',@tocell );
    [opt.zscale, varargin] = parsem( {'zscale','zaxisscale','axisscalez'}, varargin, [], 'postprocess',@tocell );
    try;
        if( ~isempty(opt.xscale{1}) );    % rescaling of axis labels is done at the end of the function
            varargin{1}(1,:) = opt.xscale{1}( varargin{1}(1,:) ); end;
        if( ~isempty(opt.yscale{1}) );
            varargin{1}(2,:) = opt.yscale{1}( varargin{1}(2,:) ); end;
        if( ~isempty(opt.zscale{1}) );
            varargin{1}(3,:) = opt.zscale{1}( varargin{1}(3,:) ); end;
    catch me;
        switch me.identifier;
            case 'MATLAB:innerdim';
                error( 'plotm:axis', 'You most likely used a scaling function which is not vectorized.\n  E.g. used matrix mulptiplication `*` instead of pointwise multiplication `.*` .' );
            otherwise;
                rethrow( me ); end; end;

    [opt.spherical, varargin] = parsem( {'spherical','sphere','hyperspherical','hypersphere','hs'}, varargin, [] );
    if( ~isempty(opt.spherical) )
        if( ~iscell(opt.spherical) );
            opt.spherical = {opt.spherical}; end;
        if( numel(opt.spherical) == 1 );
            opt.spherical{2} = 0; end;
        if( isnan(opt.spherical{2}) );
            opt.spherical{2} = min( varargin{1}(end,:) ); end;
        if( opt.spherical{2} == -inf ); 
            varargin{1} = sph2cartm( varargin{1}, opt.spherical{1} );
        elseif( opt.spherical{1} < inf )
            varargin{1}(end,:) = varargin{1}(end,:) - opt.spherical{2};
            varargin{1} = sph2cartm( varargin{1}, opt.spherical{1} );
        elseif( opt.spherical{1} == inf ); 
            varargin{1}(end,:) = sph2cartm2( varargin{1}(1:end-1,:), opt.spherical{1} );
        else
            warning( 'plotm:spherical', 'Wrong value for ''spherical''. Option ignored.' ); end; end;
    
    [opt.functional, varargin] = parsem( {'functional','func','funct','alg','algorithm','case'}, varargin, [] );
    if( isempty(opt.functional) );
        [val, varargin, functional_keyword, ~, contained] = parsem( {'kone','cone','kone*','cone*','minkowski','mink','lsr','minkowski*','minkowski**','mink*','mink**'}, varargin );
        if( contained );
            assert( val == 1 || val == 0, 'plotm:options', 'Options ''kone'', ''minkowski'', and ''lsr'' do not accept a parameter.' );
            opt.functional = functional_keyword; end; end;
    if( ~iscell(opt.functional) );
        opt.functional2 = 1;
    elseif( iscell(opt.functional) && numel(opt.functional) == 1 );
        opt.functional = opt.functional{1};
        opt.functional2 = 1;
    else;
        opt.functional2 = opt.functional{2};
        opt.functional = opt.functional{1}; end;
    if( ~isempty(opt.functional) );
        switch opt.functional;
            case {'k1','k2','k3','k4','k5','k6','k7'};  % do nothing
            case {'k','kone','cone'};               opt.functional = 'k';
            case {'k*','kone*','cone*'};            opt.functional = 'k*';
            case {'K','Kone','Cone'};               opt.functional = 'K';
            case {'K*','Kone*','Cone*'};            opt.functional = 'K*';
            case {'r','jsr','minkowski','mink'};    opt.functional = 'r';
            case {'minkowski*','mink*'};            opt.functional = 'r*';
            case {'minkowski**','mink**'};          opt.functional = 'r**';
            case {'l','lsr'};                       opt.functional = 'l';
            case {'c','complex'};                   opt.functional = 'c';
            case {'c1','c2','c3','c4','c5','c6'};   % do nothing
            otherwise; fatal_error; end; end;

    if( ~isempty(opt.functional) );
        if( isequal(opt.functional, -1) );
            if( ~isreal(varargin{1}) );
                opt.functional = 'c';
            elseif( any(varargin{1}(:) < 0) );
                opt.functional = 'r';
            elseif( all(varargin{1}(:) >= 0) );
                opt.functional = 'p';
            else;
                warning( 'plotm:functional', 'Could not determine functional type. Set <''functional'',''c''>.' );
                opt.functional = 'c'; end; end;
        expect( ischar(opt.functional) || isstring(opt.functional), 'plotm:functional', 'Could not determine functional type. Set <''functional'',''c''>.' );
        if( numel(varargin) >= 1 );
            switch lower( opt.functional );
                case {'k*','kone*'};
                    Q = varargin{1};
                    dim = size( Q, 1 );
                    num_pts = 20 * dim^2;
                    if( ~iskone( Q ) );
                        warning( 'plotm:function', 'Input is not a kone. The dual kone will be set to be the zero point.' );
                        pts = zeros( dim, 1 );
                    else;
                        % initial points
                        if( isempty(opt.auxiliary_data) );
                            opt.auxiliary_data = center_kone( Q, 'dual', 'verbose',-1, 'sym',false ); end;
                        pts = [-Q opt.auxiliary_data randn( dim, num_pts )];  % 20 is a quite arbitrary number
                        nrm = sqrt( sum( pts.^2, 1 ) );
                        pts = pts./nrm;
                        remove_idx = false( 1, size(pts, 2) );
                        for i = 1:size( pts, 2 );
                            angle = sum( pts(:,i) .* Q, 1 );
                            if( any( angle > 0 ) );
                                remove_idx(i) = true; end; end;
                        pts(:,remove_idx) = [];
                    
                        % make more points
                        delta = 3;  % distance of newly generated points
                        for rounds = 1:10;
                            new_pts = pts + delta*randn( size(pts) );
                            delta = delta/1.5;
                            nrm = sqrt( sum( new_pts.^2, 1 ) );
                            new_pts = new_pts./nrm;
                            remove_idx = false( 1, size(new_pts, 2) );
                            for i = 1:size( new_pts, 2 );
                                angle = sum( new_pts(:,i) .* Q, 1 );
                                if( any( angle >= 0 ) );
                                    remove_idx(i) = true; end; end;
                            new_pts(:,remove_idx) = [];
                            pts = [pts new_pts]; end; end;  %#ok<AGROW>
                    
                    if( size(pts, 2) < dim && opt.hull );
                        warning( 'plotm:kone', 'Could not compute a dual kone with non-empty interior. I remove the option ''hull''.' );
                        opt.arrow = 1;
                        opt.hull = 0; end;
                    pts = pts * opt.functional2;

                    if( isequal(lower(opt.functional), opt.functional) )
                        varargin{1} = reshape( [pts; zeros( size(pts) )], size(pts, 1), [] );
                    else;
                        if( ~isempty(PROJECTION) );
                            opt.auxiliary_data = PROJECTION * opt.auxiliary_data; end;
                        if( iscell(opt.auxiliary_data) );
                            assert( numel(opt.auxiliary_data) == 1, 'plotm:center', 'If center (auxiliary data) is given as a cell array, the cell array must only have 1 element.' );
                            opt.auxiliary_data = opt.auxiliary_data{1}; end;
                        varargin{1} = stereographic_projection( -pts, double(opt.auxiliary_data), 'rectilinear', true ); end;

                case {'k','kone','cone','k0','k1','k2','k3','k4','k5','k6','k7'};
                    Q = varargin{1};
                    nrm = sum( Q.^2, 1 );
                    idx_nrm = nrm < 1e-12;
                    expect( ~any(idx_nrm), 'plotm:Kone:smallmagnitude', 'Some points were removed from the input set, because they had norm less than 1e-12' ); 
                    Q(:,idx_nrm) = [];
                    if( isequal(lower(opt.functional), opt.functional) );  % lower case version
                        ex = 2;
                        nrm = ( sum( abs( Q ).^(ex), 1 ) ).^(1/ex);
                        %longest = max( nrm );
                        Q = Q./nrm;
                        if( opt.hull && size(Q, 2) > 1 );
                            Q = interpolate_kone( Q ); end;
                        %Q = Q * longest;
                        Q = Q * opt.functional2;
                        varargin{1} = reshape( [Q; zeros( size(Q) )], size(Q, 1), [] );
                    else;
                        if( isempty(opt.auxiliary_data) );
                            expect( ~ishold, 'plotm:Kone', 'No center vector for projection of kone is given, but figure has hold state `on`. This is most likely not what was wanted.' );
                            opt.auxiliary_data = center_kone( Q, 'verbose',-1, 'sym',false ); end;

                        if( ~isempty(PROJECTION) );
                            opt.auxiliary_data = PROJECTION * opt.auxiliary_data; end;
                        if( iscell(opt.auxiliary_data) );
                            assert( numel(opt.auxiliary_data) == 1, 'plotm:center', 'If center (auxiliary data) is given as a cell array, the cell array must only have 1 element.' );
                            opt.auxiliary_data = opt.auxiliary_data{1}; end;
                        [varargin{1}, ~, ~, proj_err] = stereographic_projection( Q, double(opt.auxiliary_data), 'rectilinear', true );
                        if( proj_err );
                            opt.auxiliary_data = center_kone( Q, 'verbose',-1, 'sym',false );
                            [QQ, ~, ~, proj_err] = stereographic_projection( Q, double(opt.auxiliary_data), 'rectilinear', true );
                            if( proj_err );
                                warning( 'plotm:Kone', 'Some points were outside the projectable domain.' );
                            else;
                                varargin{1} = QQ; 
                                warning( 'plotm:Kone', 'Some points were outside the projectable domain, and I used a different normal vector for projection.' ); end; end; end;

                case {'l'};
                    VV = varargin{1};
                    mx = 1.2 * max( abs(VV(:)) );
                    for ii = 1:size( varargin{1}, 1 );
                        val = varargin{1};
                        val([1:ii-1 ii+1:end],:) = mx;
                        VV = [VV val];  %#ok<AGROW>
                        val = varargin{1};
                        val(ii,:) = mx;
                        VV = [VV val]; end;  %#ok<AGROW>
                    varargin{1} = unique( [VV mx*ones( size(varargin{1}, 1), 1 )].', 'rows' ).';

                case {'p'};
                    VV = varargin{1};
                    for ii = 1:size( varargin{1}, 1 );
                        val = varargin{1};
                        val([1:ii-1 ii+1:end],:) = 0;
                        VV = [VV val];  %#ok<AGROW>
                        val = varargin{1};
                        val(ii,:) = 0;
                        VV = [VV val]; end;  %#ok<AGROW>
                    varargin{1} = unique( [VV zeros( size(varargin{1}, 1), 1 )].', 'rows' ).';

                case {'r','m','r*','m*','r**','m**'};
                    while( isequal(opt.functional(end), '*') );
                        varargin{1} = polar_set( varargin{1} );
                        opt.functional(end) = []; end;
                    varargin{1} = [varargin{1} -varargin{1}];

                case {'c'};
                    if( numel(opt.functional) == 1 );
                        opt.functional(2) = 180; end;  
                    N = size( varargin{1}, 2 );
                    t = linspace( 0, 2*pi, opt.functional(2) + 1 );
                    t = [t nan];
                    G = [cos(t);sin(t)];
                    nG = size( G, 2 );
                    val = zeros( size(varargin{1}, 1), N*nG );
                    for ii = 1:N;
                        val(:,((ii-1)*nG+1:ii*nG)) = [real( varargin{1}(:,ii) ) imag( varargin{1}(:,ii) )]*G; end; 
                    varargin{1} = val;

                otherwise;
                    error( 'plotm:functional', 'Wrong value passed to option ``funct``.' ); end; end; end;
    
end

function [ opt, varargin ] = parse_linespec_etc( opt, varargin );
        
    [opt.linespec, opt.linestyle, opt.markerstyle, opt.colorstyle] = deal('');
    found_linespec = 0;
    varargin = fliplr( varargin );
    for ii = numel( varargin ):-1:1;
        [linespec_, linestyle_, markerstyle_, colorstyle_] = parselinespec( varargin{ii} );
        if( ~isempty(linespec_) );
            [opt.linespec, opt.linestyle, opt.markerstyle, opt.colorstyle] = deal( linespec_, linestyle_, markerstyle_, colorstyle_ );
            found_linespec = found_linespec + 1;
            varargin(ii) = [];
            end; end;
    varargin = fliplr( varargin );
    expect( found_linespec <= 1, 'plotm:linespec', 'More than one linespec found. This may be an error in the input data.' );
    if(isempty(opt.colorstyle)); 
        color_ = get( gca, 'colororder' ); 
        color_ = color_(1,:);
        opt.colorgiven = [];
    else;
        color_ = opt.colorstyle;
        opt.colorgiven = opt.colorstyle; end;
    [opt.colorgiven, varargin] = parsem( {'Color','color','Colour','colour'}, varargin, opt.colorgiven );
    if( isempty(opt.colorgiven) );
        opt.colorgiven = false;
        opt.color = color_; 
    else;
        opt.color = opt.colorgiven; 
        opt.colorgiven = true; end;
    if( isstring(opt.color) || ischar(opt.color) );
        opt.color = char( opt.color );
        if( any(strcmpi(opt.color, {'scaled','scale'})) );
            opt.color = varargin{1}(end,:);
        elseif( strcmpi(opt.color, 'x') );
            opt.color = 'co1';
        elseif( strcmpi(opt.color, 'y') && ~strcmp(opt.colorstyle, 'y') );
            opt.color = 'co2';
        elseif( strcmpi(opt.color, 'z') );
            opt.color = 'co3'; end;
        if( numel(opt.color) >= 3 && strcmp(opt.color(1:2), 'co') );
            if( opt.color(3) == 'v' );
                opt.color = varargin{1}(end,:);
            else;
                idx = str2double( opt.color(3:end) );
                opt.color = varargin{1}(idx,:); end; 
            elseif( any(strcmp(opt.color, {'y','yellow'})) );
                opt.color = [1 1 0];
            elseif( any(strcmp(opt.color, {'m','magenta'})) );
                opt.color = [1 0 1];
            elseif( any(strcmp(opt.color, {'c','cyan'})) );
                opt.color = [0 1 1];
            elseif( any(strcmp(opt.color, {'r','red'})) );
                opt.color = [1 0 0];
            elseif( any(strcmp(opt.color, {'g','green'})) );
                opt.color = [0 1 0];
            elseif( any(strcmp(opt.color, {'b','blue'})) );
                opt.color = [0 0 1];
            elseif( any(strcmp(opt.color, {'w','red'})) );
                opt.color = [1 1 1];
            elseif( any(strcmp(opt.color, {'k','black'})) );
                opt.color = [0 0 0];
            elseif( any(strcmp(opt.color, {'mblue','matlabblue','matlab_blue'})) );
                opt.color = [0 0.4470 0.7410];
            elseif( any(strcmp(opt.color, {'morange','matlaborange','matlab_orange'})) );
                opt.color = [0.8500 0.3250 0.0980];
            elseif( any(strcmp(opt.color, {'myellow','matlabyellow','matlab_yellow'})) );
                opt.color = [0.9290 0.6940 0.1250];
            elseif( any(strcmp(opt.color, {'mpurple','matlabpurple','matlab_purple'})) );
                opt.color = [0.4940 0.1840 0.5560];
            elseif( any(strcmp(opt.color, {'mgreen','matlabgreen','matlab_green'})) );
                opt.color = [0.4660 0.6740 0.1880];
            elseif( any(strcmp(opt.color, {'mlightblue','matlablightblue','matlab_lightblue','mlblue','matlablblue','matlab_lblue'})) );
                opt.color = [0.3010 0.7450 0.9330];
            elseif( any(strcmp(opt.color, {'mred','matlabred','matlab_red'})) );
                opt.color = [0.6350 0.0780 0.1840];
            end; end;
    if( size( opt.color, 2 ) == 3 && min( opt.color(:) ) >= 0 && ...
        max( opt.color(:) ) > 1 && max( opt.color(:) ) <= 255 && ...
        all( iswholenumber(opt.color(:)) ) ...
      );
        opt.color = opt.color./255; end;

    [opt.arrow, varargin] = parsem( {'arrow','arrows','vector','vectors'}, varargin, tifh( isfield(opt,'arrow'), @() opt.arrow, 0 ) );
    if( opt.arrow );
        % to plot arrows, we need to use different plot commands, which expect different data. Thus we do not modify the data here, but at the plot site
        if( isempty(opt.linestyle) );
            opt.linestyle = '-';
            opt.linespec = ['-' opt.linespec]; end;
        % sze = size( varargin{1} );
        % varargin{1} = [zeros( sze );
        %                varargin{1};
        %                nan( sze )];
        % varargin{1} = reshape( varargin{1}, sze(1), [] );
        end;
end

function [ varargin ] = replace_shortcuts( varargin );
    varargin = replace_shortcuts_worker( 'lw', 'linewidth', varargin{:} );
end

function [ varargin ] = replace_shortcuts_worker( old, new, varargin );
    idx = strcmp( old, varargin );
    if( nnz(idx) == 1 );
        varargin{idx} = new; end;
end

function [ varargin ] = process_projection( opt, varargin );
    if( ~isempty(opt.proj) );
        fig = opt.fig;
        PROJECTION = getappdata( fig, 'ttoolboxes_plotm_PROJECTION' );  % get PROJECTION matrix stored in figure
        if( ~ischar(opt.proj) && isvector(opt.proj) && numel(opt.proj) == 3 );
            assert( opt.proj(1) == -3, 'plotm:proj', 'If a three element vector is given, the first number must be ''-3'', followed by the `from`-dimension and the `to`-dimension.' );
            if( isempty(PROJECTION) || ...
                ~isequal( size(PROJECTION), opt.proj([3 2]) )...
              );
                PROJECTION = make_projection( opt.proj(2), opt.proj(3), opt.functional ); end;
        elseif( ~ischar(opt.proj) && isvector(opt.proj) && numel(opt.proj) == 2 );
            % generate random projection matrix, with dimensions given by vector (from, to)
            if( opt.proj(1) == -1 );
                assert( numel(varargin) >= 1, 'plotm:proj', 'If ''-1'' is given as entry in projection vector, some data to be plotted must be passed to.' );
                opt.proj(1) = size( varargin{1}, 1 ); end;
            if( opt.proj(2) == -1 );
                opt.proj(2) = 3; end;
            PROJECTION = make_projection( opt.proj(1), opt.proj(2), opt.functional );
        elseif( isanyof(opt.proj, {0, 'delete','del','none'}) );
            % delete projection matrix
            PROJECTION = [];
        elseif( isanyof(opt.proj, {-1,'random','new'}) );
            % generate random projection matrix, with dimensions given by input
            assert( numel(varargin) == 1, 'plotm:proj', 'If ''-1'' is given as projection argument, some data to be plotted must be passed to.' );
            PROJECTION = make_projection( size(varargin{1}, 1), 3, opt.functional );
        elseif( isanyof(opt.proj, {-2, 'old'}) );
            % doing the projection of varargin{1} is already done beforehand
            return;
        elseif( ~ischar(opt.proj) && ismatrix(opt.proj) && isscalar(opt.proj) )
            PROJECTION = rand( opt.proj, size(varargin{1}, 1) );
        elseif( ~ischar(opt.proj) && ismatrix(opt.proj) && (numel(varargin) == 0 || size(opt.proj, 2) == size(varargin{1}, 1)) );  % size of varargin{1} test is only done if there is a varargin{1}
            % use input as projection matrix
            PROJECTION = opt.proj;
        elseif( isanyof(opt.proj, {'pert','perturbate'}) );
            % perturbate old projection matrix slightly (if there exists one)
            if( ~isempty(PROJECTION) );
                PROJECTION = make_projection( PROJECTION, opt.functional ); end;
        else;
            error( 'plotm:proj', 'Wrong argument to option ''projection'' is given.' ); end;
        setappdata( fig, 'ttoolboxes_plotm_PROJECTION', PROJECTION );  % store PROJECTION matrix into figure

        if( ~isempty(PROJECTION) && numel(varargin) >= 1 );
            assert( size(PROJECTION, 2) == size(varargin{1}, 1), 'plotm:proj', 'Dimensions of stored projection matrix is wrong.' );
            vprintf( 'Projection Matrix:\n %v\n', PROJECTION, 'imp',[2 opt.verbose] );
            varargin{1} = PROJECTION * varargin{1};
            end; end;
end
%% plot functions
%%%%%%%%%%%%%%%%%%%%

function [ varargin ] = plot_hyperplane( opt, varargin );
    if( ~opt.hyperplane );
        return; end;
    Q = varargin{1};
    ax = opt.ax;
    assert( isvector(Q), 'plotm:halfplane', 'The argument to option ''halfplane'' must be a vector.' );
    
    if( iscolumn(Q) );
        Q = Q.'; end;
    nullsp = null( double(Q) );
    length = max( [ax.XLim ax.YLim ax.ZLim] );
    varargin{1} = length*[nullsp -nullsp];
end

function [ opt, varargin ] = plot_meta( opt, varargin );
    [opt, varargin] = plot_meta_box( opt, varargin{:} );
    [opt, varargin] = plot_meta_origin( opt, varargin{:} );
end
function [ opt, varargin ] = plot_meta_box( opt, varargin );
    if( ~opt.box );
        return; end;
    if( isequal( opt.box, 1 ) );
        opt.box = numel( axis ) / 2; end;
    varargin{1} = mixvector( [0 1], opt.box );
    opt.hull = 1;
end
function [ opt, varargin ] = plot_meta_origin( opt, varargin );
    if( ~opt.origin);
        return; end;
    if( isequal( opt.origin, 1 ) );
        opt.origin = numel( axis ) / 2; end;
    varargin{1} = zeros( opt.origin, 1 );
end

%%

function plotinterval1( Q, opt, varargin )

    if( isempty(opt.height) );
        opt.height = 0; end;

    nn = numel(Q);
    if( numel(opt.height) == 1 ); 
        opt.height = [opt.height opt.height + 1]; end;
    X = vertcat( Q{:} )'; 
    X = [X; flipud(X)];
    Y = [opt.height(1)*ones( 2, nn ); opt.height(2)*ones( 2, nn )];
    try;
        patch( opt.ax, 'XData',X, 'YData',Y, varargin{:} );
    catch me;
        if( opt.verbose >= 2 );
            warning( 'plotm:options', 'Wrong plot options. All options are ignored.\n Thrown error:\n%s', dispc(me) );
        else;
            warning( 'plotm:option', 'Wrong plot options. All options are ignored.' ); end;
        
        patch( opt.ax, 'XData',X, 'YData',Y ); end;

    if( opt.verbose >= 1 );
        val1 = sum( diff(vertcat(Q{:}), 1, 2) );
        val2 = sum( abs(diff(vertcat(Q{:}), 1, 2)) );
        if( val1 ~= val2 );
            if( opt.verbose >= 2 )
                fprintf( 'Area (oriented/unoriented): %i / %i\n', val1, val2 ); end;
        else;
            if( opt.verbose >= 2);
                fprintf('Area: %i\n',val1); end; end; end;

end

function plotm1( Q, opt, varargin );
    if( isempty(opt.height) );
        opt.height = 1; end;
    ms = tif( isempty(opt.markersize), markersize_default(), opt.markersize );
    if( isreal(Q) );
        done = false;
        try;
            if( ~isempty(opt.linespec) );
                % quiver does not accepts linespecs. thus we do not use it.
                % stem from R2023b does not accepts linespecs. thus we may have a problem
                stem( opt.ax, Q, opt.height.*ones(1, size(Q, 2)), opt.linespec, varargin{:}, 'Color',opt.color, 'MarkerSize',ms ); 
            else
                stem( opt.ax, Q, opt.height.*ones(1, size(Q, 2)), varargin{:}, 'Color',opt.color, 'MarkerSize',ms ); 
                end;
            done = true;
        catch me;  %#ok<NASGU>
            end;
        try;
            if( ~done );
                stem( opt.ax, Q, ones(1, size(Q, 2)), opt.linespec, 'Color',opt.color, 'MarkerSize',ms); 
                warning( 'plotm:option', 'Wrong plot options. Some options are ignored.' );
                done = true; end;
        catch me;  %#ok<NASGU>
            end;
        try;
            if( ~done );
                stem( opt.ax, Q, opt.height.*ones(1, size(Q, 2)),  varargin{:}, 'Color',opt.color, 'MarkerSize',ms ); 
                warning( 'plotm:option', 'Wrong plot options. Some options are ignored.' );
                done = true; end;
        catch me;  %#ok<NASGU>
            end;
        try;
            if( ~done );
                stem( opt.ax, Q, ones(1, size(Q, 2)), 'Color',opt.color, 'MarkerSize',ms); 
                warning( 'plotm:option', 'Wrong plot options. Some options are ignored.' );
                done = true; end;
        catch me;  %#ok<NASGU>
            end;
        try;
            if( ~done );
                stem( opt.ax, Q, ones(1, size(Q, 2)) );
                warning( 'plotm:option', 'Wrong plot options. All options are ignored.' );
                done = true; end;
        catch me;  %#ok<NASGU>
            end;
        try;
            if( ~done );
                stem( opt.ax, Q, ones(1, size(Q, 2)),'Color',opt.color, 'MarkerSize',ms);
                warning( 'plotm:option', 'Wrong plot options. Some options are ignored.' );
                done = true; end;
        catch me;  %#ok<NASGU>
            end;
        if( ~done );
            error( 'plotm:option', 'Wrong plot options. Could not plot.' ); end;
    else;
        if( ~isempty(opt.linespec) );
            LINESPEC_ = {opt.linespec};
        else;
            LINESPEC_ = {}; end;
        plotm( opt.ax, [real(Q); imag(Q)], LINESPEC_{:}, 'Color',opt.color, 'MarkerSize',ms, varargin{:} );
        xlabel( 'Re' );
        ylabel( 'Im' ); end;
end
  
function plotm2( Q , opt, varargin );
    idx = ~any( isnan(Q), 1 );  % remove nans
    Qnonan = Q(:,idx);
    dim = size( Q, 1 );
    try;
        if( false );
        elseif( opt.arrow );
            if( isequal(opt.arrow, 1) );
                arrow_arg = 0;
            else
                arrow_arg = opt.arrow; end;
            ms = tif( isempty(opt.markersize), markersize_default(), opt.markersize );
            try;
                num_pts = size( Q, 2 );
                quiver( opt.ax, zeros(1, num_pts), zeros(1, num_pts), Q(1,:), Q(2,:), arrow_arg, opt.linespec, 'Color',opt.color, 'MarkerSize',ms, varargin{:} );
            catch me;  %#ok<NASGU>
                warning( 'plotm:option', 'Wrong plot options. Some (or all) options are ignored.' );
                try;
                    quiver( opt.ax, zeros(1, num_pts), zeros(1, num_pts), Q(1,:), Q(2,:), arrow_arg, opt.linespec, 'Color',opt.color, 'MarkerSize',ms );
                catch me;  %#ok<NASGU>
                    quiver( opt.ax, zeros(1, num_pts), zeros(1, num_pts), Q(1,:), Q(2,:), arrow_arg ); end; end;
        elseif( opt.hull && size(Q, 2) > dim );
            try;
                idx = convhull( Qnonan(1,:), Qnonan(2,:) );
                try;
                    ms = tif( isempty(opt.markersize), markersize_default(), opt.markersize );
                    color_ = opt.color;
                    if( isscalar(color_) );
                        color_ = num2color( color_ ); end;
                    if( opt.patch );
                        patch( opt.ax, Qnonan(1,idx), Qnonan(2,idx), opt.color, 'FaceColor',color_, varargin{:} );
                    else;
                        plot( opt.ax, Qnonan(1,idx),Qnonan(2,idx), opt.linespec, 'Color',color_, 'MarkerSize',ms, varargin{:} ); 
                         end;
                catch me;
                    warning( 'plotm:option', 'Wrong plot options. Some options are ignored. Thrown error:\n%s\n', dispc(me), dispc(me.stack(1)) );
                    if( opt.verbose >= 2 );
                        disp( me ); end;
                    try;
                        patch( opt.ax, Qnonan(1,idx), Qnonan(2,idx), opt.color, varargin{:} );
                    catch me;
                        warning( 'plotm:option', 'Wrong plot options. Some options are ignored. Thrown error:\n%s\n', dispc(me), dispc(me.stack(1)) );
                        patch( opt.ax, Qnonan(1,idx), Qnonan(2,idx), opt.color );
                        end; end;
            catch me;
                switch me.identifier;
                    case {'MATLAB:convhull:EmptyConvhull2DErrId','MATLAB:convhull:NotEnoughPtsConvhullErrId'};
                        vprintf( 'Could not construct hull.\n', 'cpr','err', 'imp',[1 opt.verbose] );
                        opt.hull = 0;
                        plotm2( Q, opt, varargin{:} );
                    otherwise;
                        throw( me ); end; end;            

        elseif( opt.delaunay );
            DT = delaunay( Q(1,:), Q(2,:) );
            triplot( opt.ax, DT, Q(1,:), Q(2,:), opt.linespec, varargin{:} );
        elseif( ~isempty(opt.boundary) );
            if( 1 >= opt.boundary && opt.boundary >= 0 );
                ms = tif( isempty(opt.markersize), markersize_default(), opt.markersize );
                % opt.boundary = -opt.boundary;
                idx = boundary( Qnonan(1,:).',Qnonan(2,:).', opt.boundary );  % compute boundary using Matlabs function
                plot( Q(1,idx),Q(2,idx), opt.linespec, 'Color',opt.color, 'MarkerSize', ms, varargin{:} );                
            elseif( opt.boundary > 1 );
                ms = tif( isempty(opt.markersize), markersize_default(), opt.markersize );
                opt.boundary = opt.boundary-1;
                D = pdist2( Qnonan.', Qnonan.', 'cityblock', 'Smallest',10);  % compute distance to other points
                DD = sum( D, 1 );  % compute something like an average distance
                DDD = cumsum( sort(DD-mean(DD)) );
                [~,idx] = min( DDD );
                bar = abs( DDD(idx+1) - DDD(idx) ) + mean( DD )*opt.boundary;
                idx = DD > bar;  % choose vertices which have high distance to other points. Thus, they are likely to be at the boundary
                plot( opt.ax, Qnonan(1,idx), Qnonan(2,idx), opt.linespec, 'Color',opt.color, 'MarkerSize',ms, varargin{:} );
            else;
                ms = tif( isempty(opt.markersize), markersize_default(), opt.markersize );
                opt.boundary = -opt.boundary;
                DT = delaunayTriangulation( Q.' );  % compute delauny triangulation
                AA = DTarea( DT ).';                % compute area of triangles
                AAA = cumsum( sort(AA-mean(AA)) );  % compute something like an average value of the areas
                [~,idx] = min( AAA );
                bar = AAA(idx(1)+1) - AAA(idx(1)) + mean( AA )*opt.boundary;
                idx = AA <= bar;  % select triangles with area less than bar
                EDGE = [DT.ConnectivityList(idx,[1 2]); DT.ConnectivityList(idx,[2 3]); DT.ConnectivityList(idx,[3 1])];  % edges of chosen triangles
                EDGE = sort( EDGE.' ).';  % remove all edges which occur multiple times
                [~, ~, idx] = unique( EDGE, 'rows' );
                counts = histc( idx, unique(idx) );
                counts = counts(idx);
                idx = counts == 1;
                EDGE = EDGE(idx,:);
                plot( opt.ax, [Qnonan(1,EDGE(:,1) ); Qnonan(1,EDGE(:,2))], [Qnonan(2,EDGE(:,1) ); Qnonan(2,EDGE(:,2))], opt.linespec, 'Color',opt.color, 'MarkerSize',ms, varargin{:} ); end;
        else;
            if( numel(opt.color) == size(Q, 2) );
                ms = tif( isempty(opt.markersize), 1, opt.markersize );
                if( ~isempty(opt.linestyle) );
                    try;  %#ok<TRYNC>
                        if( isempty(opt.markerstyle) );
                            opt.markerstyle = '.'; end;
                        holdflag = ishold;
                        if( ~holdflag );
                            clear_figure_content();
                            end;
                        try;
                            hold on;
                            plot( opt.ax, Q(1,:), Q(2,:), [opt.markerstyle, opt.linestyle, opt.colorstyle], 'MarkerSize',ms, varargin{:} );
                        catch me;  %#ok<NASGU>
                            plot( opt.ax, Q(1,:), Q(2,:), [opt.markerstyle, opt.linestyle, opt.colorstyle], 'MarkerSize',ms ); end;  % for plotting the line style
                        scatter( opt.ax, Q(1,:), Q(2,:), ms^2, opt.color, opt.markerstyle, varargin{:} );  % Markersize of scatterplot is square-root of size of plot
                        if( ~holdflag );
                            hold off; end; end; 
                else;
                    scatter( opt.ax, Q(1,:), Q(2,:), opt.markersize^2, opt.color, opt.markerstyle, varargin{:} ); end;                    
            else;
                ms = tif( isempty(opt.markersize), markersize_default(), opt.markersize );
                if( size(opt.color, 2) == 1 );
                    color_ = num2color( opt.color );
                else;
                    color_ = opt.color; end;
                plot( opt.ax, Q(1,:), Q(2,:), opt.linespec, 'Color',color_, 'MarkerSize',ms, varargin{:} ); end; end;
    catch me;
        if( opt.verbose >= 2 );
            disp( me ); end;
        warning( 'plotm:option', 'Wrong plot options. All options are ignored.\n  Error id: %s\n  Error msg: %s', me.identifier, me.message );
        plot( opt.ax, Q(1,:), Q(2,:) ); end;
    xlabel( 'x' );
    ylabel( 'y' );
    zlabel( 'z' );
end

function plotm3( Q, opt, varargin );
    if( ~isa(Q,'double') );
        warning( 'plotm:inputtype', 'Input points not of type double. Values are cast to double.' );
        Q = double( Q ); end;
    if( numel(opt.res) == 1 ); 
        opt.res = [opt.res opt.res]; end;
    x = Q(1,:).';
    y = Q(2,:).';
    z = Q(3,:).';
    idx = ~(isnan(x) | isnan(y) | isnan(z));
    xnonan = x(idx);
    ynonan = y(idx);
    znonan = z(idx);
    Q = [];  %#ok<NASGU>  % free space

    minx = min( x ); 
    maxx = max( x );
    miny = min( y ); 
    maxy = max( y );
    % test if we plot surface or points.
    % we plot only points be default, if there are more z values for the same pair (x,y) in Q
    if( opt.res(1) == -inf )
        opt.res = [max( 33, ceil(sqrt(length(x))) ), max( 33, ceil(sqrt(length(y))) )]; end;
    resval = [round( x/((maxx-minx)/(3*opt.res(1))) ), round( y/((maxy-miny)/(3*opt.res(2))) )];
    resval = unique( resval, 'rows' );
    if( isequal(opt.contour, 0) && isequal(opt.surface, 0) && isequal(opt.point, 0) && isequal(opt.hull, 0) && isempty(opt.boundary) )
        if( numel( x ) - 10 > size( resval, 1 ) || ...
            numel( x ) <= 2 || ...
            ~isempty( opt.linespec ) || ...
            ~isempty( opt.functional ) ...
          ); 
            opt.point = 1;
        else;
            opt.surface = 1; end; end;

    if( opt.surface && ~isempty(opt.linespec) );
        opt.point = 1; end;
    if( ~isequal(opt.surface, 0) || ~isequal(opt.contour, 0) );
        if( opt.contour );  %#ok<IFBDUP>
            extrapolate = {'none'};
        else;
            extrapolate = {'none'}; end;
        % XX remove warning
        xlin = linspace( minx, maxx, opt.res(1) );
        ylin = linspace( miny, maxy, opt.res(2) );
        [X,Y] = meshgrid( xlin, ylin );
        try;
            f = scatteredInterpolant( xnonan, ynonan, znonan, 'linear', extrapolate{:} );  % linear interpolation, none extrapolation
            Z = f( X, Y );
        catch me;
            if( opt.verbose >= 2 );
                disp( me ); end;        
            Z = []; end;
        if( isempty(Z) );
            opt.surface = 0; 
            opt.contour = 0; 
            opt.point = 1; end; end;

    if( ~isequal(opt.surface, 0) );
        try; 
            h = mesh( opt.ax, X, Y, Z, 'EdgeColor',opt.color, varargin{:} );  % interpolated
        catch me; 
            if( opt.verbose >= 2 );
                disp( me ); end;        
            warning( 'plotm:option', 'Wrong plot options. All options are ignored.' );
            h = mesh( opt.ax, X, Y, Z ); end;  % interpolated
        if( all(isnan(h.FaceNormals(:))) );
            warning( 'plotm:surface', 'Surface could not be computed. Plot points instead.' );
            opt.point = true; end; end;      
    
    if( opt.arrow );
        if( isequal(opt.arrow, 1) );
            arrow_arg = 0;
        else;
            arrow_arg = opt.arrow; end;

        ms = tif( isempty(opt.markersize), markersize_default(), opt.markersize );
        try;
            num_pts = numel( x );
            quiver3( opt.ax, zeros(num_pts, 1), zeros(num_pts, 1), zeros(num_pts, 1), x, y, z, arrow_arg, opt.linespec, 'Color',opt.color, 'MarkerSize',ms, varargin{:} );
        catch me;  %#ok<NASGU>
            warning( 'plotm:option', 'Wrong plot options. Some (or all) options are ignored.' );
            try;
                quiver3( opt.ax, zeros(1, num_pts), zeros(1, num_pts), zeros(1, num_pts), x, y, z, arrow_arg, opt.linespec, 'Color',opt.color, 'MarkerSize',ms );
            catch me;  %#ok<NASGU>
                quiver3( opt.ax, zeros(1, num_pts), zeros(1, num_pts), zeros(1, num_pts), x, y, z, arrow_arg ); end; end;
    elseif( opt.point );
        ms = tif( isempty(opt.markersize), markersize_default(), opt.markersize );
        try;
            color_ = opt.color;
            if( parsem('filled', varargin) );
                scatter3( opt.ax, x, y, z, ms^2, color_, varargin{:} );
            else;
                scatter3( opt.ax, x, y, z, ms^2, color_, opt.linespec, varargin{:} ); end;
        catch me; 
            if( opt.verbose >= 2 );
                disp( me ); end;        
            try; 
                plot3( opt.ax, x, y, z, opt.linespec, 'MarkerSize',ms, 'Color',opt.color, varargin{:} );
            catch me;
                if( opt.verbose >= 2 );
                    disp( me ); end;    
                warning( 'plotm:option', 'Wrong plot options. All options are ignored.' );
                scatter3( opt.ax, x, y, z ); end; end; end;

    if( ~isequal(opt.contour, 0) )
        if( isequal(opt.contour, 1) );
            contour_ = {}; 
        else;
            contour_ = {opt.contour}; end;
        mtry( {
            {@() contour3( opt.ax, X, Y, Z, contour_{:}, opt.linespec, 'LineColor',opt.color, varargin{:} )}
            {@() contour3( opt.ax, X, Y, Z, contour_{:}, opt.linespec, varargin{:} )}
            {@() contour3( opt.ax, X, Y, Z, contour_{:}, opt.linespec )}
            {@() contour3( opt.ax, X, Y, Z, contour_{:} ), @() warning( 'plotm:option', 'Wrong plot options. All options are ignored.' )}
            } ); end;
    
    if( ~isequal(opt.hull, 0) );
        try;
            if( numel(x) <= 4 );
                K = nchoosek(1:numel(x),3);
            else;
                try;
                    err = lasterror();  %#ok<LERR>
                    K = convhulln( [xnonan ynonan znonan], {'Pp','Qt'} );
                catch me;
                    lasterror( err );  %#ok<LERR>
                    if( opt.verbose >= 2 );
                        disp( me ); end;
                    K = convhulln( [xnonan ynonan znonan], {'Pp','Qt','QJ'} ); end; end;  % if input is not full dimensional
            try;
                if( opt.colorgiven );
                    if( strcmp(opt.color, 'norm') );
                        color_ = {sum( [xnonan ynonan znonan].^2, 2 ).^(1/2)};
                    elseif( numel(opt.color) == 1 );
                        color_ = {repmat( opt.color, [numel(xnonan) 1] )};
                    elseif( numel(opt.color) == 3 || ischar(opt.color) );
                        color_ = {'FaceColor',opt.color};
                    else;
                        error( 'plotm:option', 'wrong value for COLOR' ); end;
                    trisurf( K, xnonan, ynonan, znonan, color_{:}, 'Parent',opt.ax, varargin{:} ); 
                else;
                    trisurf( K, xnonan, ynonan, znonan, 'Parent',opt.ax, varargin{:} ); end;
            catch me;
                if( opt.verbose >= 1 );
                    disp( me ); end;    
                warning( 'plotm:option', 'Wrong plot options. All options are ignored.' );
                trisurf( K, xnonan, ynonan, znonan, 'Parent',opt.ax ); end;
        catch me;
            if( opt.verbose >= 2 );
                disp( me ); end;        
            warning( 'plotm:hull', 'Could not construct hull.' ); end; end;
    if( ~isempty(opt.boundary) );
        k = boundary( [xnonan ynonan znonan], opt.boundary );
        try;
            if( numel(opt.color) == numel(x) );
                trisurf( k, xnonan, ynonan, znonan, opt.color, 'Parent',opt.ax, varargin{:} );
            else;
                trisurf( k, xnonan, ynonan, znonan, 'Parent',opt.ax, varargin{:} ); end;
        catch me;
            if( opt.verbose >= 1 );
                disp( me ); end;        
            warning( 'plotm:option', 'Wrong plot options. All options are ignored.' );
            trisurf( k, xnonan, ynonan, znonan, 'Parent',opt.ax ); end; end;
    axis tight;

end

%% post process
%%%%%%%%%%%%%%%%%%%%%%
function postprocess_logaxis( opt );
    if( ~isequal(opt.logaxisflag, 0) );
        if( ~iscell(opt.logaxis) );
            opt.logaxis = {opt.logaxis}; end;
        while( ~isempty(opt.logaxis) );
            switch opt.logaxis{end};
                case {'linlin'};
                    set( gca, 'XScale', 'linear' );
                    set( gca, 'YScale', 'linear' );
                case {'linlinlin'};
                    set( gca, 'XScale', 'linear' );
                    set( gca, 'YScale', 'linear' );
                    set( gca, 'ZScale', 'linear' );
                case {'loglin'}; 
                    set( gca, 'XScale', 'log' );
                    set( gca, 'YScale', 'linear' );
                case {'linlog'}; 
                    set( gca, 'XScale', 'linear' );
                    set( gca, 'YScale', 'log' );
                case {'loglinlin'}; 
                    set( gca, 'XScale', 'log' );
                    set( gca, 'YScale', 'linear' );
                    set( gca, 'ZScale', 'linear' );
                case {'linloglin'}; 
                    set( gca, 'XScale', 'linear' );
                    set( gca, 'YScale', 'log' );
                    set( gca, 'ZScale', 'linear' );
                case {'loglinlog'}; 
                    set( gca, 'XScale', 'log' );
                    set( gca, 'YScale', 'linear' );
                    set( gca, 'ZScale', 'log' );
                case {'linloglog'}; 
                    set( gca, 'XScale', 'linear' );
                    set( gca, 'YScale', 'log' );
                    set( gca, 'ZScale', 'log' );
                case {'logx','log','semilogx'}; 
                    set( gca, 'XScale', 'log' );
                case {'logy','semilogy'}; 
                    set( gca, 'YScale', 'log' );
                case {'logz','semilogz'};
                    set( gca, 'ZScale', 'log' );
                case {'logc','semilogc'};
                    set( gca, 'ColorScale', 'log' );
                case {'loga','semiloga'};
                    set( gca, 'AlphaScale', 'log' );                     
                case {'loglog','logloglin','logxy','semilogxy'}; 
                    set( gca, 'XScale', 'log' ); 
                    set( gca, 'YScale', 'log' );
                case {'logxz','semilogxz'}; 
                    set( gca, 'XScale', 'log' ); 
                    set( gca, 'ZScale', 'log' );
                case {'logyz','semilogyz'}; 
                    set( gca, 'YScale', 'log' ); 
                    set( gca, 'ZScale', 'log' );                    
                case {'logloglog','logxyz','semilogxyz'}; 
                    set( gca, 'XScale', 'log' ); 
                    set( gca, 'YScale', 'log' );                     
                    set( gca, 'ZScale', 'log' );
                otherwise;
                    error( 'plotm:fatal', 'programming error.' ); end; 
             opt.logaxis(end) = []; end; end
end

function postprocess_xyzscale( opt );
    try;
         if( ~isempty(opt.xscale{1}) );
             rescale_axis( opt.xscale, 'x' ); end;
         if( ~isempty(opt.yscale{1}) );
             rescale_axis( opt.yscale, 'y' ); end;
         if( ~isempty(opt.zscale{1}) );
             rescale_axis( opt.zscale, 'z' ); end;
    catch me;  %#ok<NASGU>
        end;
end

function postprocess_label( opt );
    if( iscell(opt.label) );
        xlabel( opt.label{1} );
        ylabel( opt.label{2} );
        zlabel( opt.label{3} );
    elseif( ~isequal(opt.label, 0) );
        xlabel( 'x' );
        ylabel( 'y' );
        zlabel( 'z' ); end;
end

function postprocess_misc( opt ); 
    if( ~isempty(opt.alpha) );
        alpha( opt.alpha ); end;

    if( ~isempty(opt.sameaxis) );
        sameaxis( opt.sameaxis ); end;    
    
    if( ~isempty(opt.linkaxis) );
        linkaxis( opt ); end;
    
    if( opt.xkcd );
        try;
            err = lasterror();  %#ok<LERR>
            xkcdify( gca );
        catch me;
            switch me.identifier
                case {'MATLAB:UndefinedFunction','Octave:undefined-function'};
                    lasterror( err );  %#ok<LERR>
                    xkcdify_fallback( gca );
                otherwise;
                    throw( me ); end; end; end;

    if( opt.darkmode );
        try;
            err = lasterror();  %#ok<LERR>
            plot_darkmode( gca );
        catch me;
            switch me.identifier
                case {'MATLAB:UndefinedFunction','Octave:undefined-function'};
                    lasterror( err );  %#ok<LERR>
                    plot_darkmode_fallback();
                otherwise;
                    throw( me ); end; end; end;
end

function postprocess_scene( opt );
    
    switch opt.scene;
        case false;
            % do nothing
        case {1,'default','def'};
            %shading interp  % removes colors of surface

            h = findobj( opt.ax, '-property','EdgeColor' );
            set( h, 'EdgeColor','none' );

            %h = findobj( opt.ax, '-property','EdgeAlpha' );
            %set( h, 'EdgeAlpha',.2 );

            h = findobj( opt.ax, '-property','FaceLighting' );
            set( h, 'FaceLighting','flat' );

            h = findobj( opt.ax, '-property','BackFaceLighting ' );
            set( h, 'BackFaceLighting','lit' );

            lighting flat;
            light;
            camlight
            material dull
        otherwise;
            error( 'plotm:scene', 'Unkown ''scene'' preset given.' ); end
end

function postprocess_view( opt, varargin );
    if( ~isempty(opt.title) );
        title( opt.ax, opt.title ); end;

    if( numel(varargin) >= 1 && isequal(opt.functional, 'p') );
        if( size(varargin{1}, 1) >= 3 );
             view( 135, 30 ); end;
        ax_val = axis;
        if( size(varargin{1}, 1) >= 3 );
            ax_val([1 3 5]) = 0;
         else;
            ax_val([1 3]) = 0; end;
         axis( ax_val ); end;
end

%% other functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ A ] = DTarea( DT );
    N = size( DT.ConnectivityList, 1 );
    A = zeros( N, 1 );
    for n = 1:N
        idx = DT.ConnectivityList(n,:);
        p1 = DT.Points(idx(1),:);
        p2 = DT.Points(idx(2),:);
        p3 = DT.Points(idx(3),:);
        A(n) = 1/2*abs( (p2(1)-p1(1))*(p3(2)-p1(1))-(p3(1)-p1(1))*(p2(2)-p1(2)) ); end;
end

function linkaxis( opt )
    persistent LinkX LinkY LinkZ LinkC LinkCam;
            
    if( isequal(opt.linkaxis, 2) );
        opt.linkaxis = 'xyzc'; end;
    if( isequal(opt.linkaxis, 1) || isequal(opt.linkaxis, 3) );
        opt.linkaxis = 'xyzcv'; end;    
    
    [opt.linkaxis, ~, link.v] = containsstring( opt.linkaxis, {'v','Cam','cam'} ); 
    [opt.linkaxis, ~, link.x] = containsstring( opt.linkaxis, {'x','Cam','cam'} ); 
    [opt.linkaxis, ~, link.y] = containsstring( opt.linkaxis, {'y','Cam','cam'} ); 
    [opt.linkaxis, ~, link.z] = containsstring( opt.linkaxis, {'z','Cam','cam'} ); 
    [opt.linkaxis, ~, link.c] = containsstring( opt.linkaxis, {'c','Cam','cam'} ); 
    
    ax = findobj( 0, '-property', 'xlim', '-not', 'tag', 'colorbar' );
    
    % get extents
    xyzextents = [inf -inf inf -inf inf -inf];
    cextents = [inf -inf];
    for i = 1:numel( ax );
        try;  %#ok<TRYNC>
            xyzextents(1) = min( xyzextents(1), ax(i).XLim(1) );
            xyzextents(2) = max( xyzextents(2), ax(i).XLim(2) ); end;
        try;  %#ok<TRYNC>
            xyzextents(3) = min( xyzextents(3), ax(i).YLim(1) );
            xyzextents(4) = max( xyzextents(4), ax(i).YLim(2) ); end;
        try;  %#ok<TRYNC>
            xyzextents(5) = min( xyzextents(5), ax(i).ZLim(1) );
            xyzextents(6) = max( xyzextents(6), ax(i).ZLim(2) ); end;
        try;  %#ok<TRYNC>
            cextents(1) = min( cextents(1), ax(i).CLim(1) );
            cextents(2) = max( cextents(2), ax(i).CLim(2) ); end; end;
    
    % combine global extents
    for i = 1:numel( ax );
        axisi = axis( ax(i) );
        caxisi = caxis( ax(i) );
        if( link.x );
            axisi(1) = min( axisi(1), xyzextents(1) );
            axisi(2) = max( axisi(2), xyzextents(2) ); end;
        if( link.y );
            axisi(3) = min( axisi(3), xyzextents(3) ); ;
            axisi(4) = max( axisi(4), xyzextents(4) ); end;
        if( link.z && numel(axisi) >= 5 );
            axisi(5) = min( axisi(5), xyzextents(5) ); ;
            axisi(6) = max( axisi(6), xyzextents(6) ); end;
        if( link.c );
            caxisi(1) = min( caxisi(1), cextents(1) ); ;
            caxisi(2) = max( caxisi(2), cextents(2) ); end; 
        
        axis( ax(i), axisi );
        caxis( ax(i), caxisi ); end;
    
    if( link.v ); 
        LinkCam = linkprop( ax, {'CameraUpVector', 'CameraPosition', 'CameraTarget'} ); 
        %setappdata( opt.fig, 'LinkCam', LinkCam );
    elseif( ~isnumeric(LinkCam) );
        removeprop( LinkCam, 'CameraUpVector' ); 
        removeprop( LinkCam, 'CameraPosition' ); 
        removeprop( LinkCam, 'CameraTarget' ); end;
    
    if( link.x ); 
        LinkX = linkprop( ax, {'XLim'} ); 
        %setappdata( opt.fig, 'LinkX', LinkX );
    elseif( ~isnumeric(LinkX) );
        removeprop( LinkX, 'XLim' ); end;
    
    if( link.y  ); 
        LinkY = linkprop( ax, {'YLim'} ); 
        %setappdata( opt.fig, 'LinkY', LinkY );
     elseif( ~isnumeric(LinkY) );
        removeprop( LinkY, 'YLim' ); end;
    
    if( link.z ); 
        LinkZ = linkprop( ax, {'ZLim'} ); 
        %setappdata( opt.fig, 'LinkZ', LinkZ );
    elseif( ~isnumeric(LinkZ) );
        removeprop( LinkZ, 'ZLim' ); end;      
    
    if( link.c); 
        LinkC = linkprop( ax, {'CLim'} ); 
        %setappdata( opt.fig, 'LinkC', LinkC );
    elseif( ~isnumeric(LinkC) );
        removeprop( LinkC, 'CLim' ); end;       
    
    if( sum(opt.linkaxis) ~= 0 );
        warning( 'plotm:link', 'Wrong options for ''link''.' ); end;
end

function sameaxis( XYZC );
% SAMEAXES unifies/synchronizes axis limits on different axes and subplots.
%
% See also PBASPECT, DASPECT, LINKAXES, XLIM, YLIM, ZLIM.
% Created Jun/13 by Johannes Keyser (jkeyser@uni-osnabrueck.de)
% Revised Aug/13 by jkeyser: +arg HNDS to restrict axes search from parents
% Revised Okt/13 by jkeyser: +arg XYZC to generalize to any axis, +comments
% Revised Jan/14 by jkeyser: +polished for Matlab File Exchange publication
% Revised May/14 by jkeyser: +fixed check for handles, +exclusion example
% Changed by tommsch for plotm
    %XYZC = 'xyzc';
    if( isequal(XYZC, 1) || isequal(XYZC, 2) );
        XYZC = 'xyc'; end;    
    if( isequal(XYZC,3) );
        XYZC = 'xyzc'; end;        
    for xyzc = XYZC(:)' % iterate over x, y, z, c
        lim = [xyzc 'lim'];
        % find axes (== objects with color-limits - except colorbars)
        axs = findobj( 0, '-property',lim, '-not','tag','colorbar' );
        if isempty(axs)
            warning( 'plotm:nosuchlim', 'No children with "%s".', lim )
            continue; end;
        % get() and then set() the pooled min() & max() to all axes objects
        lims = get( axs, lim );
        if( iscell(lims) );
            lims = [lims{:}]; end % unpack if necessary
        set( axs, lim, [min(lims) max(lims)] ); end;
end

function [ LINESPEC, LINE, MARKER, COLOR ] = parselinespec( spec );
    if( ~ischar(spec) );
        [LINESPEC, LINE, MARKER, COLOR] = deal('');
        return; end;
    [spec, LINE] = containsstring( spec, {'--','-.',':','-'} );
    [spec, MARKER] = containsstring (spec, {'square','diamond','pentagram','hexagram','+','o','*','.','x','s','d','^','v','>','<','p','h',} );
    [spec, COLOR] = containsstring( spec, {'r','g','b','c','m','y','k','w'} );
    if( sum(spec) == 0 );
        LINESPEC = strcat( MARKER, LINE, COLOR );
    else
        COLOR = '';
        MARKER = '';
        LINE = '';
        LINESPEC = ''; end;
end

function [ strout, whatout, flag ] = containsstring( strin, whatin );
    whatout = '';
    strout = strin;
    flag = false;
    for i = whatin;
        val = strfind( strin, i );
        if( iscell(val) );
            val = val{1}; end;  % Octave hack
        if( ~isempty(val) );
            flag = true;
            whatout = i{1}; 
            for j = 1:numel( val );
                strout(val(j):val(j)+numel(i{1})-1) = 0; 
                return; end; end; end;
end

function unplot( arg );
%UNPLOT Delete the most recently created graphics object(s).
%
%   UNPLOT removes the most recently created line, surface, patch, or
%   text object created in the current axes. 
%
%   UNPLOT(N) removes the N most recent. 
%
%   UNPLOT SET takes a snapshot of the objects currently in the axes. 
%   UNPLOT REVERT removes any objects drawn since the last SET.
%
%   Note: UNPLOT does not affect objects added through the figure menus
%   and buttons. 
% Copyright 2002-2003 by Toby Driscoll (driscoll@na-net.ornl.gov).
% 17 Mar 2003: Thanks to Norbert Marwan (marwan@agnld.uni-potsdam.de) for
%    the check on length(c).
    persistent saved
    if( nargin < 1 );
        arg = 1; end;
    c = get( gca, 'children' );
    switch lower( arg );
        case 'set';
            saved = c;
        case 'revert';
            delete( setdiff(c,saved) );
        otherwise;
            if( ~ischar(arg) );
                % 2003-03-17 modified by Norbert Marwan (marwan@agnld.uni-potsdam.de)
                delete( c(1:min(arg,length(c))) ); end; end;
end

function rescale_axis( f, ax )
    if( ~iscell(f) );
        f = {f}; end;
    if( numel(f) >= 2 );
        interval = f{2};
    else;
        interval = []; end;    
    if( numel(f) >= 3 );
        epsilon = f{3};
    else;
        epsilon = []; end;    
    if( numel(f) >= 4 );
        alg = f{4};
    else;
        alg = 'all_weak'; end;
    if( numel(f) >= 5 );
        finv = f{5};
    else;
        finv = @(x) implicit_inverse( f, x, epsilon, interval, alg ); end;
    f = f{1};
        
    minnum_xticks = 4;
    
    axis_ticks = axis;
    switch ax;
        case 'x'; ticks = finv( [axis_ticks(1) xticks axis_ticks(2)] );
        case 'y'; ticks = finv( [axis_ticks(3) yticks axis_ticks(4)] );
        case 'z'; ticks = finv( [axis_ticks(5) zticks axis_ticks(6)] );
        otherwise; fatal_error; end;
    
    ticks_end = f( [ticks(1) ticks(end)] );
        
    ticks_warped = linspace( ticks(1), ticks(end), max( minnum_xticks, numel(ticks) ) );
    ticks_warped_range = max( ticks_warped ) - min( ticks_warped );
    ticks_warped_magmax = max(abs(ticks_warped));
    range = log10( ticks_warped_magmax ) - log10( ticks_warped_range );
    significants = max( ceil(range), 2 );
    ticks_warped = f( round( ticks_warped, significants, 'significant') );
    
    
    ticks_merged = unique( real( [ticks_warped ticks_end] ) );
    ticks_merged( isnan(ticks_merged) ) = [];
    
    if( numel(ticks_merged) < 4 );
        ticks_lin = f( round( ticks, significants + 1, 'significant') );
        ticks_merged = unique( real( [ticks_lin ticks_merged] ) ); end;
    
    ticks_merged( isnan(ticks_merged) ) = [];
    
    
    ticklabels = arrayfun( @num2str, finv(ticks_merged), 'UniformOutput',false );
    switch ax;
        case 'x';
            xticks( ticks_merged );
            xticklabels( ticklabels );
        case 'y';
            yticks( ticks_merged );
            yticklabels( ticklabels );
        case 'z';
            zticks( ticks_merged );
            zticklabels( ticklabels );
        otherwise;
            fatal_error; end;            
end

function clear_figure_content();
    % delete the contents of the figure, but do not use clf, 
    % since then a new window is created which comes to the foreground
    h = gca;
    delete( get(h, 'children') );
end

function clfall();
    FigList = findall( groot, 'Type', 'figure' );
    for iFig = 1:numel( FigList );
        try;
            clf( FigList(iFig) );
        catch me;  %#ok<NASGU>
            end; end; % Nothing to do
end

%% misc helper functions

function [ P ] = make_projection( varargin );

    functional = varargin{end};
    if( nargin == 2 );
        P = varargin{1};
        P = P + randn( size(P) )*.01;
        to = size( P, 1 );
        from = size( P, 2 );
    elseif( nargin == 3 );
        from = varargin{1};
        to = varargin{2}; 
        P = randn( to, from ); end;
        
    if( to == from );
        P = eye( to );
        return; end;
    
    if( isempty(functional) );
        return; end;
    
    switch lower(functional(1));
        case {'p'};
            P(P < 0) = 0;
        case {'k','m','r','c'};
            [U, S, V] = svd( P );
            S(S ~= 0) = 1;
            P = U*S*V';
        otherwise;
            fatal_error; end;
    
end

function ms = markersize_default();
    ms = 5;
end

function dummy; end  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
