function [ sco, typeout ] = cart2sphm( X, type )
% [ sco, typeout ] = cart2sphm( X, [type] )
% Transforms cartesian to hypershperical coordinates
%
% Input:
% =====
%   X       dim X N vector, cartesian coordinates
%
% Options:
% =======
%   type    int, default=0, type of coordinates to transform to (only affects the first two angles)
%           last entry in vector is always the radius \in [0, inf]
%           Positive types are in radians, negative types are in degree
%           some types have multiple identifiers
%
%       Hyperspherical coordinates:
%       ===========================
%           'hsph','h'/'hsphdeg','hd'              Hyperspherical coordinates (phi_1, ..., phi_{dim-1}, r)
%                       \phi_1, \phi_2, ... \phi_{dim-2} \in [0, pi]/[0, 180],
%                       \phi_{dim-1} \in [-pi, pi)/[-180, 180),
%                       does not cooincide in dim == 3 with matlabs cart2sph
%                       cooincides in dim == 2 with polarcoordinates where \phi_1 \in [-pi, pi)/[-180, 180) and
%                                                 matlabs cart2pol, type +-2, type +-3 (up to round-off errors)
%                       In dim == 1 these coordinates do not make sense, the function returns the input value, This bevahiour is subject to be changed
%                       (old option 0/10 and 0,1/-1)
%           'azel','ae'/'azeldeg','aed'            Azimuth/Elevation coordinates, only for dim == 2,3
%                       \phi_1 \in [-pi,    pi)/[-180, 180), from x-axis to y-axis counter-clockwise, (azimuth for S^2)
%                       \phi_2 \in [-pi/2, pi/2]/[-90,  90], from xy-plane to z-axis, (elevation for S^2) (only for dim == 3)
%                       uses matlabs cart2sph
%                       (old option 1/11 and 2/-2)
%           'azinc','ai'/'azincdeg','aid'          Azimuth/Inclination coordinates, only for dim == 2,3
%                       \phi_1 \in [-pi,  pi)/[-180, 180), from x-axis to y-axis counter-clockwise (azimuth for S^2)
%                       \phi_2 \in   [0,  pi]/[   0, 180], from z-axis to xy-plane (only for dim == 3)
%                       uses matlabs cart2sph
%                       (old option 2/12 and 3/-3)
%           'latlong','ll'/'latlongdeg','lld'      Latitude/Longitude coordinates for a spherical earth
%                       \phi_1 \in [-pi,   pi)/[-180, 180), from y-axis to x-axis clockwise
%                       \phi_2 in  [-pi/2, pi/2)/[-90,90), from xy-plane to z-axis
%           'wgs'/'wgsdeg' Latitude/Longitude coordinates for a spherical earth
%                       \phi_1 and \phi_2 is the same as for 'latlong', but the radius is taken from earth-surface, i.e. 'wgs'-radius=0 == 'll'-radius=EARTHRADIUS
%           'wgs84'/'wgs84deg' Same as 'wgs', but for the WGS84 earth
%           'ant'/'antdeg','antd'                  Antenna pattern coordinates, spherical coordinates on the xy-plane and the yz-plane, only for dim == 2,3
%                       \phi_1 \in [0, 2*pi]/[0, 360], from y-axis (North) to x-axis clockwise
%                       \phi_2 \in [0, 2*pi]/[0, 360], from y-axis (North) to minus-z-axis clockwise 
%                       only allowed in sph2cartm, sph2cartm, sph2sphm, sph2sphm2 as type1
%
%       Hypercylindrical coordinates:
%       =============================
%           'hcyl','hc'/'hcyldeg','hcd'     Hypercylindrical coordinates in ordering (phi, x_3, x_4, ..., x_{dim}, r)
%                                           Cartesian product of circle with line segments
%                       \phi \in [-pi, pi)/[-180, 180), from x_1-axis (x-axis) to x_2-axis (y-axis) counter-clockwise
%                       Note the strange ordering of coordinates!
%                       In dim == 3 these coordinate cooincide with Spherinder coordinates and with cylindrical coordinates (phi,z,r)
%                       In dim == 2 Hypercylindrical and Hyperspherical (i.e. polar) coordinates cooincide
%                       In dim == 1 these coordinates do not make sense
%           'hant'/'hantdeg','hantd'                 Strange cylindrical coordinates in ordering (phi,z,r), only for dim == 3, 
%                       \phi \in [0, 2*pi]/[0, 360], from y-axis (North) to x-axis clockwise
%                       only allowed in sph2cartm, sph2cartm, sph2sphm, sph2sphm2 as type1
%                       Note the strange ordering of coordinates!
%
%      Not implemented yet:
%      ====================
%           'spherinder'/'spherinderdeg','spherinderd'           Spherinder coordinates in ordering (phi_1, phi_2, ..., phi_{dim-2}, x_{dim}, r)
%                                                                Cartesian product of hypersphere with line segment
%                       \phi_1, \phi_2, ... \phi_{dim-3} \in [0, pi]/[0, 180],
%                       \phi_{dim-2} \in [-pi, pi)/[-180, 180),
%                       \phi \in [0, 2*pi)/[0, 360), from x_1-axis (x-axis) x_2-axis (y-axis) counter-clockwise
%                       Note the strange ordering of coordinates!
%                       In dim == 3 these coordinate coincide with Hypercylindrical coordinates
%                       In dim == 2 these coordinates do not make sense
%                       In dim == 1 these coordinates coincide with Cartesian coordinates
%
%
% =====================
%   The short string aliases are subject to be changed in future versions
%
% Output:
% =======
%   sco         angles of spherical coordinates, last entry: radius
%   typeout     the deduced type
%
% Note:     This function, and the corresponding ones given below, partially work for dim == 0 and dim == 1.
%           Some types only work for certain dimensions
%           Due to roundoff errors, the angles may lie slightly outside their documented range.
%
% E.g.: cart2sphm( [2 3 1]' )
%
%
% See also: sph2cartm, cart2sphm2, sph2cartm2, sph2sphm, sph2sphm2
%
% Written by: tommsch, 2019


%            tommsch, 2019-11-25,   sco can be a matrix of column-vectors, also in all corresponding functions
%            tommsch, 2019-11-29,   Added ''type''
%            tommsch, 2020-03-06,   Added type 20/-20
%                                   Changed values of types
%                                   Added experimental aliases for types
%                                   Added return of type
%                                   Added experimental support of type 2,3 for dim == 2
%           tommsch, 2021-03-05,    Added type 5/-5
%                                   use of string aliases is mandatory
% Changelog: 



% YY Implement types
%                   spherical, only possible for dim >= 3
%                   \phi_1 \in [0,2\pi), from x-axis to y-axis counter-clockwise, (azimuth for S^2)
%                   \phi_{dim-1} \in [-\pi,\pi], (elevation for S^2)
%                   cooincides in dim=3 with matlabs cart2sph
% YY Implement spherinder
% YY Implement hyperspherical for any dimensions
% YY Implement Longitutde/Latitude coordinates

    %parse input
    if( nargin <= 1 );
        type = 'h'; 
    else;
        sph2cartm_parsetype( type ); end;
        
    typeout = type;
    
    % handle abnormal cases
    if( nargin == 0 || isempty(X) ); 
        sco = []; 
        return; end;
    
    dim = size( X, 1 );
    if( dim == 1 ); 
        sco = X;
        return; end;
    
    %transform coordinates and do checks
    type = sph2cartm_parsetype( type );
    switch type;
        case {'hsph','hsphdeg'}; %old case 0/10
            sco = zeros( dim, size(X,2) );
            for i = 1:dim-1;
                sco(i,:) = acos( X(i,:)./sum(X(i:end,:).^2, 1).^(1/2) ); end;
            idx = X(end,:) < 0;
            sco(dim-1,idx) = -sco(dim-1,idx);
            sco(dim,:) = sum( X.^2, 1 ).^(1/2);
            idx = isnan( sco );
            sco(idx) = 0;
            sco(isnan(sco)) = 0; 
            
        case {'azel','azeldeg','azinc','azincdeg','latlong','latlongdeg','wgs','wgsdeg'};  %old case 1,2,11,12
            switch dim
                case 2;
                    [th,rho] = cart2pol( X(1,:), X(2,:) ); %transforms to [-pi,pi]
                    sco = [th;rho];
                case 3;
                    [az,el,r] = cart2sph( X(1,:), X(2,:), X(3,:) );
                    sco = [az;el;r];
                otherwise
                    error( 'cart2sphm:dim', ['Type ''' type ''' only implemented for dim == 2,3 yet.'] ); end;
        case {'wgs84','wgs84deg'};
            assert( dim == 3, 'cart2sphm:dim', ['Type ''' type ''' only possible for dim == 3.'] );
            %formular from Wikipedia: https://en.wikipedia.org/wiki/Geographic_coordinate_conversion#From_geodetic_to_ECEF_coordinates
            a = 6378137; %Semi-major axis
            b = 6356752.314245; %Semi-minor axis
            e2 = 6.69437999014e-3; %eccentricity squared
            es2 = (a^2 - b^2)/b^2;
            Z = X(3,:);
            Y = X(2,:);
            X = X(1,:);
            r = sqrt( X.^2 + Y.^2 );
            F = 54*b^2.*Z.^2;
            G = r.^2 + (1-e2).*Z.^2 - e2*(a^2-b^2);
            c = (e2^2).*F.*(r.^2)./G.^3;
            s = (1 + c + sqrt(c.^2+2.*c)).^(1/3);
            P = F./(3*(s+1+1./s).^2.*G.^2);
            Q = sqrt( 1 + 2*e2^2.*P );
            r0 = -P*e2.*r./(1+Q) + sqrt( 1/2*a^2*(1+1./Q)-(P*(1-e2).*Z.^2)./(Q.*(1+Q))-1/2*P.*r.^2 );
            U = sqrt( (r-e2*r0).^2 + Z.^2 );
            V = sqrt( (r-e2*r0).^2 + (1-e2)*Z.^2 );
            z0 = (b^2*Z)./(a*V);
            h = U.*(1-b^2./(a*V)); %altitude
            phi = atan( (Z+es2*z0)./r ); %latitude
            lambda = atan2( Y, X ); %longitude
            sco = [phi;lambda;h];
            
        case {'hcyl','hcyldeg'};
            assert( dim ~= 1, 'cart2sphm:dim', ['Type ''' type ''' (Hyperspherical coordinates) do not make sense for dim == 1.'] );
            [th,rho] = cart2pol( X(1,:), X(2,:) ); 
            sco = [th; X(3:end,:); rho];
            
        case {'antpat','antpatdeg','hantpat','hantpatdeg'};
            error( 'cart2sphm:type', ['Not possible to transform to type ''' type ''' coordinates.'] );
            
        otherwise;
            fatal_error; end;
        
    % post-processing    
    switch type
        case {'azinc','azincdeg'}; %old case 2,12
            switch dim
                case 2;
                    %do nothing
                case 3;
                    sco(2,:) = pi/2-sco(2,:);
                otherwise;
                    fatal_error; end;
        case {'latlong','latlongdeg'};
            sco(1,:) = pi/2 - sco(1,:);
        case {'wgs','wgsdeg'};
            sco([1 2],:) = sco([2 1],:);
            R = 6371000;
            sco(end,:) = sco(end,:) - R;
        case {'wgs84','wgs84deg'};
            % do nothing
        otherwise; 
            % fallthrough
        end; 
            
    % transform values to degrees if necessary
    switch type
        case {'hsph','azel','azinc','antpat','latlong','wgs84','wgs','hcyl'}; %old case 0,1,2,-1
            % do nothing
        case {'hsphdeg','azeldeg','azincdeg','antpatdeg','latlongdeg','wgs84deg','wgsdeg'}; %old case 9,10,11,12
            sco(1:end-1,:) = sco(1:end-1,:)*180/pi;
        case {'hcyldeg'};
            sco(1,:) = sco(1,:)*180/pi;
        case {'spherinderdeg'};
            sco(1:end-2,:) = sco(1:end-2,:)*180/pi;
        otherwise;
            fatal_error; end;    

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
