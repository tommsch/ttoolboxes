function d = cellm(varargin)
% d = cellm( a1, ..., an) 
% d = cellm([a1 ... an]) 
% Consistent behaviour of cell, with regards to multi-dimensional applications.
%
% Example: x = onesm(3);
%
% See also eyem, zerosm
%
% Written by: tommsch, 2018

    if( nargin == 0 ); 
        d = {};
    elseif(nargin > 1);
        d = cell( varargin{:} );
    elseif( nargin == 1 && isscalar(varargin{1}) );
        d = cell( varargin{1}, 1 );
    elseif( nargin == 1 && isempty(varargin{1}))
        d = cell(0,0);
    else
        d = cell( varargin{:} ); end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
