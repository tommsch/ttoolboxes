function [ oo ] = constructordering( varargin )
% oo = constructordering( np1, [pp1, np2, pp2, ... ] )                  (1)
% oo = constructordering( oo )                                          (2)
% oo = constructordering( imax1, ..., imaxn, 'random', len )            (3)
% Constructs an ordering.
%
% An ordering is a representation of an infinite periodic sequence.
% It is stored as an 1x2 - cell array, where the first cell is the non-periodic part, and the second cell is the periodic part
% Each cell can have an arbitrary number of rows 
% E.g.: {[2 3],[5 0]} correspdonds to 23505050505050505050...
%
% Input (1):
%   np_i, pp_i      vector of integers which are the non-periodic/periodic part for the i-th row
%                   Number of total arguments can be either 1 or an even number
%   oo              ordering, in this case oo is returned without change
%
% Input (2):
%   oo              an ordering, the functioin returns this ordering unchanged
%
% Input (3):
%   imaxj           integer, maximum entry for random ordering in row j OR
%                   1xN vector of integers with maximum entries for random ordering in the corresponding rows
%   len             integer, the length of the nonperiodic and periodic part, OR
%                   1x2 vector of integers, the length of the nonperiodic and periodic part, correspondingly
%   'random'        the string 'random', needed to identify when to construct a random ordering
%                   
% Output:
%   oo          The ordering defined by the input arguments. 
%
%
% Eg: constructordering([1 2],[3 5 6],[10 20],[30 50 60])
%     constructordering(2,2,'random',6)
%     
% See also: isordering, vector2ordering, ordering2vector, findperiod
%
% Written by: tommsch, 2018-2020

% Changlog: 2020-06-18, tommsch,    Added possibility for constructing orderings given vectors of unequal length
%                                   Improved the construction of random orderings


% handle cases (2) and (3)
if( isordering(varargin{1}) ); 
    assert( nargin == 1, 'constructordering:arg', 'An ordering is given, but there are other arguments too.');
    oo = varargin{1}; 
    return; end;

[random, varargin] = parsem( 'random', varargin, 0 );
if( random );
    oo = constructordering_random( random, varargin{:} ); 
    return; end;


% parse input
if( ~iswholenumber(nargin/2) ); %add missing periodic part if necessary
    varargin{end+1} = []; end;
nrow = numel( varargin )/2;

%fill up non-periodic parts
allnplen = cellfun( @numel, varargin(1:2:(2*nrow)) );
allplen = cellfun( @numel, varargin(2:2:(2*nrow)) );
nplen = max( allnplen );
plen = lcmm( allplen );
maxplen = max( allplen ); %used to check if all periodics are empty
oo = zeros( nrow, nplen+plen );
if( maxplen );
    for i = 1:nrow
        assert( numel(varargin{2*i}) > 0, 'constructordering:emptyperiodic', 'Empty periodic parts are only allowed if all non-periodic parts have the same length and all periodic parts are empty.' );
        val = [varargin{2*i-1} repmat( varargin{2*i}, [1 ceil(nplen+plen/numel(varargin{2*i}))]) ];
        oo(i,:) = val(1:(nplen+plen)); end;
else
    assert( all(allnplen == allnplen(1)), 'constructordering:emptyperiodic', 'Empty periodic parts are only allowed if all non-periodic parts have the same length and all periodic parts are empty.' );
    oo = cat( 1, varargin{1:2:2*nrow} );
    
end

oo = mat2cell( oo, nrow, [nplen plen] );
    



end

function oo = constructordering_random( len, varargin );

varargin( cellfun(@not, varargin) ) = [];

if( numel(len) == 1 ); %make len to 2-element vector
    len = [len len]; end;

maxind = [varargin{:}];

nrow = numel( maxind );

oo = cell(1,2);
oo{1} = zeros( nrow, len(1) );
oo{2} = zeros( nrow, len(2) );
for i = 1:nrow
    oo{1}(i,:) = randi( maxind(i), 1, len(1) );
    oo{2}(i,:) = randi( maxind(i), 1, len(2) ); end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
