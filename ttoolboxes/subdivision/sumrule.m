function sr = sumrule( varargin );

    S = varargin{1};
    
    assert( isS(S), 'sumrule:S', 'First argument must be a subdivision scheme.' );
    
    ind = cell( 1, S{1}.ndimso );
    [ind{:}] = ind2sub( S{1}.sizeo, 1:prod(S{1}.sizeo) );
    ind = cat(1,ind{:});
    C = constructdigit( S{2}, 'classify', 'sym', 'ZZ', ind );
    [val, ~, ic] = unique(C.','rows');
    N = size( val, 1 );
    
    % Compute sums of mask of elements in the same digit set
    Ad = cell( 1, N ); %these are the matrices A_\eps in Charina 2012
    for i = 1:N;
        idx = ic == i;
        Ad{i} = squeeze( summ( S{1}.ref(idx), 1 ) ); end;
    
    % Compute 1-eigenspace \Eps_A
    v1 = cell( 1, N );
    for i = 1:N
        [v1{i},d] = eig( Ad{i} );
        if( ~isvector(d) );
            d = diag( d ); end;
        idx = abs(d-1)<1e-12;
        v1{i} = v1{i}(:,idx); end;
    
    EpsA = intersectspace( v1{:} );
    
    if( ~isempty(EpsA) );
        sr(1) = true; end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
