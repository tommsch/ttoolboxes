function [ D ] = constructdigit( varargin )
% [ D ] = constructdigit( M, [options] )
% Constructs the usual digit set M[0,1)^s\cap \ZZ^s. 
%
% Input:
%       M                       a dilation matrix. I.e. a square matrix with all its eigenvalues greater than 1 in modulus.
%
% Options: 
%       'ZZ',val                If given, then only values of of the set val are used to compute the digit-set.
%                               The set val must be big enough, otherwise the output may not be a digit set.
%                               The function may restart itself, if val is too small.
%       'sym'                   the digits are computed symbolically, which takes a lot of time.
%                               If not given, but the output is not a digit set, then the algorithm may restart itself using the 'sym' option.
%       'random',val            default=0, returns a random digit set, with digits randomized at maximum by val
%       'classify'              returns a (size(ZZ,2) x abs(det(M)))-matrix. 
%                               'D' is the digit class where the entries from ZZ belong to. 
%       'verbose',val           verbose level
%
% Output:
%       D                       the digit set, each column is one digit
%
% Note:
%       Function has undefined behaviour if M is not an integer matrix
%
% E.g.: constructdigit( [2 0; 0 2] )
%
% Written by: tommsch, 2018

 %#ok<*ALIGN>

[ZZ, varargin] =         parsem( {'ZZ','Z'}, varargin, [] );
[verbose, varargin] =    parsem( {'verbose','v'}, varargin, 1 );
[symflag, varargin] =    parsem( 'sym', varargin );
[classify, varargin] =   parsem( 'classify', varargin ); 
[randomval, varargin] =  parsem( {'random','rand','r'}, varargin, 0 ); 
assert( ~isempty(ZZ) || ~classify, 'constructdigit:option', 'Option ''classify'' works only together with option ''ZZ'',<val>.');
assert( ~randomval || ~classify, 'constructdigit:option', 'Option ''classify'' does not work together with option ''random''.');
M = varargin{1}; 
varargin(1) = [];
parsem( varargin, 'test' );

if( symflag );
    try;
        sym(1);
    catch;
        warning( 'constructdigit:sym', 'Symbolic computations not possible, and thus, are disabled. Results may be wrong.' );
        symflag = -1; end; end;

dim = size( M, 1 );
m = anycast( round(abs(det(M))), 'int32' );

if( m<1 ); 
    D = []; 
    return; end;

if( m<2 );
    RVAL = 9;
    if( randomval ); 
        D = randi( 2*RVAL, dim, 1 )-RVAL; 
        return;
    else; 
        D = zeros( dim, 1 );  
        return; end; end;
    
 
if( symflag > 0 );
    IM = inv( sym(M) );
    epsilon = 0;
else
    IM = inv( M );
    epsilon = abs( IM-round(IM) )/2;
    epsilon = min( epsilon(epsilon > 2*eps) ); end;

D = zeros( dim, 0 );
if( ~isempty(ZZ) );
   maxindex = size( ZZ, 2 ); 
   for i = 1:maxindex
        v = ZZ(:,i);   
        dnew = v-M*floor( IM*v+epsilon/dim );
        D = [D dnew]; end; %#ok<AGROW>
   
else
    n = maxm( abs(M) );
    %n=norm(M,1);
    maxindex = mixvector( -n:n, dim, 0 );
    
    for i = 1:maxindex
        v = mixvector( -n:n, dim, i ); 
        dnew = v-M*floor( IM*v+epsilon/dim );
        D = [D dnew]; end; end; %#ok<AGROW>

     
if( ~classify )
    D = anycast( D, 'int32' );
    D = unique( D', 'rows' )';
    if( size(D,2) ~= m );
        if( symflag == 1 );
            % do nothing
        elseif( symflag == 0 ); 
            vprintf( 'constructdigit: Fallback to symbolic computation - may need a long time.\n', 'cpr','err', 'imp',[ 2 verbose] );
            D = constructdigit( M, varargin{:},'sym' ); 
            return; 
        elseif( symflag == -1 );
            warning( 'constructdigit:sym', 'Fallback to symbolic computation not possible. Results may be wrong.' );
            return;
        else;
            error( 'constructdigit:fatal', 'Programming error' ); end; end;

    D = double( D );
    if( randomval ); 
        D = M*randi( randomval, dim,size(D,2) )+D;
        D = D - round( summ(D)/numel(D) ); end;
else
    D = anycast( D, 'int32' );
    if( symflag <= 0 ); 
        vprintf( 'Classify may produce wrong output, if ''sym'' is not set.\n', 'cpr','err', 'imp',[ 1 verbose] ); end; end;


end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
