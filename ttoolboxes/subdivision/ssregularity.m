function [ al, TVt, T, Vt, Om ] = ssregularity( varargin )
% Computes the convergence/Hoelder regularity of a subdivision scheme
% [ al ] = ssregularity( S, [options] )
% Input :
%   S               cell array of subdivision schemes as returned by `getS`, multiple subdivision scheme, OR
%                   something which `getS` accepts
%
% Options:
%  'verbose',val    verbose level
%
% Output:
%   al              An upper bound of the regularity of the ss
%                   If the ss is not a multiple one, than the value is exact

    [verbose, varargin] = parsem( {'verbose','v'}, varargin, 1 );
    [jsropt, varargin] = parsem( {'jsropt','jsr'}, varargin, 1 );
    
    S = varargin{1};
    
    S = getS( S, 'v',verbose );
    [T,Om] = transitionmatrix( S );
    Vt = constructVt( Om );
    for i = numel(Vt):-1:1;
        [TVt, TT, ~, ~, ~, flag] =  restrictmatrix( T, Vt{i}, 'v',verbose-1 );  %#ok<ASGLU>
        if( flag );
            break; end;
    end
    Vt = Vt{i};
    al = ipa( TVt, 'v',verbose, jsropt{:}  );
    Mmax = max( cellfun( @(x) abs(det(x)), S(:,2) ) );
    
    al = sort( -log(al)/log(Mmax) );

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
