function [ str ] = latexprod( varargin );
% (experimental) Takes a vector of orderings, and returns it as a (set of) product(s) of matrices in LaTex Notation
% [ str ] = latexprod( o, var, [options] );
% Input:
%   oo      vector or cell arrays of vectors, ordering
%   var     Matrix name
%
% Output:
%   str       Latex code
%
% See also: latexbmatrix, latexprod
%
% Written by: tommsch, 2022

% Changelog:    tommsch,    2024-12-04,     'var' can now be empty

    [opt.newline, varargin] =       parsem( {'newline','nl'}, varargin, 'default',true );
    [opt.var, varargin] =           parsem( {'var'}, varargin, 'A' );
    
    oo = varargin{1};
    varargin(1) = [];
    
    if( numel(varargin) == 1 && ~isempty( varargin{1} ) );
        opt.var = varargin{1}; end;

    parsem( varargin, 'test' );

    str = '';
    [oo, cellflag] = tocell( oo );
    for n = 1:numel( oo );
        if( isempty(oo{n}) );
            str = [str 'I'];  %#ok<AGROW>
        else;
            assert( isvector(oo{n}), 'latexprod:oo', '``oo`` must consists of vectors' );
            idx = find( diff([oo{n}(:).' 0]) );
            mult = diff( [0 idx]);
            
            for i = 1:numel( idx );
                sub = num2str( oo{n}(idx(i)) );
                if( numel(sub) > 1 );
                    sub = ['{' sub '}']; end;  %#ok<AGROW>
                if( isempty(opt.var) );
                    str = [str sub];  %#ok<AGROW>
                else;
                    str = [str opt.var '_' sub]; end;  %#ok<AGROW>
                if( mult(i)>1 );
                    sup = num2str(mult(i));
                    if( numel(sup) );
                        sup = ['{' sup '}']; end;  %#ok<AGROW>
                    str = [str '^' sup];  %#ok<AGROW>
                    end; end; end;
            if( cellflag );
                str = [str ',\,']; end; end;  %#ok<AGROW>
    if( cellflag );
        if( numel(str) > 2 );
            str(end-2:end) = []; end;
        str = ['\left\{' str '\right\}']; end
    if( opt.newline );
        str = [newline str newline];
    else;
        str = strtrim( str ); end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.

%#ok<*NOSEL,*NOSEMI,*ALIGN>
