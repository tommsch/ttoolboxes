function [ str ] = latexbmatrix( varargin );
% (experimental) Takes a (set of) matrices, and returns it as a (set of) latex bmatrices
% [ str ] = latexbmatrix( M, options );
%
% Input:
%   M               matrix or cell arrays of matrices
%   newlineflag     optional, default = true, If false less newlines are in the output string
%
% Output
%   str       Latex code of M in bmatrix form
%
% See also: latexprod, latexbmatrix
%
% Written by: tommsch, 2024-10-30

% Changelog:    tommsch,    2024-10-30,     New function
%               tommsch,    2024-12-03,     Behaviour change (new options)

    [opt.begin, varargin] =         parsem( {'begin'},   varargin, '\begin{bmatrix}' );
    [opt.end, varargin] =           parsem( {'end'},   varargin, '\end{bmatrix}' );
    [opt.newline, varargin] =       parsem( {'newline','nl'}, varargin, 'default',true );
    [opt.whitespace, varargin] =    parsem( {'whitespace','ws'}, varargin, 'default',true );

    M = varargin{1};
    varargin(1) = [];

    parsem( varargin, 'test' );

    str = '';
    [M, cellflag] = tocell( M );
    for n = 1:numel( M );
        if( isempty(M{n}) );
            str = [str '\emptyset'];
        else;
            assert( ismatrix(M{n}), 'latexbmatrix:M', 'M must consists of matrices' );
            str = [str newline opt.begin];
            if( opt.newline );
                str = [str newline]; end;
            for r = 1:size( M{n}, 1 );
                for c = 1:size(M{n}, 2 ) - 1;
                    str = [str num2str(M{n}(r,c))];
                    str = tif( opt.whitespace, [str ' & '], [str '&'] );
                end
                str = [str num2str(M{n}(r,end))];
                str = tif( opt.whitespace, [str ' \\ '], [str '\\'] );
                if( opt.newline );
                    str = [str newline]; end;
            end
            if( opt.newline );
                str(end) = []; end;
            if( opt.whitespace );
                str(end-3:end) = [];
            else;
                str(end-1:end) = []; end;
            if( opt.newline );
                str = [str newline]; end;
            str = [str opt.end newline]; end;

        if( cellflag );
            if( ~opt.newline );
                str(end) = []; end;
            str = [str '\, ']; end; 
    end

    if( cellflag );
        if( numel(str) > 2 );
            str(end-2:end) = []; end;
        str = ['\left\{ ' str newline '\right\}']; end
    if( opt.newline );
        str = [newline str newline];
    else;
        str = strtrim( str ); end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.

%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*AGROW>
