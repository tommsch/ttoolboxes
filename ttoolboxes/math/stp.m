function [ A ] = stp( A, B );
% (left) semi tensor product of matrices
    n = size( A, 2 );
    p = size( B, 1 );
    s = lcm( n, p );
    A = kron( A, eye(s/n) ) * kron( B, eye(s/p) );
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
