function [ v ] = chinese( c, n );
% [ v ] = chinese( c, n );
% Brute force algorithm to solve simultaneous linear congruences for Chinese Remainder Theorem
% Input:
%   c       vector, the remainders
%   n       vector, the moduli
%
% Output:
%   v       number which solves the system
%               x = c1 mod n1
%               ...
%               x = cn mod nn
%
% E.g.:
%     chinese( [2 3 2], [3 5 7] )
%     chinese( [1 2 3], [3 4 5])
%
% Depends on: // 
%
% Written by: tommsch, 2020
%

    assert( all(n > 0), 'chinese:input', 'All moduli must be positive.' );
    assert( numel(n) == numel(c), 'chinese:input', 'Vectors n and m must have the same length.');
    c = mod( c, n );
    [m,i] = max( n );
    
    v = c(i);
    for k = 1:prod(n) + sum(n);
        if( isequal(mod(v,n),c) );
            return;
        else;
            v = v + m; end; end;
    
    v = nan;
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

