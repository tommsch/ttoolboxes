function [ ret, tot, nontot ] = totient( N );
% euler phi function / totient function
%
% See also: necklace
%
% Credits to: David Goodmanson, 2017


    if( N == 1 );
        ret = 1;
        tot = 1;
        nontot = [];
        return; end;
    
    n = 1:N-1;
    ind = gcd( n, N ) == 1;
    tot = n(ind);
    ret = numel( tot );
    
    if( nargout >= 3 );
        nontot = setdiff( n, tot ); end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
