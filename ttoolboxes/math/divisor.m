function [ d ] = divisor( n );
% divisor : 
% [ d ] = divisor(n) : provides a list of integer divisors of a number.
%
% Remark:
% This function uses the default factor() routine in Matlab and hence is 
% limited to input values upto 2^32. However if factor() routine does get
% updated for larger integers, this function will still work fine.
% Using factor() provides a significant speed improvement over manually 
% seaching for the each divisor of n.
%
% Example:
%   a = divisor(12);
%   returns -> a = [1, 2, 3, 4, 6, 12];
%
% See Also: factor, primes
% Author: Yash Kochar ( yashkochar@yahoo.com )
% Last modified: 21st June 2009
%-------------------------------------------------------------------------------

    % Input check
    assert( isscalar(n), 'divisor:NonScalarInput','Input must be a scalar.' );
    assert( n >= 1 && floor(n) == n, 'divisor:PositiveIntegerOnly', 'Input must be a positive integer.' ); 
    
    % Find prime factors of number :
    pf = factor( n );         % Prime factors of n
    upf = unique( pf );       % Unique
    
    %  the divisors
    d = upf(1).^(0:1:sum( pf == upf(1)) )';
    for f = upf(2:end);
        d = d*(f.^(0:1:sum( pf == f )));
        d = d(:); end;
    
    d = unique( d )';

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

