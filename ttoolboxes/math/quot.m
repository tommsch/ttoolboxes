function [ c ] = quot( c, n, dim )
% [ c ] = quot( c, n, dim )
% Quotient of consecutive numbers
%
% Input:
%   c       vector or matrix
%   n       number of iterations
%   dim     dimension where quot is applied. If dim is not given, it is applied in the first non-singleton dimension
%
% Output:
%   c       
%
% E.g.: quot([2 2 4 16 128 2048],2)
%
% See also: diff
%
% Written by: tommsch, 2017

%            2024-03-06,    tommsch,    Bugfix when c has only one element
% Changelog:

if( nargin <= 2 );
    dim = find( size(c)-1, 1 ); end;
if( nargin <= 1 );
    n = 1; end;
if( isempty(dim) );
    dim = 1; end;

for i = 1:n;
    str = repcell( ':',[1, ndimsm(c)] );
    str1 = str;
    str2 = str;
    str1{dim} = 1; 
    str2{dim} = sizem( c, dim );  %#ok<SIZEM>
    c1 = c; 
    c1(str1{:}) = [];
    c2 = c;
    c2(str2{:}) = [];
    c = c1./c2;
end

    
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
