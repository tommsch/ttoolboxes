function v = clamp( v, lo, hi, comp, discard );
% vout = clamp( v, lo, hi, [comp], [discard] );
% Clamps a variable to a given range (lo hi).
% 
%
% Input: 
%   v           value to be clamped
%   lo          lower bound
%   hi          higher bound
%   comp        comparison object, default: <
%   discard     bool, default: false
%               If true, clamp removes out of bound elements instead of clamping them
%
% Output:
%   vout        if v<lo, then vout=lo. if v>hi, then vout=hi. Otherwise vout=v
%
% Written by: tommsch, 2020

%            tommsch, 2023-08-21,   clamp with comparison object can handle arrays now
%                                   new option/input argument `discard`
% Changelog: tommsch, 2024-10-23,   Better support for sym

    if( nargin <= 4 || isempty(discard) );
        discard = false; end;

    if( nargin <= 3 );
        comp = []; end;

    if( discard );
        if( isempty(comp) );
            idx = isAlways( lo < v & v < hi );
        else;
            idx = isAlways( comp( lo, v ) & comp( v, hi ) ); end;
        v = v(idx);
    else;
        if( isempty(comp) );
            if( ~issymstrict(v) );
                v = max( v, lo );
                v = min( v, hi );
            else;
                v = tif( v < lo, lo, v );
                v = tif( v > hi, hi, v ); end;
        else;
            idx = isAlways( comp( v, lo ) );
            v(idx) = lo;
            idx = isAlways( comp( hi, v ) );
            v(idx) = hi;
            end; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
