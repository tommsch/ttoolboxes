function [ TA, TT, TR, N, AA, flag ] = restrictmatrix(varargin)
% [ TA, TT, TR, N, AA, flag ] = restrictmatrix( T, A, [options] )
% Restricts matrices T to a subspace A.
% Also tests, if the matrices are invariant on A.
%
% Input: 
%   T                   square matrix or cell array of square matrices
%   A                   rectangular matrix giving the columns of the subspace A, to which T will be restricted
%
% Options:
%   'verbose',val       default: 1, verbose level
%   'epsilon',val       default: 1e-11, controls the offset to test whether A is invariant or not
%   'smallsize',bool    default: true, Removes unnecessary columns of A (starting from the last column) prior to computation. Returns rank(A) x rank(A) matrix. 
%   'basis',val         default: [], column vectors which complement A to a full basis. If not given, this is constructed using unit vectors
% 
% Output:
%   TA              dimA x dimA matrices. Restriction of T to A
%   TT              dimT x dimT matrices. T in the basis of A complemented to a basis of RR^dimT
%   TR              dim(T-A) x dim(T-A) matrices. Lower right corner of matrix TT
%   N               Lower left corner of T. If T is A invariant, then NULL consists only of zeros.
%   AA              the complemented basis of A. 
%                       Thus AA\T*AA == [ TA **; NULL TR ] == TT
%   flag            Is set to 1 if subspace A is invariant under all Matrices. 0 if not. NaN if unkown.
%   
% E.g.: vdisp( restrictmatrix([1 1 0; 0 1 1; 1 0 1],[1 -1 0; 0 1 -1]') )
%
% Written by: tommsch, 2018

%            tommsch, 2019_10_25,   Added output variable 'flag'
%            tommsch, 2019_01_19,   Added option 'basis'
% Changelog: 

    [TT, A, AA, opt] = parse_input( varargin{:} );
    
    if( opt.smallsize )  % remove linear dependent vectors from A
        for j = size(A,2):-1:1;
            if( rank(A(:,[1:j-1 j+1:end]) ) == opt.rnk );  
                A(:,j) = 0; end; end;
        A = removezero( A, 2 ); end;  % remove the zero columns
    
    sze = size( A, 2 );  % number of columns of A
    if( isempty(AA) )
        AA = A;
        % make A full rank
        AA = [AA eye( opt.dim )];  % add a base of \RR^dim 
        for j = size(AA, 2):-1:sze+1;  % size(A,2)-dim+1:size(A,2);
            if( rank(AA(:,[1:j-1 j+1:end])) == opt.dim );  % remove linear dependent vectors.
                AA(:,j) = 0; end; end;
        ADD = removezero( AA(:,sze+1:end), 2 );  % remove the zero columns from the unit base
        AA = [AA(:,1:sze) ADD];
        clear ADD;
    else;
        AA = [A AA];
        assert( rank(AA) >= opt.dim, 'restrictmatrix:nobasis', 'Given ''basis'' does not constitute a complement to ''A''.' ); end;
    
    assert( rank(AA(:,1:sze)) == opt.rnk && rank(AA) == opt.dim, 'restrictmatrix:failure', 'restrictmatrix: Wrong computation.' );
    
    [TA, TR, N] = deal( cell(size(TT)) );
    for j = 1:numel( TT )  % go through all matrices in T
        TTval = AA\TT{j}*AA;
        N{j} = TTval(end-opt.cornk+1:end,1:sze);  % the residuum in the lower left corner. Equals zero if the matrix is invariant
        TR{j} = TTval(end-opt.cornk+1:end,sze+1:end); 
        TT{j} = TTval;
        TA{j} = TTval(1:sze,1:sze); end;
    
    flag = check_restriction( N, opt );
    
    if( ~opt.cellflag )
        TA = TA{1};
        TT = TT{1};
        TR = TR{1};
        N = N{1}; end;
    
end

function [ TT, A, AA, opt ] = parse_input( varargin );
    [TT, opt.cellflag] = tocell( varargin{1} );
    A = varargin{2};  % subspace
    varargin(1:2) = [];
    
    [opt.epsilon, varargin] =   parsem( {'epsilon','eps','e'}, varargin, 1e-11 );
    [opt.verbose, varargin] =   parsem( {'verbose','v'}, varargin, 1 );
    [opt.smallsize, varargin] = parsem( {'smallsize','ss','s'}, varargin, 1 );
    [AA, varargin] =            parsem( {'basis','b','AA','*A','?A'}, varargin, [] );
    parsem( varargin, 'test' );

    opt.dim = size( TT{1}, 1 );
    opt.rnk = rank( A );
    opt.cornk = opt.dim - opt.rnk;
end

function [ flag ] = check_restriction( NULL, opt );
    maxerr = 0;
    flag = 1;
    try;
        errorprinted = 0;
        for j = 1:numel( NULL );
            ERR = norm( double(simplify(NULL{j})) );
            maxerr = max( maxerr, ERR );
            if( ERR > opt.epsilon*opt.dim );
                if( errorprinted == 0 && opt.verbose >= 1 ); 
                    warning( 'restrictmatrix:notinvariant', 'Subspace not invariant under matrices.' ); end;
                vprintf( '%i (err = %g >> 0),  ', j, sum(abs(NULL{j}(:))), 'cpr','err', 'imp',[1 opt.verbose] );
                errorprinted = 1;
                flag = 0; end; end ;
        if( errorprinted == 1 ); 
            vprintf( '\n', 'imp',[1 opt.verbose] ); end;
    catch me;
        warning( 'restrictmatrix:notest', 'Could not test invariance under matrices.\n  It is likely that the matrices are not invariant.\n  Be careful and check output NULL (4th output argument).\n  Thrown error: %s\n', vdisp(me) );
        flag = NaN; end;
    vprintf( 'Maximal error during computation = %g \n', maxerr, 'imp',[2 opt.verbose] );
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
