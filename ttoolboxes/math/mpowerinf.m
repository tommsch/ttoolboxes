function [ L, piratflag ] = mpowerinf( varargin );
% [ L ] = mpowerinf( A,  [rhoflag], [realflag], [simplifyflag], [cellflag] );
% computes all limit points of infinite powers of a matrix
% 
% Input:
%   A               square matrix
%   rhoflag         logical or empty, optional, default: false, divide matrix A by rho(A) prior to compution
%   complexflag     logical or empty, optional, default: true for real input A, false for complex input A, only return real limit points
%   simplifyflag    logical or empty, optional, default: false, simplifies output and tries to remove duplicates
%   cellflag        logical or empty, optional, default = true, if false no cell array is returned. A warning is printed whenever there would be multiple limit matrices
%
% Output:
%   L               cell array of square matrices, all limit points of A^inf, given there are not too much
%   piratflag      logical, if set, then the argument all leading eigenvalues are pi-rational w.r.t to a small denominator.
%                   In this case, only one limit matrix is returned.
%
% E.g.: vdisp( mpowerinf(A) )
%
% Depends on: mixvector, rhot, expect (all from the tmisc-toolbox)
%
% Written by: tommsch, 2020

%            2024-03-15, tommsch, Behaviour change: piratflag now returns whether all eigenvalues are pi-rational (i.e. exactly the value is negated compared to the previous behaviour)
% Changelog: 
    

    [A, opt] = parse_input( varargin{:} );
    opt.nargout = nargout;
    [L, piratflag] = mpowerinf_worker( A, opt);

    L = mpowerinf_postprocess( L, opt );
    
end

function [ A, opt ] = parse_input( varargin );
    A = varargin{1};
    opt.cellflag =     tifh( numel(varargin) <= 4 || isempty(varargin{5}), true,        @() varargin{5} );
    opt.simplifyflag = tifh( numel(varargin) <= 3 || isempty(varargin{4}), ~issym( A ), @() varargin{4} );
    opt.realflag =     tifh( numel(varargin) <= 2 || isempty(varargin{3}), isreal( A ), @() varargin{3} );
    opt.rhoflag =      tifh( numel(varargin) <= 1 || isempty(varargin{2}), false,       @() varargin{2} );
    opt.maxdegreeresolution = 360;  % "maximum degree resolution of complex eigenvalues"
end

function [ L, piratflag ] = mpowerinf_worker( A, opt )
    if( ~issym(A) );
        if( rcond(A) > 1e-8 && condeigvec(A) < 1e8 );
            [v, JI, piratflag] = mpowerinf_worker_double_eigenvector( A, opt );
            L = compute_L( v, JI );
        elseif( isreal(leadingeigenvalue(A)) );
            [L, piratflag] = mpowerinf_worker_double_power( A, opt );
            if( anym(isnan([L{:}])) );
                [v, JI, piratflag] = mpowerinf_worker_double_eigenvector( A, opt );
                L = compute_L( v, JI ); end;
        else;
            [v, JI, piratflag] = mpowerinf_worker_double_eigenvector( A, opt );
            L = compute_L( v, JI ); end;
        
    else;
        [v, JI, piratflag] = mpowerinf_worker_sym( A, opt );
        L = compute_L( v, JI );
    end;
end

function [ v, JI, piratflag] = mpowerinf_worker_double_eigenvector( A, opt );
    
    if( opt.rhoflag );
        A = A/rhot( A ); 
        assert( all(isfinite(A(:))), 'mpowerinf:scaling', 'Matrix cannot be scaled. Probably spectral radius is non-finite.' ); end;
    [v, d] = eig( A ) ;  % A = v*d*invv
    expect( cond(v) < 1e8, 'mpowerinf:singular', 'Eigenvectors of matrix are badly scaled. Results may be inaccurate.' );
    if( isvector(d) );
        d = diag( d ); end;
    if( opt.rhoflag );
        [~, idx] = max( abs(d(:)) );
        d(idx) = d(idx)/abs( d(idx) ); end;

    %invv = pinv( v );
    expect( rcond(v) > 1e-12, 'mpowerinf:singular', 'Matrix is close to singular or badly scaled. Results may be inaccurate.' ); 
    nd = size( d, 1 );
    JI = cell( 1, nd );

    piratflag = true;
    for i = 1:nd
        [JI{i}, piratflag_i] = mpowerinf_jordanblock( d(i,i), opt.maxdegreeresolution, opt ); 
        piratflag = piratflag & piratflag_i; end;
end

function [ L, piratflag ] = mpowerinf_worker_double_power( A, opt );
    piratflag = true;
    if( opt.rhoflag );
        A = A/rhot( A ); 
        assert( all(isfinite(A(:))), 'mpowerinf:scaling', 'Matrix cannot be scaled. Probably spectral radius is non-finite.' ); end;
    Ao = A;
    dim = size( A, 1 );
    r_total = 1;
    while( true );
        A_new = A * A;
        if( norm( A_new - A, 1 ) < 1e-12 || ...
            any( isnan(A(:)) ) ...
          )
            break; end;
        if( norm(A_new) > dim * 1e4 );
            r = rho( A_new );
            A_new = A_new/r;
            r_total = r_total * r; end;
        A = A_new; end;
    A2 = A_new * Ao * r_total;
    if( norm(A2 - A_new, 1) > 1e-12 );
        L = {A_new, A2};
    else;
        L = {A_new}; end;
end

function [ v, JI, piratflag ] = mpowerinf_worker_sym( A, opt );
    [v, J] = jordan( A );  % A = v * J / v
    if( opt.rhoflag );
        val = rhot( A );
        expect( isAlways(val > 0), 'mpowerinf:scaling', 'Matrix has zero spectral radius. Applied scaling leads to wrong results.' );
        J = J/val; end;  % dividing afterwards is faster
    J = invblkdiag( J );
    JI = cell( 1, numel(J) );
    piratflag = true;
    for j = 1:numel( J );
        [JI{j}, piratflag_i] = mpowerinf_jordanblock( J{j}, opt.maxdegreeresolution, opt );
        piratflag = piratflag & piratflag_i; end;
end

function [ L ] = compute_L( v, JI );
    len = cellfun( 'prodofsize', JI );
    len_total = lcmm( len );
    comb = zeros( numel(len), len_total );
    for i = 1:numel( len );
        comb(i,:) = repmat( 1:len(i), [1 len_total/len(i)] ); end;
    L = cell( 1, size(comb,2) );
    val = cell( 1, numel(JI) );
    rcond_flag = rcond( double(v) ) > 1e-12;
    if( rcond_flag );
        invv = inv( v ); end;
    for i = 1:numel( L );  
        for j = 1:numel( JI );
            val{j} = JI{j}{comb(j,i)}; end; 
        if( rcond_flag );
            L{i} = v * blkdiag( val{:} ) * invv;  %#ok<MINV>
        else;
            L{i} = v * blkdiag( val{:} ) / v; end; end;
end

function [ L, piratflag ] = mpowerinf_jordanblock( J, maxD, opt );
% computes the limit of one triangular-upper-Jordan-block, 
% Input: J ... square matrix, Jordan block
%        maxD ... maximum pi-rational denominator
% Output: L ... cell array of square matrices, all limit points of J^inf

    piratflag = true;
    angle_J = diag( J );
    assert( all(isAlways(angle_J(1) == angle_J)), 'matrixpowinf:jordanblock', 'Jordanblock with unequal eigenvalues on the diagonal encountered.' );
    if( isAlways(abs(J(1)) < 1-4*eps, 'Unknown','false') );
        L = {zeros(size(J))}; 
        return; end;
    if( isAlways(abs(J(1)) > 1+4*eps) );
        L = {inf(size(J))}; 
        return; end;
    if( issym(J) );
        angle_J = angle( J(1) )/(sym(pi)*sym(2));
    else;
        angle_J = angle( J(1) )/(pi*2); end;
    %val = tif( isAlways(val<0), val+1/2, val );
    if( ~issym(angle_J) );
        [~, D] = rat( angle_J );
    else;
        [~, D] = numden( angle_J ); end;
    D = double( D );
    dim = size( J, 1 );
    E = eye( dim );
    INF = triu( inf(size(J)), 1 ) + E;
    
    if( ~iswholenumber(D) || D >= maxD );
        if( opt.nargout <= 1 );
            warning( 'matrixpowerinf:resolution', 'Complex part of matrix is not pi-rational. Only one matrix is returned.' ); end;
        piratflag = false;
        
        D = 1; end;
    L = cell( 1, D );
    for d = 1:D;
        L{d} = simplify( J^d )*E; 
        L{d} = L{d}.*INF; end;
    
    %L = uniquecell( L );
end

function [ L ] = mpowerinf_postprocess( L, opt );
    if( ~opt.cellflag );
        L = L(1); end;
    
    if( opt.realflag );
        for l = numel( L ):-1:1;
            L{l} = real( L{l} ); end; end;
    
    if( opt.simplifyflag );
        L = uniquecell( L );
        for l = 1:numel( L );
            L{l} = simplify( L{l} ); end;
        L = uniquecell( L ); end;
    
    if( ~opt.cellflag );
        L = L{1}; end;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% helper functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
