function [ w ] = limsup( v );
% [ w ] = limsup( v ) 
% Computes the cumulative limsup of vectors.
%
% E.g.: limsup( [10 1 9 2 8 3 7 4 6] )
%
% Depends on: //
% 
% See also: liminf
%
% Written by: tommsch, 2018
        assert( isvector(v), 'limsup:input', '''v'' must be a vector.' );
    w = v;  % w has same type as v
    for i = 1:numel( v ) - 1;
        w(i) = max( v(i:end) ); end;
    w(end) = v(end);

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
