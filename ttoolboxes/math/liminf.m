function [ w ] = liminf( v );
% [ w ] = liminf( v ) 
% Computes the cumulative liminf of vectors.
%
% E.g.: liminf( [10 1 9 2 8 3 7 4 6] )
%
% Depends on: //
% 
% See also: limsup
%
% Written by: tommsch, 2018
    assert( isvector(v), 'liminf:input', '''v'' must be a vector.' );
    w = v;  % w has same type as v
    for i = 1:numel( v ) - 1;
        w(i) = min( v(i:end)); end;
    w(end) = v(end);  % octave bug fix
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
