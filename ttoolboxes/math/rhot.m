function [ rhoo ] = rhot( M );
% Computes the spectral radius of matrices.
% [ rhoo ] = rhot( M )
% If M is a cell of matrices, ans is a row vector with the spectral radius of each M{i}
%
% Input:
%   M       matrix or cell array of matrices
%
% Output:
%   rhoo    spectral radius of matrix/matrices
%
% Note:
%   The returned value for an empty set may change from release to release
%
% E.g.: rhot([1 2; -1 2])
%
% Depends on: //
%
% Taken from: Jungers, JSR-toolbox
% Modified by: tommsch, 2017

% Note:
%   In this file is also a version which uses vpa to compute rho. But it is slow, thus the code is commented out.


    if( iscell(M) );
        m = length( M );
        rhoo = zeros( 1, m );
        if( issym(M{1}) );
            rhoo = sym( rhoo ); end;
        for i = 1:m;
            if( isempty(M{i}) );
                rhoo(i) = -inf;
            elseif( issparse(M{i}) );
                opts.disp = 0;
                rhoo(i) = eigs( M{i}, 1, 'LM',opts );
            else;
                val = abs(eig(M{i})); 
                if( issym(val) );
                    rhoo(i) = val(1);
                    for j = 2:numel( val );
                        try;
                            if( isAlways(rhoo(i) < val(j),'Unknown','error') );
                                rhoo(i) = val(j); end;
                        catch me;  %#ok<NASGU>
                            rhoo(i) = norm(  [rhoo(i) val(1)], inf ); end; end;
            else;
                rhoo(i) = max( val ); end;  end; end;
    else;
        if( isempty(M) );
            rhoo = [];
        elseif( issparse(M) );
            opts.disp = 0;
            rhoo = eigs( M, 1, 'LM',opts );
        else;
            try
                val = abs( eig(M) );
            catch me;  %#ok<NASGU>
                if( any(isnan(M(:))) );
                    val = nan; 
                else;
                    val = inf; end; end;
            if( issym(val) );
                rhoo = val(1);
                for j = 2:numel( val );
                    try;
                        if( isAlways(rhoo < val(j),'Unknown','error') );
                            rhoo = val(j); end;
                    catch me;  %#ok<NASGU>
                        rhoo = norm(  [rhoo val(j)], inf ); end; end;
            else;
                rhoo = max( val ); end;  end; end; % inf-norm is a workaround for symbolic stuff, it is as fast as max() for floating point, and faster for sym, since norm() does not simplify stuff
end


function [ rhoo ] = rhot_vpa( M, vpabound, newdigits )
% rhoo = rhot(M,[vpabound],[newdigits])
% 
% Computes the spectral radius of matrix M
% if M is a cell of matrices, rhoo is a row vector with the spectral radius of each M{i}
%
% Options:
%    vpabound       If vpabound is set, then the spectral radius is computed with vpa, if abs(rho-vpabound)<epsilon
%                   epsilon  = epsilon=min([1e-12, 10^-size(M, 2), max(1,1/abs(rhoo(1)-rhoo(2)))*10*eps*size(M, 2)]); 
%                   If this happens, then the return value is symbolic
%                   
%    newdigits      digits used in vpa computation (if necessary)
%                   if not given, then newdigits = max(17, size(M, 2)).
%
% changed by: tommsch, 2018


    if( nargin < 3 ); 
        newdigits = 0; end;
    if( nargin < 2 );
        vpabound = -inf; end;
    
    if( iscell(M) );
        m = numel( M );
        rhoo = zeros( 1, m );
        for i = 1:m;
            rhoo(i) = rhot( M{i}, vpabound, newdigits ); end;
    else;
        % compute spectral radius numerically
        if( numel(M) == 1 );
            rhoo = abs( M );
            return;
        elseif( issparse(M) );
            opts.disp = 0;
            rhoo = sort( eigs(M, 2, 'LM', opts),'descend','ComparisonMethod','abs' );
        elseif( ~isnumeric(M) );
            rhoo = rhovpat( M, newdigits );
            vpabound = -inf;
        else
            rhoo = sort( eig(M), 'descend', 'ComparisonMethod','abs' ); end;
        
        if( vpabound < 0 );
            rhoo = rhoo(1); 
            return; end;
        
        % test if we need vpa
        epsilon = min( [1e-12, 10^-size(M, 2), max(1,1/abs(rhoo(1)-rhoo(2)))*10*eps*size(M, 2)] ); 
        
        if( abs(rhoo(1)-vpabound) < epsilon );
            rhoo = rhot_vpa_worker( M, newdigits );
        else;
            rhoo = abs( rhoo(1) ); end;
        rhoo = sym( rhoo ); end;

end

function [ rhoo ] = rhot_vpa_worker( M, newdigits )
    digitsbackup = digits;
    if( newdigits == 0 );
        newdigits = max( 17, size(M, 2) ); end;
    digits( newdigits );
    rhoo = max( abs(eig(vpa(M))) );
    digits( digitsbackup );
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
