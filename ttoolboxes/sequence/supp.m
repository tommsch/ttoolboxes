function CO = supp( a, dim, amin )
% CO = supp( a, [dim], [amin] )
% Computes the support of an array.
%
% Input: 
%   a       dim x N matrix
%   dim     default = ndimsm(a), integer, the dimension of the array a
%   amin    default = 0, vector of length dim. Indices of the first entry in a
% 
% Output:
%   CO       coordinates as column vectors
%
% E.g.: CO = supp( [1 1; 0 1], 2, [1;-1] )
%
% See also: characteristic
%
% Written by: tommsch, 2017

%            2020-06-07, tommsch,   totally rewritten
%                                   amin and dim are now optional parameters
% Changelog: 
    
    if( nargin <= 1 || isempty(dim) );
        dim = ndimsm( a ); end;
    if( nargin <= 2 || isempty(amin) );
        amin = zerosm( dim ); end;
    if( isrow(amin) );
        amin = amin.'; end;
    
    
    
    %make to logical array and remove tensor dimensions
    a = anym( a, dim+1:ndimsm(a) ); 
    a = (a ~= 0);
    if( isa(a,'sym') ); %test if <a> is symbolic
        a = isAlways( a, 'Unknown','true' ); end;
    
    CO = cell( 1, dim );
    
    idx = find( a );
    if( isrow(idx) );
        idx = idx.'; end;
    if( isempty(idx) );
        [CO{:}] = deal( [] );
    else;
        [CO{:}] = ind2sub( sizem(a), idx ); end;  %#ok<SIZEM>
    CO = cat( 2, CO{:} ).' + amin - 1;
    if( isempty(CO) );
        CO = zeros( dim, 0 ); end;


end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
