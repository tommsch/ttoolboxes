function arr = decompact( arr, dimo, t )
% c = decompact( arr, dimo, t )
% decompactifies arrays to tensor valued arrays
% trailing singleton dimensions of t are removed
% in general, increases the number of dimensions 
%
% Input:
%   arr     (c1+t1 x ... cn+tn)-array, where ci are the sizes of the outer dimensions and tj are the sizes of the inner dimensions, 
%           the array to be decompactified
%   dimo    integer, the number of outer dimensions, This number is not necessarily equal to n, due to singleton dimensions
%   t       integer vector, the size of the inner dimensions
%
% Output:
%   c       (c1 x ... x cn x t1 x ... x tn ) array
%
% E.g.: decompact( [1 -1 0 1]', 1, [2 1] )
%   
% See also: compact
%
% Written by: tommsch, 2020

    if( ~isrow(t) );
        t = t.'; end;
    
    symflag = isa( arr, 'sym' );

    assert( ~any(t == 0), 'decompact:tensordimension', 't cannot contain zero.' );
    idx = find( t ~= 1, 1, 'last' ); %delete trailing ones ( =singleton dimensions )
    t(idx+1:end) = [];
    t((numel(t)+1):ndimsm(arr)) = 1; %fill up dimensions with one, such that sz and t have the same length
    sz = sizem( arr );  %#ok<SIZEM>
    tt = arrayfun( @(x,y) repmat(x,[1 y]), t, sz./t, 'UniformOutput',false );
    arr = mat2cell( arr, tt{:} );
    dim = ndimsm( arr{1} ) + dimo;
    p = [ (dim-dimo+1):dim 1:(dim-dimo) ];
    p = [p max(p)+1]; %bugfix
    arr = cellfun( @(x) permute(x,p), arr, 'UniformOutput',false );
    if( symflag );
        arr = cell2sym( arr );
    else;
        arr = cell2mat( arr ); end;
    

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
