function [ C ] = einsum( A, B, iA, iB, sA, sB );
% C = einsum( A, B, iA, iB, [sA, sB] )
% C = einsum( A, B, iAB, [sA, sB] )
% Efficiently calculates tensor contraction in any dimension. 
% It uses matlab's matrix multiplication so parallelized and optimized.
%
% Calculates the contraction of A and B which are n-dimensional tensors.
% The contraction is specified either 
%   by two vectors iA and iB which list the indices to contract on for each tensor.
%   by one Nx2 vector iAB = [iA iB], where iA and iB are column vectors
%
% Input:
%   A,B             tensors, which shall be contracted
%   iA,iB | iAB     Nx2 index vectors, pairs of indices which shall be contracted
%   sA,sB           vectors, optional, sizes of the matrices, if not given, sizem() is used to determine the sizes
%
% Output:
%   C               tensor, after contraction
%
% Example:
%     A = rand( 7, 4, 5 );
%     B = rand( 5, 7 );
% To contract the 1st dimension of A with the 2nd dimension of B, use
%   einsum( A, B, 1, 2 )
% The result will be of size [4,5,5]. 
%
% To contract the 3rd dimension of A with the 1st dimension of B, use
%   einsum( A, B, 3, 1 )
% The result will be of size [7,4,7]. 
%
% To do both contractions at once, use
%   einsum( A, B, [1 3], [2 1] )
%
% It is not possible to specify the order of dimensions in the output, 
% they're just in the same order as the input with the contracted dimensions omitted.
%
% See also: sizem
%
% Author: Yohai Bar-Sinai 
% Modfied by: tommsch - minor changes to prevent Matlab from removing singleton dimensions

    if( nargin == 5 );
        sB = sA;
        sA = iB;
        iB = iA(:,2);
        iA = iA(:,1);
    elseif( nargin == 4 );
        iB = iA(:,2);
        iA = iA(:,1);
    elseif( nargin == 2 );
        iA = ndimsm( A );
        iB = 1; end;
    
    if( ~isrow(iA) );
        iA = iA.'; end;
    if( ~isrow(iB) );
        iB = iB.'; end;
    
    
    dimsA = setdiff( 1:numel(sA), iA );
    dimsB = setdiff( 1:numel(sB), iB );
    if( ndimsm(A) > 1 );
        A = permute( A, [dimsA iA] ); end;
    if( ndimsm(B) > 1 );
        B = permute( B, [iB dimsB] ); end;
    A = reshape( A, [], prod(sA(iA)) );
    B = reshape( B, prod(sB(iB)), [] );
    C = A*B;
    output_shape = [sA(dimsA),sB(dimsB)];
    if( length(output_shape) > 1 );
        C = reshape( C, [sA(dimsA),sB(dimsB)] ); end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

