function arr = compact( arr, dimo )
% [ arr ] = compact( arr, C )
% Compactifies tensor valued arrays
% i.e. replaces each entry in the array at index (i1, ... ,iC) by the element (i1, ... ,ic, :, ... ,:), 
% in particular, reduces the number of dimensions
%
% Input:
%   arr         c1 x ... cC x t1 x ... tT array, where ci are the sizes of the outer dimensions and tj are the sizes of the inner dimensions, 
%   C           integer, the number of outer dimensions
%
% Output:
%   arr         N-array, N = max( C, T ), the compactified array
%
% E.g.: compact( [1 -1; 0 1]', 1 )
%   
% See also: decompact
%
% Written by: tommsch, 2020

    p = (dimo+1):ndimsm(arr);
    if( ~isempty(p) );
        arr = cellfun( @(x) shiftdim(x,dimo), num2cell(arr,p), 'UniformOutput', false );
        arr = cell2mat( arr ); end;
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
