% Class which represents the space \ell_0^{n1\times ...\times nN}(\ZZ^s), ni,k,s\in\NN
%
% Properties:
%   c       array, the sequence
%   idx     vector, index of first entry in c
%   d       scalar, default=0, experimental, value of non-stored elements
%
% Implemented methods
%   Most methods which make sense for sequences are implemented and have the same name as their matlab counterpart.
%   E.g. if variable s is a sequence, call norm(s) to compute its norm
%   Functions which make sense for the outer and/or inner/tensor dimensions also have two versions with appened -o and/or -i,
%   e.g to compute the non-zero entries (elements of \RR^{n\times k} of the sequence call nnzo.
% 
% Notes:
%   Due to stupid Matlab, the values of c can't be accessed using sequence(idx). Instead .ref()/asgn() must be used
%   Tensor valued sequences (i.e. n,k ~= 0) are only partially implemented
%   Indexing is "zero" based for the infinite part
%   Indexing is one based for the tensor part
%   Class requires m-toolbox
%   Due to strange Matlab-behaviour, some Matlab versions error when displaying some sequences
%
% Use sequence>function_name to display the help for subfunctions
% See also: sequence>sequence, 
%
% Written by: tommsch, 2017

%            2020-06-05, tommsch,   Added support for tensor valued sequences
%                                   Added experimental support for finite sequences up to a constant (added variable .d)
% Changelog:

% XX implement movsum, mod, rem, poly, charpoly, sort, bernoulli, cauchy, bincount, cross, det, cumsum, cummean (startin from 0)?
% XX implement sym

classdef sequence

    properties
        d       % value of unstored entries  % XX rename d to o (offset)
        idx     % row vector, outer-index of first entry in c
        c       % the sequence, the dimensions 1:numel(idx) are entries of the infinite sequence, the other dimensions are the tensor valued entries
    end

    methods
%% Constructors, Get, Set
%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [ obj ] = sequence( c_, idx_, t, d_ )  % Constructor  % t: variable concering the construction of tensor valued sequence
            % sequence( c, [idx], [t], [d] )
            % Constructs a sequence, i.e. an element in \ell_0^{n\times k}(\ZZ^s)
            %
            % Input: 
            % ============
            %   c      array, the sequence
            %   idx    vector, default=[0,...0], index of first entry of c, defines also the outer dimension, i.e. the variable s from above)
            %   t      vector, default=1, size of tensor values, defines the inner/tensor dimensions,  OR
            %          -1, experimental, tensor-dimensions are automatically deduced from c and idx OR
            %          nan, the input is not parsed but directly stored, idx_ must be given, d_ defaults to 0, no checks are made
            %   d      scalar, default = 0, OR0
            %          tensor of size t, (experimental), value of non-stored elements
            %          
            %
            % Notes:
            % ==========
            %   To construct a 0-dimensional sequence( i.e. one tensor-valued point), set idx to [], c to [] and d to the tensor-value
            %   To construct a scalar valued sequence, numel(idx) must be equal to ndimsm(c)
            %   To construct a tensor valued sequence you must set t to nonzero, otherwise an error is thrown for safety
            %       e.g. sequence( [1 1 1 2 2 2;1 1 1 2 2 2], [0;0], [2 3] ); defines a two-dimension sequence with tensor dimension 2x3
            %            whose elements are c.ref_(0,0) = [1 1 1;1 1 1]; c.ref_(0,1) = [2 2 2;2 2 2]; and zero otherwise
            %
            %   For scalar-valued sequences, if idx is empty then it is initialized to zero.
            %
            % See also: sequence>ref
            
            %parse input
            if( nargin >= 3 && any(isnan(t)) );  % this is the case which is time critical, thus we list it at first
                obj.c = c_;
                obj.idx = idx_;
                if( nargin == 3 );
                    obj.d = 0; 
                else;
                    obj.d = d_; end;
                return;
            elseif( nargin == 0 );
                error( 'sequence:argin', 'At least the value of the sequence must be given as first argument.' );
            elseif( nargin == 1 || ...  % c given
                    nargin == 2 && isempty(idx_) );
                obj.c = c_;
                obj.idx = zeros( ndimsm(c_), 1 );
                obj.d = 0;
                return;
            elseif( nargin <= 2 || ...  % c and idx given
                    nargin <= 3 && isempty(t)  || ...
                    nargin <= 4 && isempty(t) && isempty(d_) );
                obj.c = c_;
                if( isrow(idx_) );
                    idx_ = idx_.'; end;
                obj.idx = idx_;
                obj.d = 0;
                assert( ndimsm(c_) <= numel(idx_) , 'sequence:dim', 'Sequence is higher dimensional than index.' );
                return;
                
            % the last cases need all the same work for determining idx_,
            % thus we do it afterwards
            elseif( nargin <= 3 && ~isempty(idx_) && isequal(t,-1) );  % automatic deduction of t
                assert( ndimsm(c_) >= numel(idx_) , 'sequence:dim', 'Sequence is higher dimensional than index.' );
                t = sizem( c_ );
                t = t(numel(idx_)+1:end);
                d_ = 0;
            elseif( nargin == 4 && isempty(t) && ~isempty(d_) );  % c, [idx] and d given
                t = sizem( d_ );
            elseif( nargin <= 3 && ~isempty(t) || ...  % c, [idx] and t given
                    nargin <= 4 && ~isempty(t) && isempty(d_) );
                d_ = 0; end;
                
            if( isempty(idx_) );
                idx_ = zeros( ndimsm(c_), 1 ); end;
            if( ~iscolumn(idx_) );
                idx_ = idx_.'; end;
            
            if( isempty(c_) );
                obj.c = [];
            else;
                obj.c = decompact( c_, numel(idx_), t ); end;
            obj.idx = idx_;
            if( numel(d_) > 1 );
                obj.d = decompact( d_, 0, t );
            else;
                obj.d = d_; end;

%             idx = find( t == 1, 1, 'last' );
%             if( ~isempty(idx) );
%                 t = t(1:idx-1); end;
            %assert( ndimsm(c_) <= numel(idx_)+tif(isscalar(t),0,ndimsm(t)) , 'sequence:dim', 'Sequence is higher dimensional than index.' );
            expect( isequal(sizem(obj.d),t) || isscalar(obj.d) , 'sequence:d', 'dimension of d is wrong.' );
            
        end
        
        function [ ret ] = get_c( obj );
            % [ c ] = get_c( obj )
            % returns the raw value of .c
            % See also: sequence>get_idx, sequence>get_c, sequence>get_d, sequence>get
            ret = obj.c;
        end

        function [ ret ] = getc( obj ); 
            warning( 'sequence:deprecated', 'This function is deprecated. Use ``get_c`` instead.' );
            ret = obj.c;
        end

        function [ ret ] = get_idx( obj );
            % [ idx ] = get_idx( obj )
            % returns the raw value of .idx
            % See also: sequence>get_idx, sequence>get_c, sequence>get_d, sequence>get
            ret = obj.idx;
        end

        function [ ret ] = getidx( obj ); 
            warning( 'sequence:deprecated', 'This function is deprecated. Use ``get_idx instead.' );
            ret = obj.idx; 
        end

        function [ ret ] = get_d( obj );
            % [ d ] = get_d( obj )
            % returns the raw value of .d
            % See also: sequence>get_idx, sequence>get_c, sequence>get_d, sequence>get
            ret = obj.d;
        end

        function [ ret ] = getd( obj ); 
            warning( 'sequence:deprecated', 'This function is deprecated. Use ``get_d instead.' );
            ret = obj.d; 
        end

        function [ ret ] = get( obj );
            % [ st ] = get( obj )
            % returns the values of .c, .idx and .d as a struct
            % See also: sequence>get_idx, sequence>get_c, sequence>get_d, sequence>get
            ret.c = obj.c;
            ret.idx = obj.idx;
            ret.d = obj.d;
        end

        function [ b ] = check( obj );
            % [ b ] = check( obj );
            % makes consistency checks of the sequences
            % returns false if some checks fail
            [msg, id] = lastwarn( 'SEQUENCENOWARN' );
            expect( iscolumn(obj.idx), 'sequence:idx', 'Index is not a column vector.' );
            expect( isscalar(obj.d) || isequal(obj.sizei,sizem(obj.d)), 'sequence:tensorsize', 'Tensor size not compatible with value .d' );
            expect( isequal(class(obj.c),class(obj.d)), 'sequence:class', 'Class of .c and .d unequal.' );
            if( ~isequal(lastwarn,'SEQUENCENOWARN') );
                b = false;
            else;
                lastwarn( msg, id );
                b = true; end;
        end
        
%% Indexing
%%%%%%%%%%%%%%%
        function [ ret ] = ref( obj, varargin );
            % (experimental) seq = ref( obj, subs );
            % Nearly equivalent of matlabs subs.
            %
            % Input:
            %   subs            comma seperated list of elements or strings of comma seperated elements
            %                   The format follows the format used by subsref, except that
            %                   To use "end" and the colon operator ":", the argument must be given as a string
            %                   Furthermore this function understands the tokens "begin" and "::"
            %   
            % Token:
            %   'begin'         placeholder for the first element in a dimension
            %   'end'           placeholder for the last element in a dimension
            %   '::'            all remaining dimensions of the inner or outer dimensions, at most two '::' tokens can be used in a call to ref
            %
            % Output:
            %   c               a sequence
            %   
            % Notes:
            %   If the number of the arguments in the call ist equal to the outer-dimensions, all  inner-dimensions are returned too.
            %   The number of the arguments in the call must be equal to 1, the outer-dimensions or the sum of the outer and inner dimensions,
            %   except that trailing 1s, ':' and a '::' corresponding to inner dimensions are ignored.
            %
            % Examples:
            %   Let s be \ell^{2x3}(\ZZ^3) sequence
            %       s('1,3,2,2',2) returns the element at position (1,3,2) the (2,2)^nd value
            %       s('begin:2','::',1,3) would be "logically" equivalent to s(-inf:2,:,:,1,3)
            %
            % Note: The name of the function may be changed in future releases
            %
            % See also: sequence>asgn
            
            [obj,s,outerflag,type] = parsesubs( obj, varargin{:} ); %parse varargin (subscripts)
            
            switch type;
                case 'logical';
                    if( isequal(outerflag,0) );
                        ret = obj.c(s);
                    else;
                        n = nnz( s );
                        rep = [ones(1,obj.ndimso) obj.sizei];
                        s = repmat( s, rep);
                        ret = obj.c( s ); 
                        ret = reshape( ret, [n obj.sizei 1] ); end; %the 1 at the end is a workaround against stupid matlab behaviour
                        
                case 'coordinate';
                    if( isequal(outerflag,0) );
                        ret = obj.c(s);
                    else;
                        shifts = (0:(prod(obj.sizei)-1))*prod(obj.sizeo);
                        s = s.' + shifts;
                        ret = obj.c( s ); 
                        ret = reshape( ret, [size(s,1) obj.sizei 1] ); end; %the 1 at the end is a workaround against stupid matlab behaviour
                    
                case 'position';
                    %compute matlab indices
                    st.type = '()';
                    st.subs = s;
                    for i = 1:numel( s );
                        st.subs{i} = s{i} - obj.idxmin(i) + 1; end;

                    ret = subsref( obj.c, st );
                    val = cell( 1, numel(s) );
                    for i = 1:numel(val);
                        val(i) = s(1); end;
                    val = cellfun( @(x) x(1), s );
                    ret = sequence( ret, val(1:obj.ndimso).', nan );
            
                otherwise;
                    fatal_error; end;
                    
        end

        function [ A ] = asgn( obj, varargin );
            % seq = asgn( obj, subs, val );
            % See also: sequence>ref
            B = varargin{end};
            varargin = varargin(1:end-1);
            
            [obj,s,outerflag,type] = parsesubs( obj, varargin{:} ); %parse varargin (subscripts)
            
            switch type;
                case 'logical';
                    if( isequal(outerflag,0) );
                        obj.c(s) = B;
                    else;
                        rep = [ones(1,obj.ndimso) obj.sizei];
                        s = repmat( s, rep);
                        if( numel(B) > 1 );
                            B = repmat( B, [1 obj.numeli] ); end;
                        obj.c( s ) = B; end;
                        
                case 'coordinate';
                    if( isequal(outerflag,0) );
                        obj.c(s) = B;
                    else;
                        shifts = (0:(prod(obj.sizei)-1))*prod(obj.sizeo);
                        s = s.'+shifts;
                        if( numel(B) > 1 );
                            B = repmat( B, [1 obj.numeli] ); end;
                        obj.c( s ) = B; end; %the 1 at the end is a workaround against stupid matlab behaviour
                    
                case 'position';
                    %compute matlab indices
                    st.type = '()';
                    st.subs = s;
                    for i = 1:numel( s );
                        st.subs{i} = s{i} - obj.idxmin(i) + 1; end;
                    obj.c = subsasgn( obj.c, st, B );
                    
                otherwise;
                    fatal_error; end;
                
                A = obj;
        end

        function [ obj, s, outerflag, type ] = parsesubs( obj, varargin );
            % parses matlab subscripts and resizes obj such that all queried elements are present in obj.c
            % can handle logical and positional indexing
            %
            %
            
            ind = varargin{1};
            if( numel(varargin) == 1 && (islogical(ind) || isa(ind,'sequence') && islogical(ind.c)) );
                [obj,s,outerflag] = parsesubs_logical( obj, ind );
                type = 'logical';
            elseif( numel(varargin) == 1 && isnumeric(ind) && (size(ind,1) == obj.ndimso||size(ind,1) == obj.ndims) );
%                 expect( obj.ndimso == 1, 'sequence:linidx', ...
%                     ['Linear indexing is not supported for sequences.\n' ...
%                      'Coordinate-indexing is used for this call.\n' ...
%                      'To disable this warning type: ''warning(''off'',''sequence:linidx'')''.'] );                
%                warning( 'off', 'sequence:linidx' );
                [obj,s,outerflag] = parsesubs_coordinate( obj, ind );
                type = 'coordinate';
            else;
                [obj,s,outerflag] = parsesubs_position( obj, varargin{:} );
                type = 'position'; end
        end

        function [ obj, ind, outerflag ] = parsesubs_logical( obj, ind );
            %parses matlab subscripts for logical indexing
            %   outerflag       flag if the tensor-values are queried (false) or elements inside the tensor-values (true)
            %   ind             logical array of size equal to obj.size (of the output-obj)
            if( ~issequence(ind) );
                if( isequal(sizem(ind),obj.sizeo) );
                    outerflag = true; 
                elseif( isequal(sizem(ind),obj.size) );
                    outerflag = false;
                elseif( isvector(ind) && numel(ind) == obj.numelo )
                    outerflag = true; 
                elseif( isvector(ind) && numel(ind) == obj.numel )
                    outerflag = false;
                else;
                    error( 'sequence:subs', 'Size of logical array not compatible to raw arraw .c.' ); end;
            elseif( obj.ndimso == ind.ndimso && isequal(ind.ndimsi,0) );
                outerflag = true;
                [imin,imax] = equalize( obj.idxmino, obj.idxmaxo, ind.idxmino, ind.idxmaxo ); 
                obj = obj.setidx( imin{1}, imax{1} );
                ind = ind.setidx( imin{2}, imax{2} ).c;
            elseif( obj.ndims == ind.ndims && isequal(obj.sizei,ind.sizei) );
                outerflag = false;
                [imin,imax] = equalize( obj.idxmin, obj.idxmax, ind.idxmin, ind.idxmax ); 
                obj = obj.setidx( imin{1}, imax{1} );
                ind = ind.setidx( imin{2}, imax{2} ).c; end;
        end

        function [ obj, ind, outerflag ] = parsesubs_coordinate( obj, ind );
            %parses matlab subscripts for coordinate-indexing
            %   outerflag       flag if the tensor-values are queried (false) or elements inside the tensor-values (true)
            %   ind             logical array of size equal to obj.size (of the output-obj)
            
            if( size(ind,1) == obj.ndimso )
                outerflag = true;
                %increase size of array
                idxmin_ = min( minm( ind, 2 ), obj.idxmino );
                idxmax_ = max(  maxm( ind, 2 ), obj.idxmaxo );
                obj = obj.setidx( idxmin_, idxmax_ );   
                %compute linear matlab indices
                indmat = mat2cell( ind-idxmin_+1, ones(1,obj.ndimso), size(ind,2) ); 
                ind = sub2ind( [obj.sizeo 1], indmat{:} );  % "1" is a workaround against stupid matlab behaviour
            elseif( size(ind,1) == obj.ndims );
                outerflag = false;
                %increase size of array
                idxmin_ = min( minm( ind, 2 ), obj.idxmin );
                idxmax_ = max( maxm( ind, 2 ), obj.idxmax );
                assert( all(idxmin_(obj.ndimso+1:end) >= 1 & idxmax_(obj.ndimso+1:end) <= obj.sizei.') , 'sequence:subs', 'Coordinates of inner dimensions are wrong. Inner dimensions are one-based indexed.' );
                obj = obj.setidx( idxmin_, idxmax_ );   
                %compute linear matlab indices
                indmat = mat2cell( ind-idxmin_+1, ones(1,obj.ndims), size(ind,2) ); 
                ind = sub2ind( [obj.size 1], indmat{:} );  % "1" is a workaround against stupid matlab behaviour
            else
                error( 'sequence:subs', 'Indexing vector has wrong dimensions.' ); end;
                
        end

        function [ obj, s, outerflag ] = parsesubs_position( obj, varargin );
            % parses matlab subscripts for positional indexing, including extensions defined for sequences ('::' and  'begin')
            
            % parse varargin (subscripts)
            % separate elements of comma separted string
            outerflag = -1;
            s = varargin;
            ssplit = {};
            for i = 1:numel(s)
                if( ischar(s{i}) );
                    val = strsplit( s{i}, ',' )';
                    %val = split( s{i}, ',' );
                    ssplit(end+1:end+numel(val)) = val;
                else;
                    ssplit{end+1} = s{i}; end; end;  %#ok<AGROW>
            s = ssplit;
            
            % parse ::
            ssplit = {};
            count = 0;
            for i = 1:numel(s)
                if( isequal(s{i},'::') );
                    count = count + 1;
                    val = [(numel(ssplit)+1) : tif( numel(ssplit)<obj.ndimso, obj.ndimso, obj.ndims )];
                    ssplit(end+1:end+numel(val)) = arrayfun( @(x) obj.idxmin(x):obj.idxmax(x), val, 'UniformOutput', false ); 
                else;
                    ssplit(end+1) = s(i); end; end;  %#ok<AGROW>
            assert( count <= 2, 'sequence:badsubscript', 'At most two :: are allowed in a ref_ call.' );
            s = ssplit;
                
            
            %parse entries of subscripts
            for i = 1:numel( s );
                try;
                    val = strfind( s{i}, 'end' );
                catch me;  %#ok<NASGU> % needed for octave, if s{i} is not char, then an error is thrown
                    val = []; end;
                if( val );
                    s{i} = [s{i}(1:end-3) num2str(obj.idxmax(i))]; end;
                try;
                    val = strfind( s{i}, 'begin' );
                catch me;  %#ok<NASGU>
                    val = []; end;
                if( val );
                    s{i} = [num2str(obj.idxmin(i)) s{i}(6:end)]; end;
                if( isequal(s{i},':') );
                    if( numel(ssplit) <= obj.ndims );
                        s{i} = obj.idxmin(i) : obj.idxmax(i);
                    else
                        s{i} = 1; end;              
                elseif( ~isnumeric(s{i}) );  % handle case e.g. 5:10
                    s{i} = str2num( s{i} ); end; end;  %#ok<ST2NM>
            
            
            % remove entries 1 at the end, if there are too many entries
            if( numel(s) > obj.ndims );
                k = find( cellfun( @(x) isequal(x,1), s ) == 0, 1, 'last' );
                s(k+1:end) = []; end;
            
            % check input
            assert( numel(s) <= obj.ndims, 'MATLAB:badsubscript', 'Index exceeds array bounds.' ); 
            expect( numel(s) == obj.ndimso || numel(s) == obj.ndims, 'sequence:ref', 'Number of elements in indexing should be equal to outer- or full- dimension.' );
            
            % add tensor subscripts if not given
            for i = i+1:obj.ndims;
                s{i} =  obj.idxmin(i) : obj.idxmax(i); end;            
            
            %increase size of array
            idxmin_ = min( reshape(obj.idxmin(1:obj.ndims),1,[]), cellfun( @(x) x(1), s ) );
            idxmax_ = max( reshape(obj.idxmax(1:obj.ndims),1,[]), cellfun( @(x) x(end), s ) );
            
            obj = obj.setidx( idxmin_, idxmax_ );            
        end
        
%% Helper operators
%%%%%%%%%%%%%%%%%%%%%%%%%
        function [ obj ] = binary( o1, o2, pred );
            % obj = binary( o1, o2, pred );
            % helper function called for binary predicates which work element wise
            %
            % See also: sequence>mbinary, sequence>unary
            if( isscalar(o1) && ~isa(o1,'sequence') );
                obj = sequence( pred(o1,o2.c), o2.idx, nan );
                obj.d = pred(o1,o2.d);
            elseif( isscalar(o2) && ~isa(o2,'sequence') );
                obj = sequence( pred(o1.c,o2), o1.idx, nan );
                obj.d = pred(o1.d,o2);
            else
                [o1,o2] = equalize( o1, o2 );
                obj = sequence( pred(o1.c,o2.c), o1.idx, nan ); 
                obj.d = pred(o1.d,o2.d); end;
        end

        function [ obj ] = unary2dm( obj, pred, realflag, dflag, algflag );
            assert( obj.ndimso <= 2 && obj.ndimsi == 0, 'sequence:dim', 'This function can only be applied to scalar valued 2d-sequences.' );
            if( nargin == 4 && dflag == true );
                assert( ~anym(obj.d), 'sequence:d', 'This function can only be applied to sequences with d=0' ); end;
            obj = equalize( obj, 'square' );
            switch algflag;
                case 0;
                    obj.c = pred( obj.c );
                case 1;
                    obj.c = funm( obj.c, pred );
                case 2;
                    [v_,d_] = eig( obj.c );
                    assert( cond(v_)<1e8, 'sequence:matrixfunc', 'Sequence is not diagonalisable, and thus, this function cannot be applied to it.' );
                    val = v_*diag( pred(diag(d_)) )*pinv(v_);
                    if( nargin == 3 && realflag == true && isreal(obj.c(:)) );
                        val = real( val ); end;
                    obj.c = val;
                otherwise;
                    error( 'sequence:fatal', 'Programming error.' ); end;
        end

        function [ obj ] = unary( obj, pred, varargin );
            % obj = unary( obj, pred, varargin );
            % helper function called for unary predicates which work element wise
            %
            % See also: sequence>mbinary, sequence>binary
            obj.c = pred( obj.c, varargin{:} );
            obj.d = pred( obj.d, varargin{:} );
        end

        function [ obj ] = unarydir( obj, pred, dim );
            if( strcmp(dim,'all') );
                obj = pred( [ obj.c(:); obj.d(:) ] );
            else;
                darg = dim - obj.ndims;
                darg(darg <= 0) = [];
                obj = obj.setidx( obj.idxmin(1:obj.ndims)-1, obj.idxmax(1:obj.ndims)+1 ); %enlarge c with elements from d
                obj.c = pred( obj.c, dim ); 
                p = 1:obj.ndims;
                p(dim) = [];
                p = [p dim];
                obj.c = permute( obj.c, p );
                obj.d
                obj.idx( dim ) = [];
                obj.d = pred( obj.d, darg ); end;            
        end
              
%% Operators written by symbols
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [ obj ] = plus( o1, o2 );        obj = binary( o1, o2, @plus ); end;
        function [ obj ] = minus( o1, o2 );       obj = binary( o1, o2, @minus ); end;
        function [ obj ] = uplus( o1 );           obj = unary( o1, @uplus ); end;
        function [ obj ] = uminus( o1 );          obj = unary( o1, @uminus ); end;
        function [ obj ] = times( o1, o2 );       obj = binary( o1, o2, @times); end;
        function [ obj ] = rdivide( o1, o2 );     obj = binary( o1, o2, @rdivide );  end
        function [ obj ] = ldivide( o1, o2 );     obj = binary( o1, o2, @ldivide );  end
        function [ obj ] = mrdivide( o1, o2 );     % / operator
            obj = o2\o1;
        end

        function [ obj ] = mldivide( o1, o2 );  % \ operator
            if( isscalar(o1) && ~issequence(o1) ) %scalar\sequence
                obj = sequence( o1\o2.c, o2.idx, nan, o1\o2.d );
            elseif( isscalar(o2) && ~issequence(o2) ) %sequence\scalar
                error( 'sequence:mldivide', 'not implemented' );
            elseif( isnumeric(o1) && ~issequence(o1) ); %matrix \ sequence: multiply tensor values
                o1 = pinv( o1 );
                obj = o1*o2;
            elseif( isnumeric(o2) && ~issequence(o2) ); %sequence \ matrix
                error( 'sequence:mldivide', 'not implemented' );
            else; %sequence \ sequence
                error( 'sequence:mldivide', 'not implemented' ); %XX
            end
        end

        function [ obj ] = power( o1, o2 );       obj = binary( o1, o2, @power );    end

        function [ obj ] = mtimes( o1, o2 );
            if( isscalar(o1) && ~issequence(o1) ); %scalar x sequence
                obj = sequence( o1*o2.c, o2.idx, nan, o1*o2.d );
            elseif( isscalar(o2) && ~issequence(o2) ); %sequence x scalar
                obj = sequence( o1.c*o2, o1.idx, nan, o1.d*o2 );
            elseif( isnumeric(o1) && ~issequence(o1) ); %matrix x sequence: multiply tensor values
                c_ = einsum( o1, o2.c, [ndimsm(o1) o2.ndimso+1], sizem(o1), o2.size );
                p = [2:o2.ndimso+1 1 o2.ndimso+2:o2.ndims];
                obj = sequence( permute(c_,p), o2.idx, nan, o1*o2.d );
            elseif( isnumeric(o2) && ~issequence(o2) ); %sequence x matrix: multiply tensor values
                c_ = einsum( o1.c, o2, [o1.ndims 1], o1.size, sizem(o2) );
                obj = sequence( c_, o1.idx, nan, o1.d*o2 );
            else; %sequence x sequence
                expect( all(o1.d(:) == 0) || all(o2.d(:) == 0), 'sequence:mtimes', '''d'' must be zero for one of the matrices used for multiplication.\nThe result is wrong.' );
                if( o1.ndimsi > 0 && o2.ndimsi > 0); %tensor-valued x tensor-valued
                    p = [o1.ndimso 1; o1.ndims o2.ndimso+1];
                    [o1,o2] = equalize( o1, o2, p(1,:) );
                    c_ = einsum( o1.c, o2.c, p, o1.size, o2.size );
                    idx_ = [o1.idx(1:o1.ndimso-1) o2.idx(2:o2.ndimso)].';
                elseif( isscalar(o1) && isscalar(o2) ); %scalar-valued x scalar-valued
                    warning( 'sequence:mtimes', 'How did I get there?' );
                    p = [o1.ndimso 1]; 
                    [o1,o2] = equalize( o1, o2, p(1,:) );
                    c_ = einsum( o1.c, o2.c, p, o1.size, o2.size ); 
                    idx_ = [o1.idx(1:o1.ndimso-1) o2.idx(2:o2.ndimso)].';                     
                else; % tensor-valued x scalar-valued / scalar-valued x tensor-valued
                    p = [o1.ndimso 1];
                    [o1,o2] = equalize( o1, o2, p(1,:) );
                    c_ = einsum( o1.c, o2.c, p, o1.size, o2.size );
                    if( o1.ndims>o1.ndimso ); %tensor x scalar
                        %p2 = [1:o1.ndimso-1 ndimsm(c_)+1:ndimsm(c_)+o2.ndimso-1 o1.ndimso:ndimsm(c_)]; %is this the same?
                        p2 = [1:o1.ndimso-1 o1.ndims:o1.ndims+o2.ndimso-2 o1.ndimso:o1.ndims-1];
                    elseif( o2.ndims>o2.ndimso ) %scaled x tensor
                        p2 = [1:o1.ndimso-1 o1.ndims+1:o1.ndims+o2.ndims-1 o1.ndims]; 
                    else; %scalar x scalar
                        p2 = [1:o1.ndimso-1 o1.ndims+1:o1.ndims+o2.ndimso-1 o1.ndims o1.ndims+o2.ndims];
                    end; %the last dimension is not needed, but it is a singleton dimension, and thus removed by Matlab automatically
                    c_ = permute( c_, p2 );
                    idx_ = [o1.idx(1:o1.ndimso-1) o2.idx(2:o2.ndimso)].'; 
                end;
                obj = sequence( c_, idx_, nan, 0 ); end;
        end

        function [ obj ] = mpower( o1, o2 );   
            % obj = mpower( o1, o2 );
            % Computes powers of sequences
            % If o1 is a scalar, powers are computed element wise
            % Is o2 ia scalar, "products" are computed
            % 
            % See also: sequence>power
            if( isscalar(o1) && issequence(o1) );
                fatal_error;
                o1 = o1.c; end;
            if( isscalar(o2) && issequence(o2) );
                fatal_error;
                o2 = o2.c; end;            
            if( isscalar(o1) && isscalar(o2) );
                obj = sequence( o1^o2, [], nan );
            elseif( isscalar(o1) );
                error( 'sequence:undefined_operation', 'Scalar^sequence not possible.' );
            elseif( isscalar(o2) );
                fatal_error;
                [imin, imax] = equalize( o1, 'square' );  %#ok<ASGLU>
                % XX
            else
                error( 'sequence:not_possible', 'sequence^sequence is not possible.' ); end;
        end

%% Relational and logical Operators
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [ obj ] = eq( o1, o2 ); obj = binary( o1, o2, @eq ); end
        function [ obj ] = ne( o1, o2 ); obj = binary( o1, o2, @ne ); end
        function [ obj ] = gt( o1, o2 ); obj = binary( o1, o2, @gt ); end
        function [ obj ] = ge( o1, o2 ); obj = binary( o1, o2, @ge ); end
        function [ obj ] = le( o1, o2 ); obj = binary( o1, o2, @le ); end
        function [ obj ] = lt( o1, o2 ); obj = binary( o1, o2, @lt ); end

        function [ r ] = isequal( o1, o2 ); 
            % [ r ] = isequal( o1, o2 ); 
            % Checks whether two sequences are logically equal
            %
            % See also: sequence>isequaln
            if( ~issequence(o1) || ~issequence(o2) );
                r = false;
            elseif( ~isequal(o1.sizei, o2.sizei) || ~isequal(o1.d,o2.d) || ~isequal(o1.ndimso,o2.ndimso) );
                r = false;
            else
                [o1,o2] = equalize( o1, o2 );
                r = isequal( o1.c, o2.c ) && isequal( o1.d, o2.d ); end;
        end

        function [ r ] = isequaln( o1, o2 ); 
            % [ r ] = isequaln( o1, o2 ); 
            % Checks whether two sequences are logically equal, treating NaNs as equal
            %
            % See also: sequence>isequal
            if( ~issequence(o1) || ~issequence(o2) );
                r = false;
            elseif( ~isequal(o1.sizei, o2.sizei) || ~isequaln(o1.d,o2.d) );
                r = false;
            else
                [o1,o2] = equalize( o1, o2 );
                r = isequaln( o1.c, o2.c ) && isequaln( o1.d, o2.d ); end;
        end

        function [ obj ] = not( o1 ); obj = unary( o1, @not ); end;
        function [ obj ] = and( o1, o2 );    obj = binary( o1, o2, @and ); end
        function [ obj ] = or( o1, o2 );     obj = binary( o1, o2, @or );  end
        function [ obj ] = xor( o1, o2 );    obj = binary( o1, o2, @xor );  end
        function [ obj ] = bitand( o1, o2 ); obj = binary( o1, o2, @bitand );  end
        function [ obj ] = bitor( o1, o2 );  obj = binary( o1, o2, @bitor );  end
        function [ obj ] = bitxor( o1, o2 ); obj = binary( o1, o2, @bitxor );  end
        function [ obj ] = bitcmp( o1, o2 ); obj = binary( o1, o2, @bitcmp );  end
        function [ obj ] = swapbytes( o1 );  obj = unary( o1, @swapbytes );  end
        
%% element wise functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % simple functions
        function [ obj ] = real( o1 );  obj = unary( o1, @real );  end
        function [ obj ] = imag( o1 );  obj = unary( o1, @imag );  end
        function [ obj ] = conj( o1 );  obj = unary( o1, @conj );  end
        function [ obj ] = abs( o1 );  obj = unary( o1, @abs );  end
        function [ obj ] = sign( o1 );  obj = unary( o1, @sign );  end
        function [ obj ] = angle( o1 );  obj = unary( o1, @angle );  end
        function [ obj ] = round( o1 ); obj = unary( o1, @round ); end
        function [ obj ] = floor( o1 ); obj = unary( o1, @floor); end
        function [ obj ] = ceil( o1 ); obj = unary( o1, @ceil); end
        function [ obj ] = clamp( o1, a, b ); obj = unary( o1, @(x) min(max(x,a),b) ); end
        function [ obj ] = fix( o1 ); obj = unary( o1, @fix); end
        function [ obj ] = mod( o1, a ); obj = unary( o1, @(x) mod(x,a) ); end
        function [ obj ] = rem( o1, a ); obj = unary( o1, @(x) rem(x,a) ); end
        function [ obj ] = remainder( o1, a ); obj = unary( o1, @(x) rem(x,a) ); end
        %analytical functions
        function [ obj ] = reciprocal( o1 ); obj = unary( o1, @(x) 1/x ); end
        function [ obj ] = square( o1 ); obj = unary( o1, @(x) x.^2 ); end
        function [ obj ] = realsqrt( o1 ); obj = unary( o1, @realsqrt); end
        function [ obj ] = hypot( o1, o2 ); obj = binary( o1, o2, @hypot); end
        function [ obj ] = rsqrt( o1 ); obj = unary( o1, @rsqrt); end
        function [ obj ] = sqrt( o1 ); obj = unary( o1, @sqrt); end
        function [ obj ] = exp( o1 ); obj = unary( o1, @exp); end
        function [ obj ] = expint( o1 ); obj = unary( o1, @expint); end
        function [ obj ] = expm1( o1 ); obj = unary( o1, @expm1); end
        function [ obj ] = realpow( o1 ); obj = unary( o1, @realpow); end
        function [ obj ] = log( o1 ); obj = unary( o1, @log); end
        function [ obj ] = log1p( o1 ); obj = unary( o1, @log1p); end
        function [ obj ] = log10( o1 ); obj = unary( o1, @log10); end
        function [ obj ] = log2( o1 ); obj = unary( o1, @log2); end
        function [ obj ] = reallog( o1 ); obj = unary( o1, @reallog); end
        function [ obj ] = sin( o1 ); obj = unary( o1, @sin); end
        function [ obj ] = cos( o1 ); obj = unary( o1, @cos); end
        function [ obj ] = cosm1( o1 ); obj = unary( o1, @(x) cos(x)-x ); end
        function [ obj ] = tan( o1 ); obj = unary( o1, @tan); end
        function [ obj ] = cosh( o1 ); obj = unary( o1, @cosh); end
        function [ obj ] = coshm1( o1 ); obj = unary( o1, @(x) cosh(x)-x ); end
        function [ obj ] = sinh( o1 ); obj = unary( o1, @sinh); end
        function [ obj ] = tanh( o1 ); obj = unary( o1, @tanh); end
        function [ obj ] = asinh( o1 ); obj = unary( o1, @asin); end
        function [ obj ] = acos( o1 ); obj = unary( o1, @acos); end
        function [ obj ] = acos1p( o1 ); obj = unary( o1, @(x) acos(x+1) ); end
        function [ obj ] = atan( o1 ); obj = unary( o1, @atan); end
        function [ obj ] = atan2( o1 ); obj = binary( o1, @atan2); end
        function [ obj ] = deg2rad( o1 ); obj = binary( o1, @deg2rad); end
        function [ obj ] = rad2deg( o1 ); obj = binary( o1, @rad2deg); end
        function [ obj ] = gamma( o1 ); obj = unary( o1, @gamma ); end
        function [ obj ] = gammaln( o1 ); obj = unary( o1, @gammaln ); end
        function [ obj ] = gammainc( o1 ); obj = unary( o1, @gammainc ); end
        function [ obj ] = gammaincinv( o1 ); obj = unary( o1, @gammaincinv ); end
        function [ obj ] = digamma( o1 ); obj = unary( o1, @psi ); end
        function [ obj ] = psi( o1, k ); if( nargin == 1 ); k=0; end; obj = unary( o1, @(x) psi(k,x) ); end
        function [ obj ] = erf( o1 ); obj = unary( o1, @erf ); end
        function [ obj ] = erfinv( o1 ); obj = unary( o1, @erfinv ); end
        function [ obj ] = erfc( o1 ); obj = unary( o1, @erfc ); end
        function [ obj ] = erfcinv( o1 ); obj = unary( o1, @erfcinv ); end
        function [ obj ] = erfcx( o1 ); obj = unary( o1, @erfcx ); end
        function [ obj ] = sigmoid( o1 ); obj = unary( o1, @(x) 1./(1+exp(-x)) ); end
        
        
%% casts
%%%%%%%%%%%%%
        function [ obj ] = logical( o1 ); obj = unary( o1, @logical ); end;
        function [ obj ] = half( o1 ); obj = unary( o1, @half ); end;
        function [ obj ] = single( o1 ); obj = unary( o1, @single ); end;
        function [ obj ] = double( o1 ); obj = unary( o1, @double ); end;
        function [ obj ] = int8( o1 ); obj = unary( o1, @int8 ); end;
        function [ obj ] = int16( o1 ); obj = unary( o1, @int16 ); end;
        function [ obj ] = int32( o1 ); obj = unary( o1, @int32 ); end;
        function [ obj ] = int64( o1 ); obj = unary( o1, @int64 ); end;
        function [ obj ] = uint8( o1 ); obj = unary( o1, @uint8 ); end;
        function [ obj ] = uint16( o1 ); obj = unary( o1, @uint16 ); end;
        function [ obj ] = uint32( o1 ); obj = unary( o1, @uint32 ); end;
        function [ obj ] = uint64( o1 ); obj = unary( o1, @uint64 ); end;    

        function [ obj ] = cast( arg1, arg2, arg3 );
            if( nargin == 1 );
                error( 'sequence:cast', 'Too less inputs.' );
            elseif( nargin == 2 && (isstring(arg2) || ischar(arg2)) );
                obj = arg1;
                obj.c = cast( obj.c, arg2 );
                obj.idx = cast( obj.idx, arg2 );
                obj.d = cast( obj.d, arg2 );
            elseif( nargin == 3 );
                assert( isequal(arg2,'like'), 'If three arguments are given, then second argument must be ''like''.' );
                if( ~issequence(arg1) && issequence(arg3) );
                    assert( ndimsm(arg1) == ndims(arg3), 'sequence:wrongcast', 'Dimensions not compatible.' );
                    obj = sequence( cast(arg1,'like',arg3.c), cast(zeros(arg3.ndimso,1),'like',arg3.idx), nan, cast(zeros(size(arg3.d)),'like',arg3.d) );
                elseif( issequence(arg1) && issequence(arg3) );
                    obj = arg1;
                    obj.c = cast( obj.c, 'like', arg3 );
                    obj.d = cast( obj.d, 'like', arg3 ); 
                else; %issequence(arg1) && ~issequence(arg3)
                    obj = arg1;
                    obj.c = cast( obj.c, 'like', arg3 );
                    obj.idx = cast( obj.idx, 'like', arg3 );
                    obj.d = cast( obj.d, 'like', arg3 ); end;
            else; 
                error( 'sequence:cast', 'Wrong cast.' ); end;
        end

        function [ obj ] = typecast( obj, arg1 );
            warning( 'sequence:typecast', 'typecast does not cast the index.' );
            obj.c = typecast( obj.c, arg1 );
            obj.d = typecast( obj.d, arg1 );
        end
            
        
%% is.. functions
%%%%%%%%%%%%%%%%%%%%%%
        function [ ret ] = issequence( ~ ); 
            ret = true;
        end

        function [ ret ] = isbanded( obj, lb, ub );
            if( obj.ndims ~= 2 || ~isequal(obj.d,0) );
                ret = false; 
                return; end;
            idx_ = obj.idx;
            idx_(:) = min(idx_);
            obj = obj.characteristico.setidx( idx_ );
            lb = min( lb, max(obj.size) );
            ub = min( ub, max(obj.size) );
            ret = isbanded( double(obj.c), lb, ub );
        end

        function [ ret ] = iscolumn( obj );
            if( ~isequal(obj.d,0) );
                ret = false; 
                return; end;
            sz = obj.sizeo;
            sz(1) = [];
            ret = sz(:) == 1;
        end

        function [ ret ] = isdiag( obj ); ret = isbanded( obj, 0, 0 ); end
        function [ ret ] = isfloat( obj ); ret = isfloat(obj.c); end
        function [ ret ] = isinteger( obj ); ret = isinteger(obj.c); end
        function [ ret ] = islogical( obj ); ret = islogical(obj.c); end
        function [ ret ] = isnumeric( obj ); ret = isnumeric(obj.c); end
        function [ ret ] = ismatrix( obj ); ret = obj.ndims == 2 && obj.ndimsi == 0; end
        function [ obj ] = isprime( obj ); obj = sequence( isprime(obj.c),obj.idx, nan, isprime(obj.d) ); end
        function [ obj ] = isreal( obj ); obj = isreal(obj.c) && isreal(obj.d); end

        function [ ret ] = isrow( obj );
            if( obj.ndimso == 1 || ~isequal(obj.d,0) );
                ret = false; 
                return; end;
            sz = obj.sizeo;
            sz(2) = [];
            ret = sz(:) == 1;
        end

        function [ ret ] = isscalar( obj ); ret = obj.ndimso == 0; end
        function [ ret ] = issparse( obj ); ret = issparse( obj.c ); end
        function [ ret ] = istril( obj ); ret = isbanded( obj, inf, 0 ); end
        function [ ret ] = istriu( obj ); ret = isbanded( obj, 0, inf ); end

        function [ ret ] = isvector( obj ); 
            if( ~isequal(obj.d,0) );
                ret = false;
                return; end;
            sz = obj.sizeo;
            ret = nnz( sz == 1 ) == 1;
        end
        
%% Octave bugfix functions
        function [ obj ] = set_c( obj, value ); obj.c = value; end
        function [ obj ] = set_idx( obj, value ); obj.idx = value; end
        function [ obj ] = set_d( obj, value ); obj.d = value; end
        
%% Functions for arrays
%%%%%%%%%%%%%%%%%%%%%%%%%
        function [ r ] = idxmin( obj, varargin );
            % r = idxmin( obj, [dim | dim1, ... , dimN] );
            % Returns the smallest index currently stored in .c 
            % The first entries belong to the outer dimension, the latter entries to the inner dimensions
            % Note that, contrary to Matlabs behaviour, a trailing singleton dimension is not removed.
            % Thus, a scalar valued 2d-sequence has idxmin [?,?,1], where ?,? are integers
            %
            % when k is given, only the k^th entry is returned
            % when k is negative, the ndimsm+k+1 entry is returned
            % k can be a vector
            %
            % See also: idxmino, idxmini, idxmax, idxmaxi, idxmaxo, sizeo, sizei, size
            r = [obj.idx; ones(obj.ndimsi,1)];
            if( obj.ndimsi == 0 );
                r = [r;1]; end;
            if( nargin > 1 );
                dim = [varargin{:}];
                r = [r;1]; %add one at the end to handle out-of-bounds dimensions in varargin
                dim(dim <= 0) = dim(dim <= 0) + numel( r );
                dim(dim <= 0 | dim>=numel(r)) = numel( r );
                r = r( dim ); end;            
        end

        function [ r ] = idxmax( obj, varargin );
            % r = idxmax( obj, [dim | dim1, ... , dimN] );
            % returns the largest index currently stored in .c
            % The first entries belong to the outer dimension, the latter entries to the inner dimensions
            % Note that, contrary to Matlabs behaviour, a trailing singleton dimension is not removed.
            % Thus, a scalar valued 2d-sequence has idxmin [?,?,1], where ?,? are integers            
            %
            % See also: idxmino, idxmini, idxmax, idxmaxi, idxmaxo, sizeo, sizei, size
            r = [obj.idx + sizeo( obj ).' - 1; sizei(obj).'];
            if( nargin > 1 );
                dim = [varargin{:}];
                r = [r;1]; %add one at the end to handle out-of-bounds dimensions in varargin
                dim(dim <= 0) = dim(dim <= 0) + numel( r );
                dim(dim <= 0 | dim>=numel(r)) = numel( r );
                r = r( dim ); end;
        end

        function [ r ] = idxmino( obj, varargin );
            % r = idxmino( obj, [dim | dim1, ... , dimN] );
            % returns the smallest index of outer dimensions currently stored in .c
            %
            % See also: idxmino, idxmini, idxmax, idxmaxi, idxmaxo, sizeo, sizei, size
            r = obj.idx;
            if( nargin > 1 );
                dim = [varargin{:}];
                r = [r;1]; %add one at the end to handle out-of-bounds dimensions in varargin
                dim(dim <= 0) = dim(dim <= 0) + numel( r );
                dim(dim <= 0 | dim>=numel(r)) = numel( r );
                r = r( dim ); end;
        end

        function [ r ] = idxmaxo( obj, varargin );
            % r = idxmino( obj, [dim | dim1, ... , dimN] );
            % returns the largest index of outer dimensions currently stored in .c
            %
            % See also: idxmino, idxmini, idxmax, idxmaxi, idxmaxo, sizeo, sizei, size
            r = obj.idx + sizeo( obj ).' - 1; 
            if( nargin > 1 );
                dim = [varargin{:}];
                r = [r;1]; %add one at the end to handle out-of-bounds dimensions in varargin
                dim(dim <= 0) = dim(dim <= 0) + numel( r );
                dim(dim <= 0 | dim>=numel(r)) = numel( r );
                r = r( dim ); end;
        end

        function [ r ] = idxmini( obj, varargin );
            % r = idxmini( obj, [dim | dim1, ... , dimN] );
            % returns the smallest index of inner dimensions currently stored in .c
            % (this is always 1,...,1)
            %
            % See also: idxmino, idxmini, idxmax, idxmaxi, idxmaxo, sizeo, sizei, size
            r = ones( obj.ndimsi, 1 );
            if( obj.ndimsi == 0 );
                r = 1; end;
            if( nargin > 1 );
                dim = [varargin{:}];
                r = [r;1]; %add one at the end to handle out-of-bounds dimensions in varargin
                dim(dim <= 0) = dim(dim <= 0) + numel( r );
                dim(dim <= 0 | dim>=numel(r)) = numel( r );
                r = r( dim ); end;
        end

        function [ r ] = idxmaxi( obj, varargin );
            % r = idxmaxi( obj, [dim | dim1, ... , dimN] );
            % returns the largest index of inner dimensions currently stored in .c
            % (this is always n1,...,nN)
            %
            % See also: idxmino, idxmini, idxmax, idxmaxi, idxmaxo, sizeo, sizei, size
            r = sizei( obj ).';
            if( obj.ndimsi == 0 );
                r = 1; end;            
            if( nargin > 1 );
                dim = [varargin{:}];
                r = [r;1]; %add one at the end to handle out-of-bounds dimensions in varargin
                dim(dim <= 0) = dim(dim <= 0) + numel( r );
                dim(dim <= 0 | dim>=numel(r)) = numel( r );
                r = r( dim ); end;
        end

        function [ r ] = nnz( obj );
            % r = nnz( obj );
            % returns the number of nonzero elements in the sequence.
            % if .d is non-zero, inf is returned
            %
            % See also: nnz, nnzo, nnd, nndo, numel, numelo, ndims, ndimso, ndimsi
            r = tif( all(obj.d(:) == 0), nnz( obj.c ), inf ); 
        end

        function [ r ] = nnzo( obj );
            % r = nnzo( obj );
            % returns the number of nonzero tensor-elements
            %
            % See also: nnz, nnzo, nnd, nndo, numel, numelo, ndims, ndimso, ndimsi            
            r = tif( all(obj.d(:) == 0), nnz(anym(obj.c,obj.ndimso+1:obj.ndims)), inf );
        end

        function [ r ] = nnd( obj ); 
            % r = nnd( obj );
            % returns the number of elements unequal to .d in the sequence
            %
            % See also: nnz, nnzo, nnd, nndo, numel, numelo, ndims, ndimso, ndimsi            
            r = nnz( obj.c~=obj.d ); 
        end

        function [ r ] = nndo( obj );
            % r = nndo( obj );
            % returns the number of tensor-elements in the sequence unequal to .
            %
            % See also: nnz, nnzo, nnd, nndo, numel, numelo, ndims, ndimso, ndimsi            
            r = nnz( anym(obj.c ~= obj.d, obj.ndimso+1:obj.ndims) );
        end

        function [ r ] = numel( obj ); 
            % r = numel( obj );
            % returns the current number of elements in .c
            %
            % See also: nnz, nnzo, nnd, nndo, numel, numelo, ndims, ndimso, ndimsi            
            r = numel( obj.c ); 
        end

        function [ r ] = numelo( obj );
            % r = numel( obj );
            % returns the current number of tensor-elements in .c            
            % i.e. r = numel( obj.c )/prod( sz(obj.ndimso+1:obj.ndims) ); 
            %
            % See also: nnz, nnzo, nnd, nndo, numel, numelo, ndims, ndimso, ndimsi            
            sz = size( obj );
            r = numel( obj.c )/prod( sz(obj.ndimso+1:obj.ndims) ); 
        end

        function [ r ] = numeli( obj );
            % r = numeli( obj );
            % returns the number of elements in one tensor-elements
            % i.e. r = prod(obj.sizei)
            %
            % See also: nnz, nnzo, nnd, nndo, numel, numelo, ndims, ndimso, ndimsi            
            r = prod( obj.sizei );
        end

        function [ r ] = ndims( obj ); % dimension / dimension of sequence + dimension of values
            % r = ndims( obj )
            % returns the number of dimensions / dimension of sequence + tensor-dimension of values
            %
            % See also: nnz, nnzo, nnd, nndo, numel, numelo, ndims, ndimso, ndimsi            
            r = max( numel(obj.idx), ndimsm(obj.c) ); 
        end

        function [ r ] = ndimso( obj ); %number of outer dimensions / dimension of sequence
            % r = ndimso( obj )
            % returns the number of outer dimensions
            %
            % See also: nnz, nnzo, nnd, nndo, numel, numelo, ndims, ndimso, ndimsi            
            r = numel( obj.idx );  
        end

        function [ r ] = ndimsi( obj ); %number of inner dimensions / dimension of values
            % r = ndimso( obj )
            % returns the number of inner dimensions     
            % Scalar valued sequences have ndimsi == 0
            %
            % See also: nnz, nnzo, nnd, nndo, numel, numelo, ndims, ndimso, ndimsi            
            r = max( 0, ndimsm(obj.c)-numel(obj.idx) );
        end

        function [ r ] = size( obj, varargin );
            % r = size( obj, dim )
            % r = size( obj, dim1, ..., dimN )
            % NOT IMPLEMENTED YET: [r1, ..., rN] = size( __ ); %XX
            % returns the size() of the array .c
            %   dim   vector, if given, only the dimensions in dim are returned
            %   dimi, vector, if given, only the dimensions dimi are returned
            %   N     scalar, dim must be empty, returns the first N dimensions
            %   r1, ..., rN,  if given, dimensions are returned separately
            % Singleton dimensions are not removed, i.e. scalar sequences have size [?,...,?,1]
            %
            % See also: sizeo, sizei, size
            if( nargin == 1 );      
                r = sizem( obj.c, 1:obj.ndims ); %Some Matlab releases cant display 1d-sequences with this size-function
                if( obj.ndimsi == 0 );
                    r = [r 1]; end;
            else;
                r = sizem( obj.c, varargin{:} );  end;
        end

        function [ r ] = sizeo( obj, varargin )
            % returns the outer size() of the array .c
            %
            % See also: sizeo, sizei, size, idxmino, idxmini, idxmax, idxmaxi, idxmaxo
            r = sizem( obj.c, 1:obj.ndimso );
            if( nargin > 1 );
                r = [r 1]; %add one at the end to handle out-of-bounds dimensions in varargin
                dim = [varargin{:}];
                dim(dim <= 0) = dim(dim <= 0) + numel(r);
                dim(dim <= 0 | dim>=numel(r)) = numel(r);
                r = r( dim ); end;
        end

        function [ r ] = sizei( obj, varargin );
            % r = sizei( obj, dim, N )
            % returns the inner size() of the array .c
            %
            % See also: sizeo, sizei, size, idxmino, idxmini, idxmax, idxmaxi, idxmaxo            
            r = sizem( obj.c, obj.ndimso+1:obj.ndims );
            if( isempty(r) );
                r = 1; end;
            if( nargin > 1 );
                r = [r 1]; %add one at the end to handle out-of-bounds dimensions in varargin
                dim = [varargin{:}];
                dim(dim <= 0) = dim(dim <= 0) + numel(r);
                dim(dim <= 0 | dim>=numel(r)) = numel(r);
                r = r( dim ); end;
        end
        
        function [ varargout ] = equalize( varargin );
            % [oout1, ..., ooutn] = equalize( oin1, ..., oionn, ['square', ['D'],D] )
            % Resizes the arrays oini.c such the all arrays oini have the same idxmino and idxmaxo
            % E.g.:
            %       c1 = sequence( [1 2; 3 4]',[1;0]);
            %       c2 = sequence( [2;4]',[0;0]);
            %       D = [2 1];
            %       [c1e,c2e] = equalize( c1, c2, D)
            %
            % See also: equalize
            
            % parse input
            arg = {};
            if( isequal(varargin{end},'square') );
                arg = {'square'};
                varargin(end) = []; end;
            if ( numel(varargin) >= 2 && isequal(varargin{end-1},'D') );
                arg = varargin(end);
                varargin(end-1:end) = [];
            elseif( numel(varargin) >= 1 && ~issequence(varargin{end}) );
                arg = varargin(end);
                varargin(end) = []; end;
            
            % get indices
            ind = cell( 2, numel(varargin) );
            for i = 1:numel( varargin );
                ind{1,i} = varargin{i}.idxmino;
                ind{2,i} = varargin{i}.idxmaxo; end;
            
            % do the work
            [imino,imaxo] = equalize( ind{:}, arg{:} );
            
            % make output
            varargout = cell( size(varargin) );
            for i = 1:numel( varargin );
                varargout{i} = varargin{i}.setidx( imino{i}, imaxo{i} ); end;
        end

        function [ obj ] = shrink( obj );
            % obj = shrink( obj ) 
            % removes "zeros" at the border, does not change logically the sequence
            % does not remove "zeros" in the tensor valued entries
            % 
            % See also: removezero
            doo = obj.ndimso;
            [c_,origin_] = removezero( obj.c, 'dim', obj.ndimso, 'border', 'ignore', obj.ndimso+1:obj.ndims, 'value',obj.d );
            origin_ = origin_(:).';
            obj.c = c_;
            obj.idx = obj.idxmin(1:obj.ndims) + origin_(:) - 1;
            obj.idx = obj.idx(1:doo);
        end

        function [ obj ] = setidx( obj, idxminonew, idxmaxonew );
            % obj = setidx( obj, idxminnewo, [idxmaxnewo] );
            % Sets the minidxo and maxidxo of .c to idxminnewo and idxmaxnewo (the latter only of it is given)
            % 
            % See also: setidx
            if( nargin <= 2 );
                idxmaxonew = []; end;

            if( isscalar(obj.d) ); %if d is a scalar, padding is easy
                if( numel(idxminonew) == obj.ndims );
                    idxmin = obj.idxmin;
                else;
                    idxmin = obj.idxmino; end;
                [ obj.c, idx_ ] = setidx( obj.c, idxmin, idxminonew, idxmaxonew, obj.d );
            else;
                p = [obj.ndimsi+1:obj.ndims 1:obj.ndimsi];
                val = cellfun(@(x) permute(x,p),num2cell(obj.c,obj.ndimsi+1:obj.ndims),'UniformOutput',false);
                [ val, idx_ ] = setidx( val, obj.idxmin(1:obj.ndimsi), idxminonew(1:obj.ndimsi), idxmaxonew(1:obj.ndimsi), obj.d );
                val = cellfun( @(x) permute(x,p), val, 'UniformOutput',false );
                obj.c = cell2mat( val ); end;
            obj.idx = idx_(1:obj.ndimso);
        end

        function [ obj ] = squeeze( obj, dim );
            % obj = squeeze( obj, [dim] )
            % squeezes the array .c and removes all 1s from .idx
            % if dim is given, only dimensions dim is removed. 
            % An error is thrown if a non-singleton dimension shall be removed
            % After squeeze, the sequence may have a smaller outer and/or inner size
            %
            % See also: squeeze, squeezeo, squeezei
            if( nargin == 1 );
                dim = find( obj.size == 1 ); end;
            dim(dim<0) = dim(dim<0) + obj.ndims + 1;
            
            szi = obj.sizei;
            szc = obj.size;
            % handle c
            
            assert( all(szc(dim) == 1), 'squeeze:dim', 'Only singleton dimensions can be removed.' );
            szc( dim ) = [];
            obj.c = reshape( obj.c, [szc 1 1] );
            
            %handle d
            if( ~isscalar(obj.d) );
                ddim = dim - obj.ndimso;
                ddim(ddim <= 0) = []; 
                szi( ddim ) = [];
                obj.d = reshape( obj.d,  [szi 1 1] ); end;
            
            %handle idx
            dim( dim>obj.ndimso ) = [];
            obj.idx( dim ) = [];
        end

        function [ obj ] = squeezeo( obj, dim );
            % See also: squeeze, squeezeo, squeezei
            if( nargin == 1 );
                dim = find( obj.sizeo == 1 ); end;
            dim(dim<0) = dim(dim<0) + obj.ndimso + 1;
            dim(dim>obj.ndimso) = [];
            obj = squeeze( obj, dim );
        end

        function [ obj ] = squeezei( obj, dim );
            % See also: squeeze, squeezeo, squeezei
            if( nargin == 1 );
                dim = find( obj.sizei == 1 ) + obj.ndimso;
            else; 
                dim(dim<0) = dim(dim<0) + obj.ndimsi + 1;
                dim = dim + obj.ndimso; end;
            
            obj = squeeze( obj, dim );
        end

        function [ c_ ] = compact( obj );
            % c = compact( obj )
            % compactifies tensor valued arrays
            % Returns a plain matlab array
            %
            % See also: compact
            c_ = compact( obj.c, obj.ndimso );
        end
        
        function [ obj ] = transpose( obj );
            % obj = transpose( obj );
            % flips the dimensions of the sequence
            % See also: sequence>transposeo, sequence>transposei, sequence>ctranspose, 
            expect( obj.ndimsi<1, 'sequence:transpose', ['You may want to use ' inputname(1) '.transposeo or ' inputname(1) '.transposei to transpose.'] );
            obj.c = permute( obj.c, [obj.ndimso:-1:1 obj.ndims:-1:(obj.ndimso+1)] );
            obj.idx = obj.idx( obj.ndimso:-1:1 );
        end

        function [ obj ] = ctranspose( obj );
            % obj = transpose( obj );
            % conjugate flips the dimensions of the sequence
            % See also: sequence>ctransposei, sequence>transpose, sequence>ctransposeo
            expect( obj.ndimsi<1, 'sequence:transpose', ['You may want to use ' inputname(1) '.ctransposeo or ' inputname(1) '.ctransposei to ctranspose.'] );
            obj.c = permute( obj.c, [obj.ndimso:-1:1 obj.ndims:-1:(obj.ndimso+1)] );
            obj.c = conj( obj.c );
            obj.idx = obj.idx( obj.ndimso:-1:1 );
        end

        function [ obj ] = transposeo( obj );
            % obj = transpose( obj );
            % flips the dimensions of the outer dimensions
            % See also: sequence>transposeo, sequence>transposei, sequence>ctranspose
            obj.c = permute( obj.c, [obj.ndimso:-1:1 obj.ndimso+1:obj.ndims] );
            obj.idx = obj.idx( obj.ndimso:-1:1 );
        end

        function [ obj ] = ctransposeo( obj );
            % obj = transpose( obj );
            % conjugate flips the dimensions of the outer dimensions
            % See also: sequence>transpose, sequence>ctransposeo, sequence>ctransposei        
            obj.c = permute( obj.c, [obj.ndimso:-1:1 obj.ndimso+1:obj.ndims] );
            obj.c = conj( obj.c );
            obj.idx = obj.idx( obj.ndimso:-1:1 );
        end

        function [ obj ] = transposei( obj );
            % obj = transpose( obj );
            % flips the dimensions of the inner dimensions
            % Note that .transposei may remove singleton dimensions
            % See also: sequence>transposeo, sequence>transposei, sequence>ctranspose
            obj.c = permute( obj.c, [1:obj.ndimso obj.ndims:-1:obj.ndimso+1] );
            obj.idx = obj.idx( obj.ndimso:-1:1 );
        end

        function [ obj ] = ctransposei( obj );
            % obj = transpose( obj );
            % conjugate flips the dimensions of the inner dimensions
            % Note that .ctransposei may remove singleton dimensions
            % See also: sequence>ctransposeo, sequence>ctransposei, sequence>transpose
            obj.c = permute( obj.c, [1:obj.ndimso obj.ndims:-1:obj.ndimso+1] );
            obj.c = conj( obj.c );
            obj.idx = obj.idx( obj.ndimso:-1:1 );
        end           


%% linear algebra
%%%%%%%%%%%%%%%%%%%
        function [ r ] = norm( obj, p );
            % r = norm( obj, [p] );
            % takes the norm of a sequence,
            % -inf <= p <= inf, default: p=2
            %
            % See also: sequence>sum
            % XX
            
            if( nargin == 1 );
                p = 2; end;
            if( isequal(p,inf) || strcmp(p,'inf') );
                r = max( abs(obj.d), max( abs(obj.c(:)) ) ); 
            elseif( p==-inf );
                r = min( abs(obj.d), min( abs(obj.c(:)) ) ); 
            elseif( p == 0 );
                r = nnz( obj );
            else;
                r = tif( obj.d, inf, summ( abs(obj.c).^p, [] )^(1/p) ); end;
        end
        
%% functions for matrices (2-tensors)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [ d ] = det( obj ); 
            assert( obj.ndimso == 2 && (obj.ndimsi == 1 || obj.ndimsi == 2) && ~diff(obj,sizei), 'sequence:dim', 'This function can only be applied to scalar valued or square-matrices valued 2d-sequences.' );
            d = cast( zerosm(obj.sizei), 'like', obj.c ); 
        end

        function [ obj ] = diag( obj );
            if( obj.ndimso == 1 );
                sz = obj.sizeo;
                if( isscalar(obj.d) );
                    obj.d = repmat( obj.d, [obj.sizei 1] ); end;
                c_ = repmat( shiftdim(obj.d,-2), [sz sz] );
                ddot = repmat( {':'}, 1, obj.ndimsi );
                for i = 1:sz;
                    c_(i,i,ddot{:}) = obj.c(i,ddot{:}); end;
                obj = sequence( c_, [obj.idx(1);obj.idx(1)], nan, obj.d );
            elseif( obj.ndimso == 2 );
                obj = equalize( obj, 'square' );
                sz = obj.sizeo(1);
                c_ = zeros( [sz obj.sizei 1 1] );
                ddot = repmat( {':'}, 1, obj.ndimsi );
                for i = 1:sz;
                    c_(i,ddot{:}) = obj.c(i,i,ddot{:}); end;
                obj = sequence( c_, obj.idx(1), nan, obj.d );
            else;
                error( 'sequence:dim', 'This function can only be applied to 1d- or 2d-sequences.' ); end; 
        end

        function [ obj ] = expm1m( obj );   obj = unary2dm( obj, @expm1, true, true, 2 );   end
        function [ obj ] = log1pm( obj );   obj = unary2dm( obj, @log1p, true, true, 2 );   end
        function [ obj ] = sqrtm( obj );    obj = unary2dm( obj, @sqrtm, true, true, 0 );   end
        function [ obj ] = sinm( obj );     obj = unary2dm( obj, @sin, true, true, 1 );     end
        function [ obj ] = cosm1m( obj );   obj = unary2dm( obj, @(x) cos(x) - 1, true, true, 2 );   end
        function [ obj ] = tanm( obj );     obj = unary2dm( obj, @tan, true, true, 2 );     end
        function [ obj ] = coshm1m( obj );  obj = unary2dm( obj, @(x) cosh(x) - 1, true, true, 2 );  end
        function [ obj ] = sinhm( obj );    obj = unary2dm( obj, @sinh, true, true, 1 );    end
        function [ obj ] = tanhm( obj );    obj = unary2dm( obj, @tanh, true, true, 2 );    end
        function [ obj ] = asinm( obj );    obj = unary2dm( obj, @asin, true, true, 2 );    end
        function [ obj ] = acos1pm( obj );  obj = unary2dm( obj, @(x) acos(x + 1), true, true, 2 );  end
        function [ obj ] = acosh1pm( obj ); obj = unary2dm( obj, @(x) acosh(x + 1), true, true, 2 );  end
        function [ obj ] = atanm( obj );    obj = unary2dm( obj, @atan, true, true, 2 );    end
        function [ obj ] = asinhm( obj );   obj = unary2dm( obj, @asinh, true, true, 2 );   end
        function [ obj ] = atanhm( obj );   obj = unary2dm( obj, @atanh, true, true, 2 );   end        

        function [ obj ] = signm( obj );
            assert( ~anym(obj.d), 'sequence:d', 'This function can only be applied to sequences with d=0' );
            assert( obj.ndimso == 2 && obj.ndimsi == 0, 'sequence:dim', 'This function can only be applied to scalar valued 2d-sequences.' );
            obj = equalize( obj, 'square' );
            c_ = obj.c;
%           % Newton iteration            
%             t = @(X) 1/2*(X+pinv(X)); 
%             while( true );
%                 cn_ = t(t(t(t(c_))));
%                 if( norm(cn_-c_,'inf')<1e-12 );
%                     break;
%                 else;
%                     c_ = cn_; end; end;

%           % Using Eigenvectors
%             function [ r ] = sgn( x );
%                 r = ones(size(x));
%                 r(real(x)<0) = -1;
%                 r(real(x) == 0) = 0;
%             end 
%             [v_,d_] = eig( c_ );
%             if( isreal(c_(:)) );
%                 c_ = real( v_*diag( sgn(diag(d_)) )/v_ );
%             else;
%                 c_ = v_*diag( sgn(diag(d_)) )/v_; end;
            
            
            % Using formular from scalar case
            if( isreal(c_) );
                c_ = real( c_ * pinv((c_^2))^(1/2) );
            else;
                c_ = c_ * pinv((c_^2))^(1/2); end;
            
            obj.c = c_;
            
            %
           
        end
        
        %function d = deti( obj );  %XX
        %    obj = unary2dmi( obj, @det
        %
        
%% reduction operations
%%%%%%%%%%%%%%%%%%%%%%%%%
% norm( c, p, dim, nanflag ) %very special
% cum/mov/. std, topk ( c, x, dim, nanflag, direction )
% cum/mov/. sum, prod, (arg)min, (arg)max, mean, median, logsumexp, all, any (o,i,.) ( c, dim, nanflag, direction )
% (cum/mov/.)-sum,-prod,
% 2d: trace, 
        function [ obj ] = all( o1, dim );
            if(nargin == 1);
                dim='all'; end;
            obj = unarydir( o1, @all, dim );
        end
        
        function [ obj ] = any( o1, dim );   obj = unarydir( o1, @any, dim ); end

        function [ r ] = trace( obj ); 
            assert( obj.ndimso == 2, 'sequence:dim', 'This function can only be applied to 2d-sequences.' );
            if( anym(obj.d ~= 0) );
                r = inf*obj.d;
            else;
                obj = equalize( obj, 'square' );
                c_ = obj.c;
                r = zerosm( size(obj.d) );
                for i = 1:size(c_,1)
                    r = r + c_(i,i,:,:); end; 
                r = shiftdim( r, obj.ndimso ); end;
            if( isscalar(r) );
                r = r*onesm( obj.sizei ); end;
        end

        function [ obj ] = sum( obj, dim );
            assert( nargin == 2, 'sequence:sum', '''sum'' needs two input parameters.' );

            if( isscalar(obj.d) );
                obj.d = repmat( obj.d, obj.sizei ); end;
            sumd = infsum( real(obj.d) ) + infsum( imag(obj.d) );
            if( isequal(dim,'all') );
                obj = sum(obj.c(:)) + sum(sumd(:));
            else;
                for i = dim;
                    obj.c = sum( obj.c, i ); end;
                obj.c = obj.c + shiftdim( sumd, -obj.ndimso ); end;
        end

        function [ obj ] = sumo( obj, dim );
            % See also: sequence>sum, sequence>sumo, sequence>sumi
            assert( nargin == 2, 'sequence:sum', '''sum'' needs two input parameters.' );
            dim(dim > obj.ndimso) = [];
            obj = sum( obj, dim );
        end

        function [ obj ] = sumi( obj, dim );  % XX
            % See also: sequence>sum, sequence>sumo, sequence>sumi
            assert( nargin == 2, 'sequence:sum', '''sum'' needs two input parameters.' );
            dim = dim + obj.ndimso;
            dim(dim > obj.ndims) = [];
            obj = sum( obj, dim );
        end        
        
%% harmonic analysis
%%%%%%%%%%%%%%%%%%%%%%%%%%%
        function [ r ] = supp( obj, antiflag ); 
            % [ r ] = supp( obj ); 
            % returns the support or the anti support (if d ~= 0 or flag is set) excluding the inner dimensions
            %
            % See also: supp, sequence>symbol, sequence>characteristic, sequence>characteristico
            if( obj.d );
                expect( nargin == 2 && antiflag, 'sequence:supp', 'The anti-support is returned. To disable this warning set flag == true.' );
                r = supp( ~obj.characteristico.c, obj.ndimso, obj.idx ); 
            else;
                assert( nargin == 1 || ~antiflag, 'sequence:supp', 'The support is returned. To disable this error set flag==false.' );
                r = supp( obj.characteristico.c, obj.ndimso, obj.idx ); end;
        end

        function [ obj ] = characteristic( obj );
            % r = characteristic( obj ); 
            % returns the characteristic sequence or the anti characteristic sequence (if d ~= 0)
            %
            % See also: sequence>characteristico, sequence>symbol
            if( obj.d );
                obj.c = (obj.c == 0);
            else
                obj.c = (obj.c ~= 0); end;
        end

        function [ obj ] = characteristico( obj );
            % r = characteristico( obj ); 
            % returns the characteristic sequence or the anti characteristic sequence (if d ~= 0)
            %
            % See also: sequence>characteristico, sequence>symbol              
            if( obj.d );
                c_ = anym( ~obj.c, [obj.ndimso+1:obj.ndims] );
                obj = sequence( c_, obj.idx, nan );
            else
                c_ = anym( obj.c, [obj.ndimso+1:obj.ndims] );
                obj = sequence( c_, obj.idx, nan ); end;
        end

        function [ r ] = symbol( obj ); 
            % r = supp( obj ); 
            %returns the symbol after substracting the constant part d
            %
            % See also: sequence>supp, sequence>characteristic, sequence>characteristico
            
            r = mask2symbol( obj.c-obj.d, 'amin', obj.idx, 'dim', obj.ndims );
        end

        function [ obj ] = upsample( obj, M ); 
            % r = supp( obj, M ); 
            % upsamples the sequence
            % M must be a non-singular integer square matrix
            %
            % See also: sequence>supp
            
            assert( ~logical(obj.d), 'sequence:d', 'Upsampling is only possible with sequences where d=0.' );
            [c_, idx_] = upsamplem( obj.c, obj.idx, M, 0, obj.ndims > obj.ndimso );  %XX
            obj = sequence( c_, idx_, nan ); 
        end

        function [ oout ] = conv( varargin );
            % r = conv( oin1, ..., oinn ); 
            % convolves sequences
            % The output dimensions (inner and outer) are determined by the input dimensions
            
            
            assert( ~any(diff(cellfun( @(x) x.ndimso, varargin ))), 'sequence:ndimso', 'For conv, all sequences must have the same outer dimension.' );
            assert( nnz(cellfun( @(x) x.d, varargin )) <= 1 , 'sequence:d', 'For conv, at most one sequence can have d ~= 0.' );
            
            %shift non-finite sequences to d=0
            dl = 0; %d of left hand side
            for i = 1:nargin;
                if( varargin{i}.d ~= 0 );
                    dl = varargin{i}.d;
                    varargin{i}.c = varargin{i}.c - varargin{i}.d;
                    varargin{i}.d = 0;
                    varargin = [varargin(i) varargin];  %#ok<AGROW>
                    varargin(i+1) = [];
                    break; end; end;
            
            dimo = varargin{1}.ndimso;
            cl = varargin{1}.c;  % c of left hand side
            il = varargin{1}.idx;  % idx of left hand side
            for i = 2:nargin
                cr = varargin{i};  % c of right hand side
                dl = dl*sum( cr, 'all' );  % d of convolution
                cl = convm( cl, cr.c, 'dim', dimo );  % c of convolution
                il = il + cr.idx; end;  % index of convolution
            
            assert( numel(dl) == 1 || ~any(diff(dl(:))), 'sequence:d', 'Constant part d not a scalar after convolution. This is not yet implemented.' );
            oout = sequence( cl + dl, il, nan, dl );
            
        end

        function [ oout, mu ] = diff( oin, n, dim );
            % [oout, mu] = diff( oin, DIFF );
            % wrapper function for sequence.diffsequence whose interface is the same as matlab diff
            %
            % See also: sequence>diffsequence, diffsequence
            if( nargin == 2 );
                dim = 1; end;
            DIFF = zeros( 1, oin.ndimso );
            DIFF(dim) = n;
            [oout,mu] = oin.diffsequence( DIFF );
        end

        function [ oout, mu ] = diffsequence( oin, DIFF );
            % [oout, mu] = diffsequence( oin, DIFF );
            % computes finite differences of a sequence
            %
            % See also: diffsequence
            
            oin.c = oin.c - oin.d;
            oin.d = 0;
            [oout, mu] = diffsequence( oin.c, [DIFF(:); zeros(oin.ndimsi,1)] , 'cell', 'dim',oin.ndimso );
            for i = 1:length( oout ); 
                oout{i} = sequence( oout{i}, oin.idx, nan ); end;
            if( numel(oout) == 1 ); 
                oout = oout{1}; end;
        end
                
%% display
%%%%%%%%%%%
        function display( obj ); %#ok<DISPLAY>
            % display( obj )
            % displays a sequence
            disp( obj, inputname(1) );
        end

        function disp( obj, nm );
            % disps a sequence
            % make test concerning the sequence, this helps debugging things
            % since it is allowed to directly alter the values of .c .d and .idx, it is easy to generate not-valid sequences
            try;
                check( obj );
            catch me;
                fprintf( 2, 'Object does not seem to be a sequence. Thrown error: \n' );
                disp( me ); end;
            
            if( nargin == 1 );
                nm = inputname(1); end;
            if( obj.ndimsi == 0 );
                fprintf( [nm '.c = [\n'] ); 
                disp( obj.c ); 
                fprintf( '\b    ]\n' );
            else;
                fprintf( [nm '.c = (compact view) [\n'] ); 
                disp( compact(obj) );    
                fprintf( '\b    ]\n' );
                fprintf( ['tensor-size = [ ' num2str(obj.sizei) ' ]\n'] ); end;
            
            fprintf( [nm '.idx = [ ' num2str((obj.idx).') ' ]''\n'] );
            if( isequal(obj.d,0) );
                %do nothing
            elseif( isscalar(obj.d) );
                fprintf( [nm '.d = ['] );
                disp( obj.d ); 
                fprintf( '\b ]\n' );
            else
                fprintf( [nm '.d = (compact view) [\n'] );
                disp( shiftdim(obj.d,obj.ndimso) ); 
                fprintf( ']\n' ); end;
        end

        function plot( obj, varargin );
            plotm( obj, varargin{:} );
        end
        
    end
end


%% helper functions

function [ out ] = tif( truth, yes, no );
    % Ternary if operator
    % out = truth & yes : no ;
    %
    % Depends on: //
    %
    % E.g.: tif(true, 1, 2)
    if( truth );
        out = yes; 
    else; 
        out = no; end;
end

function expect( varargin );
    % expect(cond)
    % expect(cond,msg)
    % expect(cond,msg,A1,...,An)
    % expect(cond,errID,msg)
    % expect(cond,errID,msg,A1,...,An)
    %
    % equivalent to assert( ) which throws warnings instead of errors
    % 
    % See also: assert
    
    if( ~varargin{1} )
        if( nargin == 1 );
            warning( 'expect:failed', 'Expect failed.' );
        else
            warning( varargin{2:end} ); end; end;
end

function [ x ] = infsum( x );
    x(x > 0) = inf;
    x(x<0) = -inf;
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*SIZEM>  % sizem
%#ok<*CMEQU>  % equalize
%#ok<*NBRAK2>
 