function [ A, idxminnew, idxmaxnew ] = setidx( varargin );
% [ A, idxminnew, idxmaxnew ] = setidx( A, idxmin, idxminnew, [idxmaxnew], [value] );
% Changes the upper left index of an array by padding the array.
%
% Input:
%   A                   the array to be padded
%   idxmin              vector, the index of the first entry in A
%   idxminnew           vector, the new index of the first entry in Anew. idxminnew<=idxmin
%   idxmaxnew           vector, optional, the new index of the last entry in Anew. idxmaxnew>="idxmaxold"
%   value               double, default=0, optional, the fill value
%
% Output:
%   A                   The padded array
%   idxminnew           Equals always idxmin
%   idxmaxnew           The idx of the last entry in A
% 
% Note: Trailing singleton dimensions are ignored. 
%       idxminnew and idxmaxnew must have the same number of elements
% 
% E.g.: setidx( [1 2; 2 3], [1 1]', [0 0]', [4 3]' )
%
% See also: equalizeminidx, padarraym
%
% Written by: tommsch, 2017

%            2020-06-05, tommsch, Index vectors are allowed to be row-vectors
%            2020-09-28, tommsch, Added support for cell arrays
% Changelog: 

    A = varargin{1};
    idxmin = varargin{2};
    idxminnew = varargin{3};
    if( nargin <= 4 || isempty(varargin{5}) );
        if( iscell(A) );
            value = anycast( 0, 'like', A{1} );
        else;
            value = anycast( 0, 'like', A ); end;
    else;
        value = varargin{5}; end;
    dim = max( numel( idxmin ), numel(idxminnew) );
    idxmin(end+1:dim) = 1;
    idxminnew(end+1:dim) = 1;
    if( isrow(idxmin) );
        idxmin = idxmin.'; end;
    if( isrow(idxminnew) );
        idxminnew = idxminnew.'; end;
    padmin = idxmin - idxminnew;
    assert( all(padmin  >= 0), 'setidx:padding', 'idxminnew>idxmin' );
    %assert( ndimsm(A) <= dim, 'setidx:dim', 'Dimensions of array and idx not compatible.' );
    assert( isequal(numel(idxmin),numel(idxminnew)), 'setidx:dim', 'Index vector have different sizes.' );
    
    A = padarraym( A, [padmin; 0], value, 'pre' ); %can handle cells and symbolic stuff
    
    if( size(varargin,2) >= 4 && ~isempty(varargin{4}) );
        idxmaxnew = varargin{4};
        idxmax = sizem( A, 1:dim ).' + idxminnew - 1;
        idxmax(end+1:dim) = 1;
        idxmaxnew(end+1:dim) = 1;
        if( isrow(idxmaxnew) );
            idxmaxnew = idxmaxnew.'; end;    
        if( isrow(idxmax) );
            idxmax = idxmax.'; end;    
        padmax = idxmaxnew - idxmax;
        assert( all(padmax  >= 0), 'setidx:padding', 'idxmax>idxmaxnew');
        A = padarraym( A, [padmax; 0], value, 'post' );
    else;
        idxmaxnew = idxminnew + sizem( A ) - 1; end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*SIZEM>
