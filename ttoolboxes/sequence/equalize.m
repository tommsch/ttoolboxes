function [ imin, imax] = equalize( varargin );
% [ imin, imax ] = equalize( imin1, imax1, ..., iminn, imaxn, ['square' | 'D',D ] );
% Returns minimum and maximum indices of a given set of matrices
%
% Input:
%   iminx           index of first entry
%   imaxx           index of last entry
%
% Options:
%   'square'        Returns hyper-squares of same size. When 'square' is given, D must not be given
%   'D',D / D       J x N array, J<=dim, defines which indices j of which arrays An/indices i___n are to be equalized. When D is given, 'square' must not be given
%                   The argument 'D' can be ommitted
%
% Output:
%   imin          1xN cell array of index vectors, ew index of the first elements in C{i}
%   imax          1xN cell array of index vector, new index of the last elements in C{i}
%
% See also: setidx, sequence
%
% E.g.: [imin,imax] = equalizeidx( [0 0],[1 2], [-1 0], [0 2] );
%
% Written by: tommsch, 2020

% Changelog: 



%parse input and sanity checks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if( numel(varargin) == 0 );
    imin = {};
    imax = {};
    return; end;

if( isequal(varargin{end},'square') );
    sqflag = 1;
    varargin(end) = [];
else;
    sqflag = 0; end;
if ( numel(varargin) >= 2 && isequal(varargin{end-1},'D') );
    D = varargin{end};
    varargin(end-1:end) = [];
elseif( mod(numel(varargin),2) );
    D = varargin{end};
    varargin(end) = [];
else;
    D = 0; end;

if( numel(varargin) == 0 );
    imin = {};
    imax = {};
    return; end;

assert( ~mod(numel(varargin),2), 'An even number of indices must given.' );

if( isempty(D) );
    imin = varargin(1:2:end);
    imax = varargin(2:2:end);
    return; end;

%preprocessing
%%%%%%%%%%%%%%%%%
for i = 1:numel( varargin ); %make indices to column vector
    if( ~iscolumn(varargin{i}) );
        varargin{i} = varargin{i}.'; end; end; 

%main part
%%%%%%%%%%%%%%

if( isequal(D,0) );
    dim = cellfun( 'prodofsize', varargin ); %get dimension
    assert( all(dim == dim(1)) , 'equalizeminidx:idx', 'All indices must have the same length.' ); %test length of index vectors
    MIN = min( [varargin{1:2:end}], [], 2 ); %compute minimum
    MAX = max( [varargin{2:2:end}], [], 2 ); %compute maximum
    if( sqflag ); 
        MIN = min( MIN )*ones( dim(1), 1 ); %set all min/max indices to the min/max value, resp.
        MAX = max( MAX )*ones( dim(1), 1 ); end;    
    [imin,imax] = deal( cell(1,numel(varargin)/2) ); %make output variables
    [imin{:}] = deal( MIN );
    [imax{:}] = deal( MAX );
else;
    nD = size( D, 1 );
    nv = numel(varargin)/2;
    assert( isequal(size(D,2),nv), 'equalizeminidx:D', '''D'' must have number of columns equal to number of pairs ''iminx'',''imaxx''.' );
    imin = varargin(1:2:end); %all minimum/maximum indices
    imax = varargin(2:2:end);   
    for i = 1:nD;
        m = [];
        M = [];
        for j = 1:nv
            m = min( [m imin{j}(D(i,j))] );
            M = max( [M imax{j}(D(i,j))] ); end;
        for j = 1:nv
            imin{j}(D(i,j)) = m;
            imax{j}(D(i,j)) = M; end; end; end;
    
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
