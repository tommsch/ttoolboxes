function [ C, indmin, indmax] = equalizeminidx( varargin );
% [ C, indmin, indmax ] = equalizeminidx( {A1, idxmin1; A2, idxmin2; ...; An, idxminn} );
% [ C, indmin, indmax ] = equalizeminidx( A1, idxmin1, A2, idxmin2, ..., An, idxminn );
% [ C, indmin, indmax ] = equalizeminidx( ___, ['square' | D] );
% Makes a set of matrices the same size
%
% Input:
%   { }             N x 2 cell array
%                   first column: matrices Ai, 
%                   second column: idxmini, index of first entry of matrix
%                   Indices must be given as vectors
%                   If D is not given, all of their sizes must be equal
%               OR
%   An              matrices Ai
%   idxminn         vectors, index of first entry of matrix An
%
% Options:
%   'square'        Returns hyper-squares of same size, When given, D must not be given
%   D               J x N array, J<=dim, defines which indices j of which arrays An are to be equalized, When given, 'square' must not be given
%   
%
% Output:
%   C               Zero padded array such that index of first entry is for all matrices the same and such that their size is equal up to the dimension numel(idxmini)
%   indmin          new index of the first elements in C{i}
%   indmax          new index of the last elements in C{i}
%
% See also: setidx, sequence
%
% E.g.: A = equalizeminidx( {[1 1 2; 1 2 3],[1 1 0]';[1],[0 0 1]'} ); vdisp( A )
%       A = equalizeminidx( [1 1 2; 1 2 3], [1], [1], [-1]' ); vdisp(A)
%
% Written by: tommsch, 2018

%            2020-06-05, tommsch, Row vectors allowed for index vector
%                                 new input format
% Changelog: 


warning( 'equalizemindidx:deprecated', 'This function yields wrong results and should not be used.' );

%parse input
%%%%%%%%%%%%%
if ( mod(nargin,2) && 2*size(varargin{end},2) == nargin-1 );
    D = varargin{end};
    varargin(end) = [];
else
    D = []; end;

if( isequal(varargin{end},'square') );
    sqflag = 1;
    varargin(end) = [];
else;
    sqflag = 0; end;


%find dimension
if( iscell(varargin{1}) );
    A = varargin{1}(:,1); 
    ind = varargin{1}(:,2);
else;
    varargin = reshape( varargin, [], 2 ).'; 
    if( iscolumn(varargin) );
        varargin = varargin.'; end;
    A = varargin(:,1);
    ind = varargin(:,2); end;    

nA = size( A, 1 ); %number of matrices
dim = numel( ind{1} );
for i = 1:nA
    if( ~iscolumn(ind{i}) );
        ind{i} = ind{i}.'; end; end; %make indices to column vector

%find minimal minidx
if( isempty(D) );
    val = cellfun(@numel,ind(:));
    assert( all(val == val(1)) , 'equalizeminidx:idx', 'All indices must have the same length.' );    
    
    MIN = ind{1};
    MAX = ind{1} + sizem( A{1}, 1:dim ).';
    for i = 2:nA;    
        MIN = min( MIN, ind{i} );
        MAX = max( MAX, ind{i} + sizem( A{i}, 1:dim ).' ); end;
    MAX = MAX - 1;
    if( sqflag )
        MIN = min(MIN)*ones(dim,1);
        MAX = max(MAX)*ones(dim,1); end;    
else
    nD = size( D, 1 );
    [MIN,MAX] = deal( nD, 1 );
    for i = 1:nD;
        [m,M] = deal( zeros(1,nA) );
        for j = 1:nA
            m(j) = ind{j}(D(i,j));  % temporary variable
            M(j) = ind{j}(D(i,j)) + sizem( A{j}, D(i,j) ); end;  % temporary variable
        MIN(i) = min(m);
        MAX(i) = max(M); end;
    MAX = MAX - 1; end;



C = cell( nA, 1 );  % make output variable
if( isempty(D) );
    for i = 1:nA;
        C{i} = setidx( A{i}, ind{i}, MIN, MAX ); end;
    indmin = MIN;
    indmax = MAX;
else;
    [indmin,indmax] = deal( cell(nA,1) );
    for i = 1:nA;
        mD = arrayfun( @(x) find(D(:,i) == x), 1:numel(ind{i}), 'UniformOutput', false );
        indmin{i} = arrayfun( @(x) tif( isempty(mD{x}), ind{i}(x), MIN(mD{x}) ), 1:numel(ind{i}) );
        indmax{i} = arrayfun( @(x) tif( isempty(mD{x}), ind{i}(x) + sizem(A{i},x)-1, MAX(mD{x}) ), 1:numel(ind{i}) );
        C{i} = setidx( A{i}, ind{i}, indmin{i}, indmax{i} ); end; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*SIZEM>
