function [ ret ] = nestedcellfun_old( varargin );
% [ret] = nestedcellfun_old(func, C1, C2, ..., [options] ) applies the function func(x1,x2,...) to the contents of each cell of cell array C1,C2,... .
% Wrapper function calling cellfun for each cell in a nested array
%
% Input:
%   func                    function handle
%   C1,C2,                  arrays, cell arrays or nested cell arrays, All of the Ci must have the same topology.
%
% Options:
%   'UniformOutput',false   Then ret has the same topology as C1, otherwise (default) it is a matrix. Thus the default behaviour is the same as for cellfun.
%
% Output:
%   ret                     Either a scalar or something with the same topology as C1
%                           If 'UniformOutput' is <false> and no cell array is given, the output is not a cell array
% Note:
%   Error handlers cannot be passed to nestedcellfun
%   
% Eg: nestedcellfun(@(x) ndimsm(x),{{[1 2 1]'},[1 2; 2 3]},'UniformOutput',false)
%
% See also: flatten, unflatten
%
% Written by: tommsch, 2017

%            tommsch,   2019-03-04,     Now also handles multiple arguments which are not cell arrays
%            tommsch,   2020-09-10,     partially rewritten
% Changelog: 

    
    %parse input
    if( numel(varargin) >= 4 && numel(varargin{end-1}) >= 3 && isequal(lower(varargin{end-1}(1:3)),'uni') );
        UniformOutput = varargin{end};
        varargin(end-1:end) = [];
    else;
        UniformOutput = false; end;
    handle = varargin{1}; 
    varargin(1) = []; %the function handle
    
    C = {};
    cellflag = 0;
    Cf = cell( 1, numel(varargin) );
    for j = 1:numel( varargin );
        C{j} = varargin{j}; %#ok<AGROW>
        if( ~iscell(C{j}) );
            C{j} = C(j);  %#ok<AGROW>
        else;
            cellflag = 1; end;
        Cf{j} = flatten( C{j} ); end;
    
    
    if( isequal(UniformOutput, 0) || strcmp(UniformOutput, 'false') );
        ret = cellfun( handle, Cf{:}, 'UniformOutput',false );
        ret = unflatten( ret, C{1} ); %all C's have the same topology, thus I can use C{1}
    else;
        ret = cellfun( handle, Cf{:} ); end;
    
    if( ~cellflag && ~UniformOutput );
        ret = ret{1}; end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
