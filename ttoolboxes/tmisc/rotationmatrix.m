function [ R ] = rotationmatrix( varargin );
% [ R ] = rotationmatrix( varargin );
% generates a rotation matrix, number of input angles determines dimension
%   1 input:  2d
%   3 inputs: 3d
%   other dimensions not yet implemented
%
% Experimental:
%   rotationmatrix( 'rand',dim )    generates a random rotation matrix. The results are not uniformly distributed.
%
% Output:
%   R   rotation matrix
%
% Written by, tommsch, 2022

    if( ischar(varargin{1}) || isstring(varargin{1}) );
        if( strcmpi(varargin{1}, 'rand') || ...
            strcmpi(varargin{1}, 'random') || ...
            strcmpi(varargin{1}, 'dim') ...
          );
            switch varargin{2};;
                case 2; n_angles = 1;
                case 3; n_angles = 3;
                otherwise; error( 'rotationmatrix:input', 'Only 2d and 3d rotation matrices are possible.' ); end;
            angles = num2cell( rand( 1, n_angles )*2*pi );
            R = rotationmatrix( angles{:} );
        else;
            error( 'rotationmatrix:input', 'Wrong input. If a string is given, it must be ''rand'', followed by the dimension.' ); end;
    else;
        if( numel(varargin) == 1 && isscalar(varargin{1}) );
            R = rotationmatrix_2d( varargin{1} );
        elseif( numel(varargin) == 1 && numel(varargin{1}) == 3 || ...
                numel(varargin) == 3 ...
              );
            if( numel(varargin) == 1 );
                al = varargin{1}(1);
                be = varargin{1}(2);
                ga = varargin{1}(3);
            else;
                al = varargin{1};
                be = varargin{2};
                ga = varargin{3}; end;
            R = rotationmatrix_3d( al, be, ga );

        else;
            error( 'rotationmatrix:nargin', 'Wrong number of inputs given. For 2D matrix: 1 input, For 3D matrix: 3 inputs' ); end; end;
end

function [ R ] = rotationmatrix_2d( al );
    R = [cos(al) -sin(al); sin(al) cos(al)];
end

function [ R ] = rotationmatrix_3d( al, be, ga );
    ca = cos( al ); sa = sin( al );
    cb = cos( be ); sb = sin( be );
    cg = cos( ga ); sg = sin( ga );
    R = [ca*cg-sa*cb*sg -ca*sg-sa*cb*cg  sa*sb
         sa*cg+ca*cb*sg -sa*sg+ca*cb*cg -ca*sb
         sb*sg          sb*cg            cb   ];
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
