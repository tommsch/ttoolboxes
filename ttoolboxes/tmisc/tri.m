function [ C ] = tri( C, varargin );
% [ D ] = tri( C, [bw] )
% [ D ] = tri( C, lo, hi )
% Returns diagonals of matrices
%
% Sets all matrices not contained in [lo hi] to zero.
% If lo > hi, 
%
% See also: triu, tril
%
% Written by tommsch, 2024-10-31

% Changelog:    tommsch,    2024-10-31,     New function

    if( numel( varargin ) == 0 ); 
        k = [0 0];
    elseif( numel( varargin ) == 1 );
        k = varargin{1};
        assert( numel(k) == 2 );
    elseif( numel( varargin ) == 2 );
        k = [varargin{1:2}];
    else;
        error( 'tri:input', 'Wrong input given.' ); end;
    assert( k(1) <= k(2), 'tri:input', 'Wrong input given.' );
    
    C = triu( tril( C, k(2) ), k(1) );

end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
