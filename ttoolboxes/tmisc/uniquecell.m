function [ C, ia ,ic ] = uniquecell( A, varargin );
% [ Au, ia, ic] = uniquecell( A, [options])
% Similar behaviour as unique
% For cells of double arrays uses num2str, and thus is fast
% Otherwise compares all elements, and thus is slow
%
% E.g.: uniquecell( {1 2 1 [1 2] [1 3] [1 2]} )
%       uniquecell( {1 2 1 [1 2] [1 3] [1 2]; 10 20 10 [1 2] [1 3] [10 20]} )
%       uniquecell( {@sum @sum @plus} )
%
% See also: unique
%
% Interface written by: tommsch, 2018
% General uniquecell function written by: Konrad Schumacher, 2018-08

%               tommsch, 2024-10-08,   Merged this function with uniquecell2
% Changelog:
    
    classA = cellfun( 'isclass', A, 'double' );
    if( all(classA) );
        [C, ia, ic] = uniquecell_numeric( A, varargin{:} );
    else;
        [C, ia, ic] = uniquecell_general( A, varargin{:} ); end;
end

function [ C, ia ,ic ] = uniquecell_numeric( A, varargin );

    B = cellfun( @(x) num2str(x(:)'), A, 'UniformOutput',false );
    if( nargout > 2 );
        [~, ia, ic] = unique( B, varargin{:} );
        C = A(ia);
    else;
        [~, ia] = unique( B, varargin{:} );
        C = A(ia); end;
end

function [ C, ia, ic ] = uniquecell_general( A, varargin )
% [ outcell, ia, ic ] = uniquecell_general( incell, ['rows'], ['equalnan'], ['stable'] )
% Implements unique for cells.
% 
% Usage is similar to unique(). ND arrays of cells are supported.
% The 'rows' switch returns unique rows in a matrix of cells.
% The 'equalnan' switch treats NaNs as equal.
% 
% As sorting is not possible for mixed-type cells the output corresponds to the 'stable' switch in unique(). 
% The occurrence-switch of unique() is NOT implemented.
% 
% The following is true (w/o the 'rows' switch'):
% isequal(A(ia), C)
% isequal(C(ic), A)
% 
% Using the 'rows' switch:
% isequal(A(ia,:), C)
% isequal(C(ic,:), A)
% 
% The algorithm performs exhaustive pairwise comparisons  using isequal making it exact and type-independent 
% (in contrast to implementations using num2str); however this may come at the expense of performance.
% 
% See also: uniquecell, unique
% 
% V 1.2; 08.2018; Konrad Schumacher

    flags = {'equalnan','rows','stable'};
    assert( iscell(A), 'MATLAB:uniqueCells:InputNotACell', 'First input has to be a cell array.' );
    % defaults
    opt.iseq = @isequal;
    opt.rows = false;
    % input flags
    for i = 1:numel( varargin );
        f = validatestring( varargin{i}, flags, mfilename, 'switch',i+1 );
        fi = strcmpi( f, flags );
        switch find(fi);
            case 1; opt.iseq = @isequaln;
            case 2; opt.rows = true;
            case 3; % stable is always true. We check it, because it is a valid parameter for `uniquecell_numeric`
            otherwise; error( 'mpowerinf:fatal', 'Programming error.' ); end; end;

    % place each row in one cell to implement 'rows'-switch
    if( opt.rows );
        assert( numel(size(A)) == 2, 'MATLAB:uniqueCells:NDarraysNotSupported', 'ND arrays not supported for the ''rows'' switch.' );
        [r, c] = size( A );
        A = mat2cell( A, ones(1, r), c ); end;
    % init
    C = {};
    ia = [];
    ic = zeros( size(A) );
    % compare all cells
    n = numel( A );
    for c1 = 1:n;
        eqc = false( c1-1, 1 );
        for c2 = 1:c1-1;
            eqc(c2) = opt.iseq( A(c1), A(c2) ); end;

        if( ~any(eqc) );
            C(end+1) = A(c1);  %#ok<AGROW>
            ia(end+1) = c1;  %#ok<AGROW>
            ic(c1) = numel( C );
        else;
            ic(c1) = find(find(eqc,1,'first') == ia); end; end;

    
    if( opt.rows );  % convert row-cells back to single cells
        C = vertcat(C{:});
        ia = ia(:);
    elseif( ~isrow(A) )  % otherwise output column if input wasn't a row vector
        ia = ia(:);
        C = C(:); end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
