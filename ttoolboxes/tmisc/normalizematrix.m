function [ Tnorm ] = normalizematrix( varargin );
% [ Tnorm ] = normalizematrix( T, [how], [options] )
% Normalizes matrices in various ways.
% Input:    T           (matrix, cell array or nested cell array of matrices) the matrices to be normalized
%           how         string which tells how the matrices shall be normalized. Default: 'colnorm',2
%                       'colsum'            All columns sum up to 1. Columns which sum up to zero, are unchanged.
%                       'rowsum'            All rows sum up to 1. --"--
%                       'dirsum',dir        All lines in direction dir sum up to 1. --"--
%       
%                       'colmax'            Biggest value in each column in absolute value is set to 1. zero columns are unchanged.
%                       'rowmax'            Biggest value in each row in absolute value is set to 1. --"--
%                       'dirmax',dir        Biggest value in each line in direction dir in absolute value is set to 1. --"--
%       
%                       'colnorm',p         p-norm of each column is set to 1. zero columns are unchanged. p is a number or a function handle
%                       'rownorm',p         p-norm of each row is set to 1. --"-- . --"--
%                       'dirnorm',[dir p]   p-norm of each line in direction dir in absolute value is set to 1. --"-- . --"-- .
%       
%                       'rho'               Matrix is scaled such that spectral radius == 1.
%                       'norm',p            Matrix is scaled such that p-norm == 1. p is a number or a function handle
%                       'binary'            All nonzero entries are set to one
%                       'dotprod',v         (experimental) Matrix is normalized such that dot(M,v) == 1, M and v must be vectors. If dot(M,v) == 0, then M stays unchanged.
%                       'positive',dir      'makepositive' is applied to each line in direction dir
%
% Output:varargin{1} 
%   Tnorm               the normalized matrix
% 
% Depend on: nestedcellfun, m-toolbox
%
% E.g.: normalizematrix( randn(4), 'colsum' )
%       normalizematrix( {randn(2), randn(2)}, 'rho') )
%       normalizematrix( randn(3, 2), 'dirnorm', [1, inf] )
%
% See also: makepositive
%
% Written by: tommsch, 2018

%               2020-09-01, tommsch,    Removed use of nestedcellfun (due to performance reasons)
%               2024-06-24, tommsch,    Default option is now 'colnorm',2
%                                       Nestedcellfun is still used, for some unknown reason
% Changelog:


 %#ok<*ALIGN>

    T = varargin{1};
    if( isempty( T ) );
        Tnorm = T; 
        return; end;
    
    if( numel(varargin) == 1 );
        varargin = [varargin 'colnorm' 2]; end;
    how = varargin{2};
    
    switch how;
        %case is in ordering from most often used to seldom used
        case {'dotprod','dot'};
            Tnorm = nestedcellfun( @(x,y) normalizematrix_dotprod(x, y),               T, varargin{3}, 'UniformOutput',false );     
        case {'positive','pos'};
            Tnorm = nestedcellfun( @(x) normalizematrix_positive(x, varargin{3}),      T, 'UniformOutput',false );
        case 'colnorm';
            Tnorm = nestedcellfun( @(x) normalizematrix_dirnorm(x, [1 varargin{3}]),   T, 'UniformOutput',false );
        case 'rownorm';
            Tnorm = nestedcellfun( @(x) normalizematrix_dirnorm(x, [2 varargin{3}]),   T, 'UniformOutput',false ); 
        case 'dirnorm';
            Tnorm = nestedcellfun( @(x) normalizematrix_dirnorm(x, varargin{3}),       T, 'UniformOutput',false );         
            
        case 'colsum';
            Tnorm = nestedcellfun( @(x) normalizematrix_dirsum(x, 1),                  T, 'UniformOutput',false );
        case 'rowsum';
            Tnorm = nestedcellfun( @(x) normalizematrix_dirsum(x, 2),                  T, 'UniformOutput',false );
        case 'dirsum';
            Tnorm = nestedcellfun( @(x) normalizematrix_dirsum(x, varargin{3}),        T, 'UniformOutput',false );
        case 'colmax';
            Tnorm = nestedcellfun( @(x) normalizematrix_dirnorm(x, [1 inf]),           T, 'UniformOutput',false );
        case 'rowmax';
            Tnorm = nestedcellfun( @(x) normalizematrix_dirnorm(x, [2 inf]),           T, 'UniformOutput',false );
        case 'dirmax';
            Tnorm = nestedcellfun( @(x) normalizematrix_dirnorm(x, [varargin{3} inf]), T, 'UniformOutput',false );
        
        case 'norm';
            Tnorm = nestedcellfun( @(x) normalizematrix_norm(x, varargin{3}),          T, 'UniformOutput',false ); 
        case 'rho';
            Tnorm = nestedcellfun( @(x) normalizematrix_rho(x),                        T, 'UniformOutput',false );     
        case {'binary','bin'};
            Tnorm = nestedcellfun( @(x) normalizematrix_binary(x),                     T, 'UniformOutput',false );       
    
        otherwise;
            error( 'normalizematrix:opt', 'Unkown option' ); end;

end

function [ T ] = normalizematrix_dirsum( T, dir );
    dim = ndimsm( T );
    dirsum = sum(T, dir);
    dirsum(dirsum == 0) = 1;
    REPEAT = ones( 1, dim );
    REPEAT(dir) = size( T, dir );
    dirsum = repmat( dirsum, REPEAT );
    T = T./dirsum;
end

function [ T ] = normalizematrix_dirnorm( T, arg );
    dir = arg(1);
    normval = arg(2);
    if( isa(normval,'function_handle') );
        normhandle = normval;
    else;
        normhandle = @(x) norm(x,normval); end;
    dim = ndims( T ); 
    if( iscolumn( T ) );
        dim = 1; end;
    val = cellfun( normhandle, num2cell(T,dir) );
    val(val == 0) = 1;
    REPEAT = ones( 1, dim );
    REPEAT(dir) = size( T, dir );
    val = repmat( val, [REPEAT 1] );
    T = T./val;
end

function [ T ] = normalizematrix_norm( T, normval );
    if( isa(normval, 'function_handle') );
        T = T/normval( T );
    else;
        T = T/norm( T, normval ); end;
end

function [ T ] = normalizematrix_rho( T );
    T = T/rhot( T );
end

function [ T ] = normalizematrix_binary( T );
    T = double( logical( T ) );
end

function [ T ] = normalizematrix_dotprod( T, v );
    if( iscell(v) );
        v = v{1}; end;
    val = dot( v, T );
    val = simplify( val );
    T = T/val;
end

function [ T ] = normalizematrix_positive( T, dir );
    val = num2cell( T, dir );
    for i = 1:numel( val );
        val{i} = makepositive( val{i} ); end;
    if( issym(val{1}) );
        T = cell2sym( val );
    else;
        T = cell2mat( val ); end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>'end' of a function is missing.
