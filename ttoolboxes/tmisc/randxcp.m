function [ A ] = randxcp( varargin );

A = randx( varargin{:} ) + 1i*randx( varargin{:} );

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
