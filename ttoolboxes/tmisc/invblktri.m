function [ B ] = invblktri( A );
% (experimental) Finds square blocks of a square matrix on the diagonal
% [ ret ] = invblktri( A )
%
% Input
%   A   square matrix
%
% Output:
%   B   cell array of square matrices
%
% Note:
%   This function dismesses the upper right block part of the matrix. Thus it is not an inverse of blktri
%
% See also: blktri, blkdiag, invblkdiag, invariantsubspace
%
% Written by: tommsch, 2024-11-03

% Changelog:    tommsch,    2024-11-03,     New function

    [A, cellflag] = tocell( A );
    B = cell( size(A) );
    for j = 1:numel( A );
        dim = size( A{j}, 1 );
        assert( dim == size(A{j}, 2), 'invblkdiag:square', 'Input must be a square matrix.' );
        if( dim == 0 ); 
            B{j} = {};
            continue; end;
        c = zeros( 1, dim );
        r = zeros( 1, dim );
        idxAj = isAlways( A{j} ~= 0, 'Unknown','true' );  % speed up for symbolic stuff
        for i = 1:size( A{j}, 1 )
            c(i) = last( find( idxAj(:,i), 1, 'last' ) );
            r(i) = last( find( idxAj(i,:), 1 ) ); end;
        c = cummax( c );
        r = fliplr( cummin( fliplr(r) ) );
        c = (c <= 1:dim);
        r = (r <= 1:dim);
        cr = c & r;
        idx = find( cr, 1 );
        rhs = invblktri( A{j}(idx+1:end,idx+1:end) );
        lhs = A{j}(1:idx,1:idx);
        B{j} = [{lhs} rhs]; end;  % This construct is necessary to handle symbolic stuff correctly too
    
    if( ~cellflag );
        B = B{1};
        end;
end

function [ x ] = last( x );
    if( isempty(x) );
        x = 0; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
