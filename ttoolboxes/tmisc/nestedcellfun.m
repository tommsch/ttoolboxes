function [ ret ] = nestedcellfun( varargin )
% [ret] = nestedcellfun(func, C1, C2, ..., [options] ) 
% faster version of cellfun, applicable to nested cell arrays as well
% applies the function func(c1,c2,...) to the contents of each cell of cell array C1,C2,... .
%
% Input:
%   func                    function handle
%   C1,C2,...               arrays, cell arrays or nested cell arrays, All of the Ci must have the same topology.
%
% Options:
%   'UniformOutput',val     default=true, If false, then ret has the same topology as C1, otherwise (default) it is a matrix.
%
% Output:
%   ret                     Either a scalar or something with the same topology as C1
%                           If 'UniformOutput' is <false> and no cell array is given, the output is not a cell array
% Note:
%   Error handlers cannot be passed to nestedcellfun
%   Usage of nullary functions is experimental
%   
% Eg: nestedcellfun( @ndimsm, {{[1 2 1]'},[1 2; 2 3]}, 'UniformOutput',false )
%     nestedcellfun( @sum, {{1 2},3} ,{{4 5},6}, 'UniformOutput',false )
%
% Depend on: //
%
% See also: flatten, unflatten
%
% Written by: tommsch, 2017

%            tommsch,   2019-03-04,     Now also handles multiple arguments which are not cell arrays
%            tommsch,   2020-09-10,     totally rewritten and now faster than Matlab's cellfun
% Changelog: 

    % parse input
    %%%%%%%%%%%%
    if( numel( varargin ) >= 3 && ...
        numel( varargin{end-1} ) >= 3 && ...
        ~iscell( varargin{end-1} ) && ...
        isequal( lower(varargin{end-1}(1:3) ), 'uni' ) ...
      );
        uni = varargin{end};
        varargin(end-1:end) = [];
    else;
        uni = true; end;
    if( isequal(uni(1),'f') );
        uni = 0; end;
    handle = varargin{1}; 
    varargin(1) = [];  % the function handle


    % main part
    %%%%%%%%%%%%%
    if( isempty( varargin ) || ...
        ~iscell( varargin{1} ) ...
      ); 
        %case when there is no cell array
        ret = handle( varargin{:} ); 
        return;
    elseif( ~anym( cellfun('isclass', varargin{1}, 'cell') ) && ...
            numel( varargin ) == 1 ...
          );
        % case when there is a plain cell array and only one argument
        val = varargin{1};
        if( isequal(uni,0) );
            ret = val;
            for i = 1:numel( val );
                ret{i} = handle( val{i} ); end;
            return;
        else;
            ret = zeros( 1, numel(val) );
            for i = 1:numel( val );
                ret(i) = handle( val{i} ); end; end;
    else;
        % general case
        J = numel( varargin );
        val = cell( 1, J );
        if( isequal(uni, 0) );
            ret = cell( size(varargin{1,1}) );
            for i = 1:numel( varargin{1} );
                for j = 1:J
                    val{j} = varargin{j}{i}; end;
                if( iscell(val{1}) );
                    ret{i} = nestedcellfun( handle, val{:}, 'uni',0 );
                else
                    ret{i} = handle( val{:} ); end; end;
        else;
            ret = [];
            for i = 1:numel( varargin{1} );
                for j = 1:J
                    val{j} = varargin{j}{i}; end;
                if( iscell(val{1}) );
                    ret = [ret nestedcellfun( handle, val{:} )];  %#ok<AGROW>
                else;
                    ret(end+1) = handle( val{:} ); end; end; end; end;  %#ok<AGROW>



end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
