function [ J ] = jordant( A, realflag );

    if( nargin <= 1 );
        realflag = false; end;
    
    J = jordan( A );
    if( realflag );
        J = invblkdiag( J ); 
        for i = numel( J ):-1:2;
            if( norm(J{i-1} - J{i}')<1e-15 );
                J(i) = [];
                J(i-1) = comp2real( J(i-1) ); end; end; 
        J = blkdiag( J{:} ); end;

end

function [ Mreal ] = comp2real( M );
    %
    % MREAL = COMP2REAL(M)
    %
    % Transforms a set of square nxn matrices into
    % a set of (2*n)x(2*n) real matrices.
    % 
    % MREAL{i} = [A -B;B A];
    %
    % with A = real(M{i})
    %      B = imag(M{i})
    %
    % Taken from: JSR toolbox, (c) UCL Louvain

    m = length( M );
    Mreal = cell( 1, m );

    for imat = 1:m
        A = real( M{imat} );
        B = imag( M{imat} );
        Mreal{imat} = [A -B;B A]; end;
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
