function [ c ] = condeigvec( A, p );
% Computes the condition number of the matrix of the eigenvectors
% [ c ] = condeigvec( A, p );
%
% Written by: tommsch, 2024-03

    if( nargin == 1 );
        p = 2; end;
    [v, ~] = eig( A );
    c = cond( v, p );
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
