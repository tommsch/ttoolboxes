function [ nfo ] = matrixinfo( varargin )
% [ nfo ] = matrixinfo( M, ['what1', ..., 'whatn'], [options] )
% [ nfo ] = matrixinfo( [M], nfo, ['what1', ..., 'whatn'], [options] )
% (experimental) [ nfo ] = matrixinfo( nfo1, ..., nfon, [options] );
% Returns a lot of properties of matrices.
%
% Input: 
%   M               matrix, or cell array of matrices. Works only for non-sparse matrices
%                   For symbolic matrices this function may not work, which is considered to be a bug
%   nfo             a struct as outputed by this function. Already present entries will not bet recomputed, and thus speeding up this function
%   whati           string, determines which properties are returned.
%                   If whati is not given, most properties are returned
%   nfoi            If multiple nfo's are given, all fields which are equals are removed from all nfoi
%
% Options:
%   'compact'       If given, all fields whose values are either empty, all false, or all zero, are removed from the output struct
%   'all'           If given, then all implemented properties are computed. This may be slow
%   'verbose',val   Verbosity level
%   'epsilon'       (optional), default 
%   
% Output: 
%   If only one argument 'whati' is given, this value is returned,
%   If no or multiple arguments are given, a struct is returned with fieldnames equal to the name of whati
%
% Note: 
%   The returned struct `prop` will contain more fieldnames than queried in general
%   Which fields are returned exactly may change from release to release
%
% Possible names for 'whati':
%               'acyclic'               checks which matrices are acyclic, where the matrices are interpreted as the adjacancy matrix of a directed graph
%               'adjacency'             (experimental) the matrices are interpreted as the adjacancy matrix of a directed graph
%               'altsign'               checks which matrices are alternating sign matrices
%               'arrowhead'             checks which matrices have an arrowhead form
%               'bool'                  checks which matrices are boolean
%               'cauchy'                checks which matrices are cauchy, i.e. are of the form a_ij = 1/(xi-yj)
%               'charpoly'              computes the characteristic polynomial of the matrices
%               'circulant'             checks which matrices are circulant
%               'cmirror'               (experimental) checks which matrices are complex conjugates and or multiples of +-1 of each other
%               'commute'               checks if a matrix commutes with all others
%               'commuteall'            checks which matrices commute with each other
%               'comparison'            computes the comparison matrices
%               'cond'                  computes the 2-condition number of the matrices
%               'condeigvec'            computes the 2-condition number of the matrix of normalized eigenvectors
%               'conference'            (experimental) checks which matrices are conference, i.e. zero on the diagonal, all other entries are +-1, M*M' is a multiple of the identity
%               'conjugate'             (experimental) checks which matrices are complex conjugates of each other
%               'cycles'                (experimental) all cycles of the matrices, where the matrices are interpreted as the adjacancy matrix of a directed graph
%               'defective'             (experimental) checks which matrices are defective, i.e. do not have a full basis of eigenvectors 
%               'def'                   (experimental) checks which matrices are either positive oder negative definite
%               'det'                   the determinant
%               'diag'                  checks which matrices are diagonal
%               'diagdom'               (experimental) checks which matrices are weakly diagonal dominant, i.e. sum |a_ii| >= sum_i |a_ij| (i ~= j)  OR sum |a_ii| >= sum_j |a_ij| (i ~= j)
%               'eig'                   contains eigenvalues of the input matrices
%               'eigvec'                contains eigenvectors of the input matrices
%               'empty'                 checks which matrices have at least on dimension with size 0
%               'equal'                 checks which matrices are equal
%               'finite'                checks which matrices are finite (excluding inf and nan)
%               'frobenius'             (experimental) checks which matrices are frobenius, i.e. all entries on the main diagonal are ones, the entries below the main diagonal of at most one column are arbitrary, every other entry is zero
%               'dirac'                 (experimental) checks whether the matrices are a Dirac Matrix in dirac representation
%               'gellmann'              checks which matrices are one of the eight Gell-Mann matrices
%               'girth'                 (experimental) girth of the matrices, where the matrices are interpreted as the adjacancy matrix of a directed graph
%               'hamiltonian'           (experimental) checks which matrices are Hamiltonian, i.e. JA is symmetric where J = [0 I;-I 0]
%               'hankel'                checks which matrices are Hankel
%               'hermite'               computes the hermite form of the matrices
%               'hermitian'             checks which matrices are hermitian (M == M')
%               'hermitian_skew'        checks which matrices are skewhermitian (M == -M')
%               'hessl'                 checks which matrices are lower Hessenberg
%               'hessu'                 checks which matrices are upper Hessenberg
%               'householder'           checks which matrices are householder
%               'hurwitz'               checks which matrices are Hurwitz-stable, i.e. its eigenvalues have strictly negative part
%               'hyperbolic'            checks which matrices are hyperbolic, i.e. satisfy M * J * M' = J, where J = blkdiag( eye(ceil(n/2)), -eye(floor(n/2)) )
%               'identity'              checks which matrices are the identity, does not check whether the matrices are square!
%               'incidence'             (experimental) the incident matrix of the matrices, where the matrices are interpreted as the adjacancy matrix of a directed graph with self loops removed
%               'int'                   checks which matrices are integers (does check the value, not the type/class)
%               'involutory'            checks which matrices are involutory (M^2 == I)
%               'inv'                   checks which matrices are invertible
%               'jordan'                computes the Jordan form of the matrices
%               'kone'                  (experimental) checks whose matrices conic hulls column vectors are a pointed cone
%               'l'                     checks which matrices L-matrices
%               'laplacian'             checks which matrices could be the laplacian of a graph
%               'le'                    checks which matrices are less or equal than others
%               'leidentity'            checks which matrices are entrywise less or equal than the identity, does not check whether the matrices are square!
%               'lognorm'   
%               'lt'                    checks which matrices are less than others
%               'm'                     checks which matrices are m-matrices, i.e. z-matrices and whose eigenvalues have nonnegative real parts
%               'metzler'               checks which matrices are Metzler, i.e. all the off-diagonal components are nonnegative
%               'minors'                computes the all minors of the matrices
%               'minpoly'               computes the minimal polynomial of the matrices
%               'moore'                 (experimental) checks which matrices are Moore matrices
%               'n'                     number of matrices
%               'nan'                   checks which matrices contain nans
%               'ndims'                 the dimensions of the matrices (as given by ndimsm)
%                                       Also the field .dim_same is set, which indicates whether all matrices have the same dimension
%               'negdef'                checks which matrices are negative definite
%               'negsemidef'            checks which matrices are negative semi definite
%               'nilpotent'             (experimental) checks which matrices are nilpotent
%               'nonneg'                checks which matrices are nonnegative
%               'nonzero'               checks which matrices are fully nonzero
%               'norm'                   computes the 2-norm of all matrices
%               'normal'                checks which matrices are normal (M*M'=M'*M) (See Jungers, The JSR, Prop 2.2)
%               'numcomp'               Number of components of the matrix interpreted as the adjacancy matrix of a directed graph
%               'orthogonal'            checks which matrices are orthogonal, i.e. the columns are orthogonal to each other
%               'orthonormal'           checks which matrices are orthonormal, i.e. the columns are orthogonal to each other and have 2-norm 1
%               'pauli'                 checks which matrices are one of the four Pauli matrices
%               'permutation'           (experimental) checks which matrices are permutation matrices
%               'persymmetric'          checks which matrices are persymmetric, i.e. symmetric w.r.t to the anti-diagonal
%               'primitive'             Checks which matrices are primitive, i.e. there exists some natural number n such that M^n is elementwise positive
%               'pm1'                   checks which matrices are eiterh -1,0 or 1
%               'pos'                   checks which matrices are positive
%               'posdef'                checks which matrices are positive definite
%               'poslead'               checks which matrices have a positive leading eigenvalue
%               'possemidef'            checks which matrices are positive semi-definite
%               'projection'            checks which matrices are projection matrices, i.e. M*M == M
%               'rank'                  the rank of each matrix
%               'rat'                   checks which matrices have rational entries with denominator less than 
%               'real'                  checks which matrices are real
%               'rho'                   computes the spectral radius of all matrices
%               'rho_laplacian'         computes the spectral radius of the laplacian of the graph, where the matrices are interpreted as the adjacancy matrix of a directed graph
%               'semidef'               (experimental) checks which matrices are either positive oder negative semidefinite
%               'singular'              computes which matrices are singular
%               'size'                  the sizes of the matrices
%                                       Also sets the file .size_same, which indicates whether all matrices have the same size
%               'samesize'              checks if all matrices have the same size
%               'smith'                 computes the invariant factors of the matrix as given by the normal Smith form
%               'smpcandidate'          guessed joint-specral-radius s.m.p.s
%               'sparsity'              rel. sparsity of the matrices
%                                          Also sets the field .sparsity_all, which is the relative sparsity over all matrices
%               'square'                checks which matrices are square
%               'stieltjes'             checks which matrices are stieltjes, i.e. real, symmetric, posdef, m
%               'stochastic_column'     checks which matrices are column-stochastic
%               'stochastic_row'        checks which matrices are column-stochastic
%               'stochastic_double'     checks which matrices are column-stochastic
%               'sym'/'symbolic'        checks which matrices are symbolic
%               'symstrict'             checks which matrices are symbolic but not vpa
%               'symmetric'             checks which matrices are symmetric (M=M.')
%               'symmetric_skew'        checks which matrices are skeysymmetric (M=M.')
%               'ternary'               checks which matrices only have entries -1/0/1
%               'toeplitz'              checks which matrices are Toeplitz
%               'totallypos'            checks which matrices are totally positive, i.e. all of its minors have a positive determinant
%               'trace'                 the trace of each matrix
%               'traceless'             checks which matrices have trace 0
%               'tridiag'               checks which matrices are tridiagonal
%               'triangular'            checks which matrices are triangular
%               'tril'                  checks which matrices are lower triangular
%               'triu'                  checks which matrices are upper triangular
%               'unimodul'              checks which matrices are unimodular, i.e. have determinant = +1
%               'unipotent'             checks which matrices are unipotent, i.e. whose difference to the unit matrix is nilpotent
%               'unitary'               checks which matrices are unitary
%               'vandergraft'           checks which matrices are Vandergraft, and thus posses an invariant cone
%               'vandermonde'           (experimental) checks which matrices are vandermonde
%               'vpa'                   checks which matrices are vpa
%               'zero'                  checks which matrices have spectral radius zero
%
% Additionally allowed names, which are not deduced by this function, but evaluated if present
%               'name';                                     a name which describes the matrices
%               'has_ellipsoidal_minimal_invariant_kone';   true if the set of matrices have a minimal common invariant kone which is ellipsoidal
%               'has_polyhedral_minimal_invariant_kone';    true if the set of matrices have a minimal common invariant kone which is polyhedral
%               'has_invariant_kone';                       true if the set of matrices have a common invariant kone
%               'pairs_have_invariant_kone';                true if all pairs of the set of matrices have a common invariant kone
%               'triples_have_invariant_kone';              true if all triples of the set of matrices have a common invariant kone
%               'random'                                    true if the matrices were generated using some random process
%               
%               
%
%
% E.g.: matrixinfo( {[1 2 3; 0 2 1; 0 0 1], [2]} )
%
% Depends on: m-toolbox, tmisc-toolbox
%
% Written by: tommsch, 2019

%               2020-07-08, tommsch,    totally rewritten
%                                       some names changed
%                                       function renamed to matrixinfo
%                                       added option 'cmirror'
%               2020-09-01, tommsch,    bug fix of 'sym'
%               2020-09-25, tommsch,    added options 'symstrict' and 'vpa'
%               2024-06-28, tommsch,    Bugfix for non-square matrices
%               2024-07-18, tommsch,    Added option 'circulant','hankel','toeplitz'
%               2024-07-22, tommsch,    Behaviour change: Renamed 'dim' to 'ndims', renamed 'dim_same' to 'ndims_same'
%                                       Added options: 'altsign','circulant', 'defective', 'hankel', 'householder', 'involutory',
%                                       'l', 'laplacian', 'singular', 'toeplitz', 'tridiag',
%               2024-09-04, tommsch,    Improved parsing of input
%               2024-09-26, tommsch,    Added options: 'vandergraft', 'irreducible', 'epsilon', 'symbolic', (experimental) 'diagdom', 'arrowhead', 'conference', 'frobenius', 'vandermonde'
%                                       Bugfix for the case when this function is called recursively
%               2024-09-30, tommsch,    Added options: 'm', 'projection', 'rank', 'det', 'trace', 'kone', 'smpcandidate'
%               2024-10-10, tommsch,    Bugfix for 'cauchy'
%                                       Bugfix for: 'pm1'
%               2024-10-17, tommsch,    (experimental) Added possibility to compare different matrixinfos
%               2024-10-22, tommsch,    Added options: 'numcomp', 'acyclic', 'girth', 'circumference', 'rho_laplacian'
% Changelog:    2024-10-27, tommsch,    Bugfix in `conjugate` and `poslead` for symbolic stuff
%               2024-10-31, tommsch,    Added options: 'adjacency', 'incidence', 'unimodular', 'nilpotent', 'primitive', 'smith', 'hermite', 'charpoly'
%                                       Added basic support for code completion
%               2024-11-22, tommsch,    Added 'orthogonal', 'orthonormal', 'hyperbolic'
%                                       Added 'negdef', 'negsemidef', 'def', 'semidef', 'minors'
%                                       Added 'slpcandidate', 'persymmetric', 'unipotent', (experimental) 'hamiltonian', 'hurwitz', 'triangular'
%               2025-01-25, tommsch,    Added (experimental) 'moore', (experimental) 'comparison', (experimental) 'permutation', 'stieltjes', 'traceless'
%                                       'pauli', 'gellmann'


% XX missing: 
% General:           GRCAR, Routh–Hurwitz, P-matrix, monotone-matrix (inverse-positive-matrices), H-matrix, Wronskian
%                    Band, Bidiagonal, Bisymmetric, Block-diagonal, Block, Block tridiagonal, Centrosymmetric, ,
%                    Copositive, Discrete Fourier Transform, Equivalent, Generalized permutation, Hollow, unit Metzler, Nonnegative Pentadiagonal,
%                    Skyline, Sylvester, Z
% Products/Inverses: Companion
% With applications: Congruent, Totally unimodular, Weighing
%                    Duplication and elimination, Euclidean distance, Fundamental (linear differential equation),
%                    Gram, Moment, Pick, Rotation, Shear, Similarity, Symplectic, Totally positive, Transformation
% Statistics:        Centering, Correlation, Covariance, , Transition
% graph theory:      Biadjacency, Degree, Incidence, Seidel adjacency, number of components, number of n-partitness
% engineering:       Density, Fundamental (computer vision), Fuzzy associative, Irregular, Overlap, S, State transition, Substitution, Z (chemistry)

% Don't know what to do with it:
% ==============================
% Copositive, Seifert, Coxeter
%
% hard to implement: 
% ==================
% Alternant, Augmented Bézout, Cartan, Cofactor, Distance
%
% probably useless to implement: 
% ==============================
% Cabibbo–Kobayashi–Maskawa, Edmond, Tutte, Adjugate, Carleman, Commutation, Generator, Elementary, Walsh, Hadamard, Precision, Fisher information
%
% surely useless to implement:
% ============================
% Confusion, Quaternionic, Polynomial, Payoff, Jacobian, Design
%

%#ok<*TRYNC>

% XX Missing unit tests for a lot of properties
    [M, what, nfo, opt] = parse_input( varargin{:} );
    
    if( opt.matrix_given );
        nfo = matrix_info_worker( M, what, nfo, opt );
        % if( opt.all );
        %     N = cell( size(M) );
        %     for i = 1:numel( N );
        %         N{i} = pinv( N{i} ); end;
        %     nfoN = matrix_info_worker( N, what, nfo, opt );
        %    for n_ = fieldnames( nfoN )'; n = n_{1};
        %        nfo.(['inv_' n]) = nfoN.(n); end; end;
    end;
    

    if( istruthy(opt.oneflag, false) );
        if( isfield( nfo, opt.oneflag ) );
            nfo = nfo.(opt.oneflag);
        else;
            nfo = [];
            if( opt.verbose >= 0 );
                warning( 'matrixinfo:unkown_what', 'matrixinfo: Could not find property with name:\n %s', opt.oneflag ); end; end; end;

    if( opt.compare_nfo );
        [ nfo ] = compare_nfo( nfo{:} ); end;

    if( opt.compact );
        nfo = compactify( nfo ); end;

    %if( nargout == 0 );
    %    fprintf( '%s\n', dispc_compact( nfo ) ); end;

end

%%

function [ M, what, nfo, opt ] = parse_input( varargin );
    M = [];
    opt = struct;
    [opt.verbose, varargin] = parsem( {'verbose','v'}, varargin, 1 );
    [opt.compact, varargin] = parsem( {'compact'}, varargin );
    [opt.unknown, varargin] = parsem( {'Unknown'}, varargin, 'false' );    
    [opt.epsilon, varargin] = parsem( {'epsilon','eps'}, varargin, [] );
    opt.unknown = {'Unknown',opt.unknown};

    [opt.all, varargin] = parsem( {'all'}, varargin );
    opt.all = 2*opt.all;

    % find nfo struct
    structidx = cellfun( 'isclass', varargin, 'struct' );
    if( nnz(structidx) == 1 );
        nfo = varargin{structidx};
        varargin(structidx) = [];
        opt.compare_nfo = false;
    elseif( nnz(structidx) > 1 );
        nfo = varargin(structidx);
        varargin(structidx) = [];
        opt.compare_nfo = true;
    else;
        nfo = struct;
        opt.compare_nfo = false; end;

    % parse M
    if( numel(varargin) == 0 );
        opt.matrix_given = false;
        assert( opt.compare_nfo || numel(fieldnames(nfo)) > 0, 'matrixinfo:input', 'No matrices and no prop struct as input given.' );
    elseif( ~iscell(varargin{1}) );
        opt.matrix_given = true;
        M = varargin(1);
        varargin(1) = [];
    else;
        opt.matrix_given = true;
        M = varargin{1};
        varargin(1) = []; end;

    % parse what
    what = varargin;

    % set additional options
    opt.oneflag = false;
    if( numel(what) == 0 );
        if( ~opt.all );
            opt.all = true; end;
    elseif( numel(what) == 1 );
        opt.oneflag = what{1}; end;
    
    switch opt.all;
        case 0; % do nothing
        case 1; what = {'--all'};
        case 2; what = {'--ALL'};
        otherwise; fatal_error; end;

    if( numel(what) == 1 && strcmpi(what{1}, '--all') );
        opt.oneflag = false; end;

    for j = 1:numel( M );
        if( islogical(M{j}) );
            M{j} = double( M{j} ); end; end;  %#ok<AGROW>

    assert( islogical(opt.oneflag) || ischar(opt.oneflag) || isstring(opt.oneflag), 'matrixinfo:input', 'Given property ''what'' is not a string (but must be).\n' );
end

function [ nfo ] = matrix_info_worker( M, what, nfo, opt );
    
    nfo.symbolic = cellfun( 'isclass', M, 'sym' );
    nfo = symstrict_( M, nfo, opt );


    if( ~isempty(opt.epsilon) );
        if( isscalar(opt.epsilon) );
            opt.epsilon = repmat( opt.epsilon, size(M) ); end;
    else;
        opt.epsilon = zeros( size(M) );
        for i = 1:numel( opt.epsilon );
            if( ~nfo.symbolic(i) );
                opt.epsilon(i) = 4 * max( size(M{i}) ) * eps( norm( M{i}, 1 ) );
            elseif( nfo.vpa(i) );
                opt.epsilon(i) =  4 * max( size(M{i}) ) * 10^(-digits) * norm( M{i}, 1 );
            elseif( nfo.symstrict(i) );
                opt.epsilon(i) = 0;
            else;
                fatal_error; end; end; end;
    
    nfo.n = numel( M );
    
    while( true );
        [flag, what] = test( what, 'acyclic' );             if( flag );     nfo = acyclic_( M, nfo, opt );          if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'adjacency' );           if( flag );     nfo = adjacency_( M, nfo, opt );        if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'altsign' );             if( flag );     nfo = altsign_( M, nfo, opt );          if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'antidiag' );            if( flag );     nfo = antidiag_( M, nfo, opt );         if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'arrowhead' );           if( flag );     nfo = arrowhead_( M, nfo, opt );        if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'bool' );                if( flag );     nfo = bool_( M, nfo, opt );             if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'cauchy' );              if( flag );     nfo = cauchy_( M, nfo, opt );           if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'charpoly', 0 );         if( flag );     nfo = charpoly_( M, nfo, opt );         if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'circulant' );           if( flag );     nfo = circulant_( M, nfo, opt );        if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'circumference', 0 );    if( flag );     nfo = circumference_( M, nfo, opt );    if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'cmirror' );             if( flag );     nfo = cmirror_( M, nfo, opt );          if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'commute' );             if( flag );     nfo = commute_( M, nfo, opt );          if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'commuteall' );          if( flag );     nfo = commuteall_( M, nfo, opt );       if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'comparison', 0 );       if( flag );     nfo = comparison_( M, nfo, opt );        if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'cond' );                if( flag );     nfo = cond_( M, nfo, opt );             if( endtest(what) ); break; end; end;
        %[flag, what] = test( what, 'condeig' );             if( flag );     nfo = condeig_( M, nfo, opt );         if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'condeigvec' );          if( flag );     nfo = condeigvec_( M, nfo, opt );       if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'conference' );          if( flag );     nfo = conference_( M, nfo, opt );       if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'conjugate' );           if( flag );     nfo = conjugate_( M, nfo, opt );        if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'cycles', 0 );           if( flag );     nfo = cycles_( M, nfo, opt );           if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'def' );                 if( flag );     nfo = def_( M, nfo, opt );              if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'defective' );           if( flag );     nfo = defective_( M, nfo, opt );        if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'det' );                 if( flag );     nfo = det_( M, nfo, opt );              if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'diag' );                if( flag );     nfo = diag_( M, nfo, opt );             if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'diagdom' );             if( flag );     nfo = diagdom_( M, nfo, opt );          if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'dirac', 0 );            if( flag );     nfo = dirac_( M, nfo, opt );            if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'eigvec' );              if( flag );     nfo = eigvec_( M, nfo, opt );           if( endtest(what) ); break; end; end;  % eigvec also computes eig, so it must come beforehand
        [flag, what] = test( what, 'eig');                  if( flag );     nfo = eig_( M, nfo, opt );              if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'empty' );               if( flag );     nfo = empty_( M, nfo, opt );            if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'equal' );               if( flag );     nfo = equal_( M, nfo, opt );            if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'finite' );              if( flag );     nfo = finite_( M, nfo, opt );           if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'frobenius' );           if( flag );     nfo = frobenius_( M, nfo, opt );        if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'gellmann', 0 );         if( flag );     nfo = gellmann_( M, nfo, opt );         if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'girth', 0 );            if( flag );     nfo = girth_( M, nfo, opt );            if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'hamiltonian' );         if( flag );     nfo = hamiltonian_( M, nfo, opt );      if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'hankel' );              if( flag );     nfo = hankel_( M, nfo, opt );           if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'hermite', 0 );          if( flag );     nfo = hermiteform_( M, nfo, opt );      if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'hermitian' );           if( flag );     nfo = hermitian_( M, nfo, opt );        if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'hermitian_skew' );      if( flag );     nfo = hermitian_skew_( M, nfo, opt );   if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'hessl' );               if( flag );     nfo = hessl_( M, nfo, opt );            if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'hessu' );               if( flag );     nfo = hessu_( M, nfo, opt );            if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'householder' );         if( flag );     nfo = householder_( M, nfo, opt );      if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'hurwitz', 0 );          if( flag );     nfo = hurwitz_( M, nfo, opt );          if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'hyperbolic' );          if( flag );     nfo = hyperbolic_( M, nfo, opt );       if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'identity' );            if( flag );     nfo = identity_( M, nfo, opt );         if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'int' );                 if( flag );     nfo = int_( M, nfo, opt );              if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'inv' );                 if( flag );     nfo = inv_( M, nfo, opt );              if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'involutory' );          if( flag );     nfo = involutory_( M, nfo, opt );       if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'incidence', 0 );        if( flag );     nfo = incidence_( M, nfo, opt );        if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'irreducible', 0 );      if( flag );     nfo = irreducible_( M, nfo, opt );      if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'jordan', 0 );           if( flag );     nfo = jordan_( M, nfo, opt );           if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'kone', 0 );             if( flag );     nfo = kone_( M, nfo, opt );             if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'l' );                   if( flag );     nfo = l_( M, nfo, opt );                if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'laplacian' );           if( flag );     nfo = laplacian_( M, nfo, opt );        if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'le' );                  if( flag );     nfo = le_( M, nfo, opt );               if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'leidentity' );          if( flag );     nfo = leidentity_( M, nfo, opt );       if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'lognorm' );             if( flag );     nfo = lognorm_( M, nfo, opt );          if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'lt' );                  if( flag );     nfo = lt_( M, nfo, opt );               if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'm' );                   if( flag );     nfo = m_( M, nfo, opt );                if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'metzler' );             if( flag );     nfo = metzler_( M, nfo, opt );          if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'minors', 0 );           if( flag );     nfo = minors_( M, nfo, opt );           if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'minpoly', 0 );          if( flag );     nfo = minpoly_( M, nfo, opt );          if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'moore' );               if( flag );     nfo = moore_( M, nfo, opt );            if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'n' );                   if( flag );     nfo = n_( M, nfo, opt );                if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'nan' );                 if( flag );     nfo = nan_( M, nfo, opt );              if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'ndims' );               if( flag );     nfo = ndims_( M, nfo, opt );            if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'neg' );                 if( flag );     nfo = neg_( M, nfo, opt );              if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'negdef' );              if( flag );     nfo = negdef_( M, nfo, opt );           if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'negsemidef' );          if( flag );     nfo = negsemidef_( M, nfo, opt );       if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'nilpotent', 0 );        if( flag );     nfo = nilpotent_( M, nfo, opt );        if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'norm' );                if( flag );     nfo = norm_( M, nfo, opt );             if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'nonzero' );             if( flag );     nfo = nonzero_( M, nfo, opt );          if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'normal' );              if( flag );     nfo = normal_( M, nfo, opt );           if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'nonneg' );              if( flag );     nfo = nonneg_( M, nfo, opt );           if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'nonpos' );              if( flag );     nfo = nonpos_( M, nfo, opt );           if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'numcomp' );             if( flag );     nfo = numcomp_( M, nfo, opt );          if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'orthogonal' );          if( flag );     nfo = orthogonal_( M, nfo, opt );       if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'orthonormal' );         if( flag );     nfo = orthonormal_( M, nfo, opt );      if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'pauli', 0 );            if( flag );     nfo = pauli_( M, nfo, opt );            if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'permutation' );         if( flag );     nfo = permutation_( M, nfo, opt );      if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'persymmetric' );        if( flag );     nfo = persymmetric_( M, nfo, opt );     if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'pm1' );                 if( flag );     nfo = pm1_( M, nfo, opt );              if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'pos' );                 if( flag );     nfo = pos_( M, nfo, opt );              if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'posdef' );              if( flag );     nfo = posdef_( M, nfo, opt );           if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'poslead' );             if( flag );     nfo = poslead_( M, nfo, opt );          if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'possemidef' );          if( flag );     nfo = possemidef_( M, nfo, opt );       if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'primitive', 0 );        if( flag );     nfo = primitive_( M, nfo, opt );        if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'projection' );          if( flag );     nfo = projection_( M, nfo, opt );       if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'rank' );                if( flag );     nfo = rank_( M, nfo, opt );             if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'rat', 0 );              if( flag );     nfo = rat_( M, nfo, opt );              if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'real' );                if( flag );     nfo = real_( M, nfo, opt );             if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'rho' );                 if( flag );     nfo = rho_( M, nfo, opt );              if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'rho_laplacian' );       if( flag );     nfo = rho_laplacian_( M, nfo, opt );    if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'semidef' );             if( flag );     nfo = semidef_( M, nfo, opt );          if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'signature' );           if( flag );     nfo = signature_( M, nfo, opt );        if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'singular' );            if( flag );     nfo = singular_( M, nfo, opt );         if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'size' );                if( flag );     nfo = size_( M, nfo, opt );             if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'slpcandidate', 0 );     if( flag );     nfo = slpcandidate_( M, nfo, opt );     if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'smith', 0 );            if( flag );     nfo = smith_( M, nfo, opt );            if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'smpcandidate', 0 );     if( flag );     nfo = smpcandidate_( M, nfo, opt );     if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'sparsity' );            if( flag );     nfo = sparsity_( M, nfo, opt );         if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'square' );              if( flag );     nfo = square_( M, nfo, opt );           if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'stieltjes' );           if( flag );     nfo = stieltjes_( M, nfo, opt );        if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'stochastic_column' );   if( flag );     nfo = stochastic_column_( M, nfo, opt );if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'stochastic_double' );   if( flag );     nfo = stochastic_double_( M, nfo, opt );if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'stochastic_row' );      if( flag );     nfo = stochastic_row_( M, nfo, opt );   if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'svd' );                 if( flag );     nfo = svd_( M, nfo, opt );              if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'sym' );                 if( flag );     nfo = sym_( M, nfo, opt );              if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'symmetric' );           if( flag );     nfo = symmetric_( M, nfo, opt );        if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'symmetric_skew' );      if( flag );     nfo = symmetric_skew_( M, nfo, opt );   if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'symstrict' );           if( flag );     nfo = symstrict_( M, nfo, opt );        if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'ternary' );             if( flag );     nfo = ternary_( M, nfo, opt );          if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'toeplitz' );            if( flag );     nfo = toeplitz_( M, nfo, opt );         if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'totallypos', 0 );       if( flag );     nfo = totallypos_( M, nfo, opt );       if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'trace' );               if( flag );     nfo = trace_( M, nfo, opt );            if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'traceless' );           if( flag );     nfo = traceless_( M, nfo, opt );        if( endtest(what) ); break; end; end;        
        [flag, what] = test( what, 'tridiag' );             if( flag );     nfo = tridiag_( M, nfo, opt );          if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'trangular' );           if( flag );     nfo = triangular_( M, nfo, opt );       if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'tril' );                if( flag );     nfo = tril_( M, nfo, opt );             if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'triu' );                if( flag );     nfo = triu_( M, nfo, opt );             if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'unimodular' );          if( flag );     nfo = unimodular_( M, nfo, opt );       if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'unipotent' );           if( flag );     nfo = unipotent_( M, nfo, opt );        if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'unitary' );             if( flag );     nfo = unitary_( M, nfo, opt );          if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'vandergraft' );         if( flag );     nfo = vandergraft_( M, nfo, opt );      if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'vandermonde' );         if( flag );     nfo = vandermonde_( M, nfo, opt );      if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'vpa' );                 if( flag );     nfo = vpa_( M, nfo, opt );              if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'z' );                   if( flag );     nfo = z_( M, nfo, opt );                if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'rhozero' );             if( flag );     nfo = rhozero_( M, nfo, opt );          if( endtest(what) ); break; end; end;
        [flag, what] = test( what, 'check' );               if( flag );     check_prop( M, nfo, opt );              if( endtest(what) ); break; end; end;
        
        maybe_unused( what );
        break;
    end;    
end

%% helper functions

function [ ret, what ] = test( what, name, default );
    if( nargin <= 2 || isempty(default) );
        default = 1; end;
    idx = strcmp( what, name );
    if( any(idx) );
        ret = true;
        what( idx ) = [];
    elseif( isempty(what) );
        ret = false;
    elseif( any(strcmpi(what, '--all')) && default || ...
            any(strcmp(what, '--ALL')) ...
          );
        ret = true;
    else;
        ret = false; end;
end

function [ ret ] = endtest( what );
    if( ~any(strcmpi(what, '--all')) && isempty(what) );
        ret = true;
    else;
        ret = false; end;
end

function [ nfo ] = compact_nfo( nfo );
    for name_ = fieldnames( nfo )'; name = name_{1};
        x = nfo.(name);
        if( (isnumeric( x ) || islogical( x )) && ~any(x(:)) || ...
            isequal( x, [] ) ...
          );
            nfo = rmfield( nfo, name ); end; end;
    if( isfield(nfo, 'pm1') && isfield(nfo, 'bool') && ...
        all(nfo.pm1) && all(nfo.bool) ...
      );
        nfo = rmfield( nfo, 'pm1' );
    end;
end

function st = rmfieldt( st, names )
    names = tocell( names );
    for name_ = names; name = name_{1};
        if( isfield(st, name) );
            st = rmfield( st, name ); end; end;
end

%% test functions
function [ nfo ] = acyclic_( M, nfo, opt );
    if( isfield(nfo, 'acyclic') );
        return; end;
    nfo.acyclic = false( size(M) );
    nfo = square_( M, nfo, opt );
    nfo = adjacency_( M, nfo, opt );
    for i = 1:numel( M );
        if( ~nfo.square(i) );
            continue; end;
        nfo.acyclic(i) = isdag( digraph( nfo.adjacency{i} ) ); end;
end
function [ nfo ] = adjacency_( M, nfo, opt );
    if( isfield(nfo, 'adjacency') );
        return; end;
    clean = bprintf( '[adjacency]' );  %#ok<NASGU>
    nfo.adjacency = cell( size(M) );
    nfo = square_( M, nfo, opt );
    for i = 1:numel( M );
        if( ~nfo.square(i) );
            continue; end;
        nfo.adjacency{i} = isAlways( abs(M{i}) > 0, opt.unknown{:} ); end;
end
function [ nfo ] = altsign_( M, nfo, opt );
    if( isfield(nfo, 'altsign') );
        return; end;
    nfo = pm1_( M, nfo, opt );
    nfo = square_( M, nfo, opt );
    nfo.altsign = false( size(M) );
    for i = 1:numel( M );
        nfo.altsign(i) = isaltsign( M{i} ); end;
end
function [ nfo ] = antidiag_( M, nfo, opt );
    if( isfield(nfo, 'antidiag') );
        return; end;
    nfo.antidiag = false( size(M) );
    for i = 1:numel( M );
        Mi = fliplr( M{i} );
        mask = ~eye( size(Mi) );
        nfo.antidiag(i) = isempty( Mi ) || ...
                           isAlways( norm( Mi.*mask, 1 ) <= opt.epsilon(i) * max(size(Mi)), opt.unknown{:} ); end; 
end
function [ nfo ] = arrowhead_( M, nfo, opt );
    if( isfield(nfo, 'arrowhead') );
        return; end;
    nfo.arrowhead = false( size(M) );
    for i = 1:numel( M );
        mask = eye( size(M{i}) );
        mask(1,:) = 1;
        mask(:,1) = 1;
        nfo.arrowhead(i) = allm( isAlways( M{i}.*~mask == 0, opt.unknown{:} ) ); end;
end
function [ nfo ] = bool_( M, nfo, opt );
    if( isfield(nfo, 'bool') );
        return; end;
    nfo.bool = false( size(M) );
    for i = 1:numel( M );
        nfo.bool(i) = allm( isAlways( M{i} == 0, opt.unknown{:} ) | ...
                             isAlways( M{i} == 1, opt.unknown{:} ) ); end;
end
function [ nfo ] = cauchy_( M, nfo, opt );
    if( isfield(nfo, 'cauchy') );
        return; end;
    nfo.cauchy = false( size(M) );
    for i = 1:numel( M );
        if( isempty(M{i}) );
            continue; end;
        nfo.cauchy(i) = iscauchy( M{i}, opt.epsilon(i) ); end;
end
function [ nfo ] = charpoly_( M, nfo, opt );
    if( isfield(nfo, 'charpoly') );
        return; end;
    nfo.charpoly = cell( size(M) );
    nfo = square_( M, nfo, opt );
    nfo = finite_( M, nfo, opt );
    for j = 1:numel( M );
        if( nfo.square(j) && ~isempty(M{j}) && nfo.finite(j) );
            nfo.charpoly{j} = charpoly( M{j} ); end; end;
end
function [ nfo ] = circulant_( M, nfo, opt );
    if( isfield(nfo, 'circulant') );
        return; end;
    nfo.circulant = false( size(M) );
    for i = 1:numel( M );
        nfo.circulant(i) = iscirculant( M{i}, opt.epsilon(i) ); end;
end
function [ nfo ] = circumference_( M, nfo, opt );
    if( isfield(nfo, 'circumference') );
        return; end;
    nfo = cycles_( M, nfo, opt );
    nfo.circumference = zeros( size(M) );
    for i = 1:numel( M );
        if( isempty(nfo.cycles{i}) );
            continue; end;
        len = cellfun( 'prodofsize', nfo.cycles{i} );
        nfo.circumference(i) = max( len ); end;
end
function [ nfo ] = cmirror_( M, nfo, opt );
        nfo.cmirror = logical( eye( numel( M ) ) );
        for i = 1:numel( M );
            for j = i+1:numel( M );
                if( isAlways( norm( M{i} - conj(M{j}), 1) <= opt.epsilon(i), opt.unknown{:} ) || ...          % complex conjugated, mirrored around real axis  % works better for double numbers
                    isAlways( norm( abs(M{i}).^2 - M{i}.*M{j}, 1) <= opt.epsilon(i), opt.unknown{:} ) || ...  % complex conjugated, mirrored around real axis  % using conj fails due to a matlab bug for symbolic root expressions
                    isAlways( norm( M{i} + conj(M{j}), 1) <= opt.epsilon(i), opt.unknown{:} ) || ...          % mirrored around complex axis
                    isAlways( norm( abs(M{i}).^2 + M{i}.*M{j}, 1) <= opt.epsilon(i), opt.unknown{:} ) || ...  % mirrored around complex axis
                    isAlways( norm(M{i} + M{j}, 1) <= opt.epsilon(i), opt.unknown{:} ) || ...                 % negativ to each other, mirrored around 0
                    isAlways( norm(M{i} - M{j}, 1) <= opt.epsilon(i), opt.unknown{:} ) );                     % the same values, not mirrored
                    nfo.cmirror(i,j) = true;
                    nfo.cmirror(j,i) = true; end; end; end;
end
function [ nfo ] = commute_( M, nfo, opt );
    if( isfield(nfo, 'commute') );
        return; end;
    nfo = square_( M, nfo, opt );
    nfo = sym_( M, nfo, opt );
    nfo.commute = logical( eye( numel(M) ) );
    comm = @(A, B) A*B - B*A;
    
    for i = 1:numel( M );
        for j = i+1:numel( M );
            if( ~nfo.square(i) );
                nfo.commute(i,:) = false;
                nfo.commute(:,i) = false;
            elseif( ~nfo.square(j) );
                nfo.commute(:,j) = false;
                nfo.commute(j,:) = false;
            elseif( any(nfo.sym) )
                flag = isAlways( comm(M{i}, M{j}) == 0, opt.unknown{:} );
                nfo.commute(i,j) = all( flag(:) );
                nfo.commute(j,i) = nfo.commute(i,j);
            else;
                nfo.commute(i,j) = norm( comm(M{i}, M{j})) <= 1e-12 * norm(M{i}) * norm(M{j} );
                nfo.commute(j,i) = nfo.commute(i,j); end; end; end;
end
function [ nfo ] = commuteall_( M, nfo, opt );
    if( isfield(nfo, 'commuteall') );
        return; end;
    nfo = square_( M, nfo, opt );
    nfo = commute_( M, nfo, opt );
    nfo.commuteall = all( nfo.commute(:) );
end
function [ nfo ] = comparison_( M, nfo, opt );  %#ok<INUSD>
    if( isfield(nfo, 'comparison') );
        return; end;    
    nfo.comparison = cell( size(M) );
    for i = 1:numel( M );
        m = size( M{i}, 2 );
        nfo.comparison{i} = -abs( M{i} );
        nfo.comparison{i}(1:m+1:end) = -nfo.comparison{i}(1:m+1:end);
        end;
end
function [ nfo ] = cond_( M, nfo, opt );
    if( isfield(nfo, 'cond') );
        return; end;
    nfo = finite_( M, nfo, opt );
    nfo.cond = zeros( size(M) );
    for i = 1:numel( M );
        if( ~nfo.finite(i) );
            nfo.cond(i) = inf;
        else;
            nfo.cond(i) = cond( M{i} ); end; end;
end
% function [ nfo ] = condeig_( M, nfo, opt );
%     if( isfield(nfo, 'condeig') );
%         return; end;
%     nfo.condeig = cell( size(M) );
%     for i = 1:numel( M );
%         nfo.condeig{i} = condeig( M{i} ); end; 
% end
function [ nfo ] = condeigvec_( M, nfo, opt );
    if( isfield(nfo, 'condeigvec') );
        return; end;
    nfo.condeigvec = zeros( size(M) );
    nfo = square_( M, nfo, opt );
    nfo = eigvec_( M, nfo, opt );
    for i = 1:numel( M );
        if( nfo.square(i) );
            nfo.condeigvec(i) = cond( nfo.eigvec{i} ); end; end;
end
function [ nfo ] = conference_( M, nfo, opt );
    if( isfield(nfo, 'conference') );
        return; end;
    nfo.conference = false( size(M) );
    nfo = eigvec_( M, nfo, opt );
    for i = 1:numel( M );
        if( isempty(M{i}) );
            continue; end;
        check1 = allm( isAlways( diag(M{i}) == 0, opt.unknown{:} ) );  % zero on the diagonal
        if( ~check1 );
            continue; end;
        check2 = allm( isAlways( (abs(M{i}) - 1).*double(~eye(size(M{i}))) == 0, opt.unknown{:} ) );  % +-1 everywhere else
        if( ~check2 );
            continue; end;
        Mi = M{i}*M{i}';
        check3 = allm( isAlways( Mi/Mi(1) - eye(size(M{i})) < opt.epsilon(i) * max(size(M{i})), opt.unknown{:} ) );
        if( ~check3 );
            continue; end;
        nfo.conference(i) = true; end; 
end
function [ nfo ] = conjugate_( M, nfo, opt );
    if( isfield(nfo, 'conjugate') );
        return; end;
    nfo.conjugate = false( numel( M ), numel( M ) );
    nfo = real_( M, nfo, opt );
    for i = 1:numel( M );
        if( nfo.real(i) );  % isreal does not work with sybolic stuff
            nfo.conjugate(i,i) = true; end;
        for j = i+1:numel( M );
            test1 = norm( abs(M{i}).^2 - M{i}.*M{j}, 1 );  % I cannot use conj(), since Matlab handles root objects wrongly together with conj().
            test2 = norm( M{i} - M{j}, 1 );
            if( nfo.real(i) && nfo.real(j) );
                nfo.conjugate(i,j) = isAlways( norm( M{i} - M{j}, 1 ) <= opt.epsilon(i), opt.unknown{:} );
                nfo.conjugate(j,i) = nfo.conjugate(i,j);
                continue; end;
            
            if( isAlways( test1 <= opt.epsilon(i), opt.unknown{:} ) && ...
                isAlways( test2 >  opt.epsilon(i), opt.unknown{:} ) ...
               );
                nfo.conjugate(i,j) = true;
                nfo.conjugate(j,i) = true;
                continue; end;
            try;
                test1 = isAlways( double(test1) <= 1e-12, opt.unknown{:} );
                test2 = isAlways( double(test2) >  1e12,  opt.unknown{:} );
                if(  test1 && test2 );
                    warning( 'matrixinfo:conjugate', 'Matrices seem to be conjugated to each other in floating point precision, but not symbolically. This could be either a problem with floating point precision, or with symbolic computations.' );
                    continue;
                    end;
            catch me;
                switch me.identifier;
                    case {'symbolic:double:CantConvertVariable','symbolic:double:cantconvert'};
                        % do nothing
                    otherwise;
                        warning( 'matrixinfo:conjugate', 'Some unknown error occured.' ); end; end; end; end;
end
function [ nfo ] = cycles_( M, nfo, opt );
    if( isfield(nfo, 'cycles') );
        return; end;
    nfo = square_( M, nfo, opt );
    nfo = adjacency_( M, nfo, opt );
    nfo.cycles = cell( size(M) );
    clean = bprintf( '[cycles]' );  %#ok<NASGU>
    MAX_NUM_CYCLES = 1e7;
    for i = 1:numel( M );
        if( ~nfo.square(i) );
            continue; end;
        G = digraph( nfo.adjacency{i} );
        nfo.cycles{i} = allcycles( G, 'MaxNumCycles',MAX_NUM_CYCLES );
        expect( numel(nfo.cycles{i}) < MAX_NUM_CYCLES, 'matrixinfo:cycles', 'Graph has too many cycles. All properties related to cycles are most likely wrong.' );
        end;
end
function [ nfo ] = diag_( M, nfo, opt );
    if( isfield(nfo, 'diag') );
        return; end;
    nfo.diag = false( size(M) );
    for i = 1:numel( M );
        mask = ~eye( size(M{i}) );
        nfo.diag(i) = isempty( M{i} ) || ...
                       isAlways( norm( M{i}.*mask, 1 ) <= opt.epsilon(i) * max(size(M{i})), opt.unknown{:} ); end; 
end
function [ nfo ] = diagdom_( M, nfo, opt );
    if( isfield(nfo, 'diagdom') );
        return; end;
    nfo.diagdom = false( size(M) );
    for i = 1:numel( M );
        mask = eye( size(M{i}) );
        if( isempty(M{i}) );
            nfo.diagdom(i) = true;
            continue; end;
        sd = diag( abs(M{i}) );
        s1 = sum( abs(M{i}) .* ~mask, 1 )';
        s2 = sum( abs(M{i}) .* ~mask, 2 );
        nfo.diagdom(i) = numel(sd) == numel(s1) && all( isAlways( sd >= s1, opt.unknown{:} ) ) || ...
                          numel(sd) == numel(s2) && all( isAlways( sd >= s2, opt.unknown{:} ) ); end; 
end
function [ nfo ] = dirac_( M, nfo, opt );
    if( isfield(nfo, 'dirac') );
        return; end;
    nfo = square_( M, nfo, opt );
    nfo.dirac = false( size(M) );
    for i = 1:numel( M );
        if( ~nfo.square(i) );
            continue; end;
        if( ~isequal(size(M{i}), [4 4]) );
            continue; end;
        nfo.dirac(i) = isAlways( norm( M{i} - diag([1 1 -1 -1]) ) <= opt.epsilon(i) ) || ...
                       isAlways( norm( M{i} - fliplr(diag([1 1 -1 -1])) ) <= opt.epsilon(i) ) || ...
                       isAlways( norm( M{i} - fliplr(diag([-1i 1i 1i -1i])) ) <= opt.epsilon(i) ) || ...
                       isAlways( norm( M{i} - [0 0 1 0;0 0 0 -1;-1 0 0 0;0 1 0 0] ) <= opt.epsilon(i) );
        end;
end
function [ nfo ] = def_( M, nfo, opt );
    if( isfield(nfo, 'def') );
        return; end;
    nfo.def = false( size(M) );
    nfo = posdef_( M, nfo, opt );
    nfo = negdef_( M, nfo, opt );
    for i = 1:numel( M );        
        nfo.def(i) = nfo.posdef(i) || nfo.negdef(i); end;
end
function [ nfo ] = defective_( M, nfo, opt );
    if( isfield(nfo, 'defective') );
        return; end;
    nfo.defective = false( size(M) );
    nfo = square_( M, nfo, opt );
    nfo = eigvec_( M, nfo, opt );
    for i = 1:numel( M );
        if( nfo.square(i) );
            nfo.defective(i) = isAlways( cond( nfo.eigvec{i} ) > 1/opt.epsilon(i), opt.unknown{:} ); end; end;
end
function [ nfo ] = det_( M, nfo, opt );
    if( isfield(nfo, 'det') );
        return; end;
    nfo = square_( M, nfo, opt );
    nfo.det = nan( size(M) );
    for i = 1:numel( M );
        if( nfo.square(i) );
            nfo.det(i) = det( M{i} ); end; end;
end
function [ nfo ] = ndims_( M, nfo, opt );  %#ok<INUSD>
    if( isfield(nfo, 'ndims') );
        return; end;
    nfo.ndims = zeros( size(M) );
    for i = 1:numel( M );
        nfo.ndims(i) = ndimsm( M{i} ); end;
    nfo.ndims_same = ~any( diff(nfo.ndims(:)) );
end
function [ nfo ] = eig_( M, nfo, opt );
    if( isfield(nfo, 'eig') );
        return; end;
    nfo = square_( M, nfo, opt );
    nfo = finite_( M, nfo, opt );
    nfo.eig = cell( size(M) );
    for j = 1:numel( M );
        if( nfo.square(j) && nfo.finite(j) );
            eigval = eig( M{j} );
            if( isempty(eigval) );
                zeros( size(M{j}, 1), 1 );
            else;
                nfo.eig{j} = eigval; end;
        else;
            nfo.eig{j} = zeros( size(M{j}, 1), 1 ); end; end;
end
function [ nfo ] = eigvec_( M, nfo, opt );
    if( isfield(nfo, 'eigvec') );
        return; end;
    clean = bprintf( '[eigvec]' );  %#ok<NASGU>
    nfo = square_( M, nfo, opt );
    nfo = finite_( M, nfo, opt );
    nfo.eigvec = cell( size(M) );
    nfo.eig = cell( size(M) );
    for j = 1:numel( M );
        if( nfo.square(j) && nfo.finite(j) );
            [nfo.eigvec{j}, eigval] = eig( M{j} );
            if( isempty(eigval) );
                nfo.eig{j} = [];
            else;
                nfo.eig{j} = diag( eigval ); end;
        else;
            nfo.eig{j} = []; end; end;
end
function [ nfo ] = empty_( M, nfo, opt );  %#ok<INUSD>
    if( isfield(nfo, 'empty') );
        return; end;
    nfo.empty = false( size(M) );
    for i = 1:numel( M );
        nfo.empty(i) = isempty( M{i} ); end;
end
function [ nfo ] = equal_( M, nfo, opt );  %#ok<INUSD>
    if( isfield(nfo, 'equal') );
        return; end;
    nfo.equal = false( numel(M), numel(M) );
    for i = 1:numel( M );
        for j = 1:numel( M );
            nfo.equal(i,j) = isequal( M{i}, M{j} ); end; end;
end
function [ nfo ] = finite_( M, nfo, opt );  %#ok<INUSD>
    if( isfield(nfo, 'finite') );
        return; end;
    nfo.finite = false( size(M) );
    for i = 1:numel( M );
        nfo.finite(i) = allm( isfinite(M{i}) ); end; 
end
function [ nfo ] = frobenius_( M, nfo, opt );
    if( isfield(nfo, 'frobenius') );
        return; end;
    nfo.frobenius = false( size(M) );
    for i = 1:numel( M );
        check1 = all( isAlways( abs( diag(M{i}) - 1 ) < opt.epsilon(i), opt.unknown{:} ) );
        if( ~check1 );
            continue; end;
        Mi = M{i} .* double( ~eye(size(M{i})) );
        [r, c] = find( Mi, 1 );
        if( isempty(r) );
            nfo.frobenius(i) = true;
            continue; end;
        mask = double( ~eye( size(M{i}) ) );
        mask( c:end, c ) = 0;
        nfo.frobenius(i) = allm( isAlways( abs( M{i}.*mask ) < opt.epsilon(i), opt.unknown{:} ) ); end; 
end
function [ nfo ] = gellmann_( M, nfo, opt );
    if( isfield(nfo, 'gellmann') );
        return; end;
    nfo = square_( M, nfo, opt );
    nfo.gellmann = false( size(M) );
    for i = 1:numel( M );
        if( ~nfo.square(i) );
            continue; end;
        if( ~isequal(size(M{i}), [3 3]) );
            continue; end;
        nfo.gellmann(i) = isAlways( norm( M{i} - [0   1   0;  1  0   0;  0  0  0] )         <= opt.epsilon(i) ) || ...
                          isAlways( norm( M{i} - [0 -1i   0; 1i  0   0;  0  0  0] )         <= opt.epsilon(i) ) || ...
                          isAlways( norm( M{i} - [1   0   0;  0 -1   0;  0  0  0] )         <= opt.epsilon(i) ) || ...
                          isAlways( norm( M{i} - [0   0   1;  0  0   0;  1  0  0] )         <= opt.epsilon(i) ) || ...
                          isAlways( norm( M{i} - [0   0 -1i;  0  0   0; 1i  0  0] )         <= opt.epsilon(i) ) || ...
                          isAlways( norm( M{i} - [0   0   0;  0  0   1;  0  1  0] )         <= opt.epsilon(i) ) || ...
                          isAlways( norm( M{i} - [0   0   0;  0  0 -1i;  0 1i  0] )         <= opt.epsilon(i) ) || ...
                          isAlways( norm( M{i} - [1   0   0;  0  1   0;  0  0 -2]/sqrt(3) ) <= opt.epsilon(i) );
        end;
end
function [ nfo ] = girth_( M, nfo, opt );
    if( isfield(nfo, 'girth') );
        return; end;
    nfo = cycles_( M, nfo, opt );
    nfo.girth = zeros( size(M) );
    for i = 1:numel( M );
        if( isempty(nfo.cycles{i}) );
            continue; end;
        len = cellfun( 'prodofsize', nfo.cycles{i} );
        nfo.girth(i) = min( len ); end;
end

function [ nfo ] = hamiltonian_( M, nfo, opt );
    if( isfield(nfo, 'hamiltonian') );
        return; end;
    nfo.hamiltonian = false( size(M) );
    for i = 1:numel( M );
        nfo.hamiltonian(i) = ishamiltonian( M{i}, opt.epsilon(i) ); end;
end
function [ nfo ] = hankel_( M, nfo, opt );
    if( isfield(nfo, 'hankel') );
        return; end;
    nfo.hankel = false( size(M) );
    for i = 1:numel( M );
        nfo.hankel(i) = ishankel( M{i}, opt.epsilon(i) ); end;
end
function [ nfo ] = hermiteform_( M, nfo, opt );
    if( isfield(nfo, 'hermiteform') );
        return; end;
    clean = bprintf( '[hermiteform]' );  %#ok<NASGU>
    nfo = square_( M, nfo, opt );
    nfo = int_( M, nfo, opt );
    nfo.hermiteform = cell( size(M) );
    for i = 1:numel( M );
        if( nfo.square(i) && nfo.int(i) );
            nfo.hermiteform{i} = diag( hermiteForm( M{i} ) ); end; end;
end
function [ nfo ] = hermitian_( M, nfo, opt );  %#ok<INUSD>
    if( isfield(nfo, 'hermitian') );
        return; end;
    nfo.hermitian = false( size(M) );
    for i = 1:numel( M );
        nfo.hermitian(i) = isequal( M{i}, M{i}' ); end;
end    
function [ nfo ] = hermitian_skew_( M, nfo, opt );
    if( isfield(nfo, 'hermitian_skew') );
        return; end;
    nfo = square_( M, nfo, opt );
    nfo.hermitian_skew = false( size(M) );
    for i = 1:numel( M );
        nfo.hermitian_skew(i) = nfo.square(i) && ...
                                isAlways( norm( M{i} - M{i}', 1 ) < opt.epsilon(i), opt.unknown{:} ); end;
end    
function [ nfo ] = hessu_( M, nfo, opt );
    if( isfield(nfo, 'hessu') );
        return; end;
    nfo.hessu = false( size( M ) );
    for i = 1:numel( M );
        nfo.hessu(i) = isAlways( norm( triu(M{i}, -1) - M{i}, 1 ) < opt.epsilon(i), opt.unknown{:} ); end;
end
function [ nfo ] = hessl_( M, nfo, opt );
    if( isfield(nfo, 'hessl') );
        return; end;
    nfo.hessl = false( size( M ) );
    for i = 1:numel( M );
        nfo.hessl(i) = isAlways( norm( tril( M{i}, 1) - M{i}, 1 ) < opt.epsilon(i), opt.unknown{:} ); end;
end
function [ nfo ] = householder_( M, nfo, opt );
    if( isfield(nfo, 'householder') );
        return; end;
    nfo = square_( M, nfo, opt );
    nfo = finite_( M, nfo, opt );
    nfo.householder = false( size(M) );
    for i = 1:numel( M );
        if( nfo.square(i) && nfo.finite(i) );
            try;
                nfo.householder(i) = ishouseholder( M{i} );
            catch me;
                warning( me.identifier, '%s', me.message ); end; end; end;
end
function [ nfo ] = hurwitz_( M, nfo, opt );
    if( isfield(nfo, 'hurwitz') );
        return; end;
    nfo = eig_( M, nfo, opt );
    nfo = empty_( M, nfo, opt );
    nfo.hurwitz = false( size(M) );
    for i = 1:numel( M );
        if( ~nfo.square(i) || nfo.empty(i) );
            continue; end;
        nfo.hurwitz(i) = all( isAlways( real( nfo.eig{i} ) < -opt.epsilon(i), 'Unknown','false' ) );
        end;
end
function [ nfo ] = hyperbolic_( M, nfo, opt );
    if( isfield(nfo, 'hyperbolic') );
        return; end;
    nfo = square_( M, nfo, opt );
    nfo = finite_( M, nfo, opt );
    nfo.hyperbolic = false( size(M) );
    for i = 1:numel( M );
        [sz1, sz2] = size( M{i} );
        sz1 = ceil( sz1/2 );
        sz2 = floor( sz2/2 );
        J = blkdiag( eye(sz1), -eye(sz2) );
        if( nfo.square(i) && nfo.finite(i) );
            nrm = norm( M{i}' * J * M{i} - J, 1 );
            nfo.hyperbolic(i) = isAlways( nrm <= opt.epsilon(i), 'Unknown','false' ); end; end;
end
function [ nfo ] = identity_( M, nfo, opt );  %#ok<INUSD>
    if( isfield(nfo, 'identity') );
        return; end;
    nfo.identity = false( size(M) );
    for i = 1:numel( M );
        nfo.identity(i) = isequal( eye(size(M{i})), M{i} ); end;
end
function [ nfo ] = int_( M, nfo, opt );
    if( isfield(nfo, 'int') );
        return; end;
    nfo = finite_( M, nfo, opt ); 
    nfo = real_( M, nfo, opt );
    nfo.int = nfo.finite & ...
               nfo.real & ...
               cellfun( @(x) allm( isAlways(iswholenumber(x), opt.unknown{:}) ), M );
end
function [ nfo ] = inv_( M, nfo, opt );
    if( isfield(nfo, 'inv') );
        return; end;
    nfo = square_( M, nfo, opt );
    nfo = finite_( M, nfo, opt ); 
    nfo.inv = false( size(M) );
    for i = 1:numel( M );
        if( nfo.square(i) && nfo.finite(i) );
            try;
                nfo.inv(i) = rcond_shim( M{i} ) > opt.epsilon(i);
            catch me;
                warning( me.identifier, '%s', me.message ); end; end; end;
end
function [ nfo ] = irreducible_( M, nfo, opt );
    if( isfield(nfo, 'irreducible') );
        return; end;
    nfo = size_( M, nfo, opt );
    nfo = square_( M, nfo, opt );
    nfo = finite_( M, nfo, opt );
    if( nfo.size_same && all(nfo.square(:)) && all(nfo.finite(:)) );
        MM = invariantsubspace( M, 'all', 'v',opt.verbose );
        nfo.irreducible = numel( MM ) == 1;
    else;
        nfo.irreducible = false; end;
end
function [ nfo ] = incidence_( M, nfo, opt );
    if( isfield(nfo, 'incidence') );
        return; end;
    nfo.incidence = cell( size(M) );
    nfo = square_( M, nfo, opt );
    nfo = adjacency_( M, nfo, opt );
    for i = 1:numel( M );
        if( ~nfo.square(i) );
            continue; end;
        A = nfo.adjacency{i};
        nfo.incidence{i} = incidence( digraph( A .* ~eye(size(A)) ) ); end;
end
function [ nfo ] = involutory_( M, nfo, opt );
    if( isfield(nfo, 'involutory') );
        return; end;
    nfo = square_( M, nfo, opt );
    nfo = finite_( M, nfo, opt );
    nfo = unitary_( M, nfo, opt );
    nfo.involutory = false( size(M) );
    for i = 1:numel( M );
        if( nfo.square(i) && nfo.finite(i) );
            try;
                nfo.involutory(i) = isAlways( norm( M{i}*M{i} - eye(size(M{i})) ) < opt.epsilon(i), opt.unknown{:} );
            catch me;
                warning( me.identifier, '%s', me.message ); end; end; end;
end
function [ nfo ] = jordan_( M, nfo, opt );
    if( isfield(nfo, 'jordan') );
        return; end;
    clean = bprintf( '[jordan]' );  %#ok<NASGU>
    nfo.jordan = cell( size(M) );
    nfo = finite_( M, nfo, opt );
    if( ~ismatlab );
        return; end;
    nfo = square_( M, nfo, opt );
    for j = 1:numel( M );
        if( nfo.square(j) && nfo.finite(j) );
            nfo.jordan{j} = jordan( M{j} ); end; end;
end
function [ nfo ] = kone_( M, nfo, opt );
    if( isfield(nfo, 'kone') );
        return; end;
    nfo.kone = false( size(M) );
    nfo = finite_( M, nfo, opt );
    for j = 1:numel( M );
        if( nfo.finite(j) );
            nfo.kone(j) = iskone( M{j} ); end; end;
end
function [ nfo ] = l_( M, nfo, opt );
    if( isfield(nfo, 'l') );
        return; end;
    nfo.l = false( size(M) );
    for i = 1:numel( M );
        mask = eye( size(M{i}) );
        d = diag( M{i} );
        od = M{i}.*mask;
        nfo.l(i) = all( isAlways( d(:) > 0, opt.unknown{:} ) ) && ...
                    all( isAlways( od(:) <= 0, opt.unknown{:} ) );
        end;
end
function [ nfo ] = laplacian_( M, nfo, opt );
    if( isfield(nfo, 'laplacian') );
        return; end;
    nfo = square_( M, nfo, opt );
    nfo = int_( M, nfo, opt );
    nfo.laplacian = false( size(M) );
    for i = 1:numel( M );
        mask = eye( size(M{i}) );
        Mo = M{i}.*mask;
        nfo.laplacian(i) = allm( isAlways( diag( M{i} ) >= 0, opt.unknown{:} ) ) && ...
                            all( isAlways( Mo(:) <= 0, opt.unknown{:} ) );
        end;
end
function [ nfo ] = le_( M, nfo, opt );
    if( isfield(nfo, 'le') );
        return; end;
    nfo.le = false( numel(M), numel(M) );
    for i = 1:numel( M );
        for j = 1:numel( M );
            nfo.le(i,j) = isequal( size(M{i}), size(M{j}) ) && ...
                           allm( isAlways( M{i} <= M{j}, opt.unknown{:} ) ); end; end;
end
function [ nfo ] = leidentity_( M, nfo, opt );
    if( isfield(nfo, 'leidentity') );
        return; end;
    nfo.leidentity = false( size(M) );
    for i = 1:numel( M );
        nfo.leidentity(i) = allm( isAlways( M{i} <= eye(size(M{i})), opt.unknown{:} ) ); end;
end
function [ nfo ] = lognorm_( M, nfo, opt );  %#ok<INUSD>
    if( isfield(nfo, 'lognorm') );
        return; end;
    if( any( cellfun( @issym, M ) ) );
        nfo.lognorm = sym( false( size(M) ) );
    else;
        nfo.lognorm = zeros( size(M) ); end;
    for i = 1:numel( M );
        try;
            err = lasterror();  %#ok<LERR>
            nfo.lognorm(i) = max( eig( (M{i}+M{i}')/2 ) );
        catch me;  %#ok<NASGU>
            lasterror( err );  %#ok<LERR>
            nfo.lognorm(i) = nan; end; end;
end
function [ nfo ] = lt_( M, nfo, opt );
    if( isfield(nfo, 'lt') );
        return; end;
    nfo.lt = false( numel(M), numel(M) );
    for i = 1:numel( M );
        for j = 1:numel( M );
            nfo.lt(i,j) = isequal( size(M{i}), size(M{j}) ) && ...
                         allm( isAlways( M{i} <= M{j}, opt.unknown{:} ) ) && ...
                         anym( isAlways( M{i} <  M{j}, opt.unknown{:} ) ); end; end;
end
function [ nfo ] = m_( M, nfo, opt );
    if( isfield(nfo, 'm') );
        return; end;
    nfo = square_( M , nfo, opt );
    nfo = z_( M, nfo, opt );
    nfo = eig_( M, nfo, opt );
    nfo.m = false( size(M) );
    for i = 1:numel( M );
        if( ~nfo.square(i) || ~nfo.z(i) );
            continue; end;
        nfo.m(i) = allm( isAlways( real(nfo.eig{i}) >= 0, opt.unknown{:} ) ); end;
end
function [ nfo ] = metzler_( M, nfo, opt );
    if( isfield(nfo, 'metzler') );
        return; end;
    nfo.metzler = false( size(M) );
    for i = 1:numel( M );
        Mi = M{i} .* double( ~eye( size(M{i}) ) );
        nfo.metzler(i) = allm( isAlways( Mi >= 0, opt.unknown{:} ) ); end;
end
function [ nfo ] = minors_( M, nfo, opt );
    if( isfield(nfo, 'minors') );
        return; end;
    clean = bprintf( '[minors]' );  %#ok<NASGU>
    nfo = square_( M, nfo, opt );
    nfo.minors = cell( size(M) );
    for i = 1:numel( M );
        if( nfo.square(i) );
            nfo.minors{i} = minors( M{i} ); end; end;
end
function [ nfo ] = minpoly_( M, nfo, opt );
    if( isfield(nfo, 'minpoly') );
        return; end;
    nfo.minpoly = cell( size(M) );
    nfo = square_( M, nfo, opt );
    for j = 1:numel( M );
        if( nfo.square(j) );
            if( isempty(M{j}) );
                nfo.minpoly = [];
            else;
                try;
                    err = lasterror();  %#ok<LERR>
                    nfo.minpoly{j} = minpoly( M{j} );
                catch me;  %#ok<NASGU>
                    lasterror( err );  %#ok<LERR>
                    nfo.minpoly{j} = nan; end; end; end; end;
end
function [ nfo ] = moore_( M, nfo, opt );  %#ok<INUSD>
    if( isfield(nfo, 'moore') );
        return; end;
    nfo.moore = false( size(M) );
    for i = 1:numel( M );
        nfo.moore(i) = ismoore( M{i} ); end;
end
function [ nfo ] = n_( M, nfo, opt );  %#ok<INUSD>
    if( isfield(nfo, 'n') );
        return; end;
    nfo.n = numel( M );
end
function [ nfo ] = nan_( M, nfo, opt );  %#ok<INUSD>
    if( isfield(nfo, 'nan') );
        return; end;
    nfo.nan = cellfun( @(x) anym(isnan(x)), M );
end
function [ nfo ] = neg_( M, nfo, opt );
    if( isfield(nfo, 'neg') );
        return; end;
    nfo = real_( M, nfo, opt ); 
    nfo.neg = nfo.real & ...
               cellfun( @(x) allm(isAlways(x < 0, opt.unknown{:})), M ); 
end
function [ nfo ] = negdef_( M, nfo, opt );
    if( isfield(nfo, 'negdef') );
        return; end;
    nfo = square_( M, nfo, opt );
    nfo = eig_( M, nfo, opt );
    nfo.negdef = false( size(M) );
    for i = 1:numel( M );
        if( nfo.square(i) )
            nfo.negdef(i) = allm( isAlways(nfo.eig{i} < opt.epsilon(i), opt.unknown{:}) ); end; end;
end  
function [ nfo ] = negsemidef_( M, nfo, opt );
    if( isfield(nfo, 'negsemidef') );
        return; end;
    nfo = square_( M, nfo, opt );
    nfo = eig_( M, nfo, opt );
    nfo.negsemidef = false( size(M) );
    for i = 1:numel( M );
        if( nfo.square(i) )
            nfo.negsemidef(i) = allm( isAlways(nfo.eig{i} <= 0, opt.unknown{:}) ); end; end;
end
function [ nfo ] = nonneg_( M, nfo, opt );
    if( isfield(nfo, 'nonneg') );
        return; end;
    nfo = real_( M, nfo, opt );
    nfo.nonneg = false( size(M) );
    for i = 1:numel( M );
        nfo.nonneg(i) = nfo.real(i) & ...
                         allm( isAlways(M{i} >= 0, opt.unknown{:}) ); end;
end
function [ nfo ] = nonpos_( M, nfo, opt );
    if( isfield(nfo, 'nonpos') );
        return; end;
    nfo = real_( M, nfo, opt );
    nfo.nonpos = false( size(M) );
    for i = 1:numel( M );
        nfo.nonpos(i) = nfo.real(i) & ...
                         allm( isAlways(M{i} <= 0, opt.unknown{:}) ); end;
end
function [ nfo ] = numcomp_( M, nfo, opt );
    if( isfield(nfo, 'numcomp') );
        return; end;
    nfo.numcomp = zeros( size(M) );
    nfo = square_( M, nfo, opt );
    nfo = adjacency_( M, nfo, opt );
    for i = 1:numel( M );
        if( ~nfo.square(i) );
            continue; end;
        comp = conncomp( digraph( nfo.adjacency{i} ) );
        nfo.numcomp(i) = numel( unique(comp) ); end;
end
function [ nfo ] = orthogonal_( M, nfo, opt );
    if( isfield(nfo, 'orthogonal') );
        return; end;
    nfo.orthogonal = false( size(M) );
    nfo = square_( M, nfo, opt );
    nfo = unitary_( M, nfo, opt );
    nfo = real_( M, nfo, opt );
    for i = 1:numel( M );
        nfo.orthogonal(i) = nfo.unitary(i) && nfo.real(i) && ...
                            isAlways( abs( sum( M{i}'.*M{i}, 1 ) - 1 ) <= opt.epsilon(i), 'Unknown','false' );
        end;
end
function [ nfo ] = orthonormal_( M, nfo, opt );
    if( isfield(nfo, 'orthonormal') );
        return; end;
    nfo.orthonormal = false( size(M) );
    nfo = orthogonal_( M, nfo, opt );
    for i = 1:numel( M );
        nfo.orthonormal(i) = nfo.orthogonal(i) && ...
                             all( isAlways( abs( sum( M{i}.^2, 1 ) - 1 ) <= opt.epsilon(i), 'Unknown','false' ) );
        end;
end
function [ nfo ] = nonzero_( M, nfo, opt );
    if( isfield(nfo, 'nonzero') );
        return; end;
    nfo.nonzero = false( size(M) );
    for i = 1:numel( M );
        nfo.nonzero(i) = isAlways( all(M{i}(:) ~= 0), opt.unknown{:} ); end;
end
function [ nfo ] = normal_( M, nfo, opt );
    if( isfield(nfo, 'normal') );
        return; end;
    nfo = square_( M, nfo, opt );
    nfo.normal = false( size(M) );
    for i = 1:numel( M );
        if( nfo.square(i) );
            nfo.normal(i) = isAlways( norm(M{i}*M{i}'-M{i}'*M{i}, 1) < opt.epsilon(i), opt.unknown{:} ); end; end;
end
function [ nfo ] = nilpotent_( M, nfo, opt );
    if( isfield(nfo, 'nilpotent') );
        return; end;
    clean = bprintf( '[nilpotent]' );  %#ok<NASGU>
    nfo = square_( M, nfo, opt );
    nfo.nilpotent = false( size(M) );
    for i = 1:numel( M );
        if( nfo.square(i) );
            I = eye( size(M{i}) );
            S = I;
            Mo = I;
            for N = 1:size( M{i}, 1 )
                Mo = M{i}*Mo;
                S = S + Mo; end;
            nrm = norm( S*(I-M{i})-I, 1 );
            check = isAlways( nrm < opt.epsilon(i), opt.unknown{:} );
            nfo.nilpotent(i) = check; end; end;
end
function [ nfo ] = norm_( M, nfo, opt );  %#ok<INUSD>
    if( isfield(nfo, 'norm') );
        return; end;
    if( any( cellfun( @issym, M ) ) );
        nfo.norm = sym( zeros( size(M) ) );
    else;
        nfo.norm = zeros( size(M) ); end;
    for i = 1:numel( M );
        nfo.norm(i) = norm( M{i} ); end;
end
function [ nfo ] = pauli_( M, nfo, opt );
    if( isfield(nfo, 'pauli') );
        return; end;
    nfo = square_( M, nfo, opt );
    nfo.pauli = false( size(M) );
    for i = 1:numel( M );
        if( ~nfo.square(i) );
            continue; end;
        if( ~isequal(size(M{i}), [2 2]) );
            continue; end;
        nfo.pauli(i) = isAlways( norm( M{i} - eye(2) ) <= opt.epsilon(i) ) || ...
                       isAlways( norm( M{i} - [0 1;1 0] ) <= opt.epsilon(i) ) || ...
                       isAlways( norm( M{i} - [0 -1i;1i 0] ) <= opt.epsilon(i) ) || ...
                       isAlways( norm( M{i} - [1 0;0 -1] ) <= opt.epsilon(i) );
        end;
end
function [ nfo ] = permutation_( M, nfo, opt );  %#ok<INUSD>
    if( isfield(nfo, 'permutation') );
        return; end;
    nfo.permutation = false( size(M) );
    for i = 1:numel( M );
        nfo.permutation(i) = ispermutationmatrix( M{i} ); end;
end
function [ nfo ] = persymmetric_( M, nfo, opt );  %#ok<INUSD>
    if( isfield(nfo, 'persymmetric_') );
        return; end;
    nfo.persymmetric = false( size( M ) );
    for i = 1:numel( M );
        nfo.persymmetric(i) = isequal( M{i}, fliplr(M{i}.')); end;
end
function [ nfo ] = pm1_( M, nfo, opt );
    if( isfield(nfo, 'pm1') );
        return; end;
    nfo.pm1 = false( size( M ) );
    for i = 1:numel( M );
        nfo.pm1(i) = all( isAlways( M{i}(:) ==  1, opt.unknown{:}) | ...
                          isAlways( M{i}(:) == -1, opt.unknown{:}) ); end;
end
function [ nfo ] = pos_( M, nfo, opt );
    if( isfield(nfo, 'pos') );
        return; end;
    nfo = real_( M, nfo, opt ); 
    nfo.pos = false( size( M ) );
    for i = 1:numel( M );
        nfo.pos(i) = nfo.real(i) && ...
                     all( isAlways( M{i}(:) > 0, opt.unknown{:} ) ); end;
end
function [ nfo ] = posdef_( M, nfo, opt );
    if( isfield(nfo, 'posdef') );
        return; end;
    nfo = square_( M, nfo, opt );
    nfo = eig_( M, nfo, opt );
    nfo.posdef = false( size(M) );
    for i = 1:numel( M );
        if( nfo.square(i) )
            nfo.posdef(i) = allm( isAlways(nfo.eig{i} > opt.epsilon(i), opt.unknown{:}) ); end; end;
end  
function [ nfo ] = possemidef_( M, nfo, opt );
    if( isfield(nfo, 'possemidef') );
        return; end;
    nfo = square_( M, nfo, opt );
    nfo = eig_( M, nfo, opt );
    nfo.possemidef = false( size(M) );
    for i = 1:numel( M );
        if( nfo.square(i) )
            nfo.possemidef(i) = allm( isAlways(nfo.eig{i} >= 0, opt.unknown{:}) ); end; end;
end
function [ nfo ] = primitive_( M, nfo, opt );
    if( isfield(nfo, 'primitive') );
        return; end;
    nfo = square_( M, nfo, opt );
    nfo = nonneg_( M, nfo, opt );
    nfo = adjacency_( M, nfo, opt );
    nfo.primitive = false( size(M) );
    for i = 1:numel( M );
        if( nfo.square(i) && nfo.nonneg(i) );
            Mo = nfo.adjacency{ i };
            Mi = eye( size(Mo) );
            dimi = size( M{i}, 1 );
            n_max = dimi^2 - 2*dimi + 2;
            for n = 1:n_max;
                Mi = Mi * Mo;
                Mi = Mi > 0;
                if( all( Mi(:) ) );
                    nfo.primitive(i) = true;
                    break; end; end; end; end;
end 

function [ nfo ] = projection_( M, nfo, opt );
    if( isfield(nfo, 'projection') );
        return; end;
    nfo = square_( M, nfo, opt );
    nfo.projection = false( size(M) );
    for i = 1:numel( M );
        if( nfo.square(i) );
            nfo.projection(i) = allm( isAlways(abs(M{i}*M{i} - M{i}) <= opt.epsilon(i), opt.unknown{:}) ); end; end;
end 
function [ nfo ] = poslead_( M, nfo, opt );
    if( isfield(nfo, 'poslead') );
        return; end;
    nfo = square_( M, nfo, opt );
    nfo = eig_( M, nfo, opt );
    nfo.poslead = false( size(M) );
    for i = 1:numel( M );
        if( nfo.square(i) );
            lev = leadingeigenvalue( nfo.eig{i} );
            is_real_lev = all( isAlways( imag(lev(:)) == 0, opt.unknown{:} ) );
            if( is_real_lev && allm( isAlways(lev > 0) ) );
                nfo.poslead(i) = true; end; end; end;
end
function [ nfo ] = rank_( M, nfo, opt );
    if( isfield(nfo, 'rank') );
        return; end;
    nfo.rank = zeros( size(M) );
    nfo = finite_( M, nfo, opt );
    for i = 1:numel( M );
        if( nfo.finite(i) );
            nfo.rank(i) = rank( M{i} ); end; end;
end
function [ nfo ] = rat_( M, nfo, opt );
    if( isfield(nfo, 'rat') );
        return; end;
    nfo = sym_( M, nfo, opt );
    nfo.rat = false( size(M) );
    for i = 1:numel( M );
        if( nfo.sym(i) );
            [nom, denom] = numden( M{i} );
        else;
            [nom, denom] = rat( M{i} ); end;
        nom = round( nom );
        denom = round( denom );
        val = abs( nom./denom - M{i} );
        nfo.rat(i) = isAlways( sum( val(:) ) <= opt.epsilon(i), opt.unknown{:} ); end;
end
function [ nfo ] = rho_( M, nfo, opt );
    if( isfield(nfo, 'rho') );
        return; end;
    nfo = eig_( M, nfo, opt );
    if( any( cellfun( @issym, M ) ) );
        nfo.rho = sym( zeros( size(M) ) );
    else;
        nfo.rho = zeros( size(M) ); end;
    for i = 1:numel( M );
        try;
            err = lasterror();  %#ok<LERR>
            nfo.rho(i) = max( abs( nfo.eig{i} ) );
        catch me;  %#ok<NASGU>
            lasterror( err );  %#ok<LERR>
            nfo.rho(i) = nan; end; end;
end
function [ nfo ] = rho_laplacian_( M, nfo, opt );  %#ok<INUSD>
    if( isfield(nfo, 'rho_laplacian') );
        return; end;
    if( any( cellfun( @issym, M ) ) );
        nfo.rho_laplacian = sym( zeros( size(M) ) );
    else;
        nfo.rho_laplacian = zeros( size(M) ); end;
    for i = 1:numel( M );
        try;
            err = lasterror();  %#ok<LERR>
            in_degree = diag( sum( M{i}, 1 ) );
            Mi = in_degree - M{i};
            nfo.rho_laplacian(i) = rho( Mi );
        catch me;  %#ok<NASGU>
            lasterror( err );  %#ok<LERR>
            nfo.rho_laplacian(i) = nan; end; end;
end
function [ nfo ] = signature_( M, nfo, opt );
    if( isfield(nfo, 'signature') );
        return; end;
    nfo.signature = false( size(M) );
    for i = 1:numel( M );
        nfo.signature(i) = allm( isAlways( abs(M{i}.^2 - eye(size(M{i}))) <= opt.epsilon(i), opt.unknown{:} ) ); end;
end

function [ nfo ] = singular_( M, nfo, opt );
    if( isfield(nfo, 'singular') );
        return; end;
    nfo = inv_( M, nfo, opt );
    nfo.singular = false( size(M) );
    for i = 1:numel( M );
        nfo.singular(i) = ~nfo.inv(i); end;
end
function [ nfo ] = real_( M, nfo, opt );
    if( isfield(nfo, 'real') );
        return; end;
    nfo.real = false( size(M) );
    for i = 1:numel( M );
        res = isAlways( imag(M{i}) == 0, opt.unknown{:} );
        nfo.real(i) = all( res(:) ); end;  % isreal does not work for symbolic inputs
end
function [ nfo ] = semidef_( M, nfo, opt );
    if( isfield(nfo, 'semidef') );
        return; end;
    nfo.semidef = false( size(M) );
    nfo = possemidef_( M, nfo, opt );
    nfo = negsemidef_( M, nfo, opt );
    for i = 1:numel( M );        
        nfo.semidef(i) = nfo.possemidef(i) || nfo.negsemidef(i); end;
end
function [ nfo ] = size_( M, nfo, opt );  %#ok<INUSD>
    if( isfield(nfo, 'size') );
        return; end;
    nfo.size = cellfun( @size,M, 'UniformOutput',false );     
    val = cellfun(@(x) isequal(x, nfo.size{1}), nfo.size );
    nfo.size_same = all( val(:) ) && 1 || 0;
end
function [ nfo ] = slpcandidate_( M, nfo, opt );
    if( isfield(nfo, 'slpcandidate') );
        return; end;
    clean = bprintf( '[slpcandidate]' );  %#ok<NASGU>
    nfo = square_( M, nfo, opt );
    nfo.slpcandidate = {};
    if( all( nfo.square(:) ) && ...
        numel(nfo.size) >= 2 && isequal( nfo.size{:} ) ...
      );
        nfo.slpcandidate = findsmp( M, 'min', 'nearlycandidate',1, 'verbose',0 ); end;
end
function [ nfo ] = smith_( M, nfo, opt );
    if( isfield(nfo, 'smith') );
        return; end;
    nfo = square_( M, nfo, opt );
    nfo = int_( M, nfo, opt );
    nfo = singular_( M, nfo, opt );
    nfo.smith = cell( size(M) );
    for i = 1:numel( M );
        if( nfo.square(i) && nfo.int(i) && ~nfo.singular(i) );
            nfo.smith{i} = diag( smithForm( M{i} ) ); end; end;
end
function [ nfo ] = smpcandidate_( M, nfo, opt );
    if( isfield(nfo, 'smpcandidate') );
        return; end;
    clean = bprintf( '[smpcandidate]' );  %#ok<NASGU>
    nfo = square_( M, nfo, opt );
    nfo.smpcandidate = {};
    if( all( nfo.square(:) ) && ...
        numel(nfo.size) >= 2 && isequal( nfo.size{:} ) ...
      );
        nfo.smpcandidate = findsmp( M, 'nearlycandidate',1, 'verbose',0 ); end;
end
function [ nfo ] = sparsity_( M, nfo, opt );  %#ok<INUSD>
    if( isfield(nfo, 'sparsity') );
        return; end;
    ne = cellfun( @numel, M );
    nz = ne - cellfun( @nnz, M );
    nfo.sparsity = 1 - (ne - nz)./ne;
    nfo.sparsity_all = 1 - sum( ne(:) - nz(:) )./sum( ne(:) );
end    
function [ nfo ] = square_( M, nfo, opt );  %#ok<INUSD>
    if( isfield(nfo, 'square') );
        return; end;
    nfo.square = cellfun( @(x) issquare(x), M );
end
function [ nfo ] = stieltjes_( M, nfo, opt );
    if( isfield(nfo, 'stieltjes') );
        return; end;
    nfo = real_( M, nfo, opt );
    nfo = symmetric_( M, nfo, opt );
    nfo = posdef_( M, nfo, opt );
    nfo = m_( M, nfo, opt );
    nfo.stieltjes = false( size(M) );
    for i = 1:numel( M );
        nfo.stieltjes(i) = nfo.real(i) && nfo.symmetric(i) && nfo.posdef(i) && nfo.m(i); end;
end

function [ nfo ] = stochastic_column_( M, nfo, opt );
    if( isfield(nfo, 'stochastic_column') );
        return; end;
    nfo = square_( M, nfo, opt );
    nfo = nonneg_( M, nfo, opt );
    check3 = false( size(M) );
    for i = 1:numel( check3 ) ;
        check3(i) = allm( isAlways(norm( sum(M{i}, 1) - ones(1, size(M{i}, 2)) ) < opt.epsilon(i), opt.unknown{:}) ); end;
    nfo.stochastic_column = nfo.square & nfo.nonneg & check3;
end
function [ nfo ] = stochastic_row_( M, nfo, opt );
    if( isfield(nfo, 'stochastic_row') );
        return; end;
    nfo = nonneg_( M, nfo, opt );
    nfo = square_( M, nfo, opt );
    check3 = false( size(M) );
    for i = 1:numel( check3 );
        check3(i) = allm( isAlways(norm(sum(M{i}, 2)-ones(size(M{i}, 1), 1)) < opt.epsilon(i), opt.unknown{:}) ); end;
    nfo.stochastic_row = nfo.square & nfo.nonneg & check3;
end
function [ nfo ] = stochastic_double_( M, nfo, opt );
    if( isfield(nfo, 'stochastic_double') );
        return; end;
    nfo.stochastic_double = false( size(M) );
    nfo = stochastic_column_( M, nfo, opt );
    nfo = stochastic_row_( M, nfo, opt );
    nfo.stochastic_double = nfo.stochastic_row & nfo.stochastic_column;
end
function [ nfo ] = svd_( M, nfo, opt );
    if( isfield(nfo, 'svd') );
        return; end;
    nfo.svd = cell( size(M) );
    nfo = finite_( M, nfo, opt );
    for i = 1:numel( M );
        if( nfo.finite(i) );
            nfo.svd{i} = svd( M{i} ); end; end;
end
function [ nfo ] = sym_( M, nfo, opt );  %#ok<INUSD>
    if( isfield(nfo, 'sym') );
        return; end;
    nfo.sym = cellfun( 'isclass', M, 'sym' );
end
function [ nfo ] = symmetric_( M, nfo, opt );  %#ok<INUSD>
    if( isfield(nfo, 'symmetric') );
        return; end;
    nfo.symmetric =  cellfun( @(x) isequal(x, x.'), M );
end
function [ nfo ] = symmetric_skew_( M, nfo, opt );  %#ok<INUSD>
    if( isfield(nfo, 'symmetric_skew') );
        return; end;
    nfo.symmetric_skew = cellfun( @(x) isequal(x, -x.'), M );
end
function [ nfo ] = symstrict_( M, nfo, opt );
    if( isfield(nfo, 'symstrict') );
        return; end;
    nfo.symstrict = false( size(M) );
    nfo = vpa_( M, nfo, opt );
    for i = 1:numel( M );
        nfo.symstrict(i) = allm( issymstrict( M{i} ) ); end;
end  
function [ nfo ] = ternary_( M, nfo, opt );
    if( isfield(nfo, 'ternary') );
        return; end;
    nfo.ternary = false( size(M) );
    for i = 1:numel( M );
        nfo.ternary(i) = allm( isAlways(M{i} == -1, opt.unknown{:}) | ...
                                isAlways(M{i} == 0, opt.unknown{:}) | ...
                                isAlways(M{i} == 1, opt.unknown{:}) ); end;
end
function [ nfo ] = toeplitz_( M, nfo, opt );
    if( isfield(nfo, 'toeplitz') );
        return; end;
    nfo.toeplitz = false( size(M) );
    for i = 1:numel( M );
        nfo.toeplitz(i) = istoeplitz( M{i}, opt.epsilon(i) ); end;
end
function [ nfo ] = totallypos_( M, nfo, opt );
    if( isfield(nfo, 'totallypos') );
        return; end;
    nfo = square_( M, nfo, opt );
    nfo.totallypos = false( size(M) );
    nfo = minors_( M, nfo, opt );
    for i = 1:numel( M );
        if( nfo.square(i) );
            nfo.totallypos(i) = all( nfo.minors{i} > opt.epsilon(i) ); end; end;
end
function [ nfo ] = trace_( M, nfo, opt );
    if( isfield(nfo, 'trace') );
        return; end;
    nfo = square_( M, nfo, opt );
    nfo.trace = nan( size(M) );
    if( any(nfo.sym(:)) );
        nfo.trace = sym( nfo.trace ); end;
    for i = 1:numel( M );
        if( nfo.square(i) );
            nfo.trace(i) = trace( M{i} ); end; end;
end
function [ nfo ] = traceless_( M, nfo, opt );
    if( isfield(nfo, 'traceless') );
        return; end;
    nfo = trace_( M, nfo, opt );
    nfo.traceless = nan( size(M) );
    for i = 1:numel( M );
        nfo.traceless(i) = isAlways( nfo.trace(i) <= opt.epsilon(i), opt.unknown{:} ) ; end;
end
function [ nfo ] = triangular_( M, nfo, opt );
    if( isfield(nfo, 'triangular') );
        return; end;
    nfo = tril_( M, nfo, opt );
    nfo = triu_( M, nfo, opt );
    nfo.triangular = false( size(M) );
    for i = 1:numel( M );
        nfo.triangular(i) = nfo.triu(i) || nfo.tril(i); end;
end
function [ nfo ] = tridiag_( M, nfo, opt );
    if( isfield(nfo, 'tridiag') );
        return; end;
    nfo.tridiag = false( size(M) );
    for i = 1:numel( M );
        nfo.tridiag(i) = isAlways( norm( tril(triu(M{i}, -1),1) - M{i}, 1 ) <= opt.epsilon(i) * max(size(M{i})), opt.unknown{:} ); end; 
end
function [ nfo ] = tril_( M, nfo, opt );  %#ok<INUSD>
    if( isfield(nfo, 'tril') );
        return; end;
    nfo.tril = cellfun( @(x) isequal(tril(x,0), x), M );
end    
function [ nfo ] = triu_( M, nfo, opt );  %#ok<INUSD>
    if( isfield(nfo, 'triu') );
        return; end;
    nfo.triu = cellfun( @(x) isequal(triu(x,0), x), M );
end
function [ nfo ] = unimodular_( M, nfo, opt );
    if( isfield(nfo, 'unimodular') );
        return; end;
    nfo = det_( M, nfo, opt );
    nfo.unimodular = zeros( size(M) );
    for i = 1:numel( M );
        nfo.unimodular(i) = isAlways( abs( nfo.det(i) - 1 ) < opt.epsilon(i), opt.unknown{:} ); end;
end
function [ nfo ] = unipotent_( M, nfo, opt );
    if( isfield(nfo, 'unipotent') );
        return; end;
    clean = bprintf( '[unipotent]' );  %#ok<NASGU>
    nfo = square_( M, nfo, opt );
    nfo.unipotent = false( size(M) );
    for i = 1:numel( M );
        if( nfo.square(i) );
            U = M{i} - eye( size(M{i}) );
            I = eye( size(U) );
            S = I;
            Uo = I;
            for N = 1:size( U, 1 )
                Uo = U*Uo;
                S = S + Uo; end;
            nrm = norm( S*(I-U)-I, 1 );
            check = isAlways( nrm < opt.epsilon(i), opt.unknown{:} );
            nfo.unipotent(i) = check; end; end;
end
function [ nfo ] = unitary_( M, nfo, opt );
    if( isfield(nfo, 'unitary') );
        return; end;
    nfo = square_( M, nfo, opt );
    nfo.unitary = false( size(M) );
    for i = 1:numel( M );
        if( nfo.square(i) );
            nfo.unitary(i) = ~isempty( M{i} ) && ...
                             isAlways( norm(M{i}*M{i}' - eye(size(M{i})), 1) <= opt.epsilon(i), opt.unknown{:} ); end; end;
end
function [ nfo ] = vandergraft_( M, nfo, opt );
    if( isfield(nfo, 'vandergraft') );
        return; end;
    nfo = eig_( M, nfo, opt );
%     nfo = square_( M, nfo, opt );
    nfo.vandergraft = false( size(M) );
    for i = 1:numel( M );
        if( ~nfo.square(i) );
            continue; end;
        [deg, ~, deg1] = eigdeg( nfo.eig{i}, [], opt.verbose-1 );
        nfo.vandergraft(i) = all( deg <= deg1 ); end;
end
function [ nfo ] = vandermonde_( M, nfo, opt );
    if( isfield(nfo, 'vandermonde') );
        return; end;
    nfo.vandermonde = false( size(M) );
    nfo = square_( M, nfo, opt );
    for i = 1:numel( M );
        if( ~nfo.square(i) );
            continue; end;
        Mi = M{i};
        if( isempty(Mi) );
            continue; end;
        if( numel(Mi) == 1 );
            nfo.vandermonde(i) = isAlways( abs(Mi - 1) < opt.epsilon(i), opt.unknown{:} );
            continue; end;
        check1 = all( isAlways( abs(Mi(:,1) - 1) < opt.epsilon(i), opt.unknown{:} ) );
        if( ~check1 );
            continue; end;
        Mi = Mi(:,2:end);
        sgn = sign( Mi );
        check2 = allm( isAlways(sgn(:,1:2:end) == sgn(:,1), opt.unknown{:} ) );
        if( ~check2 );
            continue; end;
        Mi = abs(Mi).^(1./(1:size(Mi, 2)));
        Mi = Mi./Mi(:,1);
        check3 = all( isAlways( abs(Mi(:,1) - 1) < opt.epsilon(i), opt.unknown{:} ) );
        if( ~check3 );
            continue; end;
        nfo.vandermonde(i) = true; end;
end
function [ nfo ] = vpa_( M, nfo, opt );  %#ok<INUSD>
    if( isfield(nfo, 'vpa') );
        return; end;
    nfo.vpa = false( size(M) );
    for i = 1:numel( M );
        nfo.vpa(i) = any( isvpa( M{i}(:) ) ); end;
end
function [ nfo ] = z_( M, nfo, opt );
    if( isfield(nfo, 'z') );
        return; end;
    nfo.z = false( size(M) );
    for i = 1:numel( M );
        Mi = M{i} .* double( ~eye( size(M{i}) ) );
        nfo.z(i) = allm( isAlways( Mi <= 0, opt.unknown{:} ) ); end;
end
function [ nfo ] = rhozero_( M, nfo, opt );
    if( isfield(nfo, 'rhozero') );
        return; end;
    nfo = square_( M, nfo, opt );
    nfo = eig_( M, nfo, opt );
    nfo = rho_( M, nfo, opt );
    nfo.rhozero = false( size(M) );
    for i = 1:numel( M );
        if( nfo.square(i) )
            nfo.rhozero(i) = isAlways( nfo.rho(i) <= opt.epsilon(i), opt.unknown{:} ); end; end;
end  

%%

function check_prop( M, nfo, opt );  %#ok<INUSD>
    names = fieldnames( nfo );
    
    bool_array = @(name) isempty( nfo.(name) ) || ...
                        isequal( size(nfo.(name)), size(M) ) && islogicalsy( nfo.(name) );

    bool_2array = @(name) isempty( nfo.(name) ) || ...
                         isequal( size(nfo.(name)), [numel(M) numel(M)] ) && islogicalsy( nfo.(name) );

    num_array = @(name) isempty( nfo.(name) ) || ...
                        isequal( size(nfo.(name)), size(M) ) && ...
                        (isnumeric( nfo.(name) ) || issym( nfo.(name) )  );

    bool_scalar = @(name) isscalar( nfo.(name) ) && ...
                          (islogicalsy( nfo.(name) ) || isa( nfo.(name), 'named') );

    int_scalar = @(name) isscalar(nfo.(name)) && ...
                         (iswholenumber( nfo.(name)) || isa( nfo.(name), 'named') );

    name_value_cellarray = @(name) iscell(nfo.(name)) && ...
                           all( cellfun('isclass', nfo.(name)(1:2:end), 'string') | cellfun('isclass', nfo.(name)(1:2:end), 'char') );

    double_scalar = @(name) isscalar(nfo.(name));
    for name_ = names'; name = name_{1};
        switch lower( name );
            case 'name';                expect( ischar(nfo.(name)) );
            case 'has_ellipsoidal_minimal_invariant_kone';  expect( bool_scalar(name) );
            case 'has_polyhedral_minimal_invariant_kone';   expect( bool_scalar(name) );
            case 'has_invariant_kone';              expect( bool_scalar(name) );
            case 'pairs_have_invariant_kone';       expect( bool_scalar(name) );
            case 'triples_have_invariant_kone';     expect( bool_scalar(name) );

            case 'acyclic';             expect( bool_array(name) );
            case 'adjacency';
            case 'altsign';             expect( bool_array(name) );
            case 'antidiag';            expect( bool_array(name) );
            case 'arrowhead';           expect( bool_array(name) );
            case 'bool';                expect( bool_array(name) );
            case 'cauchy';              expect( bool_array(name) );
            case 'charpoly';
            case 'circulant';           expect( bool_array(name) );
            case 'circumference';       expect( num_array(name) );
            case 'cmirror';             expect( bool_2array(name) );
            case 'commute';             expect( bool_2array(name) );
            case 'commuteall';          expect( bool_scalar(name) );
            case 'comparison';
            case 'cond';                expect( num_array(name) );
            case 'condeigvec';          expect( num_array(name) );
            case 'conference';          expect( bool_array(name) );
            case 'conjugate';           expect( bool_2array(name) );
            case 'cycles';
            case 'def';                 expect( bool_array(name) );
            case 'defective';           expect( bool_array(name) );
            case 'diag';                expect( bool_array(name) );
            case 'dirac';               expect( bool_array(name) );
            case 'diagdom';             expect( bool_array(name) );
            case 'det';                 expect( num_array(name) );
            case 'dim';                 expect( num_array(name) );
            case 'dim_same';            check_property( nfo, name, M, 'scalar', 'bool' );
            case 'eig';                 check_property( nfo, name, M, 'col', 'num' );
            case 'eigvec';
            case 'empty';               expect( bool_array(name) );
            case 'equal';               expect( bool_2array(name) );
            case 'epsilon';             expect( double_scalar(name) );
            case 'finite';              expect( bool_array(name) );
            case 'frobenius';           expect( bool_array(name) );
            case 'gellmann';            expect( bool_array(name) );
            case 'girth';               expect( num_array(name) );
            case 'hamiltonian';         expect( bool_array(name) );
            case 'hankel';              expect( bool_array(name) );
            case 'hermiteform';        
            case 'hermitian';           expect( bool_array(name) );
            case 'hermitian_skew';      expect( bool_array(name) );
            case 'hessl';               expect( bool_array(name) );
            case 'hessu';               expect( bool_array(name) );
            case 'householder';         expect( bool_array(name) );
            case 'hurwitz';             expect( bool_array(name) );
            case 'hyperbolic';          expect( bool_array(name) );
            case 'identity';            expect( bool_array(name) );
            case 'incidence';
            case 'info';                expect( name_value_cellarray(name) );
            case 'int';                 expect( bool_array(name) );
            case 'inv_tridiag';
            case 'inv';                 expect( bool_array(name) );
            case 'involutory';          expect( bool_array(name) );
            case 'irreducible';         expect( bool_scalar(name) );
            case 'jordan';
            case 'kone';                expect( bool_array(name) );
            case 'l';                   expect( bool_array(name) );
            case 'laplacian';           expect( bool_array(name) );
            case 'le';                  expect( bool_2array(name) );
            case 'leidentity';          expect( bool_array(name) );
            case 'lognorm';             expect( num_array(name) );
            case 'lt';                  expect( bool_2array(name) );
            case 'k3_irreducible';      expect( bool_scalar(name) );
            case 'm';                   expect( bool_array(name) );
            case 'metzler';             expect( bool_array(name) );
            case 'minpoly';
            case 'minors';
            case 'moore';               expect( bool_array(name) );
            case 'mortal';              expect( bool_scalar(name) );
            case 'n';                   expect( int_scalar(name) );
            case 'nan';                 expect( bool_array(name) );
            case 'negdef';              expect( bool_array(name) );
            case 'negsemidef';          expect( bool_array(name) );
            case 'nilpotent';           expect( bool_array(name) );
            case 'ndims';               expect( num_array(name) );
            case 'ndims_same';          expect( bool_scalar(name) );
            case 'neg';                 expect( bool_array(name) );
            case 'neg_eigenvalues';     expect( bool_array(name) );
            case 'norm';                
            case 'nonpos';              expect( bool_array(name) );
            case 'nonzero';             expect( bool_array(name) );
            case 'normal';              expect( bool_array(name) );
            case 'nonneg';              expect( bool_array(name) );
            case 'numcomp';             expect( num_array(name) );
            case 'orthogonal';          expect( bool_array(name) );
            case 'orthonormal';         expect( bool_array(name) );
            case 'pauli';               expect( bool_array(name) );
            case 'permutation';         expect( bool_array(name) );
            case 'persymmetric';        expect( bool_array(name) );
            case 'pm1';                 expect( bool_array(name) );
            case {'pos','positive'};    expect( bool_array(name) );
            case 'posdef';              expect( bool_array(name) );
            case 'possemidef';          expect( bool_array(name) );
            case 'poslead';             expect( bool_array(name) );
            case 'primitive';           expect( bool_array(name) );
            case 'projection';          expect( bool_array(name) );
            case 'random';              expect( bool_array(name) );
            case 'rank';                expect( num_array(name) );
            case 'rat';                 expect( bool_array(name) );
            case 'real';                expect( bool_array(name) );
            case 'rho';                 expect( num_array(name) );
            case 'rho_laplacian';       expect( num_array(name) );
            case 'rhozero';             expect( bool_array(name) );
            case 'semidef';             expect( bool_array(name) );
            case 'signature';           expect( bool_array(name) );
            case 'singular';            expect( bool_array(name) );
            case 'size';
            case 'size_same';
            case 'slp';  % this shall only be used, if it the s.l.p.s are confirmed to be s.l.p.s. If not, the field .slpcandidate shall be used to indicate s.l.p.s
            case 'slpcandidate';
            case 'smith';
            case 'smp';  % this shall only be used, if it the s.m.p.s are confirmed to be s.m.p.s. If not, the field .smpcandidate shall be used to indicate s.m.p.s
            case 'smpcandidate';
            case 'sparsity';            expect( num_array(name) );
            case 'sparsity_all';        
            case 'square';              expect( bool_array(name) );
            case {'stieltjes'};         expect( bool_array(name) );
            case {'stochastic_column','cstochastic','cstoch'};   expect( bool_array(name) );
            case {'stochastic_double','dstochastic','dstoch'};   expect( bool_array(name) );
            case {'stochastic_row','rstochastic','rstoch'};      expect( bool_array(name) );
            case 'svd';
            case 'sym';                 expect( bool_array(name) );
            case 'symbolic';            expect( bool_array(name) );
            case 'symmetric';           expect( bool_array(name) );
            case 'symmetric_skew';      expect( bool_array(name) );
            case 'symstrict';           expect( bool_array(name) );
            case 'vpa';                 expect( bool_array(name) );
            case 'ternary';             expect( bool_array(name) );
            case 'toeplitz';            expect( bool_array(name) );
            case 'totallypos';          expect( bool_array(name) );
            case 'trace';               expect( num_array(name) );
            case 'traceless';           expect( bool_array(name) );
            case 'triangular';          expect( bool_array(name) );
            case 'tridiag';             expect( bool_array(name) );
            case 'tril';                expect( bool_array(name) );
            case 'triu';                expect( bool_array(name) );
            case 'unimodular';          expect( bool_array(name) );
            case 'unipotent';           expect( bool_array(name) );
            case 'unitary';             expect( bool_array(name) );
            case 'vandergraft';         expect( bool_array(name) );
            case 'vandermonde';         expect( bool_array(name) );
            case 'z';                   expect( bool_array(name) );
            case 'zero';                expect( bool_array(name) );

            otherwise;
                fprintf( 2, 'Non-standard property name occured: %s\n', name ); end;
            end;
end

function [ ret ] = check_property( nfo, name, M, shape, value )
    prop = nfo.(name);
    if( isempty(prop) );
        ret = true;
        return; end;

    prop = tocell( prop );
    shape_test = true;
    value_test = true;
    for i = 1:numel( prop );
        switch shape;
            case 'array';   shape_test = shape_test && (isempty(prop{i}) || isequal( size(prop{i}), size(M) ) );
            case '2array';  shape_test = shape_test && (isempty(prop{i}) || isequal( size(prop{i}), [numel(M) numel(M)] ) );
            case 'scalar';  shape_test = shape_test && (isempty(prop{i}) || isscalar( prop{i} ) );
            case 'col';     shape_test = shape_test && (isempty(prop{i}) || isequal( size(prop{i}), [size(M{i}, 1), 1] ) );
            case 'row';     shape_test = shape_test && (isempty(prop{i}) || isequal( size(prop{i}), [1 size(M{i}, 2)] ) );
            otherwise; fatal_error; end;

        if( isa( prop{i}, 'named' ) );
            continue; end;
        switch value;
            case {'logical','bool'};    value_test = value_test && islogicalsy( prop{i} );
            case 'num';                 value_test = value_test && isnumeric( prop{i} ) ||issym( prop{i} );
            case {'int','integer'};     value_test = value_test && iswholenumber( nfo.(name) );
            case 'name_value';          value_test = value_test && all( cellfun('isclass', nfo.(name)(1:2:end), 'string') | cellfun('isclass', nfo.(name)(1:2:end), 'char') );
            otherwise; fatal_error; end;  end;

    ret = shape_test && value_test;
    if( ~ret );
        fprintf( 2, '\nProperty test failed: %s\n', name );
        if( ~shape_test );
            fprintf( 2, '  Shape wrong.\n' );
        else;
            fprintf( 2, '  Values wrong.\n' ); end; end;

end

function [ varargin ] = compare_nfo( varargin );
    fn = cell( size(varargin) );
    for i = 1:numel( varargin );
        fn{i} = sort( fieldnames( varargin{i} ) ); end;
    fn_all = unique( vertcat( fn{:} ) )';
    for n_ = fn_all; n = n_{1};
        has_field = true;
        for i = 1:numel( varargin );
            if( ~isfield(varargin{i}, n) );
                has_field = false;
                break; end; end;
        if( ~has_field );
            continue; end;
        has_equal_value = true;
        for i = 2:numel( varargin );
            if( ~isequal( varargin{1}.(n), varargin{i}.(n) ) );
                has_equal_value = false;
                break; end; end;
        if( ~has_equal_value );
            continue; end;
        for i = 1:numel( varargin );
            varargin{i} = rmfield( varargin{i}, n ); end;

    end
    
end

function [ nfo ] = compactify( nfo );
    [nfo, cellflag] = tocell( nfo );
    for i = 1:numel( nfo );
        nfo{i} = compactify_worker( nfo{i} ); end;
    if( ~cellflag );
        nfo = nfo{1}; end;
end

function [ nfo ] = compactify_worker( nfo );
% removes properties
% - which are false for all matrices (e.g. if all matrices are not binary matrices, then .binary is removed)
% - which are trivially implied by other properties (e.g. if a matrix is invertible, then .square is removed)

    nfo_orig = nfo;

    if( isempty(nfo) );
        return; end;

    if( isfield(nfo, 'square') && all(~nfo.square) );
        nfo = rmfieldt( nfo, {'jordan','eig','eigvec','smpcandidate','singular'} );
        end;

    nfo = compact_nfo( nfo );

    %remove fields when they are false for all, or empty
    for name_ = fieldnames( nfo )'; name = name_{1};
        x = nfo.(name);
        if( (isnumeric( x ) || islogical( x )) && ~any(x(:)) || ...
            isequal( x, [] ) || isequal( x, {} ) || ...
            iscell( x ) && all( cellfun( 'prodofsize', x ) == 0 ) ...
          );
            nfo = rmfield( nfo, name ); end; end;

    % remove fields which are trivial when they are the identity matrix
    for name_ = {'commute','equal','le'}; name = name_{1};
        if( isfield( nfo, name ) && ...
            isequal( nfo.(name), eye(size(nfo.(name))) ) ...
          );
            nfo = rmfield( nfo, name ); end; end;

    % remove properties which implied by other properties
    nfo = remove_implied2( nfo, nfo_orig, 'real', {'conjugate','cmirror'} );
    nfo = remove_implied2( nfo, nfo_orig, 'unitary', 'orthonormal', 'orthogonal', {'inv','square'}, 'kone' );
    nfo = remove_implied2( nfo, nfo_orig, {'bool','pm1'}, 'ternary', 'int', 'rat', 'real' );
    nfo = remove_implied2( nfo, nfo_orig, {'bool','pm1','ternary'}, {'finite','nonneg'} );
    nfo = remove_implied2( nfo, nfo_orig, 'diag', 'tridiag', 'triu', {'hessu'} );
    nfo = remove_implied2( nfo, nfo_orig, 'diag', 'tridiag', 'tril', {'hessl'} );
    nfo = remove_implied2( nfo, nfo_orig, 'diag', {'symmetric','diagdom'} );
    nfo = remove_implied2( nfo, nfo_orig, 'nilpotent', {'finite','square'} );
    nfo = remove_implied2( nfo, nfo_orig, {'defective','inv','symmetric'}, {'square'} );
    nfo = remove_implied2( nfo, nfo_orig, {'hermitian','defective','inv','symmetric'}, {'square','normal'} );
    nfo = remove_implied2( nfo, nfo_orig, {'defective','inv','symmetric'}, {'square'} );
    nfo = remove_implied2( nfo, nfo_orig, {'vandergraft'}, {'poslead'} );
    nfo = remove_implied2( nfo, nfo_orig, {'posdef'}, {'possemidef'}, {'inv'},{'rank'} );
    nfo = remove_implied2( nfo, nfo_orig, {'negdef'}, {'negsemidef'}, {'inv'},{'rank'} );
    nfo = remove_implied2( nfo, nfo_orig, {'posdef','negdef'}, {'def'}, {'semidef'} );
    nfo = remove_implied2( nfo, nfo_orig, {'possemiodef','negsemidef'}, {'semidef'} );
    nfo = remove_implied2( nfo, nfo_orig, {'posdef','possemidef','negdef','negsemidef','def','semidef'}, {'symmetric'} );
    nfo = remove_implied2( nfo, nfo_orig, {'unimodular'}, {'det'} );
    
    if( isfield( nfo_orig, 'size' ) && max( cellfun( @max, nfo_orig.size ) ) <= 2 );
        nfo = rmfieldt( nfo, 'tridiag' );
        nfo = rmfieldt( nfo, 'triu' );
        nfo = rmfieldt( nfo, 'tril' );
        nfo = rmfieldt( nfo, 'hessu' );
        nfo = rmfieldt( nfo, 'hessl' );
        end;

    % remove properties which are boring if there is only 1 matrix
    if( nfo_orig.n == 1 );
        nfo = rmfieldt( nfo, 'cmirror' );
        nfo = rmfieldt( nfo, 'commute' );
        nfo = rmfieldt( nfo, 'commuteall' );
        nfo = rmfieldt( nfo, 'equal' );
        nfo = rmfieldt( nfo, 'le' );
        nfo = rmfieldt( nfo, 'n' );
        nfo = rmfieldt( nfo, 'ndims_same' );
        nfo = rmfieldt( nfo, 'size_same' );
        nfo = rmfieldt( nfo, 'sparsity_all' );
        end;
        
    % remove properties which need some special logic
    if( isfield( nfo_orig, 'girth' ) && isfield( nfo_orig, 'circumference' ) && isfield( nfo_orig, 'size' ) && isfield( nfo_orig, 'square' ) && isfield( nfo_orig, 'size_same' ) && ...
        all( nfo_orig.square ) && nfo_orig.size_same && ...        
        all( nfo_orig.girth == nfo_orig.girth(1) ) && all( nfo_orig.circumference == nfo_orig.circumference(1) ) &&...
        nfo_orig.girth(1) == 1 && nfo_orig.circumference(1) == nfo_orig.size{1}(1) ...
      );
        nfo = rmfieldt( nfo, 'girth' );
        nfo = rmfieldt( nfo, 'circumference' ); end;
  

end

function [ nfo ] = remove_implied2( nfo, nfo_orig, varargin );
    for i = 1:numel( varargin );
        varargin{i} = tocell( varargin{i} ); end;
    
    for n = 1:numel( varargin );
        for i = 1:numel( varargin{n} );
            if( isfield( nfo_orig, varargin{n}{i} ) && all(nfo_orig.(varargin{n}{i})(:)) );
                for m = n+1:numel( varargin );
                    for j = 1:numel( varargin{m} );
                        nfo = rmfieldt( nfo, varargin{m}{j} ); end; end;
                break; end; end; end;
end

%%

function [ ret ] = rcond_shim( X );
    if( issym(X) );
        c = cond( X, 1 );
        ret = 1/c;
    else;
        ret = rcond( X ); end;
end

function [ ret ] = ismatlab ()
    % returns true when run on m
    persistent x;
    if( isempty (x) );
        x = ~exist( 'OCTAVE_VERSION', 'builtin' ); end;
    ret = x;
end

function [ ret ] = islogicalsy( x );
    ret = islogical( x ) || ...
        isnumeric( x ) && all( x(:) == 0 | x(:) == 1 | isnan(x(:)) );
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
