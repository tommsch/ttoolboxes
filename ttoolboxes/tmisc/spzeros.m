function [ A ] = spzeros( varargin );
% Convienence function generating a sparse zero matrix.
% Has the same interface as zeros()
    if( nargin == 1 );
        A = sparse( varargin{1}, varargin{1} );
    elseif( nargin == 2 );
        A = sparse( varargin{1}, varargin{2} ); end;

end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
