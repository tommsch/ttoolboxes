function [ color ] = num2color( num, seed, brightness );
% [ color ] = num2color( num, seed, brightness )
% Assigns a fixed color to each number (non-portable)
%
% Input:
%   num         vector of integers o
%   seed        (optional) seed for random-number generator.
%               If given, then the function does not change the state of the random-number generator.
%   brightness  (optionaal), default = .5, if 0, black is returned, if 1, white is returned
%
% Output:
%   color       numel(num)x3 array with values in [0,1]
%
% E.g.: num2color(30,2)
%
% Depends on: //
%
% Written by: tommsch, 2017

%            2020-09-14, tommsch, when called with no arguments, a random color is returned
%            2023-03-28, tommsch, num can be a vector of integers
%            2023-05-04, tommsch, Added optional argument `brightness`
%            2024-07-27, tommsch, Behaviour change. Function does not use seed anymore. Each number is assigned to a fixed color (non-portable)
% Changelog: 

    if( nargin <= 0 || isempty(num) );
        num = 8; end;
    if( nargin <= 1 || isempty(seed) );
        seed = []; end;
    assert( isempty(seed), 'num2color:seed', 'Seed is not used anymore in this function.' );
    if( nargin <= 2 || isempty(brightness) );
        brightness = .5; end;

    color = zeros( numel(num), 3 );
    for i = 1:numel( num );
        if( ischar(num) );
            switch num(i);
                case 'r'; color(i,:) = [1 0 0];
                case 'g'; color(i,:) = [0 1 0];
                case 'b'; color(i,:) = [0 0 1];
                case 'c'; color(i,:) = [0 1 1];
                case 'm'; color(i,:) = [1 0 1];
                case 'y'; color(i,:) = [1 1 0];
                case 'k'; color(i,:) = [0 0 0];
                case 'w'; color(i,:) = [1 1 1];
                otherwise;
                    error( 'num2color:input', 'unsupported string given.' ); end;
        else;
            switch num(i);
                case 1; color(i,:) = [1 0 0];
                case 2; color(i,:) = [0 1 0];
                case 3; color(i,:) = [0 0 1];
                case 4; color(i,:) = [1 1 0];
                case 5; color(i,:) = [1 0 1];
                case 6; color(i,:) = [0 1 1];
                case 7; color(i,:) = [0 0 0];
                otherwise;
                    % some hash like function
                    val = double( num(i) );
                    val = real(val) + imag(val);
                    val = int64( typecast( val, 'uint8' ) );
                    val = mod( val * 74207281, 256 );
                    val(1:3)  = mod( val(1:3) + val(7), 256 );
                    val(4:6)  = mod( val(4:6) + val(8), 256 );
                    val = mod( sum( reshape( val, 2, [] ), 1 ), 256 );
                    color(i,:) = val(1:3)/256; end; end; end;

    if( brightness > .5 );
        brightness = 2*brightness - 1;
        color = (1-brightness).*color + [brightness brightness brightness];
    elseif( brightness == .5 );
        % do nothing
    elseif( brightness < .5 );
        brightness = 2*brightness;
        color = brightness.*color;
    end;
    color = clamp( color, 0, 1 );
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
