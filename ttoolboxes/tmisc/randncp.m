function [ A ] = randncp( varargin );

A = randn( varargin{:} ) + 1i*randn( varargin{:} );

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
