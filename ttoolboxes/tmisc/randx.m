function [ R ] = randx( varargin );
% Returns a rational number between -inf and inf
% [ ret ] = randx( [X] );
% Input: 
%   X       vector of integers, size of the output array
%
% Output:
%   R       array of size X with rational values in (-inf inf)
%
% E.g.: randx
%
% Written by: tommsch, 2021-01-19
%
% See also: randp
    if( nargin == 1 );
        X = varargin{1}; 
    else;
        X = [varargin{:}]; end;
    R = (randp(X)-randp(X))./(randp(X)-randp(X));
    idx = ~isfinite( R );
    R(idx) = randp( [1,nnz(idx)] ) - randp( [1,nnz(idx)] );
    
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
