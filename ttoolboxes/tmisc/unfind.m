function [ out ] = unfind( idx, dims );
% [ out ] = unfind( idx, dims );
% returns the nonzero entries of an array, given its linear indices
%
% Input:
%   idx     vector if ints, linear indices
%   dims    vector of integers, dimension of output array
%
% Output:
%   out     logical array of size dim_1 x ... x dim_N
%
% Written by: tommsch, 2024-10-21

% Changelog:    tommsch,    2024-10-21, Totally rewritten
%                                       Better error handling, multi dimension output

    assert( ~islogical(idx) && all(max(idx(:)) <= prod(dims)) && isvector(dims), 'unfind:indexing', 'unfind: The index vector must be a vector of integers smaller or equal to N' );
    out = false( [dims(:); 1]' );
    out(idx) = true;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
