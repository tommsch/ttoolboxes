function [ ret ] = imag2real( M );
% takes a complex valued array, and returns a real valued array of twice the size, by identifying a+i*b = [a -b;b a]
% [ ret ] = realify( M );
%
% Written by: tommsch, 2023-03

%            tommsch, 2023-07-10, Behaviour change: Function name changed to imag2real
% Changelog: 

    sze = size( M );
    ret = zeros( 2*sze );
    for m = 1:sze(1);
        for n = 1:sze(2);
            re = real( M(m, n) );
            im = imag( M(m, n) );
            ret(2*m-1:2*m,2*n-1:2*n) = [re -im;im re]; end; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
