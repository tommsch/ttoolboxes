function [ c, ic ] = smallestchoice( varargin )
% [ c, ic ] = smallestchoice( elements, weights, set_idx, [equalfun], [options] )
% Tries to find an optimal choice of representatives, namely the solution to the following problem:
% Sought is a minimal set of elements such that from each set in C at least on element is chosen,
% and under all such choices c1,...,ck we sought for a choice such that max( v1, ..., vk ) is minimal.
%
% The algorithm used is greedy and does not guarantee the correct solution of the posed problem.
%
% Input:
% ======
%   elements               NxJ matrix of column vector, the elements
%   weights                1xJ row vector, the value (weight) of the elements
%   set_idx                1xJ row vector of positive integers, the set to which an elelement belongs
%   equalfun               determines which values are considered to be equal
%                          Either: handle to function taking two vectors, returning true/false, default: val = @isequal; OR
%                          Or:     double, in which case equalfun = @(x, y) norm( x - y ) < val
%
% Options:
% ========
%   'verbose',val           verbose level
%
% Output:
% =======
%   c                       1xN vector of chosen elements
%   ic                      cell array of linear indices  chosen elements
%                               
% 
% Example:
% ========
%   [c, ic] = smallestchoice( [1 2 2 3 3 4], [2 1 1 1 2 1], [1 1 2 2 3 3] );
%   % Three sets to choose from:     [1 2], [2 3], [3 4]
%   % The corresponding weights are: [2 1], [1 1], [2 1]
%   % The resulting choice is:       [. Y], [Y .], [. Y]  %  c = {2 2 4}, ic = [2 3 6]
%
% Written by: tommsch, 2020
    
    % Constants
    %%%%%%%%%%%%%%%%
    EL = 1;      % elements  % values of EL, VAL, IC are fixed and cannot be changed
    VAL = 2;     % value of element
    IC = 3;      % linear index
    SET = 4;     % original set
    MAXVAL = 5;  % maximum of valfunc of all same elements
    
    
    % Pre-processing
    %%%%%%%%%%%%%%%%%%
    % parse input
    [elements, weights, set_idx, opt] = parse_input( varargin{:} );
    
    if( isempty(elements) );
        c = {};
        ic = [];
        return; end;
    
    % pre-process input
    vprintf( 'Preprocess input.\n', 'imp',[2 opt.verbose] );
    n = size( set_idx, 2 );  % number of all elements
    nS = max( unique(set_idx) );  % number of sets
    C = nan( 1, n );  % array which holds the essential information
    
    % identify equal elements
    id = 1; 
    for i = 1:n 
        if( ~isnan(C(i)) );
            continue; end;
        C(i) = id;
        id = id+1;
        if( isa(opt.equalfun, 'function_handle') );
            idx = [zeros( 1, i ) arrayfun( @(j) opt.equalfun(elements(:,i), elements(:,j)), i+1:n )];
        elseif( isscalar(opt.equalfun) );
            idx = [zeros( 1, i ) arrayfun( @(j) norm(elements(:,i) - elements(:,j)) < opt.equalfun, i+1:n )];
        elseif( size(elements, 1) >= 2 );
            idx = [zeros( 1, i ) ismember( elements(:,i+1:end).', elements(:,i).', 'rows' ).']; 
        else
            C = elements;
            break; end; 
        C(logical( isAlways(idx) )) = C(i); end;
    
    % choose of duplicates best element in each partition
    C = [C; weights; 1:n; set_idx];
    Cout = zeros( size(C, 1), 0 );
    vprintf( 'Choose best elements.\n', 'imp',[2 opt.verbose] );
    for i = unique( set_idx );
        idx_set = i == C(SET,:);
        for j = unique( C(EL,idx_set) );
            idx_el = C(EL,:) == j;
            idx_elsetval = C(VAL,:) == min( C(VAL,idx_el&idx_set) ) & idx_set & idx_el;
            Cout(:,end+1) = C(:,find( idx_elsetval, 1 )); end; end;  %#ok<AGROW>
    C = Cout;
    if( opt.verbose >= 2 );
        C = sortrows( C.', IC ).'; end;  % bring in order, for visualization and debugging reasons
    
    % compute value of elements
    C(MAXVAL,:) = nan;
    for i = 1:size( C, 2 );
        if( ~isnan(C(MAXVAL,i)) );
            continue; end;
        idx = C(EL,i) == C(EL,:);  % find equal elements
        C(MAXVAL,idx) = max( C(VAL,idx) ); end;
    
    % the data we work with
    C_full = C; 
    
    % Main Part
    %%%%%%%%%%%%%%%%%
    % select elements
    chosen = [];  % the selected elements (with values from EL)
    while( ~isempty(C) )
        %occ = sum( C(EL,:) == C(EL,:)', 1 );  % number of occurences %this line is slow if the matrix C is large, thus we use arrayfun instead
        occ = arrayfun( @(x) nnz(C(EL,:) == x), C(EL,:) );  % number of occurences 
        idxocc = occ == max( occ );  % find elements of maximal occurence of not yet selected sets
        idxmincumval = C(MAXVAL,:) == min( C(MAXVAL,idxocc) );  % find elements with minimal value
        idx = find( idxocc & idxmincumval, 1 );
        el = C(EL,idx);  % element with minimal value
        chosen = [chosen el];  %#ok<AGROW>
        idxel = C(EL,:) == el;  % find in which sets the element occurss
        idxset = any( C(SET,:) == C(SET,idxel)', 1 );  % find chosen sets
        C(:,idxset) = []; end;  % remove all sets which are chosen
    vprintf( 'Chosen elements after greedy algorithm:              %r\n', chosen, 'imp',[2 opt.verbose] );
    
    % remove superfluous elements
    C = C_full;
    for i = 1:numel( chosen ) 
        % check if chosen without C1(EL,i) is also a valid choice
        chosen_i = chosen([1:i-1 i+1:end]);
        chosen_i = unique( C( SET, anym(C(EL,:) == chosen_i',1)) );
        if( isequal(chosen_i,1:nS) )
            chosen(i) = nan;  %#ok<AGROW>
            end; end;
    chosen(isnan( chosen )) = [];
    vprintf( 'Chosen elements after removing superfluous elements: %r\n', chosen, 'imp',[2 opt.verbose] );
    
    % Post processing
    %%%%%%%%%%%%%%%%%%%%%%%
    % find indices of chosen values and translate back indices to original values
    %C = C_full  % not necessary since we did not change C in the former step
    c = cell( 1, nS );
    ic = zeros( 1, 0 );
    idx_chosen = anym(C(EL,:) == chosen.',1);
    for i = unique( C(SET,:) );
        idx_set = idx_chosen & C(SET,:) == i;
        val = C(IC,idx_set);
        ic = [ic val];  %#ok<AGROW>
        c{i} = [c{i} elements(:,val)]; end;
    ic = unique( ic );
end

function [ elements, weights, set_idx, opt ] = parse_input( varargin );
    [opt.verbose, varargin]  = parsem( {'verbose','v'}, varargin, 1 );
    
    if( numel(varargin) == 4 );
        opt.equalfun = varargin{4}; 
        varargin(4) = []; 
    else;
        opt.equalfun = []; end;
    
    assert( numel(varargin) == 3 && isequal(round(varargin{3}), varargin{3}), 'smallestchoice:arg', 'Wrong input arguments.' );
    elements = varargin{1};
    weights = varargin{2};
    set_idx = varargin{3};
    assert( isequal( size(elements, 2), size(weights, 2), size(set_idx, 2) ), 'smallestchoice:arg', 'Inputs X, V and S must have the same number of columns.' );
    
    varargin(1:3) = [];
    parsem( varargin, 'test' );
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
