function [ cout, origin ] = removezero( varargin)
% [c, origin] = removezero( c, what, [options] )
% Deletes zeros in arrays in various ways.
%
% Input:  c                                         The array where zeros shall be removed
%
%         what  
%           'compact',row-vector of integers        Removes all zeros in the given directions, shifts non-zero elements to the beginning, and fills up with zeros afterwards, should work for nd-arrays
%           row-vector of integers                  Removes empty things iterativly in the directions given by the vector 
%           'border'                                Removes all zeros at the boundary, works for nd-arrays        
%           'outside'                               Removes all zeros at the boundary in planes which do not go through zero, works for nd-arrays
%           'inside'                                Removes all zeros at the boundary in planes which go through zero, works for nd-arrays
%           'all'                                   Removes all zeros (totally) and returns a vector, works for nd-arrays
%           'left' | 'right' | 'top' | 'bottom'     Works only for 2-arrays. Removes zeros at the given side of the array
%
% Options:
%           'dim',val                               default: ndimsm(c), specifies the dimension
%           'keepdim'                               Does not fill array up with zeros if dimension afterwards is smaller than before
%           'ignore',val                            vector of integers, default=[], defines dimensions in which no zeros are removed
%                                                   Option has no effect for 'all' and, 'left', ..., 'bottom'.
%           'value',val                             double, default=0, value to be removed
%
% Output:    cout                                   Sequence c, zeros removed
%            origin                                 dim x 1-vector. Idx of first entry of cout, with respect to c. I.e.: cout(idx)=c(1).
%                                                   For 'all' is the linear index of the nonempty entries
%                                                   For 'compact' origin is empty
%                                                   If cout==[], then origin = zeros(dim,1)
%
% E.g.: removezero( padarraym([0 0 0 0;0 1 0 0;0 0 0 1;0 0 0 1], [0 0 1 1]), [1 2 3] )
%       removezero( [0 0 0 0;0 1 0 0;0 0 0 1;0 0 0 1], 'border' )
%       removezero( [0], 2 )
%
% Depends on: m-toolbox
%
% Written by: tommsch, 2018

%            2020-04-28, tommsch:       Behaviour change: Row vectors are now considered to be 2dimensional arrays
%                                       Behaviour change: removezerom keeps the dimensions not effected by row-vectors-of-integers
%                                             now: size( removezero([0 0 0],1) ) == [0 3]
%            2023-05-06, tommsch:       New return argument `idx`
% Changelog: 

    % parse input
    c = varargin{1};
    [value, varargin] =                 parsem( {'value','val'}, varargin, 0 );
    [allflag, varargin] =               parsem( {'all'}, varargin );
    [borderflag, varargin, borderstr] = parsem( {'border','outside','inside'}, varargin );
    [lrtbflag, varargin, lrtbstr] =      parsem( {'left','right','top','bottom'}, varargin );
    [compactflag, varargin] =           parsem( {'compact','cp'}, varargin );
    [keepdimflag, varargin] =           parsem( {'keepdim'}, varargin );
    [ignore, varargin] =                parsem( {'ignore','ig'}, varargin, [] );  % handle negative values in ignore
    [dim, varargin] =                   parsem( 'dim', varargin, [] );
    if( isempty(dim) );
        dim = ndimsm( c ); end;  % dimension of original array
    ignore(ignore<0) = dim + ignore(ignore<0) +1;
    
    
    % do work
    if( allflag );
        expect( isempty(ignore), 'removezerom:option', 'Option ''ignore'' does not work with ''all''.' );
        c = c(:); 
        idx = ~any( c == value, 2 );
        origin = find( idx, 1 );
        c( ~idx, : ) = [];
    elseif( borderflag );
        [c,origin] = removezero_border( c, value, ignore, borderstr );
    elseif( size(varargin, 2) >= 2 && isnumeric(varargin{2}) )  % removes zeros in directions varargin{2}
        origin = ones( dim, 1 );
        for i = varargin{2}; 
            szebefore = size( c );
            [c,origin(i)] = removezero_direction( c, value, i, ignore ); 
            if( isempty(c) );
                szebefore(i) = 0; 
                c = zeros(szebefore); end; end;
        if( ~all(origin) ); 
            origin = zeros( dim, 1 ); end;


    elseif( lrtbflag  );
        expect( isempty(ignore), 'removezerom:option', 'Option ''ignore'' does not work with ''left'', ''right'', ''top'', ''bottom''.' );
        [c,origin] = removezero_side( c, value, lrtbstr );
    elseif( compactflag );
        [c,origin] = removezero_compact( c, value, ignore, compactflag );
    else
        error( 'removezero:arg', '''removezero'': wrong argument.'); end;

    if( keepdimflag );    
        assert( isequal(allflag,0), 'removezerom:option', 'Option ''keepdim'' does not work with ''all''.' );
        c = makearraygreatagain( c, dim ); end;

    if( numel(origin)<dim );
        origin(numel( origin )+1:dim) = 1; end;

    cout = c;

end

function [ c ] = makearraygreatagain( c, targetdim );
currentdim = ndimsm( c ); 
if( currentdim<targetdim );
    val = ones( 1, targetdim );
    val = num2cell( val );
    val{end} = 2;
    c(val{:}) = 0;
    if( targetdim == 1 && ~iscolumn(c) ); 
        c = c.'; end; end;
end

function [ c, origin ] = removezero_direction( c, value, dir, ignore );
    dim = ndimsm( c );  % get dimension
    dir(dir<0) = dim + dir(dir < 0) + 1;  % handle negative dimensions
    
    if( any(dir == ignore) );
        origin = 0;
        return; end;
    
    o = [ 1:dir-1 dir+1:dim];  % all other dimensions
    IDX = cell( 1, dim );
    
    for i = o; 
        IDX{i} = 1:size( c, i ); end;
    IDX{dir} = anym( c~=value, o );  % find zeros
    if( isequal(IDX{dir},0) ); 
        c = []; 
    else; 
        c = c(IDX{:}); end;
    origin = find( IDX{dir}, 1 );
    if( isempty(origin) ); 
        origin = 0; end;
end

function [ c, origin ] = removezero_border( c, value, ignore, borderval )
    dim = ndimsm( c );  % get dimension
    IDX = cell( 1, dim );
    origin = zeros( dim, 1 );
    for i = 1:dim;
        if( any(ignore == i) );
            IDX{i} = 1:size(c,i); 
        else;
            o = [1:i-1 i+1:dim];
            idx = anym( c~=value, o );  % take slices 
            idx = idx(:);
            FIRST = find( idx, 1, 'first' );
            LAST = find( idx, 1, 'last' );
            if( isequal(borderval, 'inside') );
                LAST  = size( c, i ); end;
            if( isequal(borderval, 'outside') );  
                FIRST = 1; end;
            if( isempty(FIRST) ); 
                origin = 0; 
            else; 
                origin(i) = FIRST; end;
            IDX{i} = FIRST:LAST; end; end;
    c = c(IDX{:});
end


function [ c, origin ] = removezero_compact( c, value, ignore, dir );
    
    for i = 1:length( dir );
        if( any(ignore == dir(i)) );
            continue; end;
        len = size( c, dir(i) );
        c = num2cell( c, dir(i) );
        nd = ndims( c );
        for ii = 1:numel( c );
            c{ii} = removezero( c{ii}, 'all', 'value',value ); %remove all zeros
            c{ii}(len+1,1) = 0; %add one zero at the end, do not test for length here
            if( dir(i) ~= 1 ); 
                ordering = 1:nd;
                ordering(1) = dir(i); 
                ordering(dir(i)) = 1;
                c{ii} = permute( c{ii}, ordering ); end; end;
        c = cell2mat( c ); 
        c = removezero( c, dir(i), 'value',value ); end;
    origin = [];
end

function [ c, origin ] = removezero_side( c, value, lrtbval );
    dim = ndimsm(c);
    assert( ismatrix(c), 'removezero:matrix', 'For this option, array must be a 2-array.' );

        if( isequal(lrtbval, 'left') )
            chelp = sum( c~=value, 1 );
            idx = find( chelp, 1 );
            c = c(:,idx:end);
            origin = ones( dim, 1 ); 
            origin(2) = idx;
        elseif( isequal(lrtbval, 'right') )
            chelp = sum( c~=value, 1 );
            idx = find( chelp, 1, 'last' );
            c = c(:,1:idx);
            origin = ones( dim, 1 );
        elseif( isequal(lrtbval, 'top') )
            chelp = sum( c~=value, 2 );
            idx = find( chelp, 1 );
            c = c(idx:end,:);
            origin = ones( dim, 1 ); 
            origin(1) = idx;
        elseif( isequal(lrtbval, 'bottom') )
            chelp = sum( c~=value, 2 );
            idx = find( chelp, 1, 'last' );
            c = c(1:idx,:);
            origin = ones( dim, 1 ); end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
