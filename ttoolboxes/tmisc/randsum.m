function [ vec ] = randsum( total, nblocks, minval, gcdbound);
% generates a positive integer vector of size nblocks, whose entries sum to sum
% [ vec ] = randsum( sum, [nblocks], [minval] );
%
% Input:
%   total           the sum to which the elements shall sum up
%   nblocks         number of blocks
%   minval          mininum value of elements in vec
%   gcdbound       default=inf, if given, the algorithm tries to return a vector approximate gcd of the numbers in vec to be at most `gcdbound`
%
% Output:
%   vec         a vector with nblocks elements, summing up to total
%
% Example:
%   randsum( 100 )
%   randsum( 100, 2 )
%   randsum( 100, 4, 30 )
%
% Written by: tommsch, 2023-06-27
    
    if( total == 1 );
        assert( nargin <= 1 || isempty(nblocks) || isequal(nblocks, 1), 'randsum:input', 'if `total == 1` then `nblocks` must be 1 or not given.'  );
        vec = 1;
        return;
    elseif( total == 0 );
        assert( nargin <= 1 || isempty(nblocks) || isequal(nblocks, 0), 'randsum:input', 'if `total == 0` then `nblocks` must be zero or not given.'  );
        vec = []; 
        return; end;
    
    if( nargin <= 1 || ...
        isempty(nblocks) ...
      );
        nblocks = randi( [2 ceil(sqrt(total)/2)+1] ); end;
    assert( nblocks >= 1, 'randsum:input', '`nblocks` must be grater equal 1.' );
    
    if( nargin <= 2 || ...
        isempty(minval) ...
      );
        minval = 1; end;
    if( nargin <= 3 || isempty(gcdbound) );
        gcdbound = inf; end;
    
    assert( nblocks <= total/minval, 'Value of `minval` to large.' );
    
    vec = zeros( 1, nblocks );
    for i = 1:nblocks;
        imin = minval;
        imax = randi( max( 1, total-sum(vec)) );
        imax = clamp( imax, imin, total );
        vec(i) = randi( [imin imax] ); end;
    
    len = numel( vec );
    imax = max( 100, 10*total );
    for i = 1:imax;
        if( sum(vec) == total && gcdm(vec) <= gcdbound );
            break; end;
        sum_vec = sum( vec );
        shift = max( 1, round( abs(sum_vec - total)/3 ) );
        if( sum(vec) > total );
            idx = rand_oneof( find( vec >= minval + shift ) );
            vec(idx) = vec(idx) - shift;
        elseif( sum(vec) < total );
            idx = rand_oneof( find( vec <= total - shift) );
            vec(idx) = vec(idx) + shift;
        elseif( rand < .5 );  % this is needed for the gcdbound requirement
             idx = rand_oneof( find( vec >= minval + shift ) );
            vec(idx) = vec(idx) - shift;
        else;
             idx = rand_oneof( find( vec <= total - shift) );
            vec(idx) = vec(idx) + shift;
            end; end;
    assert( i ~= imax, 'randsum:error', 'Failed to construct suitable numbers.' );
    vec = vec( randperm(len) );
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
