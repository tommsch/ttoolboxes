function [ ret, n, d ] = rational( X, tol );
% returns a rational approximation of the input
% Convienence wrapper for 'rat' and 'numden'

    symflag = false;
    if( issym( X ) );
        symflag = true;
        X = double( X ); end;
    
    if( nargin == 1 );
        try;
            [n, d] = rat( X );
        catch me;  %#ok<NASGU>
            if( isoctave );
                [nr, dr] = rat( real(X) );
                [ni, di] = rat( imag(X) );
                n = nr.*di + 1i.*ni.*dr;
                d = dr.*di; end; end;
    else;
        try;
            [n, d] = rat( X, tol ); 
        catch me;  %#ok<NASGU>
            if( isoctave );
                [nr, dr] = rat( real(X), tol );
                [ni, di] = rat( imag(X), tol );
                n = nr.*di + 1i.*ni.*dr;
                d = dr.*di; end; end; end;
    
    if( symflag );
        n = sym( n );
        d = sym( d ); end;
    ret = n./d;
    
end

function ret = isoctave()
    % returns true when run on octave
    persistent x;
    if( isempty (x) );
        x = logical( exist ('OCTAVE_VERSION', 'builtin') ); end;
    ret = x;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
