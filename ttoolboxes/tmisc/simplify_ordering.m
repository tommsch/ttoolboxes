function [ cand, idx ] = simplify_ordering( cand );
% [ cand, idx ] = simplify_ordering( cand )
% Simplify candidates and remove duplicates
%
% Input:
%   cand        array of column vectors OR
%               cell array of column vectors
%
% Example:
%   oo = arrayfun( @(n) num2cell( mixvector( [1 2], n ), 1 ), [1:10], 'UniformOutput',false )  % all combinations of length 1 to 10
%   oo = flatten( oo )  % to flat cell array
%   oo = simplify_ordering( oo )  % remove repetitions, cyclic permutations, duplicates
%   oo = merge_oo( oo )  % merge to an array
%   len = size( oo, 1 ) - sum( oo == 0, 1 )  % count length of ordering
%   [~, idx] = sort( len )
%   oo = oo(:,idx)  % sort ordering
%
% See also: reducelength
%
% Written by: tommsch, 2018

    idx = [];
    if( isempty(cand) );
        return; end;

    cellflag = iscell( cand );
    if( ~cellflag );
        cand = num2cell( cand, 1 ); end;
    
    for i = 1:numel( cand );
        cand{i} = reducelength( cand{i} ); end;
    LENGTHMAX_P1 = max( cellfun('length', cand) ) + 1;   
    for i = 1:size( cand, 2 ); 
        cand{i}(LENGTHMAX_P1,1) = 0; end;  
    cand = cell2mat( cand );                                
    cand(end,:) = [];                                     
    [cand, idx] = unique( cand', 'rows' );
    
    if( ~cellflag );
        cand = cand';
    else;
        cand = num2cell( cand.', 1 );
        for i = 1:numel( cand );
            cand{i}(cand{i} == 0) = [];
            end; end;
        
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

