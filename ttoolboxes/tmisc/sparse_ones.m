function [ A ] = sparse_ones( varargin );
% Convienence function generating a sparse ones matrix.
% Has the same interface as ones()
    A = ones( varargin{:} );
    A = sparse( A );
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
