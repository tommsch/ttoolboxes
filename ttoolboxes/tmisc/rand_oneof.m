function [ x ] = rand_oneof( varargin )
    if( nargin > 1 )
        x = varargin{randi( nargin )};
    elseif( nargin == 1 && ~iscell(varargin{1}) );
        x = varargin{1}(randi( numel(varargin{1}) ));
    elseif( nargin == 1 && iscell(varargin{1}) );
        x = varargin{1}{randi( numel(varargin{1}) )};
    else;
        x = varargin{1}{randi( nargin )}; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>