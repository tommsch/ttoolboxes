function [ M, Mscaled, scale ] = buildproduct( varargin );
% [ M, Mscaled, scale ] = buildproduct( A, oo, [options] )
% Constructs the product, of matrices in the cell A corresponding to the sequence in oo
% This function has a big overhead. For many computations use buildproduct_fast
%
% Input:
%   A       cell array of matrices
%   oo      The ordering. Either 
%               ordering: {[1xN],[1xM]}, if A is a cell array of matrices, N or M can be zero, or 
%               row or column vector, if A is a cell array of matrices
%               (experimental) ordering: {[2xN],[2xM]}, if A is a cell-array of cell-arrays of matrices, N or M can be zero, or
%
% Options:
%         'sym'              the matrices A are converted to symbolic matrices prior to computation
%         'l',val            integer, default=inf, approximate length of the expanded product sequence (default inf)
%                                if l is infinite, the infinite part is computed via diagonalization,
%                                if l is finite, the infinite part is computed by truncation of the infinite product.
%         'reverse'          reverses the product (e.g. may be used in the computation with transition matrices)
%         'scale',bool       default = True if three outputs are requested, false otherwise. If true, input matrices are scaled so that result is better. The resulting scaling factor is returned as second output argument
%         'rand'             returns a ranomd product
%
% Output:
%   M           the matrix/vector constructed by the given product, unscaled
%   Mscaled     the matrix scaled matrix
%   scale       the scale of the output matrix. E.g. M=8, scale=100, then the real product is M=800;
%                           
%
% Info:
%   Constructs the product, of matrices in the cell A corresponding to the sequence in mo
%   i.e.: buildproduct( {M1 M2}, [1 2 2] ) == M2*M2*M1
%         buildproduct( {M1,M2}, {[],[1 2]} ) == (M2*M1)^inf
%   if oo(i) <= 0, A{oo(i)} is replaced by the identity
%
% E.g.: buildproduct({[1 1/2; 0 1/2],[1/2 0; 1/2 1]}, [1 2 2 1])
%       buildproduct({[0.8 0.2; 0.2 0.8],[0.4 0.6;0.4 0.6]}, {[],[1 2]})
%     
% 
% Depends on: parsem (to parse the options, also runs without );
%
% See also: buildproduct_fast, ordering2num
%
% Written by tommsch, 2018

    [A, oo, opt] = parse_input( varargin{:} );
    
    [M1, M2, oo] = preprocess_input( A, oo, opt );
    
    scale1 = 1;
    for t = 1:opt.LEN{1};
        j = oo{1}(1,t);  % number of set 
        i = oo{1}(2,t);  % number of transition in set j    
        if( i > 0 && j > 0 );
            if( opt.scaleflag );
                ma = max( abs(A{j}{i}(:)) );
                if( ma > 1e-9 );
                    m1 = max( abs(M1(:)) );
                    scale_factor = m1/ma;
                    if( isfinite(scale_factor) && scale_factor > 0 );
                        M1 = M1/scale_factor;
                        scale1 = scale1 * scale_factor; end; end; end;
            M1 = A{j}{i}*M1; end; end;
            
    scale2 = 1;
    for t = 1:opt.LEN{2};
        j = oo{2}(1,t);  % number of set 
        i = oo{2}(2,t);  % number of transition in set j    
        if( i > 0 && j > 0 );
            if( opt.scaleflag );
                ma = max( abs(A{j}{i}(:)) );
                if( ma > 1e-9 );
                    m2 = max( abs(M2(:)) );
                    scale_factor = m2/ma;
                    if( isfinite(scale_factor) && scale_factor > 0 );
                        M2 = M2/scale_factor;
                        scale2 = scale2 * scale_factor; end; end; end;
            M2 = A{j}{i}*M2; end; end;
    
    if( ~isempty(oo{2}) );
        if( opt.scaleflag );
            m2l = rho( M2 );
            M2 = M2/m2l;
            scale2 = (scale2 * m2l); 
            if( abs(scale2 - 1) < 4*eps );
                scale2 = 1; end; end;
        if( isfinite(opt.len) );
            scale2 = scale2^opt.len;
            M2l = {M2^opt.len};
        else;
            assert( abs(scale2 - 1) < 4*eps, 'buildproduct:scale', 'Matrices do not have spectral radius 1, and thus, cannot be used in an infinite product.' );
            M2l = mpowerinf( M2, opt.scaleflag ); end;
            
        M = cell( 1, numel(M2l) );
        if( opt.reverseflag );
            for i = 1:numel( M2l );
                M{i} = M1*M2l{i}; end;
        else
            for i = 1:numel( M2l );
                M{i} = M2l{i}*M1; end; end;
    else
        M = {M1}; end;
    
    if( numel(M) == 1 );
        M = M{1}; end;
    
    if( opt.scaleflag );
        Mscaled = M;
        scale = scale1 * scale2;
        M = M * scale; 
    else;
        Mscaled = M;
        scale = 1; end;
    


end

function [ A, oo, opt ] = parse_input( varargin );
    [opt.scaleflag, varargin] = parsem( 'scale', varargin, [] );
    if( isempty(opt.scaleflag) );
        opt.scaleflag = nargout >= 3; end;
    [opt.reverseflag, varargin] = parsem( 'reverse', varargin );
    [opt.symflag, varargin] = parsem( 'sym', varargin, -1 );
    [opt.len, varargin] = parsem( {'l','len','length'}, varargin, inf );
    [opt.rand, varargin] = parsem( {'rand','random'}, varargin );

    if( false );
    elseif( numel(varargin) == 2 && iscell(varargin{1}) );
        A = varargin{1};
        oo = varargin{2};
        varargin(1:2) = [];
    elseif( numel(varargin) == 2 );
        A = varargin{2};
        oo = varargin{1};
        varargin(1:2) = [];
    elseif( numel(varargin) == 1 && opt.rand );
        if( opt.rand == 1 );
            len = randi( [1 100] );
        else;
            len = opt.rand; end;
        A = varargin{1};
        oo = randi( [1 numel(A)], [1 len] ); 
        varargin(1) = [];
    else;
        error( 'buildproduct:input', 'Wrong input given.' ); end;
    
    parsem( varargin, 'test' );
    
    if( ~iscell(oo) && iscolumn(oo) ); 
        oo = oo.'; end;  % make oo to row vector
    if( ~iscell(oo) ); 
        val = oo; 
        oo = {val,[]}; end;  % make oo to {oo,[]}
    if( size(oo, 2) == 1 ); 
        oo = {oo{1},[]}; end;  % make {oo1} to {oo,[]}
    opt.LEN{1} = size(oo{1}, 2); 
    opt.LEN{2} = size(oo{2}, 2);  % length of sequences
    opt.len = ceil( opt.len/opt.LEN{2} );  % sic!, if LEN{2} == 0, then we do not need this value

    if( iscell(A{1}) )
        opt.dim = size( A{1}{1}, 1 );
        assert( size(oo{1}, 1) ~= 1, 'buildproduct:input', 'buildproduct: ''oo'' needs a 2xN array.');
    else;
        opt.dim = size(A{1}, 1);
        val = A; 
        A = {}; 
        A{1} = val;
        if( ~isempty(oo{1}) ); 
            oo{1}(2,:) = 1; end; 
        oo{1} = flip( oo{1}, 1 );
        if( ~isempty(oo{2}) ); 
            oo{2}(2,:) = 1; end; 
        oo{2} = flip( oo{2}, 1 ); end;    
end

function [ M1, M2, oo ] = preprocess_input( A, oo, opt );
    if( opt.symflag );
        switch opt.symflag;
            case {-1,'auto'};
                M1 = cast( eye( opt.dim ), class(A{1}{1}) );
                M2 = cast( eye( opt.dim ), class(A{1}{1}) );
            case {0,'double'};
                for i = 1:size( A, 2 ); 
                    for j = 1:numel( A{i} );
                        A{i}{j} = double( A{i}{j} ); end; end; 
                M1 = eye( opt.dim );
                M2 = eye( opt.dim );
            case {1,'vpa'};
                for i = 1:size( A, 2 ); 
                    for j = 1:numel( A{i} );
                        A{i}{j} = double( A{i}{j} ); end; end;
                M1 = vpa( eye(opt.dim) );
                M2 = vpa( eye(opt.dim) );
            case {2,'sym'};
                for i = 1:size( A, 2 ); 
                    for j = 1:numel( A{i} );
                        A{i}{j} = double( A{i}{j} ); end; end; 
                M1 = sym( eye(opt.dim) );
                M2 = sym( eye(opt.dim) );
            otherwise;
                fatal_error; end;
       
    else;
        M1 = eye( opt.dim );
        M2 = eye( opt.dim ); end;

    if( opt.reverseflag );
        oo{1} = flip( oo{1}, 2 ); 
        oo{2} = flip( oo{2}, 2 ); end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
