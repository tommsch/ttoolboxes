function [ mn, mx, imn, imx ] = minmax1( X, varargin );
    [mn, imn] = min( X, varargin{:} );
    [mx, imx] = max( X, varargin{:} );
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
