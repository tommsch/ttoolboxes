function [ varargout ] = ratt( A, tol, symflag );
    if( nargin <= 2 || isempty(symflag) );
        symflag = 0; end;
    [A, cellflag] = tocell( A );
    N = cell( size(A) );
    D = cell( size(A) );
    for i = 1:numel( A );
        if( nargin <= 1 || isempty(tol) );
            [N{i}, D{i}] = rat( A{i} );
        else;
            [N{i}, D{i}] = rat( A{i}, tol ); end;
        if( symflag );
            N{i} = sym( N{i} );
            D{i} = sym( D{i} ); end; end;

    if( ~cellflag );
        N = N{1};
        D = D{1}; end;
    
    if( nargout <= 1 );
        varargout = {N./D};
    elseif( nargout == 2 );
        varargout = {N D}; end;
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
