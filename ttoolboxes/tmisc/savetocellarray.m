function [ array ] = savetocellarray( value, idx, array, row );
% array = savetocellarray( value, idx, array, [row] )
% Stores values in a cell array corresponding to a linear index-vector.
% More precisely: Stores <val> in the cell array <array>, corresponding to the places where idx=1.
%
%Input:
%   value       vector, values to save with length nnz(idx), or a scalar
%   idx         logical array with length size([array{:}],2)
%   array       cell array of vectors, the cell array
%   row         optional, default= ':', if given, only values in the rowth row are updated
%
% Output:
%   array       cell array of vectors, The array with the stored values
%
% E.g.: savetocellarray( [10 20 30], [1 1 0 0 1], {[-1 -2], [-3 -4 -5]} )
%
% Written by: tommsch, 2018

%            2021-01-12, tommsch,   Added option row
%                                   idx does not need to be logical anymore
% Changelog: 

    if( nargin <= 3 || isempty(row) );
        row = 1:size( array{1}, 1 );; end;
    if( nnz(idx) == 0 ); 
        return; end;
    if( isempty(idx) ); 
        idx = true( size(array) ); end;
    if( isscalar(value) ); 
        value = repmat( value, numel(row), nnz(idx) ); end;
    L = cellfun( 'size', array, 2 );
    
    idx = mat2cell( idx, 1, L );
    len = zeros( 1, numel(idx) );
    for i = 1:numel(idx);
        len(i) = nnz( idx{i} ); end;
    value = mat2cell( value, numel(row), len);
    for i = 1:size( array, 2 );
        array{i}(row,logical( idx{i} )) = value{i}; end;


end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
