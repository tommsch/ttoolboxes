function [ Z, idx ] = celldiff( A, B );
% computes the set difference
% [ Z, idx ] = celldiff( A, B );
% Input:
%   A,B       cell, each entry is one element of the set
% Output:
%   Z         cell, the set A\B
%   idx       numel(A)x1 logical vector, the indices of A which are chosen for the output
%
% E.g.: celldiff( {1 2 3},{1 2} );
%
% Written by: Stephen Cobeldick, 2019
% https://www.mathworks.com/matlabcentral/answers/498456-set-difference-between-two-cell-arrays

    [XA, XB] = ndgrid( 1:numel(A), 1:numel(B) );
    X = arrayfun( @(xA, xB) isequal(A{xA}, B{xB}), XA, XB );
    idx = ~any( X, 2 );
    Z = A(idx);
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>