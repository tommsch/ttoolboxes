function [ mn ] = minors( A );
    assert( issquare(A) );
    sz = size( A, 1 );
    ct = nchoosek( 2*sz, sz ) - 1;  % number of minors
    mn = nan( 1, ct );
    i = 0;
    for order = sz:-1:1;
        px = nchoosek( 1:sz, order )';
        py = nchoosek( 1:sz, order )';
        for x = px;
            for y = py;
                i = i + 1;
                mn(i) = det( A(x,y) ); end; end; end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
