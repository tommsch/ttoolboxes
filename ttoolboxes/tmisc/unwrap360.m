function [ vec ] = unwrap360( vec, varargin );
% [ vec ] = unwrap360( vec, [wraparound], [shiftflag] );
% Versatile wrapper for Matlabs unwrap
%
% Input: 
%    vec            vector, thing to be unwraped
%    wraparound     double, default = 360, value where input wraps around
%    shiftflag      bool,   default = false, if given, vector is shifted so that its mean is near zero
%
% Output:
%    vec            vector, unwraped thing
%
% See also: unwrap
%
% Example: plot( unwrap( randn(1,100), 10, true ) );
%
% Written by: tommsch, 2021-08-13


    

    flagidx = cellfun( 'isclass', varargin, 'logical' );
    assert( nnz(flagidx) <= 1, 'wraparound:input', 'at most one logical must be given' );
    if( any(flagidx) );
        shiftflag = varargin{flagidx}; 
        varargin(flagidx) = []; 
    else;
        shiftflag = false; end;
    assert( numel(varargin) <= 1, 'wraparound:input', 'at most one wraparound as double must be given.' );
    
    if( isempty(varargin) );
        wraparound = 360;
    else;
        wraparound = varargin{1}; end;
    

    vec = vec * 2 * pi / wraparound;
    vec = unwrap( vec );
    vec = vec/( 2*pi ) * wraparound;
    
    if( shiftflag );
        m = mean( vec );
        m = round( m/wraparound ) * wraparound;
        vec = vec - m; end;

end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
