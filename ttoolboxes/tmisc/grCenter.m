function [ center, weight ] = grCenter( varargin )
% [center, weight] = grCenter( E, [options] )
% Finds the center of a tree
% Iteratively deletes leaves, until the center is found.
%
% Input:
%   E       List of edges (N x 2)-array
%
% Options:
%   'edge'          counts vertices to return an edge in all cases. 
%                   This algorithm does not work for trees with multiple components
%   'verbose',val   Verbose level, if set to 2, plot output may be generated
%
% Output:
%   center      index of vertices/pairs of vertices of the edge which are the center
%               If tree has more than one component, center is a cell array
%   weight      (behaviour may be changed) distance to outermost leave
%
% Note: 
%   If E is not a tree, the behaviour is undefined
%   Needs the m-toolboxes to parse the options.
%
% E.g.: G = [1 2;2 5; 5 6; 2 3; 3 4; 4 7; 4 8; 8 9]; 
%       plot( graph(G(:,1),G(:,2)) );
%       grCenter( G )
%       grCenter( G, 'edge' )
%
%       G = [1 2;2 5; 5 6; 2 3; 3 4; 4 7; 4 8; 8 9; 1 9]; 
%       plot( graph(G(:,1),G(:,2)) );
%
% Depends on: m-toolboxes
%
% See also: grVerCover
%
% Written by: tommsch, 2018

%#ok<*ALIGN>

    E = varargin{1};
    varargin(1) = [];
    if( size(E,1) == 1 );
        center = E;  
        weight = [0 0]; 
        return; end;
    if( size(E,1) == 0 );
        center = []; 
        weight = []; 
        return; end;
    try;
        [edgeflag, varargin] = parsem( {'edge','e'}, varargin );
        [verbose, varargin] = parsem( {'verbose','v'}, varargin );
        parsem( varargin, 'test' );
    catch;
        edgeflag = 0;
        verbose = 0; end;
        
       
    
    if( edgeflag ); 
        [center, weight] = grCenter_edge( E, verbose );
    else;
        [center, weight] = grCenter_vertex( E, verbose ); end;

end


function [center, weight] = grCenter_edge( E, verbose )

    if( verbose >= 2 );
        G = graph( E(:,1), E(:,2) ); 
        plot( G ); 
        if( verbose >= 3 );
            pause; end; end;
    
    
    V = unique( E(:) ); %list of vertices
    weight = zeros( size(V) );
    
    while(~isempty(E))
        
        %identify leaves
        [occ,idx] = hist( E(:), V );  %#ok<HIST>  % occurences, vertex-idx
        idx = idx(occ == 1); 
        %idx = idx(find(occ == 1)); %#ok<FNDSB> %original
        weight(idx) = weight(idx)+1;
        
        if( verbose >= 3 );
            plot( G, 'NodeLabel', weight ); 
            pause; 
            end;
        
        %sum up leaves
        edges = zeros( 1, numel(idx) ); %must be a column vector, but I don't know why
        for i = 1:numel( idx );
            edges(i) = find( any(E == idx(i),2) ); end;    
        E2 = E(edges,:);
        
        for i = 1:numel( edges )
            val = weight(E2(i,1)) + weight(E2(i,2));
            if(E2(i,1) == idx(i));
                weight(E2(i,2))=val;
            else;
                 weight(E2(i,1))=val; end; end
        
        %remove edges
        if( isempty(edges) );
            warning( 'grCenter:tree', 'Graph appears not to be a tree.' );
            center = nan;
            weight = nan; 
            return;
        else;
            E(edges,:) = []; end;
    
    
end

[~,center] = sort( weight, 'descend' );
center = center(1:2);

if( verbose >= 2 );
    h = plot( G, 'NodeLabel',weight );
    highlight( h, center ); end;

end

function [center, weight] = grCenter_vertex( E, ~ )

    weight = zeros( size(E,1), 1 );
    
    k = 1; %counter for weight
    kidx = 1;

    V = unique( E(:) ); %list of vertices
    center = {};
    possiblecenter = [];
    while( true );
        [occ,idx] = hist( E(:), V );  %#ok<HIST>   %occurences, vertex-idx
        idx = idx(find( occ == 1 ));  %#ok<FNDSB>

        val = possiblecenter(occ(possiblecenter) == 0);
        if( ~isempty(val) );
            center = [center num2cell( val.' )]; end; %#ok<AGROW>

        if( isempty(E) );
            break; end;
        possiblecenter = setdiff( V(occ ~= 0), idx );
        edges = cell( numel(idx), 1 );
        for i = 1:numel( idx );
            edges{i} = find( any(E == idx(i),2) ); end;
        %edges2 = arrayfun(@(x) find(any(E == x,2)),idx,'UniformOutput',0); %original
        edges = cell2mat( edges );

        %test for multiple edges ==> meaning: this edge is in the center
        [~, I] = unique( edges, 'first' );
        val = 1:size( edges, 1 );
        val(I) = [];
        val = edges( val, : );
        if( ~isempty(val) );
            center{end+1} = E(val,:); end; %#ok<AGROW>



        weight(kidx:kidx+size(edges,1)-1) = repmat( k, size(edges,1), 1 );

        kidx = kidx + size( edges, 1 );
        k = k+1;
        if( isempty(edges) );
            warning( 'grCenter:tree', 'Graph appears not to be a tree.' );
            center = nan;
            weight = nan; 
            return;
        else
            E(edges,:) = []; end; end;

    if( size(center,2) == 1 );
        center = center{1}; end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
