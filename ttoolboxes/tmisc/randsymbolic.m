function [ ret ] = randsymbolic( varargin );
% [ ret ] = randsymbolic( x1, ..., xn, [options] );
% Generating random symbolic expression
% 
% Input:
%   xi      dimension of array
% 
% Options:
%   'iteration'     integer, default=5, number of iterations for each entry, in each iteration the return value is changed
%   'start'         symbolic, default = sym(1), number to start the iteration with
%   'legacy'        logical, default = true, when true randsym(3) produces a 3x3 array (matlab behaviour), when false produces a 3x1^array (m-packge behaviour)
%   'nonan'         logical, default = true, when set, function tries not to return nan values
%
% Output:
%   ret     symbolic array, the random expressions
%
% Depends on: m-toolbox
%
% Written by: tommsch, 2020

%            tommsch, 2022-02-18, Better generation of random stuff
%                                 Behaviour change: Due to a Matlab bug, the test for nan/finiteness must be done using doubles.
%                                 Added options: 'finite', 'nan',
% Changelog: 

try;
    [it, varargin] =            parsem( {'iteration','it'}, varargin, 5 );
    [start, varargin] =         parsem( {'start','s'}, varargin, sym(1) );
    [legacy, varargin] =        parsem( {'legacy','leg'}, varargin, 1 );
    [val, varargin] =           parsem( {'nonan'}, varargin, 1 );
    [nanflag, varargin] =       parsem( {'nan'}, varargin, ~val );
    [finiteflag, varargin] =    parsem( {'finite'}, varargin, ~nanflag );
catch me;  %#ok<NASGU>
    it = 5;
    start = sym(1);
    legacy = 1;
    nanflag = 0; 
    finiteflag = 1; end;

assert( implies(nanflag, ~finiteflag) );

if( legacy );
    ret = sym( zeros(varargin{1:end}) );
else;
    ret = sym( zerosm(varargin{1:end}) ); end;

n = 1;

persistent value;
persistent operator1;
persistent operator2;
if( isempty(value) );
    value = {};
    value{end+1} = @() sym( randi(99)-50 );
    value{end+1} = @() sym( randi(99)-50 )/sym( randi(99)-50 );
    value{end+1} = @() sym( 1/(randi(9)-5) );
    value{end+1} = @() sym( randi(9)-5 );
    value{end+1} = @() sym( randi(9)-5 )/sym( randi(9)-5 );
    value{end+1} = @() sym( randn );
    value{end+1} = @() sym( randn*10^randi(1) );
    value{end+1} = @() sym( randn*10^randi(3) );
    value{end+1} = @() sym( randn*10^randi(10) );
    value{end+1} = @() sym( randn*10^randi(30) );
    value{end+1} = @() sym( randn*10^randi(100) );
    value{end+1} = @() sym( randn*10^randi(300) );
    value{end+1} = @() sym( pi );
    value{end+1} = @() exp( sym(1) );
    value{end+1} = @() sin( sym(1) );
    value{end+1} = @() cos( sym(1) );
    value{end+1} = @() sqrt( sym(2) );

    operator1 = {};
    operator1{end+1} = @(x) abs(x);
    operator1{end+1} = @(x) x^x;

    operator2 = {};
    operator2{end+1} = @(x,y) x + y;
    operator2{end+1} = @(x,y) x - y;
    operator2{end+1} = @(x,y) x * y;
    operator2{end+1} = @(x,y) x / y;
    operator2{end+1} = @(x,y) x ^ y;
    operator2{end+1} = @(x,y) x ^ (1/y);
    operator2{end+1} = @(x,y) x ^ (1/randi(99));
    %operator2{end+1} = @(x,y) max(x,y);
    %operator2{end+1} = @(x,y) min(x,y);
    
    end;
    

while( n<=numel(ret) );
    x = sym( start );
    for i = 1:it;
        try;
            ary = randi( 2 );
            switch ary;
                case 1;
                    idx_op = randi( numel(operator1) );
                    op = operator1{idx_op};
                    x = op( x );
                case 2;
                    idx_op = randi( numel(operator2) );
                    idx_val = randi( numel(value) );
                    val = value{idx_val}();
                    op = operator2{idx_op};
                    x = op( x, val );
                otherwise;
                    error( 'randsymbolic:fatal', 'Programming error.' ); end;
                %x
        catch me;  %#ok<NASGU>
            end; end;
    dx = double( x );
    if(  ~nanflag && isnan(dx) );
        continue; 
    elseif( finiteflag && ~isfinite(dx) );
        continue; 
    else;
        ret(n) = x; 
        n = n + 1; end; end;
    

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
