function [ h, o2, o3 ] = randfun( varargin );
% [ h, o2, o3 ] = randfun( [value], [options] );
% Generating random unary, vectorizable, function
%
% Input:
%   value           if given, the random function is evaluated at points value
%
% Options:
%   'difficulty'    nteger, default=5, level of difficulty of returned functions
%   'start'         symbolic, default = sym(1), number to start the iteration with
%   'complex'       if set, return complex valued functions may be returned
%
% Output:
%   h     symbolic array, the random expressions
%
% Depends on: m-toolbox
%
% Written by: tommsch, 2020

% Examples of generated functions
% h(x) = @(x)((1./(1+exp(-((-0.69138+1.6072.*((-0.66187+0.20561.*x+-0.35262.*x.^2+0.23167.*x.^3+-0.024758.*x.^4).*1i)+-0.15038.*((-0.66187+0.20561.*x+-0.35262.*x.^2+0.23167.*x.^3+-0.024758.*x.^4).*1i).^2+0.063266.*((-0.66187+0.20561.*x+-0.35262.*x.^2+0.23167.*x.^3+-0.024758.*x.^4).*1i).^3+-0.0042383.*((-0.66187+0.20561.*x+-0.35262.*x.^2+0.23167.*x.^3+-0.024758.*x.^4).*1i).^4))))).*1i); x = -10:.00001:10; plot( h(x) );

% XX add characteristic function [0,1]
% XX add heaviside function

 %#ok<*AGROW>
 
    [h, opt] = parse_input( varargin{:} );
    h = generate( h, opt );
    simplify( h, opt );
    [h, o2, o3] = postprocess( h, opt );


end

%%

function [ h, opt ] = parse_input( varargin );
    try;
        [opt.verbose, varargin] =       parsem( {'verbose','v'}, varargin, 1 );
        [opt.pos, varargin] =           parsem( {'pos','abs'}, varargin );
        [opt.maxdifficulty, varargin] = parsem( {'difficulty','d'}, varargin, 10 );
        [opt.start, varargin] =         parsem( {'start','s'}, varargin, [] );
        [opt.complexflag, varargin] =    parsem( {'complexflag','cp'}, varargin );
        [opt.values, varargin] =        parsem( {'x','values','evaluate'}, varargin, [] );
    catch;
        opt.maxdifficulty = 10;
        opt.start = sym(1); 
        opt.complexflag = false;
        opt.values = []; end;

    if( isempty(opt.start) );
        h = 'x';
    else;
        h = func2str( opt.start ); end;
    
    if( isempty(opt.values) && numel(varargin) >= 1 && isnumeric(varargin{1}) && ismatrix(varargin{1}) );
        opt.values = varargin{1};
        varargin(1) = []; end;
    
    assert( isempty(varargin) );
end

function [ h ] = generate( h, opt );
    difficulty = 0;
    while( difficulty < opt.maxdifficulty );
        a = randi( 17 );
        %a = 1;
        switch a;
            case  1; 
                h = ['( ' h ' + ' num2str(randx) ' )'];
                add = 1;
            case  {2,3}; 
                h = ['( ' h ' .* ' num2str(randxnz) ' )'];
                add = 1;
            case  {4,5,6}; 
                h = ['( ' h ' ) .^ ' num2str(randxnz)];
                add = 1;
            case  7; 
                h = [num2str(randxnzno) ' .^( ' h ' )'];
                add = 2;
            case  8; 
                h = ['sin( ' h ' )'];
                add = 3;
            case  9; 
                h = ['(1./(1+exp(-(' h '))))'];
                add = 4;
            %case  10; h = @(x) abs( h(x) );
            case 10;
                h = [ '( ' num2str(randx) '+' num2str(randx) '.*' h '.^2' ')'];
                add = 2;
            case 11;
                h = [ '( ' num2str(randx) '+' num2str(randx) '.*' h '.^3' ')'];
                add = 2;
            case 12;
                h = [ '( ' num2str(randx) '+' num2str(randx) '.*' h '.^' num2str(randi([2 10])) ')'];
                add = 2;
%            case {10,11,12}; 
%                h = [ '( ' num2str(randx) ' + ' num2str(randx) '.*' h ' + ' num2str(randx)  '/2.*' h '.^2' ' + ' num2str(randx) '/6.*' h '.^3' ' + ' num2str(randx) '/24.*' h '.^4' ' )'];
%                add = 6;
            case  13; 
                h = ['(1./' h ')'];
                add = 1;
            case 14; 
                h = ['( ' h '.* 1i)'];
                add = 1;
            case 15; 
                h = ['abs( ' h ' )'];
                add = 1;
            case 16;
                h = ['double((' h '  >= 0) & ( ' h ' <= 1))'];
                add = 2;
            case 17;
                h = ['double(' h '  >= 0)'];
                add = 2;                
            otherwise;
                end; %do nothing
        difficulty = difficulty + add; end;
end

function [ h ] = simplify( h, ~ );
    %  % This does not work, and thus, is commented out. Problem x.*0 is simplified to 0, but should become something equivalent to zeros(size(x));
    %     H = str2sym( h );  
    %     H = char( H );
    %     H = vectorize( H ); 
    %     h = H; end;
end

function [ h, o2, o3 ] = postprocess( h, opt );
    o2 = [];
    o3 = [];
    if( opt.pos );
        h = ['abs( ' h ' )']; end;
        
    if( ~opt.complexflag );
        h = ['real( ' h ' )']; 
        h = eval( ['@(x)' h] );
    elseif( opt.complexflag && nargout == 2 )
        o2 = ['imag( ' h ' )'];
        h = ['real( ' h ' )'];
        o2 = eval( ['@(x)' o2] );
        h = eval( ['@(x)' h] );
    elseif( opt.complexflag && nargout == 3 )
        o2 = ['real( ' h ' )'];
        o3 = ['imag( ' h ' )'];
        o2 = eval( ['@(x)' o2] );
        o3 = eval( ['@(x)' o3] );
        h = eval( ['@(x)' h] ); end;
    

    if( ~isempty(opt.values) );
        if( opt.verbose >= 1 );
            disp( h ); end;
        h = h( opt.values );
        if( ~isempty(o2) );
            if( opt.verbose >= 1 );
                disp( o2 ); end;
            o2 = o2( opt.values ); end;
        if( ~isempty(o3) );
            if( opt.verbose >= 1 );
                disp( o3 ); end;
            o3 = o3( opt.values ); end; end;
end
%% rngs

function ret = randx( varargin );
    if( randi(10) == 1 );
        ret = randn( varargin{:} );
    else;
        val = dist;
        ret = randi( 2*val+1, varargin{:} ) - val; end;
end

function ret = randxnz( varargin );
    ret = 0;
    while( ~any(ret) );
        ret = randx( varargin{:} ); end;
end

function ret = randxnzno( varargin );
    ret = 0;
    while( all(ret == 0) || all(abs(ret) == 1) );
        ret = randx( varargin{:} ); end;
end

function ret = dist( varargin );
    if( nargin == 0 );
        x = rand;
        m = 0;
        M = inf;
    elseif( nargin == 2 );
        x = rand;
        m = varargin{1};
        M = varargin{2};
    else;
        x = varargin{1};
        m = varargin{2};
        M = varargin{3}; end;
        
    % function which gives random integers between m and M, 0<=m<=M<=inf
    k = .5;  % the higher k, the smaller the returned values
    e = .5;  % offset is necessary so that function works for m=0 too
    ret = round((m+e)./(1 + (m+e)./M.*x.^k - x.^k)-e/2);
end

function dummy; end  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
