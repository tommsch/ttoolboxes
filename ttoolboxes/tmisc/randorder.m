function [ oo ] = randorder( varargin );
% [ oo ] = randorder( n1, ..., nn );
% generates a random ordering
%
% Input:
%   ni  number of occurences of i in output vector
%
% Output:
%  oo   row vector
%
% Example: 
%   randorder( 4, 2 );
%
% Written by:
%   tommsch, 2023-07-11

    in = [varargin{:}];
    J = numel( in );
    oo = repelem( 1:J, in );
    oo = oo(randperm( numel(oo) ));
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
