function [ M ] = matrix_from_eig( v, la );
% [ M ] = matrix_from_eig( v, la );
% Constructs a matrix from eigenvectors and eigenvalues
%
% Input:
%   v       square matrix of column vectors, the eigenvectors
%   la      vector of length equal to size(v, 1), the eigenvalues
%
% Output:
%   M       square matrix, v\la*v
%
% Example:
%   matrix_from_eig( [1  2;-2 1]', [1 -1] )
%
% Written by: tommsch, 2024

%               tommsch, 2024-00-00,    Added this function to tmisc package
% Changelog:

    if( isvector(la) );
        la = diag( la ); end;
    assert( size( v,  1 ) == size( v,  2 ) && ...
            size( v,  2 ) == size( la, 2 ) && ...
            size( la, 1)  == size( la, 2 ), ...
            'matrix_from_eig:input', 'Input has bad dimensions.' );
    M = v\la*v;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
