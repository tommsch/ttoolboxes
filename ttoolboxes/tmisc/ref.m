function [ ret ] = ref( A, varargin ); %#ok<INUSL>
    ret = eval( ['A' varargin{:}] );
end

function dummy; end  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
