function [ B ] = invblkdiag( A );
% Finds square blocks of a square matrix
% [ ret ] = blkdiag( A )
%
% Input
%   A   square matrix
%
% Output:
%   B   cell array of square matrices such that
%       blkdiag( invblkdiag(B) ) == A
%
% Written by: tommsch, 2020

% Changelog: tommsch,   2025-01-10,   Now also accepts cell arrays of matrices

    [A, cellflag] = tocell( A );
    B = cell( size(A) );
    for j = 1:numel( A );
        B{j} = invblkdiag_worker( A{j} ); end;
    if( ~cellflag );
        B = B{1}; end;
end

function [ B ] = invblkdiag_worker( A )
    dim = size( A, 1 );
    assert( dim == size(A,2), 'invblkdiag:square', 'Input must be a square matrix.' );
    if( dim == 0 ); 
        B = {}; 
        return; end;
    d = zeros( 1, dim );
    r = zeros( 1, dim );
    idxA = isAlways( A ~= 0, 'Unknown','true' );  % speed up for symbolic stuff
    for i = 1:size( A, 1 )
        d(i) = last( find( idxA(:,i), 1, 'last' ), 0 );
        r(i) = last( find( idxA(i,:), 1, 'last' ), 0 ); end;
    d = cummax( d );
    r = cummax( r );
    d = (d <= 1:dim);
    r = (r <= 1:dim);
    dr = d & r;
    idx = find( dr, 1 );
    rhs = invblkdiag( A(idx+1:end,idx+1:end) );
    lhs = A(1:idx,1:idx);
    B = [{lhs} rhs];  % This construct is necessary to handle symbolic stuff correctly too
end

function [ x ] = last( x, val );
    if( isempty(x) );
        x = val; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
