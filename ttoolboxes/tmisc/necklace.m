function [ ret ] = necklace( len, num, total )
% number of necklaces (without mirroring)
% of length `len` with `num` different colours
%
% See also: genNecklaces
%
% Written by: ??

    ret = 0;
    
    if( nargin >= 3 && total );
        for i = 1:len
            ret = ret + necklace( i, num ); end;
        return; end;
    
    
    di = divisor( len );
    for i = di
        ret = ret + totient( i ) * num^(len/i); end;
    ret = ret / len;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
