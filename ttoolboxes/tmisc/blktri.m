function [ C ] = blktri( varargin );
% makes a block triangular matrix from the given input matrices
% [ C ] = blktri( A1, A2, ..., An, 'right' );
%
% Input:
%   Ai          matrices,
%   method      string, one of
%                   'right','default','',[]: For this: size(Ai, 2) >= size(Aj, 2) for each i<j, i.e. the number of columns of the matrices must be monotone decreasing
%                                            The output matrix C has the form
%                                               C = [ [A1               ]
%                                                     0   [A2           ]
%                                                     0   ....
%                                                     0   0   0   0  [An] ]
%                                             where the 0 are zero matrices of the appropriate size. See the example, if the exaplanation is not clear
%
%
%                   'fill':  The output matrix has the form               
%                                               C = [ A1  *   *   *   *          
%                                                     0   A2  *   *   *       
%                                                     0      ....     *
%                                                     0   0   0   0   An ]
%                            where the upper right corner is filled with random values generated from the matrices A1 to An
% Output:
%   C       a block triangular matrix
%           
%
% Example:
%   blktri( 1*ones(2,5), 2*ones(3,2), 3*ones(2,1) )
%
% See also: invblktri, blkdiag, invblkdiag
%
% Written by: tommsch, 2023-06-28

    
    if( numel( varargin ) >= 1 && ...
        (ischar( varargin{end} ) || isstring( varargin{end} )) ...
      );
        method = varargin{end};
        varargin(end) = []; 
    else;
        method = 'default'; end;

    if( isanyof( method, {[],'right','default',''}) );
        C = blktri_right( varargin{:} );
    elseif( isanyof( method, {'fill','fillr','fillru','fillright','fillrightup','fillrightupper'} ) );
        C = blktri_fill( varargin{:} );
    end;
    
end

function [ C ] = blktri_right( varargin );
    if( isempty(varargin) );
        C = [];
        return; end;
    r = cellfun( 'size', varargin, 1 );  % number of rows of the matrices
    c = cellfun( 'size', varargin, 2 );  % number of columns of the matrices
    
    assert( all( diff(c) <= 0 ), 'blktri:input', 'number of columns of input matrices must be monotone decreasing' );
    
    C = zeros( [sum(r) c(1)] );
    r_run = 1;
    for i = 1:numel( varargin );
        idx_r = r_run -1 + [1:size( varargin{i}, 1 )];  
        idx_c = (c(1)-c(i)+1):c(1);
        C(idx_r,idx_c) = varargin{i};
        r_run = r_run + size( varargin{i}, 1 ); end;
    

end


function [ C ] = blktri_fill( varargin );
    for i = numel( varargin ):-1:1;
        if( isempty(varargin{i}) );
            varargin(i) = []; end; end;
    for i = 1:numel( varargin );
        for j = (i+1):numel( varargin );
            idx_c = randi( size( varargin{i}, 2 ) );
            idx_r = randi( size( varargin{j}, 1 ) );
            varargin{i} = [varargin{i} varargin{i}(:,idx_c)*varargin{j}(idx_r,:)]; end; end;
    C = blktri_right( varargin{:} );
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*NBRAK>
