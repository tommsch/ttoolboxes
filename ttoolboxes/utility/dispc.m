function [ ret ] = dispc( obj );
% Convenience function for disp returing the displayed stuff
% [ ret ] = dispc( obj );
% Input:
%   obj     anything
% Output:
%   ret     string which is (cell-)displayed using disp(obj)
%           Note that [] is dispc-ed as '[]' and {} is dispc-ed as '{}'
%
% E.g.: str = dispc( [1 2 4] )
%
% Written by: tommsch, 2021-01-22

    if( iscell(obj) );
         if( isempty(obj) );
            ret = '{}';
        else;
            ret = strtrim( evalc('celldisp( obj )') ); end;
    else;
        if( isempty(obj) );
            ret = '[]';
        else;
            ret = strtrim( evalc('disp( obj )') ); end; end;
end

function dummy; end  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
