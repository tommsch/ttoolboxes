function [ varargout ] = hexprint( r )
% prints out numbers in hex format with fixed with
% Possible types: complex, double, single, uint64, ..., int8
    if( isnumeric(r) && ~isreal(r) );
        str_r = hexprint( real(r) );
        str_i = hexprint( imag(r) );
        str = [str_r ' + 1i * ' str_i];
    else;
        switch( class(r) );
            case 'double';
                int = typecast( r, 'uint64' );
                sign =     bitand(           int, 0x8000000000000000, 'uint64' );
                exponent = bitshift( bitand( int, 0x7FF0000000000000, 'uint64' ), -52 );
                fraction = bitand(           int, 0x000FFFFFFFFFFFFF, 'uint64' );
                sign = tif( sign, '-', '+' );
                %fprintf( '%s(1+0x%X).2^0x%X', sign, exponent, fraction); 
                str = sprintf( '%s%03x_%013x', sign, exponent, fraction);
            case 'single';
                int = typecast( r, 'uint32' );
                sign =     bitand(           int, 0x80000000, 'uint32' );
                exponent = bitshift( bitand( int, 0x7F800000, 'uint32' ), -22 );
                fraction = bitand(           int, 0x007FFFFF, 'uint32' );
                sign = tif( sign, '-', '+' );
                str = sprintf( '%s%03x_%06x\n', sign, exponent, fraction);
            case {'uint64','int64'};
                str = sprintf( '%016x', r );
            case {'uint32','int32'};
                str = sprintf( '%08x', r );
            case {'uint16','int16'};
                str = sprintf( '%04x', r );
            case {'uint8','int8'};
                str = sprintf( '%02x', r );            
            otherwise;
                assert( false ); end; end;
        
if( nargout == 0 );
    fprintf( '%s\n', str );
else;
    varargout{1} = str; end;
        
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
