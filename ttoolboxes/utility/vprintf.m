function varargout = vprintf( string, varargin );
% [ [str] ] = vprintf( string, args, [options])
% vprintf( string, args, [options], 'save' )
% [ [str] ] = vprintf( string, 'load', ['str', str] )
%
% More powerful version of sprintf.
%
% Input:
% %%%%%%%%%%%
%   string              A format string as used by fprintf
%                       vprintf knows the special format strings %v and %r
%                           '%v'        passes argument to vdisp, i.e. prints out nearly anything
%                           '%r'        very compact printing of arrays
%   args                The arguments as given for fprintf
%
%
% Options:
% %%%%%%%%%%%%%%
%   'cpr',val           string, Uses cprintf to print out the text. Uses the format defined by val
%   'imp',[imp val]     Prints only if imp>=val. If imp>=val-1 and 'string',str is given, then the output is appended to str.
%                           if val >= importance    : varargin is passed to fprintf/cprintf
%                           if val >= importance-1  : if str is given (as a string), then the output is appended to str and returned
%                           if val <  importance-1  : nothing happens
%   'sze',[imp val]     Similar behaviour as for imp. But does print 'BIG SIZE SKIP\n' instead of not printing
%   'str',str           string, default='', Output is appended to str and returned (ignores 'imp')
%   'npr'               No print output is generated
%   'once',val          Only executes the command once. Possible values:
%                           val == 1  : saves the command such that further calls of it are not exectued
%                           val == -1 : Clears all saved commands, i.e. all commands are executed again when called
%                           E.g.: vprintf( 'test\n', 'once', 1 ); vprintf( 'test\n', 'once', 1 ); vprintf( 'once', -1 ); vprintf( 'test\n', 'once', 1 );
%
%   'save'              (experimental) Does not print the message, until vprintf('load') is called.
%   'load'              (experimental) Prints out all saved messages
%
%
% Output
% %%%%%%%%%%%
%   str                 (optional), string, the printed string
%                       Not set if the options 'save' is used
%
% Note:
% %%%%%%%
%  Options 'save' and 'once' store all input in a persistent variable. Thus, do not use these options if you pass much data.
%
%
% E.g.: [str] = vprintf('Talk %i me: \n%v\n', 2,[1 2 3; 3 2 1],'cpr',[.5 .5 0],'str','You! ')
%       vprintf( '%\nr', {[1 2 3] [1 2 3 4] [1 2 3 4 5] [1 2 3 4 5 6]} )
%
% Depends on: vdisp, m-toolbox
%
% See also: vdisp, fprintf, cprintf
%
% Written by: tommsch, 2018

%               2019-10-08,     tommsch,    Added option 'npr'
%               2019-01-08,     tommsch,    Added option 'once'
%               2020-02-05,     tommsch,    Added options 'save' and 'load'
%               2020-06-26,     tommsch,    Added multiple inputs/outputs for option 'str'
%               2020-07-08,     tommsch,    Option %r now can also print flat cell arrays
%                                           Removed possibilty of multiple 'str's
%               2020-09-12,     tommsch,    Bugfix for option %r
%               2020-09-24,     tommsch,    Added format specifiers for %r
%               2020-10-12,     tommsch,    Bugfix. Now one can also do print the strings 'cpr', 'npr', ...
%               2020-10-25,     tommsch,    Behaviour change, %v inserts a newline character when the to-displayed text contains at least one newline character
%               2022-03-09,     tommsch,    Experimental behaviour change, cpr-format 'err' now prints to stderr and is much faster
%               2022-11-12,     tommsch,    Behaviour change, With option <'cpr','err'> the output buffer is flushed before and after the print command
%               2024-10-01,     tommsch,    Deprecated %r specifier
%               2024-10-10,     tommsch,    Behaviour change of %r specifier: %\xr option is removed
% Changelog:    2024-10-25,     tommsch,    Updated thirdparty file cprintf to version 1.14, 2022/03/26 20:48:51

% Add option such that output is only in return value/screen/nowhere/both
% Maybe remove behaviour that output in 'ret' is different to screen output (except for option 'npr')

% XX This is not working: vprintf( '%s%s', 'cpr','err' )

%#ok<*ALIGN>


    persistent oncearray;
    persistent savemessage;
    [str, varargin] =      parsem_fast( {'str'}, varargin, '' );
    
    [saveflag, varargin] = parsem_fast( {'save'}, varargin );
    if( saveflag )
        assert( nargout == 0, 'vprintf:nargout', 'Option ''save'' cannot be used together with output arguments.' );
        savemessage{end+1} = varargin;
        return; end;
    
    [loadflag, varargin] = parsem_fast( {'load'}, varargin );
    if( loadflag )
        [str, varargin] = parsem_fast( {'str'}, varargin, '' );
        expect( isempty(varargin), 'vprintf: ''load'' should not be called with further arguments.' );
        for i = 1:numel( savemessage );
            str = vprintf( savemessage{i}{:}, 'str',str ); end;
        savemessage = {};
        if( nargout );
            varargout{1} = str; end;
        return; end
    
    [oncearray_flag, varargin] = parsem_fast( {'once'}, varargin, 0 );
    if( isempty(oncearray_flag) );
        oncearray = {}; end;
    if( oncearray_flag == -1 );  % reset
        oncearray = {};
        if( nargout );
            varargout{1} = str; end;
        return; end;
    if( oncearray_flag == 1 );
        if( searchincellarray(varargin, oncearray, 0) )
            if( nargout );
                varargout{1} = str; end;
            return;
        else;
            oncearray{end+1} = varargin; end; end;
    
    [imp, varargin] = parsem_fast( {'imp'}, varargin, [1 1] );
    if( imp(2) < imp(1) && nargout == 0 );
        if( nargout );
            varargout{1} = str; end;
        return; end;
    [npr, varargin] = parsem_fast( {'noprint','npr'}, varargin );
    [sze, varargin] = parsem_fast( {'sze'}, varargin, [1 1] );
    if( sze(2) > sze(1)*4-1 );
        txt = sprintf('BIG SIZE SKIP.');
        if( imp(2) >= imp(1) && ~npr );
            disp(txt); end;
        if( nargout );
            varargout{1} = [str txt]; end;
        return; end;
    [format, varargin] = parsem_fast( {'cpr'}, varargin, '' );
    
    % parse input string and replace '%v' by '%s%' and the corresponding argument by the output of vdisp(argument)
    percentidx = strfind( string, '%' );
    i = 1;  % counts the pairs "%-something"
    j = 1;  % counts the numbers in percentidx
    while( true );
        if( j > size(percentidx, 2) );
            break; end;
        if( string(percentidx(j)+1) == '%' );
            j = j + 2;
        elseif( string(percentidx(j)+1) == 'v' );
            assert( size(varargin, 2) >=  i, 'vprintf:nargin', 'Number of arguments wrong' );
            varargin{i} = vdisp( varargin{i} );
            if( contains( varargin{i}, newline ) );
                string = [string(1:percentidx(j)-1) '\n%s' string(percentidx(j)+2:end)];
                percentidx = percentidx + 2;  % since we added '\n' we have to shift the indices
            else;
                varargin{i} = strtrim( varargin{i} );  % XX I do not know whether this is a good idea
                string = [string(1:percentidx(j)-1) '%s' string(percentidx(j)+2:end)]; end;
            i = i + 1;
            j = j + 1;
        elseif( string(percentidx(j)+1) == 'r' );
            varargin{i} = dispc_compact( varargin{i} );
            string = [string(1:percentidx(j)-1) '%s' string(percentidx(j)+2:end)];
            i = i + 1;
            j = j + 1;
        else;
            i = i + 1;
            j = j + 1; end;
    end;
    
    assert( size(varargin, 2) == i-1, 'vprintf:nargin', 'Number of arguments wrong' );  % test if number of arguments is correct
    
    
    if( imp(2) >= imp(1) && ~npr );
        if( isequal(format, 'err') );
            if( ismatlab ); drawnow( 'update' ); else; drawnow; end;
            fprintf( 2, string, varargin{:} );
            if( ismatlab ); drawnow( 'update' ); else; drawnow; end;
        elseif( isoctave || isempty(format) );
            fprintf( string, varargin{:} );
        else;
            cprintf( format, string, varargin{:} );
            end; end
    
    if( nargout );
        txt = sprintf( string, varargin{:} );
        varargout{1} = [str txt]; end;

end

%%

function ret = ismatlab();
    % returns true when run on m
    persistent x;
    if( isempty (x) );
        x = ~exist( 'OCTAVE_VERSION', 'builtin' ); end;
    ret = x;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
