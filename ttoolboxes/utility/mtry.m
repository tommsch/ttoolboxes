function [ i, varargout ] = mtry( varargin );
% multi-try-catch block
% [ i, varargout ] = mtry( h );
% [ i, varargout ] = mtry( h1, ..., hn );
%
% Input:
%   h       cell array of function handles
%   hi      function handles
%
% Output:
%   i           index of successfull function handle
%   varargout   the output of the function handle
%
% Note:
%   If all function handles error out, than the last error is thrown
%
% Written by: tommsch, 2018

    if( numel(varargin) == 1 && iscell(varargin{1}) );
        h = varargin{1};
    else;
        h = varargin; end;
    
    varargout = cell( 1, nargout );
    if( nargin == 0 );
        i = [];
        return; end; 
    if( ~iscell(h) );
        h = {h}; end;
    for i = 1:numel( h );
        try;
            if( ~iscell(h{i}) );
                h{i} = {h{i}}; end; %#ok<CCAT1>
            for j = 1:numel( h{i} );;
                [varargout{:}] = h{i}{j}(); end;
            break;
        catch me;
            if( i == numel(h) );
                rethrow( me ); end; end; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
