function [ out ] = tif( varargin );
% Ternary if operator
% [ out ] = tif( cond1, ans1, cond2, ans2, cond3, ans3, ..., default )
% out = truth & yes : no ;
%
% E.g.: tif( true, 1, 2 )
%       tif( false, 1, true, 2, 3 );
%
%               2023-02-14,     tommsch,    Support for multiple tifs. Last argument is default
% Changelog: 

    for i = 1:2:nargin-1;
        if( isAlways( varargin{i} ) );
            out = varargin{i+1}; 
            return; end; end;
    out = varargin{i+2};
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
