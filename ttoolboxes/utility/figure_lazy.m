function [ varargout ] = figure_lazy( fig );
    % sets focus to figure with number fig, but does not bring it to the front in case it already exists
    if( nargin == 0 );
        figure;
    else;
        try;
            set( 0, 'CurrentFigure', fig );
        catch;
            figure( fig ); end; end;
    
    if( nargout >= 1 );
        varargout = cell( 1, nargout );
        varargout{1} = gcf; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>