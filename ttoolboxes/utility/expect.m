function expect( varargin );
% expect( ['break'|'keyboard'|'pause'|'default'], cond, [errID], msg, A1, ..., An )
% Input:
%   ['break'|'keyboard'|'pause']    string, if given, the specified action happens additionally. For ''default'', nothing happens additionally
%                                       'break','keyboard'      The debugger is started
%                                       'pause'                 A Message is printed, and keyinput is needed
%   cond                            bool, if false, a warning is printed
%   [eerID]                         string in the form of a Matlab exception id, the warning to be thrown when cond is false
%   msg, A1, ..., An                fprintf arguments
%
% Output:
%   Throw warning if condition is false
%   According to the options, the
% 
% See also: assert
%
% Written by: tommsch, 2020
%            tommsch, 2023-03-15,   Added options 'break','pause' as first positional arguments
%            tommsch, 2023-05-15,   Added option precond
%            tommsch, 2024-05-25,   Removed option precond
% Changelog: 

    % get action
    action = 'default';
    if( numel( varargin ) >= 1 && ...
        (ischar( varargin{1} ) || isstring( varargin{1} )) && ...
        isanyof( varargin{1}, {'break','keyboard','pause','default'} ) ...
      );
        action = varargin{1};
        varargin(1) = []; end;
    
    if( ~isAlways(varargin{1}) );
        if( nargin == 1 );
            warning( 'expect:failed', 'Expect failed.' );
        else;
            warning( varargin{2:end} ); end; 
        switch action;
            case {'default'};
                % do nothing
            case {'break','keyboard'};
                keyboard;
            case 'pause'; 
                fprintf( 2, 'Press a key to continue.\n' );                
                pause;
            otherwise;
                error( 'expect:opt', 'Wrong option given.' ); end; end;
end
        
function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*MCKBD,*KEYBOARDFUN>

