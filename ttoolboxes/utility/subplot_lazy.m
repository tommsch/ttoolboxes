function [ varargout ] = subplot_lazy( varargin );
% [ ax, mn ] = subplot_lazy( tot, p, [ratio] );
% [ ax ] = subplot_lazy( ['reorder'], p );
% wrapper for subplot; Only needed to specify the total number of subplots
%
% Note: You most likely want to use `subfigure` instead of `subplot_lazy`
%
% See also: subfigure
%
% Written by: tommsch, 2022

    if( isa(varargin{1}, 'matlab.ui.Figure') );
        hFig = varargin{1};
        figure_lazy( hFig );
        varargin(1) = [];
    else;
        hFig = gcf; end;
    if( numel(varargin) == 2 && isanyof( varargin{1}, {[],'reorder'} ) );
        ax = subplot_lazy_reorder( hFig, varargin{2} );
    elseif( numel(varargin) == 1 );
        ax = subplot_lazy_reorder( hFig, varargin{1} );
    else;
        if( numel( varargin ) <= 2 || ...
            isempty( varargin{3} ) ...
          );
            varargin{3} = 1; end;
        [ax, mn] = subplot_lazy_tot( varargin{:} );
    end;
    
    if( nargout >= 1 );
        varargout = cell( 1, nargout ); end;
    if( nargout >= 1 );
        varargout{1} = ax; end;
    if( nargout >= 2 );
        varargout{2} = mn; end;
    
end

function [ ax ] = subplot_lazy_reorder( hFig, p );

    num = getnum( hFig );
    
    if( ~isempty(num) && all(p <= num) );
        % just return axis of subplot
        ax = subplot_lazy( hFig, num, p );
        return; end;
    
    tmpFig = figure( 'Visible','off' );  % we need a new figure, because subplot deletes handles
    %tmpFig = figure;  % for Debugging
    clear_tmpFig = onCleanup( @() close(tmpFig) );
    
    %setappdata( hFig, 'ttoolboxes_subfigure_num', p );

    % reorder all current subplots
    hAxes = findobj( allchild(hFig), 'flat', 'Type','axes' );
    for i = num:-1:1;
        hSub(i) = subplot_lazy( p, i );
        set( hAxes(num-i+1), 'Position', get(hSub(i), 'Position') );
        end;
    for i = 1:num;
        delete( hSub(i) ); end;
    
    clear tmpFig  % we need to clear this variable before we calling figure( hFig )
    ax = subplot_lazy( hFig, p, p );

end

function [ ax, mn ] = subplot_lazy_tot( tot, p, ratio );
    [m, n] = compute_mn( tot, ratio );
    if( p(end) == inf );
        p(end) = m*n; end;
    ax = subplot( m, n, p );
    mn = [m n];
end

function [ m, n ] = compute_mn( tot, ratio );
    tot = max( tot );
    n = sqrt( tot/ratio );
    m = n * ratio;
    m = round( m );
    n = round( n );
    while( true );
        if( m*n < tot ); m = m + 1; else; break; end;
        if( m*n < tot ); n = n + 1; else; break; end; end;
end

function [ num ] = getnum( hFig );
    num = length( findobj(hFig, 'type','axes' ) );  % total number of figures so far
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
