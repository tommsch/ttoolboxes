function [ varargout ] = cls( varargin )
    if( nargout == 0 );
        clc( varargin{:} );
    else;
        varargout = cell( 1, nargout );
        [varargout{:}] = clc( varargin{:} ); end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
