function [ out, err ] = anycast( varargin );
% [ out, [err] ] = anycast( in1, ... inn, outtype, ['unsafe'] );
% [ out, [err] ] = anycast( in1, ... inn, 'like',p, ['unsafe'] );
% [ out, [err] ] = anycast( in, 'outtype1', ..., 'outtypen', ['unsafe'] );
% Casts to type `outtype`
% Chooses the best input for the cast.
% Empty arguments are discarded, except all are empty
%
% Input:
%   in1, ..., inn                   representation of the same thing in different types
%   type                            string, type of the output
%                                   if 'type' is empty, the first non-empty argument is returned verbatim. This can be used 
%   'like',p                        variable of the output type
%   'outtype1', ..., 'outtypen'     strings, if multiple types are given, then anycast starts to cast with the first given, and continuous until one cast succeeds
%
% Options:
%   'unsafe'            flag, when given certain unsafe casts (which are likely to alter the value) 
%                       are tried too. In particular, they are tried first.
%
% Output:
%   out                 cast of the input to type `outtype`
%   err                 if nargout == 2, no exceptions are thrown. Instead cast-failures are signalled by setting err
%                           err = 0  ...  cast succeeded
%                           err = 1  ...  cast failed, out is set to []
%
% Notes:
%   - For certain standard input/output types, a best conversion sequence is hardcoded in the file,
%     for other types, a random cast is used.
%   - For the output type, there must exist a `cast()` function or a constructor; taking at least one input type.
%   - Some special conversions are hardcoded, which can be used for 'outtypen'
%           'symrat'        throws an error if the input is not an approximation of a rational number or a simple algebraic formula
%                           e.g. anycast( sqrt(2), 'symrat' )  % succeeds and returns sqrt(sym(2))
%                                anycast( 1.18239123, 'symrat' )  % throws an error
%           'intvalrat'     throws an error if the input is not an approximation of a rational number
%                           e.g. anycast( 5/7, 'intvalrat' )  % succeeds and returns [0.09999999999999999 0.100000000000000006]
%                                anycast( 1.181278192, 'symrat' )  % throws an error
%                        
%
% Example:
%   [ out, err ] = anycast( single(1+eps), double(1+eps), 'sym' );
%       % out = sym(1), err = 1
%   anycast( int32(2), 'badtype_uai83gj', '' )  % returns int32(2)
%
% Written by: tommsch, 2022-01-26

    % fast cast for standard casts
    if( numel(varargin) <= 2 );
        try;
            out = cast( varargin{:} );
            err = 0;
            return;
        catch me;  %#ok<NASGU>
            end; end;  % do nothing;
    
    % main part
    [flag,  out, outtype, order, invar, intype, unsafe] = parse_input( varargin{:} );
    if( flag );
        return; end;

    try;
        out = anycast_preworker( outtype, order, invar, intype, unsafe );
        err = false;
    catch me;
        if( nargout == 2 );
            out = [];
            err = true;
        else;
            rethrow( me ); end; end;

end

function [ flag,  out, outtype, order, invar, intype, unsafe ] = parse_input( varargin );

    out = [];
    flag = false;
    if( isequal(varargin{end}, 'unsafe') );  % if flag is set, unsafe casts are tried too
        unsafe = true;
        varargin(end) = [];
    else;
        unsafe = false; end;
    
    if( numel( varargin ) > 2 && ...
        all( cellfun('isclass', varargin(2:end), 'char') | cellfun('isclass', varargin(2:end), 'string') ) ...
      );
        for i = 2:numel( varargin );
            
            [out, err] = anycast( varargin{1}, varargin{i} );  % call recursive with correct arguments
            if( ~err );
                outtype = [];
                order = [];
                invar = [];
                intype = [];
                unsafe = [];
                flag = true;
                return; end; end; end;

    if( numel(varargin) >= 3 && isequal( varargin{end-1}, 'like' ) );
        outtype = class( varargin{end} );
        varargin(end-1:end) = [];
    else;
        assert( isstring(varargin{end}) || ischar(varargin{end}), 'anycast:input', 'Last entry must be the type.' );
        outtype = varargin{end}; 
        varargin(end) = []; end;
    
    invar = varargin;
    assert( numel(invar) >= 1, 'anycast:input', 'No input given.' );
    
    intype = cell( 1, numel(invar) );
    emptyidx = false( 1, numel(invar) );
    for i = 1:numel( invar );
        if( isempty(invar{i}) );
            emptyidx(i) = true; end;
        intype{i} = class( invar{i} ); end;
    if( all(emptyidx) );
        emptyidx(1) = false; end;
    intype(emptyidx) = [];
    invar(emptyidx) = [];

    switch outtype;
        case '';        order = {};
        otherwise;      order = {'sym','vpa','double','single'}; end;    
    
end

%% workers
%%%%%%%%%%%%%%%%%%%%

function [ out ] = anycast_preworker( outtype, order, invar, intype, unsafe );

    % 'order' is used to choose the best input for the cast
    err = true;
    for i = 1:numel( order );
        idx = find( strcmp( intype, order{i} ) );
        if( ~isempty(idx) );
            idx = idx(1);
            
            [out, err] = anycast_worker( invar{idx}, outtype, unsafe );
            intype(idx) = [];
            invar(idx) = [];
            if( ~err );
                break; end; end; end;
    if( err );
        for i = 1:numel( invar );
            [out, err] = anycast_worker( invar{i}, outtype, unsafe );
            if( ~err );
                    break; end; end; end;
    
    if( err );
        error( 'anycast:badcast', 'Cast to %s did not succeed.', outtype ); end;
    
end

function [ out, err, outtype ] = anycast_worker( invar, outtype, unsafe )

    err = true;
    out = [];
    
    % special casts
    %%%%%%%%%%%%%%%%%%%%%%
    % user defined casts first
    outtypenew = outtype;
    if( err );
        try;
            outtypenew = eval( ['usercast_to_' outtype '();'] );
            [out, err] = eval( ['usercast_to_' outtype '( invar );'] );
        catch me;    %#ok<NASGU>
            outtype = outtypenew;
            end; end;
        
    % unsafe cast second
    if( err && unsafe );
        try;
            [out, err, outtype] = eval( ['unsafecast_from_' class(invar) '( invar, outtype );'] );
        catch me;    %#ok<NASGU>
            end; end;
    
    % standard casts
    %%%%%%%%%%%%%%%%%%%%%
        
    % first cast version:    
    if( err ); 
        try;
            out = cast( invar, outtype );
            err = false; 
        catch me;  %#ok<NASGU>
            end; end; 

    % second cast version:
    if( err ); 
        try;
            obj = eval( [outtype '.empty();'] );
            out = cast( invar, 'like', obj );
            err = false; 
        catch me;  %#ok<NASGU>
            end; end; 

    % third cast version:
    if( err ); 
        try;
            out = eval( [outtype '( invar );' ] );
            err = false; 
        catch me;  %#ok<NASGU>
            end; end;
        
    % fourth cast version
    if( err );
        try;
            if( ~isa(invar,'double') );
                [out, err] = anycast_worker( double(invar), outtype, unsafe );
                return;
                end;
            catch me;  %#ok<NASGU>
                end; end;

end

%% %%%%%%%%%%%%%%%%%
% user defined casts
%%%%%%%%%%%%%%%%%%%%%%

function [ out, err ] = usercast_to_symrat( invar );
    % when called without arguments the fallback type must be returned
    
    err = true;
    if( nargin == 0 );
        out = 'badtype_asd8128asd';  % bad type,  if this cast fails, no other cast shall be tried
        return; end;
    
    if( exist ('OCTAVE_VERSION', 'builtin') );
        w = warning( 'off', 'OctSymPy:sym:rationalapprox' );
        clean_w = onCleanup( @() warning(w) ); end;
    
    casted = sym( invar );
    [~,denom] = numden( casted );
    if( denom<1938 );
        err = false;
        out = casted; end;

end


function [ out, err ] = usercast_to_intvalrat( invar );
    % when called without arguments the fallback type must be returned
    
    err = true;
    if( nargin == 0 );
        out = 'badtype_asd8128asd';  % bad type,  if this cast fails, no other cast shall be tried
        return; end;
    
    d = double( invar );
    [num,denom] = rat( d, 0 );
    if( denom<9699690 );
        err = false;
        out = intval( num )./intval( denom ); end;

end
    

%% %%%%%%%%%%%%%%%%%%%%%
% unsafe casts
%%%%%%%%%%%%%%%%%%%

function [ x, err, outtype ] = unsafecast_from_intval( x, outtype );
    err = true;
    switch outtype;
        case {'single','double'};
            x = x.mid;
            err = false; end;
end


function dummy; end  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

