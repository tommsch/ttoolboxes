function condset( name, value )
% condset( name, value )
% Conditionally generates a variable
% Input:
%   name    string, Name of the variable
%   value   anything, Initial value of the variable
%
% Output:
%   assignin's the caller workspace a variable with name `name` and value `value`, only if no variable with this name exists yet
%
% Written by: 2023-10-09, tommsch

%               2023-11-30, tommsch, Behaviour change: Variable will be overwritten if class of `value` is different from the class of the variable to be conditionally set. Warning is thrown in this case
%               2024-07-26, tommsch, Behaviour change: No output argument is allowed
%               2024-10-10, tommsch, Behaviour change: More strict testing whether variable to be generated is not in workspace anymore
% Changelog:

    assert( nargout == 0, 'condset:argout', 'This function must have 0 output arguments currently.' );
    assert( isstring(name) || ischar(name), 'condset:args', 'First input to ''condset'' must be the variable name.' );

    var_list = evalin( 'caller', 'whos' );
    for i = 1:numel( var_list );
        if( isequal(var_list(i).name, name) );
            value_caller = evalin( 'caller', var_list(i).name );
            if( ~isequal(class(value_caller), class(value)) )
                warning( 'condset:type', 'Class of variable is different from specified class. Variable %s will not be overwritten', var_list(i).name );
            elseif( numel(value_caller) ~= numel(value) );
                warning( 'condset:type', 'numel of variable is different from specified variable. Variable %s will not be overwritten', var_list(i).name );
            else;
                end; % everything ok
            return; end; end;
            
    assignin( 'caller', name, value );
    
end

function dummy; end  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
 %#ok<*NOPTS>