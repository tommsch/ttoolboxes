function [ varargout ] = vdisp( varargin )
% [ ret ] = vdisp( C, [str] )
% Compact (but sometimes ugly) display of 'nested arrays', 'nested structs', 'matrices', etc.
% Expands cell arrays inside structs.
%
% Input:
%   C                   the thing to be displayed
%   str                 (experimental) the output is appended to str and returned as ret
%
% Remarks:
%   This function is slow if the return value is used, since it calls 'evalc'.
%   This function is slow if there is a struct inside C, since it calls 'eval' and 'evalc'
%
% Eg: vdisp({[1 2 3],[1;2;3],{[1 1; 2 2; 3 3]}})
%
%
% Depends on: //
%
% See also: vprintf
%
% Written by: tommsch, 2018 
%             Stefan, University of Copenhagen

%            tommsch, 2021-11-22: vdisp returns input
% Changelog: 



    ret = ''; 
    C = varargin{1};
    
    if( nargin == 2 ); 
        str = varargin{2}; 
        assert( isstring(str) || ischar(str), 'vdisp:str', 'The second (optional) argument must be a string.' )
    else; 
        str = ''; end;
    
    if( ismatlab );
        fs = get( 0, 'FormatSpacing' );  % store format spacing, set compact
        clean_fs = onCleanup( @() set( 0, 'FormatSpacing',fs ) );  % restore format spacing
        format compact;
    else;
        [fs_a, fs_b] = format;
        format( 'compact' );
        clean_fs = onCleanup( @() format(fs_a, fs_b) ); end;
    
    ret = vdisp_worker( C, ret, nargout );  % if nargout == 1 we return the displayed string
    if( nargout ); 
        if( nargout <= 1 );
            varargout{1} = strcat( str, ret );
        elseif( nargout <= 2 );
            varargout{1} = strcat( str, ret );
            varargout{2} = varargin;
        else;
            error( 'vdisp:narg', 'Number of output arguments wrong.' ); end; end;

end

function [ ret ] = vdisp_worker( C, ret, saveoutput );

    if( iscell(C) )
        % like celldisp, but omits linebreaks
        for i = 1:numel(C); 
            ret = vdisp_worker( C{i}, ret, saveoutput ); end;
        return;
    elseif( isstruct(C) )
        structret = dispstruct( C, [] );
        if( saveoutput ); 
            ret = [ret, structret];
        else;
            disp( structret ); end;
    else;
        if( size(C,2) == 1 && size(C,1) > 1 ); 
            try;
                C = C.';
                if( saveoutput ); 
                    ret = [ret, '(transposed)']; 
                else; 
                    fprintf( '(transposed)' ); end;
            catch me;  %#ok<NASGU>
                end;  % transpose not possible
            end;
        if( saveoutput ); 
            ret = [ret, evalc( 'disp_handler(C)' )]; 
        else; 
            disp_handler( C ); end;
        
        if( size(C, 1) > 1 ); 
            if( saveoutput ); 
                ret = [ret, newline]; 
            else; 
                fprintf( '\n' ); end; end; end;
end

function disp_handler( C );
    if( isempty(C) && isnumeric(C) );
        disp( '[]' );
    elseif( isempty(C) && iscell(C) );
        disp( '{}' );
    else;
        disp( C ); end;
end

function [ fieldstring ] = dispstruct( stru, substru );
% Display nested structures in 'structvar'; 'substructvar' is used in
%      recursion, must be [] in main call. 
% fieldstring: output
%
% Written by: Stefan, University of Copenhagen 
% Changed by: tommsch, 2019

    fieldstring = [];
    
    for i = 1:numel( stru );
        
        % display main structure at current level of recursion:
        fieldstring = [fieldstring  tif(numel(stru) > 1, ['Struct: ' num2str(i) newline ],'') substru newline evalc( 'disp(stru(i))' ) tif(numel(stru) > 1,newline,'') ];  %#ok<AGROW>
        % get fields names at current level of recursion:
        fields = fieldnames( stru(i) );
        for k = 1:length( fields )
            thing = eval( ['stru(i).' fields{k}] );  %#ok<EVLDOT>
            if( isstruct(thing) );
                fieldstring = [fieldstring newline '.' vdisp( thing, fields{k} )]; %#ok<AGROW>
            else;
                %vdisp( thing, fields{k} );
                end; end; end; 
end

function dummy; end  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
