function maybe_unused( varargin )  %#ok<VANUS>
% A function which takes anything, to silence mlint warnings about unused variables
% Using this function gives more fine grained control, since it silences the mlint warning only for the specified variables