function scale_screen( varargin );
% Scales visual stuff of Matlab, so that it can be more easily seen in presentations
% 
% Input:
%   scale_screen();                             makes everything back to normal
%   scale_screen( ['absolute'], absfactor );    double, scales everything to an absolute factor of absfactor
%   scale_screen( 'relative', reffactor );      scales everything by a relative factor of relfactor
%
% Options;
%   'verbose,val                                integer, default = 1, verbosity level
%   'demo'                                      makes some random figures, can be used for debugging
%   'override,fact                              double, can be used to set the new scaling factor by hand
%
% Notes:
%   - Some things are not scaled on purpose:
%       - Matlab GUI (Toolbar, ...)
%       
%   - Some things are not scaled, although they should:
%       - markersize
%       - labels in graphs
%
%   -  Iterative scaling of figures fails often.
%       
% Written by: tommsch, 2024-07-17

%               tommsch,    2024-26-09, Better scaling of fonts in graph windows
% Changelog:
    opt = parse_input( varargin{:} );
    
    if( opt.factor ~= 1 );
        scale_figure( opt ); 
        scale_gui( opt );
        set_default( opt );
        end;

end

function [ opt ] = parse_input( varargin );
    has_args = numel( varargin ) > 0;
    [opt.demoflag, varargin]   = parsem( {'demo'}, varargin );
    if( opt.demoflag );
        opt.factor = 1;
        print_demo(); 
        return;
        end;

    persistent total_factor;
    if( isempty(total_factor) );
        total_factor = 1; end;

    if( numel(varargin) >= 1 && isscalar(varargin{1}) );
        absolute_factor = varargin{1};
    else;
        absolute_factor = []; end;

    [relative_factor, varargin] =   parsem( {'relative'}, varargin, [], 'postprocess',@str2numt );
    [absolute_factor, varargin] =   parsem( {'absolute','factor','new_factor','total_factor','new','total'}, varargin, absolute_factor, 'postprocess',@str2numt );
    [opt.verbose, varargin] =       parsem( {'verbose','v'}, varargin, 1, 'postprocess',@str2numt );
    [override, varargin] =          parsem( {'override'}, varargin, [], 'postprocess',@str2numt );
    if( ~isempty(override) );
        total_factor = override; end;

    if( numel( varargin ) >= 1 && ...
        (ischar( varargin{1} ) || isstring( varargin{1} )) && ...
        ~isempty( str2num(varargin{1}) ) ...
      );  %#ok<ST2NM>
        absolute_factor = str2num( varargin{1} );  %#ok<ST2NM>
        varargin(1) = [];
    elseif( numel( varargin ) >= 1 && ...
            isnumeric( varargin{1} ) && ...
            isscalar( varargin{1} ) ...
          )
        absolute_factor = varargin{1};
        varargin(1) = []; end;

    parsem( varargin, 'test' );

    if( ~isempty(relative_factor) );
        assert( isempty(absolute_factor) );
        opt.factor = relative_factor;
    elseif( isempty(absolute_factor) );
        absolute_factor = 1; end;

    if( ~isempty(absolute_factor) );
        opt.factor = absolute_factor / total_factor; end;

    if( opt.verbose >= 1 );
        fprintf( 'Old scaling factor: %2.1f\n', total_factor ); end;
    total_factor = total_factor * opt.factor;
    opt.total_factor = total_factor;

    if( opt.verbose >= 2 );
        if( ~isempty(absolute_factor) );
            fprintf( 'Set to factor: %2.1f\n', absolute_factor );
        else;
            fprintf( 'Scale by factor: %2.1f\n', opt.factor ); end; 
        fprintf( 'Current total factor: %2.1f\n', opt.total_factor ); end;
    
    if( opt.factor == 1 && ~has_args );
        fprintf( 'Add the scale factor as second argument. E.g.: <a href="matlab: scale_screen 2">scale_screen 2</a>\n' ); end;
    
end

function scale_figure( opt );

    figHandles = findobj( 'Type','figure' );

    factor_arrowsize  = opt.factor;
    factor_linewidth  = opt.factor;
    factor_markersize = opt.factor;
    factor_fontsize   = sqrt( opt.factor );
    factor_ticklength = sqrt( opt.factor );

    for i = 1:numel( figHandles );
        fig = figHandles(i);

        if( ~verLessThan('matlab', '9.12') );
            fontsize( fig, 'scale', factor_fontsize ); end;

        obj = findall( fig, 'Type','Line' );
        for j = 1:numel( obj );
            lw = obj(j).LineWidth; 
            ms = obj(j).MarkerSize;
            set( obj(j), 'LineWidth',factor_linewidth * lw, ...
                         'MarkerSize',factor_markersize * ms ...
               );
            end;


        obj = findall( fig, 'Type','Axes' );
        for j = 1:numel( obj );
            fs = obj(j).FontSize;
            lw = obj(j).LineWidth; 
            tl = obj(j).TickLength;
            set( obj(j), 'FontSize',factor_fontsize * fs, ...
                         'LineWidth',factor_linewidth * lw, ...
                         'TickLength',factor_ticklength * tl ...
               );
            end;

        obj = findall( fig, 'Type','GraphPlot' );
        for j = 1:numel( obj );
            as  = obj(j).ArrowSize;
            efs = obj(j).EdgeFontSize;
            lw  = obj(j).LineWidth;
            ms  = obj(j).MarkerSize;
            nfs = obj(j).NodeFontSize;
            set( obj(j), 'EdgeFontSize',factor_fontsize * efs, ...
                         'LineWidth',factor_linewidth * lw, ...
                         'MarkerSize',factor_markersize * ms, ...
                         'NodeFontSize',factor_fontsize * nfs ...
               );            
            try;
                set( obj(j), 'ArrowSize',factor_arrowsize * as ...
                   );
            catch me;  %#ok<NASGU>
                end; end;

        obj = findall( fig, 'Type','Scatter' );
        for j = 1:numel( obj );
            lw = obj(j).LineWidth;
            set( obj(j), 'LineWidth',factor_linewidth * lw ...
               ); end; end;



end

function scale_gui( opt );

    factor_fontsize = sqrt( opt.total_factor );

    s = settings;
    try;
        err = lasterror;  %#ok<LERR>
        s.matlab.fonts.codefont.Size.TemporaryValue = s.matlab.fonts.codefont.Size.PersonalValue * factor_fontsize;
    catch me;  %#ok<NASGU>
        lasterror( err );  %#ok<LERR>
        s.matlab.fonts.codefont.Size.PersonalValue = s.matlab.fonts.codefont.Size.ActiveValue;
        s.matlab.fonts.codefont.Size.TemporaryValue = s.matlab.fonts.codefont.Size.PersonalValue * factor_fontsize;
        end;
end

function set_default( opt );

    persistent prop;
    persistent names;
    if( isempty(prop) );
        prop = get( groot, 'factory' );
        names = fieldnames( prop ); 
        for i = 1:numel( names );
            names{i} = names{i}(8:end); end; end;  % remove ''factory''
    
    % set default values
    for i = 1:numel( names );
        value = prop.(['factory' names{i}]);
        if( endsWith( names{i}, {'LineWidth','MarkerSize','HeadLength','HeadWidth'} ) );
            set_default_factory( names{i}, opt.total_factor );
        elseif( endsWith( names{i}, 'FontSize' ) );
            set_default_factory( names{i}, sqrt(opt.total_factor) );
        %elseif( endsWith( names{i}, 'Multiplier' ) );
        %    set_default_factory( names{i}, 1 );
        elseif( endsWith( names{i}, {'Width', 'Length'} ) && isscalar(value) && isnumeric(value) );
            set_default_factory( names{i}, opt.total_factor );
        elseif( endsWith( names{i}, {'Alpha','Reflectance','OthersValue','SpecularStrength','DiffuseStrength','HistogramValues','AmbientStrength'} ) );
            % do nothing
        %elseif( isscalar(value) && isnumeric(value) );
        %    set_default_factory( names{i}, opt.total_factor );
            end; end;
    
    % register callback
    % set( 0, 'defaultFigureCreateFcn', @(varargin) set_default_values_for_new_figure( opt, varargin{:}) );  % does not work
end

function set_default_factory( name, factor );
    try;
        %disp( name );
        value = get( groot, ['factory' name] ) * factor;
        set( groot, ['default' name], value );
    catch me;
        disp( name );
        fprintf( 2, 'Could not set property: %s\n', name ); 
        disp( me );
        end;
end

%%

function [ h ] = set_default_values_for_new_figure( opt, varargin );  %#ok<STOUT,DEFNU>
    %h = figure( varargin{:} );
    scale_figure( opt );
end

%%

function print_demo();
    G = [0 0 1 1;1 1 0 0;1 1 0 0;0 0 1 1];
    figure( 1 )
    graph_precomponent( G, [], 2 );
    subplot_lazy( 4, 1 )
    plotm( randn(2, 10) );
    subplot_lazy( 6, 2 )
    plotm( randn(2, 10) );

    figure( 2 );
    subplot_lazy( 2, 1 );
    scatter( randn(1, 10), randn(1, 10), 20*rand(1, 10) )
    title( 'TITLE' );
    subtitle( 'SUBTITLE 1' );

    figure( 3 );
    T = digraph( randn( 10 ) < .5 );
    plot( T );
    title( 'TITLE 2' );
    text( .4, .2, .5, 'This is a text' );

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

