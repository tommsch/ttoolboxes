function [] = keyinput( flag );
% keyinput( [flag] );
% Waits for a key-press
%
% Options:
%   flag    bool, default=true, If false, nothing happends
%
% Written by: tommsch, 2022, Using code from FEX File ID #7465 of Jos van der Geest
%  

    if( nargin <= 0 );
        flag = 1; end;

    num_written = 0;
    if( nargin > 0 && flag == false );
        return; end;

    poststr = '';
    str = sprintf( '\nPress key: (c)ontinue, (d)ebugger, (a)abort ' );
    num_written = num_written + numel( str );
    fprintf( '%s', str );
    while( true );
        chr = getk();
        switch lower( chr );
            case 'c';
                remove_output( num_written, poststr );
                break;

            case {'','d','k'};
                poststr = '\n';
                remove_output( num_written, poststr );
                str = sprintf( 'Switch to debug mode:\n' ); 
                fprintf( str );
                num_written = numel( str );
                poststr = '';
                keyboard;  %#ok<KEYBOARDFUN,MCKBD>
                remove_output( num_written, poststr );
                break;

            case {'control c','a'};
                remove_output( num_written, poststr );
                error( 'ipa:abort', '\nProgram aborted due to user input' );

            otherwise; 
                str = sprintf( 'Wrong input.' ); 
                fprintf( str );
                num_written = num_written + numel( str );
        end; end;
        
    

end

function remove_output( num_written, poststr );
    removestr = [repmat( '\b', [1 num_written] ) poststr];;
    fprintf( removestr );
end

function [ ch ] = getk();
% GETK - wait for a keypress
%
% The function GETK fulfills the similar tasks as its prototype, the
% function GETKEY. It waits for a keyboard keypress, stores the key
% identifier and a period of the waiting. Since the new function behaves
% differently from its origin, it has got another name. Appart from GETKEY,
% the function GETK comprises names of ascii characters and non-ascii keys
% into a single string. The function creates a modal figure window with 
% a name 'PRESS A KEY' at a required place of the screen.
%
%   CH = GETK waits for a single keypress. It accepts all ascii characters,
%   including backspace '\', space ' ', etc, and non-ascii keys (ctrl, alt,
%   return, ...) that can be typed on the keyboard,  Each ascii key
%   generates a single character. The other keys generate strings of their
%   names. The figure window appears at a current position of the mouse
%   cursor.  
%
%   [CH,T] = GETK(...) returns also the time T elapsed between the start of
%   the function and each keypress. This is, however, less accurate.
%
% Examples:
%
% for Matlab 6.5 and higher
% author : Miroslav Balda
% e-mail : miroslav@balda.cz
% History
% 2012-07-10, v.1.0 created by a modification of the function GETKEY v.2.0,
%                   FEX File ID #7465 of Jos van der Geest
%
% Changed by: tommsch, 2022-02-14

    narginchk( 0, 0 );      % check the input arguments
    
    % Determine the callback string to use
    callstr = 'set( gcbf, ''Userdata'', get(gcbf, ''Currentkey'') ) ; uiresume;' ;
    %callstr = 'set( gcbf, ''Userdata'', get(gcbf, ''Currentkey'') ) ;' ;
    % Set up the figure
    fh = figure(...
        'name','PRESS A KEY', ...
        'keypressfcn', callstr, ...
        'windowstyle','modal',...
        'numbertitle','off', ...
        'position',  [0 1 .1 .1],...
        'userdata','timeout');
    
    ch  = {};
    while( true );
        uiwait;  % Wait for a key press
        ch{end+1}  = [get(fh,'Userdata'),' '];   %#ok<AGROW> % and the key itself
        
        if( numel( strtrim(ch{end}) ) == 1 || ...
            strcmp( ch{end}, 'space' ) ...
          );
            break; end; end;
    
    ch = unique( ch, 'stable' );
    len = cellfun( 'prodofsize', ch );
    [~,idx] = sort( len );
    ch = ch(idx(end:-1:1));
    ch  = [ch{:}];
    ch(end)  = [];
    
    % clean up the figure, if it still exists
    if( ishandle(fh) );
        delete( fh ); end

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

