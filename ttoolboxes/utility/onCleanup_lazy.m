classdef onCleanup_lazy < handle;
% Slightly more versatile than the original `onCleanup` function
% [ obj, arg_1, ..., arg_n ] = onCleanup_lazy( h, arg_1, ..., arg_n );
% (experimental) [ obj, arg_1, ..., arg_n ] = onCleanup_lazy( h(x1, ..., xn), func );
%
% Input:
%   h           function handle taking n arguments
%   arg_i       arguments to be passed to h when object is destroyed
%   func        (experimental) function handle returning n output arguments. The n outputarguments [x1, ..., xn] = func() are passed to h at destruction time
%
% Output:
%   arg_i       the input arguments
%
% Examples:
%   [clean_dir, current_dir] = onCleanup_lazy( @(x) cd( x ), cd( new_folder ) );  % changes folder to new_folder, goes back to original folder, returns current folder in `current_dir`
%   clean_warn = onCleanup_lazy( @(x) warning( x ), warning() ) );  % changes warning settings back
%
% Written by: tommsch, 2023

%            tommsch, 2024-07-02,   Input arguments are now returned to caller
%                                   Behaviour change: Function errors out if return value is not stored
%            tommsch, 2024-07-02,   (experimental) New allowed input format: func as function handle
% Changelog: 


    properties(Transient)
        task = @nop;
        args = {};
    end

    methods
        function [ obj, varargout ] = onCleanup_lazy( functionHandle, varargin );
            assert( nargout >= 1, 'onCleanup_lazy:argout', 'The return value of this function must be stored to a variable.' ); 

            nargin_h = nargin( functionHandle );
            if( nargin_h >= 0 && ...
                nargin_h ~= numel(varargin) && ...
                numel(varargin) == 1 && ...
                isa(varargin{1}, 'function_handle') ...
              );
                argin = cell( 1, nargin_h );
                [argin{:}] = varargin{1}();
                varargin = argin; end;

            obj.args = varargin;
            obj.task = functionHandle; 
            
            assert( nargin_h < 0 || nargin_h == numel(obj.args), 'onCleanup_lazy:input', 'Wrong number of input arguments for cleanup function passed.' );

            if( nargout >= 2 );
                nargs = min( nargout-1, numel(varargin) );
                varargout = cell( 1, nargs );
                [varargout{1:nargs}] = varargin{1:nargs}; end;
        end
        function delete( obj );
            obj.task( obj.args{:} );
            obj.task = @nop;
            obj.args = {};
        end
    end
    
    methods (Hidden = true)
        function [ obj ] = saveobj( obj );  %#ok<MANU>
            error( 'onCleanup_lazy:save', 'An onCleanup_lazy object cannot be saved.' );
        end
    end
end

function nop
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
