function [ ret ] = parfor_worker_available( delay );
% [ ret ] = parfor_worker_available( delay );
% (experimental) checks whether it is likely that there are available workers to run a parfor loop
% Input:
%   delay   time in seconds, Default = 0.05s, Time which is waited until a check is done. The larger this value, the more exact may the result. 
%
% Output
%   ret     true if there is most likely an available worker for a parfor loop
%
% Written by: tommsch, 2024-06-26

%               tommsch, 2024-07-30, Bugfix for Matlab versions prior 2021b
%               tommsch, 2024-07-30, Renamed to is_parfor_worker_available
%               tommsch, 2024-09-26, Renamed to parfor_worker_available
%                                    Moved from TTEST to ttoolboxes
% Changelog:


    persistent already_started_slow

    if( isoctave );
        ret = false;
        return; end;

    % use `backgroundPool`
    if( nargin <= 0 || isempty(delay) );
        delay = 0.05;  end;

    if( isempty(already_started_slow) );
        already_started_slow = true;
        delay = 1; end;

    if( ~verLessThan('matlab', '9.11') );  %#ok<VERLESSMATLAB>
        bp = backgroundPool();  % available since Matlab R2021b
        if( bp.Busy );
            ret = false;
            return; end; end;

    % use `gcp`
    p = gcp( 'nocreate' );
    if( ~isempty( p ) && ...
        isfield( p, 'Busy' ) && ...
        p.Busy ...
      );
        ret = false;
        return; end;

    F = parfeval( @() dummy_parfor_loop, 0 );
    F_cleanup = onCleanup( @() F.cancel() );
    F.fetchNext( delay );
    ret = isequal( F.State, 'finished' );
end

function dummy_parfor_loop();
    parfor i = 1:1000;
    end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
