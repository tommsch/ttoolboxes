function [ N ] = number_of_cores;
    if( ismatlab );
        N = feature( 'numcores' );
    else;
        N =  nproc(); end;
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
