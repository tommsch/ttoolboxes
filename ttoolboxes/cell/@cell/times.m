function C = times( A, B );
% [ C ] = times( A, B )
% Scalar * Cell / Cell * Scalar : times elementwise
% Matrix * Cell / Cell * Matrix : mtimes elementwise
% Cell * Cell                   : (m)times pairwise
%
% Written by: tommsch, 2018

%            2023-00-00, tommsch, Behaviour change: cell * cell makes a pairwise product
%            2024-05-28, tommsch, Behaviour change: cell * cell is not allowed anymore
%                                 Better error message if an unsupported multiplication with cells is done 
% Changelog: 
    if( iscell(A) && ~iscell(B) && isscalar(B) )
        C = cell( size(A) );
        for i = 1:numel(A)
            C{i} = A{i}.*B; end;

    elseif( iscell(A) && ~iscell(B) )
        C = cell( size(A) );
        for i = 1:numel(A)
            C{i} = A{i}*B; end;    

    elseif( ~iscell(A) && isscalar(A) && iscell(B) )    
        C = cell( size(B) );
        for i = 1:numel(B)
            C{i} = A.*B{i}; end;        

    elseif( ~iscell(A) && iscell(B) )
        C = cell( size(B) );
        for i = 1:numel(B)
            C{i} = A*B{i}; end;    

    elseif( iscell(A) && iscell(B) )
        error( 'cell:times', 'Support for the multiplication ''cell .* cell'' is removed.' );
%        C = cell( [size(A) size(B)] );
%        C = squeeze( C );
%        i = 0;
%        for a = 1:numel( A );
%            for b = 1:numel( B );
%                i = i + 1;
%                C{i} = A{a}.*B{b}; end; end;

    else;
        error( 'cell:times', 'This multiplication with cells is not implemented.' );
    end

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
