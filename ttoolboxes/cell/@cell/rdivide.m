function [ C ] = rdivide( A, B );
% times(A,B)
% Scalar ./ Cell : mrdivide elementwise
% Cell ./ Scalar : mrdivide elementwise
% Cell ./ Cell   : rdivie elementwise
% Cell ./ Matrix : mrdivide elementwise
%
% See also: cell/mtimes
%
% Written by: tommsch, 2018

    if( iscell(A) && ~iscell(B) && ismatrix(B) );
        C = cell( size(A) );
        for i = 1:numel( A );
            C{i} = A{i}/B; end;

%    elseif( iscell(A) && ~iscell(B) && ~isscalar(B) && numel(B ) == numel(A) );
%        C = cell( size(A) );
%        for i = 1:numel( A );
%            C{i} = A{i}/B(i); end;

    elseif( ~iscell(A) && iscell(B) );
        C = cell( size(B) );
        for i = 1:numel(B)
            C{i} = A/B{i}; end;    

    elseif( iscell(A) && iscell(B) );
        C = cell( size(A) );
        for i = 1:numel( A )
            C{i} = A{i}./B{i}; end;
    end

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
