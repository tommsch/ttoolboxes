function [ C ] = ldivide( A, B );
% [ C ] = ldivide( A, B )
% Scalar .\ Cell : mldivide elementwise
% Cell .\ Scalar : mldivide elementwise
% Cell .\ Cell   : ldivide elementwise
%
% See also: cell/mtimes
%
% Written by: tommsch, 2018

    if( iscell(A) && ~iscell(B) )
        C = cell( size(A) );
        for i = 1:numel(A)
            C{i} = A{i}\B; end;
        
    elseif( ~iscell(A) && iscell(B) )
        C = cell( size(B) );
        for i = 1:numel(B)
            C{i} = A\B{i}; end;    
        
    elseif( iscell(A) && iscell(B) )
        C = cell( size(A) );
        for i = 1:numel(A)
            C{i} = A{i}.\B{i}; end;
    
    end

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
