function C = imag(C)
% C = imag(C)
% Only keeps the imaginary part of a cell array
%
% E.g.: imag({sym(1+1i) {[1i] 1}})
%
% Written by tommsch, 2019

    for i = 1:numel( C );
        C{i} = imag( C{i} ); end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
