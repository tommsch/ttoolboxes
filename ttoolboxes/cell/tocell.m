function [ x, flag ] = tocell( x );
% [ x, flag ] = tocell( x );
% Wraps the content in a cell, if it is not a cell array
% Input:
%   x       something, the thing conditionally to be wrapped
% Output:
%   x       cell array
%   flag    true if x was a cell array in the first place
%
% E.g.: tocell( 2 );    % yields {2}
%       tocell( {2} );  % yields {2}
%
% Written by: tommsch, 2023-03-02

    flag = iscell( x );
    if( ~flag );
        x = {x}; end;
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
