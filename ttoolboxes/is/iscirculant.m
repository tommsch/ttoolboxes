function [ x ] = iscirculant( M, epsilon );
    if( nargin <= 1 || isempty(epsilon) );
        if( issym(M) );
            epsilon = 0;
        else;
            epsilon = max( size(M) ) * eps( norm(M) ); end; end;
    x = false;
    if( ~issquare(M) );
        return; end;

    if( isempty(M) );
        x = true;
        return; end;

    sze = size( M, 1 );
    v1 = M(:,1);
    for c = 2:sze;
        vc = circshift( M(:,c), -c+1 );
        diff = abs( v1 - vc );
        if( any( isAlways(diff > epsilon, 'Unknown','false') ) );
            return; end; end;
    x = true;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>