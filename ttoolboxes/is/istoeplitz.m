function [ x ] = istoeplitz( M, epsilon );
    if( nargin <= 1 || isempty(epsilon) );
        if( issym(M) );
            epsilon = 0;
        else;
            epsilon = max( size(M) ) * eps( norm(M) ); end; end;

    if( isvector(M) || isempty(M) );
        x = true;
        return; end;

    num_rows = size( M, 1 );
    shift = num_rows + 1;
    maxlen = min( size(M) );
    
    x = false;

    % start in first column
    for r = size( M, 1 ):-1:1;
        idx = r:shift:numel( M );
        len = min( num_rows - r + 1, maxlen );
        idx = idx(1:len);
        diff = abs( M(idx) - M(idx(1)) );
        if( any( isAlways(diff > epsilon, 'Unknown','false') ) );
            return; end; end;

    % start in first row
    for c = 2:size( M, 2 );
        idx = ((c-1)*num_rows+1):shift:numel( M );
        len = min( maxlen, numel(idx) );
        idx = idx(1:len);
        diff = abs( M(idx) - M(idx(1)) );
        if( any( isAlways(diff > epsilon, 'Unknown','false') ) );
            return; end; end;

    x = true;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
