function [ check ] = ispathcomplete( G );

    if( isempty(G) || ~any(G(:)) );
        check = false;
        return; end;
    try;
        c = conncomp( graph(G) );
    catch me;  %#ok<NASGU>
        c = conncomp( digraph(G) ); end;
    check = all( c == c(1) );
end

function [ check ] = ispathcomplete_old( G );  %#ok<DEFNU>
    
    G = G ~= 0;
    dim = size( G, 1 );
    
    Gi = G;
    for n = 1:dim;
        Gn = G * Gi ~= 0;
        if( all(Gn(:)) );
            check = true;
            return;
        elseif( isequal(Gn, G) );
            check = false;
            return; end;
        G = Gn;
    end
    warning( 'ispathcomplete:problem', 'Could not proof whether given graph is pathcomplete.\n  I assume it is.\n' );
    check = true;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
