function [ ret ] = isaltsign( M );

    s1 = sum( M, 1 );
    s2 = sum( M, 2 );
    c1 = cumsum( M, 1 );
    c2 = cumsum( M, 2 );

    ret = all( isAlways( s1 == 1, 'Unknown','false') ) && ...
           all( isAlways( s2 == 1, 'Unknown','false' ) ) && ...
           all( isAlways( min(c1(:)) == 0, 'Unknown','false' ) ) && ...
           all( isAlways( min(c2(:)) == 0, 'Unknown','false' ) ) && ...
           all( isAlways( max(c1(:)) == 1, 'Unknown','false' ) ) && ...
           all( isAlways( max(c2(:)) == 1, 'Unknown','false' ) ) ...
           ;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
