function [ ret, warningflag ] = isirreducible( varargin );
% checks whether a set of matrices is irreducible
% [ ret, warningflag ] = isirreducible( ... )
% Input:
%   ...     this function takes the same input as `invariantsubspace`
%
% Output:
%   ret             true, if set of matrices is irreducible
%   warningflag     true, if result seems inaccurate
%
% Written by: tommsch, 2023-04-01

    [ Mret, ~, warningflag ] = invariantsubspace( varargin{:} );
    ret = numel( Mret ) == 1;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
