function [ x ] = ishankel( M, tol );
    if( nargin <= 1 || isempty(tol) );
        if( issym(M) );
            tol = 0;
        else;
            tol = max( size(M) ) * eps( norm(M) ); end; end;
    if( isvector(M) || isempty(M) );
        x = true;
        return; end;
    num_rows = size( M, 1 );
    num_cols = size( M, 2 );
    shift = num_rows - 1;
    maxlen = min( size(M) );

    x = false;
    % start in last row
    for c = size( M, 2 ):-1:1;
        idx = c*num_rows:shift:numel( M );
        len = min( num_cols - c + 1, maxlen );
        idx = idx(1:len);
        diff = abs( M(idx) - M(idx(1)) );
        if( any( isAlways(diff > tol, 'Unknown','false') ) );
            return; end; end;

    % start in first column
    for c = size( M, 1 )-1:-1:1;
        idx = c:shift:numel( M );
        len = min( maxlen, c );
        idx = idx(1:len);
        diff = abs( M(idx) - M(idx(1)) );
        if( any( isAlways(diff > tol, 'Unknown','false') ) );
            return; end; end;

    x = true;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>