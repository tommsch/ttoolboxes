function [ ret, la ] = iseigenvector( varargin )
% checks whether a vector is a left/right eigenvector of a matrix
% [ ret, la ] = iseigenvector( A, v )
% [ ret, la ] = iseigenvector( w, A )
%
% ret is true, iff v/w is a right/left eigenvector of A
% If ret is true, then la is the corresponding eigenvalue.
% If ret is false, the value of la has no meaning
%
% Example: iseigenvector( [1 1;0 1], [1;0] )
%
% Written by: tommsch, 2022-02-14

    la = nan;
    
    if( issquare(varargin{1}) );
        A = varargin{1};
        v = varargin{2};
    else;
        v = varargin{1}';
        A = varargin{2}'; end;

    assert( issquare(A), 'iseigenvector:input', 'Input matrix must be square.' );
    assert( numel(v) == size(A, 1), 'iseigenvector:input', 'Dimensions of matrix and vector do not coincide.' );
    

    ret = false;
    idx = find( v, 1 );
    if( isempty(idx) );
        return; end;
    
    prod = A*v;
    la = prod(idx) / v(idx);
    if( ~isfinite(la) );
        return; end;
    
    res = A*v - la*v;
    nrm = norm( res );
    
    if( any(isvpa(A)) );
        epsilon = 10^(-digits()+5);
    elseif( issym(A) );
        epsilon = 0;
    else;
        epsilon = 1e-9; end;
    
    if( isAlways(nrm>epsilon) );
        return; end;
    
    ret = true;
        
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

