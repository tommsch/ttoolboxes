function [ flag ] = isvpa( x );
% [ flag ] = issym( x )
% Tests if an element in an array is vpa
% flag = logical(strcmp(class(x),'sym')); %#ok<STISA> 
% If elementwise is given and true, an array is returned
%
% E.g.: isvpa( [sym(23.2) sym('23.2') 0] )   % changed to fraction by matlab, thus this is not vpa
%
% See also: issymstrict
%
% Written by: tommsch, 2020

%            tommsch, 2022-01-28, Behaviour change. Function now returns an array instead of a scalar
%                                 This change is necessary, since Matlab allows vpa`s and sym`s in the same array
% Changelog: 
    
    if( ~issym(x) );
        flag = false( size(x) );
        return; end;
    e = -2*digits - 5;
    flag = isAlways( x+10^sym(e) == x );
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
