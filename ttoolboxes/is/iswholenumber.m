function x = iswholenumber( A )
% [ x ] = iswholenumber( A )
% Tests if an entry is a whole numbers.
%
% Depends on: //
%
% Written by: tommsch, 2018

%if( issym(A) );
    %x = hasSymType( A, 'integer' ); %for Release 2019b onwards
%    x = isAlways( floor(A) == A );
%elseif( ~isnumeric(A) ); 
%    x = false; 
%    return; 
%else;
    try;
        x = isAlways( ~isinf(A) & floor(A) == A, 'Unknown','false' ); %end;
    catch me;
        switch me.identifier
            case 'symbolic:sym:isAlways:TruthUnknown';
                rethrow( me );
            otherwise;
                x = false; end;

    end
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>