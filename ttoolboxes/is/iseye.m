function [ ret ] = iseye( X, tol )
% checks if a matrix is an identity matrix
% [ ret ] = iseye( X, [tol] )
% Input:
%   X       a matrix
%   tol     double, default = norm(X,1) * 1e-12, tolerance used in comparison
%
% Output:
%   ret     true if X is an identity
%
% Note: iseye( eye(2,3) ); %returns true
%
% Written by: tommsch, 2021-01-26

    ret = false;
    if( ~ismatrix(X) );
        return; end;
    if( nargin <= 1 );
        %tol = 0; end;  
        tol = norm( X, 1 ) * 1e-12; end;
    val = abs( X - eye(size(X)) );
    ret = all( isAlways(val(:)<tol) );

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
