function [ flag, x, y ] = iscauchy( M, epsilon );
% checks if a matrix is Cauchy, i.e. of the form a_ij = 1/(xi-yj)
% Input:
%   M       the matrix to check
%
% Output:
%   flag    true if M is Cauchy
%   x, y    vectors such that M = 1./(x-y)
%
% Example:
%   [flag, x, y] = iscauchy( gallery_matrix( 'cauchy',[1 1 1], [2  4 6] ) )  % yields false, since sequence x is not injective
%
% Note:
%   - Sometimes Cauchy matrices are defined as 1./(xi+yi)
%   - Often one additionally requires that the sequences xi and yi all have distinct values
%     This is not checked by this function. If one needs this additional restriction,
%     one has to check the returned sequences x and y
%   - The sequences x and y are not unique
%       
%
% Written by: tommsch, 2024-10-10

%               2024-10-10, tommsch,    Added this function to the is-toolbox
% Changelog:

    if( nargin <= 1 || isempty(epsilon) );
        if( issym(M) );
            epsilon = 0;
        else;
            epsilon = max( size(M) ) * eps( norm(M, 1) ); end; end;
    M = 1./M;
    x = M(:,1);
    M = M - x;
    y = -M(1,:);
    M = M + y;
    flag = all( isAlways(abs(M(:)) <= epsilon, 'Unknown','false') );
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
