function [ ret ] = isanyof( x, y );
% checks whether an element of y isequal to x
% [ ret ] = isanyof( x, _ );
% [ ret ] = isanyof( {x1, ... , xN}, _ );
% [ ret ] = isanyof( _, y );
% [ ret ] = isanyof( _, {y1, ... , yM} );
% 
% Input:
%   x       anything OR cell array of anything
%   y       anything OR cell array of anything
%
% Output:
%   ret                 bool, true if isequal(x{n}, y{m}) for some n,m
%
% Note:
%   This function never throws an error
%
% Example: isanyof( 42, {[],'',42} );
%
% Written by: tommsch, 2023-05-01

% Changelog:    tommsch,    2024-11-28,     x now can also be a cell array

    x = tocell( x );
    y = tocell( y );
    err = lasterror();  %#ok<LERR>
    for n = 1:numel( x );
        for m = 1:numel( y );
            try;
                if( isequal(x{n}, y{m}) );
                    ret = true;
                    return; end;
            catch me;  %#ok<NASGU>
                end; end; end;
    lasterror( err );  %#ok<LERR>
    ret = false;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
