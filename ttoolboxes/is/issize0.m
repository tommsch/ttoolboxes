function ret = issize0( x );
% checks whether all dimensions of x have length 0
% E.g: issize0( [] )           % true
%      issize0( zeros(1, 0) )  % false
    ret = ~any( size(x) );
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
