function [ ret ] = ishouseholder( M, epsilon );
% checks if a matrix is a householder matrix
% [ ret ] = ishouseholder( M, epsilon );
%
% Notes: This implementation is very slow
%
% Written by: tommsch, 2024-07-22

    if( nargin <= 1 || isempty(epsilon) );
        epsilon = norm( M, 2 ) * max(size(M)) * eps * 1e2; end;
    if( ~issquare(M) || isempty(M) );
        ret = false;
        return; end;
    Mt = M - eye( size(M) );
    ret = isAlways( norm( (Mt ./ Mt(1,:)) ./ Mt(:,1) * abs(Mt(1)) + 1, 1 ) < epsilon, 'Unknown','false' );
    if( ret );
        ev = sort( eig( M ) );
        res = ev - [-1; ones( size(M, 1)-1, 1 )];
        ret = norm( res ) < epsilon;
    end;
        
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>