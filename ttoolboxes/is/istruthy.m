function [ x ] = istruthy( cond, failsafe );
% [ x ] = istruthy( cond, [failsafe] );
% returns the implicitly converted value of cond to bool
% if `failsafe` is true, then `false` is returned for arguments which are not convertible to bool.
    if( nargin >= 2 && all(failsafe) );
        try;
            err = lasterror();  %#ok<LERR>
            if( cond );
                x = true;
            else;
                x = false; end;
        catch me;  %#ok<NASGU>
            lasterror( err );  %#ok<LERR>
            x = false; end;
    else;
        if( cond );
            x = true;
        else;
            x = false; end; end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
