function [ chain, MST ] = partitionatepolytope( VV, n, verbose );
% [ chain, MST ] = partitionatepolytope( VV, n, [verbose] );
% Partitions points in $\RR^s$ into clusters of nearby points.
% Also returns a list of consecutive edges. This list is partially ordered.
%
% Input:
%   VV          the vertices 
%   n           vertices are split into 2^n parts
%                   The size of each part should be the same, but this does not work
%                   Thus it is good to split it into 2 or 4 times the number of available threads.
%   verbose     verbose level. Default=0.
%
% Output:
%   chain       lists of consecutive edges
%   MST         the graph corresponding to the chain and VV
%
% E.g.: [chain, MST] = partitionatepolytope( normalizematrix(randn(3,1000),'colnorm',4), 2, 1 ); 
% 
% Written by: tommsch, 2018

% YY This function needs some more attention. If this function splits up the polytope better, many cores would be used more efficient.

% XX This function needs to be rewritten, since matlabs tables are incredibly slow and buggy
% XX Change meaning of param n from 2^n to n

    if( nargin == 2 || isempty(verbose) ); 
        verbose = 1; end;
    
    [MST, chain] = compute_MST( VV );
    if( ~isempty(chain) );
        return; end;
    
    MST{1}.Nodes.idx = uint32( (1:MST{1}.numnodes).' );  % save original indices, since matlab reassigns node-ids when making subgraphs
    MST{1}.Nodes.co = VV.';
    
    MST = partitionate( MST, n );
      
    chain = to_list_of_pairs( MST );
    
    if( verbose >= 2 );
        plot_partitioning( VV, MST ); end;

end

function [ MST, chain ] = compute_MST( VV );
    chain = [];
    N = size( VV, 2 );
    % compute minimal spanning tree
    if( N == 0 );
        MST{1} = minspantree( graph(zeros(0, 0)) );
        tbl = table( zeros(0, 0), zeros(0, 0), 'VariableNames',{'co','idx'} );  % Matlab R2020a?? Bug, matlab makes struct entry instead of table
        MST{1}.Nodes = tbl;
        chain{1} = [];
        return;
    elseif( N == 1 );
        MST{1} = minspantree( graph(0) );    
        MST{1}.Nodes.co = VV.';
        MST{1}.Nodes.idx = 1.';
        chain{1} = [0 1];
        return;
    elseif( N < 20 );
        chain{1} = [0:N-1; 1:N].';
        MST{1} = graph( chain{1}(2:end,1), chain{1}(2:end,2) );
        MST{1}.Edges.Weigth = ones( N-1, 1 );
        MST{1}.Nodes.co = VV.';
        MST{1}.Nodes.idx = [1:N].';  %#ok<NBRAK>
        return;
    else;
        if( isreal(VV) );
            val_V = VV.';
        else;
            val_V = [real( VV.' ) imag( VV.' )]; end;
        try;
            W = squareform( pdist( val_V, 'squaredeuclidean' ) );
        catch me;  %#ok<NASGU>
            W = sqdist( val_V.', val_V.' ); end;
        
        W = triu( W );
        MST{1} = minspantree( graph(W, 'upper'), 'Method','dense' );         
        clear W; end;
end

function [ MST ] = partitionate( MST, n );
    for i = 1:n;
        cuts = zeros( 0, 2 );
        for j = 1:length( MST );
            center = grCenter( MST{j}.Edges.EndNodes, 'edge' ); 
            if( isempty(center) ); 
                cuts(j,1:2) = [0 1]; 
            else; 
                cuts(j,1:2) = center; end; end;
        
        for j = 1:length( MST );
            if( cuts(j,1) ~= 0 ); 
                MST{j} = rmedge( MST{j}, cuts(j,1), cuts(j,2) ); end;
            bins = conncomp( MST{j} );
            for k = 1:max( bins );
                MST{end+1} = subgraph( MST{j}, find(bins == k) ); end;  %#ok<AGROW>
            
            %assert( maxm(bins) <= 2, 'partitionatepolytope:fatal', 'Fatal error' );
            MST{j} = []; end;
        
        MST = MST(~cellfun( 'isempty', MST )); end;
end

function [ chain ] = to_list_of_pairs( MST );
    chain = cell( size(MST) );
    for j = 1:length( MST )
        chain{j} = dfsearch( MST{j}, 1, 'edgetonew' );
        if( ~isempty(chain{j}) );
            chain{j} = [0 chain{j}(1,1); chain{j}];
        else
            chain{j} = [0 1]; end; end;
end

function plot_partitioning( VV, MST );
    figure(55);
    clf; 
    hold on;
    dim = size( VV, 1 );
    h = cell( size(MST) );
    OCC = zeros( size(MST) );
    for j = 1:length( MST )
        h{j} = plot( MST{j}, 'k', 'LineWidth',2, 'Layout','layered', 'NodeColor',rand(1, 3), 'MarkerSize',5 );
        %h{j} = plot( MST{j}, 'k', 'LineWidth',2, 'NodeLabel',[], 'Layout','layered', 'NodeColor',rand(1, 3), 'MarkerSize',5 );
        h{j}.XData = real( VV(1,MST{j}.Nodes.idx) );
        h{j}.YData = real( VV(2,MST{j}.Nodes.idx) );
        if( dim >= 3 ); 
            h{j}.ZData = real( VV(3,MST{j}.Nodes.idx) ); end;
        OCC(j) = MST{j}.numnodes; end;
    AXIS = axis;
    OCC = sort( OCC );
    plot( linspace(AXIS(1), AXIS(2), length(OCC)), OCC/max( OCC )*AXIS(4) );
    axis equal
    drawnow;
    vprintf( '\nSize of subtrees: %v\n', OCC ); 
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
