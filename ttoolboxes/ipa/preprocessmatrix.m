function [ M ] = preprocessmatrix( varargin )
% [ M ] = preprocessmatrix( M, [Msym], [options] )
% Simplifies sets of matrices while preserving its joint spectral radius.
% For default values: the output set has the same JSR as the input set.
%
% Input:
%   M           (cell-array) matrices to be preprocessed
%
% Options: 
%   'addinverse',bool        default: false, Adds the transposed matrices. 
%   'addtranspose',bool      default: false, Adds the transposed matrices. 
%   'basechange,val          default: 0,    Performs a base change.
%                               val = 0          No basechange.
%                               val = 'random'   a random base change is made.
%                               val = 1          The singular value decomposition of M{1} is used. Thus M{1} is in Jordan-normal-form. If M{1} is scaled baldy, M{2} is used, etc... .
%                               val = matrix     The matrix is used. B must be invertible. M{i} = B^(-1)*M{i}*B
%   'double',bool            default: false, Transforms matrices to double
%   'exponential',val        default: false, The matrix exponential of each matrix is taken.
%   'inverse',bool           default: false, Takes the Moore-penrose pseudo-inverse of all matrices. 
%   'makepositive',bool      default: true,  Makes first non-zero entry of array positiv.
%   'perturbate',val         default: 0,     Perturbates matrices randomly by rand(val). 
%   'rat',val                default: 0,     Rationalizes matrices up to an error of val
%   'remove',val             default: true, convenience option, if false, no matrices are removed
%   'removeduplicateeps,val  default: 1e-12 or 0 if M is symbolic or option 'sym' is given,   Removes duplicates with tolerance val.
%                               if M is not symbolic and removeduplicate is not given, then a warning is printed if matrices are removed
%                               val > 0,  relative tolerance between corresponding entries of two matrices
%                               val <= 0, absolute tolerance between corresponding entries of two matrices
%                               val = inf OR val = -inf, duplicates are not removed
%   'removezero',bool        default: true, Removes all (but one) zero matrices. 
%   'sym',bool               default: false, Transforms matrices to symbolic
%   'timestep',val           default: false, Changes matrices to I*(1-val) + M*val. 
%   'transpose',bool         default: false, Transposes all matrices. 
%   'verbose',val            default: 1, Verbose level.
%
% If no options are given, the matrices are processed in the order as written above. The second to last step is again 'makepositive'
% If at least one option (except 'verbose') is given, only that option is used.
%
% Output:
%   M           cell-array of preprocessed matrices
%               If no matrices are removed or added, the return array has the same size as the input array.
%               Otherwise it is a vector.
%
% E.g.: vdisp( preprocessmatrix({[-1 2; 2 3],[-1 2; 2 3]}) )
%
% Written by tommsch, 2018

%               2020-06-20,    tommsch,    Option to specify a tolerance in removal of duplicates. If removedupliacteeps = 1 or 0, comparison is made without tolerance.
%               2020-08-28,    tommsch,    Option 'nodouble' removed
% Changelog:

%#ok<*ALIGN>

% XX Add options of: ON THE FINITENESS PROPERTY FOR RATIONAL MATRICES, RAPHAEL JUNGERS AND VINCENT D. BLONDEL


   
    [ M, opt ] = parse_input( varargin{:} );
    
    if( opt.sym );                          M = preprocessmatrix_sym( M, opt ); end;
    if( opt.double );                       M = preprocessmatrix_double( M, opt ); end;
    if( opt.basechange );                   M = preprocessmatrix_basechange( M, opt ); end;
    if( opt.inverse );                      M = preprocessmatrix_inverse( M, opt ); end;
    if( opt.addinverse );                   M = preprocessmatrix_addinverse( M, opt ); end;
    if( opt.transpose );                    M = preprocessmatrix_transpose( M, opt ); end;
    if( opt.addtranspose );                 M = preprocessmatrix_addtranspose( M, opt ); end;
    if( opt.timestep ~= 1 );                M = preprocessmatrix_timestep( M, opt ); end;
    if( opt.perturbate ~= 0 );              M = preprocessmatrix_perturbate( M, opt ); end;
    if( opt.rat );                          M = preprocessmatrix_rat( M, opt ); end;
    if( opt.removezero );                   M = preprocessmatrix_removezero( M, opt ); end;
    if( isfinite(opt.removeduplicateeps) ); M = preprocessmatrix_removeduplicates( M, opt ); end;
    if( opt.exponential );                  M = preprocessmatrix_exponential( M, opt ); end;
    if( opt.makepositive );                 M = preprocessmatrix_makepositive( M, opt ); end;

    M = postprocess( M, opt );

end

%% helper functions

function [ M, opt ] = parse_input( varargin );
    M = varargin{1};
    varargin(1) = [];
    [M, opt.cellflag] = tocell( M );

    [opt.verbose, varargin] =               parsem_fast( {'verbose','v'}, varargin,     1 );
    
    defval = tif( numel(varargin) == 0, true, false );
    
    [opt.addinverse, varargin] =            parsem_fast( 'addinverse', varargin,        0 );
    [opt.addtranspose, varargin] =          parsem_fast( 'addtranspose', varargin,      0 );
    [opt.basechange, varargin] =            parsem_fast( 'basechange', varargin,        0 );
    [opt.double, varargin] =                parsem_fast( 'inverse', varargin,           0 );
    [opt.exponential, varargin] =           parsem_fast( 'exponential', varargin,       0 );
    [opt.inverse, varargin] =               parsem_fast( 'inverse', varargin,           0 );
    [opt.makepositive, varargin] =          parsem_fast( 'makepositive', varargin,      tif(defval, 1, 0) );
    [opt.perturbate, varargin] =            parsem_fast( 'perturbate', varargin,        0 );
    [opt.rat, varargin] =                   parsem_fast( {'rat','ratt'}, varargin,      0 );
    [removeflag, varargin] =                parsem_fast( 'remove', varargin,            [] );
    [opt.removeduplicateeps, varargin] =    parsem_fast( {'removeduplicate','removeduplicates','removeduplicateeps'}, varargin,tif(defval, [], -inf) );  % enabled by default with tolerance 1e-12 or 0
    [opt.removezero, varargin] =            parsem_fast( 'removezero', varargin,        tif(defval, 1, 0) );
    [opt.sym, varargin] =                   parsem_fast( 'sym', varargin,               0 );
    [opt.timestep, varargin] =              parsem_fast( 'timestep', varargin,          1 );  % 1 means no timestep    
    [opt.transpose, varargin] =             parsem_fast( 'transpose', varargin,         0 );
    

    
    if( ~isempty(removeflag) && removeflag );
        assert( ~isfinite(opt.removeduplicateeps) && ~opt.removezero, 'preprocessmatrix:option', 'If option ''remove'' is given, not other ''remove-...''-options can be given.' );
        opt.removeduplicateeps = -1;
        opt.removezero = true; end;
    
    parsem( varargin, 'test' );
end

function [ M ] = postprocess( M, opt );
    if( ~opt.cellflag );
        M = M{1}; end;
    vprintf( '\n', 'imp',[1 opt.verbose] );
end

%% worker functions

function [ M ] = preprocessmatrix_addinverse( M, opt );
    vprintf( 'Add inverse matrices. ' , 'imp',[1 opt.verbose] );
    J = numel( M );
    M{2*J} = [];  % preallocate space
    for j = 1:J;
        M{j+J} = pinv( M{j} ); end;
end

function [ M ] = preprocessmatrix_addtranspose( M, opt );
    J = numel( M );
    M = M(:);
    vprintf( 'Add transposed matrices. ', 'imp',[1 opt.verbose] );
    M{2*J} = [];  % preallocate space
    for j = 1:J; 
        M{j+J} = M{j}.'; end;
end

function [ M ] = preprocessmatrix_basechange( M, opt );
    vprintf( 'Make basechange. ', 'imp',[1 opt.verbose] );
    dim = size( M{1}, 1 );
    if( isequal(opt.basechange,'random') ); %generate base-change matrix
        while( true )
            opt.basechange = randn(dim);
            if( cond(opt.basechange) < 10*dim );
                break; end; end;
    elseif( isequal(opt.basechange, 1) );
        for j = 1:numel( M );
            [opt.basechange, D] = eig( M{j} );
            [opt.basechange, ~] = cdf2rdf( opt.basechange, D );
            if( cond(double(opt.basechange)) < 100*size(M{1}, 2) );
                break; end; end; end;
    assert( issquare(opt.basechange), 'preprocessmatrix:option', 'Wrong value for ''basechange''.' );
    for j = 1:numel( M );
        M{j} = opt.basechange\M{j}*opt.basechange; end;
end

function [ M ] = preprocessmatrix_double( M, opt );
    vprintf( 'Cast to double. ',  'imp',[1 opt.verbose] );
    for j = 1:numel( M );
        try;
            M{j} = double( M{j} ); 
        catch me;  %#ok<NASGU>
            warning( 'preprocessmatrix:double', 'Cast to double not possible.'); end; end;
end

function [ M ] = preprocessmatrix_exponential( M, opt );
    vprintf( 'Take matrix exponential. ', 'imp',[1 opt.verbose] );
    for j = 1:numel( M ); 
        M{j} = expm( M{j} ); end;
end

function [ M ] = preprocessmatrix_inverse( M, opt );
    vprintf( 'Invert matrices. ', 'imp',[1 opt.verbose] );
    for j = 1:numel( M );
        M{j} = pinv( M{j} ); end;
end

function [ M ] = preprocessmatrix_makepositive( M, opt );
    vprintf( 'Make matrices positive. ', 'imp',[1 opt.verbose] );
    for j = 1:numel( M ); 
        M{j} = makepositive( M{j}, [], 'rho' );  end;
end

function [ M ] = preprocessmatrix_perturbate( M, opt );
    vprintf( 'Perturbate matrices. ', 'imp',[1 opt.verbose] );
    for j = 1:numel( M ); 
        dim = size( M{j}, 2 );
        if( ~issym(M{j}) );
            val = (randi( 999, dim )-500)/500 * opt.perturbate;
        else;
            val = sym( (randi( 999, dim )-500) )/sym( 500 ) * sym( opt.perturbate ); end;
        M{j} = M{j} + val; end;
end

function [ M ] = preprocessmatrix_rat( M, opt );
    vprintf( 'Rationalize matrices. ', 'imp',[1 opt.verbose] );
    for j = 1:numel( M ); 
        M{j} = ratt( M{j}, opt.rat );
    end;

end

function [ M ] = preprocessmatrix_removeduplicates( M, opt );
    vprintf( 'Search for duplicates. ', 'imp',[1 opt.verbose] );
    J = numel( M );
    idx = false( 1, J );
    for j = 1:J-1;
        for k = j+1:J;
            if( isempty(opt.removeduplicateeps) );
                if( issym(M{j}) || issym(M{k}) );  % set default value for comparison
                    opt.removeduplicateeps = 0;
                else
                    opt.removeduplicateeps = 1e-12; end; end;
            if( opt.removeduplicateeps > 0 );
                val = M{j}./M{k};
                val( isAlways(M{j} == 0) & isAlways(M{k} == 0) ) = 0;
                val = abs( 1-val );
            else;
                val = maxm( abs(M{j} - M{k}) ); end;
            if( isAlways( maxm(abs(val)) <= abs(opt.removeduplicateeps), 'Unknown','false' ) ); 
                idx(j) = true;
                break; end; end; end;
    M(idx) = [];
end

function [ M ] = preprocessmatrix_removezero( M, opt );
    vprintf( 'Remove zero matrices. ', 'imp',[1 opt.verbose] );
    idx = true( 1, numel(M) );
    for j = 1:numel( M );
        if( ~any(M{j}) ); 
            idx(j) = false; end; end;
    if( all(~idx) );
        M = M(1);
    elseif( ~all(idx) ); 
        M = M(idx); end;
end

function [ M ] = preprocessmatrix_sym( M, opt );
    vprintf( 'Cast to sym. ',  'imp',[1 opt.verbose] );
    for j = 1:numel( M );;
        M{j} = sym( M{j} ); end;
end

function [ M ] = preprocessmatrix_timestep( M, opt );
    EYE = eye( size(M{1}, 1) );
    for j = 1:numel( M ); 
        M{j} = (opt.timestep - 1)*EYE + opt.timestep*M{j}; end;
end

function [ M ] = preprocessmatrix_transpose( M, opt );
    vprintf( 'Transpose matrices. ' , 'imp',[1 opt.verbose] );
    for j = 1:numel( M ); 
        M{j} = M{j}.'; end;
end




    

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

