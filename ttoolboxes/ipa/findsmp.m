function [ cand, nearlycand, info ] = findsmp( varargin );
% [ cand, nearlycand, info ] = findsmp( T, [G], [algorithm], ['max'|'min'], [options] )
% Searches for s.m.p.-candidates (s.min.p's and s.max.p's). (Search for s.min.p's is still experimental)
% Two algorithm types available: 'Gripenberg type algorithms' and the 'Genetic algorithm'
%
% Input:
% ======
%   T                       Cell array of square matrices of the same size.
%   G                       adjacency matrix of a graph, determining admissible products (Only possible for algorithms 'modified Gripenberg')
%   algorithm               string, default='modifiedgripenberg', name of algorithm to use
%                           Implemented algorithms: 
%                               'gripenberg',
%                               'lowgripenberg', 'highgripenberg', 'randomgripenberg'
%                               'modifiedgripenberg' (default), 
%                               'bruteforce', 'necklacebruteforce',
%                               'genetic' (not recommended)
%   
% Options for both algorithms:
% ============================
%   'sym',val                       Determines how to compute the values
%                                       -1: (default) same as input matrices
%                                        0: everything in double (if possible)
%                                        1: recomputing spectral radii after numerical search in vpa (if possible)
%                                        2: recomputing spectral radii after numerical search in sym (if possible)
%                                        3: everything in vpa (if possible)
%                                        4: everything in sym (if possible)
%   'verbose',val                   default = 1, Defines the verbose level.
%   'nosimplify'                    Does not simplify products cand and nearlycand
%   'maxtime',val                   default = inf, Maximal time used for computation
%   'maxeval'                       default = inf, Maximum number of evaluations (approximate)
%   'bound',val                     default = empty, Searches until 
%                                       a) the bounds of the joint/lower spectral radius is in ( val(1), val(2) ); 
%                                       b) it is proven that the bounds cannot be fulfilled anymore
%                                       Note: The interval ( val(1), val(2) ) is open. Since for s.min.p.s the computed lower bound is usually zero,
%                                             it will be necessary to set a negative lower bound in the search for s.min.p.s.
%                       
%   'nearlycandidate',bool          convenience option. If set to `false`, then no nearly candidates are searched for
%   'nearlycandidatedelta',val      default = 0.99, Nearlycandidates must have spectral radius larger than val*bounds(1), 
%   'shortnearlycandidate',val      default = 1, Removes all nearlycandidates whose ordering is longer than the val*maximal-length-of-candidates-ordering.
%                                       Note that 'genetic' algorithm does not search for nearly-candidates at the moment, thus this option has no effect for 'genetic' algorithm
%   'maxsmpdepth',val               maximal length of products which is searched for. Can be
%                                       arbitrary high ( > 100) for 'gripenbergmodifed' algorithm
%                                       high (< 15) for 'gripenberg'
%                                       small (< 12) for 'brute force'
%                                       Default value depends on algorithm used
%   'row'                           default=false, if given, indices are returned as row vectors
%   
% 
% Options for Gripenberg type algorithms':
% =============================================
%   One can use either pre-defined options corresponding to one specific algorithm, and/or set all/any options by hand    
%   
%   'norm',functionhandle           Handle to a norm function. Default: @norm (i.e. 2-norm)
%   'rho',functionhandle            Handle to a spectral radius function. Default: @rhot                     
%   'delta',val                     default = depends on algorithm, relative delta used in the Gripenberg Algorithm, delta <= 1, the smaller the delta, the faster (and less accurate) the algorithm
%   'N',val                         scalar or 1x3 vector of doubles, default=depends on algorithm, Number of kept products in each step
%                                       val(1)  ... number of products with smallest matrix norm kept
%                                       val(2)  ... number of products with largest matrix norm kept
%                                       val(3)  ... number of products randomly kept
%   'minsmpdepth',val               default = 1, Minimal length of products
%   'nearlycanddelta',val           default = 0.99, Maximal relative difference of spectral radius between candidates and nearly-candidates
%   'maxnum_nearlycandidate',val     default = 10, Maximum number of nearly candidates returned. If number of nearly candidates is larger, 'nearlycanddelta' is decreased
%   'nosimplify',val                default = false, does not simplify candidates nor nearly candidates
%   'shortcand',val                 default = false, only keep shortest candidates
%   'min' | 'max'                   default = 'max', Defines whether to search for s.min.p's or s.max.p's
%   'hardworking',val               default = 1, sets 'maxsmpdepth' to 'hardworking' times <last-smpcandidate-length> each time a new candidate is found
%   'epsilon',val                   default = 1e-10, epsilon used for comparing spectral radii
%   'graph',G                       if given, only products admissible w.r.t the graph are considered
%   'weigth',w                      vector of size(M), if given the matrices are weigthed with the corresponding value in w to compute the averaged spectral radius
%
%       Pre-defined algorithms:
%           'gripenberg'/'grip'             Standard Gripenberg algorithm
%                                           Gives lower and upper bounds
%                                           Does not miss candidates
%                                           NN = [inf; inf; inf]; delta = 0.95,
%
%           'lowgripenberg'/'lowgrip'       Modified Gripenberg algorithm keeping NN small products, delta = 1,
%
%           'highgripenberg'/'highgrip'     Modified Gripenberg algorithm keeping NN large products, delta = 1,
%
%           'modifiedgripenberg'/'modgrip'  Modified Gripenberg algorithm keeping NN/2 small and NN/2 large products, delta = 1,
%                                           Gives good (usualy tight) lower, but bad upper bounds
%           'randomgripenberg'/'randgrip'   Modified Gripenberg algorithm keeping NNrandom products, delta = 1,
%                                           Gives good lower, but bad upper bounds
%           'bruteforce'/'bf'               Brute force algorithm,
%                                           Very slow, does not miss candidates
%                                           Only algorithm which is proven to work for searching s.min.p's
%           'necklace'/'nl'               Brute force algorithm, only computing products in different short necklace classes
%                                           Does not compute upper bounds
%                                   
% 
%
% Options for genetic algorithm:            
% ================================            
%               Written by Chia-Tche Chang, 2011. 
%               There is a bug and the returned value is sometimes wrongly normalized.                                           
%               Does not work for 's.min.p'
%       'popsize',val                   (default 300, minimum 10) population size.
%       'maxgen',val                    (default 1000) maximum number of generations.
%       'maxstall',val                  (default 15) maximum number of stalling iterations before increasing the maximum product length.
%       'maxtotstall',val               (default 100) maximum number of stalling iterations before terminating the algorithm.
%       'mutantprop',val                (default 0.3) mutation probability of a given product.
%       'muteprop',val                  (default 0.2) mutation proportion of a given product.
% 
% Output: 
% =======
%   cand                    Ordering of the products with highest spectral radius.
%   nearlycand              Orderings of products with spectral radius greater than val*bounds(1). Can be empty, depending on the algorithm.
%   info                    (struct) Additional info, depends on the algorithm used.
%                               info.time         time in seconds needed for computation
%                               info.jsrbound     (Interval) estimate on the JSR/LSR based on the spectral radius of the found candidates and norm estimates
%                               info.spectralgap  relative difference between info.jsrbound and second biggest eigenvalue found (from nearly-candidates)
%                               info.count        approximate number of computed matrices
%
% Note:
% ============
%   The Gripenberg algorithms are parallelised, the genetic algorithm is not.
%   There is a bug in the genetic algorithm and the returned value is sometimes wrongly normalized.
%
% E.g.: [ c, nc, info ] = findsmp( {[1 -1; 3 -2], [1 3; -1 -1]}, 'maxsmpdepth',15 )
%
% See also: ipa
%
% Written by: tommsch, 2018

%               tommsch, 2019-11-15    Option 'sparse' removed
%                                      Option 'vpa' changed
%               tommsch, 2020-03-25    Totally rewritten
%               tommsch, 2021-10-12    Added option: 'row'
% Changelog:

% YY Fuer modgrip selbst herausfinden lassen, wann vpa verwendet werden soll
% XX Include computation of spectral gap
% XX symbolic stuff is not correctly handled yet, due to Matlab limitations

    [ MM, normfun, rhofun, info, opt, varargin ] = parse_input( varargin{:} );
    if( isequal(opt.dim,0) );
        cand = {[]};
        nearlycand = {[]};
        info.time = 0;
        info.jsrbound = [inf inf];
        info.spectralgap = 1; 
        return; end;

    vprintf( 'Search candidate-smp:\n', 'imp',[1 opt.verbose] );
    if( isequal(opt.method,'gen') );
        [oo, RR, nadm, ~, ~, lb, ub, info] = findsmp_genetic_preworker( MM, normfun, rhofun, info, opt, varargin{:} );
    else;
        [choosematrix, computeNtRt, makePtot, computelbub, opt, varargin] = parse_modgrip_input( MM, normfun, rhofun, opt, varargin{:} );  %#ok<ASGLU>
        [oo, RR, nadm, lb, ub, opt] = findsmp_modgrip_worker( MM, makePtot, computeNtRt, computelbub, choosematrix, opt ); end;
    
    if( opt.symflag > 0 );
        digitsclean = onCleanup_lazy( @digits, digits ); end;  %#ok<NASGU>
    [oo, RR, nadm, lb, ub, opt] = recompute( MM, normfun, rhofun, oo, RR, nadm, lb, ub, opt );

    [cand, nearlycand] = identify_candidate( oo, RR, nadm, lb, ub, opt );

    % [ spectralgap ] = compute_spectralgap( lb, ub, R, epsilon );
    [cand, nearlycand] = choose_shortest_candidate( cand, nearlycand, lb, ub, opt );

    % construct return variables
    [cand, nearlycand, info] = postprocess( cand, nearlycand, oo, lb, ub, info, opt );

end

%% subfunctions of main method

function [ MM, normfun, rhofun, info, opt, varargin ] = parse_input( varargin );

    info = struct;
    [~, varargin, opt.method] =          parsem( {'default', ...
                                                 'modgrip','gripenberg','grip', ...
                                                 'lowgripenberg','lowgrip','highgripenberg','highgrip','randomgripenberg','randgrip','modifiedgripenberg','modgrip', ...
                                                 'bruteforce','bf',...
                                                 'necklace','bruteforcenecklace','necklacebruteforce','nlbf','nl','bfnl', ...
                                                 'genetic','gen'}, varargin );
    [~, varargin, opt.minmax] =          parsem( {'max','min','smaxp','sminp','jsr','lsr','jssr','jss','sub','kone','cone'}, varargin );
    switch opt.minmax;
        case {'max','smaxp','jsr'}; opt.minmax = 'max';
        case {'min','sminp','lsr','jssr','jss','sub'}; opt.minmax = 'min';
        case {'kone','cone'}; opt.minmax = 'kone';
        otherwise; fatal_error; end;

    [opt.epsilon, varargin] =           parsem( {'epsilon','eps'}, varargin, [], 'expecte',{'clcl',[0 1e-2]} );
    [opt.maxeval, varargin] =           parsem( 'maxeval', varargin, inf, 'expect',{'clcl',[0,inf]} );
    [opt.maxnum_nearlycand, varargin] = parsem( {'maxnumnearlycandidate','maxnumnc'}, varargin, 10, 'expect',{'clcl',[0 inf]} );                       % maximal number of nearlycandidates. If there are more, then delta is set to (delta+1)/2
    [opt.maxsmpdepth, varargin] =       parsem( {'maxsmpdepth','maxd','maxdepth','depth'}, varargin, [], 'expecte',{'clcl',[0 inf]} ); % maximum length of products to be computed
    [opt.maxtime, varargin] =           parsem( 'maxtime', varargin, inf, 'expect',{'clcl',[0,inf]} );
    [opt.nearlycanddelta, varargin] =   parsem( {'nearlycandidate','nearlycanddelta','ncdelta','ncd','nc'}, varargin, .99 );  % delta for nearlycandidates
    if( isanyof(opt.nearlycanddelta, {'off','none',0}) );
        opt.nearlycanddelta = 1; end;
    [normfun, varargin] =               parsem( {'norm'}, varargin, [], 'expect',@(x) isa(x,'function_handle') || isempty(x) || isnumeric(x) );
    [opt.nosimplify, varargin] =        parsem( {'nosimplify'}, varargin, 0 );
    [opt.num_thread, varargin] =        parsem( {'numthread','threads','numcore','numcores','cores','thread'}, varargin, inf );
    [opt.period, varargin] =            parsem( {'period','periodics','periodizable','periodicable'}, varargin, [] );        % only meaningful if graph is given; whether to consider all possible products, or only admissible,periodizable products
    [opt.plot, varargin] =              parsem( {'plot'}, varargin, 0 );
    [rhofun, varargin] =                parsem( {'rho'}, varargin, [], 'expecte',@(x) isa(x,'function_handle') );
    [opt.row, varargin] =               parsem( {'row','rowvector'}, varargin, false );                                        % return indices as cell array of row vectors
    [opt.scale, varargin] =             parsem( {'scale','prescale'}, varargin, 'auto' );                                                 % whether to scale the input matrices
    [opt.shortcand, varargin] =         parsem( {'shortcand','shortnc'}, varargin, 0 );                                        % only keeps the shortest candidates
    [opt.shortnearlycand, varargin] =   parsem( {'shortnearlycandidate','shortnc'}, varargin, 1, 'expect',{'clcl',[0 inf]} );  % only keeps nearlycandidates whose length is less or equal than val times the length of the shortest candidate 
    [opt.toa, varargin] =               parsem( {'weight','timeofaction','time_of_action','toa'}, varargin, [] );
    [opt.symflag, varargin] =           parsem( 'sym', varargin, [], 'expecte',{-1 0 1 2 3 4} );
    [opt.verbose, varargin] =           parsem( {'verbose','v'}, varargin, 1, 'expect',@isnumeric );  % verbosity level
    
    if( opt.num_thread == 1 );
        opt.num_thread = 0; end;

    switch opt.method
        case {'default'}; opt.method = 'def';
        case {'genetic'}; opt.method = 'gen';
        case {'gripenberg','grip'}; opt.method = 'grip';
        case {'lowgripenberg','lowgrip','lgrip'}; opt.method = 'lowgrip';
        case {'highgripenberg','highgrip','hgrip'}; opt.method = 'highgrip';
        case {'modifiedgripenberg','modgrip'}; opt.method = 'modgrip';
        case {'randomgripenberg','randomgrip','randgrip','rgip'};  opt.method = 'randgrip';
        case {'bruteforce','bf'}; opt.method = 'bf'; 
        case {'necklace','necklacebruteforce','bruteforcenecklace','bfnl','nl','nlbf'}; opt.method = 'nl';
        otherwise; fatal_error; end;

    [MM, opt.graph, varargin] = parsematrixset( varargin{:} );

    if( ~isempty(opt.graph) && isempty(opt.period) );
        switch opt.method
            case {'def','grip','lowgrip','highgrip','modgrip','randgrip'}; opt.period = false;
            case {'bf','nl'}; opt.period = true;
            case {'gen'}; error( 'findsmp:graph', 'Algorithm ''genetic'' does not work with option ''graph'' together.' );
            otherwise; fatal_error; end; end;
    
    if( isequal(opt.method, 'def') );
        opt.method = 'modgrip';
        vprintf( 'Algorithm : %s\n', opt.method, 'imp',[1 opt.verbose] );end;

    if( ~isempty(normfun) && isnumeric(normfun) ); 
        normfun = @(x) norm( x, normfun );
    elseif( isempty(normfun) );
        switch opt.method;
            case {'grip','lowgrip','highgrip','modgrip','randgrip','bf'}; normfun = @norm;
            case {'nl'}; normfun = @(M) inf;
            case {'gen'}; normfun = @norm;
            otherwise; fatal_error; end; end;
    if( isempty(rhofun) );
        switch opt.minmax;
            case {'min','max'}; rhofun = @rhot;
            case {'kone'}; rhofun = @rho_kone;
            otherwise; fatal_error; end; end;


    opt.J = numel( MM );
    assert( opt.J >= 1, 'findsmp:input', 'Cell array ''M'' must consist of at least on matrix.' );
    opt.dim = size( MM{1}, 1 );
    if( isequal(opt.dim, 0) );
        return; end;
    if( isempty(opt.symflag) );
        switch opt.minmax;
            case {'min','max'}; opt.symflag = -1;
            case {'kone'}; opt.symflag = 1;
            otherwise; fatal_error; end; end;
    if( opt.symflag == 0 || opt.symflag == 1 || opt.symflag == 2 );
        for j = 1:opt.J;
            try;
                MM{j} = double( MM{j} );
            catch me;  %#ok<NASGU>
                warning( 'findsmp:sym', 'cast to double failed for matrix %i', j ); end; end;
    elseif( opt.symflag == 3 );
        for j = 1:opt.J;
            try;
                MM{j} = vpa( MM{j} );
            catch me;  %#ok<NASGU>
                warning( 'findsmp:sym', 'cast to vpa failed for matrix %i', j ); end; end;
    elseif( opt.symflag == 4 );
        for j = 1:opt.J;
             try;
                 MM{j} = sym( MM{j} );
             catch me;  %#ok<NASGU>
                 warning( 'findsmp:sym', 'cast to sym failed for matrix %i', j ); end; end; end;
    if( opt.symflag == -1 );
        if( issymstrict( MM{1} ) );
            opt.symflag = 4;
        elseif( issym( MM{1}) );
            opt.symflag = 3;
        else;
            opt.symflag = 0;
            end; end;

    if( isanyof( opt.scale, {0,'off','no'}) );
        opt.scale = 1;
    elseif( isanyof( opt.scale, {'auto','def','default'}) );
        if( opt.symflag == 4 || ...
            ~isempty( opt.toa ) || ...
            isanyof( opt.minmax, {'kone'} ) ...
          );
            opt.scale = 1;
        else;
            opt.scale = mean( rho(MM) );
            if( opt.scale == 0 );
                opt.scale = mean( cellfun( normfun, MM) ); end;
            if( opt.scale == 0 );
                opt.scale = 1; end; 
            if( opt.symflag == 1 || opt.symflag == 2 || opt.symflag == 3 );
                opt.scale = ratt( opt.scale ); end; end; 
    else;
        assert( isscalar(opt.scale) && isnumeric(opt.scale), 'findsmp:scale', 'The ''scale'' parameter must be a numeric scalar.' ); end;
    opt.M_unscaled = MM;

    if( ~isequal(opt.scale, 1) );
        for i = 1:numel( MM );
            MM{i} = MM{i}/opt.scale; end; end;
     
    if( isempty(opt.epsilon) );
        if( any( opt.symflag == [-1 0 1 2] ) || ...
            isequal( opt.symflag, -1 ) && isfloat( MM{1} ) ...
          );
            condnumber = cellfun( @condeigvec, MM );
            opt.epsilon = max( condnumber )*1e-15;
        elseif( opt.symflag == 3 || ...
                opt.symflag == -1 &&isvpa( MM{1} ) ...
              );
            condnumber = cellfun( @condeigvec, MM );
            opt.epsilon = max( condnumber )*1e-30;
        elseif( opt.symflag == 4 || ...
                opt.symflag == -1 && issym( MM{1} ) ...
              );
            opt.epsilon = 0; 
        else;
            error( 'findsmp:fatal', 'findsmp: Input type of matrices could not be determined. This is a programming error.' ); end; end;
    if( opt.epsilon > 1e-6 );
        opt.epsilon = 1e-6;
        if( opt.verbose >= 0 );
            warning( 'findsmp:numerical', 'findsmp: Input matrices are badly conditioned. The returned smp-candidate may be wrong.' ); end; end;
    if( opt.symflag == -1 );
        if( isvpa(MM{1}) );
            opt.symflag = 1;
        elseif( issym(MM{1}) );
            opt.symflag = 2; end; end;
end

function [oo, RR, nadm, lb, ub, opt] = recompute( MM, normfun, rhofun, oo, RR, nadm, lb, ub, opt );
    % recompute chosen candidates if symflag == 1 or symflag == 2
    if( opt.symflag == 1 || opt.symflag == 2 ); 
        assert( allm(opt.toa == 1), 'findsmp:toa', '`timeofaction` with symbolic computation is not implemented yet.' );
        if( opt.symflag == 1 );
            for j = 1:opt.J;
                try;
                    MM{j} = vpa( opt.M_unscaled{j} );
                catch me;  %#ok<NASGU>
                    warning( 'findsmp:cast', 'cast to vpa failed for matrix %i', j ); end; end;
        elseif( opt.symflag == 2 );
            for j = 1:opt.J;
                try;
                    MM{j} = sym( opt.M_unscaled{j} );
                catch me;  %#ok<NASGU>
                    warning( 'findsmp:cast', 'cast to sym failed for matrix %i', j ); end; end; end;    
        switch opt.minmax;
            case {'min'};
                idx_c = isAlways( RR <= ub(end)+opt.epsilon );
                while( true );
                    idx_nc = isAlways( RR < ub(end)/opt.nearlycanddelta ) & isAlways( cellfun( 'length', oo ) <= max( cellfun('length', oo(idx_c)) )*opt.shortnearlycand );
                    if( opt.symflag >= 2 || nnz(idx_nc) < 20 || opt.nearlycanddelta > 0.99999999 );
                        break; end;
                    opt.nearlycanddelta = (opt.nearlycanddelta+2)/3; end;
                if( opt.symflag == 1 && nnz(idx_nc) >= 20 );
                    idx_nc(:) = false; end;            
            case {'max','kone'}; 
                idx_c = isAlways( RR >= lb(end)-opt.epsilon );
                while( true );
                    idx_nc = isAlways( RR < ub(end)/opt.nearlycanddelta ) & isAlways( cellfun( 'length', oo ) <= max( cellfun('length', oo(idx_c)) )*opt.shortnearlycand );
                    if( opt.symflag >= 2 || nnz(idx_nc) < 20 || opt.nearlycanddelta > 0.99999999 );
                        break; end;
                    opt.nearlycanddelta = (opt.nearlycanddelta+2)/3; end;
                if( opt.symflag == 1 && nnz(idx_nc) >= 20 );
                    idx_nc(:) = false; end;
            otherwise;
                fatal_error; end;
        idx = idx_c | idx_nc;
        vprintf( 'Recomputing spectral radii (%i):\n', numel(oo), 'imp',[1 opt.verbose] );
        if( ~opt.nosimplify && ~isempty(nadm) );
            nadm = nadm(idx);
            [oo, idx] = simplify_ordering( oo(idx) ); 
            nadm = nadm(idx); end;
        RR = sym( zeros(size(oo)) );
        scale_print = ceil( numel( oo )/100 );
        vprintf( [repmat( '.', [1 floor(numel(oo)/scale_print)] ) '\n_'], 'imp',[1 opt.verbose] );

        digitsnew = 2* max( cellfun( 'prodofsize', oo ) );
        digits( digitsnew );

        verbose = opt.verbose;
        parfor( i = 1:numel( oo ), opt.num_thread ); digits( digitsnew );  % workers have their own `digits`
        %for i = 1:numel( oo );
            if( verbose >= 1 && ~mod(i, scale_print) );
                fprintf( '\b|\n' ); end;
            RR(i) = rhofun( buildproduct_fast(MM, oo{i}) )^(1/numel(oo{i}));  %#ok<PFBNS>
            RR(i) = simplify( RR(i) ); end;
        vprintf( 'Compute extrema. ', 'imp',[1 opt.verbose] );
        switch opt.minmax;
            case {'min'}; ub = min( RR );
            case {'max','kone'}; lb = max( RR );
            otherwise; fatal_error; end; end;

    if( opt.symflag == 1 );
        opt.epsilon = 10^-vpa( ceil( digits/1.5 ) );
    elseif( opt.symflag == 2 );
        opt.epsilon = sym( 0 ); end;
end

function [ cand, nearlycand, opt ] = identify_candidate( oo, RR, nadm, lb, ub, opt );
    % remove unnecessary orderings
    % we do not process the norms, since after simplifying the orderings, the norms are wrong in general

    
    if( ~isempty(nadm) );
        oo = oo(~nadm);
        RR = RR(~nadm); end;

    % find candidates
    if( ~isempty(opt.graph) );
        switch opt.minmax;
            case {'min'};
                expect( isAlways( ub == min(RR) ), 'findsmp:graph', 'Computed extremal bounds for spectral radii are due to a non admissible ordering. I will try to fix this.' );
                ub = max( RR );
            case {'max'};
                expect( isAlways( lb == max(RR) ), 'findsmp:graph', 'Computed extremal bounds for spectral radii are due to a non admissible ordering. I will try to fix this.' );
                lb = max( RR );
            otherwise;
                fatal_error; end; end;
    
    switch opt.minmax;
        case {'min'};        c_idx = RR <= (ub(end) + opt.epsilon);
        case {'max','kone'}; c_idx = RR >= (lb(end) - opt.epsilon);
        otherwise; fatal_error; end;
    c_idx = isAlways( c_idx, 'Unknown','false' );
    assert( ~isempty(c_idx) );
    
    switch opt.minmax;
        case {'min'};        nc_idx = RR < (ub(end)/opt.nearlycanddelta);
        case {'max','kone'}; nc_idx = RR > (lb(end)*opt.nearlycanddelta);
        otherwise; fatal_error; end;

    nc_idx = nc_idx & ...
             ~c_idx & ...
             cellfun( 'length', oo ) <= max( cellfun('length', oo(c_idx)) )*opt.shortnearlycand;

    nc_idx = isAlways( nc_idx, 'Unknown','false' );
    
    if( nnz(c_idx|nc_idx) > 3000 || opt.verbose >= 2 );
        vprintf( 'Simplify %i candidates / %i nearly-candidates ', nnz(c_idx), nnz(nc_idx), 'imp',[1 opt.verbose] ); end;
    oo = oo(c_idx|nc_idx);
    RR = RR(c_idx|nc_idx);

    % remove more unnecessary orderings
    if( ~opt.nosimplify );
        [oo, idx] = simplify_ordering( oo );
        RR = RR(idx); end;

    switch opt.minmax;
        case {'min'};
            errdiff = abs( ub(end) - min(RR) );
            expect( isAlways(errdiff < 1e-9), 'findsmp:numerical', '\nNumerical errors in computation of spectral radius are greater than %g.', errdiff );
            c_idx = RR <= min(RR)+opt.epsilon;
        case {'max','kone'};
            errdiff = abs( lb(end) - max(RR) );
            expect( isAlways(errdiff < 1e-9), 'findsmp:numerical', '\nNumerical errors in computation of spectral radius are greater than %g.', errdiff );
            c_idx = RR >= max(RR)-opt.epsilon;
        otherwise; fatal_error; end;
    c_idx = isAlways( c_idx, 'Unknown','false' );
    cand = oo(c_idx);
    while( true );
        switch opt.minmax;
            case {'min'}; 
                nc_idx = ( RR < min(RR)/opt.nearlycanddelta & ~c_idx & cellfun( 'length', oo ) <= max( cellfun('length', cand) )*opt.shortnearlycand );
            case {'max','kone'}; 
                nc_idx = ( RR > max(RR)*opt.nearlycanddelta & ~c_idx & cellfun( 'length', oo ) <= max( cellfun('length', cand) )*opt.shortnearlycand );
            otherwise; fatal_error; end;
        nc_idx = isAlways( nc_idx, 'Unknown','false' );
        nearlycand = oo(nc_idx);
        if( numel(nearlycand)<opt.maxnum_nearlycand || abs(opt.nearlycanddelta-1) < 4*eps );
            break;
        else;
            opt.nearlycanddelta = (opt.nearlycanddelta+.5)/1.5; 
            vprintf( '\nDecrease nearlycanddelta to %f. ', opt.nearlycanddelta, 'imp',[2 opt.verbose] ); end; end;

    [~,c_idx] = sort( cellfun('length',cand) );
    [~,nc_idx] = sort( cellfun('length',nearlycand) );
    cand = cand(c_idx);
    nearlycand = nearlycand(nc_idx);

    vprintf( '\n', 'imp',[1 opt.verbose] );
end

function [ spectralgap ] = compute_spectralgap( lb, ub, RR, opt );  %#ok<DEFNU>
    try;
        spectralgap = lb(end)/max( RR(RR < lb(end)/(1+opt.epsilon)) );
    catch me;  %#ok<NASGU>
        spectralgap = min( RR(RR > ub(end)*(1+opt.epsilon)) )/ub(end); end;
end

function [ cand, nearlycand ] = choose_shortest_candidate( cand, nearlycand, lb, ub, opt )
    if( ~opt.nosimplify && opt.shortcand );
        idx = shortest_cand( cand );
        cand = cand( idx ); end;

    % Text output
    switch opt.minmax;
        case {'min'}; minmax_string = 'lsr';
        case {'max'}; minmax_string = 'jsr';
        case {'kone'}; minmax_string = 'kone';
        otherwise; fatal_error; end;
    vprintf( 'Candidates: \n%v\n', cand, 'imp',[2 opt.verbose] );
    if( ~isempty(nearlycand) );
        vprintf( 'Nearly candidates: \n%v\n', nearlycand, 'imp',[2 opt.verbose] ); end;
    vprintf( '\n', 'imp',[2 opt.verbose] );
    % try;
    vprintf( 'Bounds on the %s : [%.15g, %.15g]\n', minmax_string, lb(end)*opt.scale, ub(end)*opt.scale, 'imp',[1 opt.verbose] ); 
    if( isfield(opt, 'delta') && any(opt.delta) );
        vprintf( 'Relative Gripenberg-Delta: %.15g\n', opt.delta(end), 'imp',[1 opt.verbose] ); end;
    % catch me;  %#ok<NASGU>
        % end;
    % vprintf( 'Relative spectral gap <= %.15g\n', spectralgap, 'imp',[1 verbose] );

end
   
function [ cand, nearlycand, info ] = postprocess( cand, nearlycand, oo, lb, ub, info, opt );
    lb = lb * opt.scale;
    ub = ub * opt.scale;

    % construct return variables
    info.oo = oo;
    info.jsrbound = [lb(end) ub(end)];
    %info.spectralgap = spectralgap;


    if( opt.row )
        info.oo = cellfun( @(x) tif(isrow(x),x,x.'), info.o, 'UniformOutput',false );
        cand = cellfun( @(x) tif(isrow(x),x,x.'), cand, 'UniformOutput',false );
        nearlycand = cellfun( @(x) tif(isrow(x),x,x.'), nearlycand, 'UniformOutput',false ); end;
end

%% genetic

function [ oo, RR, nadm, cand, nearlycand, lb, ub, info ] = findsmp_genetic_preworker( MM, normfun, rhofun, info, opt, varargin );  %#ok<INUSL>
    nadm = [];
    assert( isequal(opt.minmax, 'max'), 'findsmp:genetic_opt', 'Genetic algorithm only works for ''smaxp''.' );
    assert( isempty(opt.toa), 'findsmp:genetic_opt', 'Genetic algorithm does not support option ''toa''.' );
    [cand, nearlycand, info] = findsmp_genetic_worker( MM, info, opt, varargin{:} );
    oo = [cand nearlycand];
    RR = zeros( 1, numel(oo) );
    parfor( i = 1:numel( oo ), opt.num_thread );
        PP = buildproduct_fast( MM, oo{i} );
        RR(i) = rhofun( PP ); end;  %#ok<PFBNS>
    RR = RR.^(1/cellfun('length',oo));
    lb = info.jsrbound(1);
    ub = info.jsrbound(end);
end

function [ cand, nearlycand, info ] = findsmp_genetic_worker( M, info, opt, varargin );
    %[ cand, nearlycand, info ] = ipa_genetic(M, [options])
    %
    %Inputs:
    %   M      -  a non-empty cell array containing square matrices of identical dimensions.
    %
    % Options:
    %       'verbose',val    (default 1) defines the printing level:
    %                        <  1 - no output is printed on the screen;
    %                       >= 1 - prints the final lower bound and the associated product at the end of the algorithm;
    %                        >= 2 - prints the current lower bound and the associated product at each generation of the algorithm;
    %                        >= 3 - prints the current maximum product length and the number of stalling iterations at this current maximum product length at each generation.
    %      'popsize',val       (default 150, minimum 10) population size.
    %      'maxgen',val        (default 500) maximum number of generations.
    %      'maxstall',val      (default 70) maximum number of stalling iterations before increasing the maximum product length.
    %      'maxtotstall',val   (default 200) maximum number of stalling iterations before terminating the algorithm.
    %                        
    %      'mutantprop',val    (default 0.3) mutation probability of a given product.
    %      'muteprop',val      (default 0.2) mutation proportion of a given product.
    %       
    % Output:
    %   bound               a lower bound on the jsr of M.
    %   c                   a product of matrices corresponding to this lower bound:
    %                       the product A1*A2*A3 is expressed as [3 2 1] in this order.
    %   nc                  Empty
    %   info.elapsedtime    cpu time used to run the algorithm.
    %   info.population     the complete population at the end of the algorithm.
    %
    % Written by Chia-Tche Chang, 2011
    % Interface modified by tommsch

    assert( isempty(opt.graph), 'findsmp:graph', 'Option ''graph'' does not work with algorithm ''genetic''.' );

    [opt.searchonlyonecandidate, varargin] =    parsem( {'minJSR','searchonlyonecandidate','one','bound'}, varargin, 0, 'expect',@(x) numel(x) == 1 );  % stops algorithm after one candidate has been found. To be used together with minJSR
    [opt.popsize, varargin] =                   parsem( 'popsize', varargin, 500, 'expect',{'clop',[1,inf]} );
    [opt.maxgen, varargin] =                    parsem( {'maxgen'}, varargin, 200, 'expect',{'clop',[1,inf]} );
    [opt.maxstalling, varargin] =               parsem( {'maxstall','maxstalling'}, varargin, 70, 'expect',{'clop',[1,inf]} );
    [opt.maxtotalstalling, varargin] =          parsem( {'maxtotstall','maxtotalstalling'}, varargin, 1000, 'expect',{'clop',[1,inf]} );
    [opt.mutantprop, varargin] =                parsem( 'mutantprop', varargin, 0.3, 'expect',{'clcl',[0,1]} );
    [opt.muteprop, varargin] =                  parsem( 'muteprop', varargin, 0.2, 'expect',{'clcl',[0,1]} );
    starttime_tommsch = tic;  % starttime of tommsch

    parsem( varargin, 'test' );

    vprintf( 'The genetic algorithm has a bug, and sometimes reports spectral radii which are normalized wrongly!\nUse this algorithm with care.\n', 'cpr','err', 'imp',[0 opt.verbose] );

    % Initialization
    starttime_chiang = cputime;  % starttime of Chiang
    m = numel( M );
    n = size( M{1}, 1 );
    k = ceil( log(opt.popsize)/log(m+1) );
    scaler = ((m+1).^(k-1:-1:0))';

    % Cache
    cache = findsmp_genetic_genCache( M, k, m, n );
    ncache = size(cache, 2);
    stalling = 0;
    totalstalling = 0;
    bound = opt.searchonlyonecandidate^k;
    bestpop = [];
    for i = 2:ncache;
        value = max( abs(eig(mat(cache(:, i)))) );
        if( value > bound );
            bound = value;
            bestpop = zeros( 1, k );
            key = i-1;
            for j = 1:k;
                bestpop(j) = floor( key/scaler(j) );
                key = key - bestpop(j)*scaler(j); end; end; end;
    bestpop = bestpop(bestpop ~= 0 );
    bound = bound^(1/k);    % YY Here is a bug. 
                            % T = overlapMat needs this line. 
                            % For a = 1/12*[3 3 4 3 3 4 3 3 4 3 3]'; M = -3; D = [-2 -1 0]; S = getS( 'a',a, 'M',M, 'D',D ); [T,Om] = transitionmatrix( S ); U = constructU( T, 1, 'sym' ); T = restrictmatrix( T, U );
                            % the line is wrong.
    X = eye( n );
    testbestpop = bestpop;
    for i = 1:length( testbestpop )-1;
        X = X * M{testbestpop(i)};
        testval = max(abs(eig(X)))^(1/i);
        if( testval >= bound );
            bestpop = testbestpop(1:i);
            bound = testval; end; end;
    if( opt.verbose >= 2 );
        fprintf(' \n');
        fprintf('Starting population: init lower bound on the JSR = %.15g with product: %s\n', tif( isempty(bestpop),0,bound), num2str(bestpop) ); end;

    % Initial population
    curlength = 2*k;
    population = floor( (m+1)*rand(opt.popsize, curlength) );

    % Genetic evolution
    gen = 0;
    counter = 0;
    while(true);
        counter=counter+gen*opt.maxstalling;
        if( counter>opt.maxeval); 
            break; end;
        gen = gen+1; 
        if( ~opt.searchonlyonecandidate && gen>=opt.maxgen ); 
            break; end;
        % Evaluation
        if( opt.searchonlyonecandidate && ~isempty(bestpop) ); 
            break; end;
        if( toc(starttime_tommsch) >= opt.maxtime ); 
            break; end;
        [nbeyes, idx] = sort( sum(population == 0, 2), 'descend' );
        population = population(idx, :);
        population(nbeyes == curlength, 1) = ceil( m*rand(1) );
        nbeyes = min( nbeyes, curlength-1 );
        fitness = zeros( opt.popsize, 1 );
        cached = floor( curlength/k );
        cachekey = zeros( opt.popsize, cached );
        for j = 1:cached;
            cachekey(:, j) = population(:, k*j-k+1:k*j)*scaler; end;
        cachekey = cachekey + 1;
        for i = 1:opt.popsize;
            X = eye(n);
            for j = 1:cached;
                X = X * mat(cache(:, cachekey(i, j)) ); end;
            for j = k*cached+1:curlength;
                if( population(i, j) ~= 0 );
                    X = X * M{population(i, j)}; end; end;
            fitness(i) = max( abs(eig(X)) )^(1/(curlength-nbeyes(i))); end;
        [fitness, idx] = sort( fitness, 'descend' );
        population = population(idx, :);

        % Local optimization
        if( fitness(1) > bound );
            stalling = 0;
            totalstalling = 0;
            bound = fitness(1);
            bestpop = population(1, :);
            bestpop = bestpop(bestpop ~= 0 );
            testbestpop = bestpop;
            lenbestpop = length( testbestpop );
            CELLX = cell( lenbestpop + 1, 1 );
            X = eye(n);
            CELLX{1} = X;
            for i = 1:lenbestpop;
                X = X * M{testbestpop(i)};
                CELLX{i+1} = X;
                testval = max( abs(eig(X)) )^(1/i);
                if( testval > bound || (testval == bound && i < length(bestpop)) );
                    bestpop = testbestpop(1:i);
                    bound = testval; end; end;
            lenbestpop = length( testbestpop );
            X = eye( n );
            localimprove = 0;
            for i = lenbestpop:-1:1;
                testval = max( abs(eig(CELLX{i}*X)) )^(1/(lenbestpop-1));
                if( testval > bound );
                    bestpop = testbestpop([1:i-1 i+1:end] );
                    bound = testval;
                    localimprove = localimprove + 1; end;
                for j = 1:m;
                    testval = max( abs(eig(CELLX{i}*M{j}*X)) )^(1/lenbestpop);
                    if( testval > bound );
                        bestpop = testbestpop;
                        bestpop(i) = j;
                        bound = testval;
                        localimprove = localimprove + 1; end; end;
                if( lenbestpop < curlength );
                    for j = 1:m;
                        testval = max( abs(eig(CELLX{i+1}*M{j}*X)) )^(1/(lenbestpop+1));
                        if( testval > bound );
                            bestpop = [testbestpop(1:i)  j  testbestpop(i+1:end)];
                            bound = testval;
                            localimprove = localimprove + 1; end; end; end;
                X = M{testbestpop(i)}*X; end;
            if( localimprove );
                population = [bestpop, zeros(1, curlength-length(bestpop) ); population(1:end-1, :)]; end;
        else
            stalling = stalling + 1;
            totalstalling = totalstalling + 1; end;

        % Stopping criterion
        if( opt.verbose >= 3 );
            fprintf( 'Generation #%3d: STA = %2d, LEN = %2d, lower bound = %.15g with product: %s\n', gen, stalling, curlength, tif( isempty(bestpop),0,bound), num2str(bestpop) );
        elseif( opt.verbose >= 2 );
            fprintf( 'Generation #%3d:  current lower bound on the JSR = %.15g with product: %s\n', gen, tif( isempty(bestpop),0,bound), num2str(bestpop) ); end;
        if( ~opt.searchonlyonecandidate && totalstalling >= opt.maxtotalstalling );
            break; end;
        if( stalling >= opt.maxstalling );
            stalling = 0;
            curlength = curlength + 1;
            if( curlength>opt.maxsmpdepth )
                break; end;
            population = [population, zeros(opt.popsize, 1)]; end;  %#ok<AGROW>


        % Selection and crossover
        nb_elite = max(2, min(3, floor(opt.popsize/50)) );
        nb_spawn = max(4, floor(opt.popsize/50) );
        nb_swap = min(opt.popsize - nb_elite - nb_spawn, floor(opt.popsize/2) );
        Nb_mix = opt.popsize - nb_elite - nb_spawn - nb_swap;
        id_swap = ceil((opt.popsize/2)*rand(nb_swap, 2) );
        id_mix = ceil(opt.popsize*rand(Nb_mix, 2) );

        pop_elite = population(1:nb_elite,:);
        pop_spawn = ceil(m*rand(nb_spawn, curlength) );
        pop_swap = zeros(nb_swap, curlength);
        for i = 1:nb_swap;
            cut = ceil(curlength*rand(1) );
            pop_swap(i, :) = [population(id_swap(i, 1), 1:cut) population(id_swap(i, 2), cut+1:end)]; end;
        pop_mix = zeros(Nb_mix, curlength);
        for i = 1:Nb_mix;
            mixer = floor(2*rand(curlength, 1))';
            pop_mix(i, :) = population(id_mix(i, 1), :) .* mixer + population(id_mix(i, 2), :) .* (1-mixer); end;

        population = [pop_elite ; pop_spawn ; pop_swap ; pop_mix];

        % Mutation
        mutestr = ceil(curlength * opt.muteprop);
        for i = 2:opt.popsize;
            if (rand(1) < opt.mutantprop);
                population(i, ceil(curlength * rand(1, mutestr))) = floor((m+1)*rand(1, mutestr) ); end; end;
    end

    % Termination
    elapsedtime = cputime - starttime_chiang;
    %bestpop = ipa_genetic_deperiod(bestpop);  % we use the function simplify_ordering instead

    % POST PROCESSING FOR tommsch-Interface GENERATION  %
    if( isempty(bestpop) ); 
        bound= 0; end;
    cand = {flip(bestpop).'};  % change order and make it to column vector, since tommsch-programs need that
    nearlycand = {};
    info.time = elapsedtime;
    info.population = population;
    info.jsrbound = [bound inf];
    info.count = counter;

    if( opt.verbose >= 1 );
        fprintf('Algorithm terminated with lower bound on the JSR = %.15g with product: %s\n', bound, num2str(bestpop) ); end;

end

function [ cache ] = findsmp_genetic_genCache( MM, k, m, n );  %  CACHE GENERATION  %
    if( k <= 1 );
        cache = zeros( n*n, m+1 );
        cache(:, 1) = vec( eye(n) );
        for i = 1:m;
            cache(:, i+1) = vec( MM{i} ); end;
        return; end;
    cache = repmat( findsmp_genetic_genCache(MM, k-1, m, n), 1, m+1 );
    gsize = (m+1)^(k-1);
    for i = 1:m;
        cache(:, gsize*i+1:gsize*(i+1)) = kron( eye(n), MM{i}) * cache(:, gsize*i+1:gsize*(i+1) ); end;
end

function [ v ] = vec( MM );
% Converts matrices to vectors
% Input:    M      matrix
% Output:   v      vector
    v = reshape( MM, numel(MM), 1 );
end

function M = mat( vv, n );
% Input:    v       vector of length 2^n
%          [n]     (optional) the value of n.
%                  If n is not given, it is computed
% Output:   M       the converted matrix
    if( nargin < 2 );
        n = floor( sqrt(length(vv)) ); end;
    M = reshape( vv, n, n );
end

%% gripenbergs

function [ choosematrix, computeNtRt, makePtot, computelbub, opt, varargin ] = parse_modgrip_input( MM, normfun, rhofun, opt, varargin );
    % parse input
    [NN, varargin] =                parsem( {'N','maxnumprod'}, varargin, [] );  % the number of kept products in each step equals approximately NN
    [opt.delta, varargin] =         parsem( 'delta', varargin, [] );             % delta from Gripenberg Algorithm
    [opt.minsmpdepth, varargin] =   parsem( 'minsmpdepth', varargin, 1, 'expect',{'clop',[1 inf]} );
    [opt.bound, varargin] =         parsem( {'bound','searchonlyonecandidate','one','minJSR','sufficientbound','b'}, varargin, [], 'expect',@(x) numel(x) == 2 || numel(x) == 0 );  % stops algorithm after one candidate with spectral radius val has been found.
    [opt.hardworking, varargin] =   parsem( 'hardworking', varargin, 1, 'expect',{'opop',[0 inf]} );
    assert( isempty( opt.toa ) || ...
            isvector( opt.toa ) && numel(opt.toa) == numel(MM), 'findsmp:toa', '`timeofaction` must be empty or a vector with number of matrices elements.' );
    
    parsem( varargin, 'test' );
    
    if( isempty(NN) );
        NN = ceil( sqrt(opt.dim*opt.J) * 10 );
    elseif( NN < 0 );
        NN = -NN*ceil( sqrt(opt.dim*opt.J) * 10 ); end;
    if( isempty(opt.maxsmpdepth) );
        switch opt.method;
            case {'grip'};      opt.maxsmpdepth =  30;
            case {'lowgrip'};   opt.maxsmpdepth = 100;
            case {'highgrip'};  opt.maxsmpdepth = 100;
            case {'modgrip'};   opt.maxsmpdepth = 100;
            case {'randgrip'};  opt.maxsmpdepth = 100;
            case {'bf'};        opt.maxsmpdepth =  10; 
            case {'nl'};        opt.maxsmpdepth =  15;
            otherwise; fatal_error; end; end;
    if( numel(NN) == 1 );
        switch opt.method;
            case {'grip'};      NN = [inf;        inf;        0];
            case {'lowgrip'};   NN = [NN;         0;          0];
            case {'highgrip'};  NN = [0;          NN;         0];
            case {'modgrip'};   NN = [ceil(NN/2); ceil(NN/2); 0];
            case {'randgrip'};  NN = [0;          0;          NN];
            case {'bf','nl'};   NN = [inf;        inf;        inf];
            otherwise; fatal_error; end; end;
    
    if( ~isempty(opt.graph) && isempty(opt.delta) );
        opt.delta = inf;
    elseif( isempty(opt.delta) );
        opt.delta = 1; end;
    switch opt.minmax;
        case {'max','kone'};  % do nothing
        case {'min'}; opt.delta = 1./(1-opt.delta);
        otherwise; fatal_error; end;
    switch opt.method;
        case {'grip','lowgrip','highgrip','modgrip','randgrip'}; 
            opt.delta_now = inf;
            opt.test_idx = true;
        case {'bf','nl'}; 
            opt.delta = inf; 
            opt.delta_now = inf; 
            opt.test_idx = false;
        otherwise; fatal_error; end;
    choosematrix = @(Nt, Rt, lb, ub, delta_now) choosematrix_lhr( Nt, Rt, lb, ub, delta_now, opt.delta, NN, opt.epsilon );  % lhr = low-high-random
        

    % computeNtRt
    computeNtRt = @(Pt, ot, toa) computeNtRt_base( Pt, ot, normfun, rhofun, opt.toa );

    % makePtot/computelbub
    switch opt.method;
        case {'grip','lowgrip','highgrip','modgrip','randgrip'}; makePtot = @makePtot_idx;  % make Ptilde/otilde
        case {'bf'};  makePtot = @makePtot_bf;
        case {'nl'};  makePtot = @makePtot_nl;
        otherwise; fatal_error; end;
    switch opt.minmax;
        case {'min'}; computelbub = @computelbub_sminp;
        case {'max','kone'}; computelbub = @computelbub_smaxp;
        otherwise; fatal_error; end;
end

function [ oo, RR, nadm, lb, ub, opt ] = findsmp_modgrip_worker( MM, makePtot, computeNtRt, computelbub, choosematrix, opt  );
    
    % set start values
    NN = [];
    RR = [];
    smpdepth = max( opt.minsmpdepth, 0 );
    living = [];  % indices of products which make it into the next round
    oo = {};  % indicator for makePtot functions that we are in the first round
    PP = {};
    nadm = false( 1, 0 );
    lb = 0;
    nlb = 0;
    ub = inf;
    nub = inf;
    Nt_mm = zeros(1,0);  % XX What is this variable?
    Rt_mm = zeros(1,0);
    opt.delta_now = min( [opt.delta_now opt.delta] );

    % start algorithm
    opt.starttime = tic;
    opt.num_eval = 0;

    while( true );
        
        % check termination conditions
        if( check_termination( lb, ub, living, smpdepth, opt ) );
            break; end;
        
        % make new matrices and orderings
        [ot, Pt, nadmt, smpdepth] = makePtot( MM, opt.graph, PP, oo, living, smpdepth, opt.period );
        opt.num_eval = opt.num_eval + numel( Pt );

        % compute norm and rho
        vprintf( 'Number of matrices with length %3i to compute: %6i,  \t', smpdepth, numel(Pt), 'imp',[2 opt.verbose] );
        vprintf( 'Orderings:\n %v \n', ot, 'imp',[3 opt.verbose] );
        vprintf( 'Matrices:\n %v \n', Pt, 'imp',[4 opt.verbose] );
        
        val = [Pt{:}];
        if( all(isfinite(val(:))) );
            [Nt, Rt] = computeNtRt( Pt, ot );
        else;
            warning( 'findsmp:nonfinite', 'Non-finite values occured during computation. Probably input matrices are scaled badly.' );
            break; end;
        Nt_mm = [Nt_mm [min(Nt); max(Nt)]];  %#ok<AGROW>
        Rt_mm = [Rt_mm [min(Rt); max(Rt)]];  %#ok<AGROW>

        % compute bounds
        [lbt, ubt, nlbt, nubt] = computelbub( lb(end), ub(end), nlb(end), nub(end), Nt, Rt, nadmt, opt.delta_now(:,end), opt.graph, opt.toa );
        
        opt.maxsmpdepth = print_output( ot, Rt, lb,lbt, ub,ubt, nlb,nlbt, nub,nubt, smpdepth, opt );

        % choose matrices
        [living, delta_nowt] = choosematrix( Nt, Rt, lbt, ubt, opt.delta_now(:,end) );        
        living = [false( 1, numel(RR) ) living];  %#ok<AGROW>

        [PP, oo, NN, RR, nadm, lb, ub, nlb, nub, living] = add_to_results( PP,Pt, oo,ot, NN,Nt, RR,Rt, nadm,nadmt, lb,lbt, ub,ubt, nlb,nlbt, nub,nubt, living, opt );

        smpdepth = smpdepth + 1;
        
        opt.delta_now = [opt.delta_now delta_nowt];
        vprintf( '\n', 'imp',[2 opt.verbose] ); 

        findsmp_plotoutput( NN, RR, Nt_mm, Rt_mm, nadm, opt );
        end;

end

function [ ot, Pt, nadm, depth ] = makePtot_idx( MM, GG, PP, oo, idx, depth, ~ );
% Input:
%   MM      cell array of square matrices, input matrices
%   PP      cell array of square matrices, product of input matrices with ordering o
%   oo      cell array of column vectors matrices, same numel as PP, ordering of products
%   idx     logical vector, nnz(idx) == numel(PP), which ordering shall get children
% Output:
%   Pt      cell array of square matrices, numel(Pt) = nnz(idx)*numel(M), product of new matrices
%   ot      cell array of column vectors, numel(ot) == nnz(idx)*numel(M), ordering of product of new matrices

    idx = isAlways( idx );

    if( isequal(oo, {}) );
        ot = num2cell( 1:numel( MM ), 1 );
        Pt = MM(:).';  % make to row vector
        depth = 1;
    
    elseif( ~isempty(GG) );
%         Pt = cell( 1, J * nnz(idx) );
%         ot = cell( 1, J * nnz(idx) );
        Pt = {};
        ot = {};
        ct = 1;
        i = 0;
        for p = find( idx );
            % make orderings
            if( ~isequal(oo,{[0]}) );  %#ok<NBRAK>
                new = find( GG( oo{p}(end),: ) );
            else;
                new = 1:numel( MM ); end;  % hack, for start case
            n_new = numel( new );
            ot(ct:ct+n_new-1) = num2cell( [repmat( oo{p}, 1, numel(new) ); new ], 1 );
            ct = ct + n_new; 
            % make matrices
            
            Pt{end+n_new} = [];  %#ok<AGROW>
            for n = new;
                i = i + 1;
                Pt{i} = MM{n} * PP{p}; end; end;

        if( isequal(oo, {[0]}) );  %#ok<NBRAK>  % hack, to remove zero line
            for i = 1:numel( ot );
                ot{i}(1) = []; end; end;  %#ok<AGROW>

        ot(ct:end) = [];
        Pt(ct:end) = [];

    else;
        J = numel( MM );
        n = nnz( idx );
        Pt = cell( 1, J * n );
        i = 0;
        % make orderings
        ot = [repmat( [oo{idx}], [1 J] ); 
              reshape( repmat(1:J, [n 1]), 1, [] )];
        
        % make matrices
        for j = 1:J;
            for p = find( idx );
                i = i + 1;
                Pt{i} = MM{j} * PP{p}; end; end;

        % postprocess ot
        ot = num2cell( ot, 1 ); end;

    if( ~isempty(GG) );
        nadm = is_notadmissible( ot, GG ); 
    else;
        nadm = []; end;

end

function [ ot, Pt, nadm, depth ] = makePtot_bf(  MM, GG, ~ , ~ , ~  , depth, period );
% Input:
%   M       cell array of square matrices, input matrices
%   o       cell array of column vectors matrices, same numel as PP, ordering of products
% Output:
%   Pt      cell array of square matrices, numel(Pt) = nnz(idx)*numel(M), product of new matrices
%   ot      cell array of column vectors, numel(ot) == nnz(idx)*numel(M), ordering of product of new matrices
% Note:
%   o is actually not used to compute the matrices Pt
%   only the length of o used as information
    if( isempty(GG) );
        ot = mixvector( 1:numel(MM), depth );
        ot = num2cell( ot, 1 );
        nadm = [];
    else;
        J = numel( MM );
        while( true );
            ot = num2cell( 1:J, 1 );
            for d = 2:depth;
                n_old = numel( ot );
                for i = n_old:-1:1;
                    new = find( GG(ot{i}(end),:) );
                    new_oo_i = num2cell( [repmat( ot{i}, [1, numel(new)] ); new], 1 );
                    ot = [ot new_oo_i]; end;  %#ok<AGROW>
                ot( 1:n_old ) = []; end;
            if( period );
                ot = remove_bad_periodizations( ot, GG ); end;
            if( ~isempty(ot) );
                break;
            else;
                depth = depth + 1; end; end;
        if( ~period );
                nadm = is_notadmissible( ot, GG );
            else;
                nadm = []; end; end;

    if( nargout >= 2 );
        Pt = cell( 1, numel(ot) );
        for i = 1:numel( ot );
            Pt{i} = buildproduct_fast( MM, ot{i} ); end; end;

end

function [ ot, Pt, nadm, depth ] = makePtot_nl(  MM, GG, ~ , ~ , ~  , depth, period );
% Input:
%   M       cell array of square matrices, input matrices
%   o       cell array of column vectors matrices, same numel as PP, ordering of products
% Output:
%   Pt      cell array of square matrices, numel(Pt) = nnz(idx)*numel(M), product of new matrices
%   ot      cell array of column vectors, numel(ot) == nnz(idx)*numel(M), ordering of product of new matrices
% Note:
%   o is actually not used to compute the matrices Pt
%   only the length of o used as information
    
    while( true );
        ot = genNecklacest( depth, numel(MM), GG, period );
        if( ~isempty(ot) );
            break; end;
        depth = depth + 1; end;
    ot = num2cell( ot, 1 );

    if( nargout >= 2 );
        Pt = cell( 1, numel(ot) );
        for i = 1:numel( ot );
            Pt{i} = buildproduct_fast( MM, ot{i} ); end; end;

    if( ~isempty(GG) );
        nadm = is_notadmissible( ot, GG );  % actually, we do the work already in genNecklacest, we could improve this
    else;
        nadm = []; end;
end

function [ nadm ] = is_notadmissible( oo, GG )
    % returns:      false   if ordering is admissible
    %               true    if ordering is admissible, but the periodization is not
    nadm = false( size(oo) );
    for n = 1:numel( oo )
        for i = 1:numel( oo{n} ) - 1;
            if( ~GG(oo{n}(i), oo{n}(i+1)) );
                nadm(n) = true;
                break; end; end;
        if( ~GG(oo{n}(end), oo{n}(1)) );
            nadm(n) = true;
            end; end;
end

function [ ot ] = remove_bad_periodizations( ot, GG )
    % sanitize all orderings which are not allowed when periodized
    % (i.e. there is not path from last to first)
    if( iscell(ot) );
        for i = numel( ot ):-1:1;
            first = ot{i}(1);
            last = ot{i}(end);
            if( ~GG(last, first) );
                ot(i) = []; end; end;
    else;
        for i = size( ot, 2 ):-1:1;
            first = ot(1,i);
            last = ot(end,i);
            if( ~GG(last, first) );
                ot(i) = []; end; end;
    end;
end

function [ Nt, Rt ] = computeNtRt_base( Pt, ot, normfun, rhofun, weight );
% Compute norm and spectral radius
% Input:
%   Pt       cell array of square matrices, input matrices
%   ot       cell array of column vectors matrices, same numel as Pt, ordering of products, only length of the ordering is used, ot must not contain zeros
%   lb       unused
%   normfun  function handle, function which computes matrix norms
%   rhofun   function handle, function which computes spectral radii
% Output:
%   Nt      vector, norms of matrices in Pt
%   Rt      vector, spectral radii of matrices in Pt
% Note:
%   o is actually not used to compute the matrices Pt
%   only the length of o used as information    

    
    nPt = numel( Pt );
    if( ~isempty(Pt) && issym(Pt{1}) );
        [Nt, Rt] = deal( sym(zeros(1, numel(Pt))) );
    else;
        [Nt, Rt] = deal( zeros(1, numel(Pt)) ); end;
    if( nPt < 200 );
        for i = 1:numel( Nt );
            Nt(i) = normfun( Pt{i} );
            Rt(i) = rhofun( Pt{i} ); end;
    else
        parfor i = 1:numel( Nt );
            Nt(i) = normfun( Pt{i} );  %#ok<PFBNS>
            Rt(i) = rhofun( Pt{i} ); end; end;  %#ok<PFBNS>
    if( nargin <= 4 || isempty(weight) );
        exponent = cellfun( 'length', ot );  % this seems to be faster than a for loop
    else;
        exponent = zeros( 1, numel(ot) );
        for i = 1:numel( ot );
            r = hist( ot{i}, 1:numel(weight) );  %#ok<HIST>
            exponent(i) = sum( r.*weight ); end; end;
    Nt = Nt.^(1./exponent);
    Rt = Rt.^(1./exponent);
end 

function [ lb, ub, nlb, nub ] = computelbub_smaxp( lb, ub, nlb, nub, Nt, Rt, nadm, delta_now, GG, weight );
% compute lower and upper bound in search for spectral maximizing products
% Input:
%   lb              scalar, current lower bound
%   ub              scalar, current upper bound
%   Nt              vector, norms of new matrices
%   Rt              vector, spectral radii of new matrices
%   delta_now       1x2 vector, only second element is important, current delta in gripenberg algorithm
% Output:
%   lb              scalar, new lower bound
%   ub              scalar, new upper bound, incorporating delta
    
    if( isempty(GG) );
        if( ~isempty(Rt) );
            lb  = symmax( [lb; symmax(Rt)] ); end;
        if( nargin <= 9 || isempty(weight) );
            ub  = symmin( [ub; symmax(Nt)/min(1, delta_now)] );
        else;
            ub = inf; end;
    else;
        if( ~isempty(Rt) );
            if( any(~nadm) );
                lb  = symmax( [lb; symmax(Rt(~nadm))] ); end;
            nlb = symmax( [lb; nlb; symmax(Rt(nadm))] ); end;
        ub = inf;
        nub = inf; end;
end

function [ lb, ub, nlb, nub ] = computelbub_sminp( lb, ub, nlb, nub, Nt, Rt, nadm, delta_now, GG, ~ );  %#ok<INUSL>
    % compute lower and upper bound
    % Function is untested
    assert( isempty(nadm) );
    lb = 0;  % due to lack of theory, this is the best known lower bound so far
    nlb = 0;
    if( isempty(GG) );
        ub = symmin( [ub; symmin(Rt)] );
    else;
        ub = inf; end;
end

function [ idxall, delta_now ] = choosematrix_lhr( Nt, ~, lb, ub, delta_now, delta, Nlhr, epsilon )  %#ok<INUSL>  % N-high-low-rand
% Choose matrix functions
% =======================
% Chooses products which will get children in the next round
%
% Input:
%       Nt/Rt               norms/spectral radii of last round
%       lb/ub               current lower/upper bound of JSR
%       delta_now           currently computed minimal relative delta, which is used to compute upper bound of JSR (for smaxp)
%       delta               relative delta from Gripenberg algorithm
%       Nlow/Nhigh/Nrand    number of products with lowest/highest norm kept, and number of randomly matrices kept
%       epsilon             epsilon used for relative comparisons (epsilon << 1)
% Output:
%       idx                 chosen products
%       delta_now           new minimal relative delta
%

%XX this function has some severe bugs when sminps are computed
%   handling of symbolic stuff
%   s.min.p.s

    % preprocess
    delta_now = min( [delta_now delta] );
    
    % sort
    [Ntsort, ia] = sort( Nt );
    [~, ib] = sort( ia );  % store indices to unsort it at the end
    
    idxall = logical( isAlways( Ntsort >= lb/(1+epsilon)/delta, 'Unknown','true' ) );
    idxall = logical( idxall );
    idxallorig = idxall;
    if( ~any(idxall) );
        return; end;
    idxall = cumsum( idxall );
    idxfirst = find( idxall == 1, 1 );
       
    val1 = Nlhr(1);
    val2 = max( idxall ) - Nlhr(2);
    val3 = find( idxall == Nlhr(1), 1 );
    if( isempty(val3) );
        val3 = 1; end;
    idxall(idxall > val1 & idxall <= val2) = 0;  % remove not-highest and not-lowest
    if( val2 - Nlhr(2) >= Nlhr(3) );  % add again random
        idxall(randperm( val2 - Nlhr(2), Nlhr(3) ) + val3) = 1;
    else;
        idxall = idxallorig; end;
    
    % compute new delta
    idxfirstafter = find( ~idxall, 1, 'last' );
    if( ~isempty( idxfirstafter ) && ...
        idxfirst < idxfirstafter ...
      )
        if( idxfirstafter == numel(Ntsort) );
            delta_now = 0;
        else;
            val = lb/Ntsort(idxfirstafter + 1);
            delta_now = min( [delta_now, val, delta] ); end; end;
    
    % un-sort
    idxall = logical( idxall(ib) );

end

function [PP, oo, NN, RR, nadm, lb, ub, nlb, nub, living] = add_to_results( PP,Pt, oo,ot, NN,Nt, RR,Rt, nadm,nadmt, lb,lbt, ub,ubt, nlb,nlbt, nub,nubt, living, opt )
    if( ~isempty(opt.plot) );
        [~, Rtidx] = sort( Rt );
        PP   = [PP   Pt(Rtidx)  ];
        oo   = [oo   ot(Rtidx)  ];
        NN   = [NN   Nt(Rtidx)  ];
        RR   = [RR   Rt(Rtidx)  ];
        if( ~isempty(nadmt) );
            nadm = [nadm nadmt(Rtidx)]; end;
        lb   = [lb   lbt ];
        ub   = [ub   ubt ];
        nlb  = [nlb  nlbt];
        nub  = [nub  nubt];
        living(end-numel(Rtidx)+1:end) = paren( living(end-numel(Rtidx)+1:end), Rtidx );
        
    else;
        PP   = [PP   Pt  ];
        oo   = [oo   ot  ];
        NN   = [NN   Nt  ];
        RR   = [RR   Rt  ];
        nadm = [nadm nadmt];
        lb   = [lb   lbt ];
        ub   = [ub   ubt ];
        nlb  = [nlb  nlbt];
        nub  = [nub  nubt]; end;
end

function [ flag ] = check_termination( lb, ub, living_idx, smpdepth, opt )
    
    flag = true;
    if( opt.num_eval == 0 );
        flag = false;
        % do not terminate
    elseif( ~any(living_idx) && opt.test_idx );
        vprintf( 'No new admissible orderings found.\n', 'imp',[1 opt.verbose] );
    elseif( opt.maxsmpdepth && smpdepth > opt.maxsmpdepth );
        % maxsmpdept == 0 means \infty
        vprintf( '''maxsmpdepth'' (%i) reached. Algorithm stops.\n', opt.maxsmpdepth, 'imp',[1 opt.verbose] );
    elseif( ~isempty(opt.bound) && ...
            (lb(end) > opt.bound(1) && ub(end)<opt.bound(2) || lb(end) > opt.bound(2) || ub(end) < opt.bound(1)) ...
          ); 
        vprintf( 'Computed jsr bounds are inside the given bounds. Algorithm stops.\n', 'imp',[1 opt.verbose] );
    elseif( toc(opt.starttime) >= opt.maxtime+.5 ); 
        vprintf( '''maxtime'' (%i) is reached (Maximum time the algorithm shall take). Algorithm stops.\n', opt.maxtime, 'imp',[1 opt.verbose] );
    elseif( opt.num_eval >= opt.maxeval ); 
        vprintf( '''num_eval'' is reached (Maximum number of evaluations). Algorithm stops.\n', 'imp',[1 opt.verbose] );
    elseif( lb(end) == inf || ub(end) == 0 );
        vprintf( 'Extremal bound reached. Algorithm stops.\n', 'imp',[1 opt.verbose] );
    else;
        % do not terminate
        flag = false;
        end;
    
end

function [ maxsmpdepth ] = print_output( ot, Rt, lb,lbt, ub,ubt, nlb,nlbt, nub,nubt, smpdepth, opt );

    maxsmpdepth = opt.maxsmpdepth;

    lowcheck = isAlways( lbt > lb(end) );
    upcheck  = isAlways( ubt < ub(end) );
    nlowcheck = isAlways( nlbt > nlb(end) );
    nupcheck  = isAlways( nubt < nub(end) );

    if( lowcheck || upcheck || nlowcheck || nupcheck );
        lu_str = '';
        lbt_str = '                ';
        ubt_str = '                ';
        nlbt_str = '                ';
        nubt_str = '                ';
        if( lowcheck );
            lu_str = [lu_str 'lower/'];
            lbt_str = sprintf( '%.15g', lbt*opt.scale ); end;
        if( upcheck );
            lu_str = [lu_str 'upper/'];
            ubt_str = sprintf( '%.15g', ubt*opt.scale ); end;
        if( nlowcheck && ~isempty(opt.graph) );
            lu_str = [lu_str 'lower (n.a.)/'];
            nlbt_str = sprintf( '%.15g', nlbt*opt.scale ); end;
        if( nupcheck && ~isempty(opt.graph) );
            lu_str = [lu_str 'upper (n.a.)/'];
            nubt_str = sprintf( '%.15g', nubt*opt.scale ); end;
        if( isempty(opt.graph) );
            lu_str(end:11) = ' ';
        else;
            lu_str(end:39) = ' '; end;
        
        vprintf( 'New bound (%s): [%s, %s], ', lu_str, lbt_str, ubt_str, 'imp',[1 tif(lowcheck, opt.verbose, opt.verbose-1)] );
        if( ~isempty(opt.graph) );
            vprintf( '\b\b / [%s, %s] (n.a.), ', nlbt_str, nubt_str, 'imp',[1 tif(lowcheck, opt.verbose, opt.verbose-1)] ); end;

        len = cellfun( 'prodofsize', ot );
        len = unique( [min(len), max(len)] );
        vprintf( 's.m.p.-length = %v\n', len, 'imp',[1 tif(lowcheck, opt.verbose, opt.verbose-1)] );
        
        if( opt.verbose >= 2 );
            idx_l = isAlways( Rt == lbt );
            idx_u = isAlways( Rt == ubt );
            if( (lowcheck || upcheck ) && any(idx_l) && any(idx_u) );
                vprintf( '\b, with maybe those orderings: (lower) %r | (upper) %r\n', reducelength(idx_l), reducelength(idx_u) );
            elseif( lowcheck && any(idx_l) );
                vprintf( '\b, with maybe those orderings: (lower) %r\n', reducelength(idx_l) );
            elseif( upcheck && any(idx_u) );
                vprintf( '\b, with maybe those orderings: (upper) %r\n', reducelength(idx_u) ); 
            else;
                % do nothing
                end; end;
        maxsmpdepth = max( [smpdepth*opt.hardworking opt.maxsmpdepth] ); end;
end

%% utility functions

function [ cand, idx ] = simplify_ordering( cand, opt )
    % Simplify candidates and remove duplicates
    if( nargin <= 1 );
        opt.graph = []; end;
%   reduceflag = ~isempty( opt.graph );
    idx = [];
    if( ~isempty(cand) );
        for i = 1:numel( cand );
            cand{i} = reducelength( cand{i} ); end;
        LENGTHMAX_P1 = max( cellfun('length',cand) ) + 1;   
        for i = 1:size( cand, 2 ); 
            cand{i}(LENGTHMAX_P1,1) = 0; end;  
        cand = cell2mat( cand );                                
        cand(end,:) = [];                                     
        [cand, idx] = unique( cand', 'rows' );
        cand = num2cell( cand.', 1 );
        for i = 1:numel( cand );
            cand{i}(cand{i} == 0) = []; end; end;
            %cand{i} = removezero( cand{i}, 1 ); end; end;
end

function out = paren( x, varargin );
    out = x(varargin{:});
end


%%

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
