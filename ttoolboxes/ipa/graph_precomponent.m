function [ fromto, H ] = graph_precomponent( G, pre, verbose );
% Computes precomponents of a graph (This is not the standard notion of "connected"
% [ fromto, H ] = graph_precomponent( G, [pre], [verbose] );
%
% Input:
% =========
%   G           adjancy matrix, defines possible paths through the graph 
%               For ipa application: matrix products)
%   pre         default=1. If 0, returns connected components instead of precomponents
%   verbose     default=1. Verbose level
%               If greater equal 2, nice plots are produced
%
% Output:
% =========
%   fromto      denotes from which to which component an edge goes
%               0 denotes, the edge goes to no component (i.e. there is no edge)
%               
%   H           Matlab graph object, where each vertex of G corresponds to a edge in Eg
%               The graph shows, which matrices map from which precomponent to wich precomponent
% 
% Notes:
% =========
%   - A precomponent, is a set of vertices which is connected to another connected precomponent.
%     More precisely, all images of the preimages of a vertex belong to the same precomponent,
%     Furthermore, each vertex belongs to exactly one precomponent.
%     In particular: comp = digraph( G'*G ).conncomp % up to numbering
%   - The graphical output (when verbose == 2) yields a good graphical representation of the precomponents.
%   - The vertices of one precomponent are in general not connected to each other
%   - I suppose, there is a theory behind it and "precomponent" is the wrong name, but I could not find anything
%
% Example:
% =========
%   G = [0 0 1 1;0 0 1 1;1 1 0 0;1 1 10 0];
%   Vertices 1 and 2: Belong to precomponent A
%   Vertices 3 and 4: Belong to precomponent B
%
% Written by: tommsch, 2023-03-31

    % parse input
    if( nargin <= 1 || isempty(pre) );
        pre = 1; end;
    if( nargin <= 2 || isempty(verbose) );
        verbose = 1; end; 
    comp = compute_precomponents_matlab( G, pre );
     
    fromto = compute_from_to( G, comp );
    

    if( nargout >= 2 || verbose>= 2 );
        [ H, name ] = make_component_graph( G, comp ); end;

    if( verbose >= 2 );
        prestr = repmat( 'pre', [1 pre] );
        plot_output( G, H, fromto, name, prestr, verbose ); end;
        
end


function [ comp ] = compute_precomponents_matlab( G, pre );
    comp = digraph( (G')^pre*G ).conncomp;
    [~, ~, comp] = unique( comp, 'stable' );
    comp = comp';
end

function [ comp ] = compute_precomponents_tommsch( G );  %#ok<DEFNU>
    % slower than _matlab version
    J = size( G, 1 );
    comp = zeros( 1, J );  % matrix with precomponent == i, take vectors in Vectorspace i
    done = false( 1, J );
    j = 1;
    while( ~all(done) );
        if( ~done(j) );
            % continue with j
        else;
            % continue with vertex which belongs to same precomponent
            j = find( ~done & comp, 1 );
            if( isempty(j) );
                % continue with a new vertex
                j = find( ~done, 1 ); end; end;
        if( comp(j) == 0 );
            comp(j) = max( comp ) + 1; end;
        M = find( G(:,j) ).';  % look which matrices go the j-th matrix
        for m = M;
            N =  find( G(m,:) );  % look where the matrix goes to
            comp(N) = comp(j); end;  %#ok<FNDSB>
        
        done(j) = true; end;
end

function [ fromto ] = compute_from_to( G, comp );
    J = size( G, 1 );
    fromto = zeros( 2, J );
    for j = 1:J;
        from = comp(j);
        to = unique( comp( logical(G(j,:)) ) );
        if( numel(to) == 0 );
%             assert( ~ispathcomplete(G) );
%             warning( 'graph_precomponent:fatal', 'Graph is most likely not path-complete.' ); 
            fromto(:,j) = [from;0];
        elseif( numel(to) == 1 );
            fromto(:,j) = [from;to];
        else;
            error( 'graph_precomponent:fatal', 'This is most likely a programming error.' ); end;
    end
end

function [ H, name ] = make_component_graph( G, comp );
    J = size( G, 1 );
    E = zeros( 0, 3 );
    for j = 1:J;
        to = find( G(j,:).', 1 );
        if( ~isempty(to) );
            E(end+1,:) = [comp(j) comp(to) j]; end;  %#ok<AGROW>
    end
        
    name = num2cell( char( unique( comp ) + 'A' - 1 ) );
    
    H = digraph( E(:,1), E(:,2), E(:,3), name );
    
end

function plot_output( G, H, fromto, name, prestr, verbose );
    
    
    % compute number of non-trivial components
    cc_all = unique( fromto(1,:) );
    cc = [];
    for c = cc_all;
        idx = c == fromto(1,:);
        Gidx = G(idx,idx);
        if( any(Gidx) );
            cc(end+1) = c; end; end;  %#ok<AGROW>
    nc = numel( cc );
    
    % pre-component graph
    if( verbose >= 3 );
        clf;
        subplot_lazy( nc+5, [1 2] ); end;
    plot( H, 'EdgeLabel',H.Edges.Weight, 'NodeColor',num2color(cc_all), 'EdgeColor',num2color(fromto(1,H.Edges.Weight)) );
    title(  [prestr 'component graph'] );
    
    if( verbose == 2 );
        return; end;
    
    % simplified pre-component graph
    subplot_lazy( nc+5, [3] );  %#ok<NBRAK>
    Hs = simplify( H );
    plot( Hs, 'EdgeLabel',Hs.Edges.Weight, 'NodeColor',num2color(cc_all), 'EdgeColor',num2color(fromto(1,Hs.Edges.Weight)) );
    title( [prestr 'component graph simplified'] );

    % graphs inside each precomponent
    ct = 0;
    for c = cc;
        ct = ct + 1;
        idx = c == fromto(1,:);
        subplot_lazy( nc+5, 3+ct );
        Gidx = G(idx,idx);
        if( ~any(Gidx) );
            continue; end;
        ct = nnz( Gidx );
        ct_inv = nnz( idx )^2 - ct;

        poststr = '';
        if( ct/2 > ct_inv );
            Gidx = ~Gidx;
            poststr = ' (Complement graph!)'; end;

        plot( h_graph(Gidx), 'NodeLabel',find(idx), 'NodeColor',num2color(c), 'EdgeColor',num2color(c) );
        title( ['Inside ' prestr 'component ' name{c} poststr] ); end;

    % input graph
    J = size( G, 1 );
    subplot_lazy( nc+5, [nc+4 inf] );
    ct = nnz( G );
    ct_inv = J^2 - ct;
    poststr = '';
    if( ct/2 > ct_inv );
        G = ~G; 
        poststr = ' (Complement graph!)'; end;
    Gc = h_graph( G );
    plot( Gc, 'NodeColor',num2color(fromto(1,:)) );
    title( ['Input graph' poststr] );
    
end

function [ varargout ] = h_graph( G, varargin );
    if( issymmetric(G) );
        h = @graph;
    else;
        h = @digraph; end;
    varargout = cell( 1, nargout );
    [varargout{:}] = h( G, varargin{:} );
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
