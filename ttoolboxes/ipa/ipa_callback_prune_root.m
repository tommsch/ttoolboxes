function [ bv ] = ipa_callback_prune_root( bv );
% [ bv ] = ipa_callback_prune_root( bv )
% Removes a root, and all its children completely from the cyclictree
%
% Conditions: Currently only applicable for case (K)
%             Does not work if a graph is given
%
% See also: ipa_execute_callback
%
% Written by: tommsch, 2024-10-01

    assert( ~ipa_isgraph(bv.cyclictree.graph), 'ipa:callback_add_candidate', 'Currently this callback does not work with graphs' );
    assert( strcmpi(bv.cyclictree.algorithm(1), 'k'), 'ipa:callback_add_candidate', 'This callback only works for the ''kone'' funct.' );
    assert( all(bv.cyclictree.smpflag == ipa_constant.candidate | bv.cyclictree.smpflag == ipa_constant.candidate), 'ipa:ipa_callback_prune_root', 'This callback only works if all starting vectors are candidates or nearly candidates.' );

    msg = '[Remove candidate: ]';
    remove_msg = true;
    vprintf( '%s', msg, 'imp',[1 bv.param.verbose] );
    msg_len = numel( msg );
    for idx_tree = numel( bv.cyclictree.v0 ):-1:1;
        if( numel(bv.cyclictree.v0) == 1 );
            break; end;
        
        idx_VV = [1:idx_tree-1 idx_tree+1:numel( bv.cyclictree.v0 )];
        %idx_VV = 1:numel( bv.cyclictree.v0 );
        VV = [bv.cyclictree.VV{idx_VV}];
        
        nrm_VV = [bv.cyclictree.norm{idx_VV}];
        
        ratflag = matrixinfo( [bv.cyclictree.v0{:} VV], 'rat' );
        if( ratflag && bv.cyclictree.epssym >= 0 );
            VVs = unique( VV', 'rows' )';
            nrm_v0 = polytopenorm( bv.cyclictree.v0{idx_tree}, VVs, 'ks', 'v',bv.param.verbose - 1, 'bound',[0 1], 'output','ub' );
            idx_nrm_v0 = isAlways( nrm_v0 <= 1, 'Unknown','false' );
        else;
            idx_VV = nrm_VV(2,:) > 1 - bv.param.epssimplepolytope;
            nrm_v0 = polytopenorm( bv.cyclictree.v0{idx_tree}, VV(:,idx_VV), 'k', 'v',bv.param.verbose - 1, 'bound',[0 1], 'output','ub' );
            idx_nrm_v0 = nrm_v0 < 1;
            end;
        
        
        if( ~any(idx_nrm_v0) );
            continue; end;
        
        msg_new = vprintf( '\b%r], ', bv.cyclictree.ordering{idx_tree}.', 'imp',[1 bv.param.verbose] );
        remove_msg = false;
        msg_len = msg_len + numel( msg_new ) - 1;
        bv.cyclictree = cyclictree_remove( idx_tree, bv.cyclictree );
        if( isempty(bv.cyclictree.smpflag) );
            bv.log.errorcode = ipa_errorcode.KONE_ERROR;
            vprintf( '\nI removed the last candidate from the cyclictree.\n  This should not happen and indicates a programming error.\n  I have to quit.  ',  'cpr','err', 'imp',[0 bv.param.verbose] );
            return; end; end;
        if( remove_msg );
            vprintf( repmat('\b', [1 msg_len] ), 'imp',[1 bv.param.verbose] ); end;
end

function [ ct ] = cyclictree_remove( idx_tree, ct );
    
    fn = fieldnames( ct )';
    len = [];
    for n_ = fn; n = n_{1};
        switch n;
            case {'VV','added_in','balancingvector','complexflag','d','has_children','norm','norm_est','norm_parent','num_added','oclass','oo','ordering','parent','prebalancing','rho','rho_ordering','smpflag','toa_ordering','v0','v0_sym','v0idx','v0s','v0s_sym'};
                if( isempty(ct.(n)) );
                    continue; end;
                ct.(n)(idx_tree) = [];
            
            case {'M','Mt','M_sym','Mt_sym','algorithm','delta','delta_sym','epssym','graph','lambda','lambda_sym','num_component','num_matrix','polytopenorm','time_lvl'}

            case {'balancingalpha'};
                ct.(n) = ipa_constant.UNSET_BALANCING_ALPHA;

            case {'norm_lvl'};
                ct.(n)(end) = inf;

            case {'livingvertex'};
                ct.(n)(:) = nan;

            case {'component_idx'};
                assert( isempty(ct.(n)) );

            otherwise;
                if( startsWith(n, 'ipa_callback') );
                    % ok
                else;
                    fprintf( 'Field in .ct struct which is unknown to me: %s\n', n  );
                    fatal_error; end; end; end;
        
    assert( isempty(len) || all( len == len(1) ) );
end





function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
