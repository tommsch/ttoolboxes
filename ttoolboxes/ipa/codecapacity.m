function [ S ] = codecapacity( varargin );
% [ S ] = codecapacity( C, [options] )
% Given forbidden difference patterns, returns a finite set of matrices, whose JSR is connected to its capacity.
% Input: 
%   C                   cell array of forbidden codes, each cell is a row vector of numbers from -base+1 to base-1
%                       all codes must have the same length and must be mutually different
%
% Options:
%   'base',val          (experimental) If not given, then val is computed by the numbers in the given code
%   'verbose',val          verbose level
%   'plot',val          (default: false) Enables plot output            
%   
% Output:
%   Cell array of matrices, whose joint spectral radius shall be computed.
%
% E.g.: codecapacity( {[1 1 0],[1 -1 0]} )
%
% Written by: tommsch, 2018

% parse input
[verbose, varargin]  =  parsem( {'verbose','v'}, varargin, 1 );
[plotflag, varargin] =  parsem( 'plot', varargin, 0 );
[base, varargin] =      parsem( 'base', varargin, [] );

if( nargin == 0 || isempty(varargin{1}) );
    C = randC(); 
elseif( ~iscell(varargin{1}) && isscalar(varargin{1}) );
    C = randC( varargin{1} );
elseif( ischar(varargin{1}) || isstring(varargin{1}) );
    C = cell( 1, numel(varargin) );
    for i = 1:numel( varargin );
        C{i} = zeros( 1, numel(varargin{i}) );
        C{i}(varargin{i} == '+') = 1;
        C{i}(varargin{i} == '-') = -1; end;
else;
    C = varargin{1}; end;
if( ~iscell(C) );
    C = {C}; end;

l = cellfun( @length, C );
if( isempty(base) );
    base = max( cellfun(@(x) max(abs(x)), C)+1 ); end;
vprintf( 'Base: %i. ',base,'imp',[1 verbose] );

if( any(diff(l)) );
    error('codecapacity:length', 'All codes must have the same length.' ); end;  % test if all C_i the same length
if( base ~= 2 );
    warning('codecapacity:base', 'Algorithm is only tested for base 2. Use algorithm with care.' ); end;
if( base == 2 );
    vprintf( 'Maximal size of vertex cover to be computed: %i. ',base^sum(cellfun(@(x) base^nnz(x == 0),C)),'imp',[2 verbose]); end;

% test if all codes are mutually different
l = l(1);
expect( l >= 2, 'codecapactiy:tooshort', 'Length of differences must be at least 2.' );
T = mixvector( 0:base-1, l );
sze = size( T );

vprintf( 'Generate table of differences. ','imp',[1 verbose] );
D = reshape( T',[1 sze(2) sze(1)])-reshape(T',[sze(2) 1 sze(1)] );  % table of differences
i1 = cell( size(C) );
i2 = cell( size(C) );
for i = 1:length( C );
    C{i} = reshape( C{i}, [1 1 length(C{i})] );
    [i1{i},i2{i}] = find( all(bsxfun(@eq, D, C{i}), 3) ); end;  % search for C{i} in the table of differences
clear D T
G = digraph( vertcat(i1{:}), vertcat(i2{:}) );
clear i1 i2

% make bipartite graph adjancy matrix
vprintf( 'Make bipartite graph adjancy matrix. ', 'imp',[1 verbose] );
P = zeros( base, base^(l-2) );
P(1:base,1) = 1;
A = P;
for i = 1:base^(l-2)-1;
    A = [A; circshift( P, i, 2 )]; end;  %#ok<AGROW>

A = repmat( A, [1 base] );
if( plotflag ); 
    subplot( 2, 1, 1 ); 
    plot( digraph(A), 'Layout','subspace3' );
    title( 'Bipartite graph adjancy (de Brujin matrix)' ); 
    drawnow; end;

vprintf( 'Compute all locally minimal vertex covers. ', 'imp',[1 verbose] );
if( plotflag ); 
    subplot( 2, 1, 2 ); 
    plot( G, 'Layout', 'force' );
    title( 'Graph for which vertex cover must be computed' ); 
    drawnow; end;

M = grVerCover( G );

vprintf( 'Generate matrices. \n', 'imp',[1 verbose] );
S = cell( size(M) );
idx = find( A );
for i = 1:numel( S );
    S{i} = A;
    S{i}(idx(M{i})) = 0; end;

end

function C = randC( N );
    % generates a random list of forbidden codes with exactly N zeros values
    if( nargin == 0 );
        N = randi( [2 10] ); end;

    
    J = ceil( (N+1)/2 );
    L = J;
    C = cell( 1, J );
    for j = 1:J;
        C{j} = randi( [-1 1], 1, L ); end;

    C = uniquecell( C );
    J = numel( C );
    while( nnz([C{:}] == 0) > N );
        idx = randi( [1 J] );
        pos = find( C{idx} == 0 );
        if( isempty(pos) );
            continue; end;
        pos = pos( randi(numel(pos)) );
        C{idx}(pos) = -1^randi( [1 2]);
        end;

end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
