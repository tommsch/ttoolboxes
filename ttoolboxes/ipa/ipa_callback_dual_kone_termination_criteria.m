function [ bv ] = ipa_callback_dual_kone_termination_criteria( bv );
% [ bv ] = ipa_callback_dual_kone_termination_criteria( bv )
% Checks whether the vertices of the generated kone do not make accute angles with vertices from the dual kone .cyclictree.polytopenorm.Kt
% Must be executed at position: "terminate_check_begin"
%
% Conditions: Only applicable for case (K)
%
% Written by: tommsch, 2024-10-16

    assert( ~ipa_isgraph(bv.cyclictree.graph), 'ipa:callback_add_candidate', 'Currently this callback does not work with graphs' );
    assert( strcmpi(bv.cyclictree.algorithm(1), 'k'), 'ipa:callback_add_candidate', 'This callback only works for the ''kone'' funct.' );

    VV = [bv.cyclictree.VV{:}];
    Kt = bv.cyclictree.polytopenorm.Kt;
    
    if( numel(VV) < numel(Kt) );
        for iv = 1:size( VV, 2 );
            doti = sum( VV(:,iv) .* Kt );
            if( any(doti > 0) );
                bv.errorcode = ipa_errorcode.NOINVARIANTKONE_DUALTEST;
                return; end; end;
    else;
        for ik = 1:size( Kt, 2 );
            doti = sum( VV .* Kt(:,ik) );
            if( any(doti > 0) );
                bv.errorcode = ipa_errorcode.NOINVARIANTKONE_DUALTEST;
                return; end; end; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
