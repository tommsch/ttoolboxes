function [ A, num_t, nfo ] = matrixenum( varargin )
% [ A, num_out, nfo ] = matrixenum( dim, N, num_in, [D], [simpleflag], [options] )
% [ A, num_out, nfo ] = matrixenum( A, [D], [simpleflag], [options] )
% Returns the k^th set of binary matrices, ordered linearly
% Returns sets of empty matrices, for certain values of k
%
% Input:
% =======
%   dim             dimension
%   N               number of matrices in the set
%   num_in          counter
%                   if num_in==-1, then the maximum number num_in which can be passed is returned
%                   if num_in is greater than the maximum number, then the behaviour is undefined
%                   if num_in is empty, than a randum number is used
%   A               cell array of matrices
%   D               vector, default=[0 1] or determined using A, set of digits, 
%   simpleflag      (experimental) string, default='', If set, then simplifies the matrices
%                       logical/'jsr':  if there exists some kk<k such that matrixenum(kk) would have the same JSR, then the empty set is returned.
%                                       if one of the matrices is the less or equal the identity matrix, this matrix is removed
%                                       if the matrices are reducible to binary matrices of smaller dimensions, the empty set is returned
%                                       passing a logical is deprecated
%                       'kone': if there exists some kk<k such that the kone of matrixenum(kk) can be used to infer information for matrixenum(k)
%
% Options:
% =========
%  'verbose',val    integer, default=1, verbose level
%  'fast',val       logical, default=true, stops simplification of matrices after the first simplification was found
% 
% Output:
% ========
%   A           1xN cell array of dimxdim matrices
%               is set to {} if a smaller num leads to the same set
%               is set to nan if num is larger than largest possible number
%   num_out     num_in of matrices which have same JSR
%               if numel(A) = N, then num_t<=num
%   nfo         table, gives information about the returned set. 
%               See the source code of matrixenum>addnfo for more info
% Note:
% =====
%   The returned set of matrices is not necessarily the one with the smalles number num
%
% E.g.: vdisp( matrixenum(2, 2, 2^5+2^3+2^2+2^1) )
%       vdisp( matrixenum(2, 2, 2^5) )
%
% See also: gallery_matrixset, matrixenum>addnfo
%
% Written by: tommsch, 2018

%            2020-08-27, tommsch, totally rewritten
%                        output format changed
%                        added parameter D (digits)
%            2020-11-15, tommsch, Behaviour change: Cell array of empty matrices are returned for certain inputs, instead of one empty matrix
%            2022-04-01, tommsch, Behaviour change: type of nfo changed to cell array (was table before). This change was necessary for Octave compatibility
% Changelog: 

    [opt, varargin] = parse_input( varargin{:} );
    
    if( numel(varargin) > 2 );
        dim = varargin{1};
        N = varargin{2};
        num = varargin{3};
        if( numel(varargin) == 4 );
            D = sortdigit( varargin{4} );
        else;
            D = [0 1]; end;
        if( isempty(dim) );
            dim = randi( [2 5] ); end;
        if( isempty(N) );
            N = floor( (53*log(2))/(dim^2*log(numel(D))) ); end;
        if( isscalar(D) );
            D = 0:(D-1); end;
        if( isempty(num) );
            maxnum = numel( D )^(dim^2*N);
            if( maxnum >= 2^53 );
                warning( 'matrixenum:index', 'Current implementation cannot generate random matrices of this size. Size and/or number-of-matrices is too large.' );
                maxnum = 2^53 - eps(2^53); end;
            num = randi( [0 maxnum-1] );
        elseif( isequal(num,-1) );
            A = numel( D )^(dim^2*N);
            return;
        elseif( num >= numel(D)^(dim^2*N) || ...
                dim == 0 ...
              );  % check input
            warning( 'matrixenum:input', '''num'' to large.' );
            A = repmat( {[]}, 1, N );
            num_t = [];
            nfo = [];
            return; end;
        A = num2matrix( dim, N, num, D );  % generate matrix
    else;
        A = varargin{1};
        if( iscell(A) );
            A = double( A );
            A = cell2mat( permute(A, [3 1 2]) ); end; 
        if( numel(varargin) == 2 );
            D = sortdigit( varargin{2} );
        else;
            val = unique(A(:));
            D = sortdigit( [min([0; val]):max([0; val])] ); %#ok<NBRAK2>
            vprintf( 'Digit set: %r\n', D, 'imp',[1 opt.verbose] ); end; end;
    
    assert( numel(D) >= 2, 'matrixenum:digit', 'Digit set ''D'' must contain at least 2 values.' );
    
    vprintf( 'num_in: %i\n', matrix2num(A,D), 'imp',[1 opt.verbose] )
    stop = false;
    nfo = cell( 0, 2 );
    if( ~isequal( opt.reduce, false ) );
        while( ~stop );  % make tests
            [An, nfo, stop] = reduce_matrix( A, D, nfo, opt );
            if( opt.fastflag && ~isequal(An,A) );
                A = An;
                break; end;
            if( isequal(An, A) );
                break; end; 
            A = An; end; end;
    
    % unique table
    for i = size( nfo, 1 ):-1:2;
        if( isequal(nfo.id(i), nfo.id(i-1)) );
            nfo(i,:) = []; end; end;
    
    % post processing
    A = squeeze( num2cell(A, [1 2]) )';
    num_t = matrix2num( A, D );
    vprintf( 'num_out: %i\n', num_t, 'imp',[1 opt.verbose] )
end

function [ opt, varargin ] = parse_input( varargin );
    [opt.verbose, varargin] = parsem_fast( {'verbose','v'}, varargin, 1 );
    [opt.fastflag, varargin] = parsem_fast( {'fast'}, varargin, 1 );
    
    if( islogical( varargin{end} ) || ...
        isscalar( varargin{end} ) && (isequal( varargin{end}, true ) || isequal( varargin{end}, false )) ...
      );
        opt.reduce = tif( varargin{end}, 'jsr', false );
        varargin(end) = [];
    elseif( isstring(varargin{end}) || ischar(varargin{end}) )
        switch lower( varargin{end} );
            case {'jsr'}; opt.reduce = 'jsr';
            case {'kone','k'}; opt.reduce = 'kone';
            otherwise; error( 'matrixenum:input', 'Wrong value for ''simpleflag'' given.' ); end;
        varargin(end) = [];
    else;
        opt.reduce = false; end;
end

function [ A, nfo, stop ] = reduce_matrix( A, D, nfo, opt );
    % makes various tests whether a matrix has a smaller number
    % if so, the new matrix is returned otherwise, the original matrix is returned
    % stop is set to true, 
    %       if it is proven that the finiteness conjecture holds OR
    %       if the problem can be reduced to a smaller problem, i.e.
    %               the matrices have invariant subspaces of the same type (i.e. boolean)
    
    stop = false;
    % test less or equal identity, only allowed if all entries are positive
    if( isanyof( opt.reduce, {'jsr'} ) );
        if( isAlways(all( A(:) >= 0 )) );
            val = matrixinfo( squeeze(num2cell(A, [1 2])), 'leidentity' );
            if( any(val) );
                nfo = addnfo( nfo, 'leidentity' );
                A(:,:,val) = []; 
                return; end; end; end;

    % test identity
    if( isanyof( opt.reduce, {'jsr','kone'} ) );
        val = matrixinfo( squeeze(num2cell(A, [1 2])), 'identity' );
        if( any(val) );
            nfo = addnfo( nfo, 'identity' );
            A(:,:,val) = []; 
            return; end; end;

    % test equality
    if( isanyof( opt.reduce, {'jsr','kone'} ) );
        val_eq = matrixinfo( squeeze(num2cell(A, [1 2])), 'equal' );
        val = val_eq .* ~eye(size(val_eq));
        val = any( triu(val) );
        if( any(val) );
            nfo = addnfo( nfo, 'equal' );
            A(:,:,val) = []; 
            return; end; end;
    
    % test zero matrices
    if( isanyof( opt.reduce, {'jsr','kone'} ) );
        idx = ~anym( A, [1 2] );
        if( any(idx) );
            A(:,:,idx) = [];
            nfo = addnfo( nfo, 'zeromatrix' ); 
            return; end; end;
    
    % test for less or equal matrices
    if( isanyof( opt.reduce, {'jsr'} ) );
        if( isempty(setdiff(unique(A(:)), [0 1])) );
            val = matrixinfo( squeeze(num2cell(A, [1 2])), 'le' );
            val = val & ~eye( size(val) );  % remove ones at diagonal
            val = find( any(val, 2) );
            if( ~isempty(val) );
                 A(:,:,val) = [];
                nfo = addnfo( nfo, 'le' ); 
                return; end; end; end;

    % test for transpose
    if( isanyof( opt.reduce, {'jsr','kone'} ) );
        [val, nfo] = testmatrix_worker( A, permute(A, [2 1 3]), D, nfo, 'transpose' );
        if( ~isequal(val,A) );
            A = val;
            return; end; end;
    
    % test for order
    if( isanyof( opt.reduce, {'jsr','kone'} ) );
        if( size(A, 3)<9 );
            p = perms( 1:size(A, 3) );
            for i = 1:size( p, 1 ) - 1;
                [val,nfo] = testmatrix_worker( A, A(:,:,p(i,:)), D, nfo, 'order' ); 
                if( ~isequal(val,A) );
                    A = val;
                    return; end; end; end; end;
    

    % test permutations
    if( isanyof( opt.reduce, {'jsr','kone'} ) );
        dim = size( A, 1 );
        if( dim<9 );
            p = perms( 1:dim );
            for i = 1:size( p, 1 ) - 1;
                [val,nfo] = testmatrix_worker( A, A(p(i,:),p(i,:),:), D, nfo, 'permutation' ); 
                if( ~isequal(val,A) );
                    A = val;
                    return; end; end; end; end;
    
    % test normal
    if( isanyof( opt.reduce, {'jsr'} ) );
        val = matrixinfo( squeeze(num2cell(A, [1 2])), 'normal' );
        if( all(val) );
            stop = true;
            nfo = addnfo( nfo, 'normal' ); 
            return; end; end;
    
    % test symmetric
    if( isanyof( opt.reduce, {'jsr'} ) );
        val = matrixinfo( squeeze(num2cell(A, [1 2])), 'symmetric' );
        if( all(val) );
            stop = true;
            nfo = addnfo( nfo, 'symmetric' ); 
            return; end; end;
    
    % test norm, and thus, upper bound for spectral radius
    if( isanyof( opt.reduce, {'jsr'} ) );
        if( isempty(setdiff(unique(A(:)), [0 1])) );
            if( ~isempty(A) && ( ...
                    max( arrayfun(@(x) norm(A(:,:,x), 1    ), 1:size(A, 3)) ) <= 1 || ...
                    max( arrayfun(@(x) norm(A(:,:,x), 2    ), 1:size(A, 3)) ) <= 1 || ... 
                    max( arrayfun(@(x) norm(A(:,:,x), inf  ), 1:size(A, 3)) ) <= 1 || ...
                    max( arrayfun(@(x) norm(A(:,:,x), 'fro'), 1:size(A, 3)) ) <= 1 ) );
                stop = true;
                nfo = addnfo( nfo, 'rho <= 1' ); 
                return; end; end; end;
    
    % test number of matrices
    if( isanyof( opt.reduce, {'jsr'} ) );
        if( size(A, 3) <= 1 );
            stop = true;
            nfo = addnfo( nfo, 'number' ); 
            return; end; end;
    
    % test positive digits
    if( isanyof( opt.reduce, {'jsr'} ) );
        if( allm(A(:) >= 0) && any(D < 0) );
            nfo = addnfo( nfo, 'positive' ); 
            return; end; end;
    
    % test negative matrices
    if( isanyof( opt.reduce, {'jsr'} ) );
        if( isequal(sort(-D),sort(D)) );
            p = mixvector( [0 1], size(A, 3) );
            for i = 1:size( p, 2 );
                val = A;
                idx = p(:,i) == 0;
                val(:,:,idx) = -val(:,:,idx);
                [val,nfo] = testmatrix_worker( A, val, D, nfo, 'negative' ); 
                if( ~isequal(val,A) );
                    A = val;
                    return; end; end; end; end;
    
    % test invariant subspace
    if( isanyof( opt.reduce, {'jsr'} ) );
        for type = {'perm','basis'};
            M = invariantsubspace( squeeze(num2cell(A, [1 2])), type{1} );
            if( numel(M) > 1 );
                val = [M{:}];
                val = cellfun( @(x) x(:).', val, 'UniformOutput',false );
                val = [val{:}];
                val = setdiff( val, D );
                if( isempty(val) );
                    stop = true;
                    nfo = addnfo( nfo, 'subspace' ); 
                    return; 
                else;
                    2;
                end; end; end; end;

    if( isanyof( opt.reduce, {'kone'} ) );
        mi =  matrixinfo( squeeze(num2cell(A, [1 2])), 'vandergraft' );
        if( ~all(mi) );
            stop = true;
            nfo = addnfo( nfo, 'vandergraft' ); 
            return; end; end;
    
    
%     % test invariant subspace  % XX add this after difference subspace computation is flawless
%     M = invariantsubspace( squeeze(num2cell(A, [1 2])), 'diff' );
%     if( numel(M) > 1 );
%         val = [M{:}];
%         val = cellfun( @(x) x(:).', val, 'UniformOutput',false );
%         val = unique( [val{:}] );
%         val = setdiff( val, D );
%         if( isempty(val) );
%             stop = true;
%             nfo = addnfo( nfo, 'subspace' ); end; end;        
    
end

function [ A, nfo ] = testmatrix_worker( A, Anew, D, nfo, id );
    if( matrix2num(Anew, D) < matrix2num(A, D) ); 
        A = Anew;
        nfo = addnfo( nfo, id ); end;
end

function [ T ] = num2matrix( dim, N, num, D );
    T = dec2base( num, numel(D) )' - '0' + 1;  % make to numel(D)-ary number, 1-base indexed
    T = D(T).';  % replace indexes in T with number in D
    T = [D(1)*ones( dim^2*N-length(T), 1 ); T];  % fill up with digits
    T = reshape( T, [dim dim N 1 1] );  % reshape to tensor of square matrices
end

function [ num ] = matrix2num( T, D );
    if( iscell(T) );  % make to cell, needed for char()
        T = cell2mat( T ); end;
    [~, ~, T] = unique( [D T(:)'], 'stable' );  % make to numel(D)-ary number, 1-base indexed
    T = T(numel( D )+1:end);
    num = char( T(:)'+ '0' -1 );
    num = base2dec( num, numel(D) );
end

function [ nfo ] = addnfo( nfo, id );
    switch id;
        % the number correspond to the theorems in "Jungers - The Joint Spectral Radius"
        case {'order'};         nfo(end+1,:) = {id,'a different ordering of matrices has smaller num'};
        case {'identity'};      nfo(end+1,:) = {id,'one matrix is the identity matrix, and thus, removed'};
        case {'equal'};         nfo(end+1,:) = {id,'at least two matrices are equal, and thus, one of them removed'};
        case {'le'};            nfo(end+1,:) = {id,'one matrix is less or equal than another matrix, and thus, removed'};
        case {'leidentity'};    nfo(end+1,:) = {id,'one matrix is less or equal than the identity matrix, and thus, removed'};
        case {'subspace'};      nfo(end+1,:) = {id,'the matrices are reducible to matrices of the same type and of smaller dimensions'};
        case {'transpose'};     nfo(end+1,:) = {id,'transposed matrices have a smaller number than num'};
        case {'permutation'};   nfo(end+1,:) = {id,'there exist matrices and permutation which have permuted a smaller number than num'};
        case {'normal'};        nfo(end+1,:) = {id,'all matrices are normal and the finiteness conjecture holds'};
        case {'symmetric'};     nfo(end+1,:) = {id,'the matrices are symmetric and the finitesness conjecture holds'};
        case {'rho <= 1'};        nfo(end+1,:) = {id,'rho(1) <= 1, and thus, the finiteness conjecture holds'};
        case {'451'};           nfo(end+1,:) = {id,'missing'};
        case {'462'};           nfo(end+1,:) = {id,'missing'};
        case {'463'};           nfo(end+1,:) = {id,'missing'};
        case {'number'};        nfo(end+1,:) = {id,'less than one matrix remaining, and thus, the finiteness property holds'};
        case {'negative'};      nfo(end+1,:) = {id,'set of matrices, some of which negative, has smaller num'};
        case {'vandergraft'};   nfo(end+1,:) = {id,'at least one matrix is not vandergraft'};
        case {'zeromatrix'};    nfo(end+1,:) = {id,'one matrix is the zero matrix, and thus, removed'};
        case {'positive'};      nfo(end+1,:) = {id,'matrices have only non-negative digits, and thus, the problem reduces to a simpler one.'};
        otherwise; error( 'matrixenum:addnfo:num', 'wrong number' ); end;
end

function D = sortdigit( D );
     D = unique( D(:) );
     idx = D  >= 0;
     D = [D(idx).' D(~idx).'];
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
