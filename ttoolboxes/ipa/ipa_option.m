function [ M, var ] = ipa_option( varargin )
% [ M, var ] = ipa_option( M, [G], [options] )
%   Parses all global options and stores them in var
%
%   Input:
%       M           input matrix as given on command line
%       G           optional, adjacency matrix of directed graph, default=[],
%                   if given this matrix determines the allowed matrix products
%                   A matrix product A_jn*...*A_j1 (an ordering [j1, ..., jn] )is allowed if G(j_i,j_i+1)==1
%                   These are exactly the paths which can be visualized via `plot( digraph(G) )`
%       [options]   command line arguments
%   
%   Options:
%   'funct',str     (optional), default='auto', The case to be considered:  Currently the below ones should be supported.
%                   The long string form can also be given alone, i.e. not using the name-value pair 'funct',str
%                       'k','K','kone'
%                       'L','lsr'
%                       'a','auto','jsr'  (default)
%                       'P','pos'
%                       'R','minkowski'
%                       'C','c','ca','cb','cc','cd','ce','C','CA','CB','CC','CD','CE','complex'
%   For the remaining options, see the source code below
%
%   Output:
%       var         the var-struct for ipa
%
%   See also: ipa
%
% Written by: tommsch, 2018

%               tommsch,    2023-03-29,     Bugfix: Removed shorthand option 'p' for 'plot'
%               tommsch,    2023-05-04,     New option: 'predelta'
%               tommsch,    2024-02-01,     New option: 'timeofaction'
%               tommsch,    2024-07-20,     New allowed value for 'smp': <'smp','all', 'maxsmpdepth',n> uses all products up to length as s.m.p.s
%               tommsch,    2024-09-09,     Big refactoring of names
%                                           Removed option 'help'
%               tommsch,    2024-10-06,     Removed option 'dumpvertex'
%               tommsch,    2024-10-17,     Removed option 'projection', 'symstrict'
%               tommsch,    2024-10-18,     Removed option 'rhocomp', added callback-convenience option 'ipa_callback_remove_matrices_based_on_rho'
% Changelog:    tommsch,    2024-10-25,     Removed option 'epspolytope'
%               tommsch,    2024-12-04,     Removed options 'pause','memory','smallestchoice'
%                                           Added callbacks 'callback_plot_begin', 'callback_plot_end'
%               tommsch,    2025-01-08,     Removed option 'threadplot'
    
    var = struct;
    var.log = make_log_stub();
    
    % search for matrix and graph
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [M, graph, varargin] = parsematrixset( varargin{:} );
    graph = preprocess_graph( graph );
    mi = matrixinfo( M, 'ternary', 'int', 'rat', 'sym', 'symstrict' );
    
    % just parse the input
    %%%%%%%%%%%%%%%%%%%%%%
    [var.param, varargin] = parse_options( M, graph, varargin{:} );
    [var.param.algorithm, var.param.minmax] = process_algorithm( var.param.algorithm );

    [var.param, varargin] = parse_dependent_options( M, var.param, varargin{:} );
    var.param =             process_dependent_values( M, var.param, mi );
    var.param =             process_values( M, var.param, mi );
    var.JSR =               var.param.JSR;
    
    [var.M, var.M_sym] =    process_M( M, var.param.epssym, mi );
        

    % Post-preprocessing
    %%%%%%%%%%%%%%%%%%%%
    vprintf( 'Arguments: %v\n',var.log.varargin, 'imp',[2 var.param.verbose] );
    parsem( varargin, 'test' );
    %if( ~isempty(varargin) && var.param.verbose >= 0 );
    %    arg_str = vdisp( varargin );
    %    error( 'ipa:unkown_option', 'Unkown options in argument: %s', arg_str ); end;
    
    var = orders_field( var );
    
end

function [ log ] = make_log_stub( varargin );
    log.starttime = tic;  % starting time
    log.error = '';
    log.errorcode = ipa_errorcode.UNSETERROR;
    log.errorinformation = {};
    log.varargin = varargin;
end

function [ param, varargin ] = parse_options( M, graph, varargin );
    % Output Options
    %%%%%%%%%%%%%%%%
    [param.plot, varargin] =                  parsem( {'plot'}, varargin, 'none' );                                  % Specfies what to plot. See "ipa_plotoutput" for help.
    param.plot = tocell( param.plot );
    [param.verbose, varargin] =               parsem( {'verbose','v'}, varargin, 1, 'expect',@isnumeric );           % Specifies the verbose-level: In particular: -1 disables all output, 0 disables all output but severe warnings. Level -1 is not recommended.
    if( numel(M) == 0 );
        return; end;    

    % Input Options
    %%%%%%%%%%%%%%%
    param.graph = graph;
    [param.toa, varargin] =                   parsem( {'timeofaction','time_of_action','toa'}, varargin, [] );                        % weights used to compute averaged spectral radius
    [param.invariantsubspace, varargin] =     parsem( 'invariantsubspace', varargin, 'auto' );                                        % Whether to search for invariant subspaces. See "invariantsubspace" for help
    [param.period, varargin] =                parsem( {'period','periodics','periodizable','periodicable'}, varargin, true);          % Only meaningful if graph is given; whether to consider all possible products, or only admissible,periodizable products
    [param.invariantsubspace_sym, varargin] = parsem( {'invariantsubspace_sym','invariantsubspacesym'}, varargin, 3, 'expect',{'clcl',[0 4]} );
    [val1, varargin] =                        parsem( 'nopreprocess', varargin );                                                                   % Disable preprocessing of matrices. See "preprocessmatrix" for help.
    [val2, varargin] =                        parsem( 'preprocess', varargin, ~val1 );                                                              % Disable preprocessing of matrices. See "preprocessmatrix" for help.
    param.preprocess = val2;
    [param.JSR, varargin] =                   parsem( 'JSR', varargin, [0 inf],                                      'expect',@(x) numel(x) == 2 );   % Can be used to provide a (true!) estimate for the JSR. This estimate is used in various parts of the algorithm. The exact behaviour is subject to be changed.
    
    % general parameters
    %%%%%%%%%%%%%%%%%%%%
    param.dim =                               size( M{1}, 1 );                                                    % Dimension of the matrices

    % preworker options
    %%%%%%%%%%%%%%%%%%%
    [param.autoextravertex, varargin] =       parsem( {'autoextravertex','autoev'}, varargin, [] );                                                                         % Adds a vertex for all directions whose corresponding singular value is less than "maximal-singular-value x autoextravertex".
    [param.balancing, varargin] =             parsem( {'balancing','balancingvector','balance'}, varargin, [] );  % Controls balancing procedure
                                                                                                                  %          -2           only extra vertices and vertices belonging to nearly candidates are balanced
                                                                                                                  %          -1           balancing is done if a balancing vector was found (this is the default behaviour if delta<1)
                                                                                                                  %           0           no balancing 
                                                                                                                  %           1/empty     balancing ( default ), algorithm restarts if balancing fails (i.e. when balancing_alpha == 1 algorithm restarts)
                                                                                                                  %           2           balancing, algorithm restarts only if balancing fails totally (i.e. when balancing_alpha == 1 algorithm does not restart)
                                                                                                                  %           vector:     1 x numel(ordering) vector, specifies balancing factors 
                                                                                                                  %     Note that, if balancing fails it is an indication that the s.m.p.s candidates are not s.m.p.s.
    [param.balancingdepth, varargin] =        parsem( 'balancingdepth', varargin, [],                                                         'expecte',{'clcl',[0 20]} );  % Length of matrix products used for balancing. default = [] ( automatic )    
    [param.basicjsrtest, varargin] =          parsem( {'basicjsrtest','basictest','basictests'}, varargin, -1 );                                                            % Whether to make basic tests to compute the jsr. Only works in trivial cases.
    [param.complexeigenvector, varargin] =    parsem( {'complexeigenvector','complex','cpev'}, varargin, [],                                  'expecte',{'clcl',[-1 4]} );  % See ''leadingeigenvector'' for help.
    [delta_epsilon, varargin] =               parsem( 'epsilon', varargin, 0,                                                                 'expect',{'clop',[0 inf]} );  % (experimental) Matrices are multiplied by 1/(1+epsilon) after the construction of the cyclic tree. A value greater than zero leads to faster convergence, but the algorithm can only report bounds for the JSR anymore.
    [param.delta, varargin] =                 parsem( {'delta','postdelta','postdeltascaling'}, varargin, 1/(1 + delta_epsilon),              'expect',{'opop',[0 inf]} );    % Matrices are multiplied by delta after the construction of the cyclic tree. A value less than one leads to faster convergence, but the algorithm can only report bounds for the JSR anymore.
    [param.extravertex, varargin] =           parsem( {'extravertex','ev'}, varargin, {} );                                                                                 % Specifies extra vertices, cell array of vectors OR matrix consisting of column vectors.    
    [param.epssimplepolytope, varargin] =     parsem( 'epssimplepolytope', varargin, 1e-8,                'expect',{'clcl',[0 1]} );    % Vertices with distance less than epssimplepolytope to other vertices are not used for the polytope in the norm computation.
    [symflag, varargin] =                     parsem( {'sym'}, varargin, [] );                                                          % Convenience option to enable symbolic computation with default parameters.
    if( ~isempty(symflag) );
        if( isanyof(symflag, {-1,'auto','default'}) );
            symflag = -1;
        elseif( isanyof(symflag, {1,true,'on','yes'}) );
            symflag = 1;
        elseif( isanyof(symflag, {0,false,'off','no'}) );
            symflag = nan;
        else;
            error( 'ipa:option', 'Wrong value for option ''sym''.' ); end; end;
    [param.epssym, varargin] =                parsem( {'eps_sym'}, varargin, symflag  );   % epsilon which is used to decided whether floating point numbers shall be compuated exactly and compared symbolically.
                                                                                                    %             nan, symbolic comparisons are disabled
                                                                                                    %              [], (default) automatic, if input matrices are integer valued, then ''-1'', otherwise ''nan''.
                                                                                                    %              -1, first tries without symbolic computation.
                                                                                                    %            >= 0, symbolic computations are enabled
    [param.findsmp.nearlycanddelta,varargin]= parsem( {'findsmp_nearlycandidate','findsmp_nearlycanddelta','findsmp_ncdelta','findsmp_ncd','findsmp_nc','nearlycandidate','nearlycanddelta','ncdelta','ncd','nc'}, varargin, .9999 );                                        % For all options of .findsmp see "findsmp" for help
    [param.findsmp.maxsmpdepth, varargin] =   parsem( {'findsmp_maxsmpdepth','maxsmpdepth'}, varargin, [],                                  'expecte',{'clop',[0 inf]} );   % Maximum lenght of s.m.p.-candidates to search for. See findsmp() for more help.
    [param.findsmp.N, varargin] =             parsem( {'findsmp_N'}, varargin, [],                                                          'expecte',{'clcl',[0 inf]} );   % Maxmimum number of candidates to keep on each level in search for s.m.p.-candidates. See findsmp() for more help.
    [param.findsmp.bound, varargin] =         parsem( {'findsmp_bound','sufficientbound'}, varargin, [],  'expect',@(x) numel(x) == 2 || numel(x) == 0 );                   % Search for s.m.p.-candidate stops if given bound is fulfilled. See findsmp() for more help.    
    [param.findsmp.delta, varargin] =         parsem( {'findsmp_delta'}, varargin, [],                                                      'expecte',{'clcl',[0 1]} );
    [param.findsmp.sym, varargin] =           parsem( {'findsmp_sym'}, varargin, [] );                                                                         % Symbolic options for findsmp
    [param.findsmp.verbose, varargin] =       parsem( {'findsmp_v','findsmp_verbose'}, varargin, param.verbose - 1 );
    [param.leadingeigenvector.sym, varargin] =parsem( {'lev_sym','leadingeigenvector_sym'}, varargin, [] );
    [param.maxnum_candidate, varargin] =      parsem( 'maxnumcandidate', varargin, [], 'expecte',{'clop',[0 inf]} );  % Algorithm restarts if there are more candidates maxnum_candidate, in which case the value of "maxsmpdepth" is reduced.
    [param.multipleeigenvector, varargin] =   parsem( 'multipleeigenvector', varargin, 1 );                           % Whether to take all leading eigenvectors of the candidates.
    [param.normalization, varargin] =         parsem( {'normalization','normalize'}, varargin, [] );                  % default = true, If false, the input matrices are not scaled according to its (estimated) JSR.
    [param.ordering, varargin] =              parsem( {'smp','ordering','oo','o'}, varargin, {} );                          % Can be used to specify which orderings are s.m.p.-candidates/nearly-s.m.p.s. Eg: < ''ordering'',{[1 2]'',[1]}.
    [param.smpflag, varargin] =               parsem( {'smpflag','ooflag','oflag'}, varargin, [] );                      % Flags indicating of which type the passed vectors are.
    [param.v0, varargin] =                    parsem( 'v0', varargin, {} );                                              % Can be used to specify the corresponding eigenvectors to the candidates defined by ''ordering''. If v0 is given, ordering must be given, v0s can be given.
    [param.v0s, varargin] =                   parsem( 'v0s', varargin, {} );                                             % an be used to specify corresponding left-eigenvectors to the candidates defined by ''ordering''. If v0s is given, ordering and v0 must be given.
    [param.lambda, varargin] =                parsem( {'lambda','la'}, varargin, [] );                                   % Can be used to specify lambda = joint spectral radius, used to normalize the matrices

    % worker options
    %%%%%%%%%%%%%%%%
    [param.algorithm, varargin] =             parsem( {'case','algorithm','algo','alg','func','funct'}, varargin, 'a' );  % The to be chosen algorithm: 'a'='auto', 'p'='pos', 'R'='mink', 'c'='complex', 'k'='kone', 'l'=lsr
    [~, varargin, retname, ~, contained] =    parsem( {'jsr','lsr','jssr','kone','cone','sub','minkowski','complex'}, varargin );
    if( contained && isequal(param.algorithm, 'a') );
        param.algorithm = retname; end;
    [param.extrapath.mindepth, varargin] =    parsem( {'extrapath_mindepth','extrapath_miniteration','extrapath_miniter'}, varargin, [],  'expecte',{'clop',[0 inf]} );  % Minimal depth before an extrapath is considered
    [param.extrapath.maxdepth, varargin] =    parsem( {'extrapath_maxdepth','extrapath_depth'}, varargin, [],            'expecte',{'clop',[0 inf]} );  % Maximal additional depth of the extrapath.
    [param.extrapath.minnum_vertex,varargin] =parsem( {'extrapath_minvertex','extrapath_minnumvertex'}, varargin, [],                  'expecte',{'clop',[0 inf]} );   % Minimal number of vertices of polytope before an extrapath is considered    
    [param.extrapath.maxnum, varargin] =      parsem( {'extrapath_maxnum','extrapath_num'}, varargin, [],                                   'expecte',{'clop',[0 inf]} );  % Checks whether images of depth extradepth  are inside.
    [param.extrapath.maxnorm, varargin] =     parsem( {'extrapath_maxnorm','extrapath_norm'}, varargin, [],                'expecte',{'clcl',[0 inf]} );  % Maximal norm of a vertex so that it is considered to be the root of an extrapath.
    [param.extrapath.flag, varargin] =        parsem( {'extrapath','extrapath_flag'}, varargin, 0 );                                                                                                          % Convenience option for setting all extraparth options. The higher the number, the more aggressive.
    [param.fastnorm, varargin] =              parsem( 'fastnorm', varargin, 1, 'expect',{0, 1, 2, 'none', 'never', 'inside', 'in', 'outside','out', 'inout'} );  % Specifies how to estimate the norms.
                                                                                                                                                                 %     0    do not estimate the norms
                                                                                                                                                                 %     1    if point is proven to be inside, its norm is set to ipa_constant.INSIDE
                                                                                                                                                                 %     2    (experimental) furthermore, if point is proven to be outside, its norm is set to IPA_OUTSIDE. This option should not be used at the moment
    [param.naturalselection, varargin] =      parsem( 'naturalselection', varargin, [], 'expecte',{'clcl',[0 inf]} );                        % Minimum number of vertices whose norm are computed in each level.
    [param.naturalselectiontype, varargin] =  parsem( 'naturalselectiontype', varargin, inf, 'expect',{inf -inf 1 -1 2 -2 3 -3 100 -100} );  % Specifies how new vertices are selected
                                                                                                                                             %               +/- inf     (default) automatic (3x by +1-norm-estimate, 1x by +2-parent-norm)
                                                                                                                                             %               +/- 1       norm-estimate
                                                                                                                                             %               +/- 2       parent-norm
                                                                                                                                             %               +/- 3       rho (is not working good)
                                                                                                                                             %               +/- 100     -rho (for debug and test reasons)
                                                                                                                                             %               If the value is positive, then all children of a vertex are selected, if at least on of them is selected. ' n
                                                                                                                                             %               If the value is negative, the algorithm cannot produce intermediate bounds for the jsr, but the algorithm may be faster.
    [param.solver, varargin] =                parsem( 'solver', varargin, 'auto', 'expect',{'auto','gurobi','matlab','sedumi','a','g','m','s'} );         % Specifies which solver to use.
    [~, varargin, retname, ~, contained] =    parsem( {'auto','gurobi','matlab','sedumi'}, varargin );         % Specifies which solver to use.
    if( contained && isequal(param.solver, 'auto') );
        param.solver = retname; end;
    [param.testoldvertex, varargin] =         parsem( {'estimateoldvertex','estimateoldvertices','testoldvertex','testoldvertices','recomputenorm','recomputevertex'}, varargin, 1 );  % When to test old vertices. 0: never, 1 = (default) auto (mostly only estimate norms), 2 = after each step (mostly only estimate norms), 3 = after each step (compute norm exactly)
    
    % termination options
    %%%%%%%%%%%%%%%%%%%%%
    [param.stale_norm, varargin] =            parsem( {'stale_norm','stale_norm_iter','max_stale_norm','stagnate_norm','stagnate_norm_iter','max_stagnate_norm'}, varargin, [] );                  % Computation stops after norm did not decrease for given number of iterations, scalar or 2-element vector
    [param.maxnum_restart, varargin] =        parsem( {'maxnum_restart','maxrestart'}, varargin, 10,   'expect',{'opcl',[0 inf]} );       % Maximum number of restarts
    [param.epseigenplane, varargin] =         parsem( {'epseigenplane','epseig'}, varargin, [],        'expecte',{'clcl',[-inf inf]} );   % Computation restarts if point is found which lies more than epseigenplane outside of the supporting eigenplanes. Set to +inf to disable this test.
    [param.epsspectralradius, varargin] =     parsem( {'epsspectralradius','epsrho'}, varargin, [],    'expecte',{'clcl',[-inf inf]} );   % Computation restarts if matrix product with averaged spectral radius greater equal 1+epsspectralradius is found. Set to +inf to disable this test.
    [param.maxiteration, varargin] =          parsem( {'maxiteration','maxiter'}, varargin, inf,       'expect',{'clcl',[1 inf]} );       % Computation stops after iterating the algorithm ''maxiteration'' times.
    [param.maxstepnumber, varargin] =         parsem( 'maxstepnumber', varargin, inf,                  'expect',{'clcl',[1 inf]} );       % Computation stops after more than ''maxstepnumber'' vertices are tested.
    [param.maxtreedepth, varargin] =          parsem( 'maxtreedepth', varargin, inf,                   'expect',{'clcl',[1 inf]} );       % Computation stops after tree has more than ''maxtreedepth'' levels
    [param.maxtreetime, varargin] =           parsem( 'maxtreetime', varargin, inf,                    'expect',{'clcl',[1 inf]} );       % Computation stops after the building of the tree takes more than ''maxtreetime'' seconds
    [param.maxtime, varargin] =               parsem( 'maxtime', varargin, inf,                        'expect',{'clcl',[1 inf]} );       % Computation stops after ''maxtime'' seconds
    [param.maxnum_vertex, varargin] =         parsem( {'maxnumvertex','maxvertexnumber','maxvertex','maxvertexnum'}, varargin, inf,  'expect',{'clcl',[1 inf]} );           % Computation stops after polytope has more than ''maxnum_vertex''
    [param.maxremainingvertex, varargin] =    parsem( {'maxremainingvertex','maxremainvertex','maxremain'}, varargin, inf,  'expect',{'clcl',[1 inf]} );     % Computation stops after polytope has more than ''maxremainingvertex'' remaining vertices to compute.
    [param.maxonenorm, varargin] =            parsem( {'maxonenorm','max1norm'}, varargin, [],         'expecte',{'clcl',[1 inf]} );      % Computation stops after more than ''maxonenorm'' vertices with norm approximately 1 occured
    [param.maxnormerr, varargin] =            parsem( {'maxnormerr','maxnormerror'}, varargin, 100,    'expect',{'clcl',[1 inf]} );       % Computation stops after more than ''maxnormerr'' norm computations returned nan or inf
    [val, varargin] =                         parsem( 'testeigenplane', varargin, [] );                                                   % Helper option: Enables eigenplane stopping criterion with default parameters. May override other parameters.
    if( isequal(val, 1) );
        param.epseigenplane = 1e-10;
    elseif( isequal(val, 0) );
        param.epseigenplane = -inf; end;
    [val, varargin] =                         parsem( {'testspectralradius', 'testrho', 'rhotest'}, varargin, [] );                       % Helper option: Enables spectral radius stopping criterion with default parameters. May override other parameters.
    if( isequal(val, 0) );
        param.epsspectralradius = inf; end;    
    [param.validateupperbound, varargin] =    parsem( {'validateupperbound','ub'}, varargin, -inf,     'expect',{'clcl',[-inf inf]} );    % Computation stops if upper estimate of JSR is less than validateupperbound.
    [param.validatelowerbound, varargin] =    parsem( {'validatelowerbound','lb'}, varargin, +inf,     'expect',{'clcl',[-inf inf]} );    % Computation stops if lower estimate of JSR is greater than validatelowerbound.
    [param.validateupperbound_norm, varargin] =  parsem( {'validateupperbound_norm','ub_norm','ub_nrm'}, varargin, [],     'expecte',{'clcl',[-inf inf]} );    % Computation stops if upper estimate of norm is less than validateupperbound_norm.

    % callbacks
    %%%%%%%%%%%
    [param.callback.begin, varargin] =                      parsem( {'callback_begin','begin'}, varargin, {} );
    [param.callback.end,   varargin] =                      parsem( {'callback_end','end'},     varargin, {} );
    [param.callback.iter_begin, varargin] =                 parsem( {'callback_iter_begin','iter_begin'}, varargin, {} );
    [param.callback.iter_end,   varargin] =                 parsem( {'callback_iter_end',  'iter_end'},   varargin, {} );
    [param.callback.worker_begin, varargin] =               parsem( {'callback_worker_begin','worker_begin'}, varargin, {} );
    [param.callback.worker_end,   varargin] =               parsem( {'callback_worker_end',  'worker_end'},   varargin, {} );
    [param.callback.generatenewvertex_begin, varargin] =    parsem( {'callback_generatenewvertex_begin','generatenewvertex_begin'}, varargin, {} );
    [param.callback.generatenewvertex_end,   varargin] =    parsem( {'callback_generatenewvertex_end',  'generatenewvertex_end'},   varargin, {} );
    [param.callback.terminate_check_begin, varargin] =      parsem( {'callback_terminate_check_begin','terminate_check_begin'},   varargin, {} );
    [param.callback.terminate_check_end,   varargin] =      parsem( {'callback_terminate_check_end',  'terminate_check_end'},     varargin, {} );
    [param.callback.plot_begin, varargin] =                 parsem( {'callback_plot_begin','plot_begin'},   varargin, {} );
    [param.callback.plot_end,   varargin] =                 parsem( {'callback_plot_end',  'plot_end'},     varargin, {} );


    % Constants and Debug-options
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [param.admissiblealgorithm, varargin] =   parsem( 'admissiblealgorithm', varargin, [] );                                              % (developer option) (experimental) Defines admissible algorithms. Program aborts if different algorithm is chosen.
    [param.alwaysout, varargin] =             parsem( {'allout','alwaysout'}, varargin );                                                 % (developer option) Assumes a new point is always out, by setting all computed norms to infinity, also the points from the root and nearvertices.
    [param.debug, varargin] =                 parsem( {'debug'}, varargin );                                                              % (developer option) indicates that this is a debug run, disables multithreading where possible etc...
    [param.dryrun, varargin] =                parsem( {'dryrun'}, varargin );                                                             % (developer option) (experimental) terminates algorithm before entering the main loop.    
    [param.epsequal, varargin] =              parsem( 'epsequal', varargin, 1e-12,                    'expect',{'clcl',[-1 1e-3]} );      % (developer option) (experimental) epsilon which is used to compare to floating numbers for equality    
    [param.minnum_matrix, varargin] =         parsem( {'minnummatrix'}, varargin, 1,                  'expect', {0,1,2} );                % (developer option) Minimum number of matrices for which invariant polytope algorithm is started.
    [nocycle, varargin] =                     parsem( {'nocycle'}, varargin, 0 );                                                         % (developer option) (deprecated option) Compute no eigenvectors of cyclic permutations.
    [param.cycle, varargin] =                 parsem( {'cycle'}, varargin, ~nocycle );                                                    % (developer option) Compute eigenvectors of cyclic permutations.
    [param.norestart, varargin] =             parsem( {'norestart'}, varargin, 0 );                                                       % (developer option) (experimental) Disables restart of algorithm. The higher the value, the more restarts are disabled.
    [param.prebalancing, varargin] =          parsem( {'prebalance','prebalancing'}, varargin, [],    'expecte',@isvector );              % (developer option) (experimental) Balancing vector for eigenvalues before computation of extra vertices   
    [param.predelta, varargin] =              parsem( {'predelta','predeltascaling'}, varargin, 1 );                                      % (developer option) (experimental) Matrices are multiplied by given delta, before! computation of the cyclic root. If this option is given, the result may be wrong.
    [param.prematrixscaling, varargin] =      parsem( {'prematrix','prematrixscaling'}, varargin, [] );                                   % (developer option) (experimental) Matrices are multiplied by given prematrix, before! computation of the cyclic root. If this option is given, the result may be wrong.
    [param.save_to_disk, varargin] =          parsem( {'save','s'}, varargin, 0,                      'expect',{0 1 2 3} );               % (developer option) Specfies how much output shall be saved. 0: (default) disables saving of output, 1: save at end, 2: save in between, 3: save in between with different filenames']
    [param.showquantity, varargin] =          parsem( 'showquantity', varargin, param.verbose*25, 'expect',@isnumeric );                  % (developer option) up to which size shall sets of vectors, matrices, etc. be displayed
    [param.num_core, varargin] =              parsem( {'numcore','numthread'}, varargin, num_cores( ~param.debug ), 'expect',{'clcl',[0 inf]} );  % Number of threads used in computation of the polytope norm. If num_core == 1, then the computation may be done in the main thread
    if( param.num_core <= 0 );
        param.num_core = 0; end;
end

function [param, varargin] = parse_dependent_options( M, param, varargin );
    if( isequal(param.minmax, 'kone') );
        [param, varargin] = parse_kone_options( M, param, varargin{:} );
    else;
        param.polytopenorm.auxiliary_data = {};
        end;
end

function [ param ] = process_dependent_values( M, param, mi );
    if( isequal(param.minmax, 'kone') );
        param = process_kone_values( M, param, mi );
    else;
        param.polytopenorm.auxiliary_data = {};
        end;
end

function [ param ] = process_kone_values( M, param, mi );  %#ok<INUSD>
    if( param.delta > 1 && isempty(param.validateupperbound_norm) );
        param.validateupperbound_norm = (1 + param.delta)/2; end;
    if( isempty(param.ordering) && isempty(param.findsmp.maxsmpdepth) && isempty(param.v0) && isempty(param.v0s) );
        param.ordering = 'all'; end;
    if( isequal(param.ordering, 'all') && isempty(param.findsmp.maxsmpdepth) );
        param.findsmp.maxsmpdepth = 1; end;
end

function [ param ] = process_values( M, param, mi );
    [param.admissiblealgorithm] =           process_admissiblealgorithm( param.admissiblealgorithm );
    [param.autoextravertex] =               process_autoextravertex( param.autoextravertex, param.graph, param.smpflag );
    [param.balancing] =                     process_balancing( param.balancing, param.delta );
    [param.basicjsrtest] =                  process_basicjsrtest( param.basicjsrtest, param.minmax, mi );
    [param.callback] =                      process_callbacks( param.callback );
    [param.complexeigenvector] =            process_complexeigenvector( param.algorithm, param.complexeigenvector, param.delta, param.minmax );
    [param.debug, param.num_core] =         process_debug( param.debug, param.num_core );
    [param.dryrun, param.callback] =        process_dryrun( param.dryrun, param.callback);
    [param.epsspectralradius] =             process_epsspectralradius( param.epsspectralradius, param.minmax );
    [param.epseigenplane, param.callback] = process_epseigenplane( param.epseigenplane, param.callback, param.minmax );
    [param.epssym] =                        process_epssym( param.epssym, param.extravertex, param.v0, param.v0s, mi );
    [param.extrapath] =                     process_extrapath( param.extrapath, param.extrapath.flag, param.dim );
    [param.fastnorm] =                      process_fastnorm( param.fastnorm );
    [param.findsmp.maxsmpdepth] =           process_maxsmpdepth( param.findsmp.maxsmpdepth, param.minmax );
    [param.findsmp.sym] =                   process_findsmp_sym( param.findsmp.sym, param.epssym );
    [param.invariantsubspace] =             process_invariantsubspace( param.invariantsubspace, numel(M), param.minnum_matrix, param.minmax );
    [param.leadingeigenvector.sym] =        process_leadingeigenvector_sym( param.leadingeigenvector.sym, param.epssym );
    [param.maxonenorm] =                    process_maxonenorm( param.maxonenorm, param.dim );
    [param.naturalselection] =              process_naturalselection( param.naturalselection, param.num_core );
    [param.normalization] =                 process_normalization( param.normalization, param.minmax );
    [param.smpflag, param.ordering, param.maxnum_candidate] = process_smp( M, param.smpflag, param.ordering, param.findsmp.maxsmpdepth, param.maxnum_candidate, param.v0 );
    [param.stale_norm] =                    process_stale_norm( param.stale_norm, param.dim );
    [param.save_to_disk, param.callback] =  process_save_to_disk( param.save_to_disk, param.callback );
    [param.solver] =                        process_solver( param.solver );
    [param.testoldvertex] =                 process_testoldvertex( param.testoldvertex );
    [param.validateupperbound_norm] =       process_validateupperbound_norm( param.validateupperbound_norm );
    [param.v0, param.v0s] =                 process_v0_v0s( param.v0, param.v0s );
    
    
    
end

function [ graph ] = preprocess_graph( graph );
    if( isempty(graph) );
        graph = {[],[]}; end;
    if( ipa_isgraph(graph) && ismatrix(graph) && ~iscell(graph) );
        component_idx = graph_precomponent( graph );
        graph = {graph, component_idx}; end;
end

function [ param, varargin ] = parse_kone_options( M, param, varargin );  %#ok<INUSD>
    [param.polytopenorm.auxiliary_data, varargin] = parsem( {'aux','center','polytopenorm_auxiliary_data'}, varargin, [] );                         % determines the center vector of the kone. If not given, then it is automatically computed
    [prune_candidates, varargin] =                  parsem( {'prune_candidates','prune_candidate','prune_roots','prune_root','prune'}, varargin );  % whether to prune candidates from the root
    [add_candidates, varargin] =                    parsem( {'add_candidates','add_candidate','add_roots','add_root', 'add'}, varargin );           % whether to prune candidates from the root

    if( prune_candidates );
        assert( isempty(param.delta) || isequal(param.delta, 1) || isequal(param.delta, sym(1)), ...
                'ipa:option', 'Option ''prune'' for case (K) does not work when option ''delta'' is used.' );
        if( isempty(param.autoextravertex) );
            vprintf( 'Option ''autoextravertex'' is set to `0`, since option ''prune'' is given.\n', 'cpr',[.7 .4 .1]', 'imp',[1 param.verbose] );
            param.autoextravertex = false; end;
        if( ~isanyof( @ipa_callback_prune_root, param.callback.generatenewvertex_end ) ...
          );
            param.callback.generatenewvertex_end{end+1} = @ipa_callback_prune_root; end; end

    if( add_candidates );
        assert( isempty(param.delta) || isequal(param.delta, 1) || isequal(param.delta, sym(1)), ...
                'ipa:option', 'Option ''add'' for case (K) does not work when option ''delta'' is used.' );
        if( ~isanyof( @ipa_callback_add_candidate, param.callback.generatenewvertex_end ) ...
          );
            param.callback.generatenewvertex_begin{end+1} = @ipa_callback_add_candidate; end; end
end

%%

function [ admissiblealgorithm ] = process_admissiblealgorithm( admissiblealgorithm );
    if( ~iscell(admissiblealgorithm) );
        admissiblealgorithm = num2cell( admissiblealgorithm ); end;
end

function [ algorithm, minmax ] = process_algorithm( algorithm );
    % algorithms and solvers
    switch algorithm;
        case {'min','lsr','lssr','jssr','jss','sub','l','lower'};
            algorithm = 'l';
            minmax = 'min';
        case {'max','jsr','a','auto'};
            algorithm = 'a'; 
            minmax = 'max';
        case {'K','Kone','KONE','Cone','CONE'};
            algorithm = 'K';
            minmax = 'kone';
        case {'k','kone','cone'};
            algorithm = 'k';
            minmax = 'kone';
        case {'k0','k1','k2','k3','k4','k5','k6','k7'};
            minmax = 'kone';
        case {'k*','K*'};
            error( 'ipa:dual_kone', 'Case (K*) is not implemented.' );
        case {'p','pos','positive'};
            algorithm = 'p';
            minmax = 'max';
        case {'r','m','mink','minkowski'};
            algorithm = 'r';
            minmax = 'max';
        case {'c','complex'};
            algorithm = 'c';
            minmax = 'max';
        case {'ca'};
            algorithm = 'c1';
            minmax = 'max';
        case {'cb'};
            algorithm = 'c2';
            minmax = 'max';
        case {'cc'};
            algorithm = 'c3';
            minmax = 'max';
        case {'cd'};
            algorithm = 'c4';
            minmax = 'max';
        case {'ce'};
            algorithm = 'c5';
            minmax = 'max';
        otherwise; error( 'ipa:opt', 'Wrong value for ''algorithm''.' ); end;
end

function [ autoextravertex ] = process_autoextravertex( autoextravertex, graph, smpflag );  
    if( isempty(autoextravertex) );
        if( ~ipa_isgraph(graph) && ~any(smpflag == ipa_constant.extravertex) );
            autoextravertex = 1;
        else;
            autoextravertex = 0; end; end;
    if( autoextravertex == 1 );
        autoextravertex = 0.01; end;
end

function [ balancing ] = process_balancing( balancing, delta );    % balancing
    if( ischar(balancing) || isstring(balancing) );
        switch balancing;
            case {'rand','random'}; balancing = 'rand';
            case 'none'; balancing = 0;
            case 'auto'; balancing = -1;
            otherwise; fatal_error; end; end;   
    if( isempty(balancing) );
        if( delta < 1 );
            balancing = -1;
        else;
            balancing = 1; end; end;
end

function [ basicjsrtest ] = process_basicjsrtest( basicjsrtest, minmax, mi );
    if( isequal(basicjsrtest, -1) );
        if( all(mi.ternary(:)) );
            basicjsrtest = 1; 
        elseif( strcmp(minmax, 'kone') );
            basicjsrtest = 0; end; end;
end

function [ callback ] = process_callbacks( callback );
    fn = fieldnames( callback );
    for n_ = fn(:)'; n = n_{1};
        if( isa(callback.(n), 'char') || isa(callback.(n), 'string') );
            if( any( exist(callback.(n), 'file') == [2 3 5 6] ) );
                callback.(n) = {str2func( callback.(n) )}; end; end;
            if( isa(callback.(n), 'function_handle') );
                callback.(n) = {callback.(n)}; end;
        assert( iscell(callback.(n)) && all( cellfun( 'isclass', callback.(n), 'function_handle' ) ), 'ipa:opt', 'A callback must be a cell array of function handles.' );
        for i = 1:numel( callback.(n) );
            nin = nargin( callback.(n){i} );
            nout = nargout( callback.(n){i} );
            assert( 0 <= nin && nin <= 3, 'ipa:callback',  'A callback function must have 0, 1, or 2 input arguments.' );
            assert( nout <= 3, 'ipa:callback',  'A callback function must have 0, 1, 2, or 3 output arguments.' ); end; end;
end

function [ complexeigenvector ] = process_complexeigenvector( algorithm, complexeigenvector, delta, minmax );
    if( isempty(complexeigenvector) );
        if( delta < 1 && ...
            ~strcmpi( algorithm(1), 'c' ) ...
          );
            complexeigenvector = 3;
        else;
            switch minmax;
                case {'kone'};      complexeigenvector = 3;
                case {'min','max'}; complexeigenvector = 0;
                otherwise; fatal_error; end; end; end;
end

function [ debug, num_core ] = process_debug( debug, num_core );
    if( isempty(debug) || isequal(debug, 0) );
        debug = {};
    elseif( istruthy(debug, 'failsafe') );
        debug = {'debug'};
    else;
        % do nothing
        end;
    if( isequal(debug, {'debug'}) );
        num_core = 0; end;
end

function [ dryrun, callback ] = process_dryrun( dryrun, callback);
    if( dryrun );
        callback.iter_begin{end+1} = @(bv) setfield( bv, 'log','errorcode', ipa_errorcode.DRYRUN ); end;
end

function [ epseigenplane, callback ] = process_epseigenplane( epseigenplane, callback, minmax );
    if( ~isempty(epseigenplane) );
        assert( isanyof(minmax, {'min','max'}), 'Eigenplane stopping criterion can only be used for JSR and LSR computation.'  ); end;
    if( isempty(epseigenplane) && ~isanyof(minmax, {'min','max'}) );
        epseigenplane = nan;
        return; end;
    if( isempty(epseigenplane) );
        epseigenplane = 1e-12; end;
    if( isfinite(epseigenplane) );
        if( ~isanyof( @ipa_callback_eigenplane_criteria, callback.terminate_check_begin ) && ...
            ~isanyof( @ipa_callback_eigenplane_criteria, callback.generatenewvertex_end ) ...
          );
            callback.generatenewvertex_end{end+1} = @ipa_callback_eigenplane_criteria; end; end;
end

function [ epsspectralradius ] = process_epsspectralradius( epsspectralradius, minmax );
    if( isempty(epsspectralradius) );
        switch minmax;
            case {'min','max'}; epsspectralradius = 1e-10;
            case 'kone';        epsspectralradius = inf;  % inf signals disabled here
            otherwise; fatal_error; end; end;
end

function [ epssym ] = process_epssym( epssym, extravertex, v0, v0s, mi );
    if( isempty(epssym) );
        try;
            err = lasterror();  %#ok<LERR>
            sym(1);  % check whether symbolic computations are possible
        catch me;  %#ok<NASGU>
            lasterror( err );  %#ok<LERR>
            epssym = nan; end; end;
        
    if( isempty(epssym) );
        if( ~isempty( extravertex ) && ~allm(issymstrict( extravertex )) || ...
            ~isempty( v0 ) && ~all(issymstrict( v0 )) || ...
            ~isempty( v0s ) && ~all(issymstrict( v0s )) ...
          );
            epssym = nan;
        elseif( all(mi.int(:)) || all(mi.symstrict(:)) || all(mi.rat(:)) );
            epssym = -1;
        else;
            epssym = nan; end; end;
end

function [ extrapath ] = process_extrapath( extrapath, extrapathflag, dim );
%     if( ~isempty(param.extrapath.mindepth) || ...
%         ~isempty(param.extrapath.maxdepth) || ...
%         ~isempty(param.extrapath.maxnum)   || ...
%         ~isempty(param.extrapath.maxnorm)  || ...
%         ~isempty(param.extrapath.minnum_vertex) || ...
%         extrapath ...
%       );
%         extrapath = 1; end;
    if( extrapathflag );
        if( isempty(extrapath.maxnum) );
            extrapath.maxnum = ceil( 8*extrapathflag^2 ); end;
        if( isempty(extrapath.maxdepth) );
            extrapath.maxdepth = ceil( 100*extrapathflag^2 ); end;
        if( isempty(extrapath.mindepth) );
            extrapath.mindepth = floor( 5/extrapathflag ); end;
        if( isempty(extrapath.minnum_vertex) );
            extrapath.minnum_vertex = floor( dim*10/(extrapathflag^1.5) ); end;
        if( isempty(extrapath.maxnorm) );
            extrapath.maxnorm = max( [1.0005*extrapathflag^2, extrapathflag^2/(dim^2)+1] ); end;
    else;
        extrapath.maxdepth = 0; end;
end

function [ fastnorm ] = process_fastnorm( fastnorm )
    switch fastnorm;
        case {0,'off','none','never'};              fastnorm = 0;
        case {1,'inside','in'};                     fastnorm = 1;
        case {2,'outside','out','inout','outin'};   fastnorm = 2;
        otherwise; error( 'ipa:option', 'Wrong value for option ''fastnorm'' is given.' ); end;
end

function [ findsmp_sym ] = process_findsmp_sym( findsmp_sym, epssym );
    if( isempty(findsmp_sym) );
        if( epssym > 0 );
            findsmp_sym = 2;
        else;
            findsmp_sym = 0; end; end;
end

function [ invariantsubspace ] = process_invariantsubspace( invariantsubspace, num_matrix, minnum_matrix, minmax );
    if( isequal( invariantsubspace, 0 ) || ...
        isequal( num_matrix, 1 ) && isequal( minnum_matrix, 1 ) && strcmp( invariantsubspace, 'auto' ) ...
      );
        invariantsubspace = 'none'; end;

    if( strcmpi(invariantsubspace, 'auto') );
        switch minmax;
            case 'kone'; invariantsubspace = 'none';
            case 'min'; invariantsubspace = 'none';
            case 'max'; invariantsubspace = 'auto';
            otherwise; fatal_error; end; end;
end

function [ leadingeigenvector_sym ] = process_leadingeigenvector_sym( leadingeigenvector_sym, epssym );
    if( isempty(leadingeigenvector_sym) );
        if( epssym > 0 );
            leadingeigenvector_sym = 2;
        else;
            leadingeigenvector_sym = 0; end; end;
end

function [ maxonenorm ] = process_maxonenorm( maxonenorm, dim );
    if( isempty(maxonenorm) );
        %if( isfinite(epssym) );
            maxonenorm = dim*10;
        %else;
        %    maxonenorm = inf; 
        %    end;
        end;
end

function [ naturalselection ] = process_naturalselection( naturalselection, num_core );
    if( isempty(naturalselection) || isequal(naturalselection, true) );
        naturalselection = 64*max( 1, num_core ); end;
    if( naturalselection == 0 );
            naturalselection = inf; end;
end

function [ maxsmpdepth ] = process_maxsmpdepth( maxsmpdepth, minmax );
    if( isempty(maxsmpdepth) );
        switch minmax;
            case {'kone'};      maxsmpdepth = 15;
            case {'min','max'}; maxsmpdepth = 50;
            otherwise; fatal_error; end; end;
end

function [ normalization ] = process_normalization( normalization, minmax );
    if( isempty(normalization) );
        switch minmax;
            case {'max','min'}; normalization = 'default';
            case {'kone'};      normalization = 'kone';
            otherwise; error( 'ipa:options', 'Wrong value for ''normalization'' given.' ); end; end;
end

function [ save_to_disk, callback ] = process_save_to_disk( save_to_disk, callback );
    if( save_to_disk >= 1 );
        if( ~isanyof( @ipa_save_to_disk, callback.worker_end ) );
            callback.worker_end{end+1} = @(bv) ipa_save_to_disk( bv, save_to_disk ); end; end;
        
    if( save_to_disk >= 2 );
        if( ~isanyof( @ipa_save_to_disk, callback.iter_end ) );
            callback.iter_end{end+1} = @(bv) ipa_save_to_disk( bv, save_to_disk ); end; end;
end

function [ stale_norm ] = process_stale_norm( stale_norm, dim );
    if( isempty(stale_norm) );
        stale_norm = max( sqrt(100)/dim, 1 ) * 10; end;
end

function [ smpflag, ordering, maxnum_candidate ] = process_smp( M, smpflag, ordering, maxsmpdepth, maxnum_candidate, v0 );
    if( ~iscell(ordering) && isvector(ordering) && ~ischar(ordering) );
        if( isrow(ordering) );
            ordering = ordering'; end;
        ordering = {ordering}; end;

   if( isequal(ordering, 'all') );
       assert( isempty(v0), 'ipa:options', 'If option <''smp'',''all''> is given, then ''v0'' must not be given.' );
        assert( ~isempty(maxsmpdepth), 'ipa:options', 'If option <''smp'',''all''> is given, then option ''maxsmpdepth'' must be given too.' );
        ordering = all_orderings( numel(M), maxsmpdepth, true );
        expect( numel(ordering) < 200, 'ipa:options', 'Value of ''maxsmpdepth'' is too large, and a lot of leading eigenvectors need to be computed. The algorithm most probably will hang.' );
        maxnum_candidate = inf;
        smpflag = zeros( 1, numel(ordering) );
        end;
    if( isempty(smpflag) );
        smpflag = zeros( size(ordering) );
        for i = 1:numel( ordering );
            smpflag(i) = tif( isempty(ordering{i}), ipa_constant.extravertex, ipa_constant.candidate ); end; end;
        
end

function [ validateupperbound_norm ] = process_validateupperbound_norm( validateupperbound_norm );
    if( isempty(validateupperbound_norm) );
        validateupperbound_norm = -inf; end;
end

function [ solver ] = process_solver( solver );
    persistent gurobiinst;

    if( ~isempty(solver) );
        switch lower(solver);
            case {'a','auto'};      solver = 'a';
            case {'g','gurobi'};    solver = 'g'; 
            case {'m','matlab'};    solver = 'm';
            case {'s','sedumi'};    solver = 's';
            otherwise; error( 'ipa:opt', 'Wrong value for ''solver''.' ); end; end;    

    if( isequal(solver, 'a') && isempty(gurobiinst) );
        model.A = sparse([1 1 0; 0 1 1]);
        model.obj = [1 2 3];
        model.modelsense = 'Max';
        model.rhs = [1 1];
        model.sense = ['<' '<'];
        params.outputflag = 0;
        try;
            res = gurobi( model, params );
            assert( isequal(res.x,[1;0;1]), 'ipa:gurobi', 'Gurobi seems to be installed, but does not work. Fix the Gurobi installation.' );
            gurobiinst = true;
        catch me;  %#ok<NASGU>
            gurobiinst = false; end; 
        solver = tif( gurobiinst, 'g', 'm' ); end;
end

function [ testoldvertex ] = process_testoldvertex( testoldvertex );
    if( isempty(testoldvertex) );
        testoldvertex = 0; end;
    switch testoldvertex;
        case {'never','no','off',0};            testoldvertex = 0;
        case {'default','sometimes','auto',1};  testoldvertex = 1;
        case {2,'estimate'};                    testoldvertex = 2;
        case {'always','on','exact',3};         testoldvertex = 3;
        otherwise; error( 'ipa:option', 'Wrong value for option ''testoldvertex''.' ); end;
end

function [ v0, v0s ] = process_v0_v0s( v0, v0s );
    if( ~iscell(v0) && iscolumn(v0) );
        v0 = {v0}; end;
    if( ~iscell(v0s) && iscolumn(v0s) );
        v0 = {v0s}; end;
end

function [ M, M_sym ] = process_M( M_in, epssym, mi );
    if( ~isfinite(epssym) );
        M_sym = double( M_in );
        M = double( M_in );
    elseif( all(mi.sym) );
        M_sym = M_in;
        M = cellfun( @double, M_in, 'UniformOutput',false );
    else;
        M = cellfun( @double, M_in, 'UniformOutput',false );
        M_sym = cellfun( @sym, M_in, 'UniformOutput',false ); end;
end

%%

function [ var ] = orders_field( var );
    var = orderfields( var );  % bring the elements in type in alphabetic order
    var.log = orderfields( var.log );
    var.param = orderfields( var.param );  % bring the elements in type in alphabetic order
end

function [ num ] = num_cores( multi_threaded );
    persistent num_;

    if( nargin == 1 && ~multi_threaded );
        num = 1;
        return; end;
    
    if( isempty(num_) );
        num_ = feature( 'numcores' ); end;  %#ok<FEATUD>
    if( ~isempty( num_ ) && ~isequal( num_, 0 ) );
        num = num_;
    elseif( parfor_worker_available( 0.1 ) );
        num = num_;
    else;
        fprintf( 2, 'Could not deduce number of available workers. I am running the program single threaded.\n' );
        num = 0; end;
end

function dummy; end  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*NBRAK>
