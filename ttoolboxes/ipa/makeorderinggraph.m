function G = makeorderinggraph(varargin)
% [ G ] = makeorderinggraph( oo, [options] )
% Constructs the graph given a partially ordered set.
%
% Input: 
%   oo      Matrix of orderings. Each column is one ordering OR
%           Cell array of orderings. Each cell consists of one column vector
%
% Options:
%   'value',val             Format of val: Nx2 cell array. First columns: name, second columns: column vector of values/cell array of strings
%   'fontsize',val          Fontsize of the vertex-labels
%   'labelonlyleaf'         Puts labels only on vertices without children
%   'labeldescription'      Puts the description (entries in 'value'{:,1}) 
%   'labelnumber'           Puts the number of the vertices
%   'nolegend'              Does not print a legend
%   'noplot'                No plot output. Just returns graph as object:
%   'verbose',val           Verbose level
%        
% Output:
%   G                       The graph as a matlab graph object
%   plot of the graph
%
% E.g.: oo = [1 0 0;1 1 0;1 1 2;1 2 0;1 2 1;1 2 1;2 0 0;2 1 0;2 1 1;2 2 0].'; 
%       makeorderinggraph( oo );
%       makeorderinggraph( oo, 'removeleaf' );
%       ipa( gallery_matrixset('rand_gauss',3,2,100,'rho'), 'plot','tree' );
%
% Written by: tommsch, 2018 

%               2019-03-30  added options 'labeldescription', 'labelnumber'
%                           cell arrays of text can be passed as vertex text
%               2020-06-21  Removed option 'labeledge'
%                           Added option 'removeleaf'
%                           Added legend and option 'nolegend'
% Changelog:

% XX Add cycles

    oo = varargin{1};  % size of oo changes during the computation
    varargin(1) = [];
    if( isnumeric(oo) ); 
        oo = num2cell(oo,1); end;  % make oo to cell array
    
    [opt.fontsize, varargin] =           parsem( 'fontsize', varargin, 5 );
    [opt.labelonlyleaf, varargin] =      parsem( 'labelonlyleaf', varargin, 0 );
    [opt.labeldescription, varargin] =   parsem( 'labeldescription', varargin );
    [opt.labelnumber, varargin] =        parsem( 'labelnumber', varargin );
    [opt.verbose, varargin] =            parsem( {'verbose','v'}, varargin,1 );    
    [opt.value, varargin] =              parsem( 'value', varargin, {} );  % format: 'name',val
    [opt.noplot, varargin] =             parsem( 'noplot', varargin );
    [opt.nolegend, varargin] =           parsem( 'nolegend', varargin );
    [opt.removeleaf, varargin] =         parsem( {'removeleaf','noleaf'}, varargin );
    
    assert( ~(opt.labelonlyleaf && opt.removeleaf), 'makeorderinggraph:opt', '''labelonlyleaf'' and ''removeleaf'' cannot be used together.' );
    
    parsem( varargin, 'test' );
    
    
    % Make input checks    
    nvalue = size( opt.value, 1 );
    if( nvalue > 0 )
        assert( iscell(opt.value), 'makeorderinggraph:arg', 'Argument of ''value'' must be a Nx2 cell' );
        assert( isequal(size(opt.value,2),2), 'makeorderinggraph:arg', 'Argument of ''value'' must be a Nx2 cell' );

        for i = 1:nvalue
            assert( ischar(opt.value{i,1}), 'makeorderinggraph:arg', 'First entries of value must be a string.' );
            if( isrow(opt.value{i,2}) ); 
                opt.value{i,2} = opt.value{i,2}.'; end;
            assert( isequal(size(opt.value{i,2}),[size(oo,2),1]), 'makeorderinggraph:arg', 'Second entries must be a column vector with as many values as elements in oo.'); end; end;

    % Preprocess
    vprintf( 'Construct Graph with %i vertices. ', numel(oo), 'imp',[1 opt.verbose] );
    
    G = zeros( 3, numel(oo) );  % the nodes (vertices)
    J = max( cellfun(@max, oo) );  % number of different matrices in tree
    
    % if there is no empty ordering, add it
    if( ~isempty(oo) && any(oo{1}) ); 
        oo = horzcat( 1, oo ); 
        oo{1} = [];  % add empty ordering (gets the number zero in the next steps)
        for i = 1:nvalue; 
            opt.value{i,2} = [0; opt.value{i,2}]; end; end;  % add values for empty ordering
    
    noo = numel( oo );
    
    for i = 1:noo;
        if( isrow(oo{i}) ); 
            oo{i} = oo{i}.'; end;  % make to column vector
        oo{i} = [0; removezero( oo{i}, 1 )]; end;  % add the root vertex temporarily
    
    for i = 1:noo;  % construct graph
        [found,idx] = searchincellarray( oo{i}(1:end-1), oo, 1 );
        if( found );
            G(:,i) = [idx(1); i; oo{i}(end)]; end; end;  % search for vertices: Format: node, node, oo-idx
        
    G = removezero( G, 2 );  % remove zeros in between
    
    if( opt.removeleaf );
        val = G(1:2,:);  % identify leafs
        val2 = unique( val(:) );
        count = histc( val(:), val2 );  %#ok<*HISTC>
        val2 = val2(count == 1);
        idxonce = ismember( val(1,:) ,val2 ) | ismember( val(2,:), val2 );  % get indices of leaves
        G(:,idxonce) = [];  % remove leaves
        val = G(1:2,:);  % fix numbering so that we do not see empty leaves
        [~, ~, ic] = unique( val(:) );
        val(:) = ic;
        G(1:2,:) = val; end;
        
    % Pass object to matlab and let matlab do some work
    vprintf( 'Pass object to matlab. ', 'imp',[1 opt.verbose] );
    G = digraph( G(1,:), G(2,:), G(3,:) ); 
    if( isempty(G.Edges) );
        return; end;  
    for i = 1:nvalue;
        G.Nodes.(opt.value{i,1}) = opt.value{i,2}; end;  % Add values to the table
    
    % Plot Graph    
    LINESTYLE = {'-',':','--','-.'};
    vprintf( 'Plot Graph. ', 'imp',[1 opt.verbose] );
    if( ~opt.noplot )
        isholdbefore = true;
        if( ~ishold ); 
            isholdbefore = false;
            cla; 
            hold on; end;
        for i = 1:J
            plot( [nan], 'Color',num2color(i), 'LineStyle',LINESTYLE{mod(i,4)+1} ); end;  %#ok<NBRAK2>  % make empty plots for legend
        h = plot( G, 'Layout', 'layered', 'NodeLabel', [], 'AssignLayers', 'asap', 'Direction', 'r' ); end;
    
    % Add text
    vprintf( 'Add Text. ', 'imp',[1 opt.verbose] );
    vertextext = cell( size(G.Nodes,1), 1 ); 
    for i = 1:length( h.XData );
        vertextext{i} = ''; end;  % make empty text
    for i = 1:length( h.XData );
        for j = 1:nvalue
            val = opt.value{j,2}(i);
            if( iscell(val) && ischar(val{1}) ); 
                val = val{1};  % do nothing
            elseif( isnumeric(val) );
                val = num2str( val.' ); end;
            if( opt.labeldescription );
                val = [opt.value{j,1} ': ' val]; end;  %#ok<AGROW>
            
            vertextext{i} = [vertextext{i} val newline]; end;
        if( opt.labelnumber );
            vertextext{i} = [num2str(i) ': ' newline vertextext{i}]; end; end;
    
    for i = 1:length( h.XData );
        if( opt.labelonlyleaf && any(G.Edges.EndNodes(:,1) == i) );
            continue; end;
        text( h.XData(i)+0.1, h.YData(i), vertextext{i}, 'FONTSIZE',opt.fontsize ); end;

    % Process Edges
    vprintf( 'Process Edges. ', 'imp',[1 opt.verbose] );
    
    
    for i = 1:J;
        idx = G.Edges.Weight == i;
        highlight( h, G.Edges.EndNodes(idx,1), G.Edges.EndNodes(idx,2), 'EdgeColor',num2color(i), 'LineWidth',1.5, 'LineStyle',LINESTYLE{mod(i,4)+1} ); end;

    % Postprocessing
    if( ~isempty(G.Nodes) ); 
        highlight( h, 1, 'NodeColor','k', 'MarkerSize',12 ); end;  % Highlight root
    
    if( ~opt.nolegend );
        legend( string(1:J) ); end;  % add legend
    
    axis off
    vprintf( '\n', 'imp',[1 opt.verbose] );
    
    if( ~opt.noplot );
        if( ~isholdbefore );
            hold off; 
        else;
            hold on; end; end;



end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
