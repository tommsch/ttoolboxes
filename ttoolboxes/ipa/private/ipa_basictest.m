function [ bv ] = ipa_basictest( bv );
% makes basic tests of the input matrices
% those are:
%   no matrix case
%   only one matrix case
%   matrices with inf/nan
%   bounds already fulfilled
%
% Input:
%   T                   cell array of square matrices
% 
% Output:
%   flag                boolean, if this flag is set, this function handled the computation of the JSR
%
% Written by: tommsch, 2020

% XX Add test for zero JSR. Write to Jungers, there are some mistakes in jsr_zeroJsr

    M = bv.param.M;
    if( numel(M) == 0 );  % no matrix case
        bv.JSR = [];
        bv.log.errorcode = ipa_errorcode.INPUTERROR; 
        vprintf( 'Empty input data. JSR is empty.\n', 'imp',[1 bv.param.verbose] ); 
        return; end;
    
    if( numel(M) == 1 && bv.param.minnum_matrix > 1 );  % 1-matrix case
        if( ipa_isgraph(bv.param.graph) && bv.param.graph{1} == 0);
            bv.JSR = nan;
            bv.log.errorcode = ipa_errorcode.NOERROR_TRIVIAL;
            bv.cyclictree.ordering = {};
            bv.cyclictree.smpflag = [];
            %bv.cyclictree.rho_ordering = nan;
            vprintf( 'One matrix is given, but powers of it do not constitute an admissible product (due to the given graph).\n JSR is set to nan by convention.\n', 'imp',[1 bv.param.verbose] ); 
            return;
        else;
            switch bv.param.minmax;
                case {'min','max'}; bv.JSR = rhot( M{1} );
                case {'kone'}; warning( 'ipa:kone', 'The returned value ist most likely wrong.' );
                otherwise; fatal_error; end;
            bv.log.errorcode = ipa_errorcode.NOERROR_TRIVIAL;
            bv.cyclictree.ordering = {[1]};
            bv.cyclictree.smpflag = [ipa_constant.candidate];
            %bv.cyclictree.rho_ordering = 1;
            vprintf( 'JSR = %15.12g \n', bv.JSR, 'imp',[1 bv.param.verbose] ); 
            return; end; end;
    
    if( all(cellfun(@(x) ~all(isfinite(x(:))), M)) );  % nan/inf - matrices
        bv.JSR = NaN;
        bv.log.errorcode = ipa_errorcode.INPUTERROR;
        bv.param.matrixtype.finite = false;
        vprintf( 'Matrices contain Infs/NaNs.\n', 'imp',[1 bv.param.verbose] ); 
        return; end;
    
    switch bv.param.minmax;
        case {'max','min'};
            if( max(bv.JSR) <= bv.param.validateupperbound );  % bounds already fulfilled
                % bv.JSR is already set
                bv.log.errorcode = ipa_errorcode.VALIDATEUPPERBOUND;
                return; end;
        case 'kone';
            % do nothing
        otherwise;
            fatal_error; end;

    switch bv.param.minmax;
        case 'max';  [ bv ] = ipa_basictest_max( bv, M );
        case 'min';  [ bv ] = ipa_basictest_min( bv, M );
        case 'kone'; [ bv ] = ipa_basictest_kone( bv, M );
        otherwise; fatal_error; end;
end

function [ bv ] = ipa_basictest_max( bv, M );

    if( ~bv.param.basicjsrtest || ipa_isgraph(bv.param.graph) );  % basic theoretic jsr tests
        return; end;
    
    if( bv.param.dim == 1 );  % 1-d case
        [bv.JSR, val] = max( abs(cell2mat(M)) ); 
        bv.log.errorcode = ipa_errorcode.NOERROR_TRIVIAL; 
        bv.cyclictree.ordering = {[val]};
        bv.cyclictree.smpflag = [ipa_constant.candidate];
        %bv.cyclictree.rho_ordering = 1;
        vprintf( 'JSR = %15.12g \n', bv.JSR, 'imp',[1 bv.param.verbose] ); 
        return; end;

    if( ~isempty(bv.param.M_sym) );
        MT = bv.param.M_sym;
    else;
        MT = bv.param.M; end;
    [r, n1, n2, ni, nf] = deal( zeros(size(MT)) );
    for i = 1:numel( MT );
        r(i)  = rho( MT{i} );
        n1(i) = norm( MT{i}, 1 );
        n2(i) = norm( MT{i}, 2 );
        ni(i) = norm( MT{i}, inf );
        nf(i) = norm( MT{i}, 'fro' ); end;

    r = max( r );
    n1 = max( n1 );
    n2 = max( n2 );
    ni = max( ni );
    nf = max( nf );
    n = min( [n1 n2 ni nf] );
    nrmstr = '';
    nrmstr = tif( n1 == r, '1', nrmstr );
    nrmstr = tif( n2 == r, '2', nrmstr );
    nrmstr = tif( ni == r, 'inf', nrmstr );
    nrmstr = tif( nf == r, 'fro', nrmstr );
    if( n == r );
        vprintf( ['Exact value found using basic tests (maximum of %s-norm of matrices coincides with spectral radius).\n', ....
                  'JSR = %15.12g \n'], ...
                   nrmstr, min(bv.JSR), ...
                   'imp',[1 bv.param.verbose] );

        bv.JSR = r;
        bv.log.errorcode = ipa_errorcode.NOERROR_TRIVIAL;
        return; end;
end

function [ bv, flag ] = ipa_basictest_min( bv, M );  %#ok<INUSD>
    flag = false;
    % no tests known yet
end


function [ bv, flag ] = ipa_basictest_kone( bv, M );  %#ok<INUSD>
    flag = false;
    % no tests known yet
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*NBRAK,*NBRAK1,*NBRAK2>
