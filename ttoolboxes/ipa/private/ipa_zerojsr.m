function [ errorcode ] = ipa_zerojsr( ct, param );

    errorcode = ipa_errorcode.UNSETERROR;

    switch param.algorithm(1);
        case {'k','K'}; return;
        case {'a','p','r','c','l'};  % do nothing
        otherwise; fatal_error; end;

    if( (ct.lambda == 0 || ~isfinite( ct.lambda )) && ...
        param.norestart < 2 ...
      ); 
        % check if we have zero-JSR
        if( param.epssym >= 0 );
            val = ct.M_sym;
        else;
            val = ct.Mt; end;
        liftedm = liftproductt( val, param.dim, true );  % it does not matter whether use scaled or unscaled matrices
        liftedm = [liftedm{:}];
        if( all(isAlways(liftedm(:) == 0)) );
            errorcode = ipa_errorcode.ZEROJSR;
        else;
            errorcode = ipa_errorcode.NONZEROJSR; end; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
