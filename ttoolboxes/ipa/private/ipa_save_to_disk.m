function [ bv ] = ipa_save_to_disk( bv, level );
% blockvar = ipa_saveoutput( blockvar )
% Saves most of the output of the running algorithm in files
% This function belongs to ipa!
%%
% Written by tommsch, 2018

    if( level >= 1 )
        vprintf('Write to file.\n', 'imp',[3 bv.param.verbose] );
        try;
            if( bv.param.plot ); 
                print( ['plot ipa ' datestr(datetime)],'-dpng' ); end;
        catch me;  %#ok<NASGU>
            end;
            
        try
            if( level >= 3)
                vprintf( 'Write ''type'' to file.\n', 'imp',[3 bv.param.verbose] );
                save( ['type ipa ' datestr(datetime)], 'bv' ); end;
        catch me;  %#ok<NASGU>
            end; end;

    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
