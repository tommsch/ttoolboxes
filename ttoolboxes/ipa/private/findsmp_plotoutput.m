function findsmp_plotoutput( NN, RR, Nt_mm, Rt_mm, nadm, opt );  %#ok<INUSD>
% findsmp_plotoutput( NN, RR, opt );
%   Plots some output


if( isequal(opt.plot, 0) || ...
    isequal(opt.plot, 'none') ...
  ); 
    return; end;  % fast return

try;
    plottype = lower( opt.plot );

    switch plottype;
        case {1,'all'};
            plot_all( NN, RR, Nt_mm, Rt_mm, opt );
        case {2,'minmax','mm','min_max'}
            plot_mm( RR, Nt_mm, Rt_mm, opt );
        case 'none';
            % do nothing
        otherwise
            fprintf( 'Wrong argument for ''plot''.\n' ); end;
        
    drawnow;
catch me;
    vprintf( '''\nfindsmp_plotoutput'': Error\n' ); 
    disp( me ); end;

end

%% plot functions
%%%%%%%%%%%%%%%%%%%%%%%%%

function plot_all( NN, RR, opt );

    idx = 1:numel( NN );
    if( ~isempty(opt.graph) );
        subplot( 2, 2, [1 2] ); end;

    plotm( [idx;NN], 'k-', 'semilogx' );
    plotm( [idx;limsup(NN)], 'k--', 'semilogx', 'hold',2 );
%     plotm( [idx(nadm);NN(nadm)], 'k.', 'semilogx', 'hold',2 );

    plotm( [idx;RR], 'r-', 'semilogx', 'hold',2 );
    plotm( [idx;cummax(RR)], 'r--', 'semilogx', 'hold',2 );
%     plotm( [idx(nadm);cummax(RR(nadm))], 'semilogx', 'r-', 'hold',2 );
%     plotm( [idx(nadm);RR(nadm)], 'r.', 'semilogx', 'hold',2 );
    title( 'Rho: red, Norms: black' ); 
    
    if( ~isempty(opt.graph) );
        if( issymmetric(opt.graph) );
            graph_h = @graph;
        else;
            graph_h = @digraph; end;
        subplot( 2, 2, 3 );
        plot( graph_h(opt.graph) );
        title( 'Graph' )

        subplot( 2, 2, 4 );
        plot( graph_h(~opt.graph) );
        title( 'Negated graph' ); end;
end

function plot_mm( RR, Nt_mm, Rt_mm, opt );  %#ok<INUSD>
    plotm_args = {'semilogy'};
    r = max( RR );
    N = size( Nt_mm, 2 );
    idx = 1:N;
    Nt_mm = Nt_mm/r;
    Rt_mm = Rt_mm/r;
    
    Nt_mn = Nt_mm(1,:);
    Nt_mx = Nt_mm(2,:);
    Nt_mn_bd = cummin( Nt_mn);
    Nt_mx_bd = limsup( Nt_mx );
    Nt_mn_s = Nt_mn.^idx;  %#ok<NASGU>
    Nt_mx_s = Nt_mx.^idx;  %#ok<NASGU>
    Nt_mn_bd_s = Nt_mn_bd.^idx;  %#ok<NASGU>
    Nt_mx_bd_s = Nt_mx_bd.^idx;  %#ok<NASGU>
    
    
    Rt_mn = Rt_mm(1,:);
    Rt_mx = Rt_mm(2,:);
    Rt_mn_bd = liminf( Rt_mn );
    Rt_mx_bd = cummax( Rt_mx );
    Rt_mn_s = Rt_mn.^idx;  %#ok<NASGU>
    Rt_mx_s = Rt_mx.^idx;  %#ok<NASGU>
    Rt_mn_bd_s = Rt_mn_bd.^idx;  %#ok<NASGU>
    Rt_mx_bd_s = Rt_mx_bd.^idx;  %#ok<NASGU>
    
    cla reset;
    plotm( [idx;Nt_mn], 'k-', plotm_args{:} );
    plotm( [idx;Rt_mx], 'r-', plotm_args{:}, 'hold','on' );  % plot this first, due to order of legend entries
    
    
    plotm( [idx;Nt_mx], 'k-', plotm_args{:}, 'hold','on' );
    plotm( [idx;Nt_mn_bd], 'k.--', plotm_args{:}, 'hold','on' );
    plotm( [idx;Nt_mx_bd], 'k.--', plotm_args{:}, 'hold','on' );
    
    plotm( [idx;Rt_mn], 'r-', plotm_args{:}, 'hold','on' );
    plotm( [idx;Rt_mn_bd], 'r.--', plotm_args{:}, 'hold','on' );
    plotm( [idx;Rt_mx_bd], 'r.--', plotm_args{:}, 'hold','on' );
    
    legend( {'rel. min/max of norms', 'rel. min/max of rhos'} );
    
    idx_check = ceil( 0.05*N );
    ymin = min( Rt_mn(idx_check:end) );
    ymax = max( Nt_mx(idx_check:end) );
    ymin_ = ymin - (ymax-ymin)/10;
    ymax_ = ymax + (ymax-ymin)/10;
    ymin = ymin_;
    ymax = ymax_;
    axis( [idx_check/2-1 N+1 ymin ymax ] );
    drawnow;
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

