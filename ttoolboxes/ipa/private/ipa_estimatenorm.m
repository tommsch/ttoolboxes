function [ bv, ct_est, str ] = ipa_estimatenorm( bv );
% ct_est    number of iterations used to compute the estimation
%
% This function belongs to ipa
% Written by: tommsch, 2018

    if( bv.param.fastnorm < 1 );
        str = '';
        ct_est = 0;
        return; end;  % do not estimate if option is set

    [selectoldvertexflag, bv] = reestimate_test( bv );  % check if we shall test old vertices
    [VV, VV_Pidx] = ipa_getpolytope( bv );  % get polytope for which we compute the norms    

    counter.in = 0;  % number of points which are prooven to be inside
    counter.out = 0;  % number of points which are prooven to be outside
    counter.estimated = 0;  % number of estimated vertices in total
    maxlevel = size( bv.cyclictree.num_added{1}, 2 );

    if( bv.param.verbose >= 1 && selectoldvertexflag);
        fprintf( '[Reestimate norms] ' );
        clean_fprintf = onCleanup( @() fprintf( repmat( '\b', [1 19] ) ) ); end;

    for i = 1:numel( bv.cyclictree.smpflag );  % iterate through all trees
        [idx_all] = select_vertices( bv, i, selectoldvertexflag, maxlevel );        
        [norm_est, counter, bv] = estimate_norm( VV, VV_Pidx, bv, i, idx_all, counter );
        [bv, counter] = save_estimate( bv, i, idx_all, norm_est, counter );
        end;
        
    if( counter.in > 0 || counter.out > 0 );
        str = [repmat( '_', [1 counter.in] ) repmat( 'O', [1 counter.out] )];
    else;
        str = ''; end;
    if( bv.param.verbose >= 2 );
        str = ['(fast) ' str]; end;
    ct_est = counter.in + counter.out;
end

function [ selectoldvertexflag, bv ] = reestimate_test( bv );
    if( bv.param.testoldvertex == 0 );  % we do not test old vertices
        selectoldvertexflag = 0;
    elseif( bv.param.testoldvertex == 1 );  % if number of vertices increases much, we test old vertices
        REESTIMATE = 2;  % by what factor the number of vertices of the polytope must grow, until we reestimate all vertices
        MINNUM = 300;  % minimum number of old vertices to test. If set too low, in the first runs old vertices are tested always
        ic = log2( sum(cumsum(vertcat(bv.cyclictree.num_added{:}), 2), 1) )/log2( REESTIMATE );
        [~, ~, ic] = unique( ceil(ic) );
        ic = diff( ic' );
        ic = ic(end);
        if( ic && ...
            sum( [bv.cyclictree.num_added{:}] ) > MINNUM ...
          ); 
            selectoldvertexflag = 1; 
        else; 
            selectoldvertexflag = 0; end;
    elseif( bv.param.testoldvertex >= 2 && bv.param.testoldvertex <= 3 );  % we always test old vertices
        selectoldvertexflag = 1;
    else;
        error( 'ipa:option', 'Wrong value for ''testoldvertex''.' ); end;
end

function [ idx_all ] = select_vertices( bv, i, selectoldvertexflag, maxlevel );
    % select vertices which we want to estimate
    idx_all = bv.cyclictree.added_in{i} == maxlevel;  % select all new vertices
    if( selectoldvertexflag );  % also select old vertices if there are enough new vertices in the polytope
        OFFSET = 2;  % Heuristic value
        % only test vertices which are added long ago (OFFSET many iterations before)
        % only test vertices which are not inside (this includes untested vertices)
        normval = bv.cyclictree.norm{i};
        idx = bv.cyclictree.added_in{i} < bv.cyclictree.added_in{i}(end) - OFFSET & ...
              normval(2,:) >= 1 - bv.param.epsequal;
        idx_all = idx_all | idx; end;
end

function [ norm_est, counter, bv ] = estimate_norm( VV, VV_Pidx, bv, i, idx_all, counter );
    pts = bv.cyclictree.VV{i}(:,idx_all);  % vertices to test
    counter.estimated = counter.estimated + size( pts, 2 );  % increase counter
    pts_Pidx = [bv.cyclictree.component_idx{:}];
    if( ~isempty(pts_Pidx) );
        pts_Pidx = pts_Pidx(idx_all);
        norm_est = zeros( 3, numel(pts_Pidx) );
        % norm_exact = zeros( 3, numel(pts_Pidx) );  % XX DEBUG - CAN BE REMOVED SAFELY
        for p = bv.cyclictree.comp;
            norm_est(:,pts_Pidx == p) = polytopenorm( pts(:,pts_Pidx == p), VV(:,VV_Pidx == p), bv.cyclictree.polytopenorm.auxiliary_data{:}, bv.cyclictree.algorithm, 'estimate',1, 'v',-1 ); 
            % norm_exact(:,pts_Pidx == p) = polytopenorm( pts(:,pts_Pidx == p), VV(:,VV_Pidx == p), bv.cyclictree.polytopenorm.auxiliary_data{:}, bv.cyclictree.algorithm, 'estimate',0, 'v',-1 );  % XX DEBUG - CAN BE REMOVED SAFELY
            % II = intersectinterval( norm_est(2:3,:)', norm_exact(2:3,:)', 'nan' );
            % EXPECT_THAT( II, tt.Not(tt.Any_(tt.IsNan)) );
        end;
    else;
        norm_est = polytopenorm( pts, VV, bv.cyclictree.polytopenorm.auxiliary_data{:}, bv.cyclictree.algorithm, 'estimate',1, 'v',-1 ); end;
end

function [ bv, counter ] = save_estimate( bv, i, idx_all, norm_est, counter );
    bv.cyclictree.norm_est{i}(idx_all) = norm_est(1,:);
    idx_all_lin = find( idx_all );

    if( bv.param.fastnorm >= 1 );  % save norms for points prooven to be inside
        idx_norm = norm_est(2,:) < 1;
        idx = idx_all_lin(idx_norm);
        bv.cyclictree.norm{i}(:,idx) = norm_est(:,idx_norm);  % ipa_constant.INSIDE;  %% XX DEBUG
        counter.in = counter.in + length( idx ); end;

    if( bv.param.fastnorm >= 2 );  % save norms for points prooven to be outside
        idx_norm = norm_est(3,:) > 1;
        idx = idx_all_lin(idx_norm);
        bv.cyclictree.norm{i}(:,idx) = norm_est(:,idx_norm);  % min( bv.cyclictree.norm{i}(:,idx), ipa_constant.OUTSIDE );
        counter.out = counter.out + length( idx ); end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
