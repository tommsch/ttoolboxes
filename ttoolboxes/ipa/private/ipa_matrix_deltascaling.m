function [ ct ] = ipa_matrix_deltascaling( ct, bv );
% Pre-matrix multiplication
% A modification of the matrix here, must preserve the leading eigenvector of the smp

% Changelog:    tommsch,    2024-10-28,     Bugfix, Matrix scaling was wrong for case (K) and 'delta' given

    if( ~strcmpi(bv.cyclictree.algorithm(1), 'k') && ...
        (isequal( bv.param.delta, -1 ) || strcmpi( bv.param.delta, 'auto' )) && ...
        bv.param.validateupperbound > 0 ...
      );
        bv.param.delta = min( bv.lambda/bv.param.validateupperbound*(1 + bv.param.epsequal), 1 ); end;  % XX I think this computation should go to option-parsing where all default values are computed

    ct.delta = 1;
    if( bv.param.epssym >= 0 );
        ct.delta_sym = sym( 1 ); end;
    
    if( isequal(bv.param.delta, 1 ) );
        return; end;

    switch bv.param.minmax;
        case {'min','max'};
            ct.delta = bv.param.delta;

        case 'kone';
            ct.delta = Et_kone( bv.param.delta, bv.cyclictree.polytopenorm.auxiliary_data{1} );
            
        otherwise; fatal_error; end;


    if( bv.param.epssym >= 0 );
        ct.delta_sym = sym( ct.delta );
        ct.delta = double( ct.delta ); end;

    for j = 1:numel( ct.Mt );
        if( bv.param.epssym >= 0 );
            ct.Mt_sym{j} = ct.delta_sym * ct.Mt_sym{j}; end;
        ct.Mt{j} = ct.delta*ct.Mt{j}; end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
