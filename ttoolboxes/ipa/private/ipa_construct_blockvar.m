function [ bv, var ] = ipa_construct_blockvar( MM, var );
    [bv{1:var.param.numblock}] = deal( struct );
    for m = 1:var.param.numblock;
        bv{m}.param = var.param;
        bv{m}.log.num_restart = 0;  % counts how often the algorithm restarts
        bv{m}.log.num_normerr = 0;  % counts how many norm computations yielded an error
        if( var.param.preprocess );      % pre-process
            numel_before = numel( MM{m} );
            MM{m} = ipa_preprocess_M( MM{m}, bv{m} );
            numel_after = numel( MM{m} );
            if( numel_before ~= numel_after );
                var.log.error = vprintf( 'Duplicate matrices or zero matrices detected in subspace %i. Numbering of matrices in the output will not be the same as for the input matrices.\n  Number of input matrices: %i\n  Number of output matrices: %i\n', m, numel_before, numel_after, 'cpr','err', 'str',var.log.error, 'imp',[0 var.param.verbose] ); end; end;
        bv{m}.param.M_sym = MM{m};
        MM{m} = double( MM{m} );
        expect( allm( isAlways(isreal([MM{m}{:}])) ), 'invariantsubspace() produced complex matrices, starting from real ones. You may want to add the option `''invariantsubspace'',''none''` to prevent this.' );
        bv{m}.param.M = MM{m}; 
        bv{m}.param.dim = size( MM{m}{1}, 1 );
        switch bv{m}.param.minmax;
            case {'min','max','jsr','lsr','ljsr','jssr'};
                bv{m}.JSR = estimatejsr( MM{m}, bv{m}.param.graph{1}, bv{m}.param.minmax );
            case {'kone'};
                bv{m}.JSR = [0 inf]; 
            otherwise;
                fatal_error; end; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>