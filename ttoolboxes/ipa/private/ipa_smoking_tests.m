function [ JSR, var ] = ipa_smoking_tests( M, var );

    var.JSR = [0 inf];
    if( numel(M) == 0 && ...
        var.param.norestart < 2 ...
      );
        var.JSR = 1;
         vprintf( 'Empty set given. JSR is set to 1 by convention.\n', 'imp',[1 var.param.verbose] );
        var.log.errorcode = ipa_errorcode.NOERROR_TRIVIAL;

    elseif( isequal(size(M{1}, 1), 0) && ...
            var.param.norestart < 2 ...
          );
        var.JSR = inf;
         vprintf( 'Empty matrices given. JSR is set to inf by convention.\n', 'imp',[1 var.param.verbose] );
        var.log.errorcode = ipa_errorcode.NOERROR_TRIVIAL;

    elseif( ~all(matrixinfo(M, 'finite')) && ...
            var.param.norestart < 2 ...
          );
        var.JSR = [NaN NaN];
         vprintf( 'Matrices contain NaNs or Infs. JSR is set to [NaN NaN] by convention.\n', 'imp',[1 var.param.verbose] );
        var.log.errorcode = ipa_errorcode.INPUTERROR;
        
        end;
    JSR = var.JSR;
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
