function [ cyclictree, errorcode, str ] = ipa_balance( cyclictree, param );
    str = '';
    balancingalpha = ipa_constant.UNSET_BALANCING_ALPHA;  % XX should get replaced by named constant
    errorcode = ipa_errorcode.UNSETERROR;
    if( false );

    elseif( isequal(param.minmax, 'kone') );
         str = vprintf( 'No balancing because case K is chosen.\n', 'imp',[2 param.verbose], 'str',str );
         balancingvector = ones( size(cyclictree.ordering, 2), 1 );
        
    % balancing vector is user provided
    elseif( ~ischar(param.balancing) && ~isempty(param.balancing) && all(isfinite(param.balancing)) && ~isscalar(param.balancing) );
        num_o = numel( cyclictree.smpflag );
        assert( numel(param.balancing) == num_o, 'ipa:balancingvectorlength', 'Length of balancing vector wrong. Given length: %i, Needed length: %i', numel(param.balancing), num_o );
        balancingvector = param.balancing;
        
    % balancing disabled due to options
    elseif( isequal(param.balancing, 0) || isequal(param.balancing, 'off') || any(~isfinite(param.balancing)) );    
        str = vprintf( 'No balancing due to user input/default options.\n', 'imp',[2 param.verbose], 'str',str );
        balancingvector = ones( size(cyclictree.ordering, 2), 1 );
        
    % random balancing
    elseif( isequal(param.balancing,'rand') );
        [balancingvector, str] = random_balancing( cyclictree );
    
    % no balancing necessary
    elseif( numel( cyclictree.smpflag ) == 1 );
        balancingvector = ones( size(cyclictree.ordering, 2), 1 );
        
    % no balancing possible
    elseif( isempty([cyclictree.v0s{:}]) || anym(isnan([cyclictree.v0s{:}])) );
        str = vprintf( 'No balancing due to coplanar eigenplane/eigenvector.\n', 'imp',[1 param.verbose], 'str',str );
        balancingvector = ones( size(cyclictree.ordering, 2), 1 );
        
    % no balancing for case 'l' yet
    elseif( strcmp(param.algorithm(1), 'l') );
        str = vprintf( 'Balancing for case ''l'' is not implemented yet.\n', 'imp',[1 param.verbose], 'str',str );
        balancingvector = ones( size(cyclictree.ordering, 2), 1 );

    % no balancing for case 'k' yet
    elseif( strcmp(param.algorithm(1), 'k') );
        str = vprintf( 'Balancing for case ''k'' is not implemented yet.\n', 'imp',[1 param.verbose], 'str',str );
        balancingvector = ones( size(cyclictree.ordering, 2), 1 );
    
    % graph given, currently no balancing
    elseif( ipa_isgraph( param.graph ) );
        str = vprintf( 'Balancing not possible, since a graph is given. I use some standard values for balancing.\n', 'imp',[1 param.verbose], 'str',str );
        [balancingvector, balancingalpha, errorcode, str] = default_balancing_factors( cyclictree, param, str );
        
    elseif( ~all( any( [cyclictree.v0s{cyclictree.smpflag == ipa_constant.candidate | cyclictree.smpflag == ipa_constant.nearlycandidate}], 1 ) ) );
        str = vprintf( 'Balancing not possible, since some dual eigenvectors could not be computed. I use some standard values for balancing.\n', 'imp',[1 param.verbose], 'str',str );
        [balancingvector, balancingalpha, str] = default_balancing_factors( cyclictree, param, str );
        
    % default    
    else;
        [balancingvector, balancingalpha, errorcode, str] = compute_balancing_factors( cyclictree, param );
        if( errorcode );
            return; end; end;
    
    [cyclictree, str_scale] = scale_v0( balancingvector, cyclictree, param );
    str = [str str_scale];
    cyclictree.balancingvector = balancingvector;
    cyclictree.balancingalpha = balancingalpha;
end

%%

function [ balancingvector, str ] = random_balancing( ct );
    [valn, vald] = rat( rand( size(ct.ordering, 2), 1 ) );
    balancingvector = valn./vald;
    if( anym( ~isreal([ct.v0{:}]) ) );
        [valn, vald] = rat( rand( size(ct.ordering, 2), 1 ) );
        balancingvector = balancingvector + 1i*valn./vald; end;
    str = vprintf( 'Random balancing with vector: %v\n',  balancingvector, 'imp',[1 param.verbose] );
end

function [ balancingvector, balancingalpha, str ] = default_balancing_factors( ct, param, str );  %#ok<INUSD>
    balancingvector = zeros( size(ct.smpflag) );
    balancingvector(ct.smpflag == ipa_constant.candidate) = 1;
    balancingvector(ct.smpflag == ipa_constant.nearlycandidate) = .5;
    balancingvector(ct.smpflag == ipa_constant.extravertex) = .01;
    balancingvector(ct.smpflag == ipa_constant.realvertex) = 1;
    balancingalpha = ipa_constant.UNSET_BALANCING_ALPHA;
end

function [ balancingvector, balancingalpha, errorcode, str ] = compute_balancing_factors( ct, param );
    errorcode = ipa_errorcode.UNSETERROR;
    str = '';
    str = vprintf( 'Balance trees. ', 'imp',[1 param.verbose], 'str',str );
    [balancingvector, balancingalpha, str_bv] = ipa_balancingvector( ct, param );
    str = [str newline str_bv];
    if( isequal(param.balancing, -2) );
        maxval = max( balancingvector );
        balancingvector = balancingvector./maxval;
        num_c = nnz( ct.smpflag == ipa_constant.candidate );
        num_r = nnz( ct.smpflag == ipa_constant.realvertex );
        balancingvector(1:(num_c + num_r)) = 1; 
        balancingalpha = ipa_constant.UNSET_BALANCING_ALPHA; end;
    if( isempty( balancingvector ) || ...
        balancingalpha < 1 || ...
        balancingalpha <= 1 && nnz( ct.smpflag == ipa_constant.candidate ) > 1 && ~(ct.epssym  >= 0) || ...
        balancingalpha == inf ...
      );
        str = vprintf( 'No balancing vector found.\n', 'imp',[1 param.verbose], 'str',str );
        balancingvector = ones( size(ct.ordering, 2), 1 );

        if( ~isequal(param.balancing, -1) && ...
            param.norestart < 1 ...
          );
            errorcode = ipa_errorcode.NOBALANCINGVECTORFOUND;
            return; end;
    else;
        val = tif( balancingalpha < 1+5e-12, ' (suboptimal) ', ' ' );
        if( ct.epssym  >= 0 );
            [n, d] = rat( balancingvector );
            balancingvector = sym(n)./sym(d); end;
        str = vprintf( ['Balancing vector' val 'found: %v'], balancingvector, 'imp',[1 param.verbose], 'str',str ); end;
    str = vprintf( '\n\n==================================\n\n', 'imp',[3 param.verbose], 'str',str, 'cpr',[0.1 0.5 .2] );
    str = vprintf( '\n', 'imp',[1 param.verbose], 'str',str );
end

function [ ct, str ] = scale_v0( balancingvector, ct, param );
    str = '';
    i_max = min( numel(ct.smpflag), numel(balancingvector) );
    for i = 1:i_max;
        ct.v0{i} = ct.v0{i} * double( balancingvector(i) );  % balance v0 and v0s.
        if( param.epssym  >= 0 );
            ct.v0_sym{i} = ct.v0_sym{i} * balancingvector(i); end;
        ct.v0s{i} = ct.v0s{i} / double( balancingvector(i) );
        str = vprintf( 'v0(%i):   \t%r\n',  i, ct.v0{i}, 'imp',[3 param.verbose], 'str',str, 'sze',[param.showquantity param.dim] ); end;
    for i = 1:numel( ct.smpflag );
        if( param.verbose >= 4 || any(ct.v0s{i}) );
            str = vprintf( 'v0s(%i):  \t%r\n', i, ct.v0s{i}, 'imp',[3 param.verbose], 'str',str, 'sze',[param.showquantity param.dim] ); end; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
