function [ ba, bc ] = complexfunct_d_cut_vertex( a, b, c, verbose );
% Computes two points whose connecting line is tangent to the unit ball
% Example: complexfunct_d_cut_vertex( [-1 1], [1 1], [1 -1], 2 )

    narginchk( 3, 4 );
    if( nargin <= 3 || isempty(verbose) );
        verbose = 1; end;

    assert( isvector(a) && isvector(b) && isvector(c), 'complexfunct_d_cut_vertex:input',           'Point ''a'', ''b'' or''c'' is not given as a vector.' );
    expect( min([norm(a) norm(b) norm(c)]) >= 1,       'complexfunct_d_cut_vertex:position:inside', 'Point ''a'', ''b'' or''c'' is strictly inside of unit circle (but should not be). Norm = 1 - %g', 1 - min([norm(a) norm(b) norm(c)]) );
    assert( isequal(numel(a), numel(b), numel(c), 2),  'complexfunct_d_cut_vertex:dimension',       'Dimension of input variables is wrong. Dimension must be 2.' );

    a = tif( isrow(a), a.', a );
    b = tif( isrow(b), b.', b );
    c = tif( isrow(c), c.', c );
    assert( (b(1)*a(2)-b(2)*a(1))*(b(1)*c(2)-b(2)*c(1)) <= 0, 'complexfunct_d_cut_vertex:position:side', 'Points ''a'' and ''b'' are on the same side w.r.t. point ''b'' (but should be on opposite sides.' );


    % va: vector from a to b
    va = b - a;
    % vc: vector from c to b
    vc = b - c;

    % bh: point on the intersection AC - 0B
    bh = [(b(1)*(a(2)*c(1) - a(1)*c(2)))/(a(2)*b(1) - a(1)*b(2) + b(2)*c(1) - b(1)*c(2)); (b(2)*(a(2)*c(1) - a(1)*c(2)))/(a(2)*b(1) - a(1)*b(2) + b(2)*c(1) - b(1)*c(2))];
    assert( norm(bh) < norm(b), 'complexfunct_d_cut_vertex:position:nonconvex', 'Points do not generate a convex shape' );

    % abn: point nearest to zero at line AB
    abn = [(va(2)*(a(1)*b(2) - a(2)*b(1)))/(va(1)^2 + va(2)^2)
                 (va(1)*(a(2)*b(1) - a(1)*b(2)))/(va(1)^2 + va(2)^2)];
    % cbn: point nearest to zero at line CB
    cbn = [(vc(2)*(c(1)*b(2) - c(2)*b(1)))/(vc(1)^2 + vc(2)^2)
                 (vc(1)*(c(2)*b(1) - c(1)*b(2)))/(vc(1)^2 + vc(2)^2)];

    expect( min(norm(abn), norm(cbn)) >= 1-4*eps, 'complexfunct_d_cut_vertex:position:intersect', 'Line through ''a-b'' or ''b-c'' intersects the unit circle (but should not). Norm = 1 - %g', 1 - min(norm(abn),norm(cbn)) );

    % bt: point b, normalized to norm 1
    bt = b/norm( b );
    % nb: normal vector to 0b
    nb = [bt(2); -bt(1)];
    % ba: new point replacing b near to a
    ba = [(a(2)*va(1)*nb(1) - (bt(2)*va(1) + a(1)*va(2))*nb(1) + bt(1)*va(1)*nb(2))/(va(1)*nb(2) - va(2)*nb(1)); (a(2)*va(1)*nb(2) - bt(2)*va(2)*nb(1) + (bt(1) - a(1))*va(2)*nb(2))/(va(1)*nb(2) - va(2)*nb(1))];
    % bc: new point replacing b near to c
    bc = [(c(2)*vc(1)*nb(1) - (bt(2)*vc(1) + c(1)*vc(2))*nb(1) + bt(1)*vc(1)*nb(2))/(vc(1)*nb(2) - vc(2)*nb(1)); (c(2)*vc(1)*nb(2) - bt(2)*vc(2)*nb(1) + (bt(1) - c(1))*vc(2)*nb(2))/(vc(1)*nb(2) - vc(2)*nb(1))];

    assert( (b(1)*a(2)-b(2)*a(1))*(b(1)*c(2)-b(2)*c(1)) <= 0, 'complexfunct_d_cut_vertex:position:sameside', 'Points ''a'' and ''b'' are on the same side w.r.t. point ''b'' (but should be on opposite sides.' );


    if( verbose >= 2 );
        if( ~ishold );
            clf; end;
        plotm( [1;1i], 'funct','c', 'hold',2 ); 
        plotm( [a b], 'ro-', 'hold',2 );
        plotm( [b c], 'go-', 'hold',2 );
        %plotm( [[0;0] bt b], 'k-', 'hold',2 );
        plotm( [a ba bc c], 'b.--', 'hold',2 );
        axis equal; end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
