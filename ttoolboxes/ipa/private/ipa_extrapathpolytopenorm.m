function [ normval, iter_e ] = ipa_extrapathpolytopenorm( vertextotest, normval, selectidx, VV, bv )

    iter_e = 0;

    idx = normval(2,:) <= bv.param.extrapath.maxnorm & normval(2,:) > 1;
    selectidx_e = selectidx;
    selectidx_e(selectidx ~= 0) = selectidx_e(selectidx_e ~= 0) & idx;
    if( ~any(selectidx_e) );
        return; end;
    vertextotest = vertextotest(:,idx);

    nMt = numel( bv.cyclictree.Mt );
    nv = size( vertextotest, 2 );
    
    vprintf( ' | (xtra) ', 'imp', [1 bv.param.verbose] );

    % make variables for parfor
    maxdepth = bv.param.extrapath.maxdepth;
    maxnum = bv.param.extrapath.maxnum;
    verbose = bv.param.verbose;
    normval_parfor = zeros( 3, nv );
    dim = bv.param.dim;
    algorithm = bv.cyclictree.algorithm;
    num_core = bv.param.num_core;
    solver = bv.param.solver;
    aux = bv.cyclictree.polytopenorm.auxiliary_data;
    parfor( i = 1:nv, bv.param.num_core );
%     for i = 1:nv
        v_new = vertextotest(:,i);
        normval_i = [2 inf 0].';  % any value larger than 1

        for m = 1:maxdepth;
            v_old = v_new;
            idx = normval_i(2,:) > 1;
            v_old = v_old(:,idx);
            nv_old = size( v_old, 2 );
            if( nv_old > maxnum );
                vprintf( 'O', 'imp', [1 verbose] );  
                normval_parfor(:,i) = nan; 
                break; 
            elseif( nv_old == 0 );
                normval_parfor(:,i) = [0.9;.9;.9]; 
                vprintf( '_', 'imp', [1 verbose] );  
                break; end;
            v_new = zeros( dim, nMt*nv_old );
            for j = 1:nMt;
                idx = (j-1)*nv_old+1 : j*nv_old;
                v_new( :, idx ) = bv.cyclictree.Mt{j}*v_old; end;  %#ok<*PFBNS>
            [ normval_i, ~, iter_im ] = polytopenorm( v_new, VV, aux{:}, algorithm, 'numcore',num_core, 'v',verbose-1, solver, 'bound',[0 1] );
            iter_e = iter_e + iter_im; end; end;

    idx_nonnan = ~any( isnan(normval_parfor), 1 );
    normval_parfor = normval_parfor(:,idx_nonnan);

    selectidx_cumsum_lin = cumsum( selectidx );
    idx = selectidx_cumsum_lin(selectidx_e ~= 0);
    normval(:,idx(idx_nonnan)) = normval_parfor;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
