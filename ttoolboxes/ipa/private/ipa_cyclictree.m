function [ ct ] = ipa_cyclictree( varargin );
% [ cyclictree ] = ipa_cyclictree( bv, cyclictree )
% [ cyclictree ] = ipa_cyclictree( bv, smpflag, ordering, v0, [v0s], [d] )
% Constructs .cyclictree as it shall be, after the first iteration
% This function belongs to ipa!
% Input:
%   cyclictree          blockvar.cyclictree struct from ipa containing the following fields
%                           .smpflag
%                           .ordering
%                           .v0, .oclass,
%   v0s                 can by empty, in which case it is not added
%
% Output:
%   Construct additional entries in .cyclictree structfun()
%       Make entries VV, oo, num_added, rho, XX MISSING 
%       VV contains the vertices of the polytope
%       oo(:,i) contains the candidates of the matrices how to obain the vertex V(:,i)
%       k counts the number of processed steps
%       num_added(k) contains the number of vertices added in the k^th step
%       V and o is such that: buildProduct(A,o(:,n))*V(:,1) == V(:,n)
%       vs are the leading eigenvectors of PI_j
%
% Written by tommsch, 2018-2020

%               tommsch, 2024-09-09,    big rename: .status     to  .has_children,
%                                                   .o          to  .oo,
%                                                   .level      to  .added_in,
%                                                   .V          to  .VV
%                                                   .Pidx       to  .component_idx
%                                                   .L          to  .num_added
%                                                   .num***     to  .num_***
%                                                   .norm***    to  .norm_***
%                                                   .timelvl    to  .time_lvl
%               tommsch, 2024-10-04,    rewritten. Now processes the trees in serial
%                                       Renamed function to ipa_cyclictree
% Changelog:

    if( numel( varargin ) == 2 && ...
        isstruct( varargin{1} ) && ...
        isstruct( varargin{2} ) ...
      );
        ct = ipa_cyclictree2( varargin{:} );
    elseif( numel( varargin ) >= 4 && numel( varargin ) <= 5 );
        ct = ipa_cyclictree5( varargin{:} );
    else;
        fatal_error; end;

    ct = fill_up_zeros( ct );
end

function [ ct ] = ipa_cyclictree2( bv, ct_in );
    ct = ct_in;
    
    % Entries for each Level (Iteration)
    ct = addfieldt( ct, 'added_in',         {} );
    ct = addfieldt( ct, 'component_idx',    {} );
    ct = addfieldt( ct, 'has_children',     {} );
    ct = addfieldt( ct, 'livingvertex',     zeros( 1, 0 ) );  % Number of added vertices in each iteration. Is only used for option <'plot','num_added'>.
    ct = addfieldt( ct, 'num_added',        {} );
    ct = addfieldt( ct, 'norm',             {} );
    ct = addfieldt( ct, 'norm_est',         {} );
    ct = addfieldt( ct, 'norm_lvl',         zeros( 1, 0 ) );  % Number of added vertices in each iteration. Is only used for option <'plot','num_added'>.
    ct = addfieldt( ct, 'norm_parent',      {} );
    ct = addfieldt( ct, 'oo',               {} );
    ct = addfieldt( ct, 'parent',           {} );
    ct = addfieldt( ct, 'rho',              {} );
    ct = addfieldt( ct, 'time_lvl',         zeros( 1, 0 ) );  % Number of added vertices in each iteration. Is only used for option <'plot','num_added'>.
    ct = addfieldt( ct, 'VV',               {} );
    
    % for candidates and nearlycandidates
    for i = 1:numel( ct_in.smpflag );
        ct = add_ct_entry( ct, ct_in.smpflag(i), ct_in.ordering{i}, ct_in.v0{i}, ct_in.oclass{i}, ct_in.graph, bv.param ); end;
    
    % set values for level-things
    ct.time_lvl(end+1) =        0;
    ct.norm_lvl(end+1) =        inf;
    ct.livingvertex(end+1) =    size( [ct.VV{:}], 2 );

    % refactoring check
    %ct_old = orderfields( ct_old );
    %ct = orderfields( ct );
    %assert( isequal(ct, ct_old) );  % Refactoring check

    % post processing
    %ct = orderfields( ct );  % bring the elements in type in alphabetic order
    
end

function [ ct ] = ipa_cyclictree5( bv, smpflag, ordering, v0, v0s );
    
    assert( isfield( bv, 'cyclictree' ), 'ipa:cyclictree', 'To add an entry to the cyclictree using the 5 to 5-argument version, the cyclictree must already be fully constructed.' );
    assert( ~ipa_isgraph(bv.cyclictree.graph), 'ipa:cyclictree', 'Currently this does not work with graphs' );
    if( bv.param.epssym >= 0 );
        assert( issym(v0), 'ipa:cyclictree5', 'To add symbolic stuff to the cyclictree, the passed data must be symbolic.' ); end;

    switch lower( bv.cyclictree.algorithm(1) );
        case {'k','r'};
            v0_sanitized = real( double( v0 ) );
            v0s_sanitized = real( double( v0s ) );
        case {'l','p'};
            v0_sanitized = real( double( v0 ) );
            v0_sanitized(v0_sanitized < 0) = 0;
            v0s_sanitized = real( double( v0s ) );
            v0s_sanitized(v0s_sanitized < 0) = 0;
        case {'c'};  % do nothing
            v0_sanitized = double( v0 );
            v0s_sanitized = double( v0s );
        otherwise; fatal_error; end;


    ct = add_ct_entry( bv.cyclictree, smpflag, ordering, v0_sanitized, [], bv.cyclictree.graph, bv.param );
    ct.smpflag(end+1) = smpflag;
    
    ct.v0{end+1} = v0;
    ct.v0s{end+1} = v0s;
    ct.complexflag(end+1) = norm( imag( double( v0 ) ) ) > 1e-12*max( size( v0 ) );
    ct.balancingvector(end+1) = 1;
    ct.balancingalpha = ipa_constant.UNSET_BALANCING_ALPHA;
    
    ct.ordering{end+1} = ordering;
    ct.time_lvl(end+1) =        0;
    ct.norm_lvl(end+1) =        inf;
    ct.livingvertex(end+1) =    size( [ct.VV{:}], 2 );
    ct.v0idx(end+1) = ct.v0idx(1);
    
    M = tifh( bv.param.epssym >= 0, @() bv.cyclictree.Mt_sym, bv.cyclictree.Mt );
    Pi = buildproduct_fast( M, ordering );
    if( isempty(bv.param.toa) );
        toa = nnz( ordering );
    else;
        toa = buildsum_fast( bv.param.toa, ordering ); end;
    ct.rho_ordering(end+1) = rho( Pi ).^(1/toa);
    ct.toa_ordering(end+1) = toa;
    
    if( bv.param.epssym >= 0 );
        ct.v0_sym{end+1} = v0;
        ct.v0s_sym{end+1} = v0s;
        ct.v0{end} = v0_sanitized;
        ct.v0s{end} = v0s_sanitized;
        end;
    
end

function [ ct ] = fill_up_zeros( ct );
    len = max( cellfun( 'prodofsize', ct.num_added ) );
    for i = 1:numel( ct.num_added );
        if( numel(ct.num_added{i}) < len );
            ct.num_added{i}(len) = 0; end; end;
end
%%

function [ st ] = addfieldt( st, name, value );
    if( ~isfield( st, name ) );
        st.(name) = value; end;
end

function [ ct ] = add_ct_entry( ct, smpflag, ordering, v0, oclass, graph, param );
    switch smpflag;
        case {ipa_constant.candidate, ipa_constant.nearlycandidate};
            ct = add_ct_entry_cnc( ct, ordering, v0, [], graph, param );
        case {ipa_constant.realvertex, ipa_constant.extravertex};
            ct = add_ct_entry_rle( ct, ordering, v0, oclass, graph, param );
        otherwise;
            fatal_error; end;
end

function [ ct ] = add_ct_entry_rle( ct, ordering, v0, oclass, graph, param ); %#ok<INUSD>
    i = numel( ct.added_in ) + 1;
    ct.added_in{i} =        int32( numel(ct.time_lvl) + 1 );
    ct.norm{i}(:,1) =       [inf;inf;inf];  % it is inf by convention, since it is the norm of the point wrt. no polytope
    ct.norm_est{i}(1) =     inf;            % it is inf by convention, since it is the norm of the point wrt. no polytope
    ct.norm_parent{i}(1) =  inf;            % by definition
    ct.parent{i}(1) =       int32( 0 );     % no parent
    ct.rho{i}(1) =          NaN;            % it is NaN, since it stems from no matrix product
    ct.has_children{i}(1) = int8( 0 );      % no children
    ct.VV{i}(:,1) =         v0;
    ct.oclass{i} =          oclass;
    ct.oo{i} =              int32( zeros(1, 1) );
    if( ipa_isgraph(graph) );
        ct.component_idx{i}(1) =    1; end;         % any number is actually allowed here
    ct.num_added{i}(1) =    int32( 1 );
end

function [ ct ] = add_ct_entry_cnc( ct, ordering, v0, ~, graph, param );
    M = tifh( param.epssym >= 0, @() ct.Mt_sym, ct.Mt );
    i = numel( ct.added_in ) + 1;
    for k = 1:size( ordering, 2 );
        orderingk = removezero( ordering(:,k), 'all' );  % the ordering which gets processed now
        szek =      size( orderingk, 1 );
        ct.oo{i}(1,1) = int32( 0 );  % contains the ordering for the vertex 
        ct.VV{i}(:,1) = double( v0 );  % save zeroth entry
        ct.oclass{i} = ordering;
        ct.component_idx{i} = [];
        for j = 1:szek-1  % I start from j=1, since j=0 is the empty product which is already added before. I only must add it once.
            newo =               orderingk(1:j);
            newv =               buildproduct_fast( M, newo, double(v0) );
            switch param.minmax;
                case {'min','max'};
                    % do nothing
                case {'kone'};
                    nrm = norm( newv );
                    if( ~(param.epssym >= 0) && nrm > 1e-12 );
                        newv = newv/nrm; end;
                otherwise;
                    fatal_error; end;
            ct.oo{end}(1:j,end+1) = int32( newo );  % contains the ordering for the vertex 
            ct.VV{end}(:,end+1) =   newv;
            if( ~isempty(newo) );
                ct.oclass{i}(1:j,end+1) = int32( newo ); end; end; 
        if( ipa_isgraph(graph) );
            for j = 1:szek;
                jj = tif( j == 1, szek, j - 1);
                ct.component_idx{i}(end+1) = graph{2}(2,ordering(jj)); end; end; end;  % This looks like a bug that I always take `{1}`. I left the code unchanged here.

    % remove duplicates
    if( size(ct.oo{end}, 2) >= 2 ); 
        [~, idx, ~] = unique( ct.oo{end}.', 'rows' );
        assert( isequal( idx, (1:size(ct.oo{end}, 2))' ) );  % I dont think is allowed to happen
        ct.oo{end} = ct.oo{end}(:,idx);
        ct.VV{end} = ct.VV{end}(:,idx); end;

    value = 1:size( ct.oo{end}, 2 );
    ct.added_in{i}(value) =     int32( numel(ct.time_lvl) + 1 );
    ct.norm{i}(1:3,value) =     inf(3, numel(value) );  % it is inf by convention, since it is the norm of the point wrt. no polytope
    ct.norm_est{i}(value) =     inf;                    % it is inf by convention, since it is the norm of the point wrt. no polytope
    ct.norm_parent{i}(value) =  inf;                    % by definition
    ct.parent{i}(value) =       int32(0);               % vertices in the root have no parents by definition, since otherwise they will not get constructed in the algorithm later
    ct.has_children{i}(value) = int8(0);                % no children by definition, since otherwise they will not get constructed in the algorithm later
    assert( ~isempty(ordering) );

    d = eig( buildproduct_fast(M, ordering(:,1)) );
    [~, d_idx] = max( abs(d) );
    ct.d(i) = d(d_idx);
    ct.rho{i}(value) = abs( ct.d(i) );
    
    ct.num_added{i}(1) =        int32( value(end) );
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
