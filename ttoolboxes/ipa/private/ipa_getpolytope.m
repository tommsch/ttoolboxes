function [ VV, component_idx ] = ipa_getpolytope( bv );   
% [ VV, component_idx ] = ipa_getpolytope( blockvar );   
% This function belongs to ipa!
% select the vertices which make up the current polytope
% This is not a polytope which is suitable to compute a norm with (in general)
%
% Input:
%   var, blockvar   the structs var/blockvar from ipa
%
% Output:
%   VV              selected vertices
%
% Written by tommsch, 2018

    JSR = bv.JSR;
    if( numel(JSR) == 1 )
        JSR = [JSR JSR]; end;

    VV = [bv.cyclictree.VV{:}];  % get all vertices
    idx_select = true( 1, size(VV, 2) );  % indices of all vertices
    component_idx = [bv.cyclictree.component_idx{:}];
    
    normval = [bv.cyclictree.norm{:}];
    idx_norm = normval(2,:) > 1; 
    %idx_select = idx_select & idx_norm;  % only select vertices which are outside or at the border of the polytope.-
    
    val = min( bv.param.epssimplepolytope, (JSR(2)/JSR(1) - 1)/1000 );
    idx_dist = normval(2,:) > 1+val;
    %idx_select = idx_select & idx_dist;  % only select vertices which are far away from other vertices 
    
    VV = VV(:,idx_select);  % remove all unselected vertices
    
    idx_zero = all( VV == 0, 1 );  % remove zero columns
    VV(:,idx_zero) = [];
    
    if( bv.cyclictree.num_component > 1 );
        component_idx = component_idx(idx_select);
        component_idx(idx_zero) = [];
    else;
        component_idx = ones( 1, size(VV, 2) ); end;
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
