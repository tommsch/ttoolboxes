function [ pts_idx, pts_component, num_selected_pts, VV, VV_idx, VV_component, nVVbig ] = ipa_selectvertex( bv );
% This function belongs to ipa!
% Selects vertices for which the norm gets computed
% Selects the vertices of the polytope which are used to compute the norms
%
% Input:
%   bv                  blockvar struct
%
% Output:
%   pts_idx             logical index vector, vertices of which norm needs to be computed
%   pts_component
%   num_selected_pts    2-element vector, number of selected vertices/number of unselected vertices
%   VV                  vertices of chosen polytope which is used to compute the norm
%   VV_idx              logical index vector, selected vertices
%   VV_component
%   nVVbig              Number of vertices in the boundary of the polytope
%
% Written by tommsch, 2018

%               2024-10-13, tommsch,    Refactored
% Changelog:

    normval = [bv.cyclictree.norm{:}];
    idx_noncomputed = isnan( normval(2,:) );  % only select vertices which are not computed yet
    idx_has_children = [bv.cyclictree.has_children{:}];

    if( bv.param.naturalselectiontype > 0 );
        minchoose = ceil( abs(bv.param.naturalselection)/numel(bv.cyclictree.Mt) ) + 1;
    else;
        minchoose = ceil( abs(bv.param.naturalselection) ); end;
    maxchoose = 4*minchoose*max( 1, bv.param.num_core );
    assert( 0 <= minchoose && minchoose <= maxchoose );
    
    pts_idx = (normval(2,:) > 1 | idx_noncomputed) & ~idx_has_children;
    if( bv.param.testoldvertex >= 3 );
        if( bv.param.verbose >= 1 );
            fprintf( '[Recompute] ' );
            clean_message = onCleanup( @() fprintf( repmat( '\b', [1 12] ) ) );
            end;
    else;
        kpi_val = get_kpi_values( bv );
        [pts_idx, ~, ~] = chooseval( kpi_val, [minchoose maxchoose], pts_idx ); 
        pts_idx = select_siblings( pts_idx, minchoose, idx_noncomputed, bv );
        % plot_output( kpi_val, barrier, bv );
        end;

    num_selected_pts = [nnz( pts_idx ), nnz( idx_noncomputed ) - nnz( pts_idx )];

    [VV, VV_idx] = select_VV( pts_idx, normval, minchoose, bv );

    [pts_component, VV_component] = compute_component( pts_idx, VV_idx, bv );
    
    % idx = all( VV == 0, 1 );  % remove zero columns
    % VV(:,idx) = [];
    % VV_component(:,idx) = [];
    
    %idx2 = find( VV_idx );
    %VV_idx(idx2(idx)) = false;

    VV_idx =         isAlways( VV_idx,        'Unknown','true' );
    pts_component  = isAlways( pts_component, 'Unknown','true' );
    VV_component =   isAlways( VV_component,  'Unknown','true' );

    assert( ~any(VV_idx & idx_noncomputed) );
    if( bv.param.testoldvertex < 3 );
        assert( ~any(pts_idx & VV_idx) );
        assert( all(implies(pts_idx, idx_noncomputed)) ); end;
    
    
    nVVbig = nnz( isAlways( normval(2,:) > 1, 'Unknown','true' ) );   % number of all relevant vertices
    
end

function [ kpi_val ] = get_kpi_values( bv );
    switch( abs(bv.param.naturalselectiontype) );
        case inf;
            switch mod( bv.log.iteration, 4 );
                case {1,2,3}; 
                    kpi_val = [bv.cyclictree.norm_est{:} ];
                case 0; 
                    kpi_val = [bv.cyclictree.norm_parent{:} ];
                otherwise;
                    fatal_error; end;
        case 1; 
            kpi_val = [bv.cyclictree.norm_est{:}];
        case 2; 
            kpi_val = [bv.cyclictree.norm_parent{:}];
        case 3; 
            kpi_val = [bv.cyclictree.rho{:}];

        % values from 100 to 999 denote selection rules which are not suitable for production
        case 100; 
            kpi_val = -[bv.cyclictree.rho{:}];
        otherwise;
            fatal_error; end;
end

function [ pts_idx ] = select_siblings( pts_idx, minchoose, idx_noncomputed, bv );
    % select all children of each parent which has at least one selected child.
    if( bv.param.naturalselectiontype > 0 && minchoose < inf );
        LL = cellfun( @sum, bv.cyclictree.num_added );
        valcum = [0 cumsum( LL )];
        for i = 1:numel( bv.cyclictree.smpflag );
            valcum_idx = valcum(i)+1:valcum(i+1);
            idx = pts_idx(valcum_idx);
            parentidx = unique( bv.cyclictree.parent{i}(idx) );
            pts_idx(valcum_idx) = ismember( bv.cyclictree.parent{i}, parentidx ) & idx_noncomputed(valcum_idx); end; end;
end

function [ VV, VV_idx ] = select_VV( pts_idx, normval, minchoose, bv );
    % Select vertices which are used to compute the norms
    
    if( bv.param.epssimplepolytope && length(bv.JSR) > 1 );
        disteps = min( bv.param.epssimplepolytope, (bv.JSR(2)/bv.JSR(1) - 1)/1000 );  % vertices nearer than distval next to each other are disregarded
    else;
        disteps = 0; end;

    if( bv.param.naturalselectiontype > 0 && minchoose < inf );
        % Choose only vertices for VV all of whose children will get a norm computed.
        % Otherwise, the maximum of the computed norms cannot be used as an intermediate true bound for the JSR computation

        % indices of vertices whose norm will be computed or is already computed
        idx_good = ~isnan( normval(2,:) ) | pts_idx;
        idx_good = mat2cell( idx_good, 1, cellfun(@sum, bv.cyclictree.num_added) );

        num_o = numel( bv.cyclictree.smpflag );
        VV_idx_i = cell( 1, num_o );
        for i = 1:num_o;
            idx_computed = ~isnan( bv.cyclictree.norm{i}(2,:) );
            VV_idx_i{i} = ~unfind( bv.cyclictree.parent{i}(idx_good{i} == 0), sum(bv.cyclictree.num_added{i}) )' & idx_computed;  % only select vertices whose childrens norms is computed or gets computed
            if( bv.param.testoldvertex < 3 );
                VV_idx_i{i} = VV_idx_i{i} & (bv.cyclictree.norm{i}(2,:) > 1+disteps); end; end;  % only select vertices which are far enough outside of the polytope.-
        VV_idx = [VV_idx_i{:}];
    else;
        VV_idx = normval(2,:) > 1;
        VV_idx = VV_idx & normval(2,:) > 1+disteps;  end;  % only select vertices which are far away from other vertices 

    VV = [bv.cyclictree.VV{:}];
    idx_nonzero = any( VV, 1 );  % remove zero columns  % these two lines were added during refactoring
    VV_idx = VV_idx & idx_nonzero;

    VV = VV(:,VV_idx);           % remove all unselected vertices
end


function [ pts_component, VV_component ] = compute_component( pts_idx, VV_idx, bv );
    if( bv.cyclictree.num_component > 1 );
        VV_component = [bv.cyclictree.component_idx{:}];
        VV_component = VV_component(VV_idx);

        pts_component = [bv.cyclictree.component_idx{:}];
        pts_component = pts_component(pts_idx); 
    else;
        VV_component = ones( 1, nnz(VV_idx) );  % before refactoring:  size(VV, 2)
        pts_component = ones( 1, nnz(pts_idx) ); end;
end

% function plot_output( kpi_val, barrier, bv );
%     if( bv.param.verbose <= 3 );
%         return; end;
%     figure( 40 );
%     clf;
%     semilogy( sort(kpi_val), '.' ); 
%     hold on;
%     plot( barrier*ones(1, size(kpi_val, 2)), 'r-' );
% end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
