function [ outtype, autoouttype, str ] = ipa_problemtype( wishedtype, varargin ); 
% Identifies problem type 'p','r','c'
% [ type ] = ipa_problemtype( type, in1, ..., inn ); 
%
% Input:
%   intype              wished typed: can be 'a', 'p', 'r', 'c'
%   in_n                arrays
%
% Options:
%   'cpflag'            default = [], if false, then outputtype is never type 'c', no checks are made whether this is sensible or not
%   'verbose', val      default = 1, verbose level
%
% Output:
%   outtype             integer, one of the above.
%   autoouttype         integer, the deduced type, ignoring wishedtype
%                       If wished type is not possible, then a warning is emitted
%   str                 string, Output messages
%
% Written by tommsch, 2018

    [verbose, varargin] = parsem( {'verbose','v'}, varargin, 1 );
    [cpflag, varargin]  = parsem( {'cpflag'}, varargin, [] );

    str = '';
    nonneg = true;
    real = true;
    finite = true;
    for j = 1:numel( varargin );
        nonneg = nonneg && all( isAlways( varargin{j}(:) >= 0, 'Unknown','false' ) );
        real = real && all( isAlways( isreal(varargin{j}(:)), 'Unknown','false' ) );
        finite = finite && all( isAlways( isfinite(varargin{j}(:)), 'Unknown','false' ) ); end;
    if( ~isempty(cpflag) );
        real = ~any(cpflag == 1); end;
    if( nonneg && real );
        outtype = 'p';  % case (P)
    elseif( real );
        outtype = 'r';  % case (R)
    elseif( finite );
        outtype = 'c';  % case (C)
    else;
        error( 'problemtype:input', 'Input matrices are non-finite.' ); end;
    
    autoouttype = outtype;
            
    if( isempty( wishedtype ) || ...
        anym( wishedtype'=='a' ) ...
      );
        % do nothing
    elseif( wishedtype(1) == 'l' && anym( outtype' == 'p' ) || ...
            wishedtype(1) == 'p' && anym( outtype' == 'p' ) || ...
            wishedtype(1) == 'r' && anym( outtype' == 'pr' ) || ...
            wishedtype(1) == 'c' && anym( outtype' == 'prc' ) ...
          );
        outtype = wishedtype;
    elseif( any(wishedtype(1) == 'kK') );
        assert( ~any(cpflag == 1), 'problemtype:k', 'Algorithm ''k'' is not implemented yet for matrices with complex leading eigenvalues.' );
        outtype = wishedtype;
    elseif( wishedtype(1) == 'l' && outtype ~= 'p' );
        assert( nonneg == true, 'problemtype:l', 'Algorithm ''l'' is only implemented yet for non-negative matrices.' );
    elseif( ~any(wishedtype(1) == 'alprck') );
        assert( nonneg == true, 'problemtype:input', 'Could not parse problem type.' );
    else;
        str = vprintf( 'Wished problem type is not possible for input. I choose another problem type.', 'cpr','err', 'imp',[1 verbose], 'str',str ); end
        
    switch outtype;
        case {'K','k','k0','k1','k2','k3','k4','k5','k6','k7','k8'};
            str = vprintf( 'Case (K) (experimental - Results are likely to be wrong).\n','imp',[1 verbose], 'cpr',[.62 .43 0], 'str',str );
        case {'p'};
            str = vprintf( 'Case (P).\n','imp',[1 verbose], 'cpr',[.72 .43 0], 'str',str );
        case {'r'};
            str = vprintf( 'Case (R).\n','imp',[1 verbose], 'cpr',[.72 .43 0], 'str',str );
        case {'l'};
            str = vprintf( 'Case (L).\n','imp',[1 verbose], 'cpr',[.72 .43 0], 'str',str );
        case {'c','c1','c2','c3','c4','c5','c6'};
            str = vprintf( 'Case (C) (%i).\n', outtype, 'imp',[1 verbose], 'cpr',[.82 .33 0], 'str',str );
            str = vprintf( 'If this set of matrices is of practical interest, please contact the author ( tommsch@gmx.at ),\nbecause case (C) never occured so far for matrices of practical interest.\n', 'imp',[0 verbose], 'str',str, 'cpr','err', 'once',1 );
        otherwise;
            error( 'ipa:problemtype', 'Deduced problem type is not supported. This is either due to: bad options provided by the user / a strang set of matrices / or a programming bug.' ); end;
        
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
