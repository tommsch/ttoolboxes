function [ bv ] = ipa_plotoutput( bv );
% [ bv ] = ipa_plotoutput( bv );
%   Plots some output
%
% Input:
%   bv          the struct blockvar from ipa    fprintf( '[Plotting]' );
%
% Available strings for var.param.plot:
%   ['type_' 'zz1_zz2_ ... _zzn'], where 'zzi' is anything from bv.cyclictree.zz      
%                   plots bv.cyclictree.zz1 to bv.cyclictree.zzn
%                   these data-things should have compatible size
%   'norm'          plots bv.cyclictree.norm (in a nice way)
%   'polytope'      plots the polytope (in a nice way)
%   'L'/'num_added' plots the number remaining vertices
%   'tree'          plots the vertices in a graph
%
% Output:
%   nice pictures
%
% Written by tommsch, 2018

%               2019-03-30  Made double logarithmic plot for option 'norm'
%                           Added identifiers 'n', 'graph', etc...
% Changelog:

    bv = ipa_execute_callback( bv, bv.param.callback.plot_begin ); if( bv.log.errorcode ); return; end;
    if( bv.param.verbose >= 1 );
        fprintf( '[Plotting] ' );
        unfprintf = onCleanup( @() fprintf( repmat( '\b', [1 11] ) ) ); end;

    % draw_at_exit = cell( 1, numel( bv.param.plot ) );
    figure_nums = tif( numel(bv.param.plot) > 1, 1:numel(bv.param.plot), 0 );
    for bv_idx = 1:numel(bv.param.plot);
        %if( numel(bv.param.plot) > 1 );
        %    figure( bv_idx ); end;
        if( isequal( bv.param.plot{bv_idx}, 0 ) || ...
            isequal( bv.param.plot{bv_idx}, 'none' ) || ...
            isempty( bv.param.plot{bv_idx} ) ...
          ); 
            return; end;  % fast return
        
        plottype = lower( bv.param.plot{bv_idx} );
        
        if( ~isa(bv.param.plot{bv_idx}, 'function_handle') );
            if( strfind(bv.param.plot{bv_idx}, 'info_') == 1 );
                plottype = 'info'; end; end;
    
        if( false );
        elseif( isa(plottype, 'function_handle') );
            h = @() plottype( bv );

        elseif( startsWith(plottype, {'info'}) );
            h = @() plot_info( bv, bv_idx, plottype );

        elseif( isanyof(plottype, {'o','oo'}) || ...
                startsWith( plottype, {'ordering'}) ...
              );
            h = @() plot_o( bv, bv_idx, plottype );

        elseif( startsWith(plottype, {'norm','n','nrm'}) );
            h = @() plot_norm( bv, bv_idx, plottype );

        elseif( isanyof(plottype, {'pt','poly'}) || ...
                startsWith(plottype, {'polytope'}) ...
              );
            h = @() plot_polytope( bv, bv_idx, plottype );

        elseif( startsWith(plottype, {'tree','graph'}) );
            h = @() plot_tree( bv, bv_idx, plottype );

        elseif( isanyof(plottype, {'num','l','num_added','numadded','added'}) );
            h = @() plot_L( bv, bv_idx, plottype );

        elseif( isanyof(plottype, {'none', 0}) );
            h = @() 1;
            % do nothing

        else;
            h = @() 1;
            vprintf( 'Wrong argument for ''plot''.\n' ); end;
    
        %draw_at_exit{bv_idx} = onCleanup( @() do_draw(h, figure_nums(bv_idx)) );  % we use a dtor here, so that the stuff gets also plotted when the user terminates with Ctrl-C
        do_draw( h, figure_nums(bv_idx) );
        end;
    bv = ipa_execute_callback( bv, bv.param.callback.plot_end ); if( bv.log.errorcode ); return; end;
end

function do_draw( h, figure_num );
    try;
        if( figure_num > 0 );
            figure( figure_num ); end;
        h();
        drawnow;
    catch me;
        fprintf( 'Error in ipa, while plotting.\nError: \n' ); 
        disp( me );
        disp( me.stack(1) );
        fprintf( '------------------------------------------------------\n' );
    end;
end

%% plot functions
%%%%%%%%%%%%%%%%%%%%%%%%%

function plot_info( bv, bv_idx, plottype );  %#ok<INUSD>
    underscore = strfind( bv.param.plot{bv_idx}, '_' );
    underscore(end+1) = length( bv.param.plot{bv_idx} ) + 1;
    name = cell( 1, length(underscore) - 1 );
    what = cell( 1, length(underscore) - 1 );
    for i = 1:numel( underscore )-1;
        name{i} = bv.param.plot{bv_idx}(underscore(i)+1:underscore(i+1)-1);
        if( isempty(strfind(name{i},'.')) );
            what{i} = bv.cyclictree.( name{i} );
        else;
            what{i} = type.( name{i} ); end;
        if(iscell(what{i})); 
            what{i} = [what{i}{:}]; end;
        if( any( strfind(name{i}, 'norm') ) || ...
            any( strfind(name{i}, 'dist') ) ...
          )
            % do nothing
            %what{i} = log10( what{i} );
            %name{i} = ['log10( ' name{i} ' )'];
        elseif( any( strfind(name{i}, 'time') ) || ...
                any( strfind(name{i}, 'dist') ) ...
              );
            what{i} = log10( cumsum(what{i}) );
            name{i} = ['log10(' name{i} ')']; end; end;

    clf; hold on; 
    if( any(diff(cellfun(@length, what))) );  % different length
        for i = 1:length( what );
            plot(what{i}, '.'); end;
        legend( name{:} );
    else;
        switch length( what );
            case 1; 
                plot( what{1}, '.' ); 
                xlabel( 'Vertex-number' ); 
                ylabel( name{1} ); 
                subtitle( name{1} );
            case 2; 
                plot( what{1}, what{2}, '.' ); 
                xlabel( name{1} ); 
                ylabel( name{2} ); 
                subtitle( [name{1} ' vs ' name{2}] );
            case 3; 
                plot3( what{1}, what{2}, what{3}, '.' ); 
                xlabel( name{1} );
                ylabel( name{2} );
                zlabel( name{3} );
                subtitle( [name{1} ' vs ' name{2} ' vs ' name{3}] ); 
                view( 3 );
            otherwise; error( 'ipa:fatal', 'Too many axes to plot.' ); end; end;
end

function plot_o( bv, bv_idx, plottype );  %#ok<INUSD>
    num_o = numel( bv.cyclictree.smpflag );
    for i = 1:num_o;
        subplot_lazy( num_o, i );
        image( removezero( bv.cyclictree.oo{i}, 'bottom' ).', 'CDataMapping', 'scaled' ); 
        cbh = colorbar ;
        num_M = numel( bv.param.M );
        cbh.Ticks = 0:num_M+1;
        cbh.TickLabels = num2cell(0:num_M+1);
        cbh.Limits = [0 num_M];
        colormap( parula(num_M + 1) );
        end;
end

function plot_norm( bv, bv_idx, plottype );  %#ok<INUSD>
    idx_alive = ipa_getalivevertex( bv, 1 );

    norm_reshaped_live = cellfun( @(x,y,z) mat2cell(x.*z, 3, y)', bv.cyclictree.norm, bv.cyclictree.num_added, idx_alive, 'UniformOutput',0 );
    norm_reshaped_live = [norm_reshaped_live{:}];
    norm_reshaped_live = arrayfun( @(x) [norm_reshaped_live{x,:}], 1:size(norm_reshaped_live,1), 'UniformOutput',0 );

    norm_reshaped_dead = cellfun( @(x,y,z) mat2cell(x.*(~z), 3, y)', bv.cyclictree.norm, bv.cyclictree.num_added, idx_alive, 'UniformOutput',0 );
    norm_reshaped_dead = [norm_reshaped_dead{:}];
    norm_reshaped_dead = arrayfun(@(x) [norm_reshaped_dead{x,:}],1:size(norm_reshaped_dead, 1), 'UniformOutput',0 );

    norm_lvl = arrayfun( @(x,y) repmat(x, 1, y), bv.cyclictree.norm_lvl, cellfun('size', norm_reshaped_live, 2), 'UniformOutput',0 );
    norm_lvl = cell2mat( norm_lvl );
    norm_live = [norm_reshaped_live{:}];
    norm_live = norm_live(2,:);
    norm_dead = [norm_reshaped_dead{:}];
    norm_dead = norm_dead(2,:);
    valy = [norm_lvl; norm_live; norm_dead];
    valy(valy<1) = 0;
    try;  %#ok<TRYNC>
        s = warning;
        warning( 'off','MATLAB:Axes:NegativeDataInLogAxis' )
        semilogy( log10(valy.'), '.' ); end;
    warning( s );
    yt = arrayfun( @(x) ['1+' num2str(10^(x)-1)], yticks, 'UniformOutput',0 );
    yticklabels( yt );
    subtitle( 'Norm' );
    ylabel( 'norm' );
    xlabel( 'index of vertex' );
end

function plot_polytope( bv, bv_idx, plottype )  %#ok<INUSL,INUSD>
    clf;  % XX I do not know whether this is correct
    [VVraw, component_idx] = ipa_getpolytope( bv );

    for c = 1:bv.cyclictree.num_component;
        VVraw_c = plot_polytope_get_VVraw_c( VVraw, component_idx, c );
        color_arg = plot_polytope_get_color( c, bv );
        VV_c = plot_polytope_process_VV( VVraw_c, bv.cyclictree.algorithm(1) );
        aux_arg = plot_polytope_get_auxiliary_plot_data( bv );
        proj_arg = plot_polytope_get_projection_matrix( bv );
        arg = [aux_arg proj_arg];

        plot_polytope_plot_hull( VV_c, arg, color_arg, bv );
        plot_polytope_plot_children( plottype, arg, color_arg, c, bv )
        plot_polytope_plot_plane( plottype, arg, c, bv );
        plot_polytope_plot_image( VV_c, plottype, arg, c, bv );
        plot_polytope_func_dependendent( arg, bv );
        plot_polytope_set_plot_properties( bv );
        end;
        
end 

function [ VVraw_c ] = plot_polytope_get_VVraw_c( VVraw, component_idx, c )
    c_idx = component_idx == c;
    VVraw_c = unique( VVraw(:,c_idx).', 'rows' ).';
end

function [ color ] = plot_polytope_get_color( c, bv );
    if( bv.cyclictree.num_component > 1 );
        subplot_lazy( bv.cyclictree.num_component, c );
        title( ['Component ' ('A'+c-1)' ] );
        color = num2color( c );
    else;
        color = []; end;
end

function [ VV_c ] = plot_polytope_process_VV( VVraw_c, algorithm )
    switch algorithm;
        case 'l';
            VV_c = VVraw_c;

        case {'k','K'};
            VV_c = VVraw_c;
            nrm = sum( abs(VV_c).^2, 1).^(1/2);
            VV_c = VV_c./nrm;

        case 'p';
            VV_c = VVraw_c;
            for i = 1:bv.param.dim;
                val = VVraw_c;
                val([1:i-1 i+1:end],:) = 0;
                VV_c = [VV_c val];  %#ok<AGROW>
                val = VVraw_c;
                val(i,:) = 0;
                VV_c = [VV_c val]; end;  %#ok<AGROW>
            VV_c = [VV_c zeros( bv.param.dim, 1 )];

        case 'r';
            VV_c = VVraw_c;
            % do nothing (logic is included in plotm)

        case 'c';
            VV_c = VVraw_c;
            % do nothing (logic is included in plotm)

        otherwise;
            error( 'ipa:fatal', 'wrong algorithm' ); end;
end

function [ proj_arg ] = plot_polytope_get_projection_matrix( bv )
    dim = size( bv.cyclictree.VV{1}, 1 );
    if( dim <= 3 );
        plotm( 'proj','delete', 'hold','on' );
        proj_arg = {};
    elseif( dim <= 3 || ...
            dim == 4 && bv.param.algorithm(1) == 'K' ...
          );
        plotm( 'proj','delete', 'hold','on' );
        proj_arg = {};
    else;
        plane = [bv.cyclictree.VV{:}];
        nrm = [bv.cyclictree.norm{:}];
        nrm = nrm(2,:);
        finite_idx = isfinite( nrm );
        status_idx = [bv.cyclictree.has_children{:}] == 0;
        normal = [];
        plane = plane(:,status_idx & finite_idx);
        nrm = nrm(:,status_idx & finite_idx);
        [~, idx] = sort( nrm, 'descend' );
        plane = plane(:,idx);
        col = [];
        oldrank = 0;
        n = 1;
        while( true );
            if( n >= size( plane, 2 ) || ...
                numel( col ) == dim - 1 ...
              );
                break; end;
            if( rank(plane(:,[col n])) == oldrank + 1 );
                oldrank = oldrank + 1;
                col = [col n];  %#ok<AGROW>
                end;
            n = n + 1; end; 
        orth = null( plane(:,col)' );
        orth = orth(:,end);
        proj = eye(dim) - (orth*orth')/(orth'*orth);
        switch bv.cyclictree.algorithm(1);
            case {'l','p','r','c'}; proj = proj(1:2,:);
            case {'k'}; proj = proj(1:3,:);
            case {'K'}; proj = proj(1:4,:);
            otherwise; fatal_error; end;
        plotm( 'proj',proj, 'funct',bv.cyclictree.algorithm, 'hold','on' );  % store projection matrix in plotm
        proj_arg = {'proj','old'}; end;
end

function [ aux_arg ] = plot_polytope_get_auxiliary_plot_data( bv );
    if( isempty(bv.cyclictree.polytopenorm.auxiliary_data) );
        aux_arg = {};
    else;
        aux_arg = ['auxiliary_data',bv.cyclictree.polytopenorm.auxiliary_data]; end;
end

function [] = plot_polytope_plot_hull( VV, arg, color_arg, bv );
    plotm( VV, '.-', arg{:}, 'color',color_arg, 'hull',1, 'funct',bv.cyclictree.algorithm, 'hold','on' );
    if( strcmp(bv.cyclictree.algorithm(1), 'k') );
        plotm( zeros(bv.param.dim, 1), 'rx', arg{:}, 'hold','on' ); end;
end

function [] = plot_polytope_plot_children( plottype, arg, color_arg, c, bv );
    nrm = [bv.cyclictree.norm{:}];
    nrmidx = nrm(2,:) >= 1;
    parentidx = [bv.cyclictree.parent{:}] == 0;
    VV = [bv.cyclictree.VV{:}];
    val_Pidx = [bv.cyclictree.component_idx{:}];
    if( ~isempty(val_Pidx) );
        idx_c = val_Pidx == c; 
    else;
        idx_c = true( size(nrmidx) ); end;

    if( isequal(bv.param.algorithm(1), 'K') );
        funct = 'K';
    elseif( ~isreal(VV) );
        funct = 'c';
    else;
        funct = ''; end;
    
    if( contains(plottype, 'allpoint') );
        idx = idx_c;
    else;
        idx = parentidx & nrmidx & idx_c; end;
    
    plotm( VV(:,idx), '.', 'color',color_arg, 'ms',2, arg{:}, 'funct',funct, 'hold','on', 'linewidth',2 );
    
    
end

function [] = plot_polytope_plot_plane( plottype, arg, c, bv );
    if( contains( plottype, 'noplane' ) || ...
        ~contains( plottype, 'plane' ) || ...
        bv.param.dim > 3 || ...
        isequal( lower(bv.param.algorithm(1)), 'k' )   ...
       );
        return; end;

    for i = 1:numel( bv.cyclictree.smpflag );
        if( bv.cyclictree.smpflag(i) ~= ipa_constant.candidate || ...
            isempty( bv.cyclictree.v0s{i} ) || ...
            false ... c ~= bv.cyclictree.v0idx ...  % XX DEBUG
          ); 
            continue; end;
        v = bv.cyclictree.v0{i};  % coordinates of point
        vs = bv.cyclictree.v0s{i};
        if( any(vs(:)) && c == bv.cyclictree.v0idx(i) );
            nr = null( real(vs).' );  % vectors in plane
            %try;
            plotm( real([v+nr/2 v-nr/2]), arg{:}, 'hull',1, 'hold','on', 'linewidth',2 );
            %catch me;
            %   end;
        end; end;
end

function [] = plot_polytope_plot_image( VV, plottype, arg, c, bv );
    if( ~contains(plottype, 'image') );
        return; end;

    if( min(bv.JSR) > 0 );
        Mt = bv.cyclictree.Mt ./ min(bv.JSR);
    elseif( max(bv.JSR) < inf );
        Mt = bv.cyclictree.Mt ./ max(bv.JSR);
    else;
        Mt = bv.cyclictree.Mt; end;
    for j = 1:numel( Mt );
        Mt{j} = bv.cyclictree.delta\Mt{j}; end;
    val_Pidx = [bv.cyclictree.component_idx{:}];
    for j = 1:numel( Mt );
        if( ~isempty(val_Pidx) );
            from = bv.cyclictree.graph{2}(1,j);
            to = bv.cyclictree.graph{2}(2,j);
            if( from ~= c );
                continue; end;
        else;
            from = 1;
            to = 1; end;
        subplot_lazy( bv.cyclictree.num_component, to ); 
        plotm( Mt{j}*VV, '--', arg{:}, 'color',num2color(from, [], .8), 'hull',1, 'funct',bv.cyclictree.algorithm, 'hold','on', 'verbose',bv.param.verbose-2, 'linewidth',2 ); end;
end

function [] = plot_polytope_func_dependendent( arg, bv );
    switch bv.cyclictree.algorithm(1);
        case {'p','r','c','l','K'};
        case 'k';
            plotm( zeros(bv.param.dim, 1) , 'ro', arg{:}, 'markersize',10, 'hold','on', 'linewidth',5 );
            plotm( zeros(bv.param.dim, 1) , 'rx', arg{:}, 'markersize',10, 'hold','on', 'linewidth',5 );
            plotm( zeros(bv.param.dim, 1) , 'r.', arg{:}, 'markersize',10, 'hold','on', 'linewidth',5 );
        otherwise;
            fatal_error; end;
end

function [] = plot_polytope_set_plot_properties( bv );
    switch bv.cyclictree.algorithm(1);
        case {'p','r','c','l'};
            if( isequal(bv.cyclictree.algorithm,'p') );
                 val = axis;
                 if( numel(val) == 6 );
                    val([1 3 5]) = 0;
                 else;
                     val([1 3]) = 0; end;
                 axis( val ); end; 
            if( bv.param.dim == 3 );
                for cc = 1:bv.cyclictree.num_component;
                    subplot_lazy( bv.cyclictree.num_component, cc );
                    alpha( 0.4 );
                    lightangle( gca, -45, 30 );
                    %shading( 'interp' );
                    view( 120, 30 ); end;
                alpha .5; end;
        case {'k'};
        case {'K'};
            if( bv.param.dim == 4 );
                alpha .5; end;
        otherwise;
            fatal_error; end;
end

%%

function plot_tree( bv, bv_idx, plottype );  %#ok<INUSD>
    persistent num_o_last;
    num_o = numel( bv.cyclictree.smpflag );
    if( isempty(num_o_last) );
        num_o_last = num_o; end;
    if( num_o_last ~= num_o );
        clf; end;
    for i = 1:num_o;
        if( num_o_last == num_o && ...
            bv.cyclictree.num_added{i}(end) == 0 ...
          );
            continue; end;
        subplot_lazy( num_o, i );
        cla;
        ENTRY = cell(0,2);
        %ENTRY = [ENTRY; {'rho'} bv.cyclictree.rho{i}];  %#ok<AGROW>  % add rho to label
        nrm = bv.cyclictree.norm{i};  %#ok<*NASGU>
        VV = bv.cyclictree.VV{i};
        if( contains(plottype, 'norm') );
            ENTRY = [ENTRY; {'norm'} nrm(2,:)]; end;  %#ok<AGROW>  % add norm to label
        %ENTRY = [ENTRY; {'o'} {cellfun( @(x) mat2str(x'), cellfun(@(x) removezero(x,'compact'),num2cell(bv.cyclictree.oo{i},1),'UniformOutput',0), 'UniformOutput',0 )}];  %#ok<AGROW>
        %ENTRY = [ENTRY; {'co'} {num2cell(VV,1)} ];  %#ok<AGROW>
        oo = bv.cyclictree.oo{i};
        if( contains(plottype, 'noleaf') );
            idx = bv.cyclictree.norm{i}(2,:) > 0.99999;
            oo = oo(:,idx);
            %nrm = nrm(:,idx);
            ENTRY{2} = ENTRY{2}(idx); end;
        makeorderinggraph( oo, 'value',ENTRY, 'verbose',bv.param.verbose-2, 'labeldescription',0, 'labelnumber',0, 'fontsize', 7 );
        valx = bv.cyclictree.ordering{i}; 
        if( iscolumn(valx) ); 
            valx = valx.'; end;
        title( ['tree (' num2str(i) '), ordering: ' num2str(valx)] ); end;
end

function plot_L( bv, bv_idx, plottype );  %#ok<INUSD>
    val = bv.cyclictree.livingvertex;
    Lval = cell2mat( bv.cyclictree.num_added' );
    Lval(end+1,:) = sum( Lval, 1 );
    semilogy( [Lval; val].' ); 
    title( '(num_added) Number of added vertices, number of remaining vertices' );
end

%% helper function
%%%%%%%%%%%%%%%%%%%%%%%%
function [ idx_alive ] = ipa_getalivevertex( bv, bv_idx );  %#ok<INUSD>
% This function belongs to ipa!
% returns linear indices of vertices which are not inside the polytope and whose children are not all computed yet
% 
% Input:
%   bv             the struct blockvar from ipa
%   cellflag       returns cell array of indices for each tree
%
% Written by tommsch, 2018
    num_o = numel( bv.cyclictree.smpflag );
    idx_alive = cell( 1, num_o );
    for i = 1:num_o;
        idx_norm = bv.cyclictree.norm{i}(2,:) > 1;  % vertices which are outside or at the border
        idx_nan = isnan( bv.cyclictree.norm{i}(2,:) );  % vertices without norm
        idx_status = bv.cyclictree.has_children{i} == 0;  % vertices which are children
        val = bv.cyclictree.parent{i}(idx_nan);
        idx_partlyfreshparent = false( size(idx_nan) );
        idx_partlyfreshparent(val) = true;  % vertices whose children norms are not fully computed
        idx_alive{i} = idx_norm & (idx_partlyfreshparent | idx_status); end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
