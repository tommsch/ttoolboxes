function [ check ] = ipa_isgraph( G );
% checks whether G is a graph suitable for ipa/findsmp or not
    if( isempty(G) );
        check = false;
    elseif( isequal( G, {[],[]} ) );
        check = false;
    elseif( (isnumeric(G) || islogical(G) ) && ...
            issquare(G) ...
          );
        check = true;
    elseif( iscell( G ) && ...
            ismatrix( G{1} ) && ...
            isequal( size(G{2}), [2 size(G{1}, 1)] ) ...
          );
        check = true;
    else
        check = false; end;
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
