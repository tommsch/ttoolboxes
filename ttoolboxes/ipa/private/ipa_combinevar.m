function [ var ] = ipa_combinevar( var, blockvar )
% This function belongs to ipa
% Combines informations from var and blockvar saves everythung under var
%
% Written by: tommsch, 2018

    % add blockvar
    var.blockvar = blockvar;
    nblock = numel( blockvar );
    
    % compute some quality metrics
    
    for i = 1:nblock;
        if( ~isfield( blockvar{i}, 'cyclictree' ) || ...
            ~isfield( blockvar{i}.cyclictree, 'norm' ) ...
          );
            tau = nan;
        else;
            nrm = [blockvar{i}.cyclictree.norm{:}];
            has_children = [blockvar{i}.cyclictree.has_children{:}];
            tau = max( nrm(2,has_children == 0) ); end;
        blockvar{i}.log.tau = tau; end;
    
    % compute JSR
    val = cellfun(@(x) x.JSR, blockvar, 'UniformOutput', 0);
    var.JSR = blockjsr( val{:} );
    
    % combine errortext
    var.log.errorcode = zeros( 1, nblock );
    for m = 1:nblock
        var.log.errorcode(m) = var.blockvar{m}.log.errorcode;
        err = var.log.error;
        % combine type.info.errortext
        if( ~isempty(blockvar{m}.log.error) );
            err = [err newline 'Block: ' num2str(m) '/' num2str( numel(blockvar) ) newline '------------------' newline blockvar{m}.log.error]; end; end;  %#ok<AGROW>
    var.log.error = err;
    var.log.errorcode = unique( var.log.errorcode );
        
    % combine type.counter
    var.log.num_stepsmall = 0;
    var.log.num_stepbig = 0;
    var.log.iteration = 0;
    var.log.totaltime = 0;
    var.log.treetime = 0;
    var.log.numberofvertex = 0;
    var.log.tau = 0;
    for m = 1:nblock
        if( isfield(blockvar{m},'log') );
            var.log.tau = max( var.log.tau, blockvar{m}.log.tau );
            var.log.num_stepsmall =   var.log.num_stepsmall +   blockvar{m}.log.num_stepsmall;
            var.log.num_stepbig =     var.log.num_stepbig +     blockvar{m}.log.num_stepbig;
            var.log.iteration =      var.log.iteration +      blockvar{m}.log.iteration;
            if( isfield(blockvar{m}.log, 'treetime') );
                var.log.treetime =       var.log.treetime +       blockvar{m}.log.treetime; end;
            if( isfield(blockvar{m}.log, 'numberofvertex') );
                var.log.numberofvertex = var.log.numberofvertex + blockvar{m}.log.numberofvertex; end; end; end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
