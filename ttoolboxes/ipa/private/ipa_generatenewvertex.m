function [ bv ] = ipa_generatenewvertex( bv );
% blockvar = ipa_generatenewvertex( blockvar );
% This function belongs to ipa!
% Generates all possible new vertices
%
% See also (callbacks): ipa_callback_add_candidate
%
% XX Implement that testeigenplane can find new s.m.p.-candidate
%
% Written by tommsch, 2018

%               2024-10-18, tommsch: Refactoring, moved a lot of code into callbacks
% Changelog:    

    
    bv = ipa_execute_callback( bv, bv.param.callback.generatenewvertex_begin ); if( bv.log.errorcode ); return; end;
    if( bv.param.verbose >= 1 );
        msg = '[Generate vertices] ';
        fprintf( '%s', msg );
        clean_msg = onCleanup( @() fprintf( repmat('\b', 1, numel(msg)) ) ); end;
    for tree_idx = 1:numel( bv.cyclictree.smpflag );  % iterate over all trees  % i is the tree

        [oo, parent, parentidx] = make_new_orderings( bv, tree_idx );
        if( isequal(bv.param.delta, 1) );
            removeidx = find_orderings_which_are_already_in_the_tree_somewhere( oo, bv, tree_idx );  % XX maybe oo must be returned too
        else;
            removeidx = false( 1, size(oo, 2) ); end;

        [vv, rho_val] = construct_vertices_coordinates( oo, bv, tree_idx, removeidx );
        %[removeidx, bv] = find_symbolic_duplicates( oo, vv, bv, tree_idx, removeidx );
        [oo, vv, bv.cyclictree, rho_val, parent] = set_properties_for_the_new_vertices( oo, vv, bv.cyclictree, rho_val, tree_idx, removeidx, parent, parentidx );  %#ok<ASGLU>

        [bv.log.errorcode, bv.log.errorinformation] = test_spectral_radii( oo, bv, rho_val );
        end;
    clear clean_msg
    bv = ipa_execute_callback( bv, bv.param.callback.generatenewvertex_end, rho_val ); if( bv.log.errorcode ); return; end;
end

%% %%%%%%%%%%%%%%%%%%%%%%%

function [ oo, parent, parentidx ] = make_new_orderings( bv, tree_idx );
    num_M = numel( bv.param.M );
    normidx = ~isnan( bv.cyclictree.norm{tree_idx}(2,:) );           % get vertices with unknown norm
    normidx = normidx & ~(bv.cyclictree.norm{tree_idx}(2,:) <= 1);
    parentidx = normidx & ~bv.cyclictree.has_children{tree_idx} == 1;      % test if childrean are already constructed
    % construct indices of parents
    parent = 1:length( parentidx );
    parent = parent(parentidx);
    parent = repmat( parent, 1, num_M );
    % make all new orderings
    oo = bv.cyclictree.oo{tree_idx}(:,parentidx);  % oo: the possible new orderings
    num_oo = size( oo, 2 );
    if( ipa_isgraph( bv.cyclictree.graph ) );
        oo = removezero_compact_fast( oo, 0 );
        oonew = [];
        ct = 0;
        idxr = 1:size( oo, 1 ) + 1;
        for j = 1:num_oo;
            [~, ~, lasto] = find( oo(:,j), 1, 'last' );
            if( isempty(lasto) && isempty(bv.cyclictree.ordering{tree_idx}) );
                new = 1:num_M;
            elseif( isempty(lasto) );
                new = find( bv.cyclictree.graph{1}(bv.cyclictree.ordering{tree_idx}(end), :) );
            else;
                new = find( bv.cyclictree.graph{1}(lasto, :) ); end;

            val = numel( new );
            idxc = (ct+1):ct+val;
            ct = ct + val;
            oonew(idxr,idxc) = [repmat( oo(:,j), [1 numel(new)] );  %#ok<AGROW>
                                new]; end;  
        oo = removezero_compact_fast( oonew, 0 );
    else;
        oo = repmat( oo, 1, num_M );
        oo = [oo; reshape( repmat(1:num_M, num_oo, 1), 1, [] )];
        oo = removezero_compact_fast( oo, 0 ); end;
end

function [ removeidx ] = find_orderings_which_are_already_in_the_tree_somewhere( oo, bv, tree_idx );  % XX I think it is not necessary to return oo
    if( (bv.cyclictree.smpflag(tree_idx) == ipa_constant.candidate || bv.cyclictree.smpflag(tree_idx) == ipa_constant.nearlycandidate ) && ...
        ~isempty(oo) ...
       );
        oclass = bv.cyclictree.oclass{tree_idx};
        if( size(oo, 1) > size(oclass, 1) );
            oclass(size( oo, 1 ),end) = 0;  % make oo and oclass the same length
        elseif( size(oo, 1) < size(oclass, 1) );
            oo(size( oclass, 1 ),end) = 0; end;
        removeidx = ismember( oo.', oclass.', 'rows' ).';  % indices of orderings are in oclass
    else;
        removeidx = false( 1, size(oo, 2) ); end;
end

function [ vv, rho_val ] = construct_vertices_coordinates( oo, bv, tree_idx, removeidx );
    descalelen = zeros( 1, size(oo, 2) );
    if( ~isequal(bv.param.delta, 1) );
        % if delta ~= 1, then the cyclic root is not scaled, but the matrices are scaled.
        % Thus, we have to descale the vertices
        switch( size(bv.cyclictree.ordering{tree_idx}, 2) );
            case 0;
                % do nothing
            case 1;
                for j = 1:size( oo, 2 );
                    descalelen(j) = len_same_start( bv.cyclictree.ordering{tree_idx}, oo(:,j) ); end;
            otherwise;
                error( 'ipa:delta', 'Currently, a value of delta unequal to 1 is only allowed if each smp or nearly-smp corresponds to only one ordering.' ); end; end;

    % the following variables are needed due to parallelization
    szeoo = size( oo, 2 );
    vv = zeros( bv.param.dim, szeoo );  % local variable where coordinates ofnew vertices are saved
    rho_val = zeros( 1, szeoo );        % local variable where spectral radii of matrix products are saved
    v0 = bv.cyclictree.v0{tree_idx};    % starting vector of cyclic root
    delta = bv.cyclictree.delta;
    Mt = bv.cyclictree.Mt;
    if( bv.param.epsspectralradius < inf );
        if( bv.param.num_core >= 1 && szeoo >= 100 );
            parfor( j = 1:szeoo, bv.param.num_core );
                if( removeidx(j) );
                    continue; end;
                newM = buildproduct_fast( Mt, oo(:,j) );
                vv(:,j) = (delta^descalelen(j)) \ newM * v0;
                rho_val(j) = (delta^-descalelen(j)) * rhot( newM ); end;
        else;
            for j = 1:szeoo;
                if( removeidx(j) );
                    continue; end;
                newM = buildproduct_fast( Mt, oo(:,j) );
                vv(:,j) = (delta^descalelen(j))\newM*v0;
                rho_val(j) = (delta^-descalelen(j)) * rhot( newM ); end;
        end;

    else;
        parfor( j = 1:szeoo, bv.param.num_core );
            if( removeidx(j) );
                continue; end;
            vv(:,j) = buildproduct_fast( Mt, oo(:,j), v0 ); end; end;     

    switch bv.param.minmax;
        case {'min','max'};  % do nothing
        case {'kone'};
            for j = 1:szeoo;
                nrm = norm( vv(:,j), inf );
                if( nrm > 1e-12 );
                    vv(:,j) = vv(:,j)/nrm; end; end;
        otherwise; fatal_error; end;
end

% function [ removeidx, bv ] = find_symbolic_duplicates( oo, vv, bv, tree_idx, removeidx );
%     if( ~(bv.cyclictree.epssym >= 0) );  % XX speed this up
%         return; end;
%     assert( ~ipa_isgraph( bv.cyclictree.graph ), 'ipa:graph', 'Symbolic computation together with graphs is not supported yet' );
%     num_tree = numel( bv.cyclictree.smpflag );
%     epssym = bv.cyclictree.epssym;
%     V = bv.cyclictree.VV;
%     lambda_sym = bv.cyclictree.lambda_sym;
%     bvo = bv.cyclictree.oo;
%     v0_sym = bv.cyclictree.v0_sym;
%     M_sym = bv.cyclictree.M_sym;
%     verbose = bv.param.verbose;
%     if( verbose >= 1 ); 
%         fprintf( ' ' ); end;
% %             for ivv = 1:size( vv, 2 );  % loop through all new vertices  % parallelize this
%     for ivv = 1:size( vv, 2 );
%     %parfor( ivv = 1:size( vv, 2 ), bv.param.num_core );  % loop through all new vertices  % parallelize this
%         if( removeidx(ivv) );
%             continue; end;
%         breakflag = false;
%         for tree_idx2 = 1:num_tree;  % loop through all trees
%             val = ([V{tree_idx2}] - vv(:,ivv));  %#ok<PFBNS>  % compute difference numerically
%             val = sum( abs(val), 1 );
%             idx = val <= epssym;  % get candidates of old equal vertices
%             for iidx = find( idx );
%                 scale1 = lambda_sym^nnz( bvo{tree_idx2}(:,iidx) );  %#ok<PFBNS>
%                 Vsym = buildproduct_fast( M_sym, bvo{tree_idx2}(:,iidx), v0_sym{tree_idx2} )/scale1;  %#ok<PFBNS>  % exact value of old vertex
%                 scale2 = lambda_sym^nnz( oo(:,ivv) );
%                 vsym = buildproduct_fast( M_sym, oo(:,ivv), v0_sym{tree_idx} )/scale2;  % exact value of new vertex in vv
%                 if( isAlways(Vsym == vsym, 'Unknown','false' ) );
%                     removeidx(ivv) = true;
%                     if( verbose >= 1 );
%                         bv.cyclictree.norm{tree_idx}(:,ivv) = ipa_constant .DUPLICATE_SYMBOLIC;
%                         fprintf( '\b_\n' );
%                     breakflag = true;
%                     break; 
%                 else;
%                     if( verbose >= 1 );
%                         fprintf( '\bE\n' ); end; end; end;
%             if( breakflag );
%                 break; end; end; end; end; 
%         if( verbose >= 1 );
%             fprintf( '\b' ); end;
% end

function [ oo, vv, cyclictree, rho_val, parent ] = set_properties_for_the_new_vertices( oo, vv, cyclictree, rho_val, tree_idx, removeidx, parent, parentidx );
    oo(:,removeidx) = [];  % delete those
    vv(:,removeidx) = [];
    rho_val(:,removeidx) = []; %
    parent = parent(~removeidx);
    norm_parent = repmat( cyclictree.norm{tree_idx}(2,parentidx), 1, cyclictree.num_matrix );
    norm_parent = norm_parent(~removeidx);  % Why is removeidx only applied to .parent and .norm_parent

    szeoo = size( oo, 2 );
    if( ipa_isgraph( cyclictree.graph ) );
        val = zeros( 1, size(oo, 2) );
        for j = 1:size( oo, 2 );
            [~, ~, val(j)] = find( oo(:,j), 1, 'last' ); end
        cyclictree.component_idx{tree_idx}(end+1:end+szeoo) = cyclictree.graph{2}(2,val); end;
    cyclictree.has_children{tree_idx}(parent) = 1;  % set `has_children` of all parents to one (i.e. they are parents)

    cyclictree.added_in{tree_idx}(end+1:end+szeoo) = length( cyclictree.num_added{tree_idx} ) + 1;
    cyclictree.has_children{tree_idx}(end+1:end+szeoo) = zeros( 1, szeoo );  % set .has_children for all new entries to zero (i.e. they have no children yet)
    cyclictree.norm_parent{tree_idx}(end+1:end+szeoo) = norm_parent;
    cyclictree.parent{tree_idx}(end+1:end+szeoo) = parent;
    cyclictree.norm_est{tree_idx}(end+1:end+szeoo) = NaN;  % norm-estimate is done in another function, because I want to be able to estimate all norms in each round again.
    cyclictree.norm{tree_idx}(:,end+1:end+szeoo) = NaN;
    cyclictree.rho{tree_idx}(end+1:end+szeoo) = rho_val;
    cyclictree.VV{tree_idx}(:,end+1:end+szeoo) = vv;
    cyclictree.num_added{tree_idx}(end+1) = szeoo;
    cyclictree.oo{tree_idx}(1:size( oo, 1 ),end+1:end+szeoo) = oo;
end

function [ errorcode, errorinformation ] = test_spectral_radii( oo, bv, rho_val )
    errorcode = bv.log.errorcode;
    errorinformation = bv.log.errorinformation;
    if( ~isfinite( bv.param.epsspectralradius ) || ...
        bv.param.norestart ...
      );
        return; end;
    switch bv.param.minmax;
        case 'max'; opt_rho = 0;
        case 'min'; opt_rho = inf;
        case 'kone'; error( 'ipa:rhotest', 'The spectral radii cannot be tested for the kone-case.' );
        otherwise; fatal_error; end;
    for n = 1:numel( rho_val );
        if( ipa_isgraph( bv.cyclictree.graph ) && ...
            ~is_admissible( oo(:,n), bv.param.graph{1}, bv.param.period ) ...
          );
            continue; end;
        switch bv.param.minmax;
            case 'max'; check = rho_val(n) > opt_rho;
            case 'min'; check = rho_val(n) < opt_rho;
            case 'kone'; error( 'ipa:rhotest', 'The spectral radii cannot be tested for the kone-case.' );
            otherwise; fatal_error; end;
        if( check );
            opt_rho = rho_val(n);
            opt_idx = n; end; end;

    switch bv.param.minmax;
        case 'max'; check = opt_rho > (1+bv.param.epsspectralradius)*bv.param.delta;
        case 'min'; check = opt_rho < bv.param.delta/(1+bv.param.epsspectralradius);
        case 'kone'; error( 'ipa:rhotest', 'The spectral radii cannot be tested for the kone-case.' );
        otherwise; fatal_error; end;
    if( check );
        % it is necessary to check whether the better candidate is not one of our chosen smp-candidates,
        % due to badly conditioned matrices
        oo_rho = reducelength( oo(:,opt_idx) );
        if( ~searchincellarray( oo_rho, bv.cyclictree.ordering, true ) );
            loo_rho = length( oo_rho );
            errorinformation = {rhot( buildproduct(bv.cyclictree.Mt, oo_rho) )^(1/loo_rho)*bv.cyclictree.lambda, oo_rho };  % {rho, ordering}
            if( all(oo(:,opt_idx) <= bv.cyclictree.num_matrix) );
                errorcode = ipa_errorcode.BETTERORDERINGFOUND;
            else;
                errorcode = ipa_errorcode.BETTERORDERINGFOUND_EXTRAMATRIX; end; end; end;
end

%% %%%%%%%%%%%%%%%%%%%%%%%
% Helper functions

function [ len ] = len_same_start( oo1, oo2 );
% compares two orderings, and checks until which element they are the same
    for len = 1:min( numel(oo1), numel(oo2) );
        assert( ~isequal(oo1(len), 0 ), 'ipa:safety', 'Programming error' );
        if( ~isequal(oo1(len), oo2(len)) );
            len = len - 1;  %#ok<FXSET>
            break; end; end;

    if( isempty(len) );
        len = 0; end;
end

function [ oo ] = removezero_compact_fast( oo, additional );  %#ok<INUSD>
    % remove all zero rows
    idx = ~any( oo, 2 );
    oo(idx,:) = [];
    
    % compact columns
    len = size( oo, 1 ); % + additional;
    for i = 1:size( oo, 2 );
        idx = oo(:,i) ~= 0;
        oo(1:len,i) = [oo(idx,i); zeros( len-nnz(idx), 1 )]; end;
    
    % remove all zero rows again
    idx = ~any( oo, 2 );
    oo(idx,:) = [];
end

function [ check ] = is_admissible( oo, G, period );
    % index 1-based
    % only trailing zeros are allowed
    check = false;
    len = nnz( oo );
    for i = 1:len-1;
        if( ~G(oo(i+1), oo(i)) );
            return; end; end;
    if( period );
        if( ~G(oo(1), oo(len)) );
            return; end; end;
    check = true; 
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
