function [ bv ] = ipa_reset_blockvar( bv )
% [ blockvar, var ] = ipa_reset_blockvar( M, blockvar )
% This function belongs to ipa!
%   Resets the blockvar struct
% Written by: tommsch, 2020

    % logging stuff
    if( ~isfield(bv.log,'error') );
        bv.log.error = ''; end;
    bv.log.errorcode = ipa_errorcode.UNSETERROR;    % this is a not-valid error code
    bv.log.errorinformation = [];         % used to store various information in cases of errors
    bv.log.starttime = tic;               % starting time
    bv.log.num_stepsmall = 0;              % total number of steps conducted
    bv.log.num_stepbig = 0;                % total number of steps conducted including the steps done in linprog
    bv.log.iteration = 0;                 % how many times the worker looped. if we make no natural selection, then this number coincides with the true! treedepth

    % general parameters
    bv.param.dim = size( bv.param.M{1}, 1 );
    
    if( isfield(bv, 'cyclictree') );
        bv = rmfield( bv , 'cyclictree' ); end;
        
    %bv.log = orderfields( bv.log );  % bring the elements in .log in alphabetic order

    vprintf( 'Preworker Start. Time: %s\n', char(datetime('now')), 'imp',[2 bv.param.verbose] );
    vprintf( 'Preprocessed matrices:\n%v\n', bv.param.M, 'imp',[3 bv.param.verbose] );
    vprintf( 'Number of matrices: %i, Dimension: %i\n', numel(bv.param.M), bv.param.dim, 'imp',[2 bv.param.verbose] );
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
