function [ bv ] = ipa_save_to_bv( bv, pts_idx, VV_idx, normval, num_selected, leveltime, iteration, ptstotest, num_normerr );
    % Saves norms and other stuff 
    % this function belongs to ipa
    
    bv.log.num_normerr = num_normerr;
    bv.cyclictree.norm =  savetocellarray( normval, pts_idx, bv.cyclictree.norm );
    bv.log.num_stepsmall = bv.log.num_stepsmall + size( ptstotest, 2 );
    bv.log.num_stepbig =   bv.log.num_stepbig + sum( iteration );
    bv.log.iteration =    bv.log.iteration + 1;
    bv = compute_new_maxnormval( bv, VV_idx );

    bv.cyclictree.livingvertex(end+1) = sum( num_selected );
    bv.cyclictree.time_lvl(end+1) = toc( leveltime );
    vprintf( '\n', 'imp',[1 bv.param.verbose] );
    
    if( bv.param.verbose >= 3 );
        val = whos( 'bv' );
        vprintf( 'Memory used for computing tree: (approx.) %.2g MiB \n', val.bytes/1024/1024 );
        vprintf( 'num_stepsmall/stepbig so far: %i/%.3gM \n', bv.log.num_stepsmall,bv.log.num_stepbig/1000000 );
        vprintf( 'Total number of vertices: %i \n', sum([bv.cyclictree.num_added{:}]) );     
        vprintf( 'Time: %.1fs/%.1fs ', bv.cyclictree.time_lvl(end), toc(bv.log.starttreetime) ); 
        vprintf( 'Vertices per tree: \n' ); 
        for iii = 1:numel( bv.cyclictree.smpflag ); 
            if( bv.cyclictree.num_added{iii}(end) ~= 0 || bv.param.verbose >= 4 ); 
                vprintf( '%r\n', removezero(bv.cyclictree.num_added{iii},'right'),                                    'imp',[3 bv.param.verbose] );end; end; end;
    

    if( bv.param.naturalselection > 0 );
        switch bv.param.minmax;
            case 'max';  vprintf( '%4i: JSR = [ %15.12g, %15.12g ], norm= %13.12g, ', bv.log.iteration, min(bv.JSR), max(bv.JSR), bv.cyclictree.norm_lvl(end), 'imp',[1 bv.param.verbose] );
            case 'min';  vprintf( '%4i: LSR = [ %15.12g, %15.12g ], norm= %13.12g, ', bv.log.iteration, min(bv.JSR), max(bv.JSR), bv.cyclictree.norm_lvl(end), 'imp',[1 bv.param.verbose] );
            case 'kone'; vprintf( '%4i: norm= %15.12g, ', bv.log.iteration, bv.cyclictree.norm_lvl(end), 'imp',[1 bv.param.verbose] );
            otherwise; fatal_error; end; end;

    vprintf( '\n\n', 'imp',[3 bv.param.verbose] );
end

function [ bv ] = compute_new_maxnormval( bv, VV_idx );
% blockvar = ipa_maxnormval( blockvar, idx_VV );
% This function belongs to ipa!
% computes the value maxnormval, respects the value of delta, predelta and epspolyte
%
% Input:
%   idx_VV       logical indices of chosen vertices for polytope used in norm computation
%
% Output:
%   maxnormval
%
% Written by tommsch, 2019

%               2019-03-30     added test whether polytope is ok for computation of intermediate bound
% Changelog: 

    
    % get all indices where .has_children == 0 and norm is computed 
    idx_freshparent = get_fresh_parent( bv );
    normval = [bv.cyclictree.norm{:}];
    maxnormval = max( normval(2,idx_freshparent) );
    
    % check if used polytope yields a new bound for the JSR
    VV_idx = mat2cell( VV_idx, 1, cellfun(@sum, bv.cyclictree.num_added) );
    for i = 1:numel( bv.cyclictree.smpflag );
        idx_noncomputed = isnan( bv.cyclictree.norm{i}(2,:) );
        idx_parents_of_noncomputed = unfind( bv.cyclictree.parent{i}(idx_noncomputed), sum(bv.cyclictree.num_added{i}) )';
        if( any(idx_parents_of_noncomputed & VV_idx{i}) );
            maxnormval = inf;
            if( bv.param.naturalselectiontype > 0 );
                vprintf( 'Intermediate bound could not be computed. This should not happen and indicates a bug. Please inform the author.\n', 'cpr','err' );
                bv.log.error = vprintf( 'Intermediate bound could not be computed. This should not happen and indicates a bug. Please inform the author.\n', 'str',bv.log.error, 'npr' ); end
            break; end; end;  % this is actually not allowed to happen with default options
        
    % consider delta, fastnorm
    if( isempty(maxnormval) ); 
        maxnormval = 1; end;  % compute maxnormval
    maxnormval = max( maxnormval, 1 );
    switch bv.param.minmax;
        case {'min','max','kone'};
            maxnormval = maxnormval/min( 1, bv.param.delta*bv.param.predelta );
        otherwise;
            fatal_error; end;
    if( bv.param.fastnorm >= 2 );
        maxnormval = inf; end;
    if( maxnormval < bv.cyclictree.norm_lvl(end) )
        bv.cyclictree.norm_lvl(end+1) = maxnormval;
    else
        bv.cyclictree.norm_lvl(end+1) = bv.cyclictree.norm_lvl(end); end;
    
    switch bv.param.minmax
        case {'max','kone'};
            if( bv.cyclictree.lambda*bv.cyclictree.norm_lvl(end) < bv.JSR(2) );
                bv.JSR(2) = bv.cyclictree.lambda*bv.cyclictree.norm_lvl(end); end;
        case 'min';
            if( bv.cyclictree.lambda/bv.cyclictree.norm_lvl(end) > bv.JSR(1) );
                bv.JSR(1) = bv.cyclictree.lambda/bv.cyclictree.norm_lvl(end); end;
        otherwise;
            fatal_error; end;

end

function [ idx_freshchild ] = get_fresh_parent( bv );
% This function belongs to ipa!
% returns linear indices of vertices which are not inside the polytope and whose children are not all computed yet
% 
% Input:
%   cellflag    returns cell array of indices for each tree
%
% Written by tommsch, 2019

    num_o = numel( bv.cyclictree.smpflag );
    idx_freshchild = cell( 1, num_o );
    for i = 1:num_o;
        idx_norm = bv.cyclictree.norm{i}(2,:) > 1;  % vertices which are outside or at the border
        idx_nan = isnan(bv.cyclictree.norm{i}(2,:));  % vertices without norm
        idx_status = bv.cyclictree.has_children{i} == 0;  % vertices which are children
        val = bv.cyclictree.parent{i}(idx_nan);
        idx_partlyfreshparent = false( size(idx_nan) );
        idx_partlyfreshparent(val) = true;  % vertices whose children norms are not fully computed

        idx_freshchild{i} = idx_norm & (idx_partlyfreshparent | idx_status); end

    idx_freshchild = [idx_freshchild{:}];
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
