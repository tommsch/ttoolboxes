function [ nrm ] = complexfunct_d_helper( pt, VV, cnorm, varargin );
% Computes the ellipse norm for points pts and complex polytope VV
% [ nrm ] = complexfunct_d_helper( pts, VV, cnorm, [options] );
%
% Input:
%   pts         points
%   VV          complex polytope
%   handle      function handle, function computing the norm of a point wrt a complex polytope
%
% Options:
%   'verbose',val       integer, default=1, verbosity level
%   'bound',bd          1x2 vector, default = [0 1], interval determining when to stop the algorithm
%                       algorithm stops when norm is either inside or outside of the interval
%   'maxvertex',val     integer, default=50, algorithm stops when approximating polytope has 2*maxvertex vertices

%
% Output:
%   p   polygon
%   r   norm of corresponding point wrt vertex (ellipse)
%

    [verbose, varargin] = parsem( {'verbose','v'}, varargin, 1 );
    [bound, varargin] = parsem( {'bound','bd'}, varargin, [], 'expecte', @(x) numel(x) == 2 && x(1)<x(2) );
    [relaccuracy, varargin] = parsem( {'relativeaccuarcy','relaccuaracy','rel'}, varargin, .99995, 'expecte', @isscalar );
    [absaccuracy, varargin] = parsem( {'absoluteaccuarcy','absaccuaracy','abs'}, varargin, .00005, 'expecte', @isscalar );
    [maxvertex, varargin] = parsem( {'maxvertex'}, varargin, 50, 'expect', @(x) 2<x && x<inf );
    parsem( varargin, 'test' );

    
    [pg,pg1,pgsj,nrm_pg] = deal( cast([],class(pt)) ); %#ok<ASGLU> %initialization is done in complexfunct_d_cutpolytope
    while( true );
        %check if we need to compute norms
        %[pg,pg1,nrm_pg] = complexfunct_d_cutpolytope( pt, pg, pg1, nrm_pg );
        [pg,pgsj,nrm_pg] = complexfunct_d_splitvertex( pt, pg, pgsj, nrm_pg );
        idx = any( isnan(nrm_pg), 1 );
        for i = find( idx );
            val = cnorm( pg(:,i) );
            factor = 1/cos( pi/(2*2^floor(log2(pgsj(i)))) );
            %factor = norm( pg1(:,i) );
            nrm_pg(:,i) = [val;val/factor];
        end
        
        if( verbose >= 3 );
            clf; hold on;
            plotm( VV, 'r', 'funct','c', 'hull' );
            plotm( pt, 'k-', 'funct','c', 'hull' );
            plotm( pg, 'k.-', 'funct','r', 'hull' );
            drawnow;
            axis equal;
%             if( size(VV,1) >= 3 );
%                 alpha 0.5;
%                 shading flat; 
%             end;
%             pause; 
            end;        
        
        mx = max( nrm_pg(1,:) );
        mn = max( nrm_pg(2,:) );
        if( ~isempty(bound) && (mn>=bound(2) || mx<=bound(1) || mn>=bound(1)&&mx<=bound(2) || mx-mn<5e-5) );
            break; end;
        if( mn/mx>relaccuracy );
            break; end;
        if( mx-mn<absaccuracy );
            break; end;
        if( size(pg,2) >= maxvertex );
            break; end;

    end
    nrm = [mx;mx;mn];

end

function [ pg, pgsj, nrm_pg ] = complexfunct_d_splitvertex( pt, pg, pgsj, nrm_pg );
    if( isempty(pg) );
        pgsj = [2 3];
        pg = [real(pt) imag(pt)]*[1 -1; 1 1].';
        nrm_pg = cast( nan( 2, size(pg,2) ), class(pt) );
    else;
        [~,idx] = max( nrm_pg(1,:), [], 2 );
        sj = 2*pgsj(:,idx(1));
        s = 2^floor( log2(sj) );
        j = sj - s;
        pgsj = [pgsj(1:idx-1) sj sj+1 pgsj(idx+1:end)];
        pg = [pg(:,1:idx-1) [real(pt) imag(pt)]*[cos(pi*[j j+1]./s-pi/2+pi/(2*s)); sin(pi*[j j+1]./s-pi/2+pi/(2*s))]/cos(pi/(2*s)) pg(:,idx+1:end)]; 
        nrm_pg = [nrm_pg(:,1:idx-1) nan(2,2) nrm_pg(:,idx+1:end)]; 
    end
end

function [ pg, pg1, nrm_pg ] = complexfunct_d_cutpolytope( pt, pg, pg1, nrm_pg ); %#ok<DEFNU>
    %
    % DO NOT USE THIS FUNCTION
    % IT HAS SEVERE NUMERICAL PROBLEMS
    %
    %cuts a vertex of the polygon, depending on nrm_p and bd
    % Output:
    %   polygon     rectangle cirumscribing the complex vertex, symmetric to the origin
    %               only half of the points are returned
    %   rel         norm of vertex of polygon wrt complex vertex (i.e. ellipse)
    
    % E.g.: complexfunct_d_cutpolytope( [2+1i;2-1i] );
    if( isempty(pg) );
        pg1 = [1 1; -1 1].';
        pg = [real(pt) imag(pt)]*pg1;
        nrm_pg = nan( 2, size(pg,2) );
    else;
        [~,idx] = max( nrm_pg(1,:), [], 2 );
        b = pg1(:,idx(1));
        if( idx == 1 );
            a = -pg1(:,end);
            c = pg1(:,2);
        elseif( idx == size(nrm_pg,2) );
            a = pg1(:,end-1);
            c = -pg1(:,1);
        else
            a = pg1(:,idx-1);
            c = pg1(:,idx+1); end;
        [ba,bc] = complexfunct_d_cut_vertex( a, b, c );
        pg1 = [pg1(:,1:idx-1) ba bc pg1(:,idx+1:end)];
        pg = [pg(:,1:idx-1) [real(pt) imag(pt)]*[ba bc] pg(:,idx+1:end)]; 
        nrm_pg = [nrm_pg(:,1:idx-1) nan(2,2) nrm_pg(:,idx+1:end)]; end;

end



    


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

