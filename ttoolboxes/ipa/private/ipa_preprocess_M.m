function [ MM, bv ] = ipa_preprocess_M( MM, bv );

    for j = 1:numel( MM );
        % la = leadingeigenvalue( MM{j} );
        % if( isAlways(isreal(la)) && isAlways(all(la >= 0)) );
        %     % do nothing
        % elseif( isAlways(isreal(la)) && isAlways(all(la <= 0)) );
        %     MM{j} = -MM{j};
        %     vprintf( 'Matrix multipled by -1, to make leading eigenvalue positive. Index of Matrix: %i\n', j, 'cpr','err', 'imp',[1 bv.param.verbose] );
        % else;
        %     vprintf( 'Leading eigenvalues of matrix have different sign or are complex. I did not change the input matrices. Index of Matrix: %i\n', j, 'cpr','err', 'imp',[1 bv.param.verbose] );end;
        % 
        if( ~ipa_isgraph(bv.param.graph) );
            MM{j} = preprocessmatrix( MM{j}, 'v',bv.param.verbose-1, 'remove',1 ); end;
    
        end;
    
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
