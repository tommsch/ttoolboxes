function [ val ] = ipa_persistent_var( str, value )
% half persistent variable
    persistent warning_underflow;
    
    switch str;    
        case 'clear';
            warning_underflow = 0;
        case 'underflow';
            val = warning_underflow;
            if( nargin == 2 );
                warning_underflow = value; end;
        otherwise;
            fatal_error; end;
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
