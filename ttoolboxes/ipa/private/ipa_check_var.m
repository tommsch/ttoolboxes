function [ log ] = ipa_check_var( log, var );
% [ log ] = ipa_check_var( log, var );
% This function belongs to ipa!
% make tests if options are sensible
% Input:
%   log     var.log struct from ipa
%   var     var struct from ipa
%
% Output:
%   log     var.log struct from ipa
%
% Written by tommsch, 2018

    if( ~isempty(var.param.predelta) && ~isequal(var.param.predelta, 1 ) || ...
        ~isempty(var.param.prematrixscaling) ...
      );
        vprintf( 'Debug options ''prematrix'' and/or ''predelta'' are given. The returned values are most likely wrong.\n', 'cpr',[.6 .4 0], 'imp',[1 var.param.verbose] );
        log.error = vprintf( 'Debug options ''prematrix'' and/or ''predelta'' are given. The returned values are most likely wrong.\n', 'str',log.error, 'npr' ); end;

    if( var.param.fastnorm >= 2 );
        vprintf( 'Note that fastnorm >= 2 usually leads to a slow down of the algorithm and disables the computation of intermediate bounds.', ...
                 'cpr',[.6 .4 0], 'imp',[1 var.param.verbose] ); end;
    
    if( var.param.alwaysout && var.param.fastnorm ); 
        vprintf( 'You may want to set <''fastnorm'',0> since ''alwaysout'' is set.\n', ...
                 'cpr','err', 'imp',[0 var.param.verbose] ); end;
    
    if( var.param.epssimplepolytope < 0 ) 
        vprintf( '''epssimplepolytope'' should be greater-equal than zero, otherwise it will have no effect.\n', ...
                 'cpr','err', 'imp',[1 var.param.verbose] ); end;
    
    if( isfinite(var.param.epsspectralradius) && var.param.epsspectralradius <= 0 );
        vprintf( '''epsspectralradius'' should be greater than zero to prevent false positives.\n', ...
                 'cpr','err', 'imp',[1 var.param.verbose] ); end;
    
    if( isfinite(var.param.epseigenplane) && var.param.epseigenplane <= 0 );
        vprintf( '''epseigenplane'' should be greater than zero to prevent false positives.\n', ...
                 'cpr','err', 'imp',[1 var.param.verbose] ); end;
    
    if( var.param.autoextravertex >= 1 ); 
        vprintf( '''autoextravertex'' should be smaller than 1.\n', ...
                 'cpr','err', 'imp',[1 var.param.verbose] ); end;
    
    if( var.param.alwaysout );      
        vprintf( '=============\n''alwaysout'' is set!\n===============\n', ...
                 'imp',[2 var.param.verbose] ); end;
    
    assert( isAlways(var.param.epsequal >= 0), 'ipa:opt', 'epsequal must be greater equal 0.' );
    
    if( ~isequal( lower(var.param.algorithm(1)), 'k' ) );        
        if( var.param.delta > 1 );
            vprintf( 'delta > 1. This is a strange option.\n', 'cpr','err', 'imp',[0 var.param.verbose] ); end;
    else;
        if( var.param.delta < 1 );
            vprintf( 'delta < 1. This is a strange option for case (K).\n', 'cpr','err', 'imp',[0 var.param.verbose] ); end; end;
        
    
    if( var.param.naturalselectiontype >= 100 && var.param.naturalselectiontype < 1000 );
        vprintf( '''naturalselectiontype'' has a value where the algorithm will behave very badly.\n', 'cpr','err', 'imp',[0 var.param.verbose] ); end;
        
    if( var.param.naturalselection<0 );
        vprintf( '''naturalselection'' < 0, thus the algorithm cannot report intermediate bounds for the JSR.\n   If the algorithm still reports bounds, these are wrong!\n   If the algorithm reports an exact value, this value is correct.\n', 'cpr','err', 'imp',[0 var.param.verbose] ); 
        log.error = vprintf( '''naturalselection'' < 0, thus the algorithm cannot report intermediate bounds for the JSR.\n   If the algorithm still reports bounds, these are wrong!\n   If the algorithm reports an exact value, this value is correct.\n', 'str',log.error, 'npr' ); end;
    
    assert( ~isfinite( var.param.epssym ) || ...
            isempty( var.param.v0) || ...
            (issym( var.param.v0{1} ) && ~any( isvpa(var.param.v0{1}) )), ...
           'ipa:opt', '''v0'' must be given as symbolics if symbolic comparisons shall be done.' );
    
    assert( ~isfinite( var.param.epssym ) || ...
            isempty( var.param.extravertex ) || ...
            (iscell( var.param.extravertex ) && issym( var.param.extravertex{1}) ) || ...
            (issym( var.param.extravertex ) && ~isvpa( var.param.extravertex)), ...
           'ipa:opt', '''extravertex'' must be given as symbolics if symbolic comparisons shall be done.' );
   
    assert( isempty( var.param.lambda ) || ...
            ~isfinite( var.param.epssym ) || ...
            (issym( var.param.lambda ) && ~isvpa( var.param.lambda )), ...
           'ipa:opt', '''lambda'' must be given as symbolics if symbolic comparisons shall be done.' );
    
    for i = 1:numel( var.param.plot );
        if( isa(var.param.plot{i}, 'function_handle') );
            continue; end;
        if( any(strfind(var.param.plot{i}, 'tree')) && var.param.fastnorm );
            vprintf( 'If the tree is plotted, and the norms are of interested, it is advisable to set ''<fastnorm,0>''.\n', 'cpr','err', 'imp',[0 var.param.verbose] ); end; end;
     
     if( ipa_isgraph(var.param.graph) && ~isgraph_yields_invariant_polytope(var.param.graph{1}) );
        vprintf( 'The returned polytope w.r.t graph may not be invariant, since some matrices do not map to all other matrices of the corresponding component.\n', 'cpr','err', 'imp',[0 var.param.verbose]); 
        log.error = vprintf( 'The returned polytope w.r.t graph may not be invariant, since some matrices do not map to all other matrices of the corresponding component.\n', 'str',log.error, 'npr' ); end;

    % checks for things with graphs
    if( ipa_isgraph(var.param.graph) && isfinite(var.param.epseigenplane) );
        vprintf( 'The eigenplane test, when graphs are provided, is experimental. The algorithm may report that a candidate is not a candidate although it is.\n  If the algorithm terminates, the result is correct though.\n', 'cpr','err', 'imp',[1 var.param.verbose] ); end;
    
    % checks for lsr
    if( isequal(var.param.minmax,'min') && ~isequal(var.param.invariantsubspace, 'none') );
        str = ['Search for invariant subspaces is enabled and the lower-spectral-radius shall be computed. This is bad.' newline ...
               '   The returned values are thus only a lower bound for the lowerspectral-radius.' newline ...
               '   Since the lower bound returned by this algorithm is in general 0,' newline ...
               '   this implies that the returned value have not mathematical meaning at all.'];
        vprintf( '%s\n', str, 'cpr','err', 'imp',[0 var.param.verbose] ); 
        log.error = vprintf( '%s\n', str, 'cpr','err', 'imp',[0 var.param.verbose], 'str',log.error, 'npr' ); end;

    assert( iscell( var.param.callback.iter_begin ) && ...
            all( cellfun( 'isclass', var.param.callback.iter_begin, 'function_handle') ), ...
            'ipa:opt', '''Value of ''iter_begin'' must be a cell array of function handles.' )
            
end

    
function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
