function [ bv, breakflag ] = ipa_interpret_result( bv );
% Interpretes the result from a run of pre_worker/worker and sets parameters for a new run, if necessary

%               2021-01-20, tommsch: faster restarts after: "no balancing vector found". Found candidates are not deleted anymore
%               2022-11-03, tommsch: Added test whether produced polytope has non-empty interior
%               2023-04-16, tommsch: Fixed a bug in the non-empty interior test
% Changelog: 

    breakflag = 0;  % indicates whether to restart the algorithm anymore
    if( isequal(bv.param.norestart, inf) );
        breakflag = 1;
        return; end;
    
    switch bv.param.minmax;
        case 'max';  xSR = 'JSR';
        case 'min';  xSR = 'LSR';
        case 'kone'; xSR = 'KSR';
        otherwise; fatal_error; end;
    
    verbose = bv.param.verbose;
    switch( bv.log.errorcode );  % parse errorcode

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % UNSET ERROR; MUST NOT HAPPEN
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        case ipa_errorcode.UNSETERROR;
            vprintf( 'Abnormal program termination or other strange things happend.\n  The errocode is not set.\n', 'cpr','err', 'imp',[0 verbose] );
            breakflag = 1;
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % STRANGE ERRORS; ERRORNUMBERS ARE NEGATIVE
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        case ipa_errorcode.INPUTERROR;
            vprintf( 'Input error.\n', 'cpr', 'err', 'imp',[0 verbose] );
            breakflag = 1;

        case ipa_errorcode.UNKOWNERROR;
            vprintf( 'Unkown error occured.\n', 'cpr','err', 'imp',[0 verbose] );
            breakflag = 1;

        case ipa_errorcode.UNSETERROR;
            vprintf( 'Abnormal program termination or other strange things happend.\n', 'cpr','err', 'imp',[0 verbose] );
            breakflag = 1;

        case ipa_errorcode.DRYRUN;
            vprintf( '''dryrun'' option was given. I abort before starting the main loop.\n', 'cpr','err', 'imp',[0 verbose] );
            breakflag = 1;

        case ipa_errorcode.BAD_OPTION;
            vprintf( 'Some bad options were given.\n', 'cpr','err', 'imp',[0 verbose] );
            if( ischar(bv.log.errorinformation) );
                vprintf( '  %s', bv.log.errorinformation, 'cpr','err', 'imp',[0 verbose] );end;
            breakflag = 1;            
            
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % WRONG TERMINATION; ERRORNUMBERS ARE NEGATIVE
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        case ipa_errorcode.NOCANDIDATEFOUND;
            vprintf( 'No candidates found.\n', 'cpr','err', 'imp',[1 verbose] );
            bv = removecyclictree( bv );
            bv.param.findsmp.maxsmpdepth = ceil( 2 + 1.6*bv.param.findsmp.maxsmpdepth );
            num_M = numel( bv.param.M );
            bv.param.findsmp.N = ceil( max( [ 2+1.2*sqrt( bv.param.dim*num_M )*10  2+1.2*bv.param.findsmp.N] ) );
            vprintf( 'Set <''maxsmpdepth'',%i>, <''findsmpN'',%i>.\n', ...
                bv.param.findsmp.maxsmpdepth, bv.param.findsmp.N, ...
                'cpr','err', 'imp',[1 verbose] );
            
        case ipa_errorcode.CANDIDATEISNOSMP;
            vprintf( 'Candidate is no smp.\n', 'cpr','err', 'imp',[1 verbose] );
            bv = rmfield( bv, 'cyclictree' );
            bv = removecyclictree( bv );
            bv.param.findsmp.maxsmpdepth = ceil( 2 + 1.6*bv.param.findsmp.maxsmpdepth );
            num_M = numel( bv.param.M );
            bv.param.findsmp.N = ceil( max( [ 2+1.2*sqrt( bv.param.dim*num_M )*10  2+1.2*bv.param.findsmp.N] ) );
            bv.param.bound = [];
            vprintf( 'Set <''maxsmpdepth'',%i>, <''findsmpN'',%i>.\n', ...
                bv.param.findsmp.maxsmpdepth, bv.param.findsmp.N, ...
                'cpr','err', 'imp',[1 verbose] );
            
        case ipa_errorcode.BETTERORDERINGFOUND;
            switch bv.param.minmax;
                case 'max'; rel = 100*(bv.log.errorinformation{1}/bv.cyclictree.lambda-1);
                case 'min'; rel = 100*(1-bv.log.errorinformation{1}/bv.cyclictree.lambda);
                case 'kone'; fatal_error;
                otherwise; fatal_error; end;
            vprintf( 'Product with better spectral radius found.\n  Length: %i\n  Ordering: %v\n  rho = %f  (better by %g%%)\n', ...
                length(bv.log.errorinformation{2}), bv.log.errorinformation{2}, bv.log.errorinformation{1}, rel, ...
                'cpr','err', 'imp',[1 verbose] );
            vprintf( 'Restart Algorithm with that ordering (This restart does not count as a full restart w.r.t to the counter ''maxnum_restart'').\n', 'cpr','err', 'imp',[1 verbose] );
            bv = removecyclictree( bv );
            bv.log.num_restart = bv.log.num_restart - .25;
            bv.param.ordering = bv.log.errorinformation(2);
            
        case ipa_errorcode.NOBALANCINGVECTORFOUND;
            switch bv.param.minmax;
                case {'max','min'};
                    %vprintf( 'Multiple leading eigenvectors occured. Balancing will not be possible.\n', 'imp',[1 param.verbose] );
                    vprintf( 'No balancing vector found. ', 'cpr','err', 'imp',[1 verbose] );
                case {'kone'};
                    fatal_error;
                otherwise;
                    fatal_error; end;
            idx = bv.cyclictree.smpflag == ipa_constant.candidate;
            olen = cellfun( 'prodofsize', bv.cyclictree.ordering(idx) );
            
            if( bv.param.findsmp.maxsmpdepth > 50 && bv.log.num_restart > 2 && isempty(bv.param.findsmp.bound));
                bv.param.ordering = bv.cyclictree.ordering;
                bv.param.balancing = -1;
                bv = removecyclictree( bv );
                vprintf( 'Set <''balancing'',-1>.\n', 'cpr','err', 'imp',[1 verbose] );
            elseif( ~isempty( olen ) && ...
                    bv.param.findsmp.maxsmpdepth > 20 && ...
                    nnz( bv.cyclictree.smpflag == ipa_constant.candidate ) > bv.param.findsmp.maxsmpdepth*0.4 ...
                  )
                bv = removecyclictree( bv );
                bv.param.balancing = -1;
                bv.param.findsmp.maxsmpdepth = max( min(olen), 5 );
                vprintf( 'Set <''balancing'',%i, ''maxsmpdepth'',%i>.\n', ...
                    bv.param.balancing, bv.param.findsmp.maxsmpdepth, ...
                    'cpr','err', 'imp',[1 verbose] );
            else;
                bv = removecyclictree( bv );
                bv.param.findsmp.maxsmpdepth = ceil( 2 + 1.6*bv.param.findsmp.maxsmpdepth );
                num_M = numel( bv.param.M );
                bv.param.findsmp.N = ceil( max( [ 2+1.2*sqrt( bv.param.dim*num_M )*10  2+1.2*bv.param.findsmp.N] ) );
                bv.param.findsmp.nearlycanddelta = (bv.param.findsmp.nearlycanddelta+1)/2;
                bv.param.findsmp.bound = [];
                vprintf( 'Set <''maxsmpdepth,%i'', ''findsmp_N'',%i, ''nearlycanddelta'',%f>.\n', ...
                    bv.param.findsmp.maxsmpdepth, bv.param.findsmp.N, bv.param.findsmp.nearlycanddelta, ...
                    'cpr','err', 'imp',[1 verbose] );end;

        case ipa_errorcode.MAXTIMEREACHED;
            vprintf( '''maxtime'' reached.\n', 'cpr','err', 'imp',[1 verbose] );
            breakflag = 1;

        case ipa_errorcode.MAXTREETIMEREACHED;
            vprintf( '''maxtreetime'' reached.\n', 'cpr','err', 'imp',[1 verbose] );
            breakflag = 1;

        case ipa_errorcode.MAXSTEPNUMBERREACHED;
            vprintf( '''maxstepnumber'' reached.\n', 'cpr','err', 'imp',[1 verbose] );
            breakflag = 1;

        case ipa_errorcode.MAXNUM_VERTEXREACHED;
            vprintf( '''maxnum_vertex'' reached.\n', 'cpr','err', 'imp',[1 verbose] );
            breakflag = 1;

        case ipa_errorcode.MAXREMAININGVERTEXREACHED;
            vprintf( '''maxremainingvertex'' reached.\n', 'cpr','err', 'imp',[1 verbose] );
            breakflag = 1;

        case ipa_errorcode.MAXTREEDEPTHREACHED;
            vprintf( '''maxtreedepth'' reached.\n', 'cpr','err', 'imp',[1 verbose] );
            breakflag = 1;

        case ipa_errorcode.TESTEIGENPLANE; 
            vprintf( 'Eigenplane test failed. ', 'cpr','err', 'imp',[1 verbose] );
            switch bv.param.minmax;
                case 'max'; comp = '>'; str = 'less';
                case 'min'; comp = '<'; str = 'greater';
                otherwise; fatal_error; end;
            vprintf( 'Candidate is no smp: <Vs, newv> = %.15g %s 1 (but should be %s or equal than 1)\n', bv.log.errorinformation{1}, comp, str, 'cpr','err', 'imp',[1 verbose] );
            % XX Use  bv.log.errorinformation{2} to construct a better candidate
            [c, r] = ipa_construct_better_candidate( bv.log.errorinformation, bv );
            if( r <= 1 );
                vprintf( 'Could not find better candidate given the eigenplane information.\n  This should not happen.\n  I fall back to increase the size of the search space of the smp-candidates.', 'cpr','err', 'imp',[1 verbose] );
                bv.param.findsmp.maxsmpdepth = ceil( 2 + 1.5*bv.param.findsmp.maxsmpdepth );
                bv.param.findsmp.N = 3 + 2*bv.param.findsmp.N;
                if( isempty(bv.param.findsmp.N) );
                    bv.param.findsmp.N = 5000; end;
                bv.param.findsmp.bound = bv.JSR(1) + eps( bv.JSR(1) );
                bv = removecyclictree( bv );
                vprintf( 'Set <''maxsmpdepth'',%i, ''findsmpN'',%i, ''bound'',%f>.\n', ...
                    bv.param.findsmp.maxsmpdepth, bv.param.findsmp.N, bv.param.findsmp.bound, ...
                    'cpr','err', 'imp',[1 verbose] );
            else;
                vprintf( 'Using the eigenplane information I found the following new candidate: %r\n', c, 'imp',[1 verbose] );
                bv.param.ordering = c;
                bv.log.num_restart = bv.log.num_restart - .25;
                end;

        case ipa_errorcode.IS_NOT_A_KONE;
            bv.log.error = vprintf( 'Constructed kone is not pointed.\n\n', 'imp',[0 bv.param.verbose], 'cpr','err', 'str',bv.log.error );
            breakflag = 1;

        case ipa_errorcode.NOINVARIANTKONE;
            bv.log.error = vprintf( 'Matrices do not to construct an invariant kone.\n\n', 'imp',[0 bv.param.verbose], 'cpr','err', 'str',bv.log.error );
            breakflag = 1;

        case ipa_errorcode.KONE_ERROR;
            bv.log.error = vprintf( 'Some general kone related error occured. I have to quit.\n\n', 'imp',[0 bv.param.verbose], 'cpr','err', 'str',bv.log.error );
            breakflag = 1;

        case ipa_errorcode.LIKELYNOINVARIANTKONE;
            bv.log.error = vprintf( 'Matrices most likely do not to construct an invariant kone. I will abort.\n\n', 'imp',[0 bv.param.verbose], 'cpr','err', 'str',bv.log.error );
            breakflag = 1;

        case ipa_errorcode.COMPLEXKONE;
            bv.log.error = vprintf( 'Matrices may have a complex kone, but this algorithm cannot compute it.\n\n', 'imp',[0 bv.param.verbose], 'cpr','err', 'str',bv.log.error );
            breakflag = 1;

        case ipa_errorcode.NOINVARIANTKONE_DUALTEST;
            bv.log.error = vprintf( 'The matrices do not have an invariant kone. The dual-kone test failed.\n\n', 'imp',[0 bv.param.verbose], 'cpr','err', 'str',bv.log.error );
            breakflag = 1;
            
        case ipa_errorcode.MAXITERATION;
            vprintf( '''maxiterations'' reached.\n', 'cpr','err', 'imp',[1 verbose] );
            breakflag = 1;
            
        case ipa_errorcode.TOOMUCHCANDIDATE;
            if( isfield(bv, 'cyclictree') && isfield(bv.cyclictree, 'smpflag') );
                idx = bv.cyclictree.smpflag == ipa_constant.candidate;
                olen = cellfun( 'prodofsize', bv.cyclictree.ordering(idx) );
            else;
                olen = cellfun( 'prodofsize', bv.cyclictree.ordering ); end;
            
            vprintf( 'Too many candidates found. Nearly all orderings seem to be smp''s.\n', 'cpr','err', 'imp',[1 verbose] );
            bv = removecyclictree( bv );
            bv.param.epsspectralradius = inf;
            bv.param.epseigenplane = inf;
            bv.param.findsmp.maxsmpdepth = ceil( olen(5) );
            vprintf( 'Set <''epseigenplane'',inf, ''epsspectralradius'',inf, ''maxsmpdepth'',%i>.\n', bv.param.findsmp.maxsmpdepth, 'cpr','err', 'imp',[1 verbose] );
            if( isequal(bv.param.balancing,1) || isequal(bv.param.balancing,2) );
                bv.param.balancing = -1;
                vprintf( 'Set <''balancing'',-1>.\n', 'cpr','err', 'imp',[1 verbose] );end;
            if( isfinite(bv.param.epssym) && bv.param.delta == 1 );
                if( bv.param.findsmp.nearlycanddelta < 1 );
                    bv.param.findsmp.nearlycanddelta = 1;
                    vprintf( 'Set <''ncdelta'',1>. ', 'cpr','err', 'imp',[1 verbose] );end; end;
            
        case ipa_errorcode.NONZEROJSR;
            vprintf( 'Could not find candidate with positive spectral radius, but set of matrices has positive joint spectral radius.\n Search for new candidate.\n', 'cpr','err', 'imp',[1 verbose] );
            bv = removecyclictree( bv );
            bv.param.findsmp.maxsmpdepth = 2 + 2*bv.param.findsmp.maxsmpdepth;
            num_M = numel( bv.param.M );
            bv.param.findsmp.N = ceil( max( [ 2+2*sqrt( bv.param.dim*num_M )*10  2+2*bv.param.findsmp.N] ) );
            bv.param.findsmp.bound = 1e-14;
            vprintf( 'Set <''maxsmpdepth'',%i, ''findsmp_N'',%i, ''bound'',%f>.\n', ...
                bv.param.findsmp.maxsmpdepth, bv.param.findsmp.N, bv.param.findsmp.bound, ...
                'cpr','err', 'imp',[1 verbose] );            
            
        case ipa_errorcode.OUTOFMEMORY;
            vprintf( 'Out of memory.', 'cpr','err', 'imp',[1 verbose] );
            breakflag = 1;            
            
        case ipa_errorcode.DUPLICATELEADINGEIGENVECTOR;
            vprintf( 'Multiple leading eigenvectors occured.\n', 'imp',[1 verbose] );
            if( isfinite(bv.param.epssym) );
                if( bv.param.findsmp.nearlycanddelta < 1 );
                    bv.param.findsmp.nearlycanddelta = 1;
                    vprintf( 'Set <''ncdelta'',1>. ', 'cpr','err', 'imp',[1 verbose] );end;
                bv = enable_symbolic_computation( bv, 5e-12 );
            else;
                vprintf( 'Algorithm aborts since symbolic computations are disabled.\n', 'cpr','err', 'imp',[1 verbose] );
                breakflag = 1; end;
            
        case ipa_errorcode.MAXONENORM;
            idx = bv.cyclictree.smpflag == ipa_constant.candidate;
            olen = cellfun( 'prodofsize', bv.cyclictree.ordering(idx) );
            vprintf( 'A lot of vertices have norm near to 1. Set some new parameters.\n', 'cpr','err', 'imp',[1 verbose] );
            switch lower( bv.cyclictree.algorithm );
                case {'kone','k','k1','k2','k3','k4','k5','k6','k7'};
                    if( bv.param.epssym < 0 );
                        bv = enable_symbolic_computation( bv, 5e-6 ); end;
                    if( bv.param.findsmp.maxsmpdepth < 10 );
                        bv.param.findsmp.maxsmpdepth = bv.param.findsmp.maxsmpdepth + 2; end;
                    bv.param.maxonenorm = inf;
                    vprintf( 'Set <''maxsmpdepth'',%i, ''maxonenorm'',inf>.\n', bv.param.findsmp.maxsmpdepth, 'cpr','err', 'imp',[1 verbose] );
                    
                case {'p','r','c1','c2','c3','c4','l'};
                    if( bv.param.epssym < 0 );
                        bv.param.maxonenorm = max( 0, ceil(0.6*bv.param.maxonenorm) );
                        switch lower( bv.cyclictree.algorithm(1) );
                            case {'c'}; epsval = 5e-5;
                            case {'r','p','l'}; epsval = 5e-12;
                            otherwise; fatal_error;  end;
                        bv = enable_symbolic_computation( bv, epsval );
                    elseif( bv.param.findsmp.nearlycanddelta < 1 && ...
                            nnz( bv.cyclictree.smpflag == ipa_constant.nearlycandidate ) > 0 ...
                          );
                        bv.param.findsmp.nearlycanddelta = 1;
                        vprintf( 'Set <''ncdelta'',1>.\n', 'cpr','err', 'imp',[1 verbose] );
                    elseif( ~isempty(olen) && nnz(bv.cyclictree.smpflag == ipa_constant.candidate) >= max(cellfun('length',bv.cyclictree.ordering)) );
                        % case when nearly every product is a s.m.p.-candidate
                        bv.param.findsmp.maxsmpdepth = min( olen );
                        bv.param.maxonenorm = inf;
                        vprintf( 'Set <''maxsmpdepth'',%i, ''maxonenorm'',inf>.\n', bv.param.findsmp.maxsmpdepth, 'cpr','err', 'imp',[1 verbose] );            
                    elseif( ~isempty(olen) && isfinite(bv.param.maxonenorm) && bv.param.findsmp.maxsmpdepth>min(olen) && max(olen)>min(olen) );
                        bv.param.findsmp.maxsmpdepth = max( [1 min(olen) max(olen)-1] );
                        vprintf( 'Set <''maxsmpdepth'',%i>.\n', bv.param.findsmp.maxsmpdepth, 'cpr','err', 'imp',[1 verbose] );            
                    elseif( isfinite(bv.param.maxonenorm) );
                        bv.param.maxonenorm = inf;
                        vprintf( 'Set <''maxonenorm'',inf>.\n', 'cpr','err', 'imp',[1 verbose] );end;

                otherwise;
                    warning( 'ipa:interpret_result', 'For this case, it is not yet implemented what shall happen when vertices norms are too often equal or near 1.\n I am guessing some new options.\n' );
                    if( bv.param.epssym < 0 );
                        bv = enable_symbolic_computation( bv, 5e-6 ); end;
                    if( bv.param.findsmp.maxsmpdepth < 10 );
                        bv.param.findsmp.maxsmpdepth = bv.param.findsmp.maxsmpdepth + 2; end;
                    bv.param.maxonenorm = inf;
                    vprintf( 'Set <''maxsmpdepth'',%i, ''maxonenorm'',inf>.\n', bv.param.findsmp.maxsmpdepth, 'cpr','err', 'imp',[1 verbose] );
                    end;

        case ipa_errorcode.ADMISSIBLEALGORITHM;
            vprintf( 'Chosen algorithm is not in the list of admissible algorithms. \n', 'cpr','err', 'imp',[1 verbose] );
            breakflag = 1;
            
        case ipa_errorcode.STALENORM;
            if( bv.param.epssym < 0 );
                switch lower( bv.cyclictree.algorithm(1) );
                    case {'c'}; epsval = 5e-5;
                    case {'k'}; epsval = 5e-5;
                    case {'r','p','l'}; epsval = 5e-12;
                    otherwise; fatal_error;  end;
                bv = enable_symbolic_computation( bv, epsval );
            else;
                vprintf( 'There was no progress in the computation for more than ''stale_norm'' iterations. I quit. \n', 'cpr','err', 'imp',[1 verbose] );
                breakflag = 1; end;

        case ipa_errorcode.UNEXPECTED_COMPLEX_EIGENVECTOR;
            vprintf( 'A complex eigenvector was generated somewhere during the computation, which should not have happen. I have to quit.\n', 'cpr','err', 'imp',[1 verbose] );
            breakflag = 1;
            
        case ipa_errorcode.NONNEGLECTABLE_COMPLEX_PART;
            vprintf( 'An eigenvector with non-neglectable negative part was generated somewhere during the computation, which should not have happen. I have to quit.\n', 'cpr','err', 'imp',[1 verbose] );
            breakflag = 1;

        case ipa_errorcode.UNEXPECTED_NONNONNEGATIVE_VECTOR;
            vprintf( 'Could not find a non-negative eigenvector. This should not happen. I have to quit.\n', 'cpr','err', 'imp',[1 verbose] );
            breakflag = 1;
            
        case ipa_errorcode.NONNEGLECTABLE_COMPLEX_PART;
            vprintf( 'The computation yielded a (dual)-eigenvector with non-neglectable complex part. This should not happen and indicates a numeric problem with the input. I have to quit.\n', 'cpr','err', 'imp',[1 verbose] );
            breakflag = 1;
            
        case ipa_errorcode.MAXNORMERR;
            vprintf( 'Norm computations produced too much errors.\n  Add the option <''maxnormerror'',inf> to disable this check.\n', 'cpr','err', 'imp',[1 verbose] );
            breakflag = 1;
            
        case ipa_errorcode.UNDERFLOW;
            vprintf( 'Potential underflow occurred. I have to stop.\n  Use option <''norestart'',2> to bypass underflow checks.\n', 'cpr','err', 'imp',[1 verbose] );
            breakflag = 1;
            
            
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % CORRECT TERMINATION; ERRORNUMBERS ARE NEGATIVE
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        case ipa_errorcode.VALIDATEUPPERBOUNDNORM;
            vprintf( 'Norm is less than validateupperbound_norm = %f. Algorithm can terminate\n', bv.param.validateupperbound_norm, 'cpr',[0,0.5,0], 'imp',[1 verbose] );
            bv = check_nonemptyinterior( bv );
            breakflag = 1;
            
        case ipa_errorcode.VALIDATEUPPERBOUND;
            vprintf( '%s is less than validateupperbound = %f. Algorithm can terminate\n', xSR, bv.param.validateupperbound, 'cpr',[0,0.5,0], 'imp',[1 verbose] );
            bv = check_nonemptyinterior( bv );
            breakflag = 1;
            
        case ipa_errorcode.VALIDATELOWERBOUND;
            vprintf( '%s is greater than validatelowerbound = %f. Algorithm can terminate\n', xSR, bv.param.validatelowerbound,'cpr',[0,0.5,0], 'imp',[1 verbose] );
            breakflag = 1;
            
        case {ipa_errorcode.NOERROR, ipa_errorcode.NOERROR_INVARIANTSUBSPACE, ipa_errorcode.NOERROR, ipa_errorcode.NOERROR_APPROX, ipa_errorcode.NOERROR_TRIVIAL};
            bv = check_nonemptyinterior( bv );
            vprintf( '\nAlgorithm terminated correctly. ', 'cpr',[0,0.5,0], 'imp',[1 verbose] );
            switch bv.log.errorcode
                case ipa_errorcode.EXACTVALUEFOUNDDURINGBALANCING;   vprintf( 'during balancing.\n', 'cpr',[0,0.5,0], 'imp',[1 verbose] );
                case ipa_errorcode.NOERROR_INVARIANTSUBSPACE;        vprintf( 'in one invariant subspace..\n', 'cpr',[0,0.5,0], 'imp',[1 verbose] );
                case {ipa_errorcode.NOERROR,ipa_errorcode.NOERROR_TRIVIAL};    vprintf( '\n', 'cpr',[0,0.5,0], 'imp',[1 verbose] );
                otherwise; fatal_error; end;
            if( bv.param.delta == 1); 
                vprintf( 'Exact value found.\n', 'cpr',[0,0.5,0], 'imp',[1 verbose] );
                bv.JSR = bv.JSR(1);
                vprintf( '%s = %15.12g\n', xSR, bv.JSR, 'imp',[1 verbose] );
            else
                if( bv.param.delta > 1 );
                    vprintf( 'Delta > 1. I do not know what that means. The returned value may be wrong.\n', 'cpr',[0,0.5,0], 'imp',[1 verbose] );end;
                bv.log.errorcode = ipa_errorcode.NOERROR_APPROX;
                vprintf( 'Interval found.\n', 'cpr',[0,0.5,0], 'imp',[1 verbose] );
                if( length(bv.JSR) == 1 );
                    bv.JSR = [bv.JSR bv.JSR/min( 1, bv.param.delta )]; end;
                vprintf( '%s = [%15.12g, %15.12g]\n', xSR, bv.JSR(1), bv.JSR(2), 'imp',[1 verbose] );end;
            breakflag = 1;

        case {ipa_errorcode.ZEROJSR};
            vprintf( 'Algorithm terminated correctly. Exact value found.\n', 'cpr',[0,0.5,0], 'imp',[1 verbose] );
            vprintf( 'LSR = JSR = 0\n', 'imp',[1 verbose] );
            breakflag = 1;
            
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  
        % THE VERY BAD TYPE Of TERMINATION
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        otherwise 
            vprintf( 'Unkown error occured. I have to quit.\n', 'cpr','err', 'imp',[1 verbose] );
            breakflag = 1; end;

    if( bv.param.norestart && ~breakflag);
        vprintf( ['Algorithm stops because it is unlikely that the algorithm will produce any result\n'...
                                '  with this set of parameters. But since option ''norestart'' is given, I cannot change the parameters.\n'...
                                '  You can either set other options by hand, or try <''norestart'',0> or <''norestart'',2> to be less/more strict w.r.t. restarts.\n'] , 'cpr','err', 'imp',[1 verbose] );
        breakflag = 1; end;
end

function bv = removecyclictree( bv );
    bv.param.smpflag = [];
    bv.param.ordering = [];
    bv.param.v0 = [];
    bv.param.v0s = [];
    bv.param.extravertex = [];
    bv.cyclictree.algoritm = [];
end

function bv = check_nonemptyinterior( bv );
    if( ~isfield( bv, 'cyclictree' ) || ...
        ~isfield( bv.cyclictree, 'V' ) ...
      );
        return; end;
    
    switch bv.cyclictree.algorithm(1);
        case {'k','K'};
            V = [bv.cyclictree.VV{:}];
            check = rank( V ) == bv.param.dim;
        case {'p','l'};
            V = [bv.cyclictree.VV{:}];
            V = any( V, 2 );
            check = all( V );
        case 'r';
            V = [bv.cyclictree.VV{:}];
            check = rank( V ) == bv.param.dim;
        case 'c';
            V = [bv.cyclictree.VV{:}];
            V = [real( V ) imag( V )];
            check = rank( V ) == bv.param.dim;
        otherwise;
            fatal_error; end;

    if( ~check )
        bv.log.error = vprintf( '\nConstructed polytope has empty interior. Thus, the computed value of the JSR may be plainly wrong.\n\n', 'imp',[0 bv.param.verbose], 'cpr','err', 'str',bv.log.error ); end;
end

function [ bv ] = enable_symbolic_computation( bv, eps );
    bv.param.epssym = eps;
    bv.param.findsmp.sym = max( 1, abs(bv.param.findsmp.sym) );
    bv.param.leadingeigenvector.sym = max( 2, abs(bv.param.findsmp.sym) );
    vprintf( 'Enable symbolic computation. Set <''epssym'',%g, ''findsmp_sym'',%g, ''leadingeigenvector_sym'',%g>.\n', bv.param.epssym , bv.param.findsmp.sym, bv.param.leadingeigenvector.sym, 'cpr','err', 'imp',[1 bv.param.verbose] );
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
