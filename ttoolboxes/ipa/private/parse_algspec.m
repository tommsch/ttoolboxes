function [ algorithm, method ] = parse_algspec( spec, varargin );
% parses algorithm specifiers
% [ algorithm, method ] = parse_algspec( spec, [M1, ..., Mn] )
%
% Input:
% =========
%   spec         string, format is given below
%   Mx           matrices, are used to deduce the output if input is 'auto'
%
% Output:
% =========
%   algorithm   string, which polytope is given, format is given below
%   method      string, which algorithm for computing the polytopenorm shall be used, format is given below
% 
% Format:
% ===========
%   algorithm: one of a, p, r, c, l, k: (large caps also possible)
%                       'a'         auto (default)
%                       'p'         case P, jsr for non-negative matrices, and use of the norm in the first orthant
%                       'r'         case R, jsr for matrices with real leading eigenvalue, and use of minkowski norm
%                       'c'         case C, jsr for matrices, and use of the elliptic minkowski norm
%                       'l'         case L, lsr for non-negative matrices
%                       'k'         case K, experimental, computation of in invariant kone
%   asterisk *: if given, the dual body will be considered. Currently only implemented for case 'k'
%   number: an integer, one digit
%   computaton method: one of e,n,i,v,s,E,N,I,V,S
%                       large caps are strict forms. small caps try a faster algorithm before when possible
%                       a           auto (default)       (old: 'a'=-1)
%                       e,g         estimate (e ... auto, g ... symbolic estimate        
%                       n           numerical            (old: 'n'=0 )
%                       v           vpa  (old: 'v'=1, 'V'=3 )
%                       s           sym  (old: 's'=2, 'S'=4 )
%
%
% Example:
% =========
%           'aa'   type: auto, method: auto
%           'c5s'  type: complex 5th method (old 'ce'), method: mixed numerically/interval arithmetic/symbolically
%           'cS'   type: complex default method, method: strictly symbolically
%
% Written by: tommsch, 2022-02-17

% Changelog:    tommsch,    2024-11-07,     Removed 'interval' algspecs



    spec = char( spec );
    [ algorithm, method ] = parse_symspec_char( spec );
    
    if( nargin >= 2 && (strcmp(algorithm(1), 'a') || ...
        strcmp(method(1), 'a')) ...
      );
        mat = cellfun( @(x) x(:)', varargin, 'UniformOutput',false );
        mat = [mat{:}];
        if( strcmp(algorithm(1), 'a') );
            assert( ~allm( isnan(mat) ), 'algspec:nan', 'If algorithm shall be deduced automatically, the least accurate description of the input data must be passed in evaluated form.' );
            if( all( isreal(mat(:)) ) );
                if( all(mat(:) >= 0) );
                    algorithm = 'p';
                else;
                    algorithm = 'r'; end;
            else;
                algorithm = 'c'; end; end;
        if( strcmp(method(1), 'a') );
            if( isvpa(mat) );
                method = 'v';
            elseif( issym(mat) );
                method = 's';
            %elseif( isa(mat, 'intval') );
            %    method = 'i';
            else;
                method = 'n'; end; end; end;

end

function [ algorithm, method ] = parse_symspec_char( spec )
    algorithm = '';
    if( numel(spec) >= 1 );
        if( false );
        elseif( startsWith(lower(spec), 'auto') );
            algorithm = 'a';
            spec(1:4)=[];
        elseif( startsWith(lower(spec), 'kone') );
            algorithm = 'k';
            spec(1:4)=[];
        else;
            switch lower( spec(1) );
                case {'a','k','p','r','c','l'}; 
                    algorithm = lower( spec(1) );
                    spec(1) = [];
                case {'d','*'};
                    algorithm = [lower( spec(2) ) '*'];
                    spec(1:2) = [];
                otherwise;
                    fatal_error; end; end; end;  % XX I think this is an allowed branch for some specs
    if( numel(spec) >= 1 && spec(1) == '*' );
        algorithm = [algorithm '*'];
        spec(1) = []; end;

    if( numel(spec) >= 1 && spec(1) >= '0' && spec(1) <= '9' );
            algorithm = [algorithm spec(1)];
            spec(1) = []; end; 
        
    method = '';
    if( strcmp(algorithm, 'r') && ~isempty(spec) );
        switch spec;
            case {'e1','e2','e3'}; method = spec; spec(1:2) = [];
            case {'eproj'}; method = 'e1'; spec(1:5) = [];
            case {'ediff'}; method = 'e2'; spec(1:5) = [];
            case {'eell'};  method = 'e3'; spec(1:4) = [];
            case {'e'};     method = 'e';  spec(1) = [];
            otherwise; end; end;  % do nothing
    
    while( numel(spec) >= 1 );
        switch lower( spec(1) );
            case {'k','p','r','c','l'};
                spec(1) = [];
            case {'a','e','g','n','v','s'}; 
                method(end+1) = spec(1);  %#ok<AGROW>
                spec(1) = [];
                if( numel(spec) >= 1 && spec(1) >= '0' && spec(1) <= '9' );
                    method(end+1) = spec(1);  %#ok<AGROW>
                    spec(1) = []; end;
            otherwise;
                error( 'ttoolboxes:parsesymspec', 'wrong input' ); end; end;
        
    if( isempty(algorithm) );
        warning( 'ipa:algspec', 'No algorithm given. I default to ''auto''.' );
        algorithm = 'a'; end;
    if( isempty(method) );
        method = 'a'; end;

    assert( isempty(spec) );
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
