function [ c, r ] = ipa_construct_better_candidate( plane_test_info, bv )
    oo = plane_test_info{2};
    tree_idx = plane_test_info{3};
    oo_root = bv.cyclictree.ordering{tree_idx};
    assert( size(oo_root, 2) == 1 )
    assert( size(oo, 2) == 1 )
    if( numel(oo_root) < numel(oo) );
        oo_root(numel( oo )) = 0;
    elseif( numel(oo) < numel(oo_root) );
        oo(numel( oo_root )) = 0; end;
    idx = find( oo ~= oo_root, 1 );
    a = [oo_root( idx:end ); oo_root( 1:idx-1 )];
    b = oo(idx:end);
    G = bv.cyclictree.graph{1};
    [c, ~, ~, r] = findsmp_ab( bv.cyclictree.Mt, G, a, b, 'verbose',bv.param.verbose - 1 );
end
   
function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
