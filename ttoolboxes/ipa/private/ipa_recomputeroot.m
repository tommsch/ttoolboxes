function [ bv ] = ipa_recomputeroot( bv );   
% blockvar = ipa_recomputeroot( blockvar ); 
% This function belongs to ipa!
% Removes vertices from the cyclic root, which are already inside of the polytope.
%
% Written by tommsch, 2018

    % recompute all pn-estimates
    VV = [bv.cyclictree.VV{:}];  % get all vertices
    component_idx = [bv.cyclictree.component_idx{:}];
    if( isempty(component_idx) );
        component_idx = true( 1, size(VV, 2) ); end;
    
    % choose all vertices
    if( size(VV, 2)^2 > 100000 || ...
        bv.param.verbose >= 2 ...
      );
        vprintf( 'Recompute vertices of root. Number of vertices: %i. \n', size(VV, 2) );end;
    nrm = zeros( 3, size(VV, 2) );
    for c = 1:bv.cyclictree.num_component;
        idx_c = component_idx == c;
        VV_c = VV(:,idx_c);
        nrm_j = zeros( 3, size(VV_c, 2) );
        for j = 1:size( VV_c, 2 );
            % even for case 'k' we do not pass the auxiliary data, because the provided center will be wrong, if we just use a subset of the vertices
            nrm_j(:,j) = polytopenorm( VV_c(:,j), VV_c(:,[1:j-1 j+1:end]), bv.cyclictree.algorithm, 'numcore',bv.param.num_core, 'v',0, bv.param.solver ); end;
        nrm(:,idx_c) = nrm_j; end;
    idx_nrm = nrm(2,:) < 1;  % be on the safe side
    
    if( any(idx_nrm) ); 
        vprintf( 'Removed %i vertices. \n', nnz(idx_nrm), 'imp',[1 bv.param.verbose] );
        bv.cyclictree.norm = savetocellarray( nrm(:,idx_nrm), idx_nrm, bv.cyclictree.norm ); end;
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
