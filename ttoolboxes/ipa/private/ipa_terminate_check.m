function [ bv ] = ipa_terminate_check( bv );
% Tests whether ipa shall terminate.
% Does not check whether there are no remaining vertices left.
% That check is done at a different location in the code
%
% This function belongs to ipa
% Written by: tommsch, 2018
    bv = ipa_execute_callback( bv, bv.param.callback.terminate_check_begin ); if( bv.log.errorcode ); return; end;

    nrm = [bv.cyclictree.norm{:}];
    if( false );
    elseif( isequal( bv.param.minmax, 'kone' ) && ...
            ~iskone( [bv.cyclictree.VV{:}] ) ...
          );
        bv.log.errorcode = ipa_errorcode.IS_NOT_A_KONE;

    elseif( bv.param.validateupperbound_norm > bv.cyclictree.norm_lvl(end) ...
          );
        bv.log.errorcode = ipa_errorcode.VALIDATEUPPERBOUNDNORM;

    elseif( bv.param.validatelowerbound < bv.cyclictree.lambda && ...
            bv.param.norestart < 1 ...
          );
        bv.log.errorcode = ipa_errorcode.VALIDATELOWERBOUND;

    elseif( bv.param.validateupperbound > max(bv.JSR) && ...
            bv.param.norestart < 1 ...
          );
        bv.log.errorcode = ipa_errorcode.VALIDATEUPPERBOUND;

    elseif( toc( bv.log.starttime ) >= bv.param.maxtime && ...
            bv.param.norestart < 1 ...
          );
        bv.log.errorcode = ipa_errorcode.MAXTIMEREACHED;

    elseif( toc( bv.log.starttreetime ) >= bv.param.maxtreetime && ...
            bv.param.norestart < 1 ...
          );
        bv.log.errorcode = ipa_errorcode.MAXTREETIMEREACHED;

    elseif( bv.log.iteration >= bv.param.maxiteration && ...
            bv.param.norestart < 1 ...
          );
        bv.log.errorcode = ipa_errorcode.MAXITERATION;

    elseif( bv.log.num_stepsmall >= bv.param.maxstepnumber && ...
            bv.param.norestart < 1 ...
          );
        bv.log.errorcode = ipa_errorcode.MAXSTEPNUMBERREACHED;

    elseif( nnz( nrm(2,:) > 1 ) >= bv.param.maxnum_vertex && ...
            bv.param.norestart < 1 ...
          );
        bv.log.errorcode = ipa_errorcode.MAXNUM_VERTEXREACHED;

    elseif( nnz( isnan(nrm(2,:)) ) > bv.param.maxremainingvertex );
        bv.log.errorcode = ipa_errorcode.MAXREMAININGVERTEXREACHED;

    elseif( bv.log.num_normerr >= bv.param.maxnormerr && ...
            bv.param.norestart < 1 ...
          );
        bv.log.errorcode = ipa_errorcode.MAXNORMERR ;

    elseif( bv.param.maxonenorm < inf && ...
            isfinite( bv.param.epssym ) && ...
            nnz( nrm(2,:) > 1 & nrm(3,:) < 1 ) > bv.param.maxonenorm+2*numel( bv.cyclictree.smpflag ) && ... 
            bv.param.norestart < 1 ...
          );
        % this does not strike for case (K) when candidates are pruned during the computation
        bv.log.errorcode = ipa_errorcode.MAXONENORM;

    elseif( nnz( abs( cummin( bv.cyclictree.norm_lvl ) - 1 ) <= 1e-5 ) > bv.param.maxonenorm+2*numel( bv.cyclictree.smpflag ) );  % XX epspolytope was here. We need eps_polytopenorm( algorithm ) actually here
        bv.log.errorcode = ipa_errorcode.MAXONENORM;

    elseif( ~isempty(bv.param.stale_norm) );
        assert( numel(bv.param.stale_norm) <= 2, 'ipa:option', 'Wrong value for option ''stale_norm''.' );
        if( bv.param.stale_norm(1) >= 1 && ...
            any( diff(bv.cyclictree.norm_lvl) < -1e-12 & diff(bv.cyclictree.norm_lvl) > -inf ) && ...
            nnz( bv.cyclictree.norm_lvl(end) == bv.cyclictree.norm_lvl ) > bv.param.stale_norm(1) ...
          );
            bv.log.errorcode = ipa_errorcode.STALENORM; end;
        if( numel(bv.param.stale_norm) == 2 && bv.param.stale_norm(2) >= 1 && ...
            nnz( bv.cyclictree.norm_lvl(end) - bv.cyclictree.norm_lvl > 0 ) >= bv.param.stale_norm(2) ...
          );
            bv.log.errorcode = ipa_errorcode.STALENORM; end;
        end;
    
    % dangerous problems
    if( strcmpi( bv.cyclictree.algorithm(1), 'l' ) && ...
        min( sum(abs([bv.cyclictree.VV{:}]), 1) ) < 1e-9 ...
      );
        if( bv.param.norestart <= 1 );
            bv.log.errorcode = ipa_errorcode.UNDERFLOW;
            
        else;
            if( ~ipa_persistent_var( 'underflow', 1 ) );
                vprintf( '\nPotential underflow occured. Results may be wrong.\n', 'imp',[0 bv.param.verbose], 'cpr','err' );
                bv.log.error = vprintf( '\nPotential underflow occured. Results may be wrong.\n', 'imp',[0 bv.param.verbose], 'str',bv.log.error, 'npr',1 ); end; end; end;

    bv = ipa_execute_callback( bv, bv.param.callback.terminate_check_end ); if( bv.log.errorcode ); return; end;
    
    % Test for successfull termination is done at a different place
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
