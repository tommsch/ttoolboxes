function ipa_printerrortext( var )
% This function belongs to ipa
% Prints out information in the fields .errortext
%
% Written by: tommsch, 2019

    if( var.param.verbose >= 0 && ~isempty(var.log.error) );
        vprintf( '\nImportant Messages: \n===================\n General Errors:\n----------------\n', 'cpr','err' );
        vprintf( '%s', var.log.error, 'cpr','err' ); end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>