function [ ba, bc ] = complexfunct_d_helper_old( varargin );

    [verbose, varargin] = parsem( {'verbose','v'}, varargin, 1 );
    
    a = varargin{1};
    c = varargin{2};
    
    assert( norm(a) > 1 && norm(c) >= 1, 'complexfunct_d_helper:b', 'Point ''a'' or ''c'' is strictly inside of unit circle (but should not be).' );
    assert( isequal(numel(a),numel(c),2), 'complexfunct_d_helper:dimension', 'Dimension of input variables is wrong. Dimension must be 2.' );    
    
    if( isrow(a) ); 
        a = a.'; end;
    if( isrow(c) ); 
        c = c.'; end;
    
    laa = sqrt( a(1)^2 + a(2)^2 - 1 );
    lac = sqrt( c(1)^2 + c(2)^2 - 1 );
    
    na = norm( a, 2 );
    nc = norm( c, 2 );
    
    na2 = na^2;
    nc2 = nc^2;
    
    
    va1 = (-laa*a+[a(2);-a(1)])/na2;
    va2 = (-laa*a-[a(2);-a(1)])/na2;
    vc1 = (-lac*c+[c(2);-c(1)])/nc2;
    vc2 = (-lac*c-[c(2);-c(1)])/nc2;
    
    bx11 = ((a(2)*va1(1)*vc1(1) - (c(2)*va1(1) + a(1)*va1(2))*vc1(1) + c(1)*va1(1)*vc1(2))/(-va1(2)*vc1(1) + va1(1)*vc1(2)));
    by11 = (-c(2)*va1(2)*vc1(1) + a(2)*va1(1)*vc1(2) + (-a(1) + c(1))*va1(2)*vc1(2))/(-va1(2)*vc1(1) + va1(1)*vc1(2));
    b11 = [bx11;by11];
    b11(~isfinite(b11)) = inf;
    
    bx12 = ((a(2)*va1(1)*vc2(1) - (c(2)*va1(1) + a(1)*va1(2))*vc2(1) + c(1)*va1(1)*vc2(2))/(-va1(2)*vc2(1) + va1(1)*vc2(2)));
    by12 = (-c(2)*va1(2)*vc2(1) + a(2)*va1(1)*vc2(2) + (-a(1) + c(1))*va1(2)*vc2(2))/(-va1(2)*vc2(1) + va1(1)*vc2(2));
    b12 = [bx12;by12];
    b12(~isfinite(b12)) = inf;
    
    bx21 = ((a(2)*va2(1)*vc1(1) - (c(2)*va2(1) + a(1)*va2(2))*vc1(1) + c(1)*va2(1)*vc1(2))/(-va2(2)*vc1(1) + va2(1)*vc1(2)));
    by21 = (-c(2)*va2(2)*vc1(1) + a(2)*va2(1)*vc1(2) + (-a(1) + c(1))*va2(2)*vc1(2))/(-va2(2)*vc1(1) + va2(1)*vc1(2));
    b21 = [bx21;by21];
    b21(~isfinite(b21)) = inf;
    
    bx22 = ((a(2)*va2(1)*vc2(1) - (c(2)*va2(1) + a(1)*va2(2))*vc2(1) + c(1)*va2(1)*vc2(2))/(-va2(2)*vc2(1) + va2(1)*vc2(2)));
    by22 = (-c(2)*va2(2)*vc2(1) + a(2)*va2(1)*vc2(2) + (-a(1) + c(1))*va2(2)*vc2(2))/(-va2(2)*vc2(1) + va2(1)*vc2(2));
    b22 = [bx22;by22];
    b22(~isfinite(b22)) = inf;
    
    
    idx1_ = norm(b11)<norm(b12);
    idx2_ = norm(b21)<norm(b22);
    
    b1_ = tif( idx1_, b11, b12 ); %with va1
    b2_ = tif( idx2_, b21, b22 ); %with va2
    
    idx__ = norm(a-b1_) + norm(c-b1_) < norm(a-b2_) + norm(c-b2_);
    if( idx__ == 0 );
        vc = vc1;
        if( idx2_ == 0 );
            va = vc1;
        else;
            va = va2; end;
    else;
        vc = vc2; 
        if( idx1_ == 0 );
            va = va1;
        else;
            va = va2; end; end;
    
    bt = tif( idx__, b1_, b2_ );
    b = bt/norm(bt);
    
    assert( norm(imag(va))<1e-8 && norm(imag(vc))<1e-8 && norm(imag(b))<1e-8, 'complexfunct_d_helper:numeric', 'Sever numerical problems in computation' );
    expect( norm(imag(va))<1e-12 && norm(imag(vc))<1e-12 && norm(imag(b))<1e-8, 'complexfunct_d_helper:numeric', 'Numerical problems in computation' );
    va = real( va );
    vc = real( vc );
    b = real( b );
    
    
    
    vb = [b(2); -b(1)];
    
    ba = [(a(2)*va(1)*vb(1) - (b(2)*va(1) + a(1)*va(2))*vb(1) + b(1)*va(1)*vb(2))/((-va(2))*vb(1) + va(1)*vb(2)); ((-b(2))*va(2)*vb(1) + a(2)*va(1)*vb(2) + (-a(1) + b(1))*va(2)*vb(2))/((-va(2))*vb(1) + va(1)*vb(2))];
    bc = [(c(2)*vc(1)*vb(1) - (b(2)*vc(1) + c(1)*vc(2))*vb(1) + b(1)*vc(1)*vb(2))/((-vc(2))*vb(1) + vc(1)*vb(2)); ((-b(2))*vc(2)*vb(1) + c(2)*vc(1)*vb(2) + (-c(1) + b(1))*vc(2)*vb(2))/((-vc(2))*vb(1) + vc(1)*vb(2))];
    
    if( verbose >= 1 );
        if( ~ishold );
            clf; end;
        plotm( [1;1i], 'funct','c', 'hold',2 ); 
        plotm( [a], 'ro', 'hold',2 );
        plotm( [c], 'bo', 'hold',2 );
        plotm( [b bt], 'ko', 'hold',2 );
        plotm( [a bt], 'r-', 'hold',2 );
        plotm( [c bt], 'b-', 'hold',2 );
        plotm( [[0;0] 2*b], 'k-', 'hold',2 );
        plotm( [b ba], 'r-', 'hold',2 );
        plotm( [b bc], 'b-', 'hold',2 );
        axis equal;
end;

assert( a'*b<1 && c'*b<1, 'complexfunct_d_helper:position', 'Points are in bad position' );


end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*NBRAK,*NBRAK2>
