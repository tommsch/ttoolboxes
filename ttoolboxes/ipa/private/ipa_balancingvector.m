function [ balancingvector, alpha, str ] = ipa_balancingvector( ct, param )
% [ balancingvector, alpha, str ] = ipa_balancingvector( M, cyclictree, param )
% This function belongs to ipa
% Input:
%   blockvar        blockvar.param struct from ipa, containing
%                       .param.autoextravertex
%                       .param.balancingdepth
%                       .param.verbose
%   cyclictree      cyclictree struct from ipa, c
%
% Output:
%   vec        balancing vector.
%   alpha      indices if balancing succeeded.                
%                   alpha >  1     the balancing succeeded.
%                   alpha <= 1     balancing failed
%
% Written by tommsch, 2018

    Mt = ct.Mt;
    str = '';
    num_o =  numel( ct.smpflag );
    num_c =  nnz( ct.smpflag == ipa_constant.candidate );
    num_r =  nnz( ct.smpflag == ipa_constant.realvertex );
    num_nc = nnz( ct.smpflag == ipa_constant.nearlycandidate );
    num_e =  nnz( ct.smpflag == ipa_constant.extravertex );
    J = numel( Mt );
    dim = size( Mt{1}, 1 ); 
    bal = cell( 1, num_o );
    
    if( num_o > 0 && num_c == 0 );
        balancingvector = 1;
        alpha = ipa_constant.UNSET_BALANCING_ALPHA;  % this is an arbitrary number in (1,inf)
        return; end;

    baldepth = tif( isempty(param.balancingdepth), min(15, ceil(log(1000)/log(J))), param.balancingdepth );
    str = vprintf( 'Balance %i Trees. ',num_o, 'imp',[1 param.verbose], 'str',str );
    str = vprintf( 'Balancing depth: %i - ',baldepth, 'imp',[2 param.verbose], 'str',str );
    str = vprintf( '\n', 'imp',[3 param.verbose], 'str',str );
    Mlift = liftproductt( double(Mt), baldepth );
    v0 = ct.v0;
    v0s = ct.v0s;
    if( param.num_core >= 1 );
        parfor( i = 1:num_o, param.num_core );
            val3 = zeros( dim, numel(Mlift) );
            for j = 1:numel( Mlift );
                val3(:,j) = Mlift{j}*v0{i}; end;
            bal{i}.VV = val3;
            %bal{i}.v0 = v0{i};
            bal{i}.v0s = v0s{i}; end;
    else;
        for i = 1:num_o;
            val3 = zeros( dim, numel(Mlift) );
            for j = 1:numel( Mlift );
                val3(:,j) = Mlift{j}*v0{i}; end;
            bal{i}.VV = val3;
            %bal{i}.v0 = v0{i};
            bal{i}.v0s = v0s{i}; end; end;
    
    % test if v0s's are non-empty
    for i = 1:num_o;
        if( isempty(ct.v0s{i}) );
            balancingvector = ones( 1, num_o );
            alpha = inf;
            return; end; end;
    
    qij = zeros( num_o, num_c );
    if( param.num_core >= 1 );
        parfor( i = 1:num_o, param.num_core );
    %     for( i = 1:num_o );
            V_i = bal{i}.VV;
            for j = 1:num_c;
                vs_j = bal{j}.v0s; %#ok<PFBNS>
                val = 0;
                for k = 1:size( V_i, 2 );
                    val = max( val, abs(V_i(:,k).'*vs_j) ); end
                qij(i,j) = max( val ); end;  end;
    else;
        for i = 1:num_o;
    %     for( i = 1:num_o );
            V_i = bal{i}.VV;
            for j = 1:num_c;
                vs_j = bal{j}.v0s;
                val = 0;
                for k = 1:size( V_i, 2 );
                    val = max( val, abs(V_i(:,k).'*vs_j) ); end
                qij(i,j) = max( val ); end;  end; end;
    bij = -log( qij );
    
    % make sanity check
    idx = abs( bij ) > 9;
    if( any(idx) );
        bij(idx) = clamp( bij(idx), -9, 9 );
        warning( 'balancepolytope:numerical', 'Balancing failed due to numerical errors.' ); end;
    % we use the notation from Protasov 2016, p28

    y = zeros( 1, num_c+1 ); 
    y(1) = -1;  % objective function

    [A, b] = y0yiyj_matrix( num_c, bij );
    lb = -inf( 1, num_c+1 ); 
    lb(1+num_c+1:end) = -inf;
    ub = inf( 1, num_c+1 );
    ub(1+num_c+1:end) = 0;
    
    idx = any( ~isfinite([A b]), 2 );
    if( anym(idx) );
        if( ~isequal(param.balancing, -1 ) );
            warning( 'balancepolytope:naninf', 'Balancing failed: Nan/Inf occured during computation of balancing vector.' ); end;
        alpha = 0;
        balancingvector = ones( 1, num_o+1 );
        return; end;
        
    if( num_c == 0 );
        balancingvector = ones( 1, num_o );
        alpha = ipa_constant.UNSET_BALANCING_ALPHA;
    elseif( num_c == 1 );
        balancingvector = 1;
        alpha = ipa_constant.UNSET_BALANCING_ALPHA;
    else;    
        try;
            opts = optimoptions( @linprog, 'Display','off', 'Algorithm','dual-simplex' );
            %[vec, ~, ~] = linprog( y, A, b, [], [], [], [], [], opts );    
            [vec, ~, ~] = linprog( y, A, b, [], [], lb, ub, [], opts );    
        catch me; try;  %#ok<NASGU>
            % try gurobi
            evalc( '[vec, ~, ~] = gurobi_linprog( y, A, b, [], [], lb, ub, [] );' ); 
        catch me;  %#ok<NASGU>
            evalc( '[vec, ~, ~] = sedumi_linprog( y, A, b, [], [], lb, ub, [] );' ); 
            end; end;
        vec = exp( vec );
        alpha = vec(1);
        balancingvector = vec(2:end); 
        if( issym(Mt{1}) );
            [N, D] = rat( balancingvector );
            balancingvector = sym( N )./sym( D ); end; end;

    % XX incorporate factors into LP program
    rho_vals = (ct.rho_ordering(num_c+1:num_c+num_nc).')./(max (ct.rho_ordering )*max( qij(num_c+num_r+1:num_c+num_r+num_nc,:),[], 2 ));
    nearlycand_factor = 0.99 * min( 1, rho_vals );
    balancingvector(num_c+num_r+1:num_c+num_r+num_nc,1) = nearlycand_factor;
    
    nearlycand_factor = param.autoextravertex .* min( 1, 1./max( qij(num_c+num_r+num_nc+1:end,:), [], 2 ).' );  
    idx = abs( nearlycand_factor ) < 1e-5 | abs( nearlycand_factor ) > 1e5;
    if( any(idx) );
        nearlycand_factor(idx) = clamp( nearlycand_factor(idx), 1e-5, 1e5 );
        warning( 'balancepolytope:numerical', 'Balancing failed due to numerical errors. Some balancing factors are clamped.' ); end;
    balancingvector(num_c+num_r+num_nc+1:num_c+num_r+num_nc+num_e, 1) = nearlycand_factor;  % make factors for extra-vertices
        
end

function [ A, B ] = y0yiyj_matrix( maxj, bij );
    L = maxj + 1;
    A = zeros( maxj^2 + 4*maxj, L );
    B = zeros( maxj^2 + 4*maxj, 1 );
    rowidx = 1;
    for i = 2:maxj+1;
        for j = 2:maxj+1;
            if( i == j ); 
                continue; end;
            A(rowidx,1:L) = zeros( 1, L ); 
            A(rowidx,1) = 1;
            A(rowidx,i) = 1;
            A(rowidx,j) = -1;
            B(rowidx,1) = bij(i - 1,j - 1);
            rowidx = rowidx + 1; end; end;
    A(rowidx:end,:) = [];
    B(rowidx:end,:) = [];
end

function dummy; end  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
