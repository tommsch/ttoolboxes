function [ log ] = ipa_write_logs( bv );
    log = bv.log;
    log.totaltime = toc( log.starttime );
    log.treetime = toc( log.starttreetime );
    vprintf( '\n', 'imp',[1 bv.param.verbose] ); 
    vprintf( 'Total time: %f s\n',log.totaltime, 'imp',[2 bv.param.verbose] ); 
    vprintf( 'Time used for building the tree: %f s\n',log.treetime, 'imp',[2 bv.param.verbose] ); 
    vprintf( 'Number of steps conducted/including steps in linprog: %i/%.3gM\n',log.num_stepsmall,log.num_stepbig/1000000, 'imp',[2 bv.param.verbose] ); 
    val = [bv.cyclictree.norm{:}];
    log.numberofvertex = nnz( val(2,:) > 1 );
    vprintf( 'Number of vertices of polytope: %i\n',log.numberofvertex, 'imp',[1 bv.param.verbose] );
    switch bv.param.minmax;
        case 'kone';
            idx = bv.cyclictree.smpflag == ipa_constant.candidate;
            vprintf( 'Products which were used to construct kone: \n%v\n',bv.cyclictree.ordering(idx), 'imp',[1 bv.param.verbose] );
        case 'min';
            vprintf( 'Product which gives upper of LSR: \n%v\n',bv.cyclictree.ordering{1}, 'imp',[1 bv.param.verbose] );
        case 'max'; 
            vprintf( 'Product which gives lower bounds of JSR: \n%v\n',bv.cyclictree.ordering{1}, 'imp',[1 bv.param.verbose] );
        otherwise;
            fatal_error; end; 
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
