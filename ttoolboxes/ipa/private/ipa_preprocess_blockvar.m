function [ bv, var ] = ipa_preprocess_blockvar( m, bv, var );
    if( var.param.numblock > 1 );
         vprintf( '=============================================\n=============================================\n=============================================\n', 'cpr',[0.0 0.3 0.3], 'imp',[3 var.param.verbose] );
         vprintf( '=============================================\nCompute Block: %i ' ,m, 'cpr',[0.0 0.3 0.3], 'imp',[1 var.param.verbose] ); 
         vprintf( 'Input: %i %s of dimension %i\n', ...
                  numel( bv{m}.param.M ), tif( numel( bv{m}.param.M ) > 1, 'matrices', 'matrix' ), ...
                  bv{m}.param.dim, 'imp',[1 var.param.verbose] );end;
    
    vprintf( '\n', 'imp',[1 var.param.verbose] );
    valmin = max( cellfun(@(x) min(x.JSR), bv) );  % blockvar is always non-empty
    valmax = max( cellfun(@(x) max(x.JSR), bv) );
    if( valmin < var.JSR(1) || ...
        valmax > var.JSR(2) ...
      );
        vprintf( 'Either wrong value via option ''JSR'' given or wrong computation.\n', 'cpr','err', 'imp',[1 var.param.verbose] );
        var.log.error = vprintf( 'Either wrong value via option ''JSR'' given or wrong computation.\n', 'str',var.log.error, 'npr' ); end;
    var.JSR(1) = max( valmin, var.JSR(1) );
    var.JSR(2) = min( valmax, var.JSR(2) );
    bv{m}.param.validateupperbound = var.JSR(1)*(1 - eps);
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>