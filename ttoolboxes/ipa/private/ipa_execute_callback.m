function [ bv ] = ipa_execute_callback( bv, func, varargin );
% boilerplate code for callback functions
% [ bv ] = ipa_execute_callback( bv, func, position );
%
% Input:
%   bv          the blockvar struct
%   func        function handle
%
% See also: ipa_callback_add_candidate
%           ipa_callback_prune_root
%           ipa_callback_dual_kone_termination_criteria
%           ipa_callback_eigenplane_criteria
%           ipa_callback_remove_matrices_based_on_rho
%           ipa_callback_cleanup_nfo
%
% Written by: tommsch, 2024-10-01

%               2024-10-01, tommsch,    Added callbacks: ipa_callback_add_candidate,
%                                                        ipa_callback_dual_kone_termination_criteria,
%                                                        ipa_callback_eigenplane_criteria
%               2024-10-16, tommsch,    Added callbacks: ipa_callback_prune_root
%                                                        ipa_callback_remove_matrices_based_on_rho
% Changelog:    2024-12-18, tommsch,    Added callbacks: ipa_callback_cleanup_nfo
%               2025-01-15, tommsch,    Bugfix for cases where anonymous function is used as callback
    
    if( isempty(func) );
        return; end;

    if( ~iscell(func) );
        func = {func}; end;

    for i = 1:numel( func );
        switch nargin( func{i} );
            case 0; argin = {};
            case 1; argin = {bv};
            otherwise; argin = {bv varargin{:}}; end;  %#ok<CCAT>
        
            switch nargout( func{i} );
                case -1;
                    msg_ans = 'ttest_8419428uriafj892hkjadgcmxvy9032zhjksada44qt4g5asfdg1as6448q64315a30u15biks9843jhf9e03954h';
                    ans = msg_ans;  %#ok<NOANS>
                    func{i}( argin{:} );
                    if( ~isequal(ans, msg_ans) );  %#ok<NOANS>
                        bv = ans; end;  %#ok<NOANS>
                case 0;
                    func{i}( argin{:} )
                case 1;
                    bv = func{i}( argin{:} );
                otherwise;
                    fatal_error; end; end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
