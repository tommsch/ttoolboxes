function [ normval, iteration, ptstotest, num_normerr ] = ipa_compute_norms( bv, pts_idx, pts_component, VV, VV_idx, VV_component );
% computes the norms
% this function belongs to ipa

    num_normerr = bv.log.num_normerr;
    ptstotest = [bv.cyclictree.VV{:}];
    ptstotest = ptstotest(:,pts_idx);

    if( ~bv.param.alwaysout );
        normval = zeros( 3, numel(pts_component) );
        iv = cell( 1, numel(pts_component) );
        iteration = zeros( 1, bv.cyclictree.num_component );
        normtime = zeros( 1, bv.cyclictree.num_component );
        proofed = zeros( 3, numel(pts_component) );

        for p = 1:bv.cyclictree.num_component;
            pidx = pts_component == p;
            Vidx = VV_component == p;
            if( bv.cyclictree.epssym >= 0 );
                len = cellfun( 'size', bv.cyclictree.norm, 2 );
                tree_idx = [];
                for i = 1:numel( len )
                    tree_idx = [tree_idx repmat( i, [1 len(i)] )]; end;  %#ok<AGROW>
                oo = merge_oo( bv.cyclictree.oo{:} );
                pts_sym{1}  = [-tree_idx(pts_idx); oo(:,pts_idx)];  % first line: tree_idx, afterwards: ordering
                pts_sym{1} = pts_sym{1}(:,pidx);  
                pts_sym{2} = bv.cyclictree.Mt_sym;  % Mt
                pts_sym{3} = bv.cyclictree.v0_sym;  % v
                pts_sym{4} = 1;  % tif( strcmpi(bv.cyclictree.algorithm(1), 'k'), 1, bv.cyclictree.lambda_sym );  % scaling

                VV_sym{1} = [-tree_idx(VV_idx); oo(:,VV_idx)];
                VV_sym{1} = VV_sym{1}(:,Vidx);
                VV_sym{2} = pts_sym{2};
                VV_sym{3} = pts_sym{3};
                VV_sym{4} = 1;  % tif( strcmpi(bv.cyclictree.algorithm(1), 'k'), 1, bv.cyclictree.lambda_sym );
            else;
                pts_sym = {};
                VV_sym = {}; end;
            vprintf( '(num) ', 'imp',[1 bv.param.verbose] );
            [ normval(:,pidx), iv(pidx), iteration(p), normtime(p), proofed(:,pidx) ] = polytopenorm( ptstotest(:,pidx), VV(:,Vidx), bv.cyclictree.polytopenorm.auxiliary_data{:}, ...
                                                                                                      bv.cyclictree.algorithm, bv.param.solver, ...
                                                                                                      'pts',pts_sym, 'VV',VV_sym, ...
                                                                                                      'numcore',bv.param.num_core, 'v',bv.param.verbose, ...
                                                                                                      'bound',tif( bv.param.fastnorm, [0 1], [] ), ...  % XX This makes problem. If the norm is computed to be e.g. [3 inf 2], then the ub reported back to ipa is inf
                                                                                                       bv.param.debug{:} ...
                                                                                                      );
            normval(:,pidx) = real( normval(:,pidx) ); end;
        iteration = sum( iteration );
        idx = isAlways( normval(2,:) < 1 & proofed(2,:) );
        if( any(proofed(:)) );
            normval(1:3,idx) = ipa_constant.INSIDE_SYMBOLIC; end;
        if( bv.param.extrapath.maxdepth > 0 && ...
            size( VV, 2 ) >= bv.param.extrapath.minnum_vertex && ...
            bv.log.iteration >= bv.param.extrapath.mindepth ...
          );
            [ normval, iteration_e ] = ipa_extrapathpolytopenorm( ptstotest, normval, pts_idx, VV, bv ); 
            iteration = iteration + iteration_e; end;
        if( bv.log.iteration >= 1 );
            erridx = normval(3,:) < 1 & isinf( normval(2,:) ) | ...
                     normval(2,:) > 1 & isnan( normval(3,:) ) | ...
                     isnan( normval(1,:) );
            num_normerr = num_normerr + nnz( erridx ); end;
    else;
        normval = [inf( 2, size(ptstotest, 2) ); zeros( 1, size(ptstotest, 2) )];
        end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
