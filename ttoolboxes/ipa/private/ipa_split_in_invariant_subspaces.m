function [ MM, var ] = ipa_split_in_invariant_subspaces( var );
    vprintf( 'Input: %i matrices of dimension %i\n', numel( var.M_sym ), var.param.dim, 'imp',[1 var.param.verbose] );
    vprintf( 'Search for invariant subspaces\n', 'imp',[2 var.param.verbose] );
    [MM, basis, warningflag] = invariantsubspace( var.M_sym, var.param.invariantsubspace, 'v',var.param.verbose-2, 'sym',tif(isfinite(var.param.epssym), var.param.invariantsubspace_sym, 0) );  % search for invariantsubspaces
    switch var.param.minmax;
        case 'kone';
            if( ~isequal(var.param.invariantsubspace, 'none') && ~isequal(var.param.invariantsubspace, 0) );
                str = ['  Note: Since the lower-spectral-radius shall be computed, you should not enable the search for invariant subspaces.' newline];
                vprintf( '%s', str, 'cpr',[.7 .4 .1]', 'imp',[1 var.param.verbose] );
                var.log.error = vprintf( '%s', str, 'str',var.log.error, 'npr' ); end;
        case 'min';
            if( ~isequal(var.param.invariantsubspace, 'none') && ~isequal(var.param.invariantsubspace, 0) );
                str = ['  Note: Since the lower-spectral-radius shall be computed, you should not enable the search for invariant subspaces.' newline];
                vprintf( '%s', str, 'cpr',[.7 .4 .1]', 'imp',[1 var.param.verbose] );
                var.log.error = vprintf( '%s', str, 'str',var.log.error, 'npr' ); end;
        case 'max';
            % do nothing
        otherwise;
            fatal_error; end;

    if( isequal(var.param.invariantsubspace, 'none') || isequal(var.param.invariantsubspace, 0) );
        str = ['Search for invariant subspaces is disabled.' newline '  Note: If the matrices have invariant subspaces, the returned results may be plainly wrong.' newline];
        
        vprintf( '%s', str, 'cpr',[.7 .4 .1]', 'imp',[1 var.param.verbose] );
        var.log.error = vprintf( '%s', str, 'str',var.log.error, 'npr' ); end;
    if( warningflag );
        vprintf( 'Invariant subspaces found and matrices are badly scaled. Algorithm may behave wrong. Results are not reliable.\n', 'cpr',[.7 .4 .1]', 'imp',[2 var.param.verbose] );
        var.log.error = vprintf( 'Invariant subspaces found and matrices are badly scaled. Algorithm may behave wrong. Results are not reliable.\n', 'str',var.log.error, 'npr' ); end;
    var.param.numblock = size( MM, 2 );
    var.param.basis = basis;
    if( var.param.numblock > 1 );  % text output if subspaces are found
        if( ~allm(issymstrict(MM{1}{1})) && isfinite(var.param.epssym) );
            vprintf( 'Symbolic computation disabled due to the presence of invariant subspaces which could only numerically be comuted.\n', 'imp',[1 var.param.verbose] );
            var.param.epssym = nan; end;
        vprintf( 'Number of Blocks: %i\n', var.param.numblock, 'imp',[2 var.param.verbose] );
        vprintf( 'Basis: \n%v\n', basis, 'imp',[3 var.param.verbose], 'sze',[var.param.showquantity, var.param.dim+var.param.numblock] );
        vprintf( 'Blocks: \n%v\n', MM, 'imp',[3 var.param.verbose], 'sze',[var.param.showquantity, var.param.dim+var.param.numblock] );end;
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
