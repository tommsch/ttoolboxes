function M = buildsum_fast( A, oo, M );
% M = buildsum_fast( A, oo, v )
% Constructs the sum of matrices in the cell A, corresponding to the sequence in prod.
% M = A{prod(end)} + A{prod(end-1)} + ... + A{prod(1)}
%
% Input:
%   A       cell array of matrices
%   oo      ordering in which to add them
%
% Output:
%   M       the product
%
% Info:
%   if prod(i) = 0, A{oo(i)} is replaced by the zero matrix
%
% E.g.: buildsum_fast( {2 4 5}, [1 0 3] )
%
% See also: buildproduct_fast
% 
% Written by: Jungers, Taken from: JSR-Toolbox
% Changed by: tommsch, 2020

    if( ~iscell(A) );
        A = num2cell( A ); end;
    
    if( nargin == 2 );
        M = zeros( size(A{1}) ); end;
    
    for t = 1:length( oo );
        if( oo(t) > 0 );
            M = A{oo(t)} + M; 
            %M = simplify( M, 1 ); 
            end; end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
