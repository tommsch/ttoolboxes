function [ M, G ] = gallery_matrixset_old( varargin );
% [ M, G ] = gallery_matrixset_old( what, dim, N, [k], [options] )
% Returns sets of matrices, mostly used for ipa.
%
% Input:
%   what            string OR integer which controls the return value
%   dim             dimension of the returned value (if applicable), 
%   N               number of matrices which shall be returned
%   k               argument used in some cases
%
% Options:
%   'bool'          Returns matrices with values 0,1.
%   'int'           Returns integer matrices
%   'irreducible'   default = true, Returns only irreducible sets of matrices
%   'norm'          Returns normalized matrices s.t. all matrices have 2-norm equal to 1
%   'pathcomplete'  default = true, returns only pathcomplete graphs
%   'pos'           Returns positive matrices
%   'rho'           Returns normalized matrices s.t. all matrices have spectral radius equal to 1
%   'sparse',val    Returns matrices with sparsity val
%   'sym',val       Specifies whether symbolic matrices can also be returned
%                       val = -1;   double, vpa and symmatrices
%                       val = 0     (default) only double matrices
%                       val = 1     double and vpa matrices
%                       val = 0     double, vpa and symmatrices
%
%   'what',val      To specify dimension by name-value pair
%   'dim',val       Integer, to specify dimension by name-value pair
%   'N',val         Integer, to specify N by name-value pair
%   'k',val         To specify k by name-value pair
%                   If any of these is specified by a name value pair, the positional argument is ignored
%
%
% Further Options:
%   'seed',val      Seed for random number generator. 
%                   If seed is set, the random number generator has the same state before and after that function 
%   'verbose',val   Verbose level
%   'nocell'        returns only the first matrix. Undefined behaviour if N > 1.
%
% Output:    
%   M               Cell array of square matrices.
%   G               adjancy matrix of a directed graph, defines admissible matrix products
%
% Note:
% Most of the options preserve no special properties of the matrices. 
% For example: The entries in the matrices in gallery_matrixset('rand_gauss',10,2,'pos') do not have a Gaussian distribution anymore.
%   
%
% E.g.:  gallery_matrixset( 'rand_gauss', 5, 2, 'seed',100, 'rho' )
%
% Written by: tommsch, 2018
% For more information write to: <a href="tommsch@gmx.at">tommsch@gmx.at</a>

%            2020-07-08, tommsch,   Removed options 'orthog_x', 'binary2'
%                                   Removed matrix 'complex'
%            2022-02-21, tommsch,   Added matrices from Moeller Thesis, Bugfix, An example of GWZ05 was wrong
%                                   experimental: Made `what`, `dim`, `N` to optional arguments
%            2022-12-28, tommsch,   Added name-value pair options for `what`, `dim`, `N`, `k`
%            2023-03-22, tommsch,   Added output `G`
%            2023-06-02, tommsch,   Renamed function to gallery_matrixset
%                                   Added convenience wrapper gallery_matrix
%                                   Added Matrices: 'iota'/'consecutive', 'ones', 'constant', 'blktriangular', 'invariantsubspace', 'stochastic_pm', 'cycliccolumn'
%                                   Added postprocessing: 'fliplr', 'flipud', 'pinv', 'transpose', 'positive', 'nonnegative'
%                                   Added options: 'row', 'col'
% Changelog: 

% XX add house, integerdata, somewhat-stochastic matrices, stochastic, matrices with entries in [-1 1]

%#ok<*ALIGN>
%#ok<*AGROW>,

    [ M, G, opt, varargin ] = parse_input( nargout, varargin{:} );
    
    if( isempty(M) && isempty(G) );
        while( true );
            [M, G] = getmatrix( opt, varargin{:} );
            if( isempty(opt.what) );
                ret = invariant( M, G, opt, true ); 
                if( ~ret );
                    continue; end; end;
            if( isempty(M) );
                if( opt.verbose  >= 0 );
                    warning( 'gallery_matrixset:arg', 'No matrices returned. Probably ''what'' is a wrong string or type ''complex'' chosen.' ); end;
                break;
            else;
                break; end; end; end;

    [M, G] = postprocess( M, G, opt );

end

function [ M, G, opt, varargin ] = parse_input( nout, varargin );    
    
    [opt.negate, varargin]              = parsem( {'negate'}, varargin, [] );
    [opt.fliplr, varargin]              = parsem( {'fliplr'}, varargin, [] );
    [opt.flipud, varargin]              = parsem( {'flipud'}, varargin, [] );
    [flip, varargin]                    = parsem( {'flip'}, varargin );
    [opt.transpose, varargin]           = parsem( {'transpose'}, varargin, [] );
    [opt.noise, varargin]               = parsem( {'noise','perturbate'}, varargin, 0 );
    [opt.pinv, varargin]                = parsem( {'pinv','invert','inv'}, varargin, [] );
    [opt.positive, varargin]            = parsem( {'pos','positive'}, varargin );
    [opt.nonnegative, varargin]         = parsem( {'nonneg','nonnegative'}, varargin );
    [opt.symmetric, varargin]           = parsem( 'symmetric', varargin );
    [opt.antisymmetric, varargin]       = parsem( 'antisymmetric', varargin );
    [opt.bool, varargin]                = parsem( {'bool','boolean'}, varargin );
    [opt.continuous, varargin]          = parsem( {'continuous','cont'}, varargin );
    [opt.finite, varargin]              = parsem( 'finite', varargin );
    [opt.graph, varargin]               = parsem( 'graph', varargin, [] );
    [opt.int, varargin]                 = parsem( 'int', varargin );
    [opt.irreducible, varargin]         = parsem( {'irreducible','noinvariantsubspace' }, varargin, [] );
    [opt.invertible, varargin]           = parsem( {'invertible'}, varargin );  % given value is bound for condition number
    if( opt.invertible == 1 );
        opt.invertible = 100; end;
    [opt.invariantsubspace, varargin]    = parsem( {'invariantsubspace','subspace'}, varargin, [] );
    [opt.metzler, varargin]             = parsem( {'metzler','quasipositive','essentiallynonnegative'}, varargin );
    [opt.norm, varargin]                = parsem( 'norm', varargin );
    [opt.nocell, varargin]              = parsem( 'nocell', varargin, [] );
    [opt.pathcomplete, varargin]        = parsem( 'pathcomplete', varargin, [] );
    [opt.pos, varargin]                 = parsem( 'pos', varargin );
    [opt.rank, varargin]                = parsem( 'rank', varargin, [] );
    [rankone, varargin]                 = parsem( {'rankone','rank1'}, varargin );
    if( isempty(opt.rank) && rankone == 1 );
        opt.rank = 1; end;
    [opt.real, varargin]                = parsem( 'real', varargin );
    [opt.rho, varargin]                 = parsem( 'rho', varargin );
    [opt.seed, varargin]                = parsem( 'seed', varargin, [] );
    [opt.sparsity, varargin]            = parsem( 'sparse', varargin, 0 );
    [opt.sym, varargin]                 = parsem( 'sym', varargin, 0 );    
    [opt.triu, varargin]                = parsem( {'triangular','triu','triangularupper','uppertriangular'}, varargin, [] );
    [opt.tril, varargin]                = parsem( {'tril','lowertriangular','triangularlower'}, varargin, [] );
    [opt.verbose, varargin]             = parsem( {'verbose','v'}, varargin, 1 );
    
    [opt.k, varargin]                   = parsem( {'k','arg'}, varargin, [] );
    [opt.N, varargin]                   = parsem( {'J','N','nummatrix'}, varargin, [] );
    [opt.dim, varargin]                 = parsem( {'dim','dimension'}, varargin, [] );
    [opt.row, varargin]                 = parsem( {'row','r'}, varargin, [] );
    [opt.col, varargin]                 = parsem( {'col','c'}, varargin, [] );
    [opt.what, varargin]                = parsem( 'what', varargin, [] );
    
    [G, varargin]                       = parsem( {'G','graph'}, varargin, [] );

    if( ~isnan(str2double( opt.k )) );
        opt.k = str2double( opt.k ); end;      
    
    if( ~isempty(opt.N) && isempty(opt.nocell) );
        opt.nocell = (opt.N == 1); end;
    
    if( ~isnan(str2double( opt.N )) );
        opt.N = str2double( opt.N ); end;    
    
    if( ~isnan(str2double( opt.dim )) );
        opt.dim = str2double( opt.dim ); end;
    
    if( numel( varargin ) >= 1 && ...
        iscell( varargin{1} ) && ...
        all( cellfun( @issquare, varargin{1} ) ) && ...
        all( cellfun( @isnumeric, varargin{1} ) ) && ...
       ~any( diff( cellfun('prodofsize', varargin{1} ) ) ) ...
      );
        M = varargin{1};
        varargin(1) = []; 
    else;
        M = []; end;   
    
    if( isempty(opt.pathcomplete) );
        opt.pathcomplete = nout > 1; end;
    
    if( isempty(opt.irreducible));
        if( ~isempty(opt.N) );
            opt.irreducible = opt.N ~= 1;
        else;
            opt.irreducible = 1; end; end;
    
    if( ~isempty(opt.what) );
    elseif( numel(varargin) >= 1 );
        opt.what = varargin{1}; 
        varargin(1) = []; end;  % old default value what = []
    
    if( isempty(opt.row) );
        opt.row = opt.dim(1); end;
    if( isempty(opt.col) );
        opt.col = opt.dim(end); end;
    if( isempty(opt.dim) );
        if( ~isempty(opt.row) );
            opt.dim = max( [opt.dim opt.row] );
        elseif( ~isempty(opt.col) );
            opt.dim = max( [opt.dim opt.col] ); end; end;
    
    if( flip );
        opt.flipud = flip;
        opt.fliplr = flip; end;
    
    if( ~isempty(opt.rank) );
        opt.finite = true; end;
    
    if( isempty(opt.what) );
        if( isempty(opt.flipud) );
            opt.flipud = rand; end;
        if( isempty(opt.fliplr) );
            opt.fliplr = rand; end;
        if( isempty(opt.transpose) );
            opt.transpose = rand; end;
        if( isempty(opt.pinv) );
            opt.pinv = rand; end;
        end;
    
    if( opt.positive );
        opt.nonnegative = true; end;    
    
    if( ~isempty(opt.seed) );
        % initialize random number generator and store state
        oldrng = rng();
        opt.cleanrng = onCleanup( @() rng(oldrng) );
        if( isnumeric(opt.seed) );
            rng( mod(opt.seed, 2^30) ); 
            rand( 1000 );
        else
            rng( opt.seed );  end; end;
    
end

function [ M, G ] = postprocess( M, G, opt );

    J = numel( M );
    
    % at first we have to do processings which may change the size of the matrices
    
    if( ~isempty(opt.invariantsubspace) );
        B = gallery_matrix( 'dim', size(M{1}, 1), 'invertible',2 );
        if( isequal(opt.invariantsubspace, 1) );
            lengths = randsum( size(M{1}, 1) );
        elseif( isscalar(opt.invariantsubspace) );
            lengths = randsum( size(M{1}, 1), opt.invariantsubspace );
        elseif( issquare(opt.invariantsubspace) );
            lengths = randsum( size(M{1}, 1) );
            B = opt.invariantsubspace;
        elseif( isvector(opt.invariantsubspace) );
            lengths = opt.invariantsubspace; end;
        clengths = cumsum( lengths );
        mask = ones( size(M{1}, 2) );
        for l = 1:numel( lengths ) - 1;
            mask((clengths(l) + 1):clengths(l+1),1:clengths(l)) = 0; end;
        for j = 1:J;
            M{j} = B\(mask.*M{j})*B; end;
        vprintf( 'Sizes of subspaces: %r\n', lengths );
        end;
    
    if( ~isempty(opt.transpose) && issquare(M{1}) );
        for j = 1:J;
            if( rand < opt.transpose );
                M{j} = M{j}.'; end; end; end;
    
    if( ~isempty(opt.pinv) && issquare(M{1}) );
        for j = 1:J;
            if( rand < opt.pinv );
                try;
                    M{j} = pinv( M{j} ); 
                catch me;  %#ok<NASGU>
                    end; end; end; end;
    
    if( ~isempty(opt.fliplr) );
        for j = 1:J;
            if( rand<opt.fliplr );
                M{j} = fliplr( M{j} ); end; end; end;
    
    if( ~isempty(opt.flipud) );
        for j = 1:J;
            if( rand < opt.flipud );
                M{j} = flipud( M{j} ); end; end; end;
    
    if( ~isempty(opt.col) || ~isempty(opt.row) );
        if( isempty(opt.row) );
            r = size( M{1}, 1 ); 
        else;
            r = min( size(M{1}, 1), opt.row ); end;
        if( isempty(opt.col) );
            c = size( M{1}, 2 );
        else;
            c = min( size(M{1}, 2), opt.col ); end;
        for j = 1:J;
            M{j} = M{j}(1:r,1:c); end; end;
    
    % now processings follow which can come in an arbitrary order
    
    if( opt.noise );
        for j = 1:J;
            M{j} = M{j} +  norm( M{j}, 1 )*opt.noise*randn( size(M{j}) ); end; end;

    if( opt.nonnegative );
        for j = 1:J;
            M{j} = abs( M{j} ); end; end;
    
    if( opt.symmetric );
        for j = 1:J;
            M{j} = (M{j} + M{j}'); end; end;
    if( opt.antisymmetric );
        for j = 1:J;
            M{j} = (M{j} - M{j}'); end; end;    
    
    if( opt.sparsity );
        idx = randperm( opt.dim^2, round(opt.dim^2*opt.sparsity) );
        for j = 1:J;
            M{j}(idx) = 0; end; end;
    if( opt.metzler );
        for j = 1:J;
            d = diag( diag(M{j}) );
            M{j} = abs( M{j} - d ) + d; end; end;
    if( opt.bool );
        for j = 1:J; M{j} = double( logical(round(M{j})) ); end; end;
    if( opt.int ); 
        for j = 1:J; M{j} = round( M{j} ); end; end;    
    if( opt.pos ); 
        for j = 1:J; M{j} = abs( M{j} ); end; end;
    
    % these processings must be last
    % these options are not combinable in general
    
    if( ~isempty(opt.triu) );
        for j = 1:J;
            M{j} = triu( M{j}, opt.triu ); end; end;
    
    if( ~isempty(opt.tril) );
        for j = 1:J;
            M{j} = tril( M{j}, opt.tril ); end; end;    
    
    if( ~isempty(opt.rank) && opt.rank<min(size(M{1})) );
        for j = 1:J;
            [U,S,V] = svd( M{j} );
            M{j} = zeros( size(M{j}) );
            for r = 1:opt.rank;
                M{j} = M{j} + U(:,r) * S(r,r) * (V(:,r)'); end; end; end;    
    if( opt.rho ); 
        for j = 1:J;
            M{j} = M{j}/rhot( M{j} ); end; end;
    if( opt.norm ); 
        for j = 1:J;
            M{j} = M{j}/norm( M{j} ); end; end;
    if( ~isequal(opt.continuous, 0)  );
        [ M, G ] = make_continuous_set( M, G, opt ); end;
    if( opt.verbose  >= 0 );
        expect( ~(opt.int && opt.rho),  'gallery_matrixset:opt', '''int'' and ''rho'' together does not return integer matrices.' );
        expect( ~(opt.norm && opt.rho), 'gallery_matrixset:opt', '''rho'' and ''norm'' does not return matrices with norm=rho=1.\n' ); end;
    
    % type changes
    switch( opt.sym );
        case -1;
            % do nothing
        case 0;
            for i = 1:numel( M );
                M{i} = double(M{i}); end;
        case 1;
            for i = 1:numel( M );
                if( issymstrict(M{i}) );
                    M{i} = vpa(M{i}); end; end;
        case 2;
            % do nothing
            end;
    
    if( opt.nocell )
        if( ~isempty(M) );
            M = M{1}; 
        else;
            M = []; end; end;
end



%% lists
%%%%%%%%%%%%%%%%%%%

function [ M, G ] = getmatrix( opt, varargin );

    randflag = isempty( opt.what ) * randi( [-1 6] );
    G = [];
    M = {};
    if( randflag == 0 && isempty(M) || randflag == 1 );
        M = getmatrix_builder( opt, varargin{:} ); end;    
    if( randflag == 0 && isempty(M) || randflag == 2 );
        M = getmatrix_basic( opt, varargin{:} ); end;
    if( randflag == 0 && isempty(M) || randflag == 3 );
        M = getmatrix_random( opt, varargin{:} ); end;
    if( randflag == 0 && isempty(M) || randflag == 4 );
        M = getmatrix_random_matlab( opt, varargin{:} ); end;
    if( randflag == 0 && isempty(M) || randflag == 5 );
        M = getmatrix_papers( opt, varargin{:} ); end;
    if( randflag == 0 && isempty(M) || randflag == 6 );
        M = getmatrix_bad( opt, varargin{:} ); end;
    if( randflag == 0 && isempty(M) || randflag == 7 );
        M = getmatrix_properties( opt, varargin{:} ); end;
    if( randflag == 0 && isempty(M) || randflag == 8 );
        M = getmatrix_misc( opt, varargin{:} ); end;
    if( randflag == 0 && isempty(M) );
        [M, G] = getmatrix_continuous( opt, varargin{:} ); end;    
    if( randflag == 0 && isempty(M) );
        M = getmatrix_signal( opt, varargin{:} ); end;
    if( isempty(G) );
        G = getmatrix_graph( M, opt, varargin{:} ); end;    
    
    if( isempty(M) && ~isempty(opt.dim) && ~anym(opt.dim) );
        M = repmat( {[]}, 1, N ); 
        return; end;
        
end

function [ M ] = getmatrix_builder( opt, varargin );
    M = {};
    
    if( check(opt.what, {'builder'}) );
        opt.what = []; end;
    
    if( isempty(opt.dim) );
        opt.dim = randi( [2 10] ); end;
    if( isempty(opt.what) ); 
        opt.what = randi( [1 6] ); end;
    if( isempty(opt.N) );
        opt.N = randp( 1, 2, 4 ); end;     
    
    what = opt.what;
    if( false );
    elseif( check(what, {1,'blkdiag','blockdiag','blockdiagonal', ...
                         2,'blktri','blktriangular','blocktriangular', ...
                         3,'invariantsubspace','invariant'}) );
        blocks = randsum( opt.dim );
        for j = 1:opt.N;
            Block = cell( 1, numel(blocks) );
            for b = 1:numel(blocks);
                Block{b} = gallery_matrix( 'dim',blocks(b), varargin{:} ); end;
            if( check(what, {1,'blkdiag','blockdiag','blockdiagonal'}) );
                M{j} = blkdiag( Block{:} );
            else;
                M{j} = blktri( Block{:}, 'fillr' ); end; end;
        if( check(what, {3,'invariantsubspace','invariant'}) );
            while( true );
                B = 2*(randi( [0 1], opt.dim ) - .5);
                if( rank(B) == opt.dim );
                    break; end; end;
            for j = 1:opt.N;
                M{j} = B\M{j}*B; end; end;
    end
end

function [ M ] = getmatrix_basic( opt, varargin );
    if( check(opt.what, {'basic'}) );
        opt.what = []; end;
    if( isempty(opt.what) ); 
        opt.what = randi( [1 6] ); end;
    
    if( isempty(opt.N) );
        opt.N = randp( 1, 2, 4 ); end;    
    if( isempty(opt.dim) );
        opt.dim = randp( 1, 2, 25, 1/opt.N ); end;
    if( isempty(opt.row) );
        opt.row = opt.dim; end;
    if( isempty(opt.col) );
        opt.col = opt.dim; end;    
    if( isempty(opt.k) );
        opt.k = 0; end;
    
    what = opt.what;
    M = {};
    if( false );
    elseif( check(what, {1,'eye','identity'}) );
        for i = 1:opt.N;
            M{i} = eye( opt.row, opt.col ); end;
    elseif( check(what, {2,'1','one','ones'}) );
        for i = 1:opt.N;
            M{i} = ones( opt.row, opt.col ); end;
    elseif( check(what, {3,'constant'}) );
        if( numel(varargin) >= 1 );
            range = varargin{1};
        else;
            range = max( 9, opt.N ); end;
        if( isscalar(range) );
            range = [-abs(range) abs(range)]; end;
        for i = 1:opt.N;
            c = randi( range );
            M{i} = c * ones( opt.row, opt.col ); end;        
    elseif( check(what, {4,'0','zero','zeros'}) );
        for i = 1:opt.N;
            M{i} = zeros( opt.row, opt.col ); end;
    elseif( check(what, {5,'jordan','jord'}) );
        for i = 1:opt.N;
            M{i} = matrix_jordan( opt.row, opt.col ); end;
    elseif( check(what, {6,'consecutive','iota'}) );
        for i = 1:opt.N;
            M{i} = reshape( 1:(opt.row*opt.col), [opt.row opt.col] ); end;
    elseif( check(what, {7,'wedge','consecutive2','iota2'}) );
        X = zeros( opt.row, opt.col );
            
        for r = 1:opt.row;
            for c = 1:opt.col;
                if( numel(varargin) >= 1 );
                    X(r,c) = varargin{1}(r,c);
                else;
                    X(r,c) = max( r, c ); end; end; end;
        for i = 1:opt.N;
            M{i} = X; end;
    end
end

function [ M ] = getmatrix_random( opt, varargin );
    % Random matrices, need two arguments 'rand_mmm',<dim>,<number of matrices>
    % XX must be refactored, so that I get rid of the hardcoded number in the cases
    if( check(opt.what, {'random'}) );
        opt.what = []; end;
    if( isempty(opt.what) ); 
        opt.what = randi( [1 28] ); end;
    if( isempty(opt.N) );
        opt.N = randp( 1, 2, 4 ); end;    
    if( isempty(opt.dim) );
        opt.dim = randp( 1, 2, 25, 1/opt.N ); end;
    if( isempty(opt.col) );
        opt.col = opt.dim; end;
    if( isempty(opt.row) );
        opt.row = opt.dim; end;    
    if( isempty(opt.k) );
        opt.k = 0; end;    
    if( isempty(opt.k) );
        opt.k = 0; end;
    k = opt.k;
    what = opt.what;
    M = {};
    if( false );
    elseif( check(what, {1,'rand_bool','bool','r_b','b'}) );  % random boolean matrix
        for i = 1:opt.N; 
            M{i} = randi( [0 1], [opt.row, opt.col] ); end; 
        
    elseif( check(what, {2,'binary','bin'}) );  % Binary matrices in sorted order  
        if( opt.verbose  >= 0 );
            expect( opt.k == abs(round( opt.k )), 'gallery_matrixset:binary', '''k'' must a be a non-negative integer.' ); end;
        M = matrixenum( opt.dim, opt.N, abs(round( opt.k )), false, 'v',opt.verbose-1 );

    elseif( check(what, {'complex','rand_complex','r_c','c'}) );  % complex eigenvector  % XX Currently we do not have an efficient method to generate these. Thus for random matrices we omit it.
        M = matrix_complex( opt.dim, opt.N, opt.k, opt.verbose );

    elseif( check(what, {4,'equal_neq','rand_equal_neg','rand_neg','r_e_n','r_n','e_n','n'}) );  % random random matrix with equally distributed values in [-1 1] 
        for i = 1:opt.N; 
            M{i} = 2*rand( opt.row, opt.col ) - .5; end; 

    elseif( check(what, {5,'gauss','gaussian','rand_gauss','randn','r_g','g'}) );  % random matrix with normally distributed values
        for i = 1:opt.N; 
            M{i} = randn( opt.row, opt.col ); end;
        
    elseif( check(what, {5,'integer','randi'}) );  % random integer matrix with uniform distributed values in some interval
        if( isequal(opt.k, 0) );
            k = [-9 9];
        elseif( isscalar(k) && k > 0 );
            k = [0 k];
        elseif( isscalar(k) && k<0 );
            k = [k -k];
        else;
            k = opt.k; end;
        for i = 1:opt.N;
            M{i} = randi( k, opt.row, opt.col ); end;        

    elseif( check(what, {6,'equal','rand_equal','rand','r_e','r','e'}) );  % random random matrix with equally distributed values in [0 1]          
        for i = 1:opt.N; 
            M{i} = rand( opt.row, opt.col ); end;

    elseif( check(what, {7,'house','rand_house'}) );
        M = matrix_house( opt );

    elseif( check(what, {8,'int','rand_int','r_int'}) );  % random matrix uniform distributed with integer values
        m = randp( 1, 1, inf, 4 );
        for i = 1:opt.N; 
            M{i} = randi( m, opt.dim, opt.dim ) - floor(m/2); end;
    elseif( check(what, {'sign'}) );
        error( 'gallery:matrixset:deprecated', '''sign'' is not a valid name anymore. Use ''sign0''/''ternary'' for [-1 0 1] matrices, or ''rademacher''/''pm1'' for a [-1 1] matrix.' );
    elseif( check(what, {9,'sign0','ternary','pm10','rand_pm10','r_pm10'}) );  % random matrix with values -1, 0 1.
        for i = 1:opt.N; 
            M{i} = randi( 3, [opt.dim opt.dim] ) - 2; end;

    elseif( check(what, {10,'rademacher','pm1','rand_pm1','r_pm1'}) );  % random matrix with values -1, 1
        for i = 1:opt.N;
            M{i} = 2*(randi( [0 1], [opt.dim opt.dim] ) - 1/2); end;

    elseif( check(what, {11,'columnstochastic','stochastic','rand_stochastic','r_stoch','stoch'}) );  % Stochastic matricesTransition matrices with mask length dim, dilation=N;
        M = matrix_stochastic( opt.dim, opt.N ); 
    elseif( check(what, {12,'stochastic_neg','rand_stochastic_neg','r_stoch_n','stoch_n'}) );  % Stochastic matricesTransition matrices with mask length dim, dilation=N;
        M = matrix_stochastic_neg( opt.dim, opt.N );
    elseif( check(what, {13,'stochastic_double','rand_doublestochastic','doublestochastic','rand_stochastic_double','r_dstoch','dstoch','r_stoch_d','stoch_d'}) );  % Double stochastic matrices
        M = matrix_doublestochastic( opt.dim, opt.N ); 
    elseif( check(what, {14,'stochastic_double_neg','rand_doublestochastic_neg','doublestochastic_neg','rand_stochastic_double_neg','r_dstoch_n','dstoch_n','r_stoch_d_n','stoch_d_n'}) ); %Double stochastic matrices
        M = matrix_doublestochastic_neg( opt.dim, opt.N );
    elseif( check(what, {15,'stochastic_pm','rand_stochasticpm','r_stochpm','stochpm'}) );  % Stochastic matrices with entries in [-1 1]
        M = matrix_stochastic_pm( opt );

    elseif( check(what, {16,'totallypositive','tp'}) );
        for n = 1:opt.N;
            M{n} = gallery( 'cauchy', cumsum(randn(1, opt.dim)) ); end;
    elseif( check(what, {'tu','rand_tu','r_tu'}) );  % (experimental)
        M = matrix_TU( opt.dim, opt.N );

    elseif( check(what, {18,'unitary','rand_unitary','r_u','u'}) );  % Unitary matrices 
        M = matrix_unitary( opt.dim, opt.N ); 
    elseif( check(what, {19,'tv0','rand_tv0','r_tv0'}) );  % (experimental) Transition matrices with mask length dim, dilation=N;           
        M = matrix_TV0( opt.dim, opt.N );

    elseif( check(what, {20,'rhozero','nilpotent'}) );  % matrices with spectral radius equal zero             
        M = matrix_zerospectralradius( opt.dim, opt.N, opt.verbose ); 
        
    elseif( check(what, {21,'hurwitz','hurwitzstable'}) );  % matrices whose real parts of eigenvalues is strictly negative
        M = matrix_hurwitz( opt.dim, opt.N, opt.verbose ); 
        
    elseif( check(what, {22,'circulant','circul'}) );
        M = matrix_circulant( opt.dim, opt.N, opt.verbose );
        
    elseif( check(what, {23,'toeplitz'}) );
        M = matrix_toeplitz( opt.dim, opt.N, opt.verbose );
        
    elseif( check(what, {24,'hankel'}) );
        M = matrix_hankel( opt.dim, opt.N, opt.verbose );
        
    elseif( check(what, {25,'chebvand'}) );
        for i = 1:opt.N; 
            M{i} = gallery( 'chebvand', opt.dim, opt.dim ); end;
        
    elseif( check(what, {26,'normal'}) );
        M = matrix_normal( opt.dim, opt.N, opt.verbose );
        
    elseif( check(what, {27,'orthogonal','orth'}) );
        M = matrix_orthogonal( opt.dim, opt.N, opt.verbose );
    
    elseif( check(what, {28,'orthonormal','unitary'}) );
        M = matrix_orthonormal( opt.dim, opt.N, opt.verbose );
        
    elseif( check(what, {29,'cycols','cycol','cycliccolumn','cycliccolumn'}) );    
        k = randi( 1 + opt.dim );
        for i = 1:opt.N;
            M{i} = gallery( 'cycol', opt.dim, k ); end;
        
    end;
end

function [ M ] = getmatrix_random_matlab( opt, varargin );
    % random matrices from Matlabs gallery
    % Random matrices, need two arguments 'rand_mmm',<dim>,<number of matrices>
    % XX must be refactored, so that I get rid of the hardcoded number in the cases
    if( check(opt.what, {'random'}) );
        opt.what = []; end;
    if( isempty(opt.what) ); 
        opt.what = randi( [1 25] ); end;
    if( isempty(opt.N) );
        opt.N = randp( 1, 2, 4 ); end;    
    if( isempty(opt.dim) );
        opt.dim = randp( 1, 2, 25, 1/opt.N ); end;
    what = opt.what;
    M = {};
    if( false );
    elseif( check(what, {1,'cauchy'}) );
        for n = 1:opt.N; 
            M{n} = gallery( 'cauchy', randn(1, opt.dim) ); end;
    elseif( check(what, {2,'chow'}) );
        if( opt.dim >= 2 );
            for n = 1:opt.N; 
                M{n} = gallery( 'chow', opt.dim, randn+1, randn ); end;
        else;
            for n = 1:opt.N; 
                M{n} = randx( opt.dim ); end; end;
    elseif( check(what, {3,'compar'}) );
        for n = 1:opt.N;
            M{n} = gallery( 'compar', randx(opt.dim, opt.dim), randi(2)-1 ); end;

    elseif( check(what, {4,'dorr'}) );
        for n = 1:opt.N;
            if( opt.dim <= 1 );
                M{n} = randn( opt.dim );
            else;
                M{n} = full( gallery( 'dorr', opt.dim, abs(randn)*.1 ) ); end; end;

    elseif( check(what, {5,'fiedler'}) );
        for n = 1:opt.N; M{n} = gallery( 'fiedler', randx(1, opt.dim) ); end;
    elseif( check(what, {6,'forsythe'}) );
        if( opt.dim >= 2 );
            for n = 1:opt.N; 
                M{n} = gallery( 'forsythe', opt.dim, abs(eps*randn), eps*randn ); end;
        else;
            for n = 1:opt.N; M{n} = randx( opt.dim ); end; end;

    elseif( check(what, {6,'gearmat','gear','gearmatrix'}) );
        len =  mixvector( [-opt.dim:-1 1:opt.dim], 2, 0 );
        assert( opt.N <= len, 'gallery_matrixset:opt', 'For matrices ''gearmat'' at most (opt.dim-1)^2 matrices can be generated (i.e. ''N'' must be less-equal (opt.dim-1)^2.' );
        idx = randperm( len, opt.N );
        for n = 1:opt.N;
            ij = mixvector( [-opt.dim:-1 1:opt.dim], 2, idx(n) );
            M{n} = gallery( 'gearmat', opt.dim, ij(1), ij(2) ); end;

    elseif( check(what, {7,'hanowa'}) );
        if( opt.verbose  >= 0 );
            expect( ~mod(opt.dim, 2), 'gallery_matrixset:hanowa', '''Hanowa'' only works for even dimensions.' ); end;
        for n = 1:opt.N; 
            M{n} = gallery( 'hanowa', round(opt.dim/2)*2, randn ); end;

    elseif( check(what, {8,'invhess'}) );
        if( opt.dim >= 2 );
            for n = 1:opt.N; 
                M{n} = gallery( 'invhess', randx(1, opt.dim) ); end;
        else;
            for n = 1:opt.N; M{n} = randx( opt.dim ); end; end;

    elseif( check(what, {9,'jordblock'}) );
        for n = 1:opt.N; 
            M{n} = gallery( 'jordbloc', opt.dim, randx ); end;

    elseif( check(what, {10,'kms'}) );
        for n = 1:opt.N; 
            M{n} = gallery( 'kms', opt.dim, randn ); end;
    elseif( check(what, {11,'krylov'}) );
        for n = 1:opt.N; 
            M{n} = gallery( 'krylov', opt.dim ); end;

    elseif( check(what, {12,'leslie'}) );
        posflag = randi( [0 1] );
        for n = 1:opt.N;
            if( posflag );
                x = rand( 1, opt.dim );
                y = rand( 1, opt.dim - 1 );
            else;
                x = randn( 1, opt.dim );
                y = randn( 1, opt.dim - 1 ); end;
            if( opt.dim == 1 );
                M{n} = x;
            else;
                M{n} = gallery( 'leslie', x, y ); end; end;

    elseif( check(what, {13,'moler'}) );
        for n = 1:opt.N; M{n} = gallery( 'moler', opt.dim, randx ); end;

    elseif( check(what, {14,'pei'}) );
        for n = 1:opt.N; 
            M{n} = gallery( 'pei', opt.dim, randx ); end;
    elseif( check(what, {15,'prolate'}) );
        for n = 1:opt.N; 
            M{n} = gallery( 'prolate', opt.dim, randn ); end;   
    elseif( check(what, {16,'randcolu','rand_colu_1','r_colu_1','colu_1'}) );  % random n-by-n matrix with columns of unit 2-norm, with random singular values whose squares are from a uniform distribution, contains no zeros   
        if( opt.dim >= 2 );
            for n = 1:opt.N; 
                M{n} = gallery( 'randcolu', opt.dim, opt.dim, 0 ); end;
        else;
            for n = 1:opt.N; 
                M{n} = sign( randn( opt.dim ) ); end; end;
    elseif( check(what, {17,'rand_colu_0','r_colu_0','colu_0'}) );  % random n-by-n matrix with columns of unit 2-norm, with random singular values whose squares are from a uniform distribution, may contains zeros   
        for n = 1:opt.N; 
            M{n} = gallery( 'randcolu', opt.dim, opt.dim, 1 ); end; 
    elseif( check(what, {18,'randcorr','rand_corr_1','r_corr_1','corr_1'}) );  % random n-by-n correlation matrix with random eigenvalues from a uniform distribution, contains no zeros. Makes maximal trees for high dimensions   
        for n = 1:opt.N; 
            M{n} = gallery( 'randcorr', opt.dim, opt.dim, 0 ); end;        
    elseif( check(what, {19,'rand_corr_0','r_corr_0','corr_0'}) );  % random n-by-n correlation matrix with random eigenvalues from a uniform distribution, may contains zeros. Makes maximal trees for high dimensions
        for n = 1:opt.N; 
            M{n} = gallery( 'randcorr', opt.dim, opt.dim, 1 ); end;
    elseif( check(what, {20,'randhess','rand_hess','r_hess','hess'}) );   % random n-by-n real, orthogonal upper Hessenberg matrix.
        for n = 1:opt.N; 
            M{n} = gallery( 'randhess', opt.dim ); end;
    elseif( check(what, {21,'randjorth'}) );
        if( opt.dim >= 2 );
            n = ceil( opt.dim/2 );
            m = opt.dim - n;
            alpha = 1 + abs( randn );
            symm = randi(2) - 1;
            method  = randi(2) - 1;
            for j = 1:opt.N; 
                M{j} = gallery( 'randjorth', n, m, alpha, symm, method ); end;
        else;
            M = repmat( {ones(opt.dim)}, 1, opt.N ); end;
    elseif( check(what, {22,'rando'}) );
        k = randi( 3 );
        for n = 1:opt.N; 
            M{n} = gallery( 'rando', opt.dim, k ); end;
    elseif( check(what, {23,'randsvd'}) );
        kappa = 1 + abs( randn );
        mode = randi(5);
        for n = 1:opt.N; 
            M{n} = gallery( 'randsvd', opt.dim, kappa, mode, randi(opt.dim), randi(opt.dim) ); end;

    elseif( check(what, {24,'sampling'}) );  %random n-by-n real, orthogonal upper Hessenberg matrix.
        if( opt.dim >= 2 );
            for n = 1:opt.N; 
                M{n} = gallery( 'sampling', randx(1, opt.dim) ); end;
        else;
            M = repmat( {ones(opt.dim)}, 1, opt.N ); end;

    elseif( check(what, {25,'vander','vandermonde'}) );  % Vandermonde matrix
        for n = 1:opt.N; 
            M{n} = vander( randx(1, opt.dim) ); end;
    %elseif( check(what, {26,'toeppd'};  % Documentation is wrong for this matrix
    %    m = ceil( round(randx) ) + 1;
    %    x = randx( 1, dim );
    %    for n = 1:opt.N; val{n} = gallery( 'toeppd', dim, m, x, randx(1,dim) ); end;

    end;
end

function [ M ] = getmatrix_papers( opt, varargin );
    % Matrices from Examples/Papers
    randflag = isempty( opt.what ) * randi( [1 9] );
    M = {};
    idx = opt.what=='_';
    opt.what(idx) = [];
    
     if( randflag == 0 && isempty(M) || randflag == 1 );
        M = getmatrix_papers_moeller( opt, varargin{:} ); end;
     if( randflag == 0 && isempty(M) || randflag == 2 );
        M = getmatrix_papers_gwz05( opt, varargin{:} ); end;
     if( randflag == 0 && isempty(M) || randflag == 3 );
        M = getmatrix_papers_gp13(  opt, varargin{:} ); end;
     if( randflag == 0 && isempty(M) || randflag == 4 );
        M = getmatrix_papers_pj08(  opt, varargin{:} ); end;
     if( randflag == 0 && isempty(M) || randflag == 5 );
        M = getmatrix_papers_gz09(  opt, varargin{:} ); end;
     if( randflag == 0 && isempty(M) || randflag == 6 );
         M = getmatrix_papers_ap12( opt, varargin{:} ); end;    
     if( randflag == 0 && isempty(M) || randflag == 7 );
         M = getmatrix_papers_ajpr14( opt, varargin{:} ); end;
     if( randflag == 0 && isempty(M) || randflag == 8 );
        M = getmatrix_papers_mejstrik2020( opt, varargin{:} ); end;
     if( randflag == 0 && isempty(M) || randflag == 9 );
        M = getmatrix_papers_misc(  opt, varargin{:} ); end;    
    
end

function [ M ] = getmatrix_papers_moeller( opt, varargin );
    % Claudia Moeller, A New Strategy for Exact Determination of the Joint Spectral Radius
    if( check(opt.what, {'moeller'}) );
        opt.what = []; end;
    M = {};
    what = opt.what;
    if( isempty(what) );
        what = randi( 23 ); end;
    if( false );
    elseif( check(what, {'moeller','moeller15','moeller2015','moeller1', 'moeller71'}) )
        fprintf( 2, ['Implemented examples from ''moeller'' are: '...
                     '  moeller71, moeller71a, moeller714, moeller71f, moeller71g,\n'...
                     '  moeller72a, moeller72b,\n'...
                     '  moeller73a, moeller74a, moeller75a (dd6), moeller76a (dd8), moeller77a, moeller78a,\n'...
                     '  moeller79a0, moeller791, moeller79a2, moeller79a3, moeller79a4, moeller710a,\n'...
                     '  feta1, feta2, feta3\n'...
                     'A random set from moeller is returned.\n'...
                     ] );
        opt.what = [];
        M = getmatrix_papers_moeller( opt );
        return;               
    elseif( check(what, {1,'moeller711','moeller71a'}) );
        if( isempty(opt.dim) );
            vprintf( 'Give value of Epsilon as second argumnt. Use default 1/8 now.\n', 'imp',[1 opt.verbose] );
            opt.dim = 1/8; end;
        eps = opt.dim;  % rho(A2) -> rho(A1) as eps -> 0
        A1 = [10/9 1/3;-1/3 0];
        A2 = [0 1/5*sqrt(1-eps);-1/5*sqrt(1-eps) 26/25-eps];
        M = {A1, A2};
    elseif( check(what, {2, 'moeller714','moeller71d'}) );  % GP2013
        D1 = [1 1;0 1];
        D2 = 9/10*[1 0;1 1];
        M = {D1, D2};
    elseif( check(what, {3, 'moeller71f'}) );
        % product bounded but not irreducible
        F1 = [1/4 0;-1/2 1/2];
        F2 = [1 -1/4;0 1/2];
        M = {F1, F2};
    elseif( check(what, {4, 'moeller71g'}) );
        % not irreducible, unbounded
        G1 = [1 1;0 1];
        G2 = [0 0;0 1/2];
        M = {G1, G2};

    elseif( check(what, {5, 'moeller72a', '3point'}) );
        % 3 point scheme, first difference scheme?
        A1 = 1/32*[ 22 -6 0;-6 22 0;0 22 -6];
        A2 = 1/32*[-6 22 0;0 22 -6;0 -6 22];
        M = {A1, A2};
    elseif( check(what, {6, 'moeller72b','moeller72'}) );
        % 3 point scheme, second difference scheme?
        B1 = 1/32*[56 0;-12 -12];
        B2 = 1/32*[-12 -12;0 56];
        M = {B1, B2};
    elseif( check(what, {7, 'moeller73a','moeller73'}) );
        % parametrized 4 point scheme
        if( isempty(opt.dim) );
            if( opt.verbose >= 1 );
                fprintf( 'Give value of omega as second argumnt. Use default 1/16 now.\n' ); end;
            opt.dim = 1/16; end;
        w = opt.dim;
        A1w = w*[4 4 0 0;-2 1/w-4 -2 0;0 4 4 0;0 -2 1/w-4 -2];
        A2w = w*[-2 1/w-4 -2 0;0 4 4 0;0 -2 1/w-4 -2;0 0 4 4];
        M = {A1w, A2w};
    elseif( check(what, {8, 'moeller74a','moeller74'}) );
        % parametrized family of dual 4-point schemes
        if( isempty(opt.dim) );
            if( opt.verbose >= 1 );
                fprintf( 'Give value of omega as second argumnt. Use default 1/128 now.\n' ); end;
            opt.dim = 1/128; end;
        w = opt.dim;
        A1w = w*[32 32 0 0;-20 1/w-24 -20 0;0 32 32 0;0 -20 1/w-24 -20];
        A2w = w*[-20 1/w-24 -20 0;0 32 32 0;0 -20 1/w-24 -20;0 0 32 32];
        M = {A1w, A2w};

    elseif( check(what, {9, 'moeller75a','moeller75','dd6'}) );
        % Dubuc-Deslaurier (DD) 6-point scheme proposed ([DD89])
        A1 = 1/8*[-18 -19 0 0;3 38 3 0;0 -18 -18 0;0 3 38 3];
        A2 = 1/8*[3 38 3 0;0 -18 -18 0;0 3 38 3;0 0 -18 -18];
        M = {A1, A2};

    elseif( check(what, {10, 'moeller76a','moeller76','dd8'}) );
        % 8-point scheme proposed by Deslaurier and Dubuc ([DD89]) 
        A1 = 1/64*[30 -14 -14 30 0 0 0 0;-5 -56 154 -56 -5 0 0 0;0 30 -14 -14 30 0 0 0;0 -5 -56 154 -56 -5 0 0;0 0 30 -14 -14 30 0 0;0 0 -5 -56 154 -56 -5 0;0 0 0 30 -14 -14 30 0;0 0 0 -5 -56 154 -56 -5];
        A2 = 1/64*[-5 -56 154 -56 -5 0 0 0;0 30 -14 -14 30 0 0 0;0 -5 -56 154 -56 -5 0 0;0 0 30 -14 -14 30 0 0;0 0 -5 -56 154 -56 -5 0;0 0 0 30 -14 -14 30 0;0 0 0 -5 -56 154 -56 -5;0 0 0 0 30 -14 -14 30];
        M = {A1, A2};

    elseif( check(what, {11, 'moeller77a','moeller77'}) );
        % C2 Ternary 4-point scheme [HIDS02] with parameter mu
        % mu is fixed to 1/11
        A1 = 1/11*[5 -4 0;-4 5 0;0 9 0];
        A2 = 1/11*[-4 5 0;0 9 0;0 5 -4];
        A3 = 1/11*[0 9 0;0 5 -4;0 -4 5];
        M = {A1, A2, A3};

    elseif( check(what, {12, 'moeller78a','moeller78'}) );
        % Quaternary 3-point scheme
        A1 = 1/32*[23 -15 0;-15 23 0;0 8 0];
        A2 = 1/32*[-15 23 0;0 8 0;0 8 0];
        A3 = 1/32*[0 8 0;0 8 0;0 23 -15];
        A4 = 1/32*[0 8 0;0 23 -15;0 -15 23];
        M = {A1, A2, A3, A4};  
    elseif( check(what, {13, 'moeller79a0','moeller790'}) );
        A1 = 1/2*[4 0;-1 -1];
        A2 = 1/2*[-1 -1;0 4];
        M = {A1, A2};
    elseif( check(what, {14,'moeller79a1','moeller791'}) );
        A1 = 1/16*[1 42 1 0;0 -14 -14 0;0 1 42 1;0 0 -14 -14];
        A2 = 1/16*[-14 -14 0 0;1 42 1 0;0 -14 -14 0;0 1 42 1];
        M = {A1, A2};
    elseif( check(what, {15, 'moeller79a2','moeller792'}) );
        A1 = 1/128*[24 448 24 0 0 0;-1 -183 -183 -1 0 0;0 24 448 24 0 0;0 -1 -183 -183 -1 0;0 0 24 448 24 0;0 0 -1 -183 -183 -1];
        A2 = 1/128*[-1 -183 -183 -1 0 0;0 24 448 24 0 0;0 -1 -183 -183 -1 0;0 0 24 448 24 0;0 0 -1 -183 -183 -1;0 0 0 24 448 24];
        M = {A1, A2};
    elseif( check(what, {16', 'moeller79a3','moeller793'}) );
        A1 = 1/1024*[-34 -2302 -2302   -34     0     0     0   0;
                       1   424  4846   424     1     0     0   0;
                       0   -34 -2302 -2302   -34     0     0   0;
                       0     1   424  4846   424     1     0   0;
                       0     0   -34 -2302 -2302   -34     0   0;
                       0     0     1   424  4846   424     1   0;
                       0     0     0   -34 -2302 -2302   -34   0;
                       0     0     0     1   424  4846   424   1;];
        A2 = 1/1024*[  1  424   4846   424     1     0     0   0;
                       0  -34  -2302 -2302   -34     0     0   0;
                       0    1    424  4846   424     1     0   0;
                       0    0    -34 -2302 -2302   -34     0   0;
                       0    0      1   424  4846   424     1   0;
                       0    0      0   -34 -2302 -2302   -34   0;
                       0    0      0     1   424  4846   424   1;
                       0    0      0     0   -34 -2302 -2302 -34;];
        M = {A1, A2};
    elseif( check(what, {17, 'moeller79a4','moeller794'}) );
        A1 = 1/8192*[44 6576  53064   6576     44      0      0      0    0  0;
                     -1 -765 -28290 -28290   -765     -1      0      0    0  0;
                      0   44   6576  53064   6576     44      0      0    0  0;
                      0   -1   -765 -28290 -28290   -765     -1      0    0  0;
                      0    0     44   6576  53064   6576     44      0    0  0;
                      0    0     -1   -765 -28290 -28290   -765     -1    0  0;
                      0    0      0     44   6576  53064   6576     44    0  0;
                      0    0      0     -1   -765 -28290 -28290   -765   -1  0;
                      0    0      0      0     44   6576  53064   6576   44  0;
                      0    0      0      0     -1   -765 -28290 -28290 -765 -1;];
        A2 = 1/8192*[-1 -765 -28290 -28290   -765     -1      0      0    0  0;
                      0   44   6576  53064   6576     44      0      0    0  0;
                      0   -1   -765 -28290 -28290   -765     -1      0    0  0;
                      0    0     44   6576  53064   6576     44      0    0  0;
                      0    0     -1   -765 -28290 -28290   -765     -1    0  0;
                      0    0      0     44   6576  53064   6576     44    0  0;
                      0    0      0     -1   -765 -28290 -28290   -765   -1  0;
                      0    0      0      0     44   6576  53064   6576   44  0;
                      0    0      0      0     -1   -765 -28290 -28290 -765 -1;
                      0    0      0      0      0     44   6576  53064 6576 44;];
        M = {A1, A2};
    elseif( check(what, {18, 'moeller710a','moeller710'}) );
        % parametrized 8-point scheme
        if( isempty(opt.dim) );
            if( opt.verbose >= 1 );
                fprintf( 'Give value of omega as second argumnt. Use default 1/278 now.\n' ); end;
            opt.dim = 1/278; end;
        w = opt.dim;
        A1 = [192*w 832*w-9/4 832*w-9/4 192*w 0 0 0 0;
              -32*w -512*w+3/8 -960*w+19/4 -512*w+3/8 -32*w 0 0 0;
              0 192*w 832*w-9/4 832*w-9/4 192*w 0 0 0;
              0 -32*w -512*w+3/8 -960*w+19/4 -512*w+3/8 -32*w 0 0;
              0 0 192*w 832*w-9/4 832*w-9/4 192*w 0 0;
              0 0 -32*w -512*w+3/8 -960*w+19/4 -512*w+3/8 -32*w 0;
              0 0 0 192*w 832*w-9/4 832*w-9/4 192*w 0;
              0 0 0 -32*w -512*w+3/8 -960*w+19/4 -512*w+3/8 -32*w;];
        A2 = [-32*w -512*w+3/8 -960*w+19/4 -512*w+3/8 -32*w 0 0 0;
              0 192*w 832*w-9/4 832*w-9/4 192*w 0 0 0;
              0 -32*w -512*w+3/8 -960*w+19/4 -512*w+3/8 -32*w 0 0;
              0 0 192*w 832*w-9/4 832*w-9/4 192*w 0 0;
              0 0 -32*w -512*w+3/8 -960*w+19/4 -512*w+3/8 -32*w 0;
              0 0 0 192*w 832*w-9/4 832*w-9/4 192*w 0;
              0 0 0 -32*w -512*w+3/8 -960*w+19/4 -512*w+3/8 -32*w;
              0 0 0 0 192*w 832*w-9/4 832*w-9/4 192*w;];
      M = {A1, A2};

    % Claudia Moeller, Feta algorithm source code
    elseif( check(what, {19,'feta1'}) );
        A1 = [1 1;0 1];
        A2 = (9/10)*[1,0;1,1];
        M = {A1,A2};
        %val(:,:,1) = A1;
        %val(:,:,2) = A2;
        % smp = [1 2] ??
    elseif( check(what, {20,'feta2'}) );
        s = (3-sqrt(5))/2;
        A1 = s*[2 1;1 1];
        A2 = s*[1 1;1 2];
        M = {A1, A2};
        % smp = {1,2} ??
    elseif( check(what, {21,'feta3'}) );  % 4point ??
        w = 1/16-1/10;
        A1 = [4*w 4*w 0 0;-2*w 1-4*w -2*w 0;0 4*w 4*w 0;0 -2*w 1-4*w -2*w];
        A2 = A1(4:-1:1,4:-1:1);
        M = {A1, A2};
        % smp = {1,2} (when w<1/16), otherwise smp [1 2]          
            
    end;
end

function [ M ] = getmatrix_papers_gwz05( opt, varargin );
    if( check(opt.what, {'gwz','gwz05','gwz2005'}) );
        opt.what = []; end;
    M = {};
    what = opt.what;
    if( isempty(what) );
        what = randi( 5 ); end;
    % Guglielmi, Wirth, Zennaro - 2005 - Complex polytope extremality results for families of matrices
    
    if( false );
    elseif( check(what, {0,'gwz2005','gwz05'}) );
        error( 'gallery_matrixset:input', ['Implemented examples from ''gwz05'' are: '...
                                  'gwz05ex61 (moeller712), gwz05ex61_legacy, gwz05ex62 (moeller713), gwz05ex64'] );
    elseif( check(what, {1,'gwz05ex61','moeller712','moeller71b'}) );
        M{1} = [cos(sym(1)) -sin(sym(1) ); sin(sym(1)) cos(sym(1))]; 
        M{2} = [1/sym(2) -1/1i;0 0]; 
    elseif( check(what, {2,'gwz05ex61_legacy','gwz05ex61old'}) );  % there was a typo here. I keep it for backwards compatibility
        M{1} = [cos(sym(1)) -sin(sym(1) ); sin(sym(1)) cos(sym(1))]; 
        M{2} = [1/sym(2) -1/2;0 0]; 
    elseif( check(what, {3,'gwz05ex62','moeller713','moeller71c'}) );
        M{1} = (3-sqrt(sym(5)))*[2 1;1 1]; 
        M{2} = (3-sqrt(sym(5)))*[1 1;1 2]; 
    elseif( check(what, {4,'gwz05ex63'}) );
        M{1} = [1 0 0;0 1/2 0;0 0 1/4]; 
        M{2} = [1/2 0 0;1/2 0 0;1/2 0 0]; 
    elseif( check(what, {5,'gwz05ex64'}) );
        M{1} = [1 1;0 0]; 
        M{2} = [0 0;1 1]; 
        M{3} = 1/2*[1 1;1 1]; 
        M{4} = 2/3*[1 0;-1 0];
    end;
end

function [ M ] = getmatrix_papers_gp13( opt, varargin );
% Guglielmi, Protasov 2013
if( check(opt.what, {'gp','gp13'}) );
        opt.what = []; end;
    M = {};
    what = opt.what;
    if( isempty(what) );
        what = randi( 1 ); end;
    if( false );
    elseif( check(what, {1,'gp13ex1','gp13e1','gp13_1'}) );
        M = {[1 1;0 1], 9/10*[1 0;1 1]};
    elseif( check(what, {1,'gp13ex2','gp13e2','gp13_2','gp13_p35'}) );
        M = {[7 0;2 3], [2 4;0 8]};  % example for lower-spectral-radius
    elseif( check(what, {1,'gp13ex81','gp13e81','gp13_81','gp13_p38'}) );
        % The asymptotics of the number of overlap-free words
        % example for jsr and lsr
        M{1} = [0 0 0 0 0 0 0 2 4 2 0 0 0 0 0 0 0 0 0 0;
                0 0 1 1 0 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 0 0 0 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0
                0 0 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 1 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 0 0 0 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 1 2 1 0 0 0 0 0 0 0 1 2 1
                0 0 1 1 0 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 1 1 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0
                1 2 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 1 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 1 0 0 0 0 0 0 1 0 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0];                
        M{2} = [0 0 0 0 0 0 0 1 2 1 0 0 0 0 0 0 0 1 2 1
                0 0 1 1 0 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 1 1 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0
                1 2 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 1 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 1 0 0 0 0 0 0 1 0 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 4 2
                0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 1 1 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 0
                0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 1 0 0 1 0 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                ];
    elseif( check(what, {1,'gp13ex82','gp13e82','gp13_82','gp13_p40','pascal_rombhus','pascalrhombus'}) );
        % Density of ones in the Pascal rhombus
        M{1} = [0 1 0 0 0;1 0 2 0 0;0 0 0 0 0;0 1 0 0 1;0 0 0 2 1];
        M{2} = [1 0 2 0 0;0 0 0 2 1;1 1 0 0 0;0 0 0 0 0;0 1 0 0 0];
    elseif( check(what, {1,'gp13ex83','gp13e83','gp13_83','gp13_p43'}) );
        % Euler binary partition function for r = 7
        % example for jsr and lsr
        M{1} = [1 1 1 1 0 0;0 1 1 1 0 0;0 1 1 1 1 0;0 0 1 1 1 0;0 0 1 1 1 1;0 0 0 1 1 1];
        M{2} = [1 1 1 0 0 0;1 1 1 1 0 0;0 1 1 1 0 0;0 1 1 1 1 0;0 0 1 1 1 0;0 0 1 1 1 1];
    elseif( check(what, {1,'gp13ex84','gp13e84','gp13_84','gp13_p44'}) );
        % Euler ternary partition function for r = 14
        % example for jsr and lsr
        M{1} = [1 1 1 1 1 0 0;0 1 1 1 1 0 0;0 1 1 1 1 1 0;0 1 1 1 1 1 0;0 0 1 1 1 1 0;0 0 1 1 1 1 1;0 0 1 1 1 1 1];
        M{2} = [1 1 1 1 1 0 0;1 1 1 1 1 0 0;0 1 1 1 1 0 0;0 1 1 1 1 1 0;0 1 1 1 1 1 0;0 0 1 1 1 1 0;0 0 1 1 1 1 1];
        M{3} = [1 1 1 1 0 0 0;1 1 1 1 1 0 0;1 1 1 1 1 0 0;0 1 1 1 1 0 0;0 1 1 1 1 1 0;0 1 1 1 1 1 0;0 0 1 1 1 1 0];
    end
end

function [ M ] = getmatrix_papers_pj08( opt, varargin );
% Parillo, Jadbabaie, 2008
    if( check(opt.what, {'pj','pj08','pj2008'}) );
        opt.what = []; end;
    M = {};
    what = opt.what;
    if( isempty(what) );
        what = randi( 1 ); end;
    if( false );
    elseif( check(what, {1,'pj08ex54','pj08e54'}) );
        M{1} = [0 1 7 4;1 6 -2 -3;-1 -1 -2 -6;3 0 9 1];
        M{2} = [-3 3 0 -2;-2 1 4 9;4 -3 1 1;1 -5 -1 -2];
        M{3} = [1 4 5 10;0 5 1 -4;0 -1 4 6;-1 5 0 1];
    end
end

function [ M ] = getmatrix_papers_gz09( opt, varargin );
    if( check(opt.what, {'gz','gz09','gz2009'}) );
        opt.what = []; end;
    M = {};
    what = opt.what;
    if( isempty(what) );
        what = randi( 3 ); end;
    % Guglielmi, Zennaro - Finding Extremal Complex Polytope Norms for Families of Real Matrices 
    if( false );
    elseif( check(what, {1,'gz09ex31'}) );
        if( isempty(opt.dim) );
            opt.dim = rand; end;
        M{1} = [cos(sym(1)) -sin(sym(1) ); sin(sym(1)) cos(sym(1))]; 
        M{2} = opt.dim/sqrt(sym(2))*[1 1;0 0]; 
    elseif( check(what, {2,'gz09ex32'}) );
        M{1} = [-1/2 -sqrt(sym(3))/2; sqrt(sym(3))/2 -1/2];
        M{2} = sym([11/20 11/20; -11/20 -11/20]);
    elseif( check(what, {3,'gz09ex41'}) );
        M{1} = [-3 -2 1 2;-2 0 -2 1; 1 3 -1 -5;-3 -3 -2 -1]; 
        M{2} = [1 0 -3 -1;-4 -2 -1 -4; -1 0 -1 2;-1 -2 -1 2];             
    end;
end

function [ M ] = getmatrix_papers_ap12( opt, varargin );
    if( check(opt.what, {'ap','ap12','ap2012'}) );
        opt.what = []; end;
    M = {};
    what = opt.what;
    if( isempty(what) );
        what = randi( 1 ); end;    
    if( false );
    elseif( check(what, {1,'ap12'}) );
        M{1} = [0 1;0 0];
        M{2} = [0 0;1 0];
    end
end

function [ M ] = getmatrix_papers_ajpr14( opt, varargin );
    if( check(opt.what, {'ajpr','ajpr14','ajpr2014'}) );
        opt.what = []; end;
    M = {};
    what = opt.what;
    if( isempty(what) );
        what = randi( 1 ); end;
    if( false );
    elseif( check(what, {1,'ajpr14ex54'}) );
        M{1} = [-1 -1;-4 0]; 
        M{2} = [3 3;-2 1];
    end
end

function [ M ] = getmatrix_papers_mejstrik2020( opt, varargin );
    if( check(opt.what, {'mejstrik','tommsch','mejstrik20','mejstrik2020','m20','m2020'}) );
        opt.what = []; end;
    M = {};
    what = opt.what;
    if( isempty(what) );
        what = randi( 5 ); end;
     % Mejstrik -  2020 - Improved invariant polytope algorithm and applications
    if( false )
     elseif( check(what, {1,'mejstrik119long','mej2051b'}) );  % smp length of 119!. norms converge very wiggly to the spectral radius. Experimentally found numbers. 
        M{1} = [ 0.163026496094203      -0.92406403398077952;    0.94910922519637453   0.75424733784141262]; 
        M{2} = [-0.95851077103541249    -0.65295899115856648;    0.6731845518678724   -0.58469670981727162]; 
     elseif( check(what, {2,'mejstrik119','mej2051','mej2051a'}) );  % smp length of 119!. norms converge very wiggly to the spectral radius. Rational approximations of the long numbers above.
        M{1} = [  15/92   -73/79;   56/59   89/118]; 
        M{2} = [-231/241 -143/219; 103/153 -38/65]; 
     elseif( check(what, {3,'mejstrikcn','mej52b'}) );  % smp length is equal to dim_2ndargument
        if( ~isempty(opt.k) && numel(varargin) == 0 );
            k = opt.k;
        elseif( isempty(opt.k) && numel(varargin) == 1 );
            k = varargin{1};
        elseif( isempty(opt.k) && numel(varargin) == 0 );
            k = rand();
        else;
            error( 'gallery_matrixset:mejstrikcn', 'Wrong options used.' ); end;
        M{1} = [1 1; 0 1]; 
        M{2} = [0 0; 1/k*exp(1+1/k) 0]; 
     elseif( check(what, {4,'mejstriklongsmp','mej2052','mej2052a'}) );  % smp length goes to infinity as dim_2ndargument goes to zero.
        if( ~isempty(opt.k) && numel(varargin) == 0 );
            k = opt.k;
        elseif( isempty(opt.k) && numel(varargin) == 1 );
            k = varargin{1};
        elseif( isempty(opt.k) && numel(varargin) == 0 );
            k = rand();
        else;
            error( 'gallery_matrixset:mejstrikcn', 'Wrong options used.' ); end;
        M{1} = [1 1; 0 1];
        M{2} = [0 0; k 0];
     elseif( check(what, {5,'mej20ex31','mej2031'}) );
        M{1} = [0 0;1 1];
        M{2} = [1 1;0 1];
    end;
end

function [ M ] = getmatrix_papers_misc( opt, varargin );
    if( check(opt.what, {'papers'}) );
        opt.what = []; end;
    M = {};
    if( isempty(opt.what) );
        opt.what = randi( [1 15] ); end;
    what = opt.what;
     % Mejstrik -  2020 - Improved invariant polytope algorithm and applications
     if( false );
     elseif( check(what, {1,'cex','haremorrissidorovtheys', 'hmst'}) );  % jsr finiteness conjecture counterexample      
        M{1} = sym( [1 1; 0 1] );  
        M{2} = sym( '0.749326546330367557943961948091344672091327370236064317358024' ) * [1 0; 1 1]; 
    elseif( check(what, {2,'cex2'}) );  % jsr finitesness conjecture counterexample
        M{1} = [1 1; 0 1]; 
        M{2} = 0.7493265463303675579439619*[1 0; 1 1]; 
    elseif( check(what, {3,'cex3'}) );  % jsr finitesness conjecture counterexample
        M{1} = [1 1; 0 1]; 
        M{2} = 0.75*[1 0; 1 1];            
    elseif( check(what, {'code'}) );  % takes very long time for random parameters, thus we do not allow it for a random choice
        M = codecapacity( varargin{:}, 'v',opt.verbose-1 ); 
    elseif( check(what, {5,'euler','euler_binary','eulerbinary','euler2'}) );
        M = matrix_euler2( varargin{:} );
    elseif( check(what, {6,'ones'}) );  % matrices of ones - should be the same as euler. But this is not tested yet    
        if( isempty(opt.N) );
            opt.N = randi( [2 5] ); end;
        M = transitionmatrix( {ones(1, opt.dim)', opt.N}, 'Omega',0:opt.dim-1, 'nocheck' );
        M = cellfun( @transpose, M, 'uniformoutput', 0 ); 
    elseif( check(what, {7,'daub'}) );  % Transition matrices of difference scheme for Daubechies wavelets.         
        M = daubechiesmatrix( opt.dim ); 

    elseif( check(what, {8,'dd','dd8','dubucdeslauriers'}) ); % Dubuc Deslauriers 8-point scheme  % Prot 2016 p29
        a = [-5    30   -56   -14   154   -14   -56    30    -5]; 
        M = transitionmatrix( {a',2,[0 1]}, 'Omega',0:7 ); 
        M = cellfun( @transpose, M, 'uniformoutput', 0 ); 
    elseif( check(what, {9,'morrisp3','mor3'}) );  % A RAPIDLY-CONVERGING LOWER BOUND FOR THE JOINT SPECTRAL RADIUS VIA MULTIPLICATIVE ERGODIC THEORY, IAN D. MORRIS
        M{1} = [2 2; 0 0]; 
        M{2} = [1 1; 1 1]; 

    elseif( check(what, {10,'grip'}) );  % Gripenberg
        M{1} = [0 1; 0 0]; 
        M{2} = 1/5*[0 0; 1 0]; 
    elseif( check(what, {11,'gripp52','grip52'}) );  % Gripenberg, p52  % long smp=1111111111112 %sic
        M{1} = 1/5*[3 0; 1 3]; 
        M{2} = 1/5*[3 -3; 0 -1];
        
     elseif( check(what, {12,'jungers16','jungers16_1','lsr','lsr1'}) );
         if( numel(varargin) >= 1 && ~isempty(varargin{1}) );
             k = varargin{1};
         else;
             k = 8; end;
         M = {[1 1;0 1], [0 0;-1/k 1]};  % this set has lsr = 0, but it converges to jungers16_2, which has lsr = 1
         
     elseif( check(what, {13,'jungers16_2','lsr2'}) );
        M = {[1 1;0 1], [0 0;0 1]};  % lsr = 1

     elseif( check(what, {13,'jungers14','lsr3'}) );
        M = {[2 0;0 4], [4 0;0 2]};  % sqrt(8) = LSR( M ) >=  max( LSR({2 4}), LSR({4 2}) ) = 2
        


    elseif( check(what, {14,'kozyakin2013', 'JSR25', 'Jungers25'}) );
        if( isempty(opt.dim) );
            if( opt.verbose >= 1 );
                fprintf( 'Give value of t as second argumnt. Use a random number in (0,1) now.\n' ); end;
            opt.dim = rand; end;
        t = opt.dim;
        M{1} = (1-t^4)*[1 -t/sqrt(1-t^2);0 0];
        M{2} = (1-t^4)*[sqrt(1-t^2) -t;t sqrt(1-t^2)];
        
     
        
    elseif( check(what, {15,'bear5'}) );
        M = [[0.0347222222; 0.0075483092; 0.0015096618; 0.0301932367; -0.0392512077; 0.0235507246; 0.5625; 0.0223429952; 0.0117753623; 0.0045289855; 0.0; -0.0078502415; 0.0012077295; 0.2264492754; 0.0132850242; 0.0099637681; -0.009057971; 0.5434782609; -0.0060386473; 0.0175120773; 0.0175120773; -0.0172101449; 0.0036231884; -0.0486111111; 0.0; 0.0301932367; 0.1736111111; 0.0377415459; 0.0075483092; 0.1509661836; -0.1962560386; 0.1177536232; 0.0; 0.1117149758; 0.0588768116; 0.0226449275; 0.0; -0.0392512077; 0.0060386473; 0.1947463768; 0.0664251208; 0.0498188406; -0.0452898551; 0.2173913043; -0.0301932367; 0.0875603865; 0.0875603865; -0.0860507246; 0.018115942; -0.2430555556; 0.0; 0.1509661836] [4.1319444444; 0.9965655193; -0.1435688406; 5.4445954106; -5.4703351449; 2.6030344203; -5.0078125; 2.7158061594; 1.4577672101; 0.6578351449; -0.078125; -1.3017059179; -0.720410628; -5.8660552536; 0.5615942029; 1.3014039855; -1.6646286232; -6.7629076087; -1.1201690821; 1.9464070048; 2.2780042271; -2.2940443841; -0.7810235507; -6.5538194444; 0.15625; 3.1668176329; 1.8055555556; 0.6763851147; 0.0206068841; 2.7472071256; -3.1217164855; 1.270946558; -3.08984375; 1.5596693841; 0.557348279; 0.4342164855; 0.0078125; -0.2760794082; -0.1654589372; -3.2227694746; 0.1313405797; 0.7261096014; -0.7147871377; -3.7924592391; -0.4400664251; 0.7970259662; 1.3846995773; -1.4924705616; -0.2906476449; -3.7717013889; -0.078125; 1.4833182367] [0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.3125; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0625; 0.0; 0.0; 0.0; 0.625; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0625; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.625; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.3125; 0.0; 0.0; 0.0; 0.625; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0] [0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.3125; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0625; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.625; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0625; 0.0; 0.0; 0.0; 0.3125; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0] [0.0; 0.0; 0.0; 0.0625; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0] [3.125; 0.5932971014; -0.027173913; 3.6231884058; -4.2934782609; 0.9510869565; -2.8125; 1.285326087; 0.4755434783; 0.5434782609; 0.0; -0.7336956522; -0.2717391304; -3.7635869565; 0.7608695652; 0.5706521739; -1.0869565217; -4.1576086957; -0.7246376812; 0.8514492754; 1.3722826087; -1.7527173913; -0.1902173913; -5.2083333333; 0.0; 1.3315217391; 1.5625; 0.2966485507; -0.0135869565; 1.8115942029; -1.8342391304; 0.4755434783; -1.40625; 0.6114130435; 0.2377717391; 0.2717391304; 0.0; -0.3668478261; -0.1358695652; -1.8817934783; 0.3804347826; 0.285326087; -0.5434782609; -2.0788043478; -0.3623188406; 0.4257246377; 0.5923913043; -0.8763586957; -0.0951086957; -2.6041666667; 0.0; 0.6657608696] [0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0625; 0.0062311292; 0.0099788647; 0.0832578502; -0.0516379831; 0.0005661232; -0.13046875; 0.0136246981; -0.0153419384; 0.0044157609; 0.0015625; -0.0540081522; 0.0032608696; -0.1159307065; 0.0178140097; 0.0013813406; -0.0281023551; -0.1497961957; -0.01977657; -0.0509812802; 0.016553442; -0.0227694746; 0.000928442; -0.0737847222; -0.015625; -0.0195199275] [-2.6041666667; -0.7300724638; 0.0498188406; -3.1702898551; 3.7047101449; -1.222826087; 2.8125; -1.5126811594; -0.6114130435; -0.4755434783; 0.0; 0.615942029; 0.2898550725; 3.410326087; -0.5615942029; -0.7336956522; 0.9510869565; 3.8722826087; 0.634057971; -0.9012681159; -1.4221014493; 1.8070652174; 0.2445652174; 4.7916666667; 0.0; -1.5036231884; -0.5208333333; -0.1585144928; 0.0099637681; -0.634057971; 0.740942029; -0.2445652174; 0.5625; -0.3025362319; -0.1222826087; -0.0951086957; 0.0; 0.1231884058; 0.0579710145; 0.6820652174; -0.1123188406; -0.1467391304; 0.1902173913; 0.7744565217; 0.1268115942; -0.1802536232; -0.2844202899; 0.5489130435; 0.0489130435; 1.4583333333; 0.0; -0.3007246377] [1.5625; 0.2966485507; -0.0135869565; 1.8115942029; -2.3967391304; 0.7880434783; -1.40625; 1.2364130435; 0.2377717391; 0.2717391304; 0.0; -0.3668478261; -0.1358695652; -1.8817934783; 0.3804347826; 0.285326087; -0.5434782609; -2.0788043478; -0.3623188406; 0.4257246377; 1.1548913043; -0.8763586957; -0.0951086957; -2.6041666667; 0.0; 0.7282608696; 3.125; 0.5932971014; -0.027173913; 3.6231884058; -4.6059782609; 1.0135869565; -2.8125; 1.535326087; 0.4755434783; 0.5434782609; 0.0; -0.7336956522; -0.2717391304; -3.7635869565; 0.7608695652; 0.5706521739; -1.0869565217; -4.1576086957; -0.7246376812; 0.8514492754; 1.6847826087; -1.7527173913; -0.1902173913; -5.2083333333; 0.0; 1.3315217391] [0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0625; 0.0; 0.3125; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0625; 0.0; 0.0] [1.1458333333; -0.1029966787; -0.2561896135; 1.4213466184; -1.9484450483; -0.0350996377; -1.5859375; 0.2802687198; -0.4862998188; 0.2887228261; 0.078125; 0.0568387681; -0.4438405797; -1.5247961957; 0.145531401; -0.3856431159; -0.4576539855; -1.8376358696; -0.3155193237; -0.1058272947; 0.5070199275; -0.5007925725; -0.3575634058; -2.0920138889; -0.15625; 0.1435688406; 3.7847222222; 0.6168855676; -0.299365942; 4.7592089372; -4.8571105072; 2.118432971; -4.0390625; 2.122509058; 1.1529664855; 0.5446105072; -0.015625; -1.3346165459; -0.9172705314; -4.9335371377; 0.5905797101; 0.8439764493; -1.5840126812; -5.6623641304; -1.1080917874; 1.7030495169; 1.8193689614; -1.7908740942; -0.8507699275; -5.8524305556; 0.15625; 2.7036533816] [-0.0763888889; -0.0074879227; -0.003442029; -0.0966183575; 0.0644927536; -0.0253623188; 0.1; -0.015942029; -0.0126811594; 0.0063405797; 0.0; 0.0265096618; 0.0016908213; 0.1170289855; -0.0202898551; -0.006884058; 0.0289855072; 0.1358695652; 0.0193236715; -0.0393719807; -0.0171497585; 0.0300724638; 0.0009057971; 0.0972222222; 0.0; -0.0743961353; -0.2361111111; -0.0233997585; -0.0123188406; -0.3019323671; 0.1952898551; -0.1105072464; 0.33125; -0.0623188406; -0.0490036232; -0.0036231884; 0.0; 0.0854468599; 0.0032004831; 0.3750905797; -0.0634057971; -0.0246376812; 0.0905797101; 0.4402173913; 0.0603864734; -0.1417874396; -0.0556763285; 0.0908514493; 0.0012681159; 0.2944444444; 0.0; -0.290821256] [0.1041666667; -0.1729128925; 0.0488375604; 0.2041817633; -0.24633907; -0.7501132246; -1.12890625; -0.3443916063; -0.9219316123; 0.0866168478; -0.0390625; 0.1024682971; 0.0960144928; -0.5793138587; 0.0422705314; -0.5177762681; -0.014379529; -0.8950407609; 0.0060386473; -1.1789704106; -0.0478940217; -0.0179461051; 0.0823143116; -0.1519097222; 0.078125; -1.0319293478; -0.3125; -0.3763964372; -0.0907306763; -0.3389190821; -0.1878774155; -1.2081068841; -0.6171875; -0.6584390097; -1.385303442; 0.1392663043; 0.015625; 0.7533967391; 0.0163043478; 0.0961277174; -0.0984299517; -0.9227807971; 0.1954257246; -0.0849184783; 0.1615338164; -1.3226147343; -0.1750452899; 0.1400588768; 0.0437047101; 0.1649305556; -0.15625; -1.3944746377] [-0.0416666667; -0.0133605072; 0.0150362319; -0.053442029; 0.015307971; -0.0366847826; 0.028125; -0.0287137681; -0.0245923913; 0.0013586957; 0.0; -0.0294384058; 0.0170289855; 0.0585597826; -0.0126811594; -0.0220108696; 0.0285326087; 0.0692934783; 0.0294384058; -0.0197463768; -0.0155797101; 0.0167119565; 0.0073369565; 0.0354166667; 0.0; -0.0367753623; -0.1041666667; -0.0473278986; 0.0561594203; -0.1268115942; 0.0086050725; -0.1426630435; -0.140625; -0.1000905797; -0.1025815217; 0.0122282609; 0.0; -0.1191123188; 0.0615942029; 0.0957880435; -0.0307971014; -0.0855978261; 0.0692934783; 0.0611413043; 0.0566123188; -0.0860507246; -0.044384058; 0.0441576087; 0.0285326087; 0.0729166667; 0.0; -0.1476449275] [0.6666666667; 0.0974637681; -0.1188405797; 0.9148550725; -0.4476449275; 0.1385869565; -2.475; 0.2061594203; 0.0380434783; 0.0434782609; 0.0; 0.1579710145; -0.1550724638; -1.5010869565; 0.1942028986; 0.0956521739; -0.2744565217; -2.2826086957; -0.1204710145; -0.031884058; 0.1889492754; -0.2402173913; -0.0652173913; -0.6666666667; 0.0; 0.0731884058; -0.3541666667; -0.0683876812; -0.0990942029; -0.3985507246; 0.4514492754; -0.2608695652; -0.39375; -0.2365942029; -0.1366847826; -0.0597826087; 0.0; 0.2327898551; -0.1242753623; -0.0453804348; -0.1920289855; -0.0940217391; 0.3070652174; -0.2364130435; 0.2672101449; -0.2561594203; -0.1894927536; 0.170923913; -0.135326087; 0.5041666667; 0.0; -0.381884058] [-5.2083333333; -0.9601449275; 0.0996376812; -6.3405797101; 7.4094202899; -2.4456521739; 5.625; -3.0253623188; -1.160326087; -0.9510869565; 0.0; 1.231884058; 0.5797101449; 6.8206521739; -1.1231884058; -1.1548913043; 1.902173913; 7.7445652174; 1.268115942; -1.8025362319; -2.8442028986; 2.6766304348; 0.4891304348; 8.3958333333; 0.0; -3.0072463768; -5.2083333333; -1.2726449275; 0.0996376812; -6.3405797101; 7.4094202899; -2.4456521739; 5.625; -3.0253623188; -1.222826087; -0.9510869565; 0.0; 1.231884058; 0.5797101449; 6.8206521739; -1.1231884058; -1.4048913043; 1.902173913; 7.7445652174; 1.268115942; -1.8025362319; -2.8442028986; 2.9891304348; 0.4891304348; 8.6458333333; 0.0; -3.0072463768] [-0.3125; 0.0430253623; -0.1684782609; -0.4528985507; 0.3804347826; 0.2717391304; 1.40625; 0.0815217391; 0.2921195652; -0.0679347826; 0.0; 0.3260869565; -0.1222826087; 0.8220108696; -0.0951086957; 0.1630434783; 0.1358695652; 1.222826087; 0.0905797101; 0.3623188406; -0.0543478261; 0.1019021739; -0.0543478261; 0.4166666667; 0.0; 0.3804347826; -0.625; 0.0860507246; -0.1494565217; -0.9057971014; 0.7608695652; 0.5434782609; 2.8125; 0.1630434783; 0.5842391304; -0.1358695652; 0.0; 0.027173913; -0.1820652174; 1.6440217391; -0.1902173913; 0.3260869565; 0.2717391304; 2.4456521739; 0.1811594203; 0.7246376812; -0.1086956522; 0.2038043478; -0.1086956522; 0.8333333333; 0.0; 0.7608695652] [0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0625; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; -0.0625; 0.0086050725; -0.0461956522; -0.0905797101; 0.0760869565; 0.0543478261; 0.28125; 0.0163043478; 0.058423913; -0.0135869565; 0.0; 0.2527173913; -0.0244565217; 0.1644021739; -0.0190217391; 0.0326086957; 0.027173913; 0.2445652174; 0.018115942; 0.0724637681; -0.0108695652; 0.0203804348; -0.0108695652; 0.0833333333; 0.0; 0.0760869565] [0.3125; 0.0311556461; 0.0498943237; 0.4162892512; -0.2581899155; 0.0028306159; -0.65234375; 0.0681234903; -0.076709692; 0.0220788043; 0.0078125; -0.2700407609; 0.0163043478; -0.5796535326; 0.0890700483; 0.0069067029; -0.1405117754; -0.7489809783; -0.0988828502; -0.254906401; 0.0827672101; -0.1138473732; 0.0046422101; -0.3689236111; -0.015625; -0.0975996377; 0.5208333333; 0.0152664553; 0.08959843; 0.7068991546; -0.4623716787; -0.1454936594; -1.26953125; 0.0401192633; -0.3071218297; 0.0526494565; -0.0078125; -0.411571558; 0.0452898551; -1.0433084239; 0.1509661836; -0.092504529; -0.2276947464; -1.3773777174; -0.1570048309; -0.6436443237; 0.1228487319; -0.1857450181; 0.0238903986; -0.6206597222; 0.078125; -0.3625452899] [0.3819444444; 0.003321256; 0.0256642512; 0.5132850242; -0.1672705314; -0.0996376812; -0.5625; -0.1201690821; 0.0126811594; 0.0769927536; 0.0; -0.1542874396; -0.0003019324; -0.6503623188; 0.1008454106; -0.018115942; -0.1539855072; -0.7608695652; -0.1026570048; 0.172705314; -0.0356280193; -0.1050724638; -0.0009057971; -0.3888888889; 0.0; 0.1799516908; -0.7361111111; -0.1365640097; 0.0089371981; -0.8212560386; 0.9926328502; -0.2155797101; 0.50625; -0.3577294686; -0.0765398551; 0.0018115942; 0.0; 0.1593599034; 0.0629830918; 0.7968297101; -0.186352657; -0.1210144928; 0.2463768116; 0.8423913043; 0.1642512077; -0.1263285024; -0.3679951691; 0.411865942; 0.0389492754; 1.2222222222; 0.0; -0.1879227053] [-0.0694444444; 0.1911043176; 0.028759058; 0.3060839372; -0.8024230072; 0.204370471; -0.69921875; 0.349071558; 0.0240602355; 0.1149230072; 0.0390625; 0.1002792874; 0.0410628019; -0.5468183877; -0.0969202899; 0.1486639493; -0.0449501812; -0.7014266304; -0.0143417874; 0.0624245169; 0.3480147947; -0.4822803442; -0.0148324275; -1.2196180556; -0.078125; 0.208861715; -0.3055555556; -0.00600468; -0.0102807971; -0.2281853865; -0.0342617754; -0.0073596014; 0.14609375; -0.0243432971; 0.0119451993; -0.0157382246; -0.0015625; -0.020116244; -0.0062801932; 0.1904325181; -0.0579710145; -0.0054574275; 0.0403306159; 0.1973505435; 0.0175120773; 0.0002566425; -0.0186669686; -0.0539968297; 0.0004302536; -0.2907986111; 0.015625; -0.0142964976] [-0.8333333333; -0.1524003623; -0.0190217391; -0.4846014493; 0.7758152174; -0.4279891304; 0.984375; -0.4252717391; -0.2139945652; -0.0883152174; 0.0; 0.1218297101; -0.0235507246; 0.8811141304; -0.3423913043; -0.1942934783; 0.0828804348; 1.120923913; 0.0656702899; -0.2998188406; -0.3414855072; 0.3512228261; -0.1019021739; 0.9895833333; 0.0; -0.5262681159; -0.1666666667; -0.0304800725; -0.0038043478; 0.0905797101; 0.1551630435; -0.0855978261; 0.196875; -0.0850543478; -0.042798913; -0.0176630435; 0.0; 0.024365942; -0.0047101449; 0.1762228261; -0.0684782609; -0.0388586957; 0.004076087; 0.2241847826; 0.013134058; -0.0599637681; -0.0682971014; 0.0702445652; -0.0203804348; 0.1979166667; 0.0; -0.1052536232] [-0.625; 0.0860507246; 0.1630434783; -0.9057971014; 0.7608695652; 0.5434782609; 2.8125; 0.1630434783; 0.5842391304; -0.1358695652; 0.0; -0.285326087; 0.0679347826; 1.6440217391; -0.1902173913; 0.3260869565; 0.2717391304; 2.4456521739; 0.1811594203; 0.7246376812; -0.1086956522; 0.2038043478; -0.0461956522; 0.8333333333; 0.0; 0.7608695652; -0.3125; 0.0430253623; 0.3940217391; -0.4528985507; 0.3804347826; 0.2717391304; 1.40625; 0.0815217391; 0.2921195652; -0.0679347826; 0.0; -0.2364130435; 0.5027173913; 0.8220108696; -0.0326086957; 0.1630434783; 0.1358695652; 1.222826087; 0.0905797101; 0.3623188406; -0.0543478261; 0.1019021739; 0.2581521739; 0.4166666667; 0.0; 0.3804347826] [0.1736111111; 0.0377415459; 0.0075483092; 0.1509661836; -0.1962560386; 0.1177536232; -0.3125; 0.1117149758; 0.0588768116; 0.0226449275; 0.0; -0.0392512077; 0.0060386473; -0.3677536232; 0.0664251208; 0.0498188406; -0.0452898551; -0.4076086957; -0.0301932367; 0.0875603865; 0.0875603865; -0.0860507246; 0.018115942; -0.2430555556; 0.0; 0.1509661836; 0.0347222222; 0.0075483092; 0.0015096618; 0.0301932367; -0.0392512077; 0.0235507246; -0.0625; 0.0223429952; 0.0117753623; 0.0045289855; 0.0; -0.0078502415; 0.0012077295; -0.0860507246; 0.0132850242; 0.0099637681; -0.009057971; -0.0815217391; -0.0060386473; 0.0175120773; 0.0175120773; -0.0172101449; 0.0036231884; -0.0486111111; 0.0; 0.0301932367] [0.0; 0.0; 0.0; 0.0; 0.0; 0.625; 0.0; 0.3125; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0625; 0.0; 0.0; 0.0; 0.0; 0.625; 0.3125; 0.0593297101; -0.0027173913; 0.3623188406; -0.4918478261; 0.7201086957; -0.28125; 0.7472826087; 0.0475543478; 0.0543478261; 0.0; -0.0733695652; -0.027173913; -0.3763586957; 0.0760869565; 0.0570652174; -0.1086956522; -0.4157608696; -0.0724637681; 0.0851449275; 0.4184782609; -0.1752717391; -0.0190217391; -0.5208333333; 0.0; 0.4456521739] [-1.2430555556; -0.1852355072; -0.0071859903; -1.4492753623; 1.4368357488; -0.2554347826; 1.08125; -0.411352657; -0.1214673913; -0.1757246377; 0.0; 0.3212560386; 0.0823067633; 1.5200181159; -0.318236715; -0.1532608696; 0.4347826087; 1.6630434783; 0.2898550725; -0.3072463768; -0.4711352657; 0.6198369565; 0.0510869565; 1.8916666667; 0.0; -0.4381642512; -0.6944444444; -0.0996376812; -0.0060386473; -0.8152173913; 0.7820048309; -0.1358695652; 0.625; -0.2143719807; -0.0679347826; -0.1639492754; 0.0; 0.184178744; 0.0437801932; 0.8650362319; -0.1781400966; -0.0815217391; 0.2445652174; 0.9510869565; 0.1630434783; -0.1811594203; -0.2506038647; 0.339673913; 0.027173913; 1.0416666667; 0.0; -0.2596618357]];
        M = mat2cell( M, [26 26], 26 )';
        
     elseif( check(what, {13,'morris17'}) );
         val = [varargin{:}];
         if( numel(val) == 3 );
             la = val(1);
             be = val(2);
             th = val(3);
         elseif( numel(val) == 2 );
             la = val(1);
             be = val(2);
             th = randu( [0 2*pi] );
         elseif( numel(val) == 0 );
             la = rand;
             be = rand;
             th = randu( [0 2*pi] );
         else;
             error( 'gallery_matrixset:morris17', '''morris17'' needs 0,2 or 3 arguments.' ); end;
        M = {[la be;0 0], [cos(th) -sin(th);sin(th) cos(th)]};
             
             
        
    end
end

function [ M ] = getmatrix_bad( opt, varargin );
    if( check(opt.what, {'bad'}) );
        opt.what = []; end;
    M = {};
    what = opt.what;
    if( isempty(what) );
        what = randi( 5 ); end;
    % Bad examples where something goes wrong
    if( false );
    elseif( check(what, {1,'nearlycomplex'}, opt) ); % pascal rhombus  % jsr=2; Prot2012, p40 
        M{1} = [0 1 0 0 0; 1 0 2 0 0; 0 0 0 0 0; 0 1 0 0 1; 0 0 0 2 1]; 
        M{2} = [1 0 2 0 0; 0 0 0 2 1; 1 1 0 0 0; 0 0 0 0 0; 0 1 0 0 0]; 
    elseif( check(what, {2,'prot2012p35','gp20132'}) );  % Prot2012, p35
        M{1} = [1 1; 0 1]; 
        M{2} = 9/10*[1 0; 1 1]; 
    elseif( check(what, {5,'prot2016p35'}) ); % Prot2016 p50 p35 p33 % matrices have common invariant subspace
        M{1} = 1/48*[48    0     0   -12     0     0;     0     0     0     0    12     0;     0     0     0     0     0    12;     0     0     0   -12     0     0;     0    12     0     0    36     0;     0     0    12     0     0    36;];
        M{2} = 1/48*[0    12     0     4     9    -4;    48    36    36   -92    -9    11;     0     0   -12   100     0   -25;     0     0     0    20     0    -8;     0     0   -48    80    48   -20;     0     0     0  -112     0    16];
        M{3} = 1/48*[0     0    12     4    -4     9;     0   -12     0   100   -25     0;    48    36    36   -92    11    -9;     0     0     0    20    -8     0;     0     0     0  -112    16     0;     0   -48     0    80   -20    48];
        M{4} = 1/48*[0    -4    -4   -12     5     5;     0    80  -100     0    80  -103;     0  -100    80     0  -103    80;    48    16    16   -12    16    16;     0   -80   112     0   -92   100;     0   112   -80     0   100   -92];            
    end;
end

function [ M ] = getmatrix_properties( opt, varargin );
    if( check(opt.what, {'propteries','property'}) );
        opt.what = []; end;
    M = {};
    what = opt.what;
    if( isempty(what) );
        what = randi( 1 ); end;
    % mortality matrices
    if( false )
    elseif( check(what, {1,'mortal','mortal1','mortal2','mortality','mortality1','mortality2'}, opt) );            
        if( numel(varargin) == 0 || isempty(varargin{1}) );
            k = randi(2); 
            fprintf( 'Choose mortal %i\n', k ); 
        elseif( isanyof(what, 'mortality1','mortal1') );
            k = 1;
        elseif( isanyof(what, 'mortality2','mortal2') );
            k = 2;            
        else;
            k = varargin{1}; end;
        switch k;
            case 1;  % https://math.stackexchange.com/questions/852424/mortality-problem
                M{1} = [ 0  1 -1; 0  1 -1; 1  0  1];
                M{2} = [-1  0  1;-1 -1  0;-1  1  1];
                % sminp = 1 2 2 1 1 1 2 2 2 2 1 1 2 1 2 2 1
            case 2;  % https://math.stackexchange.com/questions/852424/mortality-problem
                M{1} = [ 1 -1  0;-1  0 -1; 0  0  0];
                M{2} = [ 1 -1 -1; 1  0  0;-1 -1 -1];
                % sminp = 2 1 1 2 1 2 1 2 2 1 1 1 1 1 2 1 1 2
            otherwise;
                assert( isempty(opt.what), 'gallery_matrixset:mortal', 'For ''mortal'' only values of 1 and 2 are possible.' ); end; 
        end;
                    
end

function [ M ] = getmatrix_misc( opt, varargin );
    if( check(opt.what, {'misc'}) );
        opt.what = []; end;
    M = {};
    what = opt.what;
    if( isempty(what) );
        what = randi( 16 ); end;
    idx = what=='_';
    what(idx) = [];
    
    
    if( false );
    elseif( check(what, {1,'nearlycomplex'}, opt) );
        M = nearlycomplex(); 
    elseif( check(what, {2,'mejstrik'}, opt) );
        M{1} = [-0.0766   -0.7779   -0.4523; 0.5958    0.7105    0.7678; -0.8199    0.6780   -0.5047];
        M{2} = [0.2923    0.7024    0.6463; 0.7111   -0.1468   -0.1636; -0.0213   -0.2592    0.2707];
        M{3} = [0.6170   -0.2134   -0.3799; -0.7410   -0.4941    0.6517; 0.3961   -0.7559    0.5435]; 
   
    elseif( check(what,  {3,'mejstrik2'}, opt) );  % Example from my thesis
        M{1} = [2 -1; 2 0]; 
        M{2} = [0 -1; 2 2];    
    elseif( check(what,  {4,'mejstrik3'}, opt) );  % program works
        M{1} = [1 -1; 3 -2]; 
        M{2} = [1 3; -1 -1]; 
    elseif( check(what,  {5,'mejstrik4'}, opt) );  % program works
        M{1} = [1 4; 4 1]; 
        M{2} = [1 1; 0 -1]; 
        M{3} = [1 0; 1 -1];             
    elseif( check(what,  {6,'mejstrik5'}, opt) );  % _very_ long computation duration
        a = ((ones(1,15)*-1).^(1:15).*floor(sqrt(1:15)))'; 
        a(10) = -2;    
        %a(10) = -2.11    
        %a(10) = -2.237884497219498; (smallest?? value for which the matrix has real eigenvector
        M = transitionmatrix( getS({a,2},'nocheck'), 'Omega',0:13 );
    elseif( check(what,  {7,'mejstrik8'}, opt) );  % very long smp
        M{1} = [1 1 -1 1;1 0 1 0;-1 0 1 0;-1 -1 0 0];
        M{1} = M{1}/max(abs(eig(M{1})));
        M{2} = [1 1 0 0;1 1 -1 0;-1 1 0 -1; 1 -1 0 0];
        M{2} = M{2}/max(abs(eig(M{2})));
    elseif( check(what,  {8,'mejstrik9'}, opt) );  % same eigenvectors, every product is smp    
        M{1} = [1 1; 0 2];
        M{2} = [2 0; 1 1];
    elseif( check(what,  {9,'mejstrik10'}, opt) );
        % both matrices have two leading eigenvectors. 
        % feta/ipa both do not converge. 
        % At{1}^n*At{2} is the same matrix for all even n
        M{1} = [2 -1;-3 -2];
        M{2} = [3 -1;3 -3];

        % same behaviour
        % A{1} = 1/sqrt(6)*[2 -2;0 -1];
        % A{2} = 1/sqrt(6)*[2 -2;-1 -2];            
    elseif( check(what,  {10,'lotsmps'}, opt) );  % a huge amount of smp's %not working
        M = matrix_3(1, -2, 2 );
    elseif( check(what,  {11,'psp'}, opt) );  % diffscheme for pseudospline(2,3,2)  % program works
        a = 1/256*[3 -18 38 -18 3]';
        M = transitionmatrix( {a,2,[0 1]}, 'Omega',0:3 );
        M = cellfun( @transpose, M, 'uniformoutput',0 );
    elseif( check(what,  {12,'mejstrik945','mej945','945'}, opt) );  % smp length of 945?  oo = [repmat( 1,[1 940] ) repmat( 2, [1 5] )];
        M{1} = 1/8 * [8  -1   0;0   8   1;0   0   2];
        M{2} = 1/8 * [1   0   0;0   2   0;1   1   8];

    elseif( check(what,  {13,'laskawiec1'}, opt) );
        % two smps of length 6!
        M{1} = [1011/391, -(1547/205); 612/299, -(607/271)];
        M{2} = [-(616/293), 315/347; -1, -(616/293)];
    elseif( check(what,  {14,'laskawiec2'}, opt) );
         % two smps of length 6!
         M{1} = [299/126, -(1119/241);181/443, 517/511];
         M{2} = [-(169/400), 5593/962;-1, -(169/400)];
    
    elseif( check(what,   {15,'new1'}, opt) );  % interesting for new features of algorithm, both matrices are smps, three different leading eigenvectors
        M = {[1 0 ;0 -1], 1/sqrt(2)*[1 1;1 -1]}; 
        
    elseif( check(what,   {16,'eigenvalueproblem'}, opt) );
        % Hard to compute the exact number of leading eigenvectors. It is 17
        M = {[0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 1 0 0 0 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 1 0 0 0 0 0 0 0 0 5 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 0 0 0 0 0 0 1 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 5 0 0 0 0 0 0 0 0 0 2 0 0 0 0 2 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 6 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 1 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]};
        
        end;
end

function [ M, G ] = getmatrix_continuous( opt, varargin );
    M = {};
    G = [];
    what = opt.what;
    if( isempty(what) );
        what = randi( 2 ); end;
    idx = what=='_';
    what(idx) = [];
    
    
    if( false );
    elseif( check(what, {1,2,'kp4','kp5'}, opt) );
        A{1} = [-.3 .5;.2 -.4];
        A{2} = [-.6 0;0 1];
        if( check(what, {1,'kp4'}, opt) );
            m_ = 1;
            M_ = 2;
        else;
            m_ = 1;
            M_ = 2.5; end;
        if( ~isempty(opt.dim) );
            N = opt.dim;
        else;
            N = 10; end;
        len = linspace( m_, M_, N );
        for j = 1:2;
            for l = len;
               M{end+1} = expm( l * A{j} ); end; end;
        G = [zeros( N ) ones( N ); ones( N ) zeros( N )];
    end
end

function [ M ] = getmatrix_signal( opt, varargin );
    if( isempty(opt.N) );
        opt.N = randp( 1, 2, 4 ); end;
    if( isempty(opt.dim) );
        opt.dim = randp( 1, 2, 25, 1/opt.N ); end;

    if( check(opt.what, {'signal'}) );
        opt.what = []; end;
    M = {};
    what = opt.what;
    if( isempty(what) );
        what = randi( [1 12] ); end;
    idx = what=='_';
    what(idx) = [];
    
    
    if( false );
    elseif( check(what, {1,'signal_orthogonal','s_orthogonal','sorthogonal'}, opt) );
        for i = 1:opt.N; 
            M{i} = randomOrthogonal( opt.dim ); end;
        
    elseif( check(what, {2,'signal_hadamard','s_hadamard','shadamard'}, opt) );
        dim = opt.dim;
        dim_test = 2.^round( log2([dim dim/12 dim/20]) );
        [~,idx] = min( abs(dim - dim_test) );
        dim = dim_test( idx );
        for i = 1:opt.N;
            
            M{i} = hadamard( dim )/sqrt( dim ); end;
    
    elseif( check(what, {3,'signal_circulant','s_circulant','scirculant'}, opt) );
        for i = 1:opt.N; 
            R = fft( randn(opt.dim, 1) );
            R = R./abs( R );
            r = ifft( R );
            M{i} = circulant( r, randSgn(1) ); end;
        
    elseif( check(what, {4,'signal_householder','s_householder','shouseholder'}, opt) );
        for i = 1:opt.N; 
            M{i} = householderMatrix( rand(opt.dim, 1) ); end;
        
    elseif( check(what, {5,'signal_parallel','s_parallel','sparallel'}, opt) );
        for i = 1:opt.N; 
            M{i} = eye( opt.dim ); end;
        
    elseif( check(what, {6,'signal_series','s_series','sseries'}, opt) );
        for i = 1:opt.N; 
            M{i} = tril( randn(opt.dim), -1 )/opt.dim + eye( opt.dim ); end;
        
    elseif( check(what, {7,'signal_diagonalConjugated','s_diagonalConjugated','sdiagonalConjugated'}, opt) );
        for i = 1:opt.N;
            D = diag( randn(opt.dim, 1) );
            M{i} = D\randomOrthogonal( opt.dim )*D; end;
        
    elseif( check(what, {8,'signal_tinyRotation','s_tinyRotation','stinyRotation'}, opt) );
        for i = 1:opt.N;
            M{i} = tinyRotationMatrix( opt.dim, 0.01 ); end;
        
    elseif( check(what, {9,'signal_allpassInFDN','s_allpassInFDN','sallpassInFDN'}, opt) );
        for i = 1:opt.N;
            dim = round(opt.dim/2);
            g = rand( 1, dim )*0.2 + 0.6;
            A = randomOrthogonal( dim );
            M{i} = allpassInFDN( g, A, 0*g, 0*g, 0 ); end;
        
    elseif( check(what, {10,'signal_nestedAllpass','s_nestedAllpass','snestedAllpass'}, opt) );
        for i = 1:opt.N;
            g = rand( 1, opt.dim )*0.2 + 0.6;
            M{i} = nestedAllpass( g ); end;
        
    elseif( check(what, {11,'signal_SchroederReverberator','s_SchroederReverberator','sSchroederReverberator','signal_schroeder','s_schroeder','sschroeder'}, opt) );
        for i = 1:opt.N;
            dim = round( opt.dim/2 );
            allpassGain = rand( 1, dim )*0.2 + 0.6;
            combGain = ones( 1, dim );
            M{i} = SchroederReverberator( allpassGain, combGain, ones(dim, 1), ones(1, dim), 1 ); end;
        
    elseif( check(what, {12,'signal_AndersonMatrix','s_AndersonMatrix','sAndersonMatrix','signal_Anderson','s_Anderson','sAnderson'}, opt) );
        for i = 1:opt.N;
            M{i} = AndersonMatrix( opt.dim ); end;

    elseif( check(what, {13}) );
        for i = 1:opt.N;
            switch randi( [1 8] );
            case 1;       d = designfilt('bandpassiir','FilterOrder',randi([5 40]),'HalfPowerFrequency1',400,'HalfPowerFrequency2',450,'SampleRate',450 + 450*rand());
            case 2;       d = designfilt('bandpassiir','FilterOrder',randi([4 12]),'HalfPowerFrequency1',0.179620342112772,'HalfPowerFrequency2',0.28807127207179,'SampleRate',1,'DesignMethod','butter');
            case 3;       d = designfilt('highpassiir','FilterOrder',randi([3 14]),'HalfPowerFrequency',rand(),'SampleRate',1,'DesignMethod','butter');
            case 4;       d = designfilt('highpassiir','FilterOrder',randi([3 12]),'StopbandFrequency',rand(),'StopbandAttenuation',randi([1 120]),'SampleRate',1,'DesignMethod','cheby2');
            otherwise; fatal_error;
            end;
            M{i} = ss( d ); end;
    elseif( check(what, {14}) );
        for i = 1:opt.N;
            f0 = rand/2;
            f1 = f0 + (1/2 - f0)*rand;
            d = designfilt( rand_oneof('lowpassiir','highpassiir'), 'PassbandFrequency',f0, 'StopbandFrequency',f1, 'PassbandRipple', randi( [1 5] ), 'StopbandAttenuation', randi( [1 100] ), 'DesignMethod',rand_oneof( 'butter','cheby1','cheby2','ellip' ), 'MatchExactly','passband', 'SampleRate',1 );
            M{i} = ss( d ); end;
    elseif( check(what, {15}) );
        for i = 1:opt.N;
            pbf1 = rand/4;
            sbf1 = pbf1 + (1/4 - pbf1)*rand;
            sbf2 = sbf1 + (1/3 - sbf1)*rand;
            pbf2 = sbf2 + (1/2 - sbf2)*rand;
            d = designfilt('bandstopiir','PassbandFrequency1',pbf1,'StopbandFrequency1',sbf1,'StopbandFrequency2',sbf2,'PassbandFrequency2',pbf2,'PassbandRipple1',1,'StopbandAttenuation',randi([1 50]),'PassbandRipple2',1,'DesignMethod',rand_oneof( 'butter','cheby1','cheby2','ellip' ),'SampleRate',1);
            M{i} = ss( d ); end;
    elseif( check(what, {16}) );
        for i = 1:opt.N;
            b_r = randi( [1 20] );
            b_c = randi( [1 20]);
            a_c = randi( [1 b_c] );
            b = randn( b_r, b_c );
            a = randn( 1, a_c );
            if( a_c < b_c );
                a(end+1:b_c) = 0; end;
            
            M{i} = tf2ss( b, a ); end;
    end
end

function [ G ] = getmatrix_graph( M, opt, varargin );
    if( isempty(opt.graph) );
        G = [];
        return; end;
    
    randflag =  isempty(opt.what);
    
    what = opt.graph;
    if( isempty(opt.graph) );
        what = randi( 1 ); end;
    try;
        idx = what=='_';
        what(idx) = []; 
    catch me;  %#ok<NASGU>
        end;
    
    try;
        J = numel( M );
        if( ischar(what) || isstring(what) );
            % do nothing
            n = min( J, 2 );
        elseif( iscell(what) );
            n = what{2};
            what = what{1};
        else;
            what = 'default';
            n = min( J, 2 ); end;
        
        if( false );
        elseif( isanyof( what, {'block','biblock'} ) );            
            G = graph_block( n, J );

        elseif( isequal( what, 'default' ) || ...
                isanyof( what, {'cyclic','cycle','bicyclic','bicycle'} ) ...
              );
            G = graph_cycle( n, J );

        elseif( isanyof( what, {'cyclic_unbd','cycle_unbd','bicyclic_unbd','bicycle_unbd'} ) );
            G = graph_cycle_unbd( n, J );
            
        elseif( isanyof( what, {'rand','random','birand','birandom'} ) );
            G = graph_random( n, J );
            
        elseif( isanyof( what, {'perm','permute','permutation','biperm','bipermute','bipermutation'} ) );
            G = graph_permute( n, J );
            end;
            
     if( numel(what) >= 2 && isequal(what(1:2), 'bi') );
        G = ( G + G' ) ~= 0; end;
    catch me;
        if( ~randflag );
            rethrow( me ); end; end;
end

%% generating functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ T ] = matrix_jordan( r, c )
    d = max( r, c );
    T = [[zeros(d-1,1) eye(d-1) ];
          zeros(1,d)];
    T = T(1:r,1:c); 
end

function [ T ] = matrix_3( c0, c1, c2 );
    T{1} = [c0 0; c2 c1];
    T{2} = [c1 c0; 0 c2];
end

function [ T ] = matrix_euler2( varargin )
    % Euler binary partition function
    % length of smp == 2 if dim has even number of ones in binary expansion
    % length of smp == 1 if dim has odd number of ones in binary expansion
    if( numel(varargin) == 0 || isempty(varargin{1}) );
        r = 7;
    else;
        r = varargin{1}; end;
    r = r - 1;
    T{1} = zeros( r, r );
    T{2} = T{1};
    for i = 1:r
        for j = 1:r
            T{1}(i,j) = tif( 1<= 2*j-i && 2*j-i <= r+1, 1, 0 );
            T{2}(i,j) = tif( 0<= 2*j-i && 2*j-i <= r,   1, 0 ); end; end;
end

function [ TU ] = matrix_TU( dim, N )
    sze = dim;
    if( dim >= 3 );
        dim = 2; end;
    try;
        while( true );
            while( true );
                M = randi( [-4 4], dim );
                val = abs( eig(M) );
                if( min(val) <= 1 );
                    continue; end;
                if( abs(det(M)) ~= N );
                    continue; end;
                break; end;
            l = randi( ceil([sze 2*sze].^(1/dim)) );
            a = randi( [-5 5], l*ones(1, dim) );
            a = sequence( a );
            S = getS( 'a',a, 'M',M, 'verbose',-1 );
            S = normalizeS( S, 'verbose',-1, 'gauss' );         
            if( ~isempty(S) ); 
                break; 
            else;
                fprintf( '.' ); end; end;
        fprintf( '\n' );
        T = transitionmatrix( S );
        U = constructU( T, 0, 'verbose',-1 );
        TU = restrictmatrix( T, U, 'verbose',-1 );
    catch me;
        disp( me );
        fprintf( 2, 'Error while generating transition matrix.\n' );
        TU = []; end;
end

function [ TV0 ] = matrix_TV0( l, M )
%     dim = size( M, 2 );
    while( true );
        len = randi( [l 2*M*l] );
        a = randi( [-5 5], [len 1] );
        a = sequence( a );
        S = getS( 'a',a, 'M',M, 'verbose',-1, 'fix',1 );
        S = normalizeS( S, 'verbose',2, 'scale' );
        if( ~isempty(S) ); 
            break; end; end;
    [T, Om] = transitionmatrix( S );
    V0 = constructV( Om, 0 );
    TV0 = restrictmatrix( T, V0 );            
end

function [ T ] = matrix_stochastic( dim, N )
    T = cell( 1, N );
    for i = 1:N
        T{i} = normalizematrix( rand( dim ), 'colsum' ); end;
end

function [ T ] = matrix_doublestochastic( dim, N )
    T = cell(1,N);
    for i = 1:N
        T{i} = zeros( dim, dim );
        c = rand( 1, dim );
        c = c/sum( c );
        for k = 1:dim
            [~,idx] = sort( randperm(dim) ); 
            idx = idx + (0:dim-1)*dim;     
            T{i}(idx) = T{i}(idx) + c(k); end; end;
end

function [ T ] = matrix_stochastic_neg( dim, N )
    T = cell( 1, N );
    for i = 1:N
        T{i} = normalizematrix( rand( dim )-.5, 'colsum' ); end;
end

function [ T ] = matrix_doublestochastic_neg( dim, N )
    T = cell( 1, N );
    for i = 1:N
        T{i} = zeros( dim, dim );
        c = rand( 1, dim ) - .5;
        c = c/sum( c );
        for k = 1:dim
            [~,idx] = sort( randperm(dim) ); 
            idx = idx + (0:dim-1)*dim;     
            T{i}(idx) = T{i}(idx) + c(k); end; end;
end

function [ T ] = matrix_stochastic_pm( opt );
    T = cell( 1, opt.N );
    for i = 1:opt.N;
        T{i} = rand( [opt.row opt.col] )*2 - 1;
        nrm = sum( abs( T{i} ), 1 );
        T{i} = T{i}./nrm; end;
end

function [ val ] = matrix_unitary( dim, J )
    % this function generates a random unitary matrix of order 'n' and verifies
    val = cell( 1, J );
    for j = 1:J
       X = randn( dim ); % generate a random matrix 
       [Q,R] = qr( X ); % factorize the matrix
       R = diag( diag(R)./abs(diag(R)) );
       val{j} = Q*R; end; % unitary matrix         
end



function [ T ] = matrix_zerospectralradius( dim, J, verbose )

    if( dim == 0 );
        if( verbose  >= 0 );
            warning( 'gallery_matrixset:zero', 'Empty matrices with zero spectral radius are not defined. I return the empty matrix.' ); end;
        T = repcellm( [], [1 J] );
        return; 
    elseif( dim == 1 )
        T = repcellm( 0, [1 J] );
        return; end;

    T = cell( 1, J );   
    for j = 1:J
        %method = randi( [1 3] );
        method = 6;
        switch method;
            case 1 ;
                % generates nilpotent rank 1 matrices
                v = randn( dim, 1 );
                w = randn( dim, 1 );
                w(1) = 0;
                v(1) = 1;
                w(1) = -v'*w;
                T{j} = v*w';
            case 2;
                N = 3;
                while( true );
                    A = randi( N, dim ) - floor( N/2 );
                    while( rhot(A) ~= 0 );
                        idx = find( A );
                        n = max( min( ceil(numel(A)/9), ceil(numel(idx)/9)), 1 );
                        A( idx(randperm( numel(idx), n )) ) = 0; end;
                    if( anym(A) ~= 0 );
                        T{j} = A; end; end;

            case 3;
                % This is slower, and does not have better numerical properties
                jordan = [[zeros(dim-1,1) eye(dim-1) ];zeros(1,dim)];
                U = matrix_unitary( dim, 1 );
                U = U{1};
                T{j} = U*jordan*U';
                
            case 4;
                % this seems to be the best method
                blocks = randi( [1 dim] );
                len = rand( [1 blocks] );
                len = round( len/sum(len)*dim );
                
                while( sum(len)<dim );
                    idx = find( len, 1 );
                    len(idx) = len(idx) + 1; end;
                while( sum(len)>dim );
                    idx = find( len, 1 );
                    len(idx) = len(idx) - 1; end;
                
                jordan = cell( 1, blocks );
                for b = 1:blocks;
                    jordan{b} = [[zeros(len(b)-1,1) eye(len(b)-1) ];zeros(1,len(b))]; end;
                jordan = blkdiag( jordan{:} );
                
                range_ub = 1;
                range_lb = 0;
                while( true );
                    B = randi( [-round(range_lb) round(range_ub)], [dim dim] );
                    if( rank(B) == dim );
                        break; 
                    else;
                        range_ub = range_ub * 1.1; 
                        range_lb = range_lb * 1.1; end; end;
                if( dim<10 );
                    T{j} = round(adjoint(B))*jordan*B;
                else;
                    T{j} = B\jordan*B; end;
                    
            case 5;
                    B = rand( dim );
                    T{j} = B\triu( rand(dim), 1 )*B;
                
            case 6;
                % generates adjancy matrix of an acyclic graph
                T{j} = triu( rand(dim)<rand, 1 ) .* randn( [dim dim] );
                p = randperm( dim );
                T{j} = T{j}(p,p);
                %B = randn( dim );
                %T{j} = B\T{j}*B;
                

            otherwise;
                fatal_error; end; end;




end

function [ T ] = matrix_hurwitz( dim, J, ~ );
    T = cell( 1, J );
    for j = 1:J;
        X = randn( dim );
        [v,d] = eig( X );
        d2 = diag( d );
        idx = real(d2) > 0;
        d2(idx) = -d2(idx);
        T{j} = real( v  * diag( d2 ) / v );
        
        
%         D = diag( -rand(1, dim) * rand );
%         while( true );
%             V = randn( dim );  % random invertible matrix
%             if( cond(V)<dim^2 );
%                 break; end; end;
%         T{j} = V*D/V; 
        end;
end

function [ T ] = matrix_circulant( dim, J, ~ );
    T = cell( 1, J );
    for j = 1:J;
        v = randn( 1, dim );
        if( dim == 1 );
            T{j} = v;
        else;
            T{j} = gallery( 'circul', v ); end; end;
end

function [ T ] = matrix_toeplitz( dim, J, ~ );
    T = cell( 1, J );
    for j = 1:J;
        switch 0; % randi( [0 1] );
            case 0;
                r = randn( 1, dim );
                c = randn( 1, dim );
                c(1) = r(1);
                T{j} = toeplitz( r, c );
            case 1;
                r = randn( 1, dim );
                T{j} = toeplitz( r );
            otherwise;
                fatal_error; end; end;
end

function [ T ] = matrix_hankel( dim, J, ~ );
    T = cell( 1, J );
    for j = 1:J;
        switch randi( [0 1] );
            case 0;
                r = randn( 1, dim );
                c = randn( 1, dim );
                c(1) = r(end);
                T{j} = hankel( r, c );
            case 1;
                r = randn( 1, dim );
                T{j} = hankel( r );
            otherwise;
                fatal_error; end; end;
end

function [ T ] = matrix_normal( dim, J, ~ );
    T = cell( 1, J );
    for j = 1:J;
        switch randi( [0 1] );
            case 0;
                d = rand( 1, dim );
                
            case 1;
                d = randn( 1, dim );
            otherwise;
                fatal_error; end; 
        switch randi( [0 1] );
            case 0;
                O = rand( dim, dim );
            case 1;
                O = randn( dim, dim );                
            otherwise;
                fatal_error; end; 
       [O, ~] = qr( O );
       T{j} = O'*diag( d )*O; end;            
end

function [ T ] = matrix_orthogonal( dim, J, ~ );
    T = cell( 1, J );
    for j = 1:J;
        switch randi( [0 1] );
            case 0;
                O = rand( dim, dim );
            case 1;
                O = randn( dim, dim );                
            otherwise;
                fatal_error; end; 
       [O, ~] = qr( O );
       T{j} = O; end;            
end

function [ T ] = matrix_orthonormal( dim, J, ~ );
    T = matrix_orthogonal( dim, J );
    for j = 1:numel( T );
        for i = 1:size( T{j}, 2 );
            nrm = sum(T{j}(:,i).^2 );
            T{j}(:,i) = T{j}(:,i)/nrm; end; end;
end


function [ T ] = matrix_house( opt );
    if( opt.dim == 0 );
        T = repcellm( [], [1 opt.N] );
    else;    
        T = cell( 1, opt.N );
        E = eye( opt.dim );
        for i = 1:opt.N
            [v,beta] = gallery( 'house', randn(opt.dim, 1), randi(3)-1 );
            T{i} =  E - beta*(v*v'); end; end;
end

function [ val ] = matrix_complex( dim, N, type, verbose );

    if( isempty(type) );
        type = randi( [-12, 10] ); end;
    if( isempty(N) );
        N = randi( [2, 5] ); end;
    if( dim<0 );
        choice = dim;
    elseif( isstring(dim) || ischar(dim) );
        choice = dim;
    elseif( isstring(type) || ischar(type) );
        choice = type;
    elseif( iscell(type) );
        choice = type{1};
    else
        choice = type(1); end;

    if( dim<2 );
        if( verbose  >= 0 );
            warning( 'gallery_matrixset:zero', 'Matrices with complex eigenvalues must have at least dimension 2. I return the empty matrices.' ); end;
        val = repcellm( [], [1 N] );
        return; 
    end;

    %J=2, dim=2; %XX
    j2d2 = {};
    j2d2{end+1} = {[0 1;0 0],[0 -1;1 0]};
    j2d2{end+1} = {[0 1;0 0],[1 -1;1 1]};
    j2d2{end+1} = {[0 1;0 0],[1 1;-1 1]};
    j2d2{end+1} = {[0 1;0 1],[1 -1;1 1]};
    j2d2{end+1} = {[0 1;0 -1],[1 -1;1 1]};
    j2d2{end+1} = {[0 1;0 -1],[1 1;-1 1]};
    j2d2{end+1} = {[1 -1;1 1],[1 0;-1 0]};  %#ok<NASGU> % XX

    if( false );  % first do negative things
    elseif( check(choice, -11) );
        A=[ 0  0  0  0; 0  0 -3 -2; -3 -2  3 -5;  3 -5  0  0];
        B=[ 0  0  0 -3; 0 -3 -2  3; -2  3 -6  0; -5  0  0  0];  
        val = {A,B};
    elseif( check(choice, -10) );
        a = 1/210*[ 189, 90, 63, 245, 168, 21, 120, 147, -35, 42 ];
        M = [1 3;-2 -1];
        D =  [0 0;1 -1;2 -2;2 -1;3 -2]';
        Sd = getS({a,M,D});
        [T,Om] = transitionmatrix( Sd );
        V0 = constructVt( Om, 0 );
        val = restrictmatrix( T, V0 );
    elseif( check(choice, -9) );
        a = 1/9*[6 3 -3 0 3 6 3].';
        %a = 1/9*[6 2 -3 1 3 6 3].'; %has smp of length 2 !
        %a = 1/78*[ 39, -6, 26, 18, 65, 6, -13, -6, 26, 0, -39, 48, -39, -6, 39, 24, -26 ];
        %a = 1/78*[ 1/2, 9/13, -1/3, -3/13, 1, 8/13, -1/6, -2/13, 1/2, 1/13, -1/2];
        %a = 1/288*[ 72, 128, 72, -16, 63, 144, 0, 48, 81, -16];
        M = -2;
        D = [-1 0];
        Sd = getS({a,M,D});
        [T,Om] = transitionmatrix( Sd );
        V0 = constructVt( Om, 0 );
        val = restrictmatrix( T, V0 );

    elseif( check(choice, -8) );  % something goes wrong with type 'c2' and 'c3'
        val{1} = [-1 -6;23 -20]./[7 7;24 17];
        val{2} = [-13 3;-1 1]./[19 5 1 91];
    elseif( check(choice, -7) );  % something goes wrong with type 'c3'
        val{1} = [-0.149986651394040  -0.859119052376630; 0.958228924322373  -1.178553377472830];
        val{2} = [-0.684696254498354   0.605656859173540; -0.999878041051203   0.010982365741844];
    elseif( check(choice, -6) );  % example 6.1 from "Guglielmi, Wirth, Zennaro - 2005 - COMPLEX POLYTOPE EXTREMALITY RESULTS FOR FAMILIES OF MATRICES
        val{1} = [[cos(1) 0;0 cos(1)] [-sin(1) 0;0 -sin(1)];[sin(1) 0;0 sin(1)] [cos(1) 0;0 cos(1)]];
        val{2} = [[1/2 0;0 1/2] [0 1/2;-1/2 0];[0 1/2;-1/2 0] [1/2 0;0 1/2]];
    elseif( check(choice, -5) );  % example 4.1 from "Guglielmi, Zennaro - 2009 - Finding Extremal Complex Polytope Norms for Families of Real Matrices
        val{1} = [-3 -2 1 2;-2 0 -2 1;1 3 -1 -5;-3 -3 -2 -1];
        val{2} = [1 0 -3 -1;-4 -2 -1 -4;-1 0 -1 2;-1 -2 -1 2];
    elseif( check(choice, -4) );  % example 3.2 from "Guglielmi, Zennaro - 2009 - Finding Extremal Complex Polytope Norms for Families of Real Matrices
        val{1} = [-1/2 -sqrt(3)/2;sqrt(3)/2 -1/2];
        val{2} = [11/20 11/20;-11/20 -11/20];
    elseif( check(choice, -3) );  % example 3.1 from "Guglielmi, Zennaro - 2009 - Finding Extremal Complex Polytope Norms for Families of Real Matrices
        if( numel(type) == 1 );
            type(2) = 1; end;  % type(2) = beta; 0 < beta <= 1
        val{1} = [cos(1) -sin(1);sin(1) cos(1)]; 
        val{2} = type(2)*[sqrt(2)/2 sqrt(2)/2;0 0];
    elseif( check(choice, -2) );  % example from Protasov, The Barabanov norm is generically unique, simple, and easily computed, 2020, Equation(12)
        val{1} = [0 1;-1 0];
        val{2} = [.89 .646;-.129 -.178];
    elseif( check(choice, -1) );  % example from Protasov, The Barabanov norm is generically unique, simple, and easily computed, 2020, Equation(12)
        val{1} = [0 1;-1 0];
        val{2} = [0.34 1.046;-.523 .170]; 
    end;

    if( choice <= 0 || round(choice) ~= choice || choice > 10 );
        choice = randi( 4 ) + 3; end;        
        
    vprintf( 'Matrix_complex Choice: %i\n', choice, 'imp',[1 verbose] );
    if( false );
    elseif( check(choice, {0,1,2,'1','1rat'}) );
        % matrices with complex leading eigenvalue and findsmp reports all of them to be s.m.p.s
        val = {};
        i = 0;
        while( numel(val)<N && i<2000);
            j = 0;
            while( true );  % search for set all of which s.m.p.s
                i = i+1;
                j = j+1;
                if( i > 2000 );
                    if( verbose  >= 0 );
                        warning( 'tallery:complex', 'Failed to find set of matrices.' ); end;
                    return; end;
                if( j > 300 );
                    vprintf( '\n!', 'imp',[1 verbose] );
                    val(end) = [];
                    break; end;
                vprintf( '.', 'imp',[1 verbose] );
                switch choice;
                    case {1,'1'}; A = complexleadingmatrix( dim );
                    otherwise; A = complexleadingmatrix( dim, true ); end;  % '1rat'
                A = A / rhot( A );
                c = findsmp( [val A], 'v',-1 );
                if( isequal( c, num2cell(1:numel(val)+1)) );
                    val = [val A];
                    vprintf( '%i/%i', numel(val), N, 'imp',[1 verbose] );
                    break; end; end; end;
        vprintf( '\n', 'imp',[1 verbose] );

    elseif( check(choice, {3,4,'3','3rat'}) );
        % matrices where one of them has a complex leading eigenvalue and it is the s.m.p.
        val = {};
        i = 0;
        while( numel(val)<N && i<1000 );
            i = i + 1;
            vprintf( '.', 'imp',[1 verbose] );
            switch choice;
                case {3,'3'}; A = complexleadingmatrix( dim ); A = A/rhot(A);
                otherwise;    A = complexleadingmatrix( dim, true ); end;  % '3rat'
            c = findsmp( [val A], 'v',-1 );
            if( isequal(c,{[1]}) );  %#ok<NBRAK>
                val = [val A];
                vprintf( '%i/%i',numel(val), N, 'imp',[1 verbose] ); end; end;
        vprintf( '\n', 'imp',[1 verbose] );

    elseif( check(choice, {5,6,'5','5rat'}) );
        % matrices, the first has complex leading eigenvalue with rho=1, 
        % the others are scaled such that that the first is the smp-candidate
        %     type 4, the others have arbitrary leading eigenvector
        %     type 5, the others have complex leading eigenvector
        ACCURACY = 0.999999; %minimum accuracy
        val = {};
        i = 0;
        while( numel(val)<N && i<3000);
            l = .8;  % lower factor
            u = 1/l;  % upper factor
            switch type;
                case {5,'5'}; B = complexleadingmatrix( dim ); B = B/rhot( B );
                otherwise; B = complexleadingmatrix( dim, true ); end;  % '5rat'
            while( true && i<3000 );
                i = i+1;
                c = findsmp( [val B], 'v',-1 );
                if( ~numel(val) );
                    val = {B};
                    break;
                elseif( ~all(1 == cellfun('prodofsize',c)) || ~isequal(c{1},1) );
                    if( ~any(vertcat(c{:}) == numel(val)+1) )
                        if( verbose >= 0 );
                            warning( 'tallery:complex', 'Failed to find set of matrices.' ); end;
                        return; end;
                    vprintf( '-', 'imp',[1 verbose] );
                    B = B*l;
                    u = (u+0.2)/1.2;
                    u = max( u, 1/((1+ACCURACY)/2) );
                else;
                    vprintf( '+', 'imp',[1 verbose] );
                    if( l/u>ACCURACY ); 
                        val = [val B];
                        break; end;
                    B = B*u;
                    l = (l+0.2)/1.2; 
                    l = min( l, (1+ACCURACY)/2 );
                end; end;
            vprintf( '%i/%i',numel(val), N, 'imp',[1 verbose] ); end;
        vprintf( '\n', 'imp',[1 verbose] );

    elseif( check(choice, {7,'7','7rat'}) );
        % matrices, the first has complex leading eigenvalue with rho=1, 
        % the others are modified entrywise such that the first is the smp-candidate
        % type 6, the others have arbitrary leading eigenvector
        % type 7, the others may have complex leading eigenvector
        val = {};
        i = 0;
        while( numel(val)<N && i<1000 );
            NUMLOOPS = 14;
            f = 1.4;
            switch type;
                case {7,'7'};
                    B = complexleadingmatrix( dim ); B = B/rhot( B );
                otherwise;
                    B = complexleadingmatrix( dim, true ); end;  % '7rat'
            while( true && i<1000 );
                i = i+1;
                c = findsmp( [val B], 'v',-1 );
                idx = randperm( numel(B), floor((dim^2)/4) );
                if( ~numel(val) );
                    val = {B};
                    break;                
                elseif( ~all(1 == cellfun('prodofsize',c)) || ~isequal(c{1},1) );
                    vprintf( '-', 'imp',[1 verbose] );
                    B(idx) = B(idx)/f;
                else;
                    if( i>NUMLOOPS ); 
                        val = [val B];
                        break; end;
                    vprintf( '+', 'imp',[1 verbose] );
                    B(idx) = B(idx)*f;
                    f = (f+0.1)/1.1; end; end;
            vprintf( '%i/%i', numel(val), N, 'imp',[1 verbose] ); end;
        vprintf('\n', 'imp',[1 verbose] );

    elseif( check(choice, {8,'8'}) );
        % matrices where the product of two matrices has complex leading eigenvector
        val = {};
        i = 0;
        if( verbose  >= 0 );
            expect( isempty(N), 'gallery_matrixset:complex', '\nFor type 8 the number of matrices is random.\nSet N = [] to disable this warning.' ); end;
        vprintf( 'subtype: ', 'imp',[1 verbose] );
        j = 0;
        while( true && i<100000 );
            i = i + 1;
            A = complexleadingmatrix( dim ); 
            A = A/rhot( A );
            %A = randn( dim );
            while( true );  % try all matrix factorizations
                j = j + 1;
                switch j;
                    case 1; continue; %T = cell(1,2); [T{:}] = balance( A ); %[T{:}] = lu( A );
                    case 2; continue; %T = cell(1,3); [T{:}] = equilibrate( A ); %qr( A );
                    case 3; continue; %T = cell(1,2); [T{:}] = schur( A ); T{3} = T{1}';
                    case 4; continue; %T = cell(1,3); [T{:}] = svd( A );
                    case 5; continue; %T = cell(1,3); [T{:}] = poldecomp( A ); T = T([1,2]);
                    case 6; continue; %T = cell(1,3); [T{:}] = poldecomp( A ); T = T([1,3]);
                    case 7; continue; %T = cell(1,2); [T{:}] = frd( A );
                    case 8; 
                        T = cell(1,2); 
                        T{1} = complexleadingmatrix( dim ); 
                        T{1}=T{1}/rhot(T{1}); 
                        T{2} = T{1}\A;
                    case 9; continue;
%                         T = cell(1,2); 
%                         [T{:}] = deal( A ); 
%                         idx = randi( numel(T{1}) );
%                         T{1}(idx) = T{1}(idx)-abs(randn(1)*0.01); 
%                         idx = randi( numel(T{1}) );
%                         T{2}(idx) = T{2}(idx)-abs(randn*.01);
                    otherwise; j = 0; break; end;

                if( any(rhot(T).^numel(T) > rhot(A)) );
                    continue; end;
                %vprintf( '%i ', j, 'imp',[1 verbose] ); 
                %rhot( T )
                c = findsmp( T, 'v',-1, 'bound',[1+10*eps inf] );   
                %vdisp( c );
                v0 = leadingeigenvector( T, c, 'complexeigenvector',0, 'cycle',0, 'rep',0, 'multipleleadingeigenvector',0 ); 
                if( any(matrixinfo(v0,'real')) );
                    continue; end;
                c = findsmp( T, 'v',-1 );   
                %vdisp( c );
                v0 = leadingeigenvector( T, c, 'complexeigenvector',0, 'cycle',0, 'rep',0, 'multipleleadingeigenvector',0 ); 
                if( any(matrixinfo(v0,'real')) );
                    continue; end;                
                val = T;
                break; end;
            if( ~isempty(val) );
                vprintf( '%i/2 ', j, 'imp',[1 verbose] ); 
                break; end; end;
        vprintf( '\n', 'imp',[1 verbose] );

    elseif( check(choice, {9,'prot1'}) );
        if( numel(type) == 3 );
            al = type(2);
            be = type(3);
        else;
            al = 1;
            be = .5; end;
        if( iscell(al) );
            al = al{1};
            be = be{1}; end;
        Pxy = [1 0 0;0 1 0;0 0 0].';
        Pyz = [0 0 0;0 1 0;0 0 1].';
        Sxz = [0 0 1;0 1 0;-1 0 0].';
        Szx = [0 0 -1;0 1 0;1 0 0].';
        Rxa = [1 0 0;0 cos(al) sin(al);0 -sin(al) cos(al)].';
        Rzb = [cos(be) sin(be) 0;-sin(be) cos(be) 0;0 0 1].';
        val{1} = Rxa*Sxz*Pxy;
        val{2} = Rzb*Szx*Pyz;

    elseif( check(choice, {10,'prot2'}) );
        if( numel(type) >= 3 );
            al = type(2);
            be = type(3);
        else;
            al = sym(sqrt(2))*sym(pi);
            be = sym(sqrt(3))*sym(pi); end;
        if( numel(type) >= 4 );
            a = type(4);
        else;
            a = sym(9/10); end;
        if( iscell(al) );
            al = al{1};
            be = be{1}; end;  
        if( iscell(a) );
            a = a{1}; end;
        Rxa = [cos(al) sin(al);-sin(al) cos(al)].';
        Rxb = [cos(be) sin(be);-sin(be) cos(be)].';
        val{1} = Rxa;
        val{2} = Rxb*[a 0;0 1]; end;
    
     if( isempty(val) );
         warning( 'gallery_matrixset:complex', 'Failed to generator example.' ); end;
end

function [ val ] = nearlycomplex();
   val{1} = [
        0.260779273117590   0.321746900633509   0.430143562965100
        0.332728106032852   0.165237727924909   0.246741880562854
        0.435544012664021   0.460173223125738   0.331865771472727
        ];
   
   val{2} = [
        0.682274614607416  -0.373006400071818   0.529326054171797
        0.741899861356640   0.551196958575390  -0.263494595696068
       -0.371216501721239   1.002783855858494   0.524985866808493
       ];
end

function [ R, U, V ] = poldecomp( F );  %#ok<DEFNU>
    %POLDECOMP  Performs the polar decomposition of a regular square matrix.
    %   [R U V] = POLDECOMP(F) factorizes a non-singular square matrix F such
    %   that F=R*U and F=V*R, where
    %   U and V are symmetric, positive definite matrices and
    %   R is a rotational matrix
    C = F'*F;
    [Q0, lambdasquare] = eig( C );
    lambda = sqrt(diag((lambdasquare))); % extract the components
    Uinv = repmat(1./lambda',size(F,1),1).*Q0*Q0';
    R = F*Uinv;
    U = R'*F;
    V = F*R';
end

function [ K, L ] = frd( X );  %#ok<DEFNU>
% FRD - Full rank factorization of input matrix X. 
% [K L] = frd( X ) will write X as the product of two matrices X = KL where both K and
% L have the same rank as X
% https://de.mathworks.com/matlabcentral/fileexchange/19197-full-rank-factorization
    [U, S, V] = svd(X) ;
    [n,p] = size(S);
    D1 = zeros(n,p);
    D2 = zeros(p,p);
    a = diag(S);
    sqrt_a = a.^(.5) ;
    for i = 1:length(a)
        D1(i,i) = sqrt_a(i);
        D2(i,i) = sqrt_a(i); end;
    K = U*D1 ;
    L = D2*V' ;
end

%% signal matrices
%%%%%%%%%%%%%%%%%%%%

% Code taken from fdnToolbox, (c) Sebastian Jiro Schlecht
function [ Q ] = randomOrthogonal( n );
% Generates a random n x n orthogonal real matrix
%
% Matrix distributed according to the Haar measure over the group of
% orthogonal matrices. The Haar measure provides a uniform distribution
% over the orthogonal matrices.
%
% Based on Nick Higham
% https://nhigham.com/2020/04/22/what-is-a-random-orthogonal-matrix/

    [Q, R] = qr( randn(n) );
    Q = Q*diag( sign(diag(R)) );
end

function [ r ] = randSgn( varargin );
%randSgn - Generate random sign (-1/+1), wrapper for randi
%
% Syntax:  r = randSgn(varargin)
%
% Inputs:
%    varargin - same as randi
%
% Outputs:
%    r - random sign value
%
% Example: 
%    r = randSgn(3)
%    r = randSgn(4,2)
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: 
% Author: Dr.-Ing. Sebastian Jiro Schlecht, 
% Aalto University, Finland
% email address: sebastian.schlecht@aalto.fi
% Website: sebastianjiroschlecht.com
% 28 December 2019; Last revision: 28 December 2019

    r = randi( [0 1], varargin{:} )*2 - 1;
end

function [ H ] = householderMatrix( u );
%householderMatrix - Create Householder matrix from vector
%
% Syntax:  H = householderMatrix(u)
%
% Inputs:
%    u - Vector u which is orthogonal to the reflection hyperplane
%
% Outputs:
%    H - Householder transformation matrix
%
% Example: 
%    H = householderMatrix(randn(4,1))
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: 
% Author: Dr.-Ing. Sebastian Jiro Schlecht, 
% Aalto University, Finland
% email address: sebastian.schlecht@aalto.fi
% Website: sebastianjiroschlecht.com
% 28 December 2019; Last revision: 28 December 2019

    u = u(:);
    u = u / norm(u);

    N = length(u);
    H = eye(N) - 2*(u*u');
end

function [ rotationMatrix ] = tinyRotationMatrix( N, delta, varargin );
%tinyRotationMatrix - orthogonal matrix with a small eigenvalue angles
%
% Syntax:  rotationMatrix = tinyRotationMatrix(N,delta)
%
% Inputs:
%    N - Matrix size 
%    delta - Mean normalized eigenvalue angle
%    spread - (optional) Spreading of eigenvalue angle 
%    logMatrix - (optional) Initial logarithm matrix
%
% Outputs:
%    rotationMatrix - output matrix
%
% Example: 
%    rotationMatrix = tinyRotationMatrix(4,1/48000,0)
%    rotationMatrix^48000 is identity
%
% Author: Dr.-Ing. Sebastian Jiro Schlecht, 
% Aalto University, Finland
% email address: sebastian.schlecht@aalto.fi
% Website: sebastianjiroschlecht.com
% 17. January 2020; Last revision: 17. January 2020

    % Input Parser
    p = inputParser;
    p.addOptional('spread',0.1);
    p.addOptional('logMatrix',randn(N));
    parse(p, varargin{:});

    spread = p.Results.spread;
    x = p.Results.logMatrix;

    % Generate skew symmetric matrix
    skewSymmetric = (x - x')/2;

    [v,e] = eig(skewSymmetric);

    % Make complex conjugate
    IDX = nearestneighbour(v,conj(v));
    frequencySpread = 2*(rand(N,1)-0.5) * spread + 1;
    frequencySpread = (frequencySpread(IDX) + frequencySpread) / 2;
    frequencySpread( IDX - 1:N == 0 ) = 0; % non-complex eigenvalue 

    % Scale and spread
    E = diag(e);
    nE = E ./ abs(E) .* frequencySpread * delta * pi;

    % Create orthogonal matrix
    skewSymmetric = real(v* diag(nE)*v');
    skewSymmetric = (skewSymmetric - skewSymmetric') / 2;

    rotationMatrix = expm(skewSymmetric);

    % Alternative
    % rotationMatrix = v*diag(exp(nE))*v';
    % nearestOrthogonal

end

function [ matrix, inputGain, outputGain, direct ] = allpassInFDN( g, A, b, c, d );
%allpassInFDN - Create allpass in FDN of size [2N,2N]
% see Schlecht, S. (2017). Feedback delay networks in artificial
% reverberation and reverberation enhancement
%
% Syntax:  [matrix, inputGain, outputGain, direct] = allpassInFDN(g, A)
%
% Inputs:
%    g - Array of feedforward/back gains
%    A - feedback matrix 
%    b - input gains 
%    c - output gains
%    d - direct gains
%
% Outputs:
%   matrix - FDN feedback matrix
%   inputGain - FDN input gains
%   outputGain - FDN output gains
%   direct - FDN direct gain
%
% Example: 
%    allpassInFDN(randn(3,1),randomOrthogonal(3))
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: isAllpassFDN
% Author: Dr.-Ing. Sebastian Jiro Schlecht, 
% Aalto University, Finland
% email address: sebastian.schlecht@aalto.fi
% Website: sebastianjiroschlecht.com
% 28 December 2019; Last revision: 28 December 2019

    G = diag(g);
    I = eye(size(G));

    matrix = [-A*G, A; I - G^2, G];
    inputGain = [b; 0*b];
    outputGain = [g; c];
    direct = d;

end

function [ matrix, inputGain, outputGain, direct ] = nestedAllpass( g );
%nestedAllpass - Create nested allpass as FDN 
%
% Syntax:  [matrix, inputGain, outputGain, direct] = nestedAllpass(g)
%
% Inputs:
%    g - Array of feedforward/back gains
%
% Outputs:
%   matrix - FDN feedback matrix
%   inputGain - FDN input gains
%   outputGain - FDN output gains
%   direct - FDN direct gain
%
% Example: 
%    nestedAllpass(randn(3,1))
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: isAllpassFDN
% Author: Dr.-Ing. Sebastian Jiro Schlecht, 
% Aalto University, Finland
% email address: sebastian.schlecht@aalto.fi
% Website: sebastianjiroschlecht.com
% 28 December 2019; Last revision: 28 December 2019

    % Initialize feedforward/back allpass filter 
    matrix = g(1);
    inputGain = 1-g(1)^2;
    outputGain = 1;
    direct = -g(1);

    % Iterative nesting of feedforward/back allpass filters
    for it = 2:length(g)
        [matrix, inputGain, outputGain, direct] = nestingFDNinAllpass(g(it), matrix, inputGain, outputGain, direct);
    end
end

function [ N_matrix, N_inputGain, N_outputGain, N_direct ] = nestingFDNinAllpass( allpassGain, matrix, inputGain, outputGain, direct );
    % Nest a FDN within an feedforward/back allpass filter 

    N_matrix = [matrix, inputGain; outputGain*allpassGain, direct*allpassGain];

    N_inputGain = [ 0*inputGain; 1 - allpassGain^2];

    N_outputGain = [ outputGain, direct];

    N_direct = -allpassGain;

end

function [matrix, inputGain, outputGain, direct] = SchroederReverberator(allpassGain, combGain, b, c, d);
%SchroederReverberator - Create combs and allpass as FDN
% see Schlecht, S. (2017). Feedback delay networks in artificial
% reverberation and reverberation enhancement
%
% Syntax:  [matrix, inputGain, outputGain, direct] = SchroederReverberator(g, A)
%
% Inputs:
%    allpassGain - Array of feedforward/back gains for allpass
%    combGain - feedback gains for parallel comb filters
%    b - input gains of comb filters
%    c - output gains of comb filters
%    d - direct gains of comb filters
%
% Outputs:
%   matrix - FDN feedback matrix
%   inputGain - FDN input gains
%   outputGain - FDN output gains
%   direct - FDN direct gain
%
% Example: 
%    
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: isAllpassFDN
% Author: Dr.-Ing. Sebastian Jiro Schlecht, 
% Aalto University, Finland
% email address: sebastian.schlecht@aalto.fi
% Website: sebastianjiroschlecht.com
% 28 December 2019; Last revision: 28 December 2019

    [APmatrix, APinputGain, APoutputGain, APdirect] = seriesAllpass(allpassGain);

    P = diag(combGain);

    S = APinputGain * c;

    matrix = [P, 0*P; S, APmatrix];
    inputGain = [b; 0*APinputGain];
    outputGain = [APdirect*c, APoutputGain];
    direct = d * APdirect;

end

function A = AndersonMatrix(N, varargin);
%AndersonMatrix - Generates block circulant orthogonal matrix
% NxN block circulant matrix (only first off-diagonal) consisting of KxK
% matrices of type. 
%
% see Anderson, H., Lin, K., So, C., Lui, S. (2015). Flatter Frequency
% Response from Feedback Delay Network Reverbs Proc. Int. Comput. Music
% Conf. 2015(), 238 - 241.
%
% Syntax:  [A] = AndersonMatrix(N, varargin)
%
% Inputs:
%    N - Size of generated matrix A
%    K - (Optional) size of matrix blocks 
%    type - (Optional) matrix type of blocks
%
% Outputs:
%    A - Generated Matrix
%
% Example: 
%    A = AndersonMatrix(9,3,'circulant')
%    A = AndersonMatrix(8)
%
% Other m-files required: fdnMatrixGallery
%
% See also: fdnMatrixGallery
% Author: Dr.-Ing. Sebastian Jiro Schlecht, 
% Aalto University, Finland
% email address: sebastian.schlecht@aalto.fi
% Website: sebastianjiroschlecht.com
% 19. April 2020; Last revision: 19. April 2020

    % parse
    f = factor(N);
    p = inputParser;
    p.addOptional('K', f(1), @(x) isnumeric(x) && isscalar(x) && x >= 1 );
    p.addOptional('type','Hadamard',@(x) ischar(x) );
    parse(p, varargin{:});

    K = p.Results.K;
    type = p.Results.type;

    assert( mod(N,K) == 0, 'N needs to be dividable by K.');

    % generate
    numberOfBlocks = N/K;

    for it = 1:numberOfBlocks
        M{it} = fdnMatrixGallery(K,type, varargin);
    end

    A = blkdiag(M{:});
    A = circshift(A,K);
end


%% generating graph functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ G ] = graph_block( n, J );
    [len, n] = make_block_lengths( n, J, 1 );
    assert( sum(len) == J );
    blk = cell( 1, n );
    for i = 1:n;
        blk{i} = ones( len(i) ); end;
    G = ~blkdiag( blk{:} ); 
end

function [ G ] = graph_cycle( n, J );
    if( isscalar(n) );
        [len, n] = make_block_lengths( n, J, 1 );
    else;
        len = n; 
        n = numel( len ); end;
    assert( sum(len) == J );
    G = zeros( J, J );
    for i = 1:n;
        if( i<n );
            nr_z1 = sum(len(1:i));
            nr_o = len(i+1);
        else;
            nr_z1 = 0;
            nr_o = len(1); end;
        nr_z2 = J - nr_z1 - nr_o;
        nc = len(i);
        idxc_G = (1+sum(len(1:i-1))):sum(len(1:i));
        G(:,idxc_G) = [zeros( nr_z1,nc) 
                         ones( nr_o, nc )
                         zeros( nr_z2, nc)]; end;
end

function [ G ] = graph_cycle_unbd( n, J );
    if( isscalar(n) );
        [len, n] = make_block_lengths( n, J, 2 );
    else;
        len = n;
        n = numel( n ); end;
    assert( sum(len) == J );
    G = zeros( J, J );
    for i = 1:n;
        idx_row = (sum( len(1:i-1) ) + 1):(sum( len(1:i) ) - 1);
        if( i < n );
            idx_col = (sum( len(1:i) ) + 1):sum( len(1:i+1) );
        else;
            idx_col = 1:len(1); end; 
        G(idx_row,idx_col) = 1;

        idx_row = idx_row(end) + 1;
        idx_col = (sum( len(1:i-1) ) + 1):sum( len(1:i) );
        G(idx_row,idx_col) = 1;
    end;
end

function [ G ] = graph_random( n, J );
    while( true );
    %     f = factors( J );
    %     n = f( randi(numel(f)) );
        Go = rand( n, J ) < rand/log(J);
        G = Go;
        for i = 1:(J/n)-1;
            G = [G; circshift(Go, i*n, 2)]; end;
        ft = graph_precomponent( G, [], 1 );
        if( max(ft) == n );
            break; end; end;
end

function [ G ] = graph_permute( n, J );
    if( n == 2 && J == 2 );
        G = [0 1;1 0];
        return; end;
    
    len = make_block_lengths( n, J, 1 );
    E = cell( 1, numel(len) );
    for i = 1:numel( len );
        E{i} = ones( len(i) ); end;
    G = blkdiag( E{:} );
    G = G(:, randperm( J ) );   
end

%% post process functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ Tout, G ] = make_continuous_set( T, G, opt );
    assert( isempty(G), 'gallery_matrixset:graph', 'If a continuous set shall be generated, no graph must be generated.' );
    M = [];
    m = [];
    if( iscell(opt.continuous) && ischar(opt.continuous{1}) );
        args = opt.continuous;
        [ M, args ] = parsem( {'M','max','maxtime'}, args, [] );
        [ m, args ] = parsem( {'m','min','mintime','dwelltime','dwell'}, args, [] );
        [ N, args ] = parsem( {'N'}, args, 10 );
        [ selfflag, args ] = parsem( 'self', args, 1 );
        parsem( args, 'test' );
    elseif( isequal(opt.continuous, 1 ) );
        selfflag = 1;
        N = 10;
    else;
        selfflag = 1;
        N = opt.continuous; end;
    
    if( isempty(M) && isempty(m) );
        m = 1;
        M = 2;
    elseif( isempty(M) );
        M = 2*m;
    elseif( isempty(m) );
        m = M/2; end;
    
    if( selfflag );
        Tau = linspace( m, M, N-1 );
        assert( N >= 2, 'gallery_matrixset:graph', '''N'' must be at least 2 when ''self'' option is given.' );
    else;
        assert( N >= 1, 'gallery_matrixset:graph', '''N'' must be at least 1.' );
        Tau = linspace( m, M, N ); end;
    
    J = numel( T );
    
    Tout = {};
    for j = 1:J
        for tau = Tau;
            Tout{end+1} = expm( tau * T{j} ); end;
        if( selfflag );
            Tout{end+1} = Tout{end}; end; end;
    
    if( selfflag );
        G = graph_cycle_unbd( repmat( N, [1 J] ), numel(Tout) );
    else;
        G = graph_cycle( repmat( N, [1 J] ), numel(Tout) ); end;
    
    
end

%% utility functions
%%%%%%%%%%%%%%%%%%%%%%%%%

function [ len, n ] = make_block_lengths( n, J, minsze );
    J = J - (minsze-1)*n;
    if( ~isscalar(n) );
        len = n;
        assert( sum(len) == J );
        n = numel( n ); 
        return; end;
    assert( n<=J );
    while( true );
        len = rand( 1, n );
        len = floor( len/sum(len)*J );
        while( sum(len)<J );
            idx = randi( [1 n] );
            len(idx) = len(idx) + 1; end;
        if( nnz(len) == n );
            break; end; end;
    len = len + (minsze-1);
end

function [ ret ] = randx( varargin );
    switch randi( 5 );
        case 1;
            ret = randi( dist+1, varargin{:} );
        case 2;
            ret = randi( dist+1, varargin{:} ) - randi( dist+1, varargin{:} );
        case 3;
            ret = randn( varargin{:} );
        case 4;
            ret = dist( rand(varargin{:}), 1, inf ); 
        case 5; 
            ret = dist( rand(varargin{:}), 1, inf ) - dist( rand(varargin{:}), 1, inf ); 
    end;
    
%     switch randi(3);
%         case 1;
%             val = randi(9);
%             ret = round( ret/9 )*val;
%         case 2;
%             h = @(x) exp(exp(x)) - exp(exp(-x));
%             ret = h( ret );
%         case 3;
%             end; %do nothing
end

function [ ret ] = dist( varargin );
    if( nargin == 0 );
        x = rand;
        m = 0;
        M = inf;
    elseif( nargin == 2 );
        x = rand;
        m = varargin{1};
        M = varargin{2};
    else;
        x = varargin{1};
        m = varargin{2};
        M = varargin{3}; end;
        
    % function which gives random integers between m and M, 0<=m<=M<=inf
    k = .5;  % the higher k, the smaller the returned values
    e = .5;  % offset is necessary so that function works for m=0 too
    ret = round((m+e)./(1 + (m+e)./M.*x.^k - x.^k)-e/2);
end

function [ check ] = invariant( M, G, opt, checkflags );
    check = true;
    check = check && (isempty(opt.N)   || numel(M) == opt.N);
    check = check && (isempty(opt.dim) || ...
                      numel(M) >= 1 && isequal( size(M{1}, 1), opt.dim ));
    check = check && (isempty( G ) || ...
                      size(G, 1) == numel(M) && issquare(G) && ~isstring(G) && ~ischar(G));
    
    if( checkflags );
        
        check = check && (~opt.invertible   || allm( matrixinfo( M, 'cond' ) < opt.invertible ) );
        check = check && (~opt.positive     || allm( matrixinfo( M, 'nonzero' ) ) );
        check = check && (~opt.bool         || allm( matrixinfo( M, 'bool' ) ) );
        check = check && (~opt.finite       || allm( matrixinfo( M, 'finite' ) ) );
        if( opt.irreducible );
            try;
                MM = invariantsubspace( M );
                if( numel(MM) > 1 );
                    check = false; end;
            catch me;  %#ok<NASGU>
                check = false; end; end;
        check = check && (~opt.int          || allm( matrixinfo( M, 'int' ) ) );
        
        check = check && (~opt.norm         || allm( abs(matrixinfo( M, 'norm' )-1)<1e-9 ) );
        check = check && (~opt.pathcomplete || isempty(G) || ispathcomplete( G ) );
        check = check && (~opt.pos          || allm( matrixinfo( M, 'pos' ) ) );
        check = check && (~opt.real         || allm( matrixinfo( M, 'real' ) ) );
%         check = check && (~opt.rho        || allm( abs(matrixinfo( M, 'rho' )-1)<1e-9 ) );
        check = check && (~opt.sparsity     || allm( matrixinfo( M, 'sparsity' )<opt.sparsity ) );
        
    end
    
        
end

function [ ret ] = check( what, names, opt )
    if( nargin == 2 );
        verbose = 1;
    else;
        verbose = opt.verbose; end;
    if( isempty(what) );
        ret = false;
        return; end;
    switch lower( what );
        case names;
            if( isnumeric(what) && verbose >= 1 );
                for i = 1:numel( names )
                    if( ischar(names{i}) || isstring(names{i}) );
                        fprintf( 'Choice: %s\n', names{i} ); 
                        break; end; end; end;
            ret = true;
        otherwise;
            ret = false; end;
end

%% playground
%%%%%%%%%%%%%%%%%%%%

function playground;  %#ok<DEFNU>

    % nice bad example, funktioniert in Matlab2016a, aber nicht mehr in Matlab2017a
    % funktioniert in L'Aquila auch mit Matlab2017a
    M = [2 1; 0 3]; a0=[1 0 0; 1 1 1; 0 1 1];D=[     0     1     1     1     2     2;     0     0     1     2     1     2 ];
    S = getS( {1/6*convm(a0,a0),M,D}, 'bigcheck' );
    [T, ~] = transitionmatrix( S );
    U2 = double([1 0 0 -1 0 0 0 0; 0 0 1 0 0 -1 0 0; 0 0 0 0 1 0 0 -1; 1 0 -2 0 1 0 0 0; 0 1 0 -2 0 1 0 0; 0 0 1 0 -2 0 1 0; 0 0 0 1 0 -2 0 1]');
    TU2 = restrictmatrix( T, U2, 'outputdouble' );
    ipa( TU2, 'verbose',2, 'autoextravertex',0.01 )

    
    
    ipa( gallery_matrixset('cex2'),'plot','tree','verbose',2,'autoextravertex',0)
    ipa({[2 1; 0 -2],[2 0; -1 -2]},'invariantsubspace','none','maxsmpdepth',2);  % gut um selectordering zu testen
    ipa( {[2 1; 0 2],[2 0; -1 2]} ); % alle orderings mit Laenge kleiner gleich 7 haben rho == 2, darüber steigt er an auf 2.36007056204988
    ipa( gallery_matrixset('ani',5),'maxsmpdepth',1,'verbose',2,'invariantsubspace','none','plot','norm','autoextravertex',0,'clc','waitafterbalancing');  % Multivariates Subdivision Bsp das nur mit Balancing funktioniert. Gegenbeispiel zur Vermutung Protasovs.


    %ipa(gallery_matrixset('nondominantsmp'),'addtranspose','plot','polytope','nobalancing','verbose',2,'simplepolytope',1e-10,'testeigenplane',0,'testspectralradius',0,'findsmp_N',1000,'maxsmpdepth',500)
    ipa( gallery_matrixset('vale'),'maxsmpdepth',1,'autoextravertex',1,'plot','norm','clc','verbose',2,'simplepolytope',1,'clc','balancingdepth',3,'ordering',{[1]},'waitafterbalancing',1)  %#ok<NBRAK>
    
    
    % balancing problem
    A =[    -3     2;    -4    -3]; B =[     3     2;     0     0];
    ipa( {A,B}, 'plot','polytope' )
    
    % set which seems to have infinetly many smps with infinetly many leading eigenvectors
    A = [0 0 1;0 1 1;1 0 1]; B=[0 1 0;1 0 1;1 1 1];
    ipa( {A,B} );
    %
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
