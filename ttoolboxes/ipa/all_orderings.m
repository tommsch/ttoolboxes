function [ oo ] = all_orderings( values, maxlen, rootflag );
% Generates all possible combinatons of tuples from `values` of length up to `maxlen`
% [ oo ] = all_orderings( values, maxlen, [rootflag] );
%
% Input:
%   values      vector, The values used for generating the tuples. (experimental) If scalar is given, the 1:values is used as vector.
%   maxlen      integer, The maximal length of the tuple
%   rootflag    default = false, If true, representatives of necklaces are generated (i.e. cyclic permutations do count as the same tuple)
%
% Output:
%   oo          cell array of column vectors, all possible tuples
%
% Example:
%   all_orderings( 1:2, 2, true )   % yields {[1],[2],[1;2]}
%   all_orderings( 1:2, 2, false )  % yields {[1],[2],[1;1],[1;2],[2;1],[2;2]}
%
% Written by: tommsch, 2024-07-16
%   
    if( isscalar(values) );
        values = 1:values; end;

    if( nargin <= 2 || isempty(rootflag) );
        rootflag = false; end;

    oo = {};
    for l = 1:maxlen;
        oo_l = mixvector( values, l );
        if( rootflag );
            oo_l = simplify_ordering( oo_l );
            oo_l(:,sum( oo_l ~= 0, 1 ) ~= l ) = []; end;
        oo_l = num2cell( oo_l, 1 );
        for i = 1:numel( oo_l );
            oo_l{i}(oo_l{i} == 0) = []; end;
        oo = [oo oo_l]; end;  %#ok<AGROW>
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.

%#ok<*NOSEL,*NOSEMI,*ALIGN>
