function [ varargout ] = ipa_nfo( varargin );
% (experimental) helper function for evaluating nfo struct from `ipa`
% [ P ] = ipa_nfo( nfo, 'polytope' );
%   Output:
%   P       cell array of polytopes
%
% Written by: tommsch, 2023-05-29

% Changelog:    tommsch,    2025-01-20,     Readded to repository


    [nfo, opt, varargin] = parse_input( varargin{:} );
    switch opt.what
        case {'P','polytope'};
            argout = ipa_nfo_polytope( nfo, opt, varargin{:} );
            assert( nargout >= 1 );
        otherwise;
            error( 'ipa_nfo:mode', 'Wrong first argument given.' ); end;
        
    if( nargout > 0 );
        nout = min( nargout, numel(argout) );
        varargout = argout( 1:nout ); end;
        
end

function [ nfo, opt, varargin ] = parse_input( varargin );
    [~, varargin, opt.what] =   parsem( {'polytope'}, varargin );
    [opt.verbose, varargin] =   parsem( {'verbose','v'}, varargin, 1 );
    [opt.plot, varargin] =      parsem( {'plot'}, varargin, [] );  
    
    nfo = varargin{1};
    varargin(1) = [];
end

%%

function [ argout ] = ipa_nfo_polytope( nfo, opt, varargin );
    assert( numel(nfo.blockvar) == 1, 'ipa_nfo:blockvar', 'Currently ipa_nfo can only handle the case with exactly 1 blockvar.' );

    try;
        Pidx = [nfo.blockvar{1}.cyclictree.component_idx{:}];
    catch;
        Pidx = [nfo.blockvar{1}.cyclictree.Pidx{:}];
        end;
    nc = numel( unique( Pidx ) );
    nrm = [nfo.blockvar{1}.cyclictree.norm{:}];
    try;
        VV = [nfo.blockvar{1}.cyclictree.VV{:}];
    catch;
        VV = [nfo.blockvar{1}.cyclictree.V{:}];
        end;
    
    switch nfo.blockvar{1}.cyclictree.algorithm(1);
        case {'l','p','r','k'};
            eps = 5e-9;
        case {'c'};
            eps = 5e-5; 
        otherwise;
            fatal_error; end;

    idx  = nrm(2,:) >= eps;
    Pidx = Pidx(idx);
    VV   = VV(:,idx);

    P = cell( 1, nc );
    for i = 1:nc;
        idxi = Pidx == i;
        P{i} = VV(:,idxi); end;
    
    argout = {P};
end

%%

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
