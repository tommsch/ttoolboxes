function [ ct, errorcode, str ] = makecandidate( M, param );
% [ ct, errorcode, str ] = makecandidate( M, [param] );
% Computes all smp candidates and nearly candidates, as well a set of vectors 
% such that a non-degenerate polytope can be constructed using the leading eigenvectors and some extra-vertices
%
% Input:
%   M               cell array of NxN matrices, the matrices for which smp candidates and nearly candidates shall be computed
%                   if M is symbolic, then symbolic expressions for some data is also computed
%   param           a struct containing the following fields
%                   If a field is missing, default values are used
%       .ordering        cell array of matrices of column vectors, default={}, orderings which are used for candidates and nearly candidates
%       .v0              corresponding leading eigenvectors of the matrix products defined by oo
%       .v0s             corresponding dual leading eigenvectors of the matrix products defined by oo
%       .extravertex     cell array of Nx1 column vectors, extra vertices which are added 
%       .admissiblealgorithm
%       .algorithm
%       .lambda
%       .maxnum_candidate
%       .verbose
%       .epsequal
%       .epssym
%       .graph
%       .multipleeigenvector
%       .complexeigenvector
%       .nearlycandidate
%       .autoextravertex
%       .prebalancing
%       .findsmp.maxsmpdepth
%       .findsmp.N
%       .findsmp.nearlycanddelta
%       .findsmp.bound
%
% Output:
%   ct                          cyclictree-struct, containing the following fields
%       .Mt                     cell array of double matrices, processed and normalized input matrices. If input matrices M are symbolic, then Mt is also symbolic
%       .Mt_sym                 same as .Mt, does not exist if if symbolic computation is disabled
%       .algorithm              problem type/algorithm to use ( case (P), (R), (C), (L) )
%       .complexflag            logical array. True for entries where .v0 is complex valued
%       .d                      row vector, corresponding eigenvalues to eigenvectors
%       .epssym
%       .lambda                 the maximal averaged spectral radius of all products
%       .lambda_sym             same as lambda, does not exist if if symbolic computation is disabled
%       .num_matrix             number of input matrices
%       .num_component          number of polytopes
%       .ordering               cell array of matrices of column vectors, ordering of matrix products
%       .rho_ordering           row vector, the averaged spectral radius of the cooresponding product in .ordering
%       .rho_ordering_sym       same as rho_ordering, does not exist if if symbolic computation is disabled
%       .smpflag                row vector of integers, flag whether a corresponding product is a candidate, nearly-candidate, or extra-vector
%       .toa_ordering           time of action of corresponding orderings
%       .v0                     cell array of column vectors, the leading eigenvector of the cooresponding product in .ordering
%       .v0_sym                 same as v0, does not exist if if symbolic computation is disabled
%       .v0s                    cell array of column vectors, the dual leading eigenvector of the cooresponding product in .ordering
%       .v0_sym                 same as v0s, does not exist if if symbolic computation is disabled
%   
%   errorcode           value of `type` ipa_errorcode 
%   str                 string, contains text output from this function
%   
% Behaviour:
% ==========
%   if v0 is given, then it is assumed that blockvar.cyclictree.ordering is given already for multiple leading eigenvectors, whose multiplicity is one.
%   if v0s is given, v0 must be given too.
%   if v0 is not given, candidates are computed 
%   if M is symbolic, but v0or v0s  is not, a warning is printed
%
%  When M is not symbolic OR ~(param.epssym >= 0), the symbolic output variables may not computed or computed in type double.
%
% Written by tommsch, 2018
% Rewritten by tommsch, 2020

%               2020-07-15, tommsch,    Bugfix of <'complexeigenvector',3> option. New behaviour is experimental
%               2020-08-28, tommsch,    Added computation of symbolic candidates
%               2021-01-26, tommsch,    Added computation of limit matrices
%               2024-09-09, tommsch,    Removed .maxlengthordering
%               2024-10-04, tommsch,    Removed .num_matrix, .num_candidate, .num_nearlycandidate, .num_extravertex, .num_limitvertex
%                                       Computation of epssym is moved to this function
%               2024-10-08, tommsch,    Big refactoring
%                                       Code clenaup: Removed computation of limit matrices
%                                                     Removed .num_extramatrix
%                                                     Removed option 'addlimitpt', 'addlimitmatrix'
%                                                     Removed heuristic selection of leading eigenvectors
%               2024-10-15, tommsch,    Big refactoring
%                                       Moved Mt to ct struct to field .Mt
%                                       Added .Mt_sym field in ct struct
%               2024-10-22, tommsch,    Added .d field in ct struct
% Changelog:    2024-10-28, tommsch,    Removed .M, .M_sym field in ct struct

% YY Wenn mehrere orderings fuer einen cand gegeben sind, teste ob alle diese orderings den gleichen eigenvector haben

    if( nargin <= 1 || isempty(param) );   
        param = struct; end;
    
    param = initialize( M, param );
    ct = struct;  %#ok<NASGU>
    str = '';
    errorcode = ipa_errorcode.UNSETERROR;  %#ok<NASGU>
    
    % start algorithm
    %%%%%%%%%%%%%%%%%
    [ct, errorcode, str] = select_smp_candidates( M, param, str );
    if( errorcode );
        return; end; 
    
    [ct, str] = compute_lambda( M, ct, param, str ); 

    [ct, errorcode, str] = select_leading_eigenvectors( M, ct, param, str );
    if( errorcode ); 
        return; end;
    
    [ct, errorcode, str] = remove_duplicate_vector( ct, param, str );
    if( errorcode ); 
        return; end;

    [ct, str] = remove_nearlycand( ct, param, str );

    ct = prebalance( ct, param );

    [ct, str] = realify_eigenvectors( ct, param, str );

    [ct, errorcode, str] = remove_stuff_depending_on_case( M, ct, param, str );
    if( errorcode );
        return; end;

    [ct.algorithm, errorcode, str] = set_problem_type( [M{:} ct.v0{:}], ct, param, str );  % this needs to be done before we compute autoextravertices
    if( errorcode );
        return; end;

    [ct, errorcode, str] = tidy_up_vectors( ct, param, str );
    if( errorcode );
        return; end;

    [ct, str] = remove_interior_vector( ct, param, str );

    ct = scale_matrices( M, ct, param );

    [ct, errorcode, str] = compute_extravertices( M, ct, param, str );
    if( errorcode );
        return; end;

    [ct.polytopenorm.auxiliary_data, errorcode, str ] = compute_auxiliary_polytopenorm_data( M, ct, param, str );
    if( errorcode );
        return; end;
    
    ct = compute_epssym( ct, param );
    
    ct = compute_rho_ordering( M, ct, param );

    ct = compute_oclass( ct );    
    
    ct = make_output( M, ct, param );
    
    ct = orderfields( ct );

    if( ~isanyof( param.ordering, {'none'} ) && ...
        size( ct.smpflag, 2 ) == 0 && ...
        param.norestart < 2 ...
      );
        errorcode = ipa_errorcode.NOCANDIDATEFOUND;
        return; end;
    
    if( param.verbose >= 1 );
        [ str ] = printout( ct, param, str ); end;
    
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ ct, errorcode, str ] = select_smp_candidates( M, param, str );
    % reads parm.smpflag
    %           .ordering
    %           .toa
    % sets ct.smpflag
    %        .ordering
    errorcode = ipa_errorcode.UNSETERROR;
    ct.ordering = {};  % dummy value for better debugging experience

    str = vprintf( 'Select smp-candidates: ', 'imp',[2 param.verbose], 'str',str );
    str = vprintf( '\n', 'imp',[3 param.verbose], 'str',str );

    if( isanyof(param.ordering, {'none',('none')',{'none'},{('none')'}}) );
        ct.ordering = {};
        ct.smpflag = [];

    elseif( isempty(param.ordering) );
        assert( isempty(param.smpflag) && isempty(param.v0), 'makecandidate:input', 'If no ordering is given, no smpflag and no v0 must be given.' );
        if( numel(param.findsmp.bound) == 2 );
            bound = param.findsmp.bound;
        elseif( numel(param.findsmp.bound) == 1 );
            bound = [param.findsmp.bound inf];
        else;
            bound = []; end;
        str = vprintf( 'Find candidates. ', 'imp',[2 param.verbose], 'str',str );
        if( param.findsmp.maxsmpdepth > 0 );
            if( ~issym(M{1}) && param.findsmp.sym < 0 );
                symflag = 0;
            else;
                symflag = abs( param.findsmp.sym ); end;
            if( isempty(param.graph) );
                GG = [];
            else;
                GG = param.graph{1}; end;
            switch param.minmax;
                case {'min','max'};  % do nothing
                case {'kone'}; symflag = min( symflag, 3 );
                otherwise; fatal_error; end;
            reset_warning = onCleanup_lazy( @warning, warning('off','findsmp:numerical') );  %#ok<NASGU>  % temporarily disable warning 'findsmp:numerical'
            [c, nc, nfo] = findsmp( M, 'graph',GG, 'toa',param.toa, param.minmax, 'maxsmpdepth', param.findsmp.maxsmpdepth, 'v',param.findsmp.verbose, 'N',param.findsmp.N, 'bound',bound, 'nearlycanddelta',param.findsmp.nearlycanddelta, 'sym',symflag );  %#ok<ASGLU>
        else;
            [c, nc] = deal( {} ); end;
        
        ct.ordering = [c nc];
        ct.smpflag = [repmat( ipa_constant.candidate, 1, numel(c) ), ...
                      repmat( ipa_constant.nearlycandidate, 1, numel(nc) )];

    elseif( isempty(param.smpflag) );
        assert( isempty(param.v0) || numel(param.ordering) == numel(param.v0) );
        ct.ordering = param.ordering;
        ct.smpflag = repmat( ipa_constant.candidate, 1, numel(ct.ordering) );

    else;
        ct.ordering = param.ordering;
        ct.smpflag = param.smpflag;
        if( ~isequal( numel(ct.ordering), numel(ct.smpflag) ) );
            str = vprintf( 'The number of given smps and the number of given orderings is not the same (but should be).\nI have to quit.\n', 'cpr','err', 'imp',[0 param.verbose], 'str',str );
            errorcode = ipa_errorcode.BAD_OPTION; end; end;
    
    if( nnz( ct.smpflag == ipa_constant.candidate ) > param.maxnum_candidate && ...
        param.norestart < 2 ...
      );  % if too much candidates were found
        ct.ordering = ct.ordering(ct.smpflag == ipa_constant.candidate);
        errorcode = ipa_errorcode.TOOMUCHCANDIDATE;
        return; end; 
end

function [ ct, str ] = compute_lambda( M, ct, param, str ); 
    % reads param.lambda
    % sets  ct.lambda
    if( ~isempty(param.lambda) );
        ct.lambda = param.lambda;
        return; end;
    
    idx_Pi = ct.smpflag == ipa_constant.candidate;  
    if( ~any( ct.smpflag == ipa_constant.candidate ) );
        if( isempty(param.lambda) && ~isequal(param.JSR, [0 inf]) );
            ct.lambda = min( param.JSR );
        else;
            switch param.minmax;
                case 'kone';
                    str = vprintf( ['No smp given. Since the value of lambda is not important for case (K), it just use the value 1.' ...
                                    'You can set another value by providing the JSR with the option <''JSR'',value>.\n'], 'imp',[1 param.verbose], 'str',str );
                    ct.lambda = 1;
                case {'min','max'};
                    str = vprintf( ['No smp given. Compute spectral radius of s.m.p.-candidate by calling findsmp.\n' ...
                                    '  The found s.m.p.-candidate will not be used for the cyclic tree,\n' ...
                                    '  but merely to have a number for the JSR.\n' ...
                                    '  You can bypass this step by providing the JSR with the option <''JSR'',value>.\n'], 'imp',[1 param.verbose], 'str',str );
                    symflag = param.epssym >= 0;
                    if( isempty(param.graph) );
                        GG = [];
                    else;
                        GG = param.graph{1}; end;
                    c = findsmp( M, 'graph',GG, param.minmax, 'v',param.verbose-1, 'sym',symflag, 'nc',0 );
                    if( isempty(c) );
                        ct.lambda = 1;
                    else;
                        toa = buildsum_fast( param.toa, c{1} );
                        ct.lambda = rho( buildproduct_fast( M, c{1}  ) )^(1/toa); end;
            otherwise;
                fatal_error; end; end;
            str = vprintf( 'Chosen value for lambda: %v\n', ct.lambda, 'imp',[1 param.verbose], 'str',str );
        
    else;
        Pi = cellfun( @(x) buildproduct_fast(M, x), ct.ordering(idx_Pi), 'UniformOutput',false );
        rhoi = cast( zeros( 1, numel(Pi) ), 'like',Pi{1} );
        for i = 1:numel( Pi );
            toa = buildsum_fast( param.toa, ct.ordering{i} );
            switch param.minmax;
                case {'min','max'}; rhoi(i) = rhot( Pi{i} ).^(1/toa);
                case {'kone'}; rhoi(i) = rho_kone( Pi{i} ).^(1/toa);
                otherwise; fatal_error; end; end;
        
        switch param.minmax;
            case 'min';
                ct.lambda = real( min( real( rhoi ) ) );
            case {'max','kone'};
                ct.lambda = real( max( real( rhoi ) ) );
            otherwise; fatal_error; end; end;
end

function [ ct, errorcode, str ] = select_leading_eigenvectors( M, ct, param, str )
    % sets ct.ordering
    %        .v0
    %        .v0s
    %        .complexflag,
    %        .smpflag
    errorcode = ipa_errorcode.UNSETERROR;  %#ok<NASGU>
    if( isempty(param.v0) );
        [ct, errorcode, str] = select_leading_eigenvectors_compute_them( M, ct, param, str );
    else;
        [ct, errorcode, str] = select_leading_eigenvectors_from_user_input( M, ct, param, str );
        end;
end

function [ ct, errorcode, str ] = select_leading_eigenvectors_compute_them( M, ct, param, str );
    % reads .ordering
    % sets  .ordering
    %       .v0
    %       .v0s
    %       .complexflag
    %       .smpflag
    %       .d
    errorcode = ipa_errorcode.UNSETERROR;
    str = vprintf( 'Compute eigenvectors. ', 'imp',[2 param.verbose], 'str',str );
    cpev = param.complexeigenvector;
    if( numel(cpev) == 1 );
        cpev(2) = max( 2, param.complexeigenvector ); end;
    [v0,  v0s,  mult, cpflag,  oo,  ind,  d]  = leadingeigenvector( M, ct.ordering(ct.smpflag == ipa_constant.candidate),       'multipleeigenvector',param.multipleeigenvector, 'v',param.verbose-1, 'complexeigenvector',cpev(1), 'simpleconjugate', 'cycle',tif(param.epssym >= 0 || param.cycle, 0, 1), 'sym',param.leadingeigenvector.sym, 'normalization',param.minmax );  %#ok<ASGLU>
    [nv0, nv0s, ~,    ncpflag, noo, nind, nd] = leadingeigenvector( M, ct.ordering(ct.smpflag == ipa_constant.nearlycandidate), 'multipleeigenvector',param.multipleeigenvector, 'v',param.verbose-1, 'complexeigenvector',cpev(2), 'simpleconjugate', 'cycle',tif(param.epssym >= 0 || param.cycle, 0, 1), 'sym',param.leadingeigenvector.sym, 'normalization',param.minmax );  %#ok<ASGLU>

    for i = 1:numel( v0s );
        if( isAlways(norm(v0s{i}, 1 ) > 1e7, 'Unknown','true') || ...
            mult(i) > 1 || ...
            any( ~isfinite(v0s{i}) ) ...
          );
            %str = vprintf( 'Multiple leading eigenvectors occured. Balancing will not be possible.\n', 'imp',[1 param.verbose], 'str',str );
            v0s{i}(:) = 0; end; end;

    if( ~(param.epssym >= 0) );
        for i = 1:numel( v0 );
            v0{i} = double( v0{i} );
            v0s{i} = double( v0s{i} ); end;
        d = double( d );
        for i = 1:numel( nv0 );
            nv0{i} = double( nv0{i} );
            nv0s{i} = double( nv0s{i} ); end;
        nd = double( nd ); end;

    v0 =   remove_small_complex_part( v0,   1e-12*numel(v0)  );
    v0s =  remove_small_complex_part( v0s,  1e-12*numel(v0s) );
    nv0 =  remove_small_complex_part( nv0,  1e-12*numel(nv0)  );
    nv0s = remove_small_complex_part( nv0s, 1e-12*numel(nv0s) );

    switch param.algorithm(1);
        case {'k','K'};
            if( any(cpflag == 1) );
                errorcode = ipa_errorcode.COMPLEXKONE;
                return; end;

        case {'a','p','r','c','l'};
            % do nothing
        otherwise; fatal_error; end;

    rv0 = {};
    if( param.complexeigenvector == 3 );  % make all orderings/vectors to extra vertices
        idx_v0 = ~matrixinfo( v0, 'real' );
        if( any(idx_v0) );
            rv0 = [rv0 real(v0(idx_v0)) imag(v0(idx_v0))];
            oo(idx_v0) = [];
            v0(idx_v0) = [];
            v0s(idx_v0) = [];
            cpflag(idx_v0) = [];
            d(idx_v0) = []; end;

        idx_nv0 = ~matrixinfo( nv0, 'real' );
        if( any(idx_nv0) );
            rv0 = [rv0 real(nv0(idx_nv0)) imag(nv0(idx_nv0))];
            noo(idx_nv0) = [];
            nv0(idx_nv0) = [];
            nv0s(idx_nv0) = [];
            ncpflag(idx_nv0) = [];
            nd(idx_nv0) = []; end; end;

    ct.smpflag = [repmat( ipa_constant.candidate, 1, numel(v0) ), ...
                  repmat( ipa_constant.nearlycandidate, 1, numel(nv0) ), ...
                  repmat( ipa_constant.realvertex, 1, numel(rv0) )];
    ct.v0 = [v0 nv0 rv0];
    ct.v0s = [v0s nv0s num2cell( zeros( size([rv0{:}]) ), 1 )];
    ct.ordering = [oo noo cell( 1, numel(rv0) )];
    ct.complexflag = [cpflag ncpflag zeros(1, numel(rv0))];
    ct.d = [d nd];
end

function [ ct, errorcode, str ] = select_leading_eigenvectors_from_user_input( M, ct, param, str );
    % reads parm.v0
    %           .v0s
    % checks whether given vectors are indeed eigenvectors
    errorcode = ipa_errorcode.UNSETERROR;
    v0 = param.v0;
    v0s = param.v0s;
    assert( all( any( ct.smpflag == [ipa_constant.candidate 
                                     ipa_constant.nearlycandidate
                                     ipa_constant.realvertex
                                     ipa_constant.extravertex], 1 ) ), 'makecandidate:smpflag', 'Given smpflag must be less than 5.' );
    if( numel(ct.ordering) < numel(v0) );
        str = vprintf( 'Given number of orderings does not coincide with number of eigenvectors.\nYou probably want to use the option ''extravertex'' instead/additionaly to option ''v0''.', 'str',str );
        errorcode = ipa_errorcode.INPUTERROR;
        return; end;
    
    if( any(isvpa(M{1})) );
        epsilon = 10^(-digits()+5);
    elseif( issym(M{1}) );
        epsilon = 0;
    else;
        epsilon = 1e-10*norm( M{1} ); end;        
    
    for i = 1:numel( ct.ordering );
        if( ct.smpflag(i) == ipa_constant.extravertex );
            continue; end;
        MM = buildproduct_fast( M, ct.ordering{i} );
        [check, la] = iseigenvector( MM, v0{i} );
        if( numel( v0 ) >= i && ...
            ~isempty( v0{i} ) ...
          );
            assert( check, 'makecandidate:v0', 'Given vector is not an eigenvector. Index of vector: %i', i );
            assert( implies(ct.smpflag(i) == ipa_constant.candidate, isAlways(abs(abs(la)/rho(MM) - 1) <= epsilon)), 'makecandidate:v0', 'Given eigenvector is not an eigenvector to a leading eigenvalue. Index of vector: %i', i ); 
        else;
            error( 'makecandidate:fatal', 'Programming error.' ); end;  % this error should be caught somewhere before
        
        if( numel( v0s ) >= i && ...
            ~isempty( v0s{i} ) && ...
            any( v0s{i} ) ...
          );
            [check, la] = iseigenvector( v0s{i}', MM );
            assert( check, 'makecandidate:v0s', 'Given vector is not a dual eigenvector.' );
            assert( implies(ct.smpflag(i) == ipa_constant.candidate, isAlways(abs(abs(la)/rho(MM) - 1) <= epsilon)), 'makecandidate:v0s', 'Given dual eigenvector is not a dual eigenvector to a leading eigenvalue. Index of vector: %i', i );
            assert( isAlways(abs(abs(v0s{i}'*v0{i}) - 1) <= epsilon), 'makecandidate:v0s', 'Given dual eigenvector is not normalized correctly.\n  Index of vector: %i\n  Normalization: <v0, v0s>=%f', i, abs(v0s{i}'*v0{i}) ); end; end;
    
    ct.v0 = v0;
    ct.v0s = v0s;
    ct.complexflag = cellfun( @(x) any( isAlways(imag(x) ~= 0, 'Unknown','true' )), v0 );
    ct.d = nan( size(v0) );
end

function [ ct, errorcode, str ] = remove_duplicate_vector( ct, param, str );
    % searches for duplicates in ct.v0
    errorcode = ipa_errorcode.UNSETERROR;
    [~, ia, ~] = uniquecell( ct.v0 ); 
    ia = ia.';
    switch param.algorithm(1);
        case {'k','K'};
            % do nothing
        case {'a','p','r','c','l'};
            if( numel( ia ) ~= numel( ct.v0 ) );
                if( ~isfinite( param.epssym ) );
                    str = vprintf( 'Duplicate leading eigenvectors occured and symbolic computation is disabled. Algorithm may not terminate.\n', 'cpr',[.72 .43 0], 'imp',[1 param.verbose], 'str',str ); 
                elseif( param.delta ~= 1 );
                    str = vprintf( 'Duplicate leading eigenvectors occured, but delta ~= 1. Algorithm may or may not terminate. You may want to enable symbolic computations by hand.\n', 'cpr',[.72 .43 0], 'imp',[1 param.verbose], 'str',str ); 
                elseif( ~(param.epssym >= 0) );
                    str = vprintf( 'Duplicate leading eigenvectors occured.\n', 'cpr',[.72 .43 0], 'imp',[1 param.verbose], 'str',str ); 
                    errorcode = ipa_errorcode.DUPLICATELEADINGEIGENVECTOR;
                    return; end; end;
        otherwise;
            fatal_error; end;
    ct = keep_ct_entry( ct, ia );
end



function [ ct, str ] = remove_nearlycand( ct, param, str );
    % Remove nearly candidates
    if( ~isanyof( param.findsmp.nearlycanddelta, {'none','off', 0, 1} ) );
        return; end;
    str = vprintf( 'Remove nearly candidates. ', 'imp',[2 param.verbose], 'str',str );
    idx = ct.smpflag == ipa_constant.nearlycandidate;
    ct = keep_ct_entry( ct, ~idx );
end    

function [ ct ] = scale_matrices( M, ct, param );
     % reads M
     % sets ct.Mt
     %      ct.Mt_sym
    if( param.epssym >= 0 );
        ct.Mt_sym = cell( size(M) ); end;
    ct.Mt = cell( size(M) );  % normalized input matrices
    if( param.epssym >= 0 );
        ct.Mt_sym = ct.Mt; end;
    for i = 1:numel( M );
        la = real( double( ct.lambda ) );
        la_sym = ct.lambda;
        switch param.normalization;
            case {1,'default','smp'};
                ct.Mt{i} = double( M{i} )/double( la^param.toa(i) );
                if( param.epssym >= 0 );
                    ct.Mt_sym{i} = M{i}/la_sym^param.toa(i); end;

            case {'notoa','no_time_of_action','no_toa'}
                ct.Mt{i} = double( M{i} )/la;
                if( param.epssym >= 0 );
                    ct.Mt_sym{i} = M{i}/la_sym; end;

            case {'kone','rho'};
                if( param.epssym >= 0 );
                    ct.Mt_sym{i} = M{i}; end;  % This matrix is on purpose not scaled
                r = rho( double(M{i}) );
                if( isAlways(r > 1e-3, 'Unknown','false' ) );
                    ct.Mt{i} = double( M{i} )/r;
                else;
                    ct.Mt{i} = double( M{i} ); end;
                if( param.epssym >= 0 );
                    ct.Mt_sym{i} = M{i}; end;

            case {'off'};
                % do not scale
                if( param.epssym >= 0 );
                    ct.Mt_sym{i} = M{i}; end;
                ct.Mt{i} = double( M{i} );

            otherwise;
                fatal_error; end; end;
        
    % Pre-matrix/Pre-delta multiplication (Debug options)
    if( ~isempty(param.prematrixscaling) );
        assert( ismatrix(param.prematrixscaling) );
        for i = 1:numel( ct.Mt );
            ct.Mt{i} = param.prematrixscaling * ct.Mt{i};
            if( bv.param.epssym >= 0 );
                ct.Mt_sym{i} = param.prematrixscaling * ct.Mt_sym{i}; end; end; end;

    if( ~isempty(param.predelta) && ~isequal(param.predelta, 1) );
        for i = 1:numel( ct.Mt );
            ct.Mt{i} = param.predelta{i} * ct.Mt{i};
            if( bv.param.epssym >= 0 );
                ct.Mt_sym{i} = param.predelta{i} * ct.Mt_sym{i}; end; end; end;
end

function [ ct ] = prebalance( ct, param );
    % scales .v0
    % sets .prebalancing
    % Pre-balancing (Debug option)
    if( isempty(param.prebalancing) );
        ct.prebalancing = [];
        return; end;
    if( ischar(param.prebalancing) );
        assert( strcmp(param.prebalancing, 'rand') || strcmp(param.prebalancing, 'random') );
        ct.prebalancing = round(5040 * rand( 1, numel(ct.v0) ))/5040;
        if( anym( ~isreal([ct.v0{:}]) ) );
             ct.prebalancing = param.prebalancing + 1i*round(5040 * rand( 1, numel(ct.v0) ))/5040; end;end;
    
    assert( numel(ct.prebalancing) == numel(ct.v0), 'makecandidate:prebalancing:length', '\nLength of prebalancing vector wrong. Given length: %i, Needed length: %i', numel(param.prebalancing), numel(ct.v0) );
    for i = 1:numel( ct.v0 );
        ct.v0{i} = ct.v0{i} * ct.prebalancing(i); end;
end


function [ ct, str ] = realify_eigenvectors( ct, param, str );
    % reads .v0
    % may remove-and-add-new entries
    switch param.complexeigenvector;
        case {0,1,2};
            return;

        case {3,4};
            for i = numel( ct.v0 ):-1:1;
                if( isAlways( imag(ct.v0{i}) ~= 0, 'Unknown','true' ) );  % isreal does not work with symbolic input
                    ev_r = [real( ct.v0{i} ) imag( ct.v0{i} )];
                    ct.v0(i) = [];
                    ct.v0(end+1:end+2) = num2cell( ev_r, 1 );
                    ct.v0s(i) = [];
                    ct.v0s(end+1:end+2) = num2cell( zeros( size(ev_r) ), 1 );
                    ct.complexflag(i) = [];
                    ct.complexflag(end+1:end+2) = false;
                    ct.smpflag(i) = [];
                    ct.smpflag(end+1:end+2) = ipa_constant.extravertex;
                    ct.ordering(i) = [];
                    [ct.ordering{end+1:end+2}] = deal( [] );
                    end; end;
        otherwise;
            fatal_error; end;
end

function [ ct, errorcode, str ] = remove_stuff_depending_on_case( M, ct, param, str );
    errorcode = ipa_errorcode.UNSETERROR;
    nonneg = matrixinfo( M, 'nonneg' );
    switch lower( param.algorithm(1) );
        case 'a';
            if( all(nonneg) );
                [ct, errorcode, str] = remove_nonneg_eigenvectors( ct, str ); end;

        case {'p','l'};
            assert( all(nonneg), 'makecandidate:p', 'Case (P) is specified, but the matrices are not non-negative.' );
            [ct, errorcode, str] = remove_nonneg_eigenvectors( ct, str );

        case 'k';
            [ct, errorcode, str] = remove_nonneg_eigenvalues( ct, str );
            if( errorcode );
                return; end;
            [ct, errorcode, str ] = orientate_eigenvectors_for_kone( M, ct, param, str );
            if( errorcode );
                return; end;

        case {'r','c'};
            % do nothing

        otherwise;
            fatal_error; end;

end

function [ ct, errorcode, str ] = remove_nonneg_eigenvectors( ct, str );
    % May remove entries
    errorcode = ipa_errorcode.UNSETERROR;
    has_smp_before = nnz( ct.smpflag == ipa_constant.candidate ) > 0;
    idx = cellfun( @(x) any(isAlways(x < 0, 'Unknown', 'true')), ct.v0 );
    ct = keep_ct_entry( ct, ~idx );
    if( has_smp_before && ...
        nnz( ct.smpflag == ipa_constant.candidate ) == 0 ...
      )
        errorcode = ipa_errorcode.UNEXPECTED_NONNONNEGATIVE_VECTOR;
        return; end;
    if( ~all(ct.d) );
        errorcode = ipa_errorcode.UNEXPECTED_NONNONNEGATIVE_VECTOR; end;
end

function [ ct, errorcode, str ] = remove_nonneg_eigenvalues( ct, str );
    % May remove entries
    
    errorcode = ipa_errorcode.UNSETERROR;
    has_smp_before = nnz( ct.smpflag == ipa_constant.candidate ) > 0;
    if( issym(ct.d) );
        epsilon = 0;
    else;
        epsilon = 4*eps(ct.d)*size( ct.v0{1}, 1 ); end;
    idx = isAlways( ct.d < 0 | abs( imag(ct.d) ) > epsilon, 'Unknown','true' );
    ct = keep_ct_entry( ct, ~idx );
    if( has_smp_before && ...
        nnz( ct.smpflag == ipa_constant.candidate ) == 0 ...
      )
        errorcode = ipa_errorcode.UNEXPECTED_NONNONNEGATIVE_VECTOR;
        return; end;
end


 function [ ct, errorcode, str ] = orientate_eigenvectors_for_kone( M, ct, param, str );
    % reads .v0
    % sets  .polytopenorm.Kt  (only for case (K))
    % may remove candidates (and associated fields)

    errorcode = ipa_errorcode.UNSETERROR;

    switch lower( param.algorithm(1) );
        case {'k'};
            if( numel(ct.smpflag) == 0 );
                ct.polytopenorm.Kt = [];
                return; end;
            if( ~isfield(ct, 'polytopenorm') || ~isfield(ct.polytopenorm, 'Kt') );
                Kt = -approximate_kone( M, 'dual', 'verbose',param.verbose-1 );  % this field is only present if algorithm is 'k'
                ct.polytopenorm.Kt = double( Kt );
                if( ~iskone(ct.polytopenorm.Kt) );
                    errorcode = ipa_errorcode.NOINVARIANTKONE;
                    return; end; end;
            idx_c = ct.smpflag == ipa_constant.candidate;
            [ct.v0(idx_c),  ct.v0s(idx_c),  switchedflag_1, errorcode, str] = orientate_eigenvectors_for_kone_worker( ct.v0(idx_c),  ct.v0s(idx_c),  ct.polytopenorm.Kt, param, str );
            if( errorcode );
                return; end;

            idx_nc = ct.smpflag == ipa_constant.nearlycandidate;
            [ct.v0(idx_nc), ct.v0s(idx_nc), switchedflag_2, ~,         str] = orientate_eigenvectors_for_kone_worker( ct.v0(idx_nc), ct.v0s(idx_nc), ct.polytopenorm.Kt, param, str );

            idx_r = ct.smpflag == ipa_constant.realvertex;
            [ct.v0(idx_r), ~,                switchedflag_3, errorcode, str] = orientate_eigenvectors_for_kone_worker( ct.v0(idx_r),  [],             ct.polytopenorm.Kt, param, str );
            if( errorcode );
                return; end;

            idx_ev = ct.smpflag == ipa_constant.extravertex;
            [ct.v0(idx_ev),  ~,              switchedflag_4, ~,         str] = orientate_eigenvectors_for_kone_worker( ct.v0(idx_ev), [],             ct.polytopenorm.Kt, param, str );
            if( any( [switchedflag_1 switchedflag_2 switchedflag_3 switchedflag_4] ) );
                str = vprintf( 'Orientiation of some starting vectors is switched due to dual-kone requirements.\n  This is not a problem, just take this into account when checking the results.\n', 'imp',[1 param.verbose], 'str',str ); end;

            for i = numel( ct.v0 ):-1:1;
                if( isempty(ct.v0{i}) || ~any(ct.v0{i}) );
                    ct = remove_ct_entry( ct, i ); end; end;

        case {'a','p','l','c','r'};
            fatal_error;

        otherwise;
            fatal_error; end;

    if( numel(ct.smpflag) == 0 );
        errorcode = ipa_errorcode.KONE_ERROR; end;
end

function [ v, vs, switched_flag, errorcode, str ] = orientate_eigenvectors_for_kone_worker( v, vs, Kt, param, str );
    errorcode = ipa_errorcode.UNSETERROR;
    switched_flag = false;
    Kt = real( double( Kt ) );
    for i = 1:size( v, 2 );
        for j = 1:numel( v{i} );  % with this loop and intermediate vpa, this is faster and more robust
            v{i}(j) = vpa( v{i}(j) ); end;
        vi = real( double( v{i} ) );
        if( norm(vi, 1) < 1e-12 );
            continue; end;
        ip = sum( vi .* Kt, 1 );
        if( all(ip <= 0) );
            % do nothing
        elseif( all(ip >= 0) );
            v{i} = -v{i};
            switched_flag = true;
            if( ~isempty(vs) );
                vs{i} = -vs{i}; end;
        else;
            errorcode = ipa_errorcode.NOINVARIANTKONE_DUALTEST;
            v{i} = zeros( size(v{i}, 1), 0 );
            if( ~isempty(vs) );
                vs{i} = zeros( size(v{i}) ); end;  % XX Is a missing vs-vector stored as an empty set or as a zero vector?
            str = vprintf( 'I had to remove some vectors from the starting set, since I could not find a correct orientation.\n', 'imp',[1 param.verbose], 'str',str, 'cpr','err' ); end; end;
end

function [ algorithm, errorcode, str ] = set_problem_type( data_val, ct, param, str );
    % sets algorithm
    errorcode = ipa_errorcode.UNSETERROR;
    [algorithm, autoalgorithm, str_val] = ipa_problemtype( param.algorithm, tif(param.epssym >= 0, data_val, double(data_val)), 'cpflag',ct.complexflag, 'v',param.verbose );  % sets the value of type.info.algorithm
    if( ~isempty( param.admissiblealgorithm ) && ...
        ~searchincellarray( autoalgorithm, param.admissiblealgorithm ) ...
      );
        errorcode = ipa_errorcode.ADMISSIBLEALGORITHM;
        return; end;
    str = [str str_val];
end

function [ ct, str ] = remove_interior_vector( ct, param, str )  
% may remove entries from ct
    VV = double( [ct.v0{:}] );
    %clf; hold on;
    %center = center_kone( VV );
    %plotm( VV, 'center',center, 'Kone', 'hull', 'linewidth',2, 'r-' );
    nrm = polytopenorm( VV, VV, ct.algorithm, 'v',0, 'output','ub', param.debug{:} );
    idx_in = isAlways( nrm < 0.99999, 'Unknown','false' );
    if( ~isempty(idx_in) && all(idx_in) );
        idx_in(1) = false; end;
    %VV(:,idx) = [];
    %plotm( VV, 'center',center, 'Kone', 'hull', 'linewidth',2, 'g--' );
    ct = keep_ct_entry( ct, ~idx_in );
    if( nnz(idx_in) );
        str = vprintf( 'Removed %i/%i starting vectors\n', nnz(idx_in), numel(idx_in), 'str',str, 'imp',[1 param.verbose] ); end;
end

function [ ct, errorcode, str ] = compute_extravertices( M, ct, param, str );
    [ ct ] = add_extravertices( ct, param );
    [ ct, errorcode, str ] = compute_autoextravertices( M, ct, param, str );
end

function [ ct ] = add_extravertices( ct, param );
    if( isempty(param.extravertex) );
        return; end;
    
    if( param.epssym >= 0 );
        ct.v0  = [ct.v0 double(param.extravertex)];
        ct.v0s = [ct.v0s num2cell( zeros(size([param.extravertex{:}])), 1 )];

        if( ~isempty(param.extravertex) );
            for i = 1:numel( param.extravertex );
                ct.v0_sym{end+1}  = param.extravertex{i}; 
                ct.v0_syms{end+1}  = sym( zeros(size(ct.v0_sym{1}, 1), 1) );  end; end;  % []-notation does not work for symbolic expressions
        ct.ordering(end+1:end+numel( param.extravertex )) = {0};
        ct.smpflag(end+1:end+numel( param.extravertex )) = ipa_constant.extravertex;
        ct.complexflag(end+1:end+numel( param.extravertex )) = ~matrixinfo( param.extravertex, 'real' );
    else;
        ct.v0  = [ct.v0 param.extravertex];
        ct.v0s = [ct.v0s num2cell( zeros(size([param.extravertex{:}])), 1 )];
        ct.ordering(end+1:end+numel( param.extravertex )) = {0};
        ct.smpflag(end+1:end+numel( param.extravertex )) = ipa_constant.extravertex;
        ct.complexflag(end+1:end+numel( param.extravertex )) = ~matrixinfo( param.extravertex, 'real' ); end;
    
        
end

function [ ct, errorcode, str ] = compute_autoextravertices( M, ct, param, str );
    % may add entries to ct
    errorcode = ipa_errorcode.UNSETERROR;
    if( ~param.autoextravertex );
        return; end;
    vin = [ct.v0{:}];
    if( ~ipa_isgraph(param.graph) );
        dim = size( ct.Mt{1}, 1 );
        if( isempty(vin) );
            str = vprintf( 'No vertices are computed so far up to this point (compute_autoextravertices).\n  This can be a programming error, or due to a strange input set.', 'imp',[0 param.verbose], 'str',str );
            lastwarn( '', 'makecandidate:autoextravertex:empty' );
            return; end;
        M_d = cell( 1, numel(ct.Mt) );
        vin_d = double( vin );
        for i = 1:numel( ct.Mt );
            M_d{i} = double( ct.Mt{i} ); end;
        existing_vectors = zeros( dim, 0 );
        if( size(vin_d, 2) < dim*3 );
            for i = 1:numel( ct.ordering );
                for j = 1:numel( ct.ordering{i} );
                    existing_vectors = [existing_vectors rational(buildproduct_fast( M_d, ct.ordering{i}(1:j), vin_d ))]; end; end;  %#ok<AGROW>
        else;
            existing_vectors = vin_d; end;
        if( size(existing_vectors, 2) < dim*5 );
            lifting_depth = floor( log(dim) / log(numel(M_d)) );
            MMd = liftproductt( M_d, lifting_depth );
            for i = 1:numel( MMd );
                existing_vectors = [existing_vectors MMd{i}*vin_d]; end; end;  %#ok<AGROW>
        str = vprintf( 'Compute extra vertices. ', 'imp',[2 param.verbose], 'str',str );    
        idx = any( ~isfinite( existing_vectors ), 1 );
        existing_vectors(:,idx) = [];
        ev_a = extravertex( existing_vectors, ct.Mt, 'threshold',param.autoextravertex, 'problemtype',ct.algorithm, 'cell', 'v',param.verbose );
    else;
        ev_a = {};
        str = vprintf( 'Currently no auto extravertices can be computed when a graph is given.', 'imp',[0 param.verbose], 'str',str );
        lastwarn( '', 'makecandidate:autoextravertex' ); end;

    if( param.epssym >= 0 );
        ct.v0 = [ct.v0 double(ev_a)];
        ct.v0s = [ct.v0s num2cell( zeros( size([ev_a{:}]) ), 1 )];
        if( ~isempty(ev_a) );
            for i = 1:numel( ev_a );
                ct.v0_sym{end+1} = sym( ev_a{i} );
                ct.v0s_sym{end+1} = sym( zeros( size(ev_a{1},1), 1 ) ); end; end;
    else;
        ct.v0 = [ct.v0 ev_a];
        ct.v0s = [ct.v0s num2cell( zeros( size([ev_a{:}]) ), 1 )]; end;
    ct.smpflag = [ct.smpflag repmat( ipa_constant.extravertex,     1, numel(ev_a) )];
    ct.ordering = [ct.ordering cell( 1, numel(ev_a) )];
    ct.complexflag = [ct.complexflag false( 1, numel(ev_a) )];

    switch lower( ct.algorithm(1) );
        case 'k';
            [ct, errorcode, str] = orientate_eigenvectors_for_kone( M, ct, param, str );
        case {'p','l','c','r'};
        otherwise;
            fatal_error; end;
end

function [ ct, errorcode, str ] = tidy_up_vectors( ct, param, str );
    % 1) removes vectors depending on case: E.g. non-nonnegative eigenvectors stemming from multiple leading eigenvalues in case (P)
    % 2) removes small non-positive or complex parts which may happen sometimes
    % sets          .v0
    %               .v0_sym
    %               .v0s
    %               .v0s_sym
    % may remove entries from ct

    errorcode = ipa_errorcode.UNSETERROR;

    if( param.epssym >= 0 );
        ct.v0_sym = ct.v0;
        ct.v0 = double( ct.v0 );
        ct.v0s_sym = ct.v0s;
        ct.v0s = double( ct.v0s ); end;

    alg = lower( ct.algorithm(1) );
    for i = numel( ct.v0 ):-1:1;
        epsilon = 1e-12 * numel( ct.v0{i} );
        if( param.epssym >= 0 );
            epsilon_sym = tif( any(isvpa(ct.v0_sym{i})), 10^(-digits + 4), 0 ); end;
            
        if( ~ct.complexflag(i) );  % XX I do not know whether this is a good idea. Maybe I should get rid of .complexflag
            ct.v0{i} = real( ct.v0{i} ); end;

        % tidy up
        if( isequal(ct.smpflag(i), ipa_constant.nearlycandidate) );
            if( isanyof(alg, {'k','l','p','r'}) );
                if( norm( imag(ct.v0{i}), 1 ) > epsilon );
                    ct = remove_ct_entry( ct, i );
                    continue; end; end;
            if( isanyof(ct.algorithm(1), {'l','p'}) );
                if( norm( ct.v0{i}(ct.v0{i} < 0 ), 1 )> epsilon );
                    ct = remove_ct_entry( ct, i );
                    continue; end; end; end;
        
        if( isanyof(alg, {'k','l','p','r'}) );
            ct.v0{i} =  remove_small_complex_part( ct.v0{i}, epsilon );
            ct.v0s{i} = remove_small_complex_part( ct.v0s{i}, epsilon );
            if( param.epssym >= 0 );
                ct.v0_sym{i} =  remove_small_complex_part( ct.v0_sym{i}, epsilon_sym );
                ct.v0s_sym{i} = remove_small_complex_part( ct.v0s_sym{i}, epsilon_sym ); end; end;

        if( isanyof(alg, {'l','p'}) );
            ct.v0{i} =  remove_small_negative_part( ct.v0{i}, epsilon );
            ct.v0s{i} = remove_small_negative_part( ct.v0s{i}, epsilon );
            if( param.epssym >= 0 );
                ct.v0_sym{i} =  remove_small_negative_part( ct.v0_sym{i}, epsilon_sym );
                ct.v0s_sym{i} = remove_small_negative_part( ct.v0s_sym{i}, epsilon_sym ); end; end;
                
        if( isanyof(alg, {'l','p'}) );
            if( norm( ct.v0s{i}(ct.v0s{i} < 0), 1 ) > epsilon );
                ct.v0s{i}(:) = 0;; end; end;

        end;
        
    % find duplicates
    [~, ia, ~] = uniquecell( ct.v0, 'stable' );
    ct = keep_ct_entry( ct, ia );
end

function [ aux, errorcode, str ] = compute_auxiliary_polytopenorm_data( M, ct, param, str );
    errorcode = ipa_errorcode.UNSETERROR;
    switch param.minmax;
        case 'kone';
            [aux, errorcode, str] = compute_auxiliary_data_kone( M, [ct.v0{:}], ct, param, str );

        case {'min','max'};
            aux = {};
            
        otherwise;
            fatal_error; end;
end

function [ center, errorcode, str ] = compute_auxiliary_data_kone( M, K, ct, param, str );
    errorcode = ipa_errorcode.UNSETERROR;
    center = [];

    if( ~isempty(param.polytopenorm.auxiliary_data) );
        center = tocell( param.polytopenorm.auxiliary_data );
        return; end;
    
    flag = any( ct.complexflag(ct.smpflag == ipa_constant.candidate) );
    %c2 = center_kone( approximate_kone( M, K ), [], param.verbose - 1 );
    if( norm(imag(K)) < 1e-12*max(size(K)) );
        K = real( K ); end;
    for j = 1:numel( M );
        M{j} = double( M{j} ); end;
    if( flag );
        K = real( K ); end;
    for rounds = 1:5;
        if( size(K, 2) >= 100 );
            break; end;
        Knew = cell( 1, numel(M) );
        for j = 1:numel( M );
            Knew{j} = M{j}*K; end;
        K = [K Knew{:}]; end %#ok<AGROW>
    if( ~iskone( K ) );
        errorcode = ipa_errorcode.NOINVARIANTKONE;
        return; end;
    center = center_kone( K, 'verbose',(param.epssym >= 0) + param.verbose - 2 );
    center = {center};
end

function [ ct ] = compute_epssym( ct, param );
    % sets .epssym
    if( param.epssym >= 1 );
        switch ct.algorithm(1);
            case {'a','l','r','p'}; ct.epssym = 10^param.epssym * 5e-9; 
            case {'k','K'};         ct.epssym = 10^param.epssym * 5e-6; 
            case {'c'};             ct.epssym = 10^param.epssym * 1e-4;
            otherwise; fatal_error_; end;
    else;
        ct.epssym = param.epssym; end; 
end

function [ ct ] = compute_rho_ordering( M, ct, param );
    % Sets ct.rho_ordering
    %        .toa
    ct.rho_ordering = nan( size(ct.ordering) );
    ct.toa_ordering = nan( size(ct.ordering) );
    for i = 1:numel( ct.ordering );
        if( isempty(ct.ordering{i}) );
            continue; end;
        Pi = buildproduct_fast( M, ct.ordering{i} );
        ct.toa_ordering(i) = buildsum_fast( param.toa, ct.ordering{i} );
        ct.rho_ordering(i) = rhot( Pi ).^(1./ct.toa_ordering(i)); end
end

function [ ct ] = compute_oclass( ct );
    % sets .oclass
    ct.oclass = cell( size(ct.ordering) );
    for i = 1:numel( ct.smpflag );
        ct.oclass{i} = unique( ct.ordering{i}.', 'rows' ).'; end;
    
    % if( ~all( cellfun( @(x) all( isfinite(x(:)) ), ct.v0s) ) );  % I do not know why this code is here
    %     ct.v0s = cell( 1, numel(ct.smpflag) );
    % else;
    %     ct.v0s = double( ct.v0s ); end;
end

function [ ct ] = make_output( M, ct, param );
% sets  .M_sym
%       .Mt_sym
%       .num_matrix
%       .lambda
%       .lambda_sym
%       .comp
%       .v0idx  % should be renamed to comp_ordering
%       .num_component
%       .graph


    
    % .num_matrix
    ct.num_matrix = numel( ct.Mt );
    
    % .v0idx
    ct.v0idx = zeros( size(ct.ordering) );  % to which component the vector belongs
    if( ipa_isgraph(param.graph) );
        ct.comp = unique( param.graph{2}(2,:) );
        for i = 1:numel( ct.ordering );
            if( isempty(ct.ordering{i}) );
                ct.v0idx(i) = 1;
            else;
                ct.v0idx(i) = param.graph{2}(2, ct.ordering{i}(end)); end; end; end;
    
    % .graph
    ct.graph = param.graph;
    
    % .num_component
    if( ~ipa_isgraph(param.graph) );
        ct.num_component = 1;
    else;
        ct.num_component = numel( unique( param.graph{2} ) ); end;
    
    % .___sym    
    if( issym(M{1}) || param.epssym >= 0 );
        % these fields are only set if symbolic computation is enabled
        ct.lambda_sym = ct.lambda;
        ct.lambda = real( double( ct.lambda ) );
        end;
end



function [ str ] = printout( ct, param, str );
    str = vprintf( 'Selected candidates: \n%r\n', ct.ordering(ct.smpflag == ipa_constant.candidate), 'str',str );
    if( any(ct.smpflag == ipa_constant.nearlycandidate) ); 
        str = vprintf( 'Selected nearlycandidates: \n%r\n', ct.ordering(ct.smpflag == ipa_constant.nearlycandidate), 'imp',[2 param.verbose], 'str',str ); end;
    
    str = vprintf( 'Number of vertices:', 'str',str );
    if( any(ct.smpflag == ipa_constant.candidate) );        str = vprintf( ' (c) %i',   nnz(ct.smpflag == ipa_constant.candidate),       'str',str ); end;
    if( any(ct.smpflag == ipa_constant.realvertex) );       str = vprintf( ' / (r) %i', nnz(ct.smpflag == ipa_constant.realvertex),      'str',str ); end;
    if( any(ct.smpflag == ipa_constant.nearlycandidate) );  str = vprintf( ' / (n) %i', nnz(ct.smpflag == ipa_constant.nearlycandidate), 'str',str ); end;
    if( any(ct.smpflag == ipa_constant.extravertex) );      str = vprintf( ' / (e) %i', nnz(ct.smpflag == ipa_constant.extravertex),     'str',str ); end;
    str = vprintf( ' (', 'str',str );
    if( any(ct.smpflag == ipa_constant.candidate) );        str = vprintf( ' candidates',         'str',str ); end;
    if( any(ct.smpflag == ipa_constant.realvertex) );       str = vprintf( ' / realed vertices',  'str',str ); end;
    if( any(ct.smpflag == ipa_constant.nearlycandidate) );  str = vprintf( ' / nearlycandidates', 'str',str ); end;
    if( any(ct.smpflag == ipa_constant.extravertex) );      str = vprintf( ' / extravertices',    'str',str ); end; 
    str = vprintf( ' )\n', 'str',str );
    
    if( param.verbose >= 2 );
        if( any(ct.smpflag == ipa_constant.candidate) );        str = vprintf( 'v0:  %r\n', ct.v0(ct.smpflag == ipa_constant.candidate),        'str',str ); end;
        if( any(ct.smpflag == ipa_constant.nearlycandidate) );  str = vprintf( 'v0s: %r\n', ct.v0(ct.smpflag == ipa_constant.nearlycandidate),  'str',str ); end;
        if( any(ct.smpflag == ipa_constant.realvertex) );       str = vprintf( 'rv0: %r\n', ct.v0(ct.smpflag == ipa_constant.realvertex),       'str',str ); end;
        if( any(ct.smpflag == ipa_constant.extravertex) );      str = vprintf( 'ev:  %r\n', ct.v0(ct.smpflag == ipa_constant.extravertex),      'str',str ); end;
        if( any(ct.smpflag == ipa_constant.candidate) );        str = vprintf( '\n', 'imp',[2 param.verbose], 'str',str ); end; end;
end    

%%%%%%%%%%%%%%%%%%

function [ param ] = initialize( M, param );
    % sanity checks
    if( nargin <= 1 );
        param = struct; end;
    assert( iscell(M), 'makecandidate:input', 'First argument must be a cell array of square matrices.' );

    % set param 
    param = setdefault( param, 'verbose', 1, 0 );
    verbose = param.verbose;
    param = setdefault( param, 'M_sym',                 M,          verbose );
    param = setdefault( param, 'admissiblealgorithm',   [],         verbose );
    param = setdefault( param, 'algorithm',             'a',        verbose );
    param = setdefault( param, 'autoextravertex',       1,          verbose );
    param = setdefault( param, 'complexeigenvector',    1,          verbose );
    param = setdefault( param, 'debug',                 {},         verbose );
    param = setdefault( param, 'delta',                 1,          verbose );
    param = setdefault( param, 'epsequal',              1,          verbose );
    param = setdefault( param, 'epssym',                1,          verbose );
    param = setdefault( param, 'extravertex',           [],         verbose );
    param = setdefault( param, 'graph',                 [],         verbose );
    param = setdefault( param, 'JSR',                   [0 inf],    verbose );
    param = setdefault( param, 'lambda',                [],         verbose );
    param = setdefault( param, 'maxnum_candidate',       100,        verbose, true );
    param = setdefault( param, 'minmax',                'max',      verbose );
    param = setdefault( param, 'multipleeigenvector',   1,          verbose );
    param = setdefault( param, 'norestart',             0,          verbose );
    param = setdefault( param, 'normalization',         1,          verbose );
    param = setdefault( param, 'ordering',              [],         verbose );
    param = setdefault( param, 'prebalancing',          [],         verbose );
    param = setdefault( param, 'predelta',              1,          verbose );
    param = setdefault( param, 'prematrixscaling',      [],         verbose );
    param = setdefault( param, 'smpflag',               [],         verbose );
    param = setdefault( param, 'v0',                    [],         verbose );
    param = setdefault( param, 'v0s',                   [],         verbose );
    param = setdefault( param, 'toa',                   [],         verbose );
    
    param                       = setdefault( param,                    'extrapath',     struct,    verbose );
    param.extrapath             = setdefault( param.extrapath,          'maxdepth',           0,    verbose );    
    
    param                       = setdefault( param,                    'findsmp',        struct,   verbose );
    param.findsmp               = setdefault( param.findsmp,            'bound',              [],   verbose );
    param.findsmp               = setdefault( param.findsmp,            'delta',              [],   verbose );
    param.findsmp               = setdefault( param.findsmp,            'maxsmpdepth',        50,   verbose );
    param.findsmp               = setdefault( param.findsmp,            'N',                  [],   verbose );
    param.findsmp               = setdefault( param.findsmp,            'nearlycanddelta', .9999,   verbose );
    param.findsmp               = setdefault( param.findsmp,            'sym',                 0,   verbose );
    param.findsmp               = setdefault( param.findsmp,            'verbose',   verbose - 1,   verbose );
    
    param                       = setdefault( param,                    'leadingeigenvector', struct,   verbose );
    param.leadingeigenvector    = setdefault( param.leadingeigenvector, 'sym',                    -1,   verbose );
    
    param                       = setdefault( param,                    'polytopenorm', struct,   verbose );
    param.polytopenorm          = setdefault( param.polytopenorm,       'auxiliary_data', [],     verbose );

    if( isequal(param.extravertex, 0 ) );
        param.autoextravertex = 0;
        param.extravertex = []; end;
   
    if( ~iscell(param.extravertex) ); 
        param.extravertex = num2cell( param.extravertex, 1 ); end;   
    
    % sanity checks and further initializations
    ev = param.extravertex;
    assert( all( cellfun(@iscolumn, ev) ) && ...
           (isempty( ev ) || isequal( numel(ev{1}), size(M{1}, 1)) ), 'ipa:extravertex', 'extravertices must be given as column vectors with number of rows equal to the dimension of the matrices.' );    
    if( ~iscell(param.ordering) && isvector(param.ordering) );
        param.ordering = {param.ordering}; end;
    
    if( isempty(param.ordering) );
        assert( isempty(param.v0s), 'makecandidate:arg', 'If `ordering` is not given, `v0s` must not be given too.' ); 
        if( isempty(param.v0) );
            % everything ok
        elseif( ~isempty(param.smpflag) && all(param.smpflag == ipa_constant.extravertex) );
            param.ordering = repcell( [], [1 numel(param.v0)] );
        else;
            assert( isempty(param.v0s), 'makecandidate:arg', 'If `ordering` is not given, `v0` must not be given too, or smpflag for all entries must equal 2.' ); end;
    else;
        val = cellfun( @(x) x(:), param.ordering, 'UniformOutput', false );
        val = vertcat(val{:});
        assert( isanyof(param.ordering, {{('none')'}}) || ...
                isempty(val) || ...
                max(val) <= numel(M) && min(val) >= 0, 'makecandidate:oo', 'Entries in oo are not in the range [0 numel(M)].' ); end;
        
    if( isempty(param.toa) );
        param.toa = ones( size(M) ); 
    elseif( isscalar(param.toa) );
        param.toa = repmat( param.toa, size(M) ); end;
        
    dim = size( M{1}, 1 );
    if( isempty(param.v0) );
        assert( isempty(param.v0s), 'makecandidate:arg', 'If v0 is not given, v0s must not be given too.' ); 
    elseif( isempty(param.v0s) );
        [param.v0s{1:numel(param.v0)}] = deal( zeros(dim, 1) ); end;
end

function [ p ] = setdefault( p, name, value, verbose, emptysetflag );
    if( ~isfield(p, name) );
        p.(name) = value; 
        if( verbose >= 1 );
            fprintf( 'Missing field set to default: %s.%s = \t%s\n', inputname(1), name, dispc(value) ); end; end;
    if( nargin >= 5 && emptysetflag );
        if( isempty(p.(name)) );
            p.(name) = value; end; end; 
end

function [ ct ] = remove_ct_entry( ct, ia );
    negative_ia = 1:numel( ct.smpflag );
    negative_ia(negative_ia == ia) = [];
    ct = keep_ct_entry( ct, negative_ia );
end

function [ ct ] = keep_ct_entry( ct, ia );
    
    fn = fieldnames( ct )';
    len = [];
    for n_ = fn; n = n_{1};
        switch n;
            case {'complexflag','d','prebalancing','ordering','smpflag','toa','v0','v0s','v0_sym','v0s_sym'};
                if( isempty(ct.(n)) );
                    continue; end;
                ct.(n) = ct.(n)(ia);
                len(end+1) = numel( ct.(n) );  %#ok<AGROW>
            case {'algorithm','lambda','polytopenorm'};
                % do nothing
            otherwise;
                fatal_error; end; end;
        
    assert( isempty(len) || all( len == len(1) ) );
end

function [ v ] = remove_small_complex_part( v, eps );
    [v, flag] = tocell( v );
    for i = 1:numel( v );
        err = norm( imag(v{i}), 1 );
        if( isAlways(err <= eps, 'Unknown','false') );
            v{i} = real( v{i} ); end; end;
    if( ~flag );
        v = v{1}; end;
end

function [ v ] = remove_small_negative_part( v, eps );
    [v, flag] = tocell( v );
    for i = 1:numel( v );
        idx_less0 = isAlways( v{i} < 0, 'Unknown','true' );
        err = norm( v{i}(idx_less0), 1 );
        if( isAlways(err <= eps, 'Unknown','false') );
            v{i}(idx_less0) = 0; end; end;
    if( ~flag );
        v = v{1}; end;
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
