function [ VV, boundary_idx, nrm_idx ] = boundary_vertices( varargin );
% [ VVo, boundary_idx ] = boundary_vertices( VVi, algorithm, [idxi] );
% (experimental) Returns the indices of the vertices which are ont the boundary of a polytope
%
% Input:
%   VVi     matrix of column vectors OR cell array of matrices of column vectors, the vertices which make up the polytope
%   idxi    same format of VVi of vectors of bools, if given then all vertices of VVi(.) for which idxi(.) is false are considered to be inside
%
% Output:
%   VVo     same format as input VV, only the vertices of VV which are on the boundary of the polytope defined by VV
%   bidx    vector of bools OR cell array of vector of bools, indices of the vertices of VVi which are on the boundary of the polytope
%
% Written by: tommsch, 2024-12-18

% Changelog:    2024-12-18, tommsch,    New experimental function boundary_vertices

    [opt.verbose, varargin] = parsem( {'verbose','v'}, varargin, 1, 'postprocess',@str2numt );
    VV = varargin{1};
    opt.spec = varargin{2};
    varargin(1:2) = [];

    idxi = [];
    if( numel(varargin) == 1 );
        idxi = varargin{1};
        idxi = tocell( idxi );
        varargin(1) = []; end;

    parsem( varargin, 'test' );
    

    [VV, opt.cellflag] = tocell( VV );
    
    
    boundary_idx = cell( size(VV) );
    nrm_idx = cell( size(VV) );
    for i = 1:numel( VV );
        boundary_idx{i} = true( 1, size( VV{i}, 2 ) );
        nrm_idx{i} = zeros( 1, size( VV{i}, 2 ) );
        if( ~isempty(idxi) ) ;
            assert( isequal( size(idxi{i}, 2), size( VV{i}, 2 ) ) ); end;
        for j = size( VV{i}, 2 ):-1:1;
            if( ~isempty(idxi) && ~idxi{i}(j) );
                nrm_idx{i}(j) = 0;
            else;
                idx_V = [1:j-1 j+1:size( VV{i}, 2 )];
                nrm_idx{i}(j) = polytopenorm( VV{i}(:,j), [VV{1:i-1}  VV{i}(:,idx_V) VV{i+1:end}], opt.spec, 'verbose',opt.verbose, 'output','ub', 'bound',[0 1] ); end;
            if( nrm_idx{i}(j) < 1 );
                boundary_idx{i}(j) = false;
                VV{i}(:,j) = []; end; end;
        if( opt.verbose >= 1 );
                fprintf( '\n' ); end; end;

    if( ~opt.cellflag );
        VV = VV{1}; end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
