function [ M_ret, B_ret, warningflag ] = invariantsubspace( varargin )
% [ M_ret, B_ret, warningflag ] = invariantsubspace( M, ['method'], [options] )
% Searches for invariant subspaces of matrices M.
%
% Input:
%   M           Cell array of matrices
%   'method'    A string of the following
%               'none'      Nothing happens,  M_ret={M}, B=eye(dim)
%               'auto'      (default): 'perm' for positive matrices, everything for the rest
%               'perm'      Tries to find a permutation such that M is in block-diagonal form, using permTriangult, originally from the JSR-toolbox by Jungers
%               'basis'     Tries to find a basis such that M is in block-diagonal form, using jointTriangult, originally from the JSR-toolbox by Jungers
%               'diff'      If M are transition matrices, tries to find subspaces of differences U, as described in <'Smoothness of anisotropic wavelets, frames and subdivision schemes', M. Charina and V.Yu. Protasov> 
%                           First tries numerically, then symbolically, then with vpa.
%               'all'       Tries all methods
%
% Options:
%   'verbose',val           Verbose level, Default=0
%   'sym',val               Determines how to compute
%                               -1,'auto': (default) same as input matrices
%                                0,'double': everything in double (same as old option 'double')
%                                1,'vpa': recompute basis in vpa after computation in double
%                                2,'sym': recompute basis in sym after computation in double
%                                3,'vpastrict': compute everything in vpa (same as old option 'vpa')
%                                4,'symstrict': compute everything in sym (same as old option 'sym')
%   'double'                Casts matrices to double before computing subspaces
%   'vpa                    Casts matrices to vpa before computing subspaces
%   'sym'                   Casts matrices to sym before computing subspaces
%   'epsilon',val           Epsilon used in algorithm 'basis'
%   'V0',val                Subspace used in algorithm 'basis'
%   'outputtype'            debug option: returns as M_ret the output type of the first matrix as a string. 
%                           The algorithm may try to compute the invariant subspaces or not.
%   'complex',val           (experimental), default = -1, Only applies to real input matrices
%                               -1: (default) If the output would be complex, then the output is realified
%                                0: If the output would be complex, then no invariant subspaces are returned
%                                1: If the output would be complex, the complex output is returned
%
% Output:
%   M_ret           cell array of the blocks in the block diagonal. 
%                   The output type is determined by 'sym'. 
%                       -1: Equal to input type
%                        0: double
%                        1-4: sym or vpa (currently we cannot do better)
%   B_ret           square matrix, change of basis such that B\M{i}*B has upper block triangular form
%   warningflag     logical, Is set when to result seems inaccuracte
%
% E.g.: [M, B] = invariantsubspace( {[1 1 1; 1 0 1; 0 1 0], [1 -1 0; 0 2 0; 1 1 2]}, 'verbose',2 )
%       invariantsubspace( {[1]} );  % yields {{[1]}}
%
% See also: jointTriangult, permTriangult, constructU, restrictmatrix, invblktri
%
% Written by: tommsch, 2018, and Jungers (JSR-toolbox)

%               tommsch,    2020-06-19, renamed option 'trans' to 'diff'. But old name still works
%               tommsch,    2020-09-25, Behaviour change for option sym, now has similar behaviour as findsmp
%                                       added return value warningflag
%               tommsch,    2021-04-29, added debug option 'outputtype'
%               tommsch,    2021-09-15, output type is at least input type or higher whenever B == eye
%               tommsch,    2022-02-02, Implemented recursive search for invariant subspaces
%               tommsch,    2022-05-26, More robust algorithm for option 'sym',3
%               tommsch,    2024-10-22, New method 'all', which tries all methods always
% Changelog:    

    [M{1}, opt] = parse_input( varargin{:} );
    M_orig = varargin{1};

    if( isempty(M{1}) );
        M_ret = {cell(1,0)};
        B_ret = [];
        warningflag = false;
        return; 

    elseif( size(M{1}{1}, 1 ) == 0 );
        M_ret = M;
        B_ret = [];
        warningflag = false;
        return; end;

    
    [M{1}, M_sym, opt] = invariantsubspace_preprocess( M{1}, opt );
    
    ct = 0;
    M{1} = M(1);
    dim = size( M{1}{1}{1}, 1 );
    B = {{eye( dim )}};
    while( true );
        ct = ct + 1;
        M{ct+1} = {};  %#ok<AGROW>
        B{ct+1} = {};  
        for i = 1:numel( M{ct} );
            [M_ct_i, B_ct_i] = invariantsubspace_worker( M{ct}{i}, opt );
            M{ct+1} = [M{ct+1}, M_ct_i(:)'];
            if( issym(B_ct_i) || issym(B{ct+1}) );
                B{ct+1} = [B{ct+1}, {B_ct_i}];    
            else;
                B{ct+1} = [B{ct+1}, B_ct_i]; end; end;
    %         if( ~iscell(B{ct+1}) );  % workaround for sym behaviour in Matlab
    %             B(ct+1) = {B(ct+1)}; end; end;
        if( numel(M{end}) == numel(M{end-1}) );
            M(end) = [];
            B(end) = [];
            break; end; end;
    BB = blkdiag( B{end}{:} );
    MM = M{end};
    
    [M_ret, B_ret, warningflag] = invariantsubspace_recompute( M{1}{1}, M_sym, MM, BB, opt );
    
    [M_ret, B_ret] = realify( M_ret, B_ret, M_orig, opt );
    
    vprintf( '\nSizes of matrices in invariant subspaces: %r\n', cellfun(@(x) size(x{1},2),M_ret), 'imp',[2 opt.verbose] );

    if( opt.outputtype );
        M_ret = class( M_ret{1}{1} ); end;
        
end

function [ M, opt ] = parse_input( varargin );
    [opt.V0, varargin]           = parsem( {'V0'}, varargin, [] );
    [opt.complex, varargin]      = parsem( {'complex'}, varargin, -1 );
    [opt.epsilon, varargin]      = parsem( {'epsilon','eps','e'}, varargin, 1e-12 );
    [opt.verbose, varargin]      = parsem( {'verbose','v'}, varargin, 1 );
    [opt.symflag, varargin]      = parsem( {'sym'}, varargin, -1 );
    [val, varargin]              = parsem( {'double','d'}, varargin );
    [opt.simplifytime, varargin] = parsem( {'simplify','simplifytime'}, varargin, 0 );
    [opt.outputtype, varargin]   = parsem( {'outputtype'}, varargin );
    if( val );
        opt.symflag = 0; end;
    [val, varargin]    = parsem( {'vpa'}, varargin );
    if( val );
        opt.symflag = 1; end;
    switch opt.symflag;
        case {-1,0,1,2,3,4};  % do nothing
        case {'auto'}; opt.symflag = -1;
        case {'double','doublestrict','strictdouble','doubledouble'}; opt.symflag = 0;
        case {'vpa','doublevpa'}; opt.symflag = 1;
        case {'sym','doublesym'}; opt.symflag = 2;
        case {'vpastrict','strictvpa','vpavpa'}; opt.symflag = 3;
        case {'strict','symstrict','strictsym','symsym'}; opt.symflag = 4;
        otherwise; error( 'ipa:invariantsubspace', 'Wrong input for ''sym''.' ); end;

    M = varargin{1};
    varargin(1) = [];
    if( ~iscell(M) );
        M = squeeze( num2cell(M,[1 2]) )'; end;
    opt.M_in = M;
    
    if( isempty(M) );
        return; end;
    if( isempty(opt.complex) );
        opt.complex = anym( isAlways( imag([opt.M_in{:}]) ~= 0 ) ); end;
    
    opt.dim = size( M{1}, 2 );
    opt.J = numel( M );
    if( isempty(varargin) ); 
        opt.method = 'auto'; 
    else; 
        opt.method = varargin{1}; 
        varargin(1) = []; end;

    parsem( varargin, 'test' );
end

function [ M, M_sym, opt ] = invariantsubspace_preprocess( M, opt );
    persistent syminstalled_flag

    if( ~isrow(M) );
        M = M(:).'; end;

    if( opt.symflag == -1 && isempty(syminstalled_flag) );
        try;
            err = lasterror();  %#ok<LERR>
            sym( 0 );
            syminstalled_flag = true;
        catch me;  %#ok<NASGU>
            lasterror( err );  %#ok<LERR>
            syminstalled_flag = false; end; end;

    M_sym = cell( size(M) );
    if( opt.symflag == -1 );        
        if( ~syminstalled_flag || isfloat(M{1}) );
            opt.symflag = 0;
        elseif( isvpa(M{1}) );
            opt.symflag = 3;
        elseif( issym(M{1}) );
            opt.symflag = 4; end; end;
    if( opt.symflag == 0 );
        for j = 1:opt.J;
            M{j} = double( M{j} ); end;
    elseif( opt.symflag == 1 || opt.symflag == 2 );
        for j = 1:opt.J;
            M_sym{j} = sym( M{j} );
            M{j} = double( M{j} ); end;

    elseif( opt.symflag == 3 );
        for j = 1:opt.J;
            M{j} = vpa( M{j} ); end;

    elseif( opt.symflag == 4 );
        for j = 1:opt.J;
            M{j} = sym( M{j} ); 
            expect( ~isvpa(M{j}), 'invariantsubspace:cast', 'Cannot convert matrix %i to sym.\n' ); end; end;

    nonneg = allm( isAlways( [M{:}]  >= 0, 'Unknown','false' ) );
    if( any( strcmp(opt.method, {'a','auto'}) ) );
        if( nonneg ); 
            vprintf( 'Since the matrices are nonnegative, we only search for triangulating permutations. \n', 'imp',[2 opt.verbose] );
            opt.method = 'perm';
        else;
            opt.method = 'all'; end; end;
end

function [ M_ret, B ] = invariantsubspace_worker( M, opt );
    opt.EYE = eye( opt.dim );
    vprintf( 'Search for common invariant subspaces: ', 'imp',[2 opt.verbose] )

    switch opt.method;
        case {'n','none'};
            B = cast( opt.EYE, 'like', M{1} );
            M_ret = {M};    

        case {'p','perm','permute','permutation'};
            [ ~, M_ret, B ] = searchperm( M, opt );

        case {'b','basis','base'};
            [ ~, M_ret, B ] = searchbasis( M, opt );

        case {'trans','transition','transitionmatrix','diff'};
            [ ~, M_ret, B ] = searchdiff( M, opt, 1 );  % flag says: symbolically

        case {'all'};
            [ triang, M_ret, B ] = searchperm( M, opt );
            if( ~triang ); 
                [ triang, M_ret, B ] = searchbasis( M, opt); end
            if( ~triang ); 
                s = warning( 'off', 'restrictmatrix:notinvariant' );
                cleanwarning = onCleanup( @() warning(s) );
                [ ~, M_ret, B ] = searchdiff( M, opt, 0 ); end;  % flag says: not symbolically

        otherwise;
            error( 'invariantsubspace:type', 'Wrong ''method'' type given.' ); end; 
end

function [ M_ret, B, warningflag ] = invariantsubspace_recompute( M, M_sym, M_ret, B, opt );
    M_test = cell( size(M) );
    warningflag = false;
    
     if( iseye(B) );
         nfo = matrixinfo( opt.M_in, 'vpa', 'sym' );
         if( any(nfo.sym(:)) || isequal(opt.symflag, 2) || isequal(opt.symflag, 4) );
             M_cast  = cellfun( @(x) sym(x), opt.M_in, 'UniformOutput',false );
         elseif( any(nfo.vpa(:)) || isequal(opt.symflag,1) || isequal(opt.symflag,3) );
             M_cast  = cellfun( @(x) vpa(x), opt.M_in, 'UniformOutput',false );
         else;
             M_cast = cellfun( @(x) double(x), opt.M_in, 'UniformOutput',false ); end;
         sze = zeros( 1, numel(M_ret) );
         for i = 1:numel( M_ret );
             sze(i) = size( M_ret{i}{1}, 1 );
             for j = 1:opt.J;
                 idx = (sum(sze(1:i-1))+1):sum(sze(1:i));
                 M_ret{i}{j} = M_cast{j}( idx, idx ); end; end;
         B = cast( eye(opt.dim), class(M_ret{1}{1}) );
         return; end;

        
        if( opt.symflag == 0 );
            B_test = B;
            M_test = M;
            epsilon = 1e-12;
        elseif( opt.symflag == 1 );
            for i = 1:numel( M );
                M_test{i} = vpa( M_sym{i} ); end;
            B_test = vpa( B );
            epsilon = vpa( 10^(sym(digits)-3) );
        elseif( opt.symflag == 2 );
            epsilon = sym(0);
            for i = 1:numel( M );
                M_test{i} = sym( M_sym{i} ); end;    
            if( ~all(matrixinfo(M_test,'symstrict')) );
                vprintf( 'Recomputation in sym not possible. Cast of input matrices to sym (not to vpa!) failed.\n', 'cpr','err', 'imp',[0 opt.verbose] );
                opt.symflag = 0; end; 
            B_test = B;
            idx = abs( B_test ) < 5e-12;
            B_test(idx) = 0;
            B_test = sym( B_test );
            if( ~all(matrixinfo( B_test, 'symstrict' )) );
                vprintf( 'Recomputation in sym not possible. Cast of basis to sym (not to vpa!) failed.\n', 'cpr','err', 'imp',[0 opt.verbose] );
                opt.symflag = 0; end; 
        elseif( opt.symflag == 3 );
            % check whether the basis is likely to be real
            if( ~opt.complex );
                residual = sum(abs(imag(B(:))));
                if( isAlways(residual < 1e-12) );
                    B = real(B); 
                elseif( isAlways(residual < 1e-9) )
                    clear M_ret
                    M_ret{1} = M;
                    B = cast( eye(opt.dim), class(M_ret{1}{1}) );
                    if( isAlways(residual < 1e-9) );
                        vprintf( 'Algorithm produced complex result for real input matrices.\n  These are most likely numerical errors.\n  The input matrices are returned unchanged.\n  You can try to use option <''sym'',4> or\n  accept the result and use option <''complex'',1>.\n', 'cpr','err', 'imp',[0 opt.verbose] ); end;
                    warningflag = false;
                    return;
                    end; end;
            
            for i = 1:numel( M );
                M_test{i} = sym( double(M{i}) ); end;
            B_test = double( B );
            idx = abs( B_test ) < 5e-12;
            B_test(idx) = 0;
            B_test = sym( B_test );
            epsilon = sym( 0 ); end;

        if( opt.symflag ~= 4  );
            sze = cellfun( @(x) size(x{1},1), M_ret );
            blk = arrayfun( @(x) ones(x), sze, 'UniformOutput',false );  % mask where the output matrices must be zero
            blk = blkdiag(blk{:}) | triu( ones(size(M{1},1)) );
            blk = ~blk;
            
            if( any(blk(:)) );
                sze = [cumsum([1 sze(1:end-1)]); cumsum( sze ) ];  % is needed later
                flag = true;
                M_ret_test = cell( size(M_ret) );
                for i = 1:numel( M_sym );
                    val2 = B_test\M_test{i}*B_test;
                    val = val2.*blk;
                    val = max( abs(val(:)) );
                    if( ~isAlways( val <= 1e-12, 'Unknown','false' ) );
                        warningflag = true;
                        if( opt.verbose  >= 0 );
                            warning( 'invariantsubspace:badscaled', 'Matrices are badly scaled. Results are very inaccurate.' ); end;
                        flag = false;
                        break; 
                    elseif( ~isAlways( val<=epsilon, 'Unknown','false' ) && opt.symflag ~= 0 );
                        flag = false;
                        break;  
                    elseif( opt.symflag == 0 );
                        flag = false;
                    else  % if( symflag ~= 3 );
                        for j = 1:size( sze, 2 );
                            M_ret_test{j}{i} = val2(sze(1,j):sze(2,j),sze(1,j):sze(2,j)); end; end; end;
            elseif( opt.symflag == 0 )
                flag = false;
            else;
                flag = true;
                M_ret_test = {nestedcellfun( @sym, M_test, 'UniformOutput',false )};
                B_test = nestedcellfun( @sym, B_test, 'UniformOutput',false ); end;
            
            if( flag );
                M_ret = M_ret_test;
                B = B_test; end;  end;
    %end;
end

function [ M_ret, B_ret ] = realify( M_ret, B_ret, M_orig, opt );
    if( ~isreal(B_ret) );
        switch opt.complex;
            case -1;
                if( opt.verbose >= 1 );
                    fprintf( 'Computed basis is complex. Output is realified. \n  (To change this behaviour set the option ''complex'')\n' ); end;
                B_ret = imag2real( B_ret );
                for i = 1:numel( M_ret );
                    for j = 1:numel( M_ret{i} );
                        M_ret{i}{j} = imag2real( M_ret{i}{j} ); end; end;
            case 0;
                M_ret = {M_orig};
                B_ret = eye( size(M_orig{1}) );
            case 1;
                % do nothing
            otherwise
                error( 'ttoolboxes:fatal', 'Programming error' ); end; end;
end

%%

function [ triang, M_ret, B ] = searchbasis( M, opt );
    vprintf( 'Search for basis: ', 'imp',[2 opt.verbose] )
    [~,M_ret,B] = jointTriangult( M, opt.V0, opt.epsilon, 'verbose',opt.verbose, 'simplify',opt.simplifytime );
    if( ~isequal(B,eye(opt.dim)) );
        vprintf( 'basis found. ', 'imp',[2 opt.verbose] )
        triang = true;
    else
        vprintf( 'no basis found. ', 'imp',[2 opt.verbose] )
        triang = false; end; 
end

function [ triang, M_ret, B ] = searchperm( M, opt );
    if( opt.verbose >= 2 );
        fprintf( 'Search for permutation: ' ); end;
    [triang, M_ret, B] = permTriangult( M );
    if( ~triang );
        if( opt.verbose >= 2 );
            fprintf('no permutation found. ' ); end;
        B = opt.EYE;
        M_ret = {M};
        triang = false;
    else;
        if( opt.verbose >= 2 );
            fprintf( 'permutation found. ' ); end;
        B  = opt.EYE(:,B);
        triang = true; end; 
    B = anycast( B, class(M{1}) );
end

function [ triang, M_ret, B ] = searchdiff( M, opt, symflag );
    try;
        vprintf( 'Search for difference subspace ', 'imp',[2 opt.verbose] );
        if( symflag ); 
            vprintf('(symbolically): ', 'imp',[2 opt.verbose] ); 
        else; 
            vprintf( '(numerically): ', 'imp',[2 opt.verbose] ); end;
        U = constructU( M, 1, 'verbose',opt.verbose-1 );
        if( matrixinfo(M,'real') );
            U = real( U ); end;
        [MU, ~,MR, NN, BASIS] = restrictmatrix( M, U, 'verbose',opt.verbose-2 );
        if( symflag );
            if( max(max([NN{:}])) > 10e-12 ); 
                U = constructU( M, 1, 'sym', 'verbose',opt.verbose-2 );
                [MU, ~, MR, NN, BASIS] = restrictmatrix( M, U, 'verbose',opt.verbose-2 ); end;
            if(max(max([NN{:}])) > 10e-12); 
                U = constructU( M, 1, 'vpa',200 );
                [MU, ~, MR, NN, BASIS] = restrictmatrix( M, U, 'verbose',opt.verbose-2 ); end; end;
        if( max(max([NN{:}]))<10e-12 );
            vprintf( 'difference subspace found. \n', 'imp',[2 opt.verbose] );
            M_ret{1} = MU; 
            M_ret{2} = MR; 
            B = BASIS;
            triang = true;
        else;
            vprintf( 'no difference subspace found. \n', 'imp',[2 opt.verbose] );
            B = opt.EYE;
            M_ret = {M};
            triang = false; end;
    catch;
        vprintf( 'Could not search for difference subspace. \n', 'imp',[2 opt.verbose] );
        B = opt.EYE;
        M_ret = {M};
        triang = false; end;
end
    
function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
