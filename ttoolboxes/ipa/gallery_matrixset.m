function [ M, G, prop ] = gallery_matrixset( varargin );
% [ M, G, prop ] = gallery_matrixset( [M], what, [options], [filters] )
% Returns sets of matrices, mostly used for ipa.
%
% Input:
%   [M]             a cell array of matrices. If given, the input matrix is only modified accordingly to the given options
%   what            string, specifies which matrix to generate (Can be given as name-value argument too)
%
% Filters:
%   Filters are list of properties the returned matrices shall satisfy.
%   They are mostly tested after a matrixset is generated.
%   Thus, the more filter are provide, the longer it will take until a set is generated.
%   'bool'          Returns matrices with values 0,1.
%   'int'           Returns integer matrices
%   'irreducible'   default = true, Returns only irreducible sets of matrices
%   'norm'          Returns normalized matrices s.t. all matrices have 2-norm equal to 1
%   'pos'           Returns positive matrices
%   'rho'           Returns normalized matrices s.t. all matrices have spectral radius equal to 1
%   'sparse',val    Returns matrices with sparsity val
%   'dim',val       Integer, to specify dimension by name-value pair
%   'N',val         Integer, to specify N by name-value pair
%  
% Options:
%   'graph',str     string, specifies which graph to generate
%   'what',str      string, specifies which matrix to generate (Can be given as positional argument too, see above)
%   'nocell'        returns only the first matrix. Undefined behaviour if N > 1.
%   'maxiter',val   integer, default=50, How many tries shall be made
%   'seed',val      Seed for random number generator. 
%                   If seed is set, the random number generator has the same state before and after that function 
%   'sym',val       Specifies whether symbolic matrices can also be returned
%                       val = -1;   double, vpa and sym-matrices
%                       val =  0    (default) only double matrices
%                       val =  1    double and vpa matrices
%                       val =  2    double, vpa and sym-matrices
%                       val =  3    vpa and sym-matrices
%                       val =  4    only sym-matrices
%   'verbose',val   Verbose level
%
% Output:    
%   M               Cell array of square matrices.
%   G               adjancy matrix of a directed graph, defines admissible matrix products
%   prop            struct, fields described properties of returned matrix
%                       .bool   matrix has entries 0/1
%
% Note:
%   - Most of the options preserve no special properties of the matrices. 
%     For example: The entries in the matrices in gallery_matrixset('rand_gauss',10,2,'pos') do not have a Gaussian distribution anymore in general.
%   - Use the function `randomize_matrixset` to apply random changes to the matrix set
%   
% See also: randomize_matrixset, gallery_matrix, gallery_graph, gallery_mp, gallery_signal
%
% E.g.:  gallery_matrixset( 'rand_gauss', 'dim',5, 'N',2, 'seed',100, 'rho' )
%
% Written by: tommsch, 2018
% For more information write to: <a href="tommsch@gmx.at">tommsch@gmx.at</a>

%               2020-07-08, tommsch,    Removed options 'orthog_x', 'binary2'
%                                       Removed matrix 'complex'
%               2022-02-21, tommsch,    Added matrices from Moeller Thesis, Bugfix, An example of GWZ05 was wrong
%                                       experimental: Made `what`, `dim`, `N` to optional arguments
%               2022-12-28, tommsch,    Added name-value pair options for `what`, `dim`, `N`, `k`
%               2023-03-22, tommsch,    Added output `G`
%               2023-06-02, tommsch,    Renamed function to gallery_matrixset
%                                       Added convenience wrapper gallery_matrix
%                                       Added Matrices: 'iota'/'consecutive', 'ones', 'constant', 'blktriangular', 'invariantsubspace', 'stochastic_pm', 'cycliccolumn'
%                                       Added postprocessing: 'fliplr', 'flipud', 'pinv', 'transpose', 'positive', 'nonnegative'
%                                       Added options: 'row', 'col'
%               2024-03-06, tommsch,    Behaviour change: Changed API enormously. This function may not work well for the moment and the next decade, but its API is much better.
%                                       Use `gallery_matrixset_old` for the old API.
%               2024-03-29, tommsch,    Added postprocessing: 'singular'
%               2024-07-22, tommsch,    Behaviour change: No postprocessings are done, unless explicitely demanded (via given options)
%               2024-07-23, tommsch,    'cauchy' and 'totallypositive' now use different algorithms to generate matrices
%               2024-07-23, tommsch,    Removed all postprocessings. Postprocessings are now done by the funciton `randomize_matrixset`
%                                       Removed 'pathcomplete'
%               2024-09-09, tommsch,    Behaviour change: Only generates square matrices, removed options 'row', 'col'
% Changelog:    2024-11-27, tommsch,    Added .random field to prop for some matrix sets

% XX
% sets which are not random (e.g. 'ones') should only be randomly selected if J == 1
%
% house, integerdata, somewhat-stochastic matrices, stochastic, matrices with entries in [-1 1]
% persymmetric, complex, imaginery, hurwitz, P-matrix, monotone-matrix (inverse-positive-matrices), M-matrix, H-matrix, companion-matrix, comparison-matrix, Wronskian
%
% General: Alternant, Anti-diagonal, Anti-Hermitian, Anti-symmetric, Arrowhead, Band, Bidiagonal, Bisymmetric, Block-diagonal, Block, Block tridiagonal, Cauchy, Centrosymmetric, Conference, Complex, Hadamard,
% Copositive, Diagonally dominant, Discrete Fourier Transform, Elementary, Equivalent, Frobenius, Generalized permutation, Hadamard, Hollow, Logical Matrix, unit Metzler, Nonnegative Pentadiagonal,
% Permutation, Persymmetric, Polynomial, Quaternionic, Signature, Skyline, Sylvester, Triangular, Vandermonde, Walsh, Z
%
% Products/Inverses: Companion, Convergent, Definite, Hurwitz, Positive-definite, Stieltjes
%
% With applications: Congruent, Idempotent or Projection, Involutory, Nilpotent,  Unimodular, Unipotent, Totally unimodular, Weighing
%
% Alterant, Adjugate, Alternating sign, Augmented Bézout, Carleman, Cartan, Cofactor, Commutation, Confusion, Coxeter, Distance, Duplication and elimination, Euclidean distance, Fundamental (linear differential equation),
% Generator, Gram, Jacobian, Moment, Payoff, Pick, Random, Rotation, Seifert, Shear, Similarity, Symplectic, Totally positive, Transformation
% 'altsign','circulant', 'defective', 'hankel', 'householder', 'involutory', 'laplacian', 'singular', 'toeplitz', 'tridiag',
%
% Statistics: Centering, Correlation, Covariance, Design, Doubly stochastic, Fisher information, Hat, Precision, Stochastic, Transition
%
% graph theory: Biadjacency, Degree, Edmonds, Incidence, Seidel adjacency, Tutte
%
%
% engineering: L, M, H, Z, P, Cabibbo–Kobayashi–Maskawa, Density, Fundamental (computer vision), Fuzzy associative, Gamma, Gell-Mann, Hamiltonian, Irregular, Overlap, S, State transition, Substitution, Z (chemistry)


%#ok<*ALIGN>
%#ok<*AGROW>,

    [ M, G, prop, filter, opt ] = parse_input( varargin{:} );
    test_what_for_randomize_matrixset_options( opt );
    if( isempty(M) && isempty(G) );
        while( true );
            opt.max_iter = opt.max_iter - 1;
            M = [];
            if( opt.max_iter >= 0 );  % maximum number of rounds to try
                [M, G, prop] = getmatrix( filter, opt );
            elseif( isempty(M) );
                if( opt.verbose >= 0 );
                    warning( 'gallery_matrixset:arg', 'No matrices returned. Possible reasons:\n  1) ''what'' is a wrong string,\n  2) case ''complex'' chosen,\n  3) some incompatible properties were requested,\n  4) There is a bug in the programm (very likely)\n' ); end;
                break; end;
            inv_check = invariant( M, G, filter, opt ); 
            if( inv_check );
                break; end; end;
            
        if( ~isempty(M) && isempty(opt.what) && opt.verbose >= 1 );
            fprintf( 'Choice: %s\n', prop.name ); end; end;

    if( ~isempty(M) && nargout >= 2 );
        prop = matrixinfo( M, prop );
    else;
        prop = struct; end;
    [M, G] = make_output( M, G, opt );
end

function [ M, G, prop, filter, opt ] = parse_input( varargin );
    [opt.graph, varargin]                   = parsem( 'graph', varargin, [] );
    [opt.max_iter, varargin]                = parsem( {'maxiter','max_iter','maxround','max_round','rounds','num_try','num_tries'}, varargin, 50 );
    [opt.nocell, varargin]                  = parsem( 'nocell', varargin, [] );
    [opt.seed, varargin]                    = parsem( {'rng','seed'}, varargin, [] );
    [opt.sym, varargin]                     = parsem( 'sym', varargin, 0 );    
    [opt.verbose, varargin]                 = parsem( {'verbose','v'}, varargin, 1, 'postprocess',@str2numt );
    [opt.what, varargin]                    = parsem( 'what', varargin, [] );

    [M, varargin]                           = parsem( {'M','matrix'}, varargin, [] );
    [G, varargin]                           = parsem( {'G','graph'}, varargin, [] );
    [prop, varargin]                        = parsem( {'prop'}, varargin, struct );

    [filter.filter, varargin]               = parsem( {'filter'}, varargin, {}, 'postprocess', @tocell );
    [filter.dim, varargin]                  = parsem( {'dim','dimension'}, varargin, [], 'postprocess',@str2numt );
    [filter.bool, varargin]                 = parsem( {'bool','boolean'}, varargin, 'postprocess',@zero_one_to_logical );
    [filter.different, varargin]            = parsem( {'different','set'}, varargin, 'postprocess',@zero_one_to_logical );
    [filter.finite, varargin]               = parsem( 'finite', varargin, 'postprocess',@zero_one_to_logical );
    
    [filter.int, varargin]                  = parsem( 'int', varargin, 'postprocess',@zero_one_to_logical );
    [invariantsubspace_flag, varargin]      = parsem( {'invariantsubspace','subspace'}, varargin );
    expect( ~invariantsubspace_flag, 'The option ''invariantsubspace'' is replaced by the option ''irreducible''.' );
    [filter.irreducible, varargin]          = parsem( {'irreducible','noinvariantsubspace' }, varargin, 'postprocess',@zero_one_to_logical );
    [filter.invertible, varargin]           = parsem( {'invertible'}, varargin, 'postprocess',@zero_one_to_logical );
    if( filter.invertible == 1 );
        filter.invertible = [0 1e8]; end;
    if( numel(filter.invertible) == 1 && filter.invertible );
        filter.invertible = [0 filter.invertible]; end;
    [filter.N, varargin]                    = parsem( {'J','N','nummatrix'}, varargin, [2 64], 'postprocess',@str2numt );
    [filter.neg, varargin]                  = parsem( {'neg'}, varargin, 'postprocess',@zero_one_to_logical );
    [filter.negate, varargin]               = parsem( {'negate'}, varargin, [] );
    [filter.nonnegative, varargin]          = parsem( {'nonneg','nonnegative'}, varargin, 'postprocess',@zero_one_to_logical );
    [filter.norm, varargin]                 = parsem( 'norm', varargin, [] );
    if( numel(filter.norm) == 1 );
        filter.norm = [0 filter.norm]; end;
    [filter.pos, varargin]                  = parsem( {'pos','positive'}, varargin, 'postprocess',@zero_one_to_logical );
    if( filter.pos );
        filter.nonnegative = true; end;
    [filter.poslead, varargin]              = parsem( {'poslead','posleadev'}, varargin, 'postprocess',@zero_one_to_logical );  % positive leading eigenvalue
    [filter.rat, varargin]                  = parsem( 'rat', varargin, 'postprocess',@zero_one_to_logical );
    [filter.real, varargin]                 = parsem( 'real', varargin, 'postprocess',@zero_one_to_logical );

    [M, varargin] =                             process_M( M, varargin{:} );
    [opt.what, opt.what_idx, varargin] =        process_what( opt.what, varargin{:} );
    [opt.cleanrng] =                            process_seed( opt.seed );
    opt.arg = varargin;
    
end

function [ x ] = zero_one_to_logical( x );
    if( isequal(x, 0) );
        x = false;
    elseif( isequal(x, 1) );  % accepts, true, 1, single(1), 
        x = true;
    else;
        error( 'gallery_matrixset:value', 'Bad value given for some option.' ); end;
end

function [ what, what_idx, varargin ] = process_what( what, varargin );
    if( ~isempty(what) );
        % do nothing
    elseif( numel(varargin) >= 1 );
        what = varargin{1}; 
        varargin(1) = []; end;  % old default value what = []
    what = lower( what );
    what(what == '_') = [];
    idx_colon = find( what == ':' );
    if( numel(idx_colon) >= 1 );
        what_idx = what(idx_colon+1:end);
        what = what(1:idx_colon-1);
        converted_what_idx = str2num( what_idx );  %#ok<ST2NM>
        if( ~isempty(converted_what_idx) );
            what_idx = converted_what_idx; end;
    else;
        what_idx = []; end;
end

function [ M, varargin ] = process_M( M, varargin );
    if( numel( varargin ) >= 1 && ...
        iscell( varargin{1} ) && ...
        all( cellfun( @issquare, varargin{1} ) ) && ...
        all( cellfun( @isnumeric, varargin{1} ) ) && ...
       ~any( diff( cellfun('prodofsize', varargin{1} ) ) ) && ...
       isempty(M) ...
      );
        M = varargin{1};
        varargin(1) = []; end;
end

function [ cleanrng ] = process_seed( seed );
    cleanrng = [];
    if( ~isempty(seed) );
        % initialize random number generator and store state
        cleanrng = onCleanup_lazy( @(x) rng(x), rng );
        if( isnumeric(seed) );
            rng( mod(seed, 2^30) ); 
            rand( 1000 );
        else
            rng( seed );  end; end;
end

function [ M, G ] = make_output( M, G, opt );
    % type changes
    switch( opt.sym );
        case -1;
            % do nothing
        case 0;
            for i = 1:numel( M );
                M{i} = double(M{i}); end;
        case 1;
            for i = 1:numel( M );
                if( issymstrict(M{i}) );
                    M{i} = vpa(M{i}); end; end;
        case 2;
            % do nothing
        case 3;
            for i = 1:numel( M );
                if( ~issym(M{i}) );
                    M{i} = vpa( M{i} ); end; end;
        case 4;
            for i = 1:numel( M );
                if( ~issym(M{i}) );
                    M{i} = sym( M{i} ); end; end;
            
        otherwise;
            fatal_error; end;
    
    if( opt.nocell )
        if( ~isempty(M) );
            M = M{1}; 
        else;
            M = []; end; end;
end

%% lists
%%%%%%%%%%%%%%%%%%%

function [ M, G, prop ] = getmatrix( filter, opt );

    randflag = isempty( opt.what ) * randi( [2 9] );
    G = [];
    M = {};
    prop = struct;
    %if( randflag == 0 && isempty(M) );
    %    [M, ~, prop] = getmatrix_builder( filter, opt ); end;    
    if( randflag == 0 && isempty(M) || randflag == 2 );
        [M, ~, prop] = getmatrix_basic( filter, opt ); end;
    if( randflag == 0 && isempty(M) || randflag == 3 );
        [M, ~, prop] = getmatrix_random( filter, opt ); end;
    if( randflag == 0 && isempty(M) || randflag == 4 );
        [M, ~, prop] = getmatrix_matlab( filter, opt ); end;
    if( randflag == 0 && isempty(M) || randflag == 5 );
        [M, ~, prop] = getmatrix_papers( filter, opt ); end;
    if( randflag == 0 && isempty(M) || randflag == 6 );
        [M, ~, prop] = getmatrix_bad( filter, opt ); end;
    if( randflag == 0 && isempty(M) || randflag == 7 );
        [M, ~, prop] = getmatrix_kone( filter, opt ); end;
    if( randflag == 0 && isempty(M) || randflag == 8 );
        [M, ~, prop] = getmatrix_properties( filter, opt ); end;
    if( randflag == 0 && isempty(M) || randflag == 9 );
        [M, ~, prop] = getmatrix_misc( filter, opt ); end;
    if( randflag == 0 && isempty(M) || randflag == 10 );
        [M, ~, prop] = getmatrix_singleton( filter, opt ); end;
    if( randflag == 0 && isempty(M) );
        [M, G, prop] = getmatrix_continuous( filter, opt ); end;    
    if( randflag == 0 && isempty(M) );
        [M, ~, prop] = getmatrix_signal( filter, opt ); end;
    if( isempty(G) );
        [~, G, ~]    = getmatrix_graph( M, filter, opt ); end;    
    
    
    if( isempty(M) && ~isempty(filter.dim) && ~anym(filter.dim) );
        M = repmat( {[]}, 1, N ); 
        return; end;
        
end

function [ M, G_, prop ] = getmatrix_builder( filter, opt );  %#ok<DEFNU>
    M = {};
    G_ = [];
    prop = struct;
    if( check(opt.what, {'builder'}) );
        opt.what = []; end;
    
    if( isempty(opt.what) ); 
        opt.what = randi( [1 6] ); end;
    
    what = opt.what;
    if( false );
    elseif( check(what, {1,'blkdiag','blockdiag','blockdiagonal', ...
                         2,'blktri','blktriangular','blocktriangular', ...
                         3,'invariantsubspace','invariant'}) );
        prop.name = 'blkdiag';
        d = condget( filter, 'dim' );
        blocks = randsum( d );
        for j = 1:N;
            Block = cell( 1, numel(blocks) );
            b = 1;
            while( b <= numel(Block) );
                err = lasterror();  %#ok<LERR>
                try;
                    Block{b} = gallery_matrix( 'dim',blocks(b), opt.arg{:} );
                    b = b + 1;
                catch me;  %#ok<NASGU>
                    lasterror( err );  %#ok<LERR>
                    end; end; % do nothing
                if( check(what, {1,'blkdiag','blockdiag','blockdiagonal'}) );
                    M{j} = blkdiag( Block{:} );
                else;
                    M{j} = blktri( Block{:}, 'fillr' ); end; end;
        if( check(what, {3,'invariantsubspace','invariant'}) );
            while( true );
                B = 2*(randi( [0 1], d ) - .5);
                if( rank(B ) == d );
                    break; end; end;
            for j = 1:N;
                M{j} = B\M{j}*B; end; end; end;
    prop = matrixinfo( M, prop );
end

function [ M, G_, prop ] = getmatrix_basic( filter, opt );
    G_ = [];
    if( check(opt.what, {'basic'}) );
        opt.what = []; end;
    if( isempty(opt.what) ); 
        opt.what = randi( [1 6] ); end;
    
    N = condget( filter, 'N' );
    
    what = opt.what;
    M = {};
    prop = struct;
    if( false );
    elseif( check(what, {1,'eye','identity'}) );
        d = condget( filter, 'dim' );
        prop.name = 'identity';
        for i = 1:N;
            M{i} = eye( d, d ); end;

    elseif( check(what, {2,'1','one','ones'}) && ...
            (N == 1 || ~isscalar(what)) ...
          );
        d = condget( filter, 'dim' );
        prop.name = 'ones';
        for i = 1:N;
            M{i} = ones( d, d ); end;
        
    elseif( check(what, {3,'constant'}) && ...
            (N == 1 || ~isscalar(what)) ...
          );
        prop.name = 'constant';
        if( numel(opt.arg) == 1 );
            range = opt.arg{1};
        else;
            range = max( 9, N ); end;
        if( isscalar(range) );
            range = [-abs(range) abs(range)]; end;
        for i = 1:N;
            c = randi( range );
            d = condget( filter, 'dim' );
            M{i} = c * ones( [d d] ); end;        

    elseif( check(what, {4,'0','zero','zeros'}) && ...
            (N == 1 || ~isscalar(what)) ...
          );
        prop.name = 'zeros';
        d = condget( filter, 'dim' );
        for i = 1:N;
            M{i} = zeros( d, d ); end;

    elseif( check(what, {5,'jordan','jord'}) );
        d = condget( filter, 'dim' );
        prop.name = 'jordan';
        for i = 1:N;
            M{i} = matrix_jordan( d, d ); end;

    elseif( check(what, {6,'consecutive','iota'}) );
        d = condget( filter, 'dim' );
        prop.name = 'iota';
        for i = 1:N;
            start = randi( [-d d] );
            M{i} = reshape( start+(1:(d*d)), [d d] ); end;
        prop.random = true( size(M) );

    elseif( check(what, {7,'wedge','consecutive2','iota2'}) );
        prop.name = 'wedge';
        d = condget( filter, 'dim' );
        X = zeros( d, d );
        for r = 1:d;
            for c = 1:d;
                if( numel(varargin) == 1 );
                    X(r,c) = opt.arg{1}(d,d);
                else;
                    X(r,c) = max( r, c ); end; end; end;
        for i = 1:N;
            M{i} = X; end;
    end
end

function [ M, G_, prop ] = getmatrix_random( filter, opt );
    % Random matrices, need two arguments 'rand_mmm',<dim>,<number of matrices>
    % XX must be refactored, so that I get rid of the hardcoded number in the cases
    if( check(opt.what, {'random'}) );
        opt.what = []; end;
    if( isempty(opt.what) ); 
        opt.what = randi( [1 28] ); end;
    N = condget( filter, 'N' );
    what = opt.what;
    M = {};
    G_ = [];
    prop = struct;
    if( false );
    elseif( check(what, {1,'rand_bool','bool','r_b','b'}) );  % random boolean matrix
        d = condget( filter, 'dim' );
        for i = 1:N;
            M{i} = randi( [0 1], [d d] ); end; 
        prop.name = 'bool';
        prop.bool = true( size(M) );
        prop.pos = true( size(M) );
        prop.random = true( size(M) );
        
    elseif( check(what, {2,'binary','bin','enum'}) );  % Binary matrices in sorted order
        if( isempty(opt.arg) );
            opt.arg = {[]}; end;
        M = matrixenum( condget(filter, 'dim', []), condget(filter, 'N', []), opt.arg{:}, false, 'v',opt.verbose-1 );
        prop.name = 'binary';
        prop.bool = true( size(M) );
        prop.pos = true( size(M) );

    elseif( check(what, {'complex','rand_complex','r_c','c'}) );  % complex eigenvector  % XX Currently we do not have an efficient method to generate these. Thus for random matrices we omit it.
        M = matrix_complex( condget(filter, 'dim'), condget(filter, 'N'), opt.verbose, opt.arg{:} );
        prop.name = 'complex';
        prop.complex = true( size(M) );
        prop.random = true( size(M) );

    elseif( check(what, {4,'equal_neg','rand_equal_neg','rand_neg','r_e_n','r_n','e_n','n'}) );  % random random matrix with equally distributed values in [-1 1] 
        d = condget( filter, 'dim' );
        for i = 1:N; 
            M{i} = 2*rand( d ) - .5; end; 
        prop.name = 'rand_equal_neg';
        prop.real = true( size(M) );
        prop.random = true( size(M) );

    elseif( check(what, {5,'gauss','gaussian','rand_gauss','randn','r_g','g'}) );  % random matrix with normally distributed values
        d = condget( filter, 'dim' );
        for i = 1:N; 
            M{i} = randn( d ); end;
        prop.name = 'gaussian';
        prop.real = true( size(M) );
        prop.random = true( size(M) );

    elseif( check(what, {5,'gaussint','gaussianinteger','rand_gauss_int','randin','randni'}) );  % random matrix with normally distributed values
        d = condget( filter, 'dim' );
        scale = 1/rand();
        for i = 1:N;
            M{i} = round( scale*randn( d ) ); end;
        prop.name = 'gaussian_integer';
        prop.int = true( size(M) );
        prop.real = true( size(M) );
        prop.random = true( size(M) );
        
    elseif( check(what, {5,'integer','randi','int','rand_int','r_int'}) );  % random integer matrix with uniform distributed values in some interval
        d = condget( filter, 'dim' );
        if( isempty(opt.arg) );
            k = [];
        else;
            k = opt.arg{1}; end;
        if( isempty(k) );
            value_range = randp( 1, 1, inf, 4 );
            prop.pos = true( size(M) );
        elseif( isequal(k, 0) );
            value_range = randp( 1, 1, inf, 4 );
            value_range = [-value_range value_range];
        elseif( isscalar(k) && k > 0 );
            value_range = [0 k];
            prop.pos = true( size(M) );
        elseif( isscalar(k) && k < 0 );
            value_range = [k -k];
        elseif( numel(k) == 2 );
            value_range = k;
        else;
            error( 'gallery:matrixset:option', 'Could not parse argument to ''randi''.' ); end;
        for i = 1:N;
            M{i} = randi( value_range, d, d ); end;        
        prop.name = 'integer';
        prop.int = true( size(M) );
        prop.real = true( size(M) );
        prop.random = true( size(M) );

    elseif( check(what, {6,'equal','rand_equal','rand','r_e','r','e'}) );  % random random matrix with equally distributed values in [0 1]          
        d = condget( filter, 'dim' );
        for i = 1:condget( filter, 'N' ); 
            M{i} = rand( d, d ); end;
        prop.name = 'equally_distributed';
        prop.real = true( size(M) );
        prop.pos = true( size(M) );
        prop.random = true( size(M) );

    elseif( check(what, {7,'house','rand_house'}) );
        M = matrix_house( filter, opt );
        prop.name = 'house';
        prop.real = true( size(M) );
        prop.symmetric = true( size(M) );
        prop.orthogonal = true( size(M) );
        prop.involutory = true( size(M) );
        prop.random = true( size(M) );

    elseif( check(what, {'sign'}) );
        error( 'gallery:matrixset:deprecated', '''sign'' is not a valid name anymore. Use ''sign0''/''ternary'' for [-1 0 1] matrices, or ''rademacher''/''pm1'' for a [-1 1] matrix.' );

    elseif( check(what, {9,'sign0','ternary','pm10','rand_pm10','r_pm10'}) );  % random matrix with values -1, 0 1.
        d = condget( filter, 'dim' );
        for i = 1:condget( filter, 'N' ); 
            M{i} = randi( 3, [d d] ) - 2; end;
        prop.name = 'sign';
        prop.ternary = true( size(M) );
        prop.random = true( size(M) );

    elseif( check(what, {10,'rademacher','pm1','rand_pm1','r_pm1'}) );  % random matrix with values -1, 1
        d = condget( filter, 'dim' );
        for i = 1:condget( filter, 'N' );
            M{i} = 2*(randi( [0 1], [d d] ) - 1/2); end;
        prop.name = 'pm1';
        prop.real = true( size(M) );
        prop.ternary = true( size(M) );
        prop.random = true( size(M) );

    elseif( check(what, {11,'c_stochastic','column_stochastic','stochastic_c','stochastic','rand_stochastic','r_stoch','stoch'}) );  % Stochastic matrices
        M = matrix_stochastic( condget(filter, 'dim'), condget(filter, 'N') ); 
        prop.name = 'stochastic';
        prop.cstochastic = true( size(M) );
        prop.real = true( size(M) );
        prop.positive = true( size(M) );
        prop.random = true( size(M) );

    elseif( check(what, {12,'stochastic_neg','rand_stochastic_neg','r_stoch_n','stoch_n'}) );  % Stochastic matrices
        M = matrix_stochastic_neg( condget(filter, 'dim'), condget(filter, 'N') );
        prop.name = 'stochastic_neg';
        prop.cstochastic = true( size(M) );
        prop.real = true( size(M) );
        prop.positive = true( size(M) );
        prop.random = true( size(M) );

    elseif( check(what, {13,'stochastic_double','double_stochastic','rand_double_stochastic','rand_stochastic_double','r_d_stoch','d_stoch','r_stoch_d','stoch_d'}) );  % Double stochastic matrices
        M = matrix_doublestochastic( condget(filter, 'dim'), condget(filter, 'N') ); 
        prop.name = 'stochastic_double';
        prop.cstochastic = true( size(M) );
        prop.rstochastic = true( size(M) );
        prop.real = true( size(M) );
        prop.positive = true( size(M) );
        prop.random = true( size(M) );

    elseif( check(what, {14,'stochastic_double_neg','rand_double_stochastic_neg','double_stochastic_neg','rand_stochastic_double_neg','r_d_stoch_n','d_stoch_n','r_stoch_d_n','stoch_d_n'}) );  % Double stochastic matrices
        M = matrix_doublestochastic_neg( condget(filter, 'dim'), condget(filter, 'N') );
        prop.name = 'stochastic_double_neg';
        prop.real = true( size(M) );
        prop.positive = true( size(M) );
        prop.random = true( size(M) );

    elseif( check(what, {15,'stochastic_pm','rand_stochasticpm','r_stochpm','stochpm'}) );  % Stochastic matrices with entries in [-1 1]
        M = matrix_stochastic_pm( filter, opt );
        prop.name = 'stochastic_pm';
        prop.real = true( size(M) );
        prop.random = true( size(M) );

    elseif( check(what, {16,'cauchy'}) );
        N = condget( filter, 'N' );
        if( ~isempty(opt.arg) );
            assert( isequal(N, 1 ) && ...
                    numel( opt.arg ) == 2 && ...
                    isvector( opt.arg{1} ) && isvector( opt.arg{2} ) && ...
                    numel( opt.arg{1} ) == numel( opt.arg{2} ), ...
                    'gallery_matrixset:cauchy', 'Matrix ''cauchy'' needs two vectors of the same length. Furthermore ''N'' must be 1.\n  Otherwise, do not give concrete values.' );
            d = condget( filter, 'dim', numel(opt.arg{1}) );
            assert( d == numel(opt.arg{1}), 'gallery_matrixset:cauchy', 'Lenght of given vector must coincide with dimension.' );
            vec_given = true;
        else;
            d = condget( filter, 'dim' );
            vec_given = false; end;

        distinct_flag = true;
        for n = 1:N;
            if( vec_given );
                v = opt.arg{1};
                w = opt.arg{2};
            else;
                v = randn( 1, d );
                w = randn( 1, d ); end;
            if( numel( unique([v(:);w(:)]) ) ~= numel( [v(:);w(:)] ) );
                distinct_flag = false; end;
            if( d == 1 );
                M{n} = 1/(v-w);
            else;
                M{n} = gallery( 'cauchy', v, -w ); end; end;
        prop.name = 'cauchy';
        if( distinct_flag );
            prop.cauchy = true( size(M) );
            prop.totally_positive = true( size(M) );
            prop.real = true( size(M) ); end;
        prop.random = true( size(M) );

    elseif( check(what, {17,'totally_positive','tp'}) );
        d = condget( filter, 'dim' );
        for n = 1:condget( filter, 'N' );
            v = cumsum( rand(1, d) );
            M{n} = gallery( 'cauchy', v ); end;
        prop.name = 'totally_positive';
        prop.cauchy = true( size(M) );
        prop.totally_positive = true( size(M) );
        prop.real = true( size(M) );
        prop.random = true( size(M) );

    elseif( check(what, {'tu','rand_tu','r_tu'}) );  % (experimental)
        M = matrix_TU( filter, opt );
        prop.name = 'tu';
        prop.transitionmatrix = true;
        prop.real = true( size(M) );
        prop.random = true( size(M) );

    elseif( check(what, {19,'tv0','rand_tv0','r_tv0'}) );  % (experimental) Transition matrices with mask length dim, dilation=N;           
        M = matrix_TV( filter, opt );
        prop.name = 'tv';
        prop.transitionmatrix = true;
        prop.real = true( size(M) );
        prop.random = true( size(M) );

    elseif( check(what, {18,'unitary','rand_unitary','r_u','u'}) );  % Unitary matrices 
        M = matrix_unitary( condget(filter, 'dim'), condget(filter, 'N') ); 
        prop.name = 'unitary';
        prop.orthogonal = true( size(M) );
        prop.orthonormal = true( size(M) );
        prop.unitary = true( size(M) );
        prop.real = true( size(M) );
        prop.random = true( size(M) );

    elseif( check(what, {20,'rhozero','nilpotent'}) );  % matrices with spectral radius equal zero
        M = matrix_zerospectralradius( condget( filter, 'dim' ), condget(filter, 'N'), opt.verbose );
        prop.name = 'nilpotent';
        prop.nilpotent = true( size(M) );
        prop.real = true( size(M) );
        prop.random = true( size(M) );
        
    elseif( check(what, {21,'hurwitz','hurwitzstable'}) );  % matrices whose real parts of eigenvalues is strictly negative
        M = matrix_hurwitz( condget(filter, 'dim'), condget(filter, 'N'), opt.verbose ); 
        prop.name = 'hurwitz';
        prop.neg_eigenvalues = true( size(M) );
        prop.real = true( size(M) );
        prop.random = true( size(M) );
        
    elseif( check(what, {22,'circulant','circul'}) );
        M = matrix_circulant( condget(filter, 'dim'), condget(filter, 'N'), opt.verbose );
        prop.name = 'circulant';
        prop.persymmetric = true( size(M) );
        prop.circulant = true( size(M) );
        prop.toeplitz = true( size(M) );
        prop.real = true( size(M) );
        prop.random = true( size(M) );
        
    elseif( check(what, {23,'toeplitz'}) );
        M = matrix_toeplitz( filter, opt );
        prop.name = 'toeplitz';
        prop.circulant = true( size(M) );
        prop.real = true( size(M) );
        prop.random = true( size(M) );
        
    elseif( check(what, {24,'hankel'}) );
        M = matrix_hankel( condget(filter, 'dim'), condget(filter, 'N'), opt.verbose );
        prop.name = 'hankel';
        prop.symmetric = true( size(M) );
        prop.real = true( size(M) );
        prop.random = true( size(M) );
        
    elseif( check(what, {25,'chebvand'}) );
        d = condget( filter, 'dim' );
        for i = 1:condget( filter, 'N' ); 
            x = randn( 1, d );
            M{i} = gallery( 'chebvand', x ); end;
        prop.name = 'chebvand';
        prop.real = true( size(M) );
        prop.random = true( size(M) );
        
    elseif( check(what, {26,'normal'}) );
        M = matrix_normal( condget(filter, 'dim'), condget(filter, 'N'), opt.verbose );
        prop.name = 'normal';
        prop.normal = true( size(M) );
        prop.random = true( size(M) );
        
    elseif( check(what, {27,'orthogonal','orth'}) );
        M = matrix_orthogonal( condget(filter, 'dim'), condget(filter, 'N'), opt.verbose );
        prop.name = 'orthogonal';
        prop.orthogonal = true( size(M) );
        prop.real = true( size(M) );
        prop.random = true( size(M) );
    
    elseif( check(what, {28,'orthonormal','unitary'}) );
        M = matrix_orthonormal( condget(filter, 'dim'), condget(filter, 'N'), opt.verbose );
        prop.name = 'orthonormal';
        prop.orthonormal = true( size(M) );
        prop.orthogonal = true( size(M) );
        prop.real = true( size(M) );
        prop.random = true( size(M) );
        
    elseif( check(what, {29,'cycols','cycol','cyclic_column'}) );    
        d = condget( filter, 'dim' );
        k = randi( 1 + d );
        for i = 1:condget( filter, 'N' );
            M{i} = gallery( 'cycol', d, k ); end;
        prop.name = 'cycol';
        prop.real = true( size(M) );
        prop.random = true( size(M) );
        
    end;
end

function [ M, G_, prop ] = getmatrix_matlab( filter, opt );
    % random matrices from Matlabs gallery
    % Random matrices, need two arguments 'rand_mmm',<dim>,<number of matrices>
    % XX must be refactored, so that I get rid of the hardcoded number in the cases
    if( check(opt.what, {'matlab'}) );
        opt.what = []; end;
    if( isempty(opt.what) ); 
        opt.what = randi( [1 25] ); end;
    what = opt.what;
    M = {};
    G_ = [];
    prop = struct;
    if( false );
    elseif( check(what, {2,'chow'}) );
        d = condget( filter, 'dim' );
        N = condget( filter, 'N' );
        if( d >= 2 );
            for n = 1:N;
                M{n} = gallery( 'chow', d, randn+1, randn ); end;
        else;
            for n = 1:N;
                M{n} = randx( d ); end; end;
        prop.name = 'chow';
        prop.singular = true( size(M) );
        prop.random = true( size(M) );

    elseif( check(what, {3,'compar'}) );
        prop.name = 'compar';
        d = condget( filter, 'dim' );
        for n = 1:condget( filter, 'N' );
            M{n} = gallery( 'compar', randx(d, d), randi(2)-1 ); end;
        prop.random = true( size(M) );

    elseif( check(what, {4,'dorr'}) );
        prop.name = 'dorr';
        d = condget( filter, 'dim' );
        for n = 1:condget( filter, 'N' );
            if( d <= 1 );
                M{n} = randn( d );
            else;
                M{n} = full( gallery( 'dorr', d, abs(randn)*.1 ) ); end; end;
        prop.tridiag = true( size(M) );
        prop.random = true( size(M) );

    elseif( check(what, {5,'fiedler'}) );
        prop.name = 'fiedler';
        d = condget( filter, 'dim' );
        for n = 1:condget( filter, 'N' );
            M{n} = gallery( 'fiedler', d ); end;

    elseif( check(what, {6,'forsythe'}) );
        prop.name = 'forsythe';
        d = condget( filter, 'dim' );
        if( d >= 2 );
            for n = 1:condget( filter, 'N' ); 
                M{n} = gallery( 'forsythe', d, abs(eps*randn), eps*randn ); end;
        else;
            for n = 1:condget( filter, 'N' );
                M{n} = randx( d ); end; end;
        prop.random = true( size(M) );

    elseif( check(what, {6,'gearmat','gear','gearmatrix'}) );
        prop.name = 'gearmat';
        d = condget( filter, 'dim' );
        N = condget( filter, 'N' );
        len =  mixvector( [-d:-1 1:d], 2, 0 );
        assert( N <= len, 'gallery_matrixset:opt', 'For matrices ''gearmat'' at most (dim-1)^2 matrices can be generated (i.e. ''N'' must be less-equal (dim-1)^2.' );
        idx = randperm( len, N );
        for n = 1:N;
            ij = mixvector( [-d:-1 1:d], 2, idx(n) );
            M{n} = gallery( 'gearmat', d, ij(1), ij(2) ); end;
        prop.random = true( size(M) );

    elseif( check(what, {7,'hanowa'}) );
        prop.name = 'hanowa';
        d = condget(filter, 'dim', 2*randi([2 10]));
        if( ~mod(d, 2) == 0 );
            if( isequal(what, 7) )
                % do nothing
            else;
                assert( mod(d, 2)==0, 'gallery_matrixset:hanowa', '''Hanowa'' only works for even dimensions.' );
                for n = 1:condget( filter, 'N' ); 
                    M{n} = gallery( 'hanowa', d, randn ); end; end; end;
        prop.random = true( size(M) );

    elseif( check(what, {8,'invhess'}) );
        prop.name = 'invhess';
        d = condget( filter, 'dim' );
        if( d >= 2 );
            for n = 1:condget( filter, 'N' ); 
                M{n} = gallery( 'invhess', randx(1, d) ); end;
        else;
            for n = 1:condget( filter, 'N' );
                M{n} = randx( d ); end; end;
        prop.random = true( size(M) );

    elseif( check(what, {9,'jordblock'}) );
        prop.name = 'jordblock';
        d = condget( filter, 'dim' );
        for n = 1:condget( filter, 'N' ); 
            M{n} = gallery( 'jordbloc', d, randx ); end;
        prop.random = true( size(M) );

    elseif( check(what, {10,'kms'}) );
        prop.name = 'kms';
        d = condget( filter, 'dim' );
        prop.posdef = false( size(M) );
        for n = 1:condget( filter, 'N' ); 
            r = randn;
            M{n} = gallery( 'kms', d, randn );
            prop.posdef(n) = -1 < r && r < 1;
        end;
        prop.toeplitz = true( size(M) );
        prop.inv_tridiag = true( size(M) );
        prop.random = true( size(M) );

    elseif( check(what, {11,'krylov'}) );
        prop.name = 'krylov';
        d = condget( filter, 'dim' );
        for n = 1:condget( filter, 'N' ); 
            M{n} = gallery( 'krylov', d ); end;
        prop.random = true( size(M) );

    elseif( check(what, {12,'leslie'}) );
        prop.name = 'leslie';
        d = condget( filter, 'dim' );
        pos = condget( filter, 'pos' );
        for n = 1:condget( filter, 'N' );
            if( pos );
                x = rand( 1, d );
                y = rand( 1, d - 1 );
            else;
                x = randn( 1, d );
                y = randn( 1, d - 1 ); end;
            if( d == 1 );
                M{n} = x;
            else;
                M{n} = gallery( 'leslie', x, y ); end; end;
        prop.random = true( size(M) );

    elseif( check(what, {13,'moler'}) );
        prop.name = 'moler';
        d = condget( filter, 'dim' );
        for n = 1:condget( filter, 'N' );
            M{n} = gallery( 'moler', d, randx ); end;
        prop.symmetric = true( size(M) );
        prop.posdef = true( size(M) );
        prop.random = true( size(M) );

    elseif( check(what, {14,'pei'}) );
        prop.name = 'pei';
        d = condget( filter, 'dim' );
        for n = 1:condget( filter, 'N' ); 
            M{n} = gallery( 'pei', d, randx ); end;
        prop.random = true( size(M) );

    elseif( check(what, {15,'prolate'}) );
        prop.name = 'prolate';
        d = condget( filter, 'dim' );
        N = condget( filter, 'N' );
        prop.posdef = false( 1, N );
        for n = 1:N; 
            al = randn;
            prop.posdef(n) = 0 < al && al < .5;
            M{n} = gallery( 'prolate', d, randn ); end;   
        prop.toeplitz = true( size(M) );
        prop.symmetric = true( size(M) );
        prop.random = true( size(M) );

    elseif( check(what, {16,'rand_colu','colu'}) );  % random n-by-n matrix with columns of unit 2-norm, with random singular values whose squares are from a uniform distribution, contains no zeros   
        prop.name = 'rand_colu';
        d = condget( filter, 'dim' );
        s = rand( 1, d );
        s = sqrt(d) * s/norm( s );
        k = randi( [0 1] );
        if( d >= 2 );
            for n = 1:condget( filter, 'N' ); 
                M{n} = gallery( 'randcolu', s, d, k ); end;
        else;
            for n = 1:condget( filter, 'N' ); 
                M{n} = sign( randn( d, d ) ); end; end;
        prop.random = true( size(M) );

    elseif( check(what, {18,'rand_corr','corr'}) );  % random n-by-n correlation matrix with random eigenvalues from a uniform distribution, contains no zeros. Makes maximal trees for high dimensions   
        prop.name = 'rand_corr';
        d = condget( filter, 'dim' );
        s = rand( 1, d );
        s = d * s/norm( s, 1 );
        k = randi( [0 1] );
        for n = 1:condget( filter, 'N' ); 
            M{n} = gallery( 'randcorr', s, k ); end;
        prop.posdef = true( size(M) );
        prop.symmetric = true( size(M) );
        prop.random = true( size(M) );

    elseif( check(what, {20,'rand_hess','r_hess','hess'}) );   % random n-by-n real, orthogonal upper Hessenberg matrix.
        prop.name = 'rand_hess';
        d = condget(filter, 'dim');
        for n = 1:condget( filter, 'N' ); 
            M{n} = gallery( 'randhess', d ); end;
        prop.orthogonal = true( size(M) );
        prop.random = true( size(M) );

    elseif( check(what, {21,'rand_jorth'}) );
        d = condget(filter, 'dim');
        if( d >= 2 );
            n = ceil( d/2 );
            m = d - n;
            alpha = 1 + abs( randn );
            symm = randi(2) - 1;
            method  = randi(2) - 1;
            for j = 1:condget( filter, 'N' ); 
                M{j} = gallery( 'randjorth', n, m, alpha, symm, method ); end;
        else;
            M = repmat( {ones(condget(filter, 'dim'))}, 1, condget(filter , 'N') ); end;
        prop.name = 'rand_jorth';
        prop.hyperbolic = true( size(M) );
        prop.random = true( size(M) );

    elseif( check(what, {22,'rando'}) );
        prop.name = 'rando';
        d = condget( filter, 'dim' );
        k = randi( 3 );
        for n = 1:condget( filter, 'N' ); 
            M{n} = gallery( 'rando', d, k ); end;
        prop.random = true( size(M) );

    elseif( check(what, {23,'randsvd'}) );
        prop.name = 'randsvd';
        d = condget( filter, 'dim' );
        kappa = 1 + abs( randn );
        mode = randi(5);
        for n = 1:condget( filter, 'N' ); 
            M{n} = gallery( 'randsvd', d, kappa, mode, randi(d), randi(d) ); end;
        prop.random = true( size(M) );

    elseif( check(what, {24,'sampling'}) );  % random n-by-n real, orthogonal upper Hessenberg matrix.
        prop.name = 'sampling';
        d = condget( filter, 'dim' );
        N = condget( filter, 'N' );
        if( d >= 2 );
            for n = 1:N; 
                M{n} = gallery( 'sampling', randx(1, d) ); end;
        else;
            M = repmat( {ones(d)}, 1, N ); end;
        prop.random = true( size(M) );

    elseif( check(what, {25,'vander','vandermonde'}) );  % Vandermonde matrix
        prop.name = 'vandermonde';
        d = condget(filter, 'dim');
        vand_vec = randx( 1, d );
        for n = 1:condget( filter, 'N' ); 
            M{n} = vander( vand_vec ); end;
        prop.random = true( size(M) );

    %elseif( check(what, {26,'toeppd'};  % Documentation is wrong for this matrix
    %    m = ceil( round(randx) ) + 1;
    %    x = randx( 1, dim );
    %    for n = 1:condget( filter, 'N' );
    %       val{n} = gallery( 'toeppd', dim, m, x, randx(1,dim) ); end;

    end;
end

function [ M, G_, prop ] = getmatrix_papers( filter, opt );
    % Matrices from Examples/Papers
    randflag = isempty( opt.what ) * randi( [1 11] );
    M = {};
    G_ = [];
    prop = struct;
    idx = opt.what == '_';
    opt.what(idx) = [];
    
     if( randflag == 0 && isempty(M) || randflag == 1 );
        [M, ~, prop] = getmatrix_papers_moeller( filter, opt ); end;
     if( randflag == 0 && isempty(M) || randflag == 2 );
        [M, ~, prop] = getmatrix_papers_gwz05( filter, opt ); end;
     if( randflag == 0 && isempty(M) || randflag == 3 );
        [M, ~, prop] = getmatrix_papers_gp13(  filter, opt ); end;
     if( randflag == 0 && isempty(M) || randflag == 4 );
        [M, ~, prop] = getmatrix_papers_gj13(  filter, opt ); end;
     if( randflag == 0 && isempty(M) || randflag == 5 );
        [M, ~, prop] = getmatrix_papers_glp15(  filter, opt ); end;
     if( randflag == 0 && isempty(M) || randflag == 6 );
        [M, ~, prop] = getmatrix_papers_pj08(  filter, opt ); end;
     if( randflag == 0 && isempty(M) || randflag == 7 );
        [M, ~, prop] = getmatrix_papers_gz09(  filter, opt ); end;
     if( randflag == 0 && isempty(M) || randflag == 8 );
        [M, ~, prop] = getmatrix_papers_ap12( filter, opt ); end;    
     if( randflag == 0 && isempty(M) || randflag == 9 );
        [M, ~, prop] = getmatrix_papers_ajpr14( filter, opt ); end;
     if( randflag == 0 && isempty(M) || randflag == 10 );
        [M, ~, prop] = getmatrix_papers_mejstrik2020( filter, opt ); end;
     if( randflag == 0 && isempty(M) || randflag == 11 );
        [M, ~, prop] = getmatrix_papers_misc( filter, opt ); end;    
    
end

function [ M, G_, prop ] = getmatrix_papers_moeller( filter, opt );
    % Claudia Moeller, A New Strategy for Exact Determination of the Joint Spectral Radius
    if( check(opt.what, {'moeller'}) );
        opt.what = []; end;
    M = {};
    G_ = [];
    prop = struct;
    what = opt.what;
    if( isempty(what) );
        what = randi( 23 ); end;
    if( false );
    elseif( check(what, {'moeller','moeller15','moeller2015','moeller1', 'moeller71'}) )
        fprintf( 2, ['Implemented examples from ''moeller'' are: '...
                     '  moeller71, moeller71a, moeller714, moeller71f, moeller71g,\n'...
                     '  moeller72a, moeller72b,\n'...
                     '  moeller73a, moeller74a, moeller75a (dd6), moeller76a (dd8), moeller77a, moeller78a,\n'...
                     '  moeller79a0, moeller791, moeller79a2, moeller79a3, moeller79a4, moeller710a,\n'...
                     '  feta1, feta2, feta3\n'...
                     'A random set from moeller is returned.\n'...
                     ] );
        opt.what = [];
        [M, ~, prop] = getmatrix_papers_moeller( filter, opt );
        return;
    elseif( check(what, {1,'moeller711','moeller71a'}) );
        prop.name = 'moeller711';
        
        if( isempty(opt.arg) );
            vprintf( 'Give value of Epsilon as second argumnt. Use default 1/8 now.\n', 'imp',[1 opt.verbose] );
            eps = 1/8;
        else;
            eps = opt.arg{1};
            prop.random = true( 1, 2 ); end;
        A1 = [10/9 1/3;-1/3 0];
        A2 = [0 1/5*sqrt(1-eps);-1/5*sqrt(1-eps) 26/25-eps];
        M = {A1, A2};

    elseif( check(what, {2, 'moeller714','moeller71d'}) );  % GP2013
        prop.name = 'moeller714';
        D1 = [1 1;0 1];
        D2 = 9/10*[1 0;1 1];
        M = {D1, D2};

    elseif( check(what, {3, 'moeller71f'}) );
        % product bounded, reducible
        prop.name = 'moeller71f';
        F1 = [1/4 0;-1/2 1/2];
        F2 = [1 -1/4;0 1/2];
        M = {F1, F2};

    elseif( check(what, {4, 'moeller71g'}) );
        % unbounded, reducible
        prop.name = 'moeller71g';
        G1 = [1 1;0 1];
        G2 = [0 0;0 1/2];
        M = {G1, G2};

    elseif( check(what, {5, 'moeller72a', '3point'}) );
        % 3 point scheme, first difference scheme?
        prop.name = 'moeller72a';
        A1 = 1/32*[ 22 -6 0;-6 22 0;0 22 -6];
        A2 = 1/32*[-6 22 0;0 22 -6;0 -6 22];
        M = {A1, A2};

    elseif( check(what, {6, 'moeller72b','moeller72'}) );
        % 3 point scheme, second difference scheme?
        prop.name = 'moeller72b';
        B1 = 1/32*[56 0;-12 -12];
        B2 = 1/32*[-12 -12;0 56];
        M = {B1, B2};

    elseif( check(what, {7, 'moeller73a','moeller73'}) );
        % parametrized 4 point scheme
        prop.name = 'moeller73a';
        if( isempty(opt.arg) );
            if( opt.verbose >= 1 );
                fprintf( 'Give value of omega as second argumnt. Use default 1/16 now.\n' ); end;
            w = 1/16;
        else;
            w = opt.arg{1};
            prop.random = true( 1, 2 ); end;
        A1w = w*[4 4 0 0;-2 1/w-4 -2 0;0 4 4 0;0 -2 1/w-4 -2];
        A2w = w*[-2 1/w-4 -2 0;0 4 4 0;0 -2 1/w-4 -2;0 0 4 4];
        M = {A1w, A2w};

    elseif( check(what, {8, 'moeller74a','moeller74'}) );
        % parametrized family of dual 4-point schemes
        prop.name = 'moeller74a';
        if( isempty(opt.arg) );
            if( opt.verbose >= 1 );
                fprintf( 'Give value of omega as second argumnt. Use default 1/128 now.\n' ); end;
            w = 1/128;
        else;
            w = opt.arg{1};
            prop.random = true( 1, 2 ); end;
        A1w = w*[32 32 0 0;-20 1/w-24 -20 0;0 32 32 0;0 -20 1/w-24 -20];
        A2w = w*[-20 1/w-24 -20 0;0 32 32 0;0 -20 1/w-24 -20;0 0 32 32];
        M = {A1w, A2w};

    elseif( check(what, {9, 'moeller75a','moeller75','dd6'}) );
        % Dubuc-Deslaurier (DD) 6-point scheme proposed ([DD89])
        prop.name = 'moeller75a';
        A1 = 1/8*[-18 -19 0 0;3 38 3 0;0 -18 -18 0;0 3 38 3];
        A2 = 1/8*[3 38 3 0;0 -18 -18 0;0 3 38 3;0 0 -18 -18];
        M = {A1, A2};

    elseif( check(what, {10, 'moeller76a','moeller76','dd8'}) );
        % 8-point scheme proposed by Deslaurier and Dubuc ([DD89]) 
        prop.name = 'moeller76a';
        A1 = 1/64*[30 -14 -14 30 0 0 0 0;-5 -56 154 -56 -5 0 0 0;0 30 -14 -14 30 0 0 0;0 -5 -56 154 -56 -5 0 0;0 0 30 -14 -14 30 0 0;0 0 -5 -56 154 -56 -5 0;0 0 0 30 -14 -14 30 0;0 0 0 -5 -56 154 -56 -5];
        A2 = 1/64*[-5 -56 154 -56 -5 0 0 0;0 30 -14 -14 30 0 0 0;0 -5 -56 154 -56 -5 0 0;0 0 30 -14 -14 30 0 0;0 0 -5 -56 154 -56 -5 0;0 0 0 30 -14 -14 30 0;0 0 0 -5 -56 154 -56 -5;0 0 0 0 30 -14 -14 30];
        M = {A1, A2};

    elseif( check(what, {11, 'moeller77a','moeller77'}) );
        % C2 Ternary 4-point scheme [HIDS02] with parameter mu
        % mu is fixed to 1/11
        prop.name = 'moeller77a';
        A1 = 1/11*[5 -4 0;-4 5 0;0 9 0];
        A2 = 1/11*[-4 5 0;0 9 0;0 5 -4];
        A3 = 1/11*[0 9 0;0 5 -4;0 -4 5];
        M = {A1, A2, A3};

    elseif( check(what, {12, 'moeller78a','moeller78'}) );
        % Quaternary 3-point scheme
        prop.name = 'moeller78a';
        A1 = 1/32*[23 -15 0;-15 23 0;0 8 0];
        A2 = 1/32*[-15 23 0;0 8 0;0 8 0];
        A3 = 1/32*[0 8 0;0 8 0;0 23 -15];
        A4 = 1/32*[0 8 0;0 23 -15;0 -15 23];
        M = {A1, A2, A3, A4};

    elseif( check(what, {13, 'moeller79a0','moeller790'}) );
        prop.name = 'moeller79a0';
        A1 = 1/2*[4 0;-1 -1];
        A2 = 1/2*[-1 -1;0 4];
        M = {A1, A2};

    elseif( check(what, {14,'moeller79a1','moeller791'}) );
        prop.name = 'moeller79a1';
        A1 = 1/16*[1 42 1 0;0 -14 -14 0;0 1 42 1;0 0 -14 -14];
        A2 = 1/16*[-14 -14 0 0;1 42 1 0;0 -14 -14 0;0 1 42 1];
        M = {A1, A2};

    elseif( check(what, {15, 'moeller79a2','moeller792'}) );
        prop.name = 'moeller79a2';
        A1 = 1/128*[24 448 24 0 0 0;-1 -183 -183 -1 0 0;0 24 448 24 0 0;0 -1 -183 -183 -1 0;0 0 24 448 24 0;0 0 -1 -183 -183 -1];
        A2 = 1/128*[-1 -183 -183 -1 0 0;0 24 448 24 0 0;0 -1 -183 -183 -1 0;0 0 24 448 24 0;0 0 -1 -183 -183 -1;0 0 0 24 448 24];
        M = {A1, A2};

    elseif( check(what, {16', 'moeller79a3','moeller793'}) );
        prop.name = 'moeller79a3';
        A1 = 1/1024*[-34 -2302 -2302   -34     0     0     0   0;
                       1   424  4846   424     1     0     0   0;
                       0   -34 -2302 -2302   -34     0     0   0;
                       0     1   424  4846   424     1     0   0;
                       0     0   -34 -2302 -2302   -34     0   0;
                       0     0     1   424  4846   424     1   0;
                       0     0     0   -34 -2302 -2302   -34   0;
                       0     0     0     1   424  4846   424   1;];
        A2 = 1/1024*[  1  424   4846   424     1     0     0   0;
                       0  -34  -2302 -2302   -34     0     0   0;
                       0    1    424  4846   424     1     0   0;
                       0    0    -34 -2302 -2302   -34     0   0;
                       0    0      1   424  4846   424     1   0;
                       0    0      0   -34 -2302 -2302   -34   0;
                       0    0      0     1   424  4846   424   1;
                       0    0      0     0   -34 -2302 -2302 -34;];
        M = {A1, A2};

    elseif( check(what, {17, 'moeller79a4','moeller794'}) );
        prop.name = 'moeller79a4';
        A1 = 1/8192*[44 6576  53064   6576     44      0      0      0    0  0;
                     -1 -765 -28290 -28290   -765     -1      0      0    0  0;
                      0   44   6576  53064   6576     44      0      0    0  0;
                      0   -1   -765 -28290 -28290   -765     -1      0    0  0;
                      0    0     44   6576  53064   6576     44      0    0  0;
                      0    0     -1   -765 -28290 -28290   -765     -1    0  0;
                      0    0      0     44   6576  53064   6576     44    0  0;
                      0    0      0     -1   -765 -28290 -28290   -765   -1  0;
                      0    0      0      0     44   6576  53064   6576   44  0;
                      0    0      0      0     -1   -765 -28290 -28290 -765 -1;];
        A2 = 1/8192*[-1 -765 -28290 -28290   -765     -1      0      0    0  0;
                      0   44   6576  53064   6576     44      0      0    0  0;
                      0   -1   -765 -28290 -28290   -765     -1      0    0  0;
                      0    0     44   6576  53064   6576     44      0    0  0;
                      0    0     -1   -765 -28290 -28290   -765     -1    0  0;
                      0    0      0     44   6576  53064   6576     44    0  0;
                      0    0      0     -1   -765 -28290 -28290   -765   -1  0;
                      0    0      0      0     44   6576  53064   6576   44  0;
                      0    0      0      0     -1   -765 -28290 -28290 -765 -1;
                      0    0      0      0      0     44   6576  53064 6576 44;];
        M = {A1, A2};

    elseif( check(what, {18, 'moeller710a','moeller710'}) );
        % parametrized 8-point scheme
        prop.name = 'moeller710a';
        if( isempty(opt.arg) );
            if( opt.verbose >= 1 );
                fprintf( 'Give value of omega as second argumnt. Use default 1/278 now.\n' ); end;
            w = 1/278;
        else;
            w = opt.arg{1};
            prop.random = true( 1, 2 ); end;
        A1 = [192*w 832*w-9/4 832*w-9/4 192*w 0 0 0 0;
              -32*w -512*w+3/8 -960*w+19/4 -512*w+3/8 -32*w 0 0 0;
              0 192*w 832*w-9/4 832*w-9/4 192*w 0 0 0;
              0 -32*w -512*w+3/8 -960*w+19/4 -512*w+3/8 -32*w 0 0;
              0 0 192*w 832*w-9/4 832*w-9/4 192*w 0 0;
              0 0 -32*w -512*w+3/8 -960*w+19/4 -512*w+3/8 -32*w 0;
              0 0 0 192*w 832*w-9/4 832*w-9/4 192*w 0;
              0 0 0 -32*w -512*w+3/8 -960*w+19/4 -512*w+3/8 -32*w;];
        A2 = [-32*w -512*w+3/8 -960*w+19/4 -512*w+3/8 -32*w 0 0 0;
              0 192*w 832*w-9/4 832*w-9/4 192*w 0 0 0;
              0 -32*w -512*w+3/8 -960*w+19/4 -512*w+3/8 -32*w 0 0;
              0 0 192*w 832*w-9/4 832*w-9/4 192*w 0 0;
              0 0 -32*w -512*w+3/8 -960*w+19/4 -512*w+3/8 -32*w 0;
              0 0 0 192*w 832*w-9/4 832*w-9/4 192*w 0;
              0 0 0 -32*w -512*w+3/8 -960*w+19/4 -512*w+3/8 -32*w;
              0 0 0 0 192*w 832*w-9/4 832*w-9/4 192*w;];
      M = {A1, A2};

    % Claudia Moeller, Feta algorithm source code
    elseif( check(what, {19,'feta1'}) );
        prop.name = 'feta1';
        A1 = [1 1;0 1];
        A2 = (9/10)*[1,0;1,1];
        M = {A1,A2};
        %val(:,:,1) = A1;
        %val(:,:,2) = A2;
        % smp = [1 2] ??

    elseif( check(what, {20,'feta2'}) );
        prop.name = 'feta2';
        s = (3-sqrt(5))/2;
        A1 = s*[2 1;1 1];
        A2 = s*[1 1;1 2];
        M = {A1, A2};
        % smp = {1,2} ??

    elseif( check(what, {21,'feta3'}) );  % 4point ??
        prop.name = 'feta3';
        w = 1/16-1/10;
        A1 = [4*w 4*w 0 0;-2*w 1-4*w -2*w 0;0 4*w 4*w 0;0 -2*w 1-4*w -2*w];
        A2 = A1(4:-1:1,4:-1:1);
        M = {A1, A2};
        % smp = {1,2} (when w<1/16), otherwise smp [1 2]          
            
    end;
end

function [ M, G_, prop ] = getmatrix_papers_gwz05( filter, opt );
    maybe_unused( filter );
    if( check(opt.what, {'gwz','gwz05','gwz2005'}) );
        opt.what = []; end;
    M = {};
    G_ = [];
    prop = struct;
    what = opt.what;
    if( isempty(what) );
        what = randi( 5 ); end;
    % Guglielmi, Wirth, Zennaro - 2005 - Complex polytope extremality results for families of matrices
    
    if( false );
    elseif( check(what, {0,'gwz2005','gwz05'}) );
        error( 'gallery_matrixset:input', ['Implemented examples from ''gwz05'' are: '...
                                  'gwz05ex61 (moeller712), gwz05ex61_legacy, gwz05ex62 (moeller713), gwz05ex64'] );

    elseif( check(what, {1,'gwz05_1','gwz05ex61','moeller712','moeller71b'}) );
        prop.name = 'gwz05ex61';
        M{1} = [cos(sym(1)) -sin(sym(1) ); sin(sym(1)) cos(sym(1))]; 
        M{2} = [1/sym(2) -1/1i;0 0];

    elseif( check(what, {2,'gwz05_2','gwz05ex61_legacy','gwz05ex61old'}) );  % there was a typo here. I keep it for backwards compatibility
        prop.name = 'gwz05ex61_legacy';
        M{1} = [cos(sym(1)) -sin(sym(1) ); sin(sym(1)) cos(sym(1))]; 
        M{2} = [1/sym(2) -1/2;0 0]; 

    elseif( check(what, {3,'gwz05_3','gwz05ex62','moeller713','moeller71c'}) );
        prop.name = 'gwz05ex62';
        M{1} = (3-sqrt(sym(5)))*[2 1;1 1]; 
        M{2} = (3-sqrt(sym(5)))*[1 1;1 2]; 

    elseif( check(what, {4,'gwz05_4','gwz05ex63'}) );
        prop.name = 'gwz05ex63';
        M{1} = [1 0 0;0 1/2 0;0 0 1/4]; 
        M{2} = [1/2 0 0;1/2 0 0;1/2 0 0]; 

    elseif( check(what, {5,'gwz05_5','gwz05ex64'}) );
        prop.name = 'gwz05ex64';
        M{1} = [1 1;0 0]; 
        M{2} = [0 0;1 1]; 
        M{3} = 1/2*[1 1;1 1]; 
        M{4} = 2/3*[1 0;-1 0];
    end;
end

function [ M, G_, prop ] = getmatrix_papers_gp13( filter, opt );
% Guglielmi, Protasov 2013
    maybe_unused( filter );
    if( check(opt.what, {'gp','gp13'}) );
        opt.what = []; end;
    M = {};
    G_ = [];
    prop = struct;
    what = opt.what;
    if( isempty(what) );
        what = randi( [1 6] ); end;
    if( false );
    elseif( check(what, {1,'gp13ex1','gp13e1','gp13_1'}) );
        prop.name = 'gp13ex1';
        M = {[1 1;0 1], 9/10*[1 0;1 1]};

    elseif( check(what, {2,'gp13ex2','gp13e2','gp13_2','gp13_p35'}) );
        prop.name = 'gp13ex2';
        prop.interesting_for = {'lsr'};
        M = {[7 0;2 3], [2 4;0 8]};  % example for lower-spectral-radius

    elseif( check(what, {3,'gp13ex81','gp13e81','gp13_81','gp13_p38'}) );
        % The asymptotics of the number of overlap-free words
        % example for jsr and lsr
        prop.name = 'gp13ex81';
        M{1} = [0 0 0 0 0 0 0 2 4 2 0 0 0 0 0 0 0 0 0 0;
                0 0 1 1 0 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 0 0 0 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0
                0 0 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 1 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 0 0 0 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 1 2 1 0 0 0 0 0 0 0 1 2 1
                0 0 1 1 0 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 1 1 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0
                1 2 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 1 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 1 0 0 0 0 0 0 1 0 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0];                
        M{2} = [0 0 0 0 0 0 0 1 2 1 0 0 0 0 0 0 0 1 2 1
                0 0 1 1 0 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 1 1 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0
                1 2 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 1 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 1 0 0 0 0 0 0 1 0 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 4 2
                0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 1 1 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 0
                0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 1 0 0 1 0 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0
                0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
                ];

    elseif( check(what, {4,'gp13ex82','gp13e82','gp13_82','gp13_p40','pascal_rhomhus','pascalrhombus'}) );
        % Density of ones in the Pascal rhombus
        prop.name = 'gp13ex82';
        M{1} = [0 1 0 0 0;1 0 2 0 0;0 0 0 0 0;0 1 0 0 1;0 0 0 2 1];
        M{2} = [1 0 2 0 0;0 0 0 2 1;1 1 0 0 0;0 0 0 0 0;0 1 0 0 0];

    elseif( check(what, {5,'gp13ex83','gp13e83','gp13_83','gp13_p43'}) );
        % Euler binary partition function for r = 7
        % example for jsr and lsr
        prop.name = 'gp13ex83';
        M{1} = [1 1 1 1 0 0;0 1 1 1 0 0;0 1 1 1 1 0;0 0 1 1 1 0;0 0 1 1 1 1;0 0 0 1 1 1];
        M{2} = [1 1 1 0 0 0;1 1 1 1 0 0;0 1 1 1 0 0;0 1 1 1 1 0;0 0 1 1 1 0;0 0 1 1 1 1];

    elseif( check(what, {6,'gp13ex84','gp13e84','gp13_84','gp13_p44'}) );
        % Euler ternary partition function for r = 14
        % example for jsr and lsr
        prop.name = 'gp13ex84';
        M{1} = [1 1 1 1 1 0 0;0 1 1 1 1 0 0;0 1 1 1 1 1 0;0 1 1 1 1 1 0;0 0 1 1 1 1 0;0 0 1 1 1 1 1;0 0 1 1 1 1 1];
        M{2} = [1 1 1 1 1 0 0;1 1 1 1 1 0 0;0 1 1 1 1 0 0;0 1 1 1 1 1 0;0 1 1 1 1 1 0;0 0 1 1 1 1 0;0 0 1 1 1 1 1];
        M{3} = [1 1 1 1 0 0 0;1 1 1 1 1 0 0;1 1 1 1 1 0 0;0 1 1 1 1 0 0;0 1 1 1 1 1 0;0 1 1 1 1 1 0;0 0 1 1 1 1 0];
    end
end

function [ M, G_, prop ] = getmatrix_papers_gj13( filter, opt );
% Protasov, Jungers - Is switching systems stability harder for continuous time systems?
    maybe_unused( filter );
    if( check(opt.what, {'gj13'}) );
        opt.what = []; 
        if( ~isempty(opt.arg) && iscell(opt.arg) && isscalar(opt.arg{1}) && isnumeric(opt.arg{1}) );
            opt.what = opt.arg{1};
            opt.arg = []; end; end;
    M = {};
    G_ = [];
    prop = struct;
    what = opt.what;
    if( isempty(what) );
        what = randi( [0 7] ); end;
    if( false );
    elseif( check(what, {0,'gj13_1'}) );
        if( ~isempty(opt.arg) );
            tau = opt.arg{1};
        else;
            tau = 0.028; end;
        A1 = [-0.1322  0.0349 -0.1182;
                 0.0953 -0.1397 -0.1719;
                 0.0787  0.0223 -0.3281];
        A2 = [ 0.0891  0.1397 -0.0916;
                 0.0338 -0.2269 -0.0707
                 0.7417  0.3028 -0.5121];
        M{1} = eye(3) + tau*A1;
        M{2} = eye(3) + tau*A2;
    end
end

function [ M, G_, prop ] = getmatrix_papers_glp15( filter, opt );
% Guglielmi, Laglia, Protasov 2015
    maybe_unused( filter );
    if( check(opt.what, {'glp','glp15'}) );
        opt.what = []; 
        if( ~isempty(opt.arg) && iscell(opt.arg) && isscalar(opt.arg{1}) && isnumeric(opt.arg{1}) );
            opt.what = opt.arg{1};
            opt.arg = []; end; end;
    M = {};
    G_ = [];
    prop = struct;
    what = opt.what;
    if( isempty(what) );
        what = randi( [0 7] ); end;
    if( false );
    elseif( check(what, {0,'glp15_0','glp15_27'}) );
        prop.name = 'glp15_0';
        A1 = logm( [1 1;-1 1] );
        A2 = logm( [1 1;-1 0] );
        if( ~isempty(opt.arg) );
            beta = opt.arg{1};
        else;
            % 
            beta = 0.373463076;
            %gamma = 0.433445;
            end;
        M{1} = expm( A1 - beta*eye(2) );
        M{2} = expm( A2 - beta*eye(2) );

    elseif( check(what, {1,'glp15_1','glp15_47'}) );
        % interesting for lsr
        prop.name = 'glp15_1';
        A1 = logm( [7 0;2 3] );
        A2 = logm( [2 4;0 8] );
        if( ~isempty(opt.arg) );
            beta = opt.arg{1};
        else;
            beta = 0.8069807; end;
        M{1} = expm( A1 - beta*eye(2) );
        M{2} = expm( A2 - beta*eye(2) );

    elseif( check(what, {2,'glp15_2','glp15_612'}) );
        prop.name = 'glp15_2';
        M{1} = [-.9 -1 -1 -1 0;0  -.9 -1 -1  0;-1 -1 -.9 0 0; 0 0 -1 -1.9 -1;0  0 -1  0 -1.9];
        M{2} = [-.9 -1  0  0 0;0 -1.9 -1 -1 -1; 0  0 -.9 0 0;-1 0  0 -1.9  0;0 -1  0 -1  -.9];

    elseif( check(what, {3,'glp15_3','glp15_613'}) );
        prop.name = 'glp15_3';
        M{1} = [-2  0  0;10  -2 0;0  0 -11];
        M{2} = [-11 0 10; 0 -11 0;0 10  -2];

    elseif( check(what, {4,'glp15_4','glp15_614'}) );
        prop.name = 'glp15_4';
        M{1} = [-1   1/10 1/10;1/10 -1   1/10; 1/6  1/6  -1/3];
        M{2} = [-1/2 1/10 9/8;1/6   -1/3 7/8;  1/10 1/10 -1];

    elseif( check(what, {5,'glp15_5','glp15_617'}) );
        prop.name = 'glp15_5';
        M{1} =[
        -15  1   1  0  3   2   0   0
          2 -9   3  2  3   1   2   1
          1  3 -13  2  1   1   0   3
          2  0   1 -7  1   0   0   1
          1  0   1  1 -8   0   1   0
          1  3   1  2  3 -11   2   2
          1  3   1  3  1   1 -10   1
          2  1   3  2  3   2   3 -11];
        M{2} = [
        -10   2   2   0  1  3   2  0
          0 -16   2   1  2  3   1  2
          2   2 -14   3  1  2   3  1
          0   3   3 -13  3  2   0  0
          3   2   1   2 -9  0   1  3
          1   3   0   0  1 -7   0  0
          0   2   3   2  2  3 -17  2
          2   2   2   2  2  3  2 -17];

    elseif( check(what, {6,'glp15_6','glp15_6110'}) );
        prop.name = 'glp15_6';
        M{1} = [0 1 0 0 1 0 1 1 0 1 1 1 0 0 1 0 0 1 1 1 0 1 1 1 0 0 -1 0 0 0 1 0 1 1 1 0 0 0 0 0 0 0 0 1 0 1 0 0 1 1 1 1 -1 0 1 0 1 0 0 0 1 0 0 1 0 0 0 1 0 0 1 1 0 1 1 1 0 1 0 1 0 0 0 1 0 0 0 1 0 0 1 1 0 0 1 0 1 1 0 0 1 0 0 0 0 1 0 0 1 0 1 0 0 1 1 0 0 0 0 1 1 0 1 0 1 1 1 1 0 1 1 1 0 0 0 0 0 1 0 1 0 0 0 1 1 0 1 1 1 1 1 0 0 1 0 1 1 1 1 1 0 1 1 1 1 1 0 1 0 0 0 1 0 0 0 0 0 0 0 1 0 0 -1 0 1 0 0 0 0 0 0 1 0 1 0 1 0 1 0 1 0 0 1 0 0 1 0 0 0 1 0 0 0 1 0 1 1 1 1 1 1 0 0 1 1 1 0 0 1 0 1 1 1 0 1 1 1 1 0 0 0 0 1 0 1 1 0 1 0 1 1 1 0 1 0 1 0 1 0 1 -1 1 0 0 1 0 1 0 0 0 0 0 0 1 1 0 1 0 0 1 0 1 0 1 0 1 0 1 0 1 0 0 0 0 1 1 0 0 0 1 1 0 1 0 0 0 0 1 1 0 1 1 1 1 1 0 0 1 0 1 1 0 1 0 0 1 1 1 1 1 0 0 1 0 1 0 0 0 1 1 0 1 1 0 1 1 1 0 0 0 1 1 1 1 0 0 0 0 0 0 0 1 1 1 -1 1 1 1 0 0 1 1 0 1 1 1 1 0 0 0 0 1 1 0 1 1 1 0 0 1 1 1 1 1 0 1 0 0 1 1 1 1 1 0 0 1 1 1 1 1 0 1 1 1 1 1 -1 0 0 1 1 1 1 1 0 1 0 1 1 1 0 1 0 1 0 0 1 0 0 1 0 0 0 1 1 0 0 1 1 0 1 1 1 0 0 0 0 0 1 1 0 0 0 0 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 1 1 1 0 0 0 0 0 1 1 1 1 1 1 1 1 0 0 0 0 1 1 0 0 1 1 0 1 1 1 1 1 0 1 1 0 1 0 0 0 1 0 1 0 1 1 1 1 1 0 0 1 0 1 1 1 0 1 1 1 1 0 0 0 1 1 1 0 1 1 0 1 1 0 1 1 0 0 1 0 0 1 0 0 0 0 1 1 1 1 1 1 0 1 0 0 0 1 1 1 0 0 0 1 1 0 0 1 1 0 0 1 -1 0 0 1 1 1 0 0 0 0 1 1 1 0 0 1 0 0 1 1 0 1 0 0 0 0 0];
        M{1} = reshape( M{1}, [25 25] );
        M{2} = [-1 0 0 0 1 1 0 1 1 0 1 1 0 1 0 0 1 1 1 0 0 1 0 0 0 1 0 1 1 0 0 1 1 1 0 1 1 0 1 0 0 1 1 1 1 1 0 1 1 1 1 1 0 1 0 0 1 1 1 1 0 0 0 0 0 1 0 0 1 1 0 0 1 1 1 1 1 1 0 1 0 1 1 1 0 1 0 0 0 1 1 0 1 0 0 1 1 1 1 1 0 1 0 1 0 1 0 1 1 1 1 1 1 0 1 0 0 0 0 0 1 1 1 0 0 1 1 1 1 1  -1 1 0 0 0 1 0 1 1 1 0 0 0 1 0 0 1 1 0 1 0 0 0 1 0 1  -1 0 0 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 1 1 1 1 0  -1 0 1 0 1 0 1 0 1 1 1 1 0 1 1 1 0 0 0 0 0 0 1 0 1 1 0 1 1 1 0 1 1 0 0 0 0 1 1 1 1 1 0 1 0 0 0 1 1 0 0 1 0 0 0 0 0 0 1 0 0 1 1 0 1 0 1 1 1 1 1 0 1 1 1 0 0 0  -1 1 1 1 1 1 0 1 1 0 0 0 1 0 1 0 1 1 1 0 0 0 1 1 1 0  -1 0 0 1 1 1 0 1 0 0 1 0 0 0 0 0 1 1 0 1 0 0 1 1 0 0 1 1 1 0 1 0 0 0 1 0 1 0 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 1 1 1 0 0 0 1 0 0 0 0 0 0 1 0 0 0 0 1 0 0 1 1 1 0 0 1 1 0 1 0 1 1 1 1 0 1 1 1 0 0 0 1 0 1 0 1 1 1 1 0 1 0 1 1 0 0 0 0 1 1 1 0 1 0 0 1 0 0 0 1 0 1 0 0 1 1 0 1 0 1 0 0 0 0 0 0 0 1 0 0 1 1 1 1 1 1 1 1 1 0 1 0  -1 1 0 1 1 1 0 0 0 1 0 0 1 1 1 0 0 0 0 0 0 1 0 0 1 1 0 1 1 0 1 0 0 0 0 1 0 0 1 0 1 0 1 0 1 1 1 1 0 1 1 0 0 0 0 0 1 1 0 0 1 0 0 1 0 0 0 1 1 0 1 0 1 1 1 1 1 1 0 1 1 0 0 0 1 1 1 1 1 1 1 1 1 0 0 0 1 1 1 0 1 0 1 0 1 1 0 1 0 0 0 0 0 0 0 0 0 1 1 1 0 1 0 1 0 0 1 0 1 0  -1 1 1 1 1 0 1 0 1 0 1 1 0 1 1 0 1 1 0 0 1 0 1 1 1 1 0 1 0 0 1 1 0 0 1 0 0 0 1 0 1 1 1 1 1 1 1 0 1 0 0 0 1];
        M{2} = reshape( M{2}, [25 25] );

    elseif( check(what, {7,'glp15_7','glp15_6113'}) );
        prop.name = 'glp15_7';
        M{1} = [-5 -4;-1 -2];
        M{2} = [-2 -4;20 -2];

    end
end
function [ M, G_, prop ] = getmatrix_papers_pj08( filter, opt );
% Parillo, Jadbabaie, 2008
maybe_unused( filter );
    if( check(opt.what, {'pj','pj08','pj2008'}) );
        opt.what = []; end;
    M = {};
    G_ = [];
    prop = struct;
    what = opt.what;
    if( isempty(what) );
        what = randi( 1 ); end;
    if( false );
    elseif( check(what, {1,'pj08ex54','pj08e54'}) );
        prop.name = 'pj08ex54';
        M{1} = [0 1 7 4;1 6 -2 -3;-1 -1 -2 -6;3 0 9 1];
        M{2} = [-3 3 0 -2;-2 1 4 9;4 -3 1 1;1 -5 -1 -2];
        M{3} = [1 4 5 10;0 5 1 -4;0 -1 4 6;-1 5 0 1];
    end
end

function [ M, G_, prop ] = getmatrix_papers_gz09( filter, opt );  %#ok<INUSD>
    if( check(opt.what, {'gz','gz09','gz2009'}) );
        opt.what = []; end;
    M = {};
    G_ = [];
    prop = struct;
    what = opt.what;
    if( isempty(what) );
        what = randi( 3 ); end;
    % Guglielmi, Zennaro - Finding Extremal Complex Polytope Norms for Families of Real Matrices 
    if( false );
    elseif( check(what, {1,'gz09ex31'}) );
        prop.name = 'gz09ex31';
        if( isempty(opt.arg) );
            al = rand;
        else;
            al = opt.arg{1}; end;
        M{1} = [cos(sym(1)) -sin(sym(1) ); sin(sym(1)) cos(sym(1))]; 
        M{2} = al/sqrt(sym(2))*[1 1;0 0]; 
        prop.random = true( 1, 2 );

    elseif( check(what, {2,'gz09ex32'}) );
        prop.name = 'gz09ex32';
        M{1} = [-1/2 -sqrt(sym(3))/2; sqrt(sym(3))/2 -1/2];
        M{2} = sym([11/20 11/20; -11/20 -11/20]);

    elseif( check(what, {3,'gz09ex41'}) );
        prop.name = 'gz09ex41';
        M{1} = [-3 -2 1 2;-2 0 -2 1; 1 3 -1 -5;-3 -3 -2 -1]; 
        M{2} = [1 0 -3 -1;-4 -2 -1 -4; -1 0 -1 2;-1 -2 -1 2];             
    end;
end

function [ M, G_, prop ] = getmatrix_papers_ap12( filter, opt );
    maybe_unused( filter );
    if( check(opt.what, {'ap','ap12','ap2012'}) );
        opt.what = []; end;
    M = {};
    G_ = [];
    prop = struct;
    what = opt.what;
    if( isempty(what) );
        what = randi( 1 ); end;    
    if( false );
    elseif( check(what, {1,'ap12'}) );
        prop.name = 'ap12';
        M{1} = [0 1;0 0];
        M{2} = [0 0;1 0];
    end
end

function [ M, G_, prop ] = getmatrix_papers_ajpr14( filter, opt );
% Ahmadi, Jungers, Parrilo, Roozbehani - 2014 - Joint spectral radius and path-complete graph lyapunov functions
    maybe_unused( filter );
    if( check(opt.what, {'ajpr','ajpr14','ajpr2014'}) );
        opt.what = []; end;
    M = {};
    G_ = [];
    prop = struct;
    what = opt.what;
    if( isempty(what) );
        what = randi( [0 1] ); end;
    if( false );
    elseif( check(what, {1,'ajpr14_1','ajpr14_51'}) );
        prop.name = 'ajpr14_51';
        M{1} = [ 10 -6  -1;   8  1 -16; -8  0 17];
        M{2} = [ -5  9 -14;   1  5  10;  3  2 16];
        M{3} = [-14  1   0; -15 -8 -12; -1 -6  7];
        M{4} = [  1 -8  -2;   1 16   3; 16 11 14];

    elseif( check(what, {1,'ajpr14_2','ajpr14_52'}) );
        prop.name = 'ajpr14_52';
        M{1} = [1 0;1 0];
        M{2} = [0 1;0 -1];

    % elseif( check(what, {3,'ajpr14_4','ajpr14_53'}) );
    %     prop.name = 'ajpr14_53';
    %     M{1} = [0  -2  2 2  4;  0  0 -4 -1 -6;  2  6  0 -8  0; -2 -2 -3  1 -3;-1 -5 2 6 -4];
    %     M{2} = [-5 -2 -4 6 -1;  1  1  4  3 -5; -2  3 -2  8 -1;  0  8 -6  2  5;-1 -5 1 7 -4];
    %     M{3} = [3  -8 -3 2 -4; -2 -2 -9  4 -1; -4 -1  4 -3  0;  0  5  0 -3  5];

    elseif( check(what, {4,'ajpr14_4','ajpr14_54','ajpr14ex54'}) );
        prop.name = 'ajpr14_54';
        M{1} = [-1 -1;-4 0]; 
        M{2} = [3 3;-2 1];
    end
end

function [ M, G_, prop ] = getmatrix_papers_mejstrik2020( filter, opt );
maybe_unused( filter );    
    if( check(opt.what, {'mejstrik','tommsch','mejstrik20','mejstrik2020','m20','m2020'}) );
        opt.what = []; end;
    M = {};
    G_ = [];
    prop = struct;
    what = opt.what;
    if( isempty(what) );
        what = randi( 5 ); end;
     % Mejstrik -  2020 - Improved invariant polytope algorithm and applications
    if( false )
     elseif( check(what, {1,'mejstrik119long','mej2051b'}) );  % smp length of 119!. norms converge very wiggly to the spectral radius. Experimentally found numbers. 
        prop.name = 'mejstrik119long';
        M{1} = [ 0.163026496094203      -0.92406403398077952;    0.94910922519637453   0.75424733784141262]; 
        M{2} = [-0.95851077103541249    -0.65295899115856648;    0.6731845518678724   -0.58469670981727162]; 

     elseif( check(what, {2,'mejstrik119','mej2051','mej2051a'}) );  % smp length of 119!. norms converge very wiggly to the spectral radius. Rational approximations of the long numbers above.
        prop.name = 'mejstrik119';
        M{1} = [  15/92   -73/79;   56/59   89/118]; 
        M{2} = [-231/241 -143/219; 103/153 -38/65]; 

     elseif( check(what, {3,'mejstrikcn','mej52b'}) );  % smp length is equal to dim_2ndargument
        prop.name = 'mejstrikcn';
        switch numel(opt.arg);
            case 1; k = opt.arg{1};
            case 0; k = rand();
            otherwise; error( 'gallery_matrixset:mejstrikcn', 'Wrong options used.' ); end;
        M{1} = [1 1; 0 1]; 
        M{2} = [0 0; 1/k*exp(1+1/k) 0]; 
        prop.random = true( 1, 2 );

     elseif( check(what, {4,'mejstriklongsmp','mej2052','mej2052a'}) );  % smp length goes to infinity as dim_2ndargument goes to zero.
        prop.name = 'mejstriklongsmp';
        switch numel(opt.arg);
             case 1; k = opt.arg{1};
             case 0; k = rand();
             otherwise; error( 'gallery_matrixset:mejstrikcn', 'Wrong options used.' ); end;
        M{1} = [1 1; 0 1];
        M{2} = [0 0; k 0];
        prop.random = true( 1, 2 );

     elseif( check(what, {5,'mej20ex31','mej2031'}) );
        prop.name = 'mej20ex31';
        M{1} = [0 0;1 1];
        M{2} = [1 1;0 1];
    end;
end

function [ M, G_, prop ] = getmatrix_papers_misc( filter, opt );
    if( check(opt.what, {'papers'}) );
        opt.what = []; end;
    M = {};
    G_ = [];
    prop = struct;
    if( isempty(opt.what) );
        opt.what = randi( [1 15] ); end;
    what = opt.what;
     % Mejstrik -  2020 - Improved invariant polytope algorithm and applications
     if( false );
     elseif( check(what, {1,'cex','haremorrissidorovtheys', 'hmst'}) );  % jsr finiteness conjecture counterexample      
         try;
            err = lasterror();  %#ok<LERR>
            prop.name = 'haremorrissidorovtheys';
            M{1} = sym( [1 1; 0 1] );  
            M{2} = sym( '0.749326546330367557943961948091344672091327370236064317358024' ) * [1 0; 1 1]; 
         catch me;  %#ok<NASGU>
             % if symbolic toolbox is not installed
             lasterror( err ); end;  %#ok<LERR>

     elseif( check(what, {2,'cex2'}) );  % jsr finitesness conjecture counterexample
        prop.name = 'cex2';
        M{1} = [1 1; 0 1]; 
        M{2} = 0.7493265463303675579439619*[1 0; 1 1]; 

     elseif( check(what, {3,'cex3'}) );  % jsr finitesness conjecture counterexample
        prop.name = 'cex3';
        M{1} = [1 1; 0 1]; 
        M{2} = 0.75*[1 0; 1 1];            

     elseif( check(what, {'code'}) );  % takes very long time for random parameters, thus we do not allow it for a random choice
        prop.name = 'code';
        M = codecapacity( opt.arg{:}, 'v',opt.verbose-1 ); 

     elseif( check(what, {5,'euler','euler_binary','euler2'}) );
        prop.name = 'euler_binary';
        M = matrix_euler2( opt.arg{:} );

    elseif( check(what, {6,'transition_ones'}) );  % matrices of ones - should be the same as euler. But this is not tested yet    
        prop.name = 'transition_ones';
        N = condget( filter, 'N' );
        dim = condget( filter, 'dim' );
        M = transitionmatrix( {ones(1, dim)', N}, 'Omega',0:dim-1 );
        M = cellfun( @transpose, M, 'uniformoutput', 0 );

    elseif( check(what, {7,'daub'}) );  % Transition matrices of difference scheme for Daubechies wavelets.         
        prop.name = 'daub';
        d = condget(filter, 'dim');
        M = daubechiesmatrix( d, 'v',-1 );

    elseif( check(what, {8,'dd','dd8','dubucdeslauriers'}) ); % Dubuc Deslauriers 8-point scheme  % Prot 2016 p29
        prop.name = 'dubucdeslauriers';
        a = [-5    30   -56   -14   154   -14   -56    30    -5]; 
        M = transitionmatrix( {a',2,[0 1]}, 'Omega',0:7 ); 
        M = cellfun( @transpose, M, 'uniformoutput', 0 );

    elseif( check(what, {9,'morrisp3','mor3'}) );  % A RAPIDLY-CONVERGING LOWER BOUND FOR THE JOINT SPECTRAL RADIUS VIA MULTIPLICATIVE ERGODIC THEORY, IAN D. MORRIS
        prop.name = 'morrisp3';
        M{1} = [2 2; 0 0]; 
        M{2} = [1 1; 1 1]; 

    elseif( check(what, {10,'grip'}) );  % Gripenberg
        prop.name = 'grip';
        M{1} = [0 1; 0 0]; 
        M{2} = 1/5*[0 0; 1 0];

    elseif( check(what, {11,'gripp52','grip52'}) );  % Gripenberg, p52  % long smp=1111111111112 %sic
        prop.name = 'gripp52';
        M{1} = 1/5*[3 0; 1 3]; 
        M{2} = 1/5*[3 -3; 0 -1];
        
     elseif( check(what, {12,'jungers16','jungers16_1','lsr','lsr1'}) );
        prop.name = 'jungers16_1';
        switch numel(opt.arg);
            case 0; k = 8;
            case 1; k = opt.arg{1};
            otherwise; error( 'gallery_matrixset:jungers16', 'wrong options given.' ); end;
        M = {[1 1;0 1], [0 0;-1/k 1]};  % this set has lsr = 0, but it converges to jungers16_2, which has lsr = 1
         
     elseif( check(what, {13,'jungers16_2','lsr2'}) );
        prop.name = 'jungers16_2';
        M = {[1 1;0 1], [0 0;0 1]};  % lsr = 1

    elseif( check(what, {13,'jungers14','lsr3'}) );
        prop.name = 'jungers14';
        M = {[2 0;0 4], [4 0;0 2]};  % sqrt(8) = LSR( M ) >=  max( LSR({2 4}), LSR({4 2}) ) = 2

    elseif( check(what, {14,'kozyakin2013', 'JSR25', 'Jungers25'}) );
        prop.name = 'kozyakin2013';
        if( isempty(opt.arg) );
            if( opt.verbose >= 1 );
                fprintf( 'kozyakin2013: Give value of t as second argumnt. Use a random number in (0,1) now.\n' ); end;
            t = rand;
        else;
            t = opt.arg{1}; end;
        M{1} = (1-t^4)*[1 -t/sqrt(1-t^2);0 0];
        M{2} = (1-t^4)*[sqrt(1-t^2) -t;t sqrt(1-t^2)];
        prop.random = true( 1, 2 );
        
    elseif( check(what, {15,'bear5','bear'}) );
        prop.name = 'bear5';
        M = [[0.0347222222; 0.0075483092; 0.0015096618; 0.0301932367; -0.0392512077; 0.0235507246; 0.5625; 0.0223429952; 0.0117753623; 0.0045289855; 0.0; -0.0078502415; 0.0012077295; 0.2264492754; 0.0132850242; 0.0099637681; -0.009057971; 0.5434782609; -0.0060386473; 0.0175120773; 0.0175120773; -0.0172101449; 0.0036231884; -0.0486111111; 0.0; 0.0301932367; 0.1736111111; 0.0377415459; 0.0075483092; 0.1509661836; -0.1962560386; 0.1177536232; 0.0; 0.1117149758; 0.0588768116; 0.0226449275; 0.0; -0.0392512077; 0.0060386473; 0.1947463768; 0.0664251208; 0.0498188406; -0.0452898551; 0.2173913043; -0.0301932367; 0.0875603865; 0.0875603865; -0.0860507246; 0.018115942; -0.2430555556; 0.0; 0.1509661836] [4.1319444444; 0.9965655193; -0.1435688406; 5.4445954106; -5.4703351449; 2.6030344203; -5.0078125; 2.7158061594; 1.4577672101; 0.6578351449; -0.078125; -1.3017059179; -0.720410628; -5.8660552536; 0.5615942029; 1.3014039855; -1.6646286232; -6.7629076087; -1.1201690821; 1.9464070048; 2.2780042271; -2.2940443841; -0.7810235507; -6.5538194444; 0.15625; 3.1668176329; 1.8055555556; 0.6763851147; 0.0206068841; 2.7472071256; -3.1217164855; 1.270946558; -3.08984375; 1.5596693841; 0.557348279; 0.4342164855; 0.0078125; -0.2760794082; -0.1654589372; -3.2227694746; 0.1313405797; 0.7261096014; -0.7147871377; -3.7924592391; -0.4400664251; 0.7970259662; 1.3846995773; -1.4924705616; -0.2906476449; -3.7717013889; -0.078125; 1.4833182367] [0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.3125; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0625; 0.0; 0.0; 0.0; 0.625; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0625; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.625; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.3125; 0.0; 0.0; 0.0; 0.625; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0] [0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.3125; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0625; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.625; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0625; 0.0; 0.0; 0.0; 0.3125; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0] [0.0; 0.0; 0.0; 0.0625; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0] [3.125; 0.5932971014; -0.027173913; 3.6231884058; -4.2934782609; 0.9510869565; -2.8125; 1.285326087; 0.4755434783; 0.5434782609; 0.0; -0.7336956522; -0.2717391304; -3.7635869565; 0.7608695652; 0.5706521739; -1.0869565217; -4.1576086957; -0.7246376812; 0.8514492754; 1.3722826087; -1.7527173913; -0.1902173913; -5.2083333333; 0.0; 1.3315217391; 1.5625; 0.2966485507; -0.0135869565; 1.8115942029; -1.8342391304; 0.4755434783; -1.40625; 0.6114130435; 0.2377717391; 0.2717391304; 0.0; -0.3668478261; -0.1358695652; -1.8817934783; 0.3804347826; 0.285326087; -0.5434782609; -2.0788043478; -0.3623188406; 0.4257246377; 0.5923913043; -0.8763586957; -0.0951086957; -2.6041666667; 0.0; 0.6657608696] [0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0625; 0.0062311292; 0.0099788647; 0.0832578502; -0.0516379831; 0.0005661232; -0.13046875; 0.0136246981; -0.0153419384; 0.0044157609; 0.0015625; -0.0540081522; 0.0032608696; -0.1159307065; 0.0178140097; 0.0013813406; -0.0281023551; -0.1497961957; -0.01977657; -0.0509812802; 0.016553442; -0.0227694746; 0.000928442; -0.0737847222; -0.015625; -0.0195199275] [-2.6041666667; -0.7300724638; 0.0498188406; -3.1702898551; 3.7047101449; -1.222826087; 2.8125; -1.5126811594; -0.6114130435; -0.4755434783; 0.0; 0.615942029; 0.2898550725; 3.410326087; -0.5615942029; -0.7336956522; 0.9510869565; 3.8722826087; 0.634057971; -0.9012681159; -1.4221014493; 1.8070652174; 0.2445652174; 4.7916666667; 0.0; -1.5036231884; -0.5208333333; -0.1585144928; 0.0099637681; -0.634057971; 0.740942029; -0.2445652174; 0.5625; -0.3025362319; -0.1222826087; -0.0951086957; 0.0; 0.1231884058; 0.0579710145; 0.6820652174; -0.1123188406; -0.1467391304; 0.1902173913; 0.7744565217; 0.1268115942; -0.1802536232; -0.2844202899; 0.5489130435; 0.0489130435; 1.4583333333; 0.0; -0.3007246377] [1.5625; 0.2966485507; -0.0135869565; 1.8115942029; -2.3967391304; 0.7880434783; -1.40625; 1.2364130435; 0.2377717391; 0.2717391304; 0.0; -0.3668478261; -0.1358695652; -1.8817934783; 0.3804347826; 0.285326087; -0.5434782609; -2.0788043478; -0.3623188406; 0.4257246377; 1.1548913043; -0.8763586957; -0.0951086957; -2.6041666667; 0.0; 0.7282608696; 3.125; 0.5932971014; -0.027173913; 3.6231884058; -4.6059782609; 1.0135869565; -2.8125; 1.535326087; 0.4755434783; 0.5434782609; 0.0; -0.7336956522; -0.2717391304; -3.7635869565; 0.7608695652; 0.5706521739; -1.0869565217; -4.1576086957; -0.7246376812; 0.8514492754; 1.6847826087; -1.7527173913; -0.1902173913; -5.2083333333; 0.0; 1.3315217391] [0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0625; 0.0; 0.3125; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0625; 0.0; 0.0] [1.1458333333; -0.1029966787; -0.2561896135; 1.4213466184; -1.9484450483; -0.0350996377; -1.5859375; 0.2802687198; -0.4862998188; 0.2887228261; 0.078125; 0.0568387681; -0.4438405797; -1.5247961957; 0.145531401; -0.3856431159; -0.4576539855; -1.8376358696; -0.3155193237; -0.1058272947; 0.5070199275; -0.5007925725; -0.3575634058; -2.0920138889; -0.15625; 0.1435688406; 3.7847222222; 0.6168855676; -0.299365942; 4.7592089372; -4.8571105072; 2.118432971; -4.0390625; 2.122509058; 1.1529664855; 0.5446105072; -0.015625; -1.3346165459; -0.9172705314; -4.9335371377; 0.5905797101; 0.8439764493; -1.5840126812; -5.6623641304; -1.1080917874; 1.7030495169; 1.8193689614; -1.7908740942; -0.8507699275; -5.8524305556; 0.15625; 2.7036533816] [-0.0763888889; -0.0074879227; -0.003442029; -0.0966183575; 0.0644927536; -0.0253623188; 0.1; -0.015942029; -0.0126811594; 0.0063405797; 0.0; 0.0265096618; 0.0016908213; 0.1170289855; -0.0202898551; -0.006884058; 0.0289855072; 0.1358695652; 0.0193236715; -0.0393719807; -0.0171497585; 0.0300724638; 0.0009057971; 0.0972222222; 0.0; -0.0743961353; -0.2361111111; -0.0233997585; -0.0123188406; -0.3019323671; 0.1952898551; -0.1105072464; 0.33125; -0.0623188406; -0.0490036232; -0.0036231884; 0.0; 0.0854468599; 0.0032004831; 0.3750905797; -0.0634057971; -0.0246376812; 0.0905797101; 0.4402173913; 0.0603864734; -0.1417874396; -0.0556763285; 0.0908514493; 0.0012681159; 0.2944444444; 0.0; -0.290821256] [0.1041666667; -0.1729128925; 0.0488375604; 0.2041817633; -0.24633907; -0.7501132246; -1.12890625; -0.3443916063; -0.9219316123; 0.0866168478; -0.0390625; 0.1024682971; 0.0960144928; -0.5793138587; 0.0422705314; -0.5177762681; -0.014379529; -0.8950407609; 0.0060386473; -1.1789704106; -0.0478940217; -0.0179461051; 0.0823143116; -0.1519097222; 0.078125; -1.0319293478; -0.3125; -0.3763964372; -0.0907306763; -0.3389190821; -0.1878774155; -1.2081068841; -0.6171875; -0.6584390097; -1.385303442; 0.1392663043; 0.015625; 0.7533967391; 0.0163043478; 0.0961277174; -0.0984299517; -0.9227807971; 0.1954257246; -0.0849184783; 0.1615338164; -1.3226147343; -0.1750452899; 0.1400588768; 0.0437047101; 0.1649305556; -0.15625; -1.3944746377] [-0.0416666667; -0.0133605072; 0.0150362319; -0.053442029; 0.015307971; -0.0366847826; 0.028125; -0.0287137681; -0.0245923913; 0.0013586957; 0.0; -0.0294384058; 0.0170289855; 0.0585597826; -0.0126811594; -0.0220108696; 0.0285326087; 0.0692934783; 0.0294384058; -0.0197463768; -0.0155797101; 0.0167119565; 0.0073369565; 0.0354166667; 0.0; -0.0367753623; -0.1041666667; -0.0473278986; 0.0561594203; -0.1268115942; 0.0086050725; -0.1426630435; -0.140625; -0.1000905797; -0.1025815217; 0.0122282609; 0.0; -0.1191123188; 0.0615942029; 0.0957880435; -0.0307971014; -0.0855978261; 0.0692934783; 0.0611413043; 0.0566123188; -0.0860507246; -0.044384058; 0.0441576087; 0.0285326087; 0.0729166667; 0.0; -0.1476449275] [0.6666666667; 0.0974637681; -0.1188405797; 0.9148550725; -0.4476449275; 0.1385869565; -2.475; 0.2061594203; 0.0380434783; 0.0434782609; 0.0; 0.1579710145; -0.1550724638; -1.5010869565; 0.1942028986; 0.0956521739; -0.2744565217; -2.2826086957; -0.1204710145; -0.031884058; 0.1889492754; -0.2402173913; -0.0652173913; -0.6666666667; 0.0; 0.0731884058; -0.3541666667; -0.0683876812; -0.0990942029; -0.3985507246; 0.4514492754; -0.2608695652; -0.39375; -0.2365942029; -0.1366847826; -0.0597826087; 0.0; 0.2327898551; -0.1242753623; -0.0453804348; -0.1920289855; -0.0940217391; 0.3070652174; -0.2364130435; 0.2672101449; -0.2561594203; -0.1894927536; 0.170923913; -0.135326087; 0.5041666667; 0.0; -0.381884058] [-5.2083333333; -0.9601449275; 0.0996376812; -6.3405797101; 7.4094202899; -2.4456521739; 5.625; -3.0253623188; -1.160326087; -0.9510869565; 0.0; 1.231884058; 0.5797101449; 6.8206521739; -1.1231884058; -1.1548913043; 1.902173913; 7.7445652174; 1.268115942; -1.8025362319; -2.8442028986; 2.6766304348; 0.4891304348; 8.3958333333; 0.0; -3.0072463768; -5.2083333333; -1.2726449275; 0.0996376812; -6.3405797101; 7.4094202899; -2.4456521739; 5.625; -3.0253623188; -1.222826087; -0.9510869565; 0.0; 1.231884058; 0.5797101449; 6.8206521739; -1.1231884058; -1.4048913043; 1.902173913; 7.7445652174; 1.268115942; -1.8025362319; -2.8442028986; 2.9891304348; 0.4891304348; 8.6458333333; 0.0; -3.0072463768] [-0.3125; 0.0430253623; -0.1684782609; -0.4528985507; 0.3804347826; 0.2717391304; 1.40625; 0.0815217391; 0.2921195652; -0.0679347826; 0.0; 0.3260869565; -0.1222826087; 0.8220108696; -0.0951086957; 0.1630434783; 0.1358695652; 1.222826087; 0.0905797101; 0.3623188406; -0.0543478261; 0.1019021739; -0.0543478261; 0.4166666667; 0.0; 0.3804347826; -0.625; 0.0860507246; -0.1494565217; -0.9057971014; 0.7608695652; 0.5434782609; 2.8125; 0.1630434783; 0.5842391304; -0.1358695652; 0.0; 0.027173913; -0.1820652174; 1.6440217391; -0.1902173913; 0.3260869565; 0.2717391304; 2.4456521739; 0.1811594203; 0.7246376812; -0.1086956522; 0.2038043478; -0.1086956522; 0.8333333333; 0.0; 0.7608695652] [0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0625; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; -0.0625; 0.0086050725; -0.0461956522; -0.0905797101; 0.0760869565; 0.0543478261; 0.28125; 0.0163043478; 0.058423913; -0.0135869565; 0.0; 0.2527173913; -0.0244565217; 0.1644021739; -0.0190217391; 0.0326086957; 0.027173913; 0.2445652174; 0.018115942; 0.0724637681; -0.0108695652; 0.0203804348; -0.0108695652; 0.0833333333; 0.0; 0.0760869565] [0.3125; 0.0311556461; 0.0498943237; 0.4162892512; -0.2581899155; 0.0028306159; -0.65234375; 0.0681234903; -0.076709692; 0.0220788043; 0.0078125; -0.2700407609; 0.0163043478; -0.5796535326; 0.0890700483; 0.0069067029; -0.1405117754; -0.7489809783; -0.0988828502; -0.254906401; 0.0827672101; -0.1138473732; 0.0046422101; -0.3689236111; -0.015625; -0.0975996377; 0.5208333333; 0.0152664553; 0.08959843; 0.7068991546; -0.4623716787; -0.1454936594; -1.26953125; 0.0401192633; -0.3071218297; 0.0526494565; -0.0078125; -0.411571558; 0.0452898551; -1.0433084239; 0.1509661836; -0.092504529; -0.2276947464; -1.3773777174; -0.1570048309; -0.6436443237; 0.1228487319; -0.1857450181; 0.0238903986; -0.6206597222; 0.078125; -0.3625452899] [0.3819444444; 0.003321256; 0.0256642512; 0.5132850242; -0.1672705314; -0.0996376812; -0.5625; -0.1201690821; 0.0126811594; 0.0769927536; 0.0; -0.1542874396; -0.0003019324; -0.6503623188; 0.1008454106; -0.018115942; -0.1539855072; -0.7608695652; -0.1026570048; 0.172705314; -0.0356280193; -0.1050724638; -0.0009057971; -0.3888888889; 0.0; 0.1799516908; -0.7361111111; -0.1365640097; 0.0089371981; -0.8212560386; 0.9926328502; -0.2155797101; 0.50625; -0.3577294686; -0.0765398551; 0.0018115942; 0.0; 0.1593599034; 0.0629830918; 0.7968297101; -0.186352657; -0.1210144928; 0.2463768116; 0.8423913043; 0.1642512077; -0.1263285024; -0.3679951691; 0.411865942; 0.0389492754; 1.2222222222; 0.0; -0.1879227053] [-0.0694444444; 0.1911043176; 0.028759058; 0.3060839372; -0.8024230072; 0.204370471; -0.69921875; 0.349071558; 0.0240602355; 0.1149230072; 0.0390625; 0.1002792874; 0.0410628019; -0.5468183877; -0.0969202899; 0.1486639493; -0.0449501812; -0.7014266304; -0.0143417874; 0.0624245169; 0.3480147947; -0.4822803442; -0.0148324275; -1.2196180556; -0.078125; 0.208861715; -0.3055555556; -0.00600468; -0.0102807971; -0.2281853865; -0.0342617754; -0.0073596014; 0.14609375; -0.0243432971; 0.0119451993; -0.0157382246; -0.0015625; -0.020116244; -0.0062801932; 0.1904325181; -0.0579710145; -0.0054574275; 0.0403306159; 0.1973505435; 0.0175120773; 0.0002566425; -0.0186669686; -0.0539968297; 0.0004302536; -0.2907986111; 0.015625; -0.0142964976] [-0.8333333333; -0.1524003623; -0.0190217391; -0.4846014493; 0.7758152174; -0.4279891304; 0.984375; -0.4252717391; -0.2139945652; -0.0883152174; 0.0; 0.1218297101; -0.0235507246; 0.8811141304; -0.3423913043; -0.1942934783; 0.0828804348; 1.120923913; 0.0656702899; -0.2998188406; -0.3414855072; 0.3512228261; -0.1019021739; 0.9895833333; 0.0; -0.5262681159; -0.1666666667; -0.0304800725; -0.0038043478; 0.0905797101; 0.1551630435; -0.0855978261; 0.196875; -0.0850543478; -0.042798913; -0.0176630435; 0.0; 0.024365942; -0.0047101449; 0.1762228261; -0.0684782609; -0.0388586957; 0.004076087; 0.2241847826; 0.013134058; -0.0599637681; -0.0682971014; 0.0702445652; -0.0203804348; 0.1979166667; 0.0; -0.1052536232] [-0.625; 0.0860507246; 0.1630434783; -0.9057971014; 0.7608695652; 0.5434782609; 2.8125; 0.1630434783; 0.5842391304; -0.1358695652; 0.0; -0.285326087; 0.0679347826; 1.6440217391; -0.1902173913; 0.3260869565; 0.2717391304; 2.4456521739; 0.1811594203; 0.7246376812; -0.1086956522; 0.2038043478; -0.0461956522; 0.8333333333; 0.0; 0.7608695652; -0.3125; 0.0430253623; 0.3940217391; -0.4528985507; 0.3804347826; 0.2717391304; 1.40625; 0.0815217391; 0.2921195652; -0.0679347826; 0.0; -0.2364130435; 0.5027173913; 0.8220108696; -0.0326086957; 0.1630434783; 0.1358695652; 1.222826087; 0.0905797101; 0.3623188406; -0.0543478261; 0.1019021739; 0.2581521739; 0.4166666667; 0.0; 0.3804347826] [0.1736111111; 0.0377415459; 0.0075483092; 0.1509661836; -0.1962560386; 0.1177536232; -0.3125; 0.1117149758; 0.0588768116; 0.0226449275; 0.0; -0.0392512077; 0.0060386473; -0.3677536232; 0.0664251208; 0.0498188406; -0.0452898551; -0.4076086957; -0.0301932367; 0.0875603865; 0.0875603865; -0.0860507246; 0.018115942; -0.2430555556; 0.0; 0.1509661836; 0.0347222222; 0.0075483092; 0.0015096618; 0.0301932367; -0.0392512077; 0.0235507246; -0.0625; 0.0223429952; 0.0117753623; 0.0045289855; 0.0; -0.0078502415; 0.0012077295; -0.0860507246; 0.0132850242; 0.0099637681; -0.009057971; -0.0815217391; -0.0060386473; 0.0175120773; 0.0175120773; -0.0172101449; 0.0036231884; -0.0486111111; 0.0; 0.0301932367] [0.0; 0.0; 0.0; 0.0; 0.0; 0.625; 0.0; 0.3125; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0; 0.0625; 0.0; 0.0; 0.0; 0.0; 0.625; 0.3125; 0.0593297101; -0.0027173913; 0.3623188406; -0.4918478261; 0.7201086957; -0.28125; 0.7472826087; 0.0475543478; 0.0543478261; 0.0; -0.0733695652; -0.027173913; -0.3763586957; 0.0760869565; 0.0570652174; -0.1086956522; -0.4157608696; -0.0724637681; 0.0851449275; 0.4184782609; -0.1752717391; -0.0190217391; -0.5208333333; 0.0; 0.4456521739] [-1.2430555556; -0.1852355072; -0.0071859903; -1.4492753623; 1.4368357488; -0.2554347826; 1.08125; -0.411352657; -0.1214673913; -0.1757246377; 0.0; 0.3212560386; 0.0823067633; 1.5200181159; -0.318236715; -0.1532608696; 0.4347826087; 1.6630434783; 0.2898550725; -0.3072463768; -0.4711352657; 0.6198369565; 0.0510869565; 1.8916666667; 0.0; -0.4381642512; -0.6944444444; -0.0996376812; -0.0060386473; -0.8152173913; 0.7820048309; -0.1358695652; 0.625; -0.2143719807; -0.0679347826; -0.1639492754; 0.0; 0.184178744; 0.0437801932; 0.8650362319; -0.1781400966; -0.0815217391; 0.2445652174; 0.9510869565; 0.1630434783; -0.1811594203; -0.2506038647; 0.339673913; 0.027173913; 1.0416666667; 0.0; -0.2596618357]];
        M = mat2cell( M, [26 26], 26 )';
        
    elseif( check(what, {13,'morris17'}) );
        prop.name = 'morris17';
        val = [opt.arg{:}];
        switch numel( val );
            case 0;
                la = rand;
                be = rand;
                th = randu( [0 2*pi] );
            case 2;
                la = val(1);
                be = val(2);
                th = randu( [0 2*pi] );
            case 3;
                la = val(1);
                be = val(2);
                th = val(3);
             otherwise;
                 error( 'gallery_matrixset:morris17', '''morris17'' needs 0,2 or 3 arguments.' ); end;             
        M = {[la be;0 0], [cos(th) -sin(th);sin(th) cos(th)]};
    end
end

function [ M, G_, prop ] = getmatrix_bad( filter, opt );
    maybe_unused( filter );
    if( check(opt.what, {'bad'}) );
        opt.what = []; end;
    M = {};
    G_ = [];
    prop = struct;
    what = opt.what;
    if( isempty(what) );
        what = randi( 4 ); end;
    % Bad examples where something goes wrong
    if( false );
    elseif( check(what, {1,'nearlycomplex'}) );  % pascal rhombus  % jsr=2; Prot2012, p40 
        prop.name = 'nearlycomplex';
        M{1} = [0 1 0 0 0; 1 0 2 0 0; 0 0 0 0 0; 0 1 0 0 1; 0 0 0 2 1]; 
        M{2} = [1 0 2 0 0; 0 0 0 2 1; 1 1 0 0 0; 0 0 0 0 0; 0 1 0 0 0]; 

    elseif( check(what, {2,'prot2012p35','gp20132'}) );  % Prot2012, p35
        prop.name = 'prot2012p35';
        M{1} = [1 1; 0 1]; 
        M{2} = 9/10*[1 0; 1 1]; 

    elseif( check(what, {3,'prot2016p35'}) );  % Prot2016 p50 p35 p33 % matrices have common invariant subspace
        prop.name = 'prot2016p35';
        M{1} = 1/48*[48    0     0   -12     0     0;     0     0     0     0    12     0;     0     0     0     0     0    12;     0     0     0   -12     0     0;     0    12     0     0    36     0;     0     0    12     0     0    36;];
        M{2} = 1/48*[0    12     0     4     9    -4;    48    36    36   -92    -9    11;     0     0   -12   100     0   -25;     0     0     0    20     0    -8;     0     0   -48    80    48   -20;     0     0     0  -112     0    16];
        M{3} = 1/48*[0     0    12     4    -4     9;     0   -12     0   100   -25     0;    48    36    36   -92    11    -9;     0     0     0    20    -8     0;     0     0     0  -112    16     0;     0   -48     0    80   -20    48];
        M{4} = 1/48*[0    -4    -4   -12     5     5;     0    80  -100     0    80  -103;     0  -100    80     0  -103    80;    48    16    16   -12    16    16;     0   -80   112     0   -92   100;     0   112   -80     0   100   -92];            

    elseif( check(what, {4,'st06_2'}) );  % Schneider, Tam - Matrices leaving a cone invariant, Example 2
        prop.name = 'st06_2';
        prop.K3_irreducible = true;
        M{1} = [-1 0 0;0 0 0;0 0 1];

    elseif( check(what, {5,'bad_for_ipa_1'}) );  % M{1} has duplicate leading eigenvector. M{1} and M{2} both are s.m.p.s. Ipa must not terminate
        prop.name = 'bad_for_ipa_1';
        M = {[8 2 -6;-7 18 -5;-4 -2 10], [6 2 8;2 6 8;4 2 10]};
    end;
end

function [ M, G_, prop ] = getmatrix_kone( filter, opt );
    maybe_unused( filter );
    if( check(opt.what, {'kone'}) );
        if( ~isempty(opt.what_idx) );
            what = opt.what_idx;
        elseif( numel(opt.arg) == 1 );
            what = opt.arg{1};
            opt.arg = [];
        else;
            what = []; end;
    else;
        what = opt.what; end;
    M = {};
    G_ = [];
    prop = struct;
    if( isempty(what) );
        what = randi( [0 35] ); end;
    % Examples interesting for kone computation
    if( false );
    elseif( check(what, {0,'kone_binary_neg'}) );
        prop.name = 'kone_binary_neg';
        dim = condget( filter, 'dim' );
        threshold_base = rand;
        mixed_threshold = rand < .5;
        n = 0;
        n_max = condget( filter, 'N' );
        while( n < n_max );
            threshold = tif( mixed_threshold, rand, threshold_base );
            Mn = double( rand( dim ) < threshold );
            idx_nonzeros = find( Mn(:) );
            if( ~isempty(idx_nonzeros) )
                idx = randi( numel(idx_nonzeros) );
                idx = idx_nonzeros(idx);
                Mn(idx) = -Mn(idx); end;
            if( matrixinfo(Mn, 'vandergraft', 'verbose',opt.verbose - 1) );
                n = n + 1;
                M{n} = Mn; end; end;
        prop.random = true( size(M) );

    elseif( check(what, {1,'kone_nonneg_neg'}) );
        prop.name = 'kone_nonneg_neg';
        dim = condget( filter, 'dim' );
        threshold_base = rand*.8 + .1;
        rounds_base = randi( [1 dim] );
        mixed = rand < .5;
        n = 0;
        n_max = condget( filter, 'N' );
        while( n < n_max );
            threshold = tif( mixed, rand, threshold_base );
            while( true );
                Mn = rand( dim ) .* double( rand(dim) < threshold );
                if( dim == 1 || ...
                    any(Mn(:)) && ~all(Mn(:)) ...
                  );
                    break; end; end;

            if( dim > 1 );
                idx_nonzeros = find( Mn(:) );
                rounds = tif( mixed, randi([1 dim]), rounds_base );
                rounds = clamp( rounds, 0, floor(numel(idx_nonzeros)/2) );
                idx = randperm( numel(idx_nonzeros), rounds );
                idx = idx_nonzeros(idx);
                Mn(idx) = -Mn(idx); end;
            if( matrixinfo(Mn, 'vandergraft', 'verbose',opt.verbose-1) );
                n = n + 1;
                M{n} = Mn; end; end;
        prop.random = true( size(M) );

    elseif( check(what, {1,'kone_ab'}) );
        prop.name = 'kone_ab';
        prop.has_invariant_kone = true;  % true
        prop.has_polyhedral_minimal_invariant_kone = true;  % true
        % extremal vertices: eigenvectors of M1*M2, and M2*M1
        % ipa v2024-06-20 computes it: [r,nfo] = ipa( T, 'plot',{'polytope','tree'}, 'kone', 'smp',{[1;2]} )
        M = cell( 1, 2 );
        M{1}=[0 2;1 3];
        M{2}=[2 4;3 5];

    elseif( check(what, {2,'kone_p10_1'}) );  % Protasov - 2010 - When do several linear operators share an invariant cone, Example 1
        % has no invariant cone
        prop.name = 'kone_p10_1';
        prop.has_invariant_kone = false;
        if( numel(opt.arg) == 1 );
            al = opt.arg{1};
        else;
            al = rand; end;
        M{1} = [1 0 1;0 1 0;0 0 1];
        M{2} = [0 -al 0;al 0 0;0 0 1];

    elseif( check(what, {3,'kone_emt05_87'}) );  % Edwards, McDonald, Tsatsomeros - 2005 - On matrices with common invariant cones with applications in neural and gene networks, Example 8.7
        prop.name = 'kone_emt05_87';
        prop.has_invariant_kone = true;
        prop.has_invariant_subspace = true;
        M{1} = [3 -3 1;0 4 0;-1 -3 5];
        M{2} = [-1 -3 5;-2 6 2;-7 -1 11];

    elseif( check(what, {4,'kone_emt05_89'}) );  % Edwards, McDonald, Tsatsomeros - 2005 - On matrices with common invariant cones with applications in neural and gene networks, Example 8.9
        % Both A and B satisfy the Perron condition and so they each have (proper) invariant
        % cones. Let us consider whether A and B have a common invariant cone of dimension
        % k ⩽2 or not. We compute the second compounds of A and B to be
        % The matrices C(A, B) and C(A(2), B(2)) of Theorem 8.6 are invertible. As a
        % consequence, A and B do not have a common invariant subspace of dimension 1
        % (eigenvector) nor dimension 2. Thus they cannot have a common invariant cone of
        % dimension 1 nor dimension 2
        prop.name = 'kone_emt05_89';
        prop.has_invariant_kone = false;
        M{1} = [3 0 1;-5 -2 -1;1 0 3];
        M{2} = [1 2 0;-1 0 1;1 1 1];

    elseif( check(what, {5,'kone_rss10_71','kone_rss10_1'}) );  % Rodman, Seyalioglu, Spitkovsky - 2010 - On common invariant cones for families of matrices, Example 7.1
        prop.name = 'kone_rss10_71';
        prop.has_invariant_kone = false;
        if( numel(opt.arg) == 1 && numel(opt.arg{1}) == 2 );
            pq = opt.arg{1};
            assert( ~isequal(pq(1), pq(2)), 'gallery_matrixset:args', 'The two given numbers must be different.' );
        else;
            pq = rand( 1, 2 ); end;
        M{1} = [1 pq(1);0 -1];
        M{2} = [1 pq(2);0 -1];
        prop.random = true( size(M) );

    elseif( check(what, {6,'kone_rss10_72','kone_rss10_2'}) );  % Rodman, Seyalioglu, Spitkovsky - 2010 - On common invariant cones for families of matrices, Example 7.2
        % A triple of matrices T := {A, B, C},
        % A, B, C ∈ R2×2
        % V
        % with the following properties:
        % (a) det M > 0 for all M ∈T;
        % (b) A, B, C are normal matrices (in particular, diagonalizable);
        % L. Rodman et al. / Linear Algebra and its Applications 432 (2010) 911–926
        % 925
        % (c) there is no T-invariant proper cone;
        % (d) each pair of matrices in T has a common invariant proper cone;
        % (e) no two matrices in T have a common eigenvector.
        if( numel(opt.arg) == 1 );
            assert( numel(opt.arg{1}) == 6, 'gallery_matrixset:args', 'Wrong number of arguments.' );
            la = reshape( opt.arg{1}, [2 3] );
            assert( all( diff(la, [], 1) > 0 ), 'gallery_matrixset:args', 'The leading eigenvalues must be larger than the non-leading eigenvalues. Give the eigenvalues as a 2xJ matrix.' );
        else;
            la = rand( 2, 3 );
            la = sort( la, 1 ); end;
        M{1} = matrix_from_eig( [1  0; 0 1]', la(1:2) );
        M{2} = matrix_from_eig( [1  2;-2 1]', la(3:4) );
        M{3} = matrix_from_eig( [1 -2; 2 1]', la(5:6) );
        prop.name = 'kone_rss10_72';
        prop.has_invariant_kone = false;
        prop.normal = true( size(M) );
        prop.pairs_have_invariant_kone = true;
        prop.random = true( size(M) );

    elseif( check(what, {7,'kone_rss10_72_12','kone_rss10_2_12','kone_rss10_72_3','kone_rss10_2_3'}) );  % Rodman, Seyalioglu, Spitkovsky - 2010 - On common invariant cones for families of matrices, Example 7.2, matrix M{1} and M{2}
        % extremal vertices of kone: Leading eigenvectors of: M1 and M2
        if( numel(opt.arg) == 1 );
            assert( numel(opt.arg{1}) == 4, 'gallery_matrixset:args', 'Wrong number of arguments.' );
            la = reshape( opt.arg{1}, [2 2] );
            assert( all( diff(la, [], 1) > 0 ), 'gallery_matrixset:args', 'The leading eigenvalues must be larger than the non-leading eigenvalues. Give the eigenvalues as a 2xJ matrix.' );
        else;
            la = rand( 2, 2 );
            la = sort( la, 1 ); end;
        M = {matrix_from_eig( [1  0; 0 1]', la(1:2) ), ...
             matrix_from_eig( [1  2;-2 1]', la(3:4) )};
        prop.name = 'kone_rss10_72_12';
        prop.has_invariant_kone = true;
        prop.has_polyhedral_minimal_invariant_kone = true;
        prop.normal = true( size(M) );
        prop.random = true( size(M) );

    elseif( check(what, {8,'kone_rss10_72_23','kone_rss10_2_23','kone_rss10_72_1','kone_rss10_2_1'}) );  % Rodman, Seyalioglu, Spitkovsky - 2010 - On common invariant cones for families of matrices, Example 7.2, matrix M{2} and M{3}
        % extremal vertices of kone: Leading eigenvectors of: M1 and M2
        if( numel(opt.arg) == 1);
            assert( numel(opt.arg{1}) == 4, 'gallery_matrixset:args', 'Wrong number of arguments.' );
            la = reshape( opt.arg{1}, [2 2] );
            assert( all( diff(la, [], 1) > 0 ), 'gallery_matrixset:args', 'The leading eigenvalues must be larger than the non-leading eigenvalues. Give the eigenvalues as a 2xJ matrix.' );
        else;
            la = rand( 2, 2 );
            la = sort( la, 1 ); end;
        M = cell( 1, 2 );
        M{1} = matrix_from_eig( [1  2;-2 1]', la(1:2) );
        M{2} = matrix_from_eig( [1 -2; 2 1]', la(3:4) );
        prop.name = 'kone_rss10_72_23';
        prop.has_invariant_kone = true;
        prop.normal = true( size(M) );
        prop.random = true( size(M) );

    elseif( check(what, {9,'kone_rss10_72_31','kone_rss10_2_31','kone_rss10_72_13','kone_rss10_2_13','kone_rss10_72_2','kone_rss10_2_2','kone_rss10_72_2','kone_rss10_2_2'}) );  % Rodman, Seyalioglu, Spitkovsky - 2010 - On common invariant cones for families of matrices, Example 7.2, matrix M{3} and M{1}
        if( numel(opt.arg) == 1 );
            assert( numel(opt.arg{1}) == 4, 'gallery_matrixset:args', 'Wrong number of arguments.' );
            la = reshape( opt.arg{1}, [2 2] );
            assert( all( diff(la, [], 1) > 0 ), 'gallery_matrixset:args', 'The leading eigenvalues must be larger than the non-leading eigenvalues. Give the eigenvalues as a 2xJ matrix.' );
        else;
            la = rand( 2, 2 );
            la = sort( la, 1 ); end;
        M{1} = matrix_from_eig( [1  0; 0 1]', la(1:2) );
        M{2} = matrix_from_eig( [1 -2; 2 1]', la(3:4) );
        prop.name = 'kone_rss10_72_31';
        prop.has_invariant_kone = true;
        prop.normal = true( size(M) );
        prop.random = true( size(M) );

    elseif( check(what, {10,'kone_rss10_73','kone_rss10_3'}) );  % Rodman, Seyalioglu, Spitkovsky - 2010 - On common invariant cones for families of matrices, Example 7.3
        if( numel(opt.arg) == 1 );
            assert( numel(opt.arg{1}) == 8, 'gallery_matrixset:args', 'Wrong number of arguments.' );
            la = reshape( opt.arg{1}, [2 4] );
            assert( all( diff(la, [], 1) > 0 ), 'gallery_matrixset:args', 'The leading eigenvalues must be larger than the non-leading eigenvalues. Give the eigenvalues as a 2xJ matrix.' );
        else;
            la = rand( 2, 4 );
            la = sort( la, 1 ); end;
        M{1} = matrix_from_eig( [1  0; 0  1]', la(1:2) );
        M{2} = matrix_from_eig( [0  1; 1  0]', la(3:4) );
        M{3} = matrix_from_eig( [1  1; 1 -1]', la(5:6) );
        M{4} = matrix_from_eig( [1 -1; 1  1]', la(7:8) );
        prop.name = 'kone_rss10_73';
        prop.has_invariant_kone = false;
        prop.triples_have_invariant_kone = true;
        prop.random = true( size(M) );
        
    elseif( check(what, {11,'kone_rss10_73_1','kone_rss10_3_1'}) );  % Rodman, Seyalioglu, Spitkovsky - 2010 - On common invariant cones for families of matrices, Example 7.3, No matrix 1
        if( numel(opt.arg) == 1 );
            assert( numel(opt.arg{1}) == 6, 'gallery_matrixset:args', 'Wrong number of arguments.' );
            la = reshape( opt.arg{1}, [2 3] );
            assert( all( diff(la, [], 1) > 0 ), 'gallery_matrixset:args', 'The leading eigenvalues must be larger than the non-leading eigenvalues. Give the eigenvalues as a 2xJ matrix.' );
        else;
            la = rand( 2, 3 );
            la = sort( la, 1 ); end;
        M = cell( 1, 3 );
        M{1} = matrix_from_eig( [0  1; 1  0]', la(1:2) );
        M{2} = matrix_from_eig( [1  1; 1 -1]', la(3:4) );
        M{3} = matrix_from_eig( [1 -1; 1  1]', la(5:6) );
        prop.name = 'kone_rss10_73_1';
        prop.has_invariant_kone = true;
        prop.random = true( size(M) );
        
    elseif( check(what, {12,'kone_rss10_73_2','kone_rss10_3_2'}) );  % Rodman, Seyalioglu, Spitkovsky - 2010 - On common invariant cones for families of matrices, Example 7.3, No matrix 2
        if( numel(opt.arg) == 1 );
            assert( numel(opt.arg{1}) == 6, 'gallery_matrixset:args', 'Wrong number of arguments.' );
            la = reshape( opt.arg{1}, [2 3] );
            assert( all( diff(la, [], 1) > 0 ), 'gallery_matrixset:args', 'The leading eigenvalues must be larger than the non-leading eigenvalues. Give the eigenvalues as a 2xJ matrix.' );
        else;
            la = rand( 2, 3 );
            la = sort( la, 1 ); end;
        M{1} = matrix_from_eig( [1  0; 0  1]', la(1:2) );
        M{2} = matrix_from_eig( [1  1; 1 -1]', la(3:4) );
        M{3} = matrix_from_eig( [1 -1; 1  1]', la(5:6) );
        prop.name = 'kone_rss10_73_2';
        prop.has_invariant_kone = false;
        prop.random = true( size(M) );
        
    elseif( check(what, {13,'kone_rss10_73_3','kone_rss10_3_3'}) );  % Rodman, Seyalioglu, Spitkovsky - 2010 - On common invariant cones for families of matrices, Example 7.3, No matrix 3
        if( numel(opt.arg) == 1 );
            assert( numel(opt.arg{1}) == 6, 'gallery_matrixset:args', 'Wrong number of arguments.' );
            la = reshape( opt.arg{1}, [2 3] );
            assert( all( diff(la, [], 1) > 0 ), 'gallery_matrixset:args', 'The leading eigenvalues must be larger than the non-leading eigenvalues. Give the eigenvalues as a 2xJ matrix.' );
        else;
            la = rand( 2, 3 );
            la = sort( la, 1 ); end;
        M{1} = matrix_from_eig( [1  0; 0  1]', la(1:2) );
        M{2} = matrix_from_eig( [0  1; 1  0]', la(3:4) );
        M{3} = matrix_from_eig( [1 -1; 1  1]', la(5:6) );
        prop.name = 'kone_rss10_73_3';
        prop.has_invariant_kone = false;
        prop.random = true( size(M) );
        
    elseif( check(what, {14,'kone_rss10_73_4','kone_rss10_3_4'}) );  % Rodman, Seyalioglu, Spitkovsky - 2010 - On common invariant cones for families of matrices, Example 7.3, No matrix 4
        if( numel(opt.arg) == 1 );
            assert( numel(opt.arg{1}) == 6, 'gallery_matrixset:args', 'Wrong number of arguments.' );
            la = reshape( opt.arg{1}, [2 3] );
            assert( all( diff(la, [], 1) > 0 ), 'gallery_matrixset:args', 'The leading eigenvalues must be larger than the non-leading eigenvalues. Give the eigenvalues as a 2xJ matrix.' );
        else;
            la = rand( 2, 3 );
            la = sort( la, 1 ); end;
        M{1} = matrix_from_eig( [1  0; 0  1]', la(1:2) );
        M{2} = matrix_from_eig( [0  1; 1  0]', la(3:4) );
        M{3} = matrix_from_eig( [1  1; 1 -1]', la(5:6) );
        prop.name = 'kone_rss10_73_4';
        prop.has_invariant_kone = false;
        prop.random = true( size(M) );

    elseif( check(what, {15,'kone_rss10_74','kone_rss1074'}) );  % Rodman, Seyalioglu, Spitkovsky - 2010 - On common invariant cones for families of matrices, Example 7.4
        M{1} = [1 1;0 1];
        M{2} = [1 -1;0 1];
        prop.name = 'kone_rss10_74';
        prop.has_invariant_kone = false;

    elseif( check(what, {16,'kone_rss10_75','kone_rss1075'}) );  % Rodman, Seyalioglu, Spitkovsky - 2010 - On common invariant cones for families of matrices, Example 7.5
        M{1} = [1 0 0;0 -1 0;0 0 -1];
        M{2} = [-1 0 0;0 -1 0;0 0 1];
        prop.name = 'kone_rss10_75';
        prop.has_invariant_kone = false;

    elseif( check(what, {17,'kone_rss10_76','kone_rss1076'}) );  % Rodman, Seyalioglu, Spitkovsky - 2010 - On common invariant cones for families of matrices, Example 7.5
        qm = [];
        r = [];
        if( numel(opt.arg) == 1 );
            if( numel(opt.arg{1}) == 1 );
                r = opt.arg{1};
            else;
                r = opt.arg{1}(1);
                qm = opt.arg{1}(2:end); end;
        elseif( numel(opt.arg) == 2 );
            r = opt.arg{1};
            qm = opt.arg{2}; end;
        if( isempty(r) );
            r = rand; end;
        if( isempty(qm) );
            len = randi( [1 10] );
            qm = cumsum( rand(1, len) ).^2; end;
        M = cell( 1, numel(qm) );
        for j = 1:numel( qm );
            M{j} = [1 qm(j);0 r]; end;
        prop.name = 'kone_rss10_76';
        prop.has_invariant_kone = true;
        prop.random = true( size(M) );

    elseif( check(what, {18,'kone_sw91_41'}) );  % Stern, Wolkowicz- 1991 - Invariant Ellipsoidal Cones, Example 4.1
        M = {[0 0 0;1 0 0;0 1 0]};
        prop.name = 'kone_sw91_41';
        prop.has_ellipsoidal_invariant_kone = false;

    % In "Stern, Wolkowicz- 1991 - Invariant Ellipsoidal Cones", there is a notion about "Inertia" which may be of interest for more examples
    elseif( check(what, {19,'kone_sw91_54'}) );  % Stern, Wolkowicz- 1991 - Invariant Ellipsoidal Cones, Example 4.1
        M = {[2 -1/2 -1;-1/2 1 0;-1 0 0]};
        prop.name = 'kone_sw91_54';
        prop.has_ellipsoidal_invariant_kone = false;
        prop.info = {'inertia',[2 0 1]};

    elseif( check(what, {20,'kone_regression_1'}) );
        M = cell( [1 2] );
        M{1} = [0.066425498017150336 0.5504731842592242 1.0752896391026423;0.75608495740446036 0.43256478715876595 -0.08581496766222603; -0.13551791083395928  0.84640652833340846 0.1849524359369234 ];
        M{2} = [1.6455415947111531   1.2951305757773783 1.1742748457568832;2.5019610848003846  0.76506820843925438  0.029405066849389781;-0.38155563955987426 -0.45112026266628835 0.92522125230695518];
        prop.name = 'kone_regression_1';
        prop.has_invariant_kone = true;
        prop.has_polyhedral_minimal_invariant_kone = true;

    elseif( check(what, {21,'kone_regression_2'}) );
        % leading eigenvectors as computed by matlab do not yield an invariant kone, since some point in the wrong direction
        M = cell( [1 2] );
        M{1} = [-1.3073322741750224 -1.4591560443577878   0.55945985678468524;  2.4292539523544172   1.8420658893769042 -0.92984200029460307;0.91493127126345919 -0.64172398020843802 1.0153332079070934];
        M{2} = [-0.48165199844922441 0.51280957226315815 -0.089765448920425553;-0.50235455671635576 -0.62859506583735314 0.4939996964736722; 0.32976589848533611 -1.2300107736636579  3.5142639866180008];
        prop.name = 'kone_regression_2';
        prop.has_invariant_kone = true;
        prop.has_polyhedral_minimal_invariant_kone = true;

    elseif( check(what, {21,'kone_regression_3'}) );  % Not known whether there exists an invariant kone
        M = cell( [1 2] );
        M{1} = [2.0173541408424427 0.95760797732744429  1.3523267099893601; -0.050722936942086044 0.46473694791759262 -0.69473438400381693;0.11268609468021666 0.84170973629966517 0.88730718124964847];
        M{2} = [2.381913423951219 -0.48092904338883258 -0.74082956686486512;-0.44159614754716103  0.31270006432429492  0.18962395942686608;1.2682772686338493 -1.8963527049550597 -0.96354454482558971];
        % smp = [1;2] ?
        prop.name = 'kone_regression_3';
        prop.has_invariant_kone = true;
        prop.has_polyhedral_minimal_invariant_kone = true;

    elseif( check(what, {22,'kone_regression_4'}) );  % Not known whether there exists an invariant kone
        M = cell( [1 2] );
        M{1} = [0.10750273614033061 -0.25365177053326615 1.6224045867994501 -1.4642019924659544;1.1617395438535958 0.063054180697335732 0.5781536400039593 0.6086546234642265;-0.14170178003373857 0.26284816701888875 1.370868696626818 0.17517173212927964;-0.47508642065051399 -1.0006955144092655 0.58628194449897231 2.3322857639847303];
        M{2} = [2.3809599616705213 0.86421550254267177 -2.471236873511105 -1.3988912501351669;-0.07802576450930665 -0.19397544045099907 2.6125408076513934 0.85486641096697968;0.77354114903169657 -0.5864256080042265 -1.2776306197357556 -0.038514190173399014;-1.01011866822404 -1.0645755530987786 1.4347237670403581 0.43179881324726049];
        prop.name = 'kone_regression_4';
        prop.has_invariant_kone = true;
        prop.has_polyhedral_minimal_invariant_kone = named( 'unlikely' );

    elseif( check(what, {23,'kone_regression_5'}) );
        M=cell([1 2]);
        M{1}=[1.577891385239691 -0.11128940048835978 -0.73548422169418526;-0.8917372947836224 -0.36888610050495352 0.32038333576177597;-0.40890387223135832 1.0781518440116757 -0.61725352607424955];
        M{2}=[0.84596229212909391 -0.098574961336713354 -1.3326806657276766;-0.40282002693231311 -0.20284326758999577 0.30783206008212527;-0.91780647408330962 0.22822871341669998 0.1983602984009262];
        %center = [0.84131228915286083;-0.31972967386739354;-0.41602350188576714];
        % smp = [1;2]
        prop.name = 'kone_regression_5';
        prop.has_invariant_kone = true;
        % interesting case  % Seems to have problems in invariant kone computation similar to multiple smp case, which there is solved via balancing

    elseif( check(what, {24,'kone_regression_6'}) );
        % Seems to have an invariant kone, but algorithm does not terminate. smp = [2]?
        % Seems to have nearly an invariant subspace
        M = cell([1 2]);
        M{1} = [0.31345658609944549 0.32326555740609564 0.33209895538688516;0.32326555740609564 0.33370826277425225 0.3431298999801804;0.33209895538688516 0.3431298999801804 0.353098998848979];
        M{2} = [1.1568712291769863 0.98511219924031401 0.58340899548044955;0.98511219924031401 0.85776155781116692 0.53625751101516894;0.58340899548044955 0.53625751101516894 0.39005723243283991];
        prop.name = 'kone_regression_6';
        prop.has_invariant_kone = true;

    elseif( check(what, {25,'kone_regression_7'}) );
        M = cell([1 2]);
        M{1} = [0.44621602133297344 0.10669681090878635 0.85205742848400545;0.44298438321734668 0.46149674592730205 0.77314543320821261;0.22226862854124507 0.63335544890349882 0.20774130232204036];
        M{2} = [0.82640885741320325 0.90950418798805954 0.052344265011731395;0.75827411727066518 0.0063542094993949139 0.32035606569683284;0.91381697036258613 0.98597869235414881 0.13611082564883881];
        prop.name = 'kone_regression_7';
        prop.has_invariant_kone = true;
        prop.info = 'Has an invariant kone, but only algorithm invariant_kone_Et terminates. No idea about smp.';

    elseif( check(what, {26,'kone_regression_8'}) );
        M = cell([1 2]);
        M{1} = [0.65153735213673147 0.93219694820674515 -0.23236443418247157;-0.42819849080296252 0.89924158166541612 -0.51980409765321256;-1.2596196378800066 -0.6421301436598893 0.34444967467578191];
        M{2} = [1.1999294755277068 0.54501703585035521 -0.90780624719789449;0.56823212954080793 0.15308249761181805 -0.66933402774829087;0.023361536184893721 -1.0076355295556276 -0.039111686750840743];
        prop.name = 'kone_regression_8';
        prop.has_invariant_kone = true;

    elseif( check(what, {27,'kone_regression_9'}) );
        % Algorithm invariant_kone_allproducts does not terminate
        M = cell([1 2]);
        M{1} = [-0.93302101038896457 0.11288748307412076 -0.31062210834712134;-0.017335436828963189 1.5805397976456774 1.1487714362276187;-1.4106161412892759 0.74529938769325366 1.2459304503514703];
        M{2} = [-0.63585087915118399 -0.52678274150459081 -0.63039873213699771;0.10242755050205413 -0.18697208817552971 0.7016890188292535;-0.31069715107472029 -0.6518530822536317 1.8045976739462126];
        prop.name = 'kone_regression_9';
        prop.has_invariant_kone = true;

    elseif( check(what, {28,'kone_regression_10'}) );
        M = cell([1 2]);
        M{1} = [0.56267122049487406 1 0;0.6537731365189362 0.56267122049487406 1;0.5286163689715393 0.6537731365189362 0.56267122049487406];
        M{2} = [4.7770770256820514 1 0;4.3473104930025812 4.7770770256820514 1;9.0642315056801479 4.3473104930025812 4.7770770256820514];
        prop.name = 'kone_regression_10';
        prop.has_invariant_kone = true;
        prop.info = 'Algorithm invariant_kone_allproducts does not terminate';

    elseif( check(what, {29,'kone_regression_11'}) );
        M = cell([1 2]);
        M{1} = [0.76542473454963167 0 0.23457526545036839;0 0.33317721935849431 0.66682278064150569;0.23457526545036839 0.66682278064150569 0.098601953908125956];
        M{2} = [0 0.684254150659384 0.31574584934061606;1 0 0;0 0.31574584934061606 0.684254150659384];
        prop.name = 'kone_regression_11';
        prop.irreducible = false;
        prop.has_invariant_kone = true;
        prop.has_polyhedral_minimal_invariant_kone = true;
        prop.info = 'Algorithm invariant_kone_allproducts and invariant_kone_Et both do not work well, although there should be an invariant kone';

    elseif( check(what, {30,'kone_regression_12'}) );
        M = {[1 1;0 1], 0.9*[1 0;1 1]};
        prop.name = 'kone_regression_12';
        prop.has_invariant_kone = true;


    elseif( check(what, {31,'kone_regression_13'}) );
        % invariant Kegel looks very Stanitzel like
        M = cell([1 3]);
        M{1} = [1.0034338290219751 -0.012013486674492174 -0.083807953606928942;-0.012013486674492174 1.0420300082370382 0.29320788176404944;-0.083807953606928942 0.29320788176404944 1.0454638372590139];
        M{2} = [1.0440187439364799 0.0079756625385561386 -0.30006457452294294;0.0079756625385561386 1.0014450933225336 -0.054368061697168971;-0.30006457452294294 -0.054368061697168971 1.0454638372590139];
        M{3} = [1.0110325929334349 -0.019490148866433799 0.1502224013805529;-0.019490148866433799 1.0344312443255792 -0.26538248838196171;0.1502224013805529 -0.26538248838196171 1.0454638372590139];
        prop.name = 'kone_regression_13';
        prop.has_invariant_kone = named( 'probably not' );
        prop.has_polyhedral_minimal_invariant_kone = named( 'unlikely' );

    elseif( check(what, {31,'kone_regression_14'}) );
        M = cell([1 2]);
        M{1} = [1 -0.099531114088745348 -0.2078778637353447;-0.099531114088745348 1 -0.099531114088745459;-0.2078778637353447 -0.099531114088745459 1];
        M{2} = [1 -0.4207578905186744 -0.4207578905186744;-0.4207578905186744 1 -0.58802012637794776;-0.4207578905186744 -0.58802012637794776 1];
        prop.name = 'kone_regression_14';
        prop.has_invariant_kone = true;
        prop.has_polyhedral_minimal_invariant_kone = true;
        prop.info = 'may have a polyhedral common invariant kone';

    elseif( check(what, {32,'kone_regression_15'}) );
        
        M = cell([1 2]);
        M{1} = [4 2 1;1 1 1;4 2 1];
        M{2} = [0.3844792600723852 0.62006391611864109 1;0.24742229997991119 -0.49741562096491421 1;1.1127142112949124 -1.0548526964912743 1];
        prop.name = 'kone_regression_15';
        prop.has_invariant_kone = true;
        prop.has_polyhedral_minimal_invariant_kone = true;
        prop.vandermonde = [true true];
        prop.info = 'smps of length 3, may have a polyhedral common invariant kone';

    elseif( check(what, {33,'kone_regression_16'}) );
        M = cell([1 2]);
        M{1} = [0 0 0;1 1 1;0 1 0];
        M{2} = [1 0 1;1 0 1;0 0 0];
        prop.name = 'kone_regression_16';
        prop.has_invariant_kone = true;
        prop.has_polyhedral_minimal_invariant_kone = true;

    elseif( check(what, {34,'kone_regression_17'}) );
        M = cell([1 2]);
        M{1} = [10 6 0;14 10 6;21 14 10];
        M{2} = [19 6 0;32 19 6;73 32 19];
        prop.name = 'kone_regression_17';
        prop.has_invariant_kone = true;
        prop.has_polyhedral_minimal_invariant_kone = named( 'seems like' );

    elseif( check(what, {35,'kone_regression_18'}) );
        M=cell([1 2]);
        M{1}=[5310 6 -441;6 5294 -151;-441 -151 5313];
        M{2}=[5312 -4 457;-4 5293 -94;457 -94 5313];
        prop.name = 'kone_regression_18';
        prop.has_invariant_kone = true;
        prop.info = 'Center vector is hard to compute.';

    elseif( check(what, {36,'kone_gm51'}) );
        M = {[7 0;2 3], [2 4;0 8]};  % Guglielmi, Maiale 2024, Section 5.1. Actually an example for the lower spectral radius
        prop.name = 'kone_gm51';
        prop.interesting_for = {'lsr'};
        
    elseif( check(what, {37,'kone_gm52'}) );
        M = {[5 1 0 0;0 5 2 0;0 0 3 1;0 0 0 2], [1 2 3 4;0 2 5 6;0 0 3 7;0 0 0 4]};  
        prop.name = 'kone_gm52';
        prop.interesting_for = {'lsr'};
        prop.info = 'Guglielmi, Maiale 2024, Section 5.2. Actually an example for the lower spectral radius';
        
    elseif( check(what, {37,'kone_gm61'}) );
        M = {[0 1 0 0 0;1 0 2 0 0;0 0 0 0 0;0 1 0 0 1;0 0 0 2 1], [1 0 2 0 0;0 0 0 2 1;1 1 0 0 0;0 0 0 0 0;0 1 0 0 0]}; % Guglielmi, Maiale 2024, Section 6.1. Actually an example for the lower spectral radius
        prop.name = 'kone_gm61';
        prop.interesting_for = {'lsr'};

    elseif( check(what, {37,'kone_gm81'}) );
        M = {1/5*[3 0;1 3], 1/5*[3 -3;0 -1]}; 
        prop.name = 'kone_gm81';
        prop.interesting_for = {'lsr'};
        prop.info = 'Guglielmi, Maiale 2024, Section 8.1. Actually an example for the lower spectral radius';

    elseif( check(what, {38,'kone_dim5_1'}) );
        M=cell([1 2]);
        M{1}=[1.0743276853102861 -1.2626123558574036 -0.96054403235513774 -0.95211724315704604 -0.3109374542027728;-1.0226052567661126 0.21676649362159994 -0.065822836349323732 0.99542426840634801 -0.64798755942762409;-0.59074946878071355 0.82692473003588085 1.9248382610143413 2.9947037242326235 -1.9867451675150667;-0.36107947948927444 -1.126429717251048 0.37471689122953528 0.086617842417120214 -0.55811112340846669;1.4553967671297972 -0.40562361279330228 -0.83622450431755613 -1.6207666907475498 -1.996855516824888];
        M{2}=[-0.68834606026899803 0.7365936884495351 0.045458520195084597 -0.58443351211380457 -0.7332510608334083;-1.9729229457688722 -0.54885569205486218 -1.068243797911415 -1.1060037086821368 -0.59495909615795506;-1.9713489035933431 -0.097261894239904514 -0.82721512345857662 0.50401032907165122 0.56362654679896429;1.1512398776027553 0.51336138058718561 1.5448843109227057 1.2154162593495705 -1.0538104593642896;-0.18398760231637482 0.85767460432634623 -1.3440264815643495 0.1707212303516856 0.53557926892149299];
        prop.name = 'kone_dim5_1';
        prop.info = 'most likely no invariant kone';

    elseif( check(what, {39,'kone_regression_19'}) );
        M = {[0 -1 -1;-1 0 -1;0 0 0],[1 -1 -1;-1 1 1;-1 0 -1]};
        prop.name = 'kone_regression_19';
        prop.has_invariant_kone = true;
        prop.info = 'leading eigenvectors seem to be inside the extremal faces of the minimal invariant kone';
    
    elseif( check(what, {40,'kone_regression_20'}) );
        M={[1 0 -1;1 0 0;0 0 1],[1 0 0;1 1 -1;0 -1 1]};
        prop.name = 'kone_regression_20';
        prop.has_polyhedral_minimal_invariant_kone = true;
        prop.info = 'minimal invariant kone consists of images of leading eigenvectors M{1} AND M{2}';

    elseif( check(what, {41,'kone_regression_21'}) );
        M={[0 1;1 0],[0 1;1 1]};
        prop.name = 'kone_regression_21';

    elseif( check(what, {43,'kone_multiple_1'}) );
        M=cell([1 2]);
        M{1}=[0 0 0;0 0 0;0 0 1];
        M{2}=[1 1 0;0 1 1;1 0 1];
        prop.name = 'kone_multiple_1';
        prop.info = 'Exact algorithm terminates with start M{1} and/or M{2}';
        
    end;
end

function [ M, G_, prop ] = getmatrix_properties( filter, opt );
    maybe_unused( filter );
    if( check(opt.what, {'propteries','property'}) );
        opt.what = []; end;
    M = {};
    G_ = [];
    prop = struct;
    what = opt.what;
    if( isempty(what) );
        what = randi( [1 3] ); end;
    % mortality matrices
    if( false )
    elseif( check(what, {'mortal','mortal1','mortal2','mortality','mortality1','mortality2'}) );
        % https://math.stackexchange.com/questions/852424/mortality-problem
        error( 'gallery_matrixset:mortal', 'The matrix ''mortal'' was removed. Use ''mortal1'' or ''mortal2'' instead' );
    elseif( check(what, {1,'mortal1','mortality1'}) );
        prop.name = 'mortal1';
        prop.mortal = true;
        M{1} = [ 0  1 -1; 0  1 -1; 1  0  1];
        M{2} = [-1  0  1;-1 -1  0;-1  1  1];
        % sminp = [1 2 2 1 1 1 2 2 2 2 1 1 2 1 2 2 1]

    elseif( check(what, {2,'mortal2','mortality2'}) );
        prop.name = 'mortal2';
        prop.mortal = true;
        M{1} = [ 1 -1 0;-1 0 -1;0 0 1];
        M{2} = [1 -1 -1;1 0 0;-1 -1 -1];
        % sminp = [2 1 1 2 1 2 1 2 2 1 1 1 1 1 2 1 1 2]

    elseif( check(what, {3,'multiplesmp1','multismp1','daubechies5','daub5'}) );
        prop.name = 'daubechies5';
        prop.smp = {1 2};
        M=cell([1 2]);
        M{1}=[0.22641898258355836 0 0 0;0.15089396948743078 -0.27815137021276348 0.22641898258355836 0;0.0047174279390678718 -0.041379009797293552 0.15089396948743078 -0.27815137021276348;0 0 0.0047174279390678718 -0.041379009797293552];
        M{2}=[-0.27815137021276348 0.22641898258355836 0 0;-0.041379009797293552 0.15089396948743078 -0.27815137021276348 0.22641898258355836;0 0.0047174279390678718 -0.041379009797293552 0.15089396948743078;0 0 0 0.0047174279390678718];

    elseif( check(what, {4,'multiplesmp2','multismp2','daubechies6','daub6'}) );
        prop.name = 'daubechies6';
        prop.smp = {1 2};
        M=cell([1 2]);
        M{1}=[0.15774243200290142 0 0 0 0;0.17783194749125375 -0.24695077794217285 0.15774243200290142 0 0;0.015897265196542915 -0.071747332942922737 0.17783194749125375 -0.24695077794217285 0.15774243200290142;0 -0.0015235338056025065 0.015897265196542915 -0.071747332942922737 0.17783194749125375;0 0 0 -0.0015235338056025065 0.015897265196542915];
        M{2}=[-0.24695077794217285 0.15774243200290142 0 0 0;-0.071747332942922737 0.17783194749125375 -0.24695077794217285 0.15774243200290142 0;-0.0015235338056025065 0.015897265196542915 -0.071747332942922737 0.17783194749125375 -0.24695077794217285;0 0 -0.0015235338056025065 0.015897265196542915 -0.071747332942922737;0 0 0 0 -0.0015235338056025065];

    elseif( check(what, {4,'multiplesmp3','multismp3','daubechies7','daub7'}) );
        prop.name = 'daubechies7';
        prop.smp = {1 2};
        M=cell([1 2]);
        M{1}=[0.1100994307456237 0 0 0 0 0;0.18839356713498501 -0.20990473159384077 0.1100994307456237 0 0 0;0.032449199910143486 -0.099863200359989107 0.18839356713498501 -0.20990473159384077 0.1100994307456237 0;0.00050022685312249061 -0.0060494926900448078 0.032449199910143486 -0.099863200359989107 0.18839356713498501 -0.20990473159384077;0 0 0.00050022685312249061 -0.0060494926900448078 0.032449199910143486 -0.099863200359989107;0 0 0 0 0.00050022685312249061 -0.0060494926900448078];
        M{2}=[-0.20990473159384077 0.1100994307456237 0 0 0 0;-0.099863200359989107 0.18839356713498501 -0.20990473159384077 0.1100994307456237 0 0;-0.0060494926900448078 0.032449199910143486 -0.099863200359989107 0.18839356713498501 -0.20990473159384077 0.1100994307456237;0 0.00050022685312249061 -0.0060494926900448078 0.032449199910143486 -0.099863200359989107 0.18839356713498501;0 0 0 0.00050022685312249061 -0.0060494926900448078 0.032449199910143486;0 0 0 0 0 0.00050022685312249061];

    elseif( check(what, {4,'multiplesmp4','multismp4','daubechies8','daub8'}) );
        prop.name = 'daubechies8';
        prop.smp = {1 2};
        M=cell([1 2]);
        M{1}=[0.076955622108152474 0 0 0 0 0 0;0.18615056910323771 -0.17317772971296994 0.076955622108152474 0 0 0 0;0.05186905810634674 -0.12192642649688989 0.18615056910323771 -0.17317772971296994 0.076955622108152474 0 0;0.0022843278022850553 -0.014176783648788912 0.05186905810634674 -0.12192642649688989 0.18615056910323771 -0.17317772971296994 0.076955622108152474;0 -0.00016613726137322538 0.0022843278022850553 -0.014176783648788912 0.05186905810634674 -0.12192642649688989 0.18615056910323771;0 0 0 -0.00016613726137322538 0.0022843278022850553 -0.014176783648788912 0.05186905810634674;0 0 0 0 0 -0.00016613726137322538 0.0022843278022850553];
        M{2}=[-0.17317772971296994 0.076955622108152474 0 0 0 0 0;-0.12192642649688989 0.18615056910323771 -0.17317772971296994 0.076955622108152474 0 0 0;-0.014176783648788912 0.05186905810634674 -0.12192642649688989 0.18615056910323771 -0.17317772971296994 0.076955622108152474 0;-0.00016613726137322538 0.0022843278022850553 -0.014176783648788912 0.05186905810634674 -0.12192642649688989 0.18615056910323771 -0.17317772971296994;0 0 -0.00016613726137322538 0.0022843278022850553 -0.014176783648788912 0.05186905810634674 -0.12192642649688989;0 0 0 0 -0.00016613726137322538 0.0022843278022850553 -0.014176783648788912;0 0 0 0 0 0 -0.00016613726137322538];

        end;
                    
end

function [ M, G_, prop ] = getmatrix_singleton( filter, opt );
    % matrix sets of size 1
    maybe_unused( filter );
    if( check(opt.what, {'singleton'}) );
        opt.what = []; end;
    M = {};
    G_ = [];
    prop = struct;
    what = opt.what;
    if( isempty(what) );
        what = randi( 16 ); end;
    idx = what == '_';
    what(idx) = [];
    
    
    if( false );
    elseif( check(what, {1,'wilson'}) );
        prop.name = 'wilson';
        switch randi( [-1 3] );
            case -1; M = {[5 7 6 5;7 10 8 7;6 8 10 9;5 7 9 10]};  % this is the true Wilson matrix
            case  0; M = {[10 1 4 0;1 10 5 -1;4 5 10 7;0 -1 7 9]};
            case  1; M = {[1 3 10 10;3 4 8 9;10 8 3 9;10 9 9 3]};
            case  2; M = {[2 7 10 10;7 10 10 9;10 10 10 1;10 9 1 9]};
            case  3; M = {[9 1 1 5;1 10 1 9;1 1 10 1;5 9 1 10]};
            otherwise; fatal_error; end;

    

    end;
end

function [ M, G_, prop ] = getmatrix_misc( filter, opt );
    maybe_unused( filter );
    if( check(opt.what, {'misc'}) );
        opt.what = []; end;
    M = {};
    G_ = [];
    prop = struct;
    what = opt.what;
    if( isempty(what) );
        what = randi( 16 ); end;
    idx = what == '_';
    what(idx) = [];
    
    
    if( false );
    elseif( check(what, {1,'nearlycomplex'}) );
        prop.name = 'nearlycomplex';
        M = nearlycomplex();

    elseif( check(what, {2,'mejstrik'}) );
        prop.name = 'mejstrik';
        M{1} = [-0.0766   -0.7779   -0.4523; 0.5958    0.7105    0.7678; -0.8199    0.6780   -0.5047];
        M{2} = [0.2923    0.7024    0.6463; 0.7111   -0.1468   -0.1636; -0.0213   -0.2592    0.2707];
        M{3} = [0.6170   -0.2134   -0.3799; -0.7410   -0.4941    0.6517; 0.3961   -0.7559    0.5435]; 

    elseif( check(what,  {3,'mejstrik2'}) );  % Example from my thesis
        prop.name = 'mejstrik2';
        M{1} = [2 -1; 2 0]; 
        M{2} = [0 -1; 2 2];    

    elseif( check(what,  {4,'mejstrik3'}) );  % program works
        prop.name = 'mejstrik3';
        M{1} = [1 -1; 3 -2]; 
        M{2} = [1 3; -1 -1]; 

    elseif( check(what,  {5,'mejstrik4'}) );  % program works
        prop.name = 'mejstrik4';
        M{1} = [1 4; 4 1]; 
        M{2} = [1 1; 0 -1]; 
        M{3} = [1 0; 1 -1];             

    elseif( check(what,  {6,'mejstrik5'}) );  % _very_ long computation duration
        prop.name = 'mejstrik5';
        a = ((ones(1,15)*-1).^(1:15).*floor(sqrt(1:15)))'; 
        a(10) = -2;    
        %a(10) = -2.11    
        %a(10) = -2.237884497219498; (smallest?? value for which the matrix has real eigenvector
        M = transitionmatrix( getS({a,2},'nocheck'), 'Omega',0:13 );

    elseif( check(what,  {7,'mejstrik8'}) );  % very long smp
        prop.name = 'mejstrik8';
        M{1} = [1 1 -1 1;1 0 1 0;-1 0 1 0;-1 -1 0 0];
        M{1} = M{1}/max(abs(eig(M{1})));
        M{2} = [1 1 0 0;1 1 -1 0;-1 1 0 -1; 1 -1 0 0];
        M{2} = M{2}/max(abs(eig(M{2})));

    elseif( check(what,  {8,'mejstrik9',}) );  % same eigenvectors, every product is smp, Guglielmi Maiale 2024
        prop.name = 'mejstrik9';
        prop.interesting_for = {'lsr'};
        M{1} = [1 1; 0 2];
        M{2} = [2 0; 1 1];

    elseif( check(what,  {9,'mejstrik10'}) );
        % both matrices have two leading eigenvectors. 
        % feta/ipa both do not converge. 
        % At{1}^n*At{2} is the same matrix for all even n
        prop.name = 'mejstrik10';
        M{1} = [2 -1;-3 -2];
        M{2} = [3 -1;3 -3];

        % same behaviour
        % A{1} = 1/sqrt(6)*[2 -2;0 -1];
        % A{2} = 1/sqrt(6)*[2 -2;-1 -2];            
    elseif( check(what,  {10,'lotsmps'}) );  % a huge amount of smp's %not working
        prop.name = 'lotsmps';
        M = matrix_3(1, -2, 2 );

    elseif( check(what,  {11,'psp','pseudospline'}) );  % diffscheme for pseudospline(2,3,2)  % program works
        prop.name = 'pseudospline';
        a = 1/256*[3 -18 38 -18 3]';
        M = transitionmatrix( {a,2,[0 1]}, 'Omega',0:3 );
        M = cellfun( @transpose, M, 'uniformoutput',0 );

    elseif( check(what,  {12,'mejstrik945','mej945','945'}) );  % smp length of 945?  oo = [repmat( 1,[1 940] ) repmat( 2, [1 5] )];
        prop.name = 'mejstrik945';
        M{1} = 1/8 * [8  -1   0;0   8   1;0   0   2];
        M{2} = 1/8 * [1   0   0;0   2   0;1   1   8];

    elseif( check(what,  {13,'laskawiec1'}) );
        % two smps of length 6!
        prop.name = 'laskawiec1';
        M{1} = [1011/391, -(1547/205); 612/299, -(607/271)];
        M{2} = [-(616/293), 315/347; -1, -(616/293)];

    elseif( check(what,  {14,'laskawiec2'}) );
         % two smps of length 6!
         prop.name = 'laskawiec2';
         M{1} = [299/126, -(1119/241);181/443, 517/511];
         M{2} = [-(169/400), 5593/962;-1, -(169/400)];
    
    elseif( check(what,   {15,'new1'}) );  % interesting for new features of algorithm, both matrices are smps, three different leading eigenvectors
        prop.name = 'new1';
        M = {[1 0 ;0 -1], 1/sqrt(2)*[1 1;1 -1]}; 
        
    elseif( check(what,   {16,'eigenvalueproblem'}) );
        % Hard to compute the exact number of leading eigenvectors. It is 17
        prop.name = 'eigenvalueproblem';
        M = {[0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 1 0 0 0 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 1 0 0 0 0 0 0 0 0 5 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 0 0 0 0 0 0 1 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 5 0 0 0 0 0 0 0 0 0 2 0 0 0 0 2 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 6 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 1 0 0 0 0;0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]};
        
        end;
end

function [ M, G, prop ] = getmatrix_continuous( filter, opt );  %#ok<INUSD>
    M = {};
    G = [];
    prop = [];
    what = opt.what;
    if( isempty(what) );
        what = randi( 2 ); end;
    idx = what == '_';
    what(idx) = [];
    
    
    if( false );
    elseif( check(what, {1,2,'kp4','kp5'}) );
        prop.name = 'kp4';
        A{1} = [-.3 .5;.2 -.4];
        A{2} = [-.6 0;0 1];
        if( check(what, {1,'kp4'}) );
            m_ = 1;
            M_ = 2;
        else;
            m_ = 1;
            M_ = 2.5; end;
        if( ~isempty(opt.arg) );
            N = opt.arg{1};
        else;
            N = 10; end;
        len = linspace( m_, M_, N );
        for j = 1:2;
            for l = len;
               M{end+1} = expm( l * A{j} ); end; end;
        G = [zeros( N ) ones( N ); ones( N ) zeros( N )];
    end
end

function [ M, G_, prop ] = getmatrix_signal( filter, opt );
    N = condget( filter, 'N' );
    d = condget(filter, 'dim');
    if( check(opt.what, {'signal'}) );
        opt.what = []; end;
    M = {};
    G_ = [];
    prop = struct;
    what = opt.what;
    if( isempty(what) );
        what = randi( [1 12] ); end;
    idx = what == '_';
    what(idx) = [];
    
    
    if( false );
    elseif( check(what, {1,'signal_orthogonal','s_orthogonal','sorthogonal'}) );
        prop.name = 'signal_orthogonal';
        for i = 1:N; 
            M{i} = randomOrthogonal( d ); end;
        prop.random = true( size(M) );
        
    elseif( check(what, {2,'signal_hadamard','s_hadamard','shadamard'}) );
        prop.name = 'signal_hadamard';
        d_test = 2.^round( log2([d d/12 d/20]) );
        [~,idx] = min( abs(d - d_test) );
        d = d_test( idx );
        for i = 1:N;
            M{i} = hadamard( d )/sqrt( d ); end;
    
    elseif( check(what, {3,'signal_circulant','s_circulant','scirculant'}) );
        prop.name = 'signal_circulant';
        for i = 1:N; 
            R = fft( randn(d, 1) );
            R = R./abs( R );
            r = ifft( R );
            M{i} = circulant( r, randSgn(1) ); end;
        prop.random = true( size(M) );
        
    elseif( check(what, {4,'signal_householder','s_householder','shouseholder'}) );
        prop.name = 'signal_householder';
        for i = 1:N; 
            M{i} = householderMatrix( rand(d, 1) ); end;
        prop.random = true( size(M) );
        
    elseif( check(what, {5,'signal_parallel','s_parallel','sparallel'}) );
        prop.name = 'signal_parallel';
        for i = 1:N; 
            M{i} = eye( d ); end;
        
    elseif( check(what, {6,'signal_series','s_series','sseries'}) );
        prop.name = 'signal_series';
        for i = 1:N; 
            M{i} = tril( randn(d), -1 )/d + eye( d ); end;
        prop.random = true( size(M) );
        
    elseif( check(what, {7,'signal_diagonalConjugated','s_diagonalConjugated','sdiagonalConjugated'}) );
        prop.name = 'signal_diagonalConjugated';
        for i = 1:N;
            D = diag( randn(d, 1) );
            M{i} = D\randomOrthogonal( d )*D; end;
        prop.random = true( size(M) );
        
    elseif( check(what, {8,'signal_tinyRotation','s_tinyRotation','stinyRotation'}) );
        prop.name = 'signal_tinyRotation';
        for i = 1:N;
            M{i} = tinyRotationMatrix( d, 0.01 ); end;
        
    elseif( check(what, {9,'signal_allpassInFDN','s_allpassInFDN','sallpassInFDN'}) );
        prop.name = 'signal_allpassInFDN';
        for i = 1:N;
            dim = round(d/2);
            g = rand( 1, dim )*0.2 + 0.6;
            A = randomOrthogonal( dim );
            M{i} = allpassInFDN( g, A, 0*g, 0*g, 0 ); end;
        prop.random = true( size(M) );
        
    elseif( check(what, {10,'signal_nestedAllpass','s_nestedAllpass','snestedAllpass'}) );
        prop.name = 'signal_nestedAllpass';
        for i = 1:N;
            g = rand( 1, d )*0.2 + 0.6;
            M{i} = nestedAllpass( g ); end;
        prop.random = true( size(M) );
        
    elseif( check(what, {11,'signal_SchroederReverberator','s_SchroederReverberator','sSchroederReverberator','signal_schroeder','s_schroeder','sschroeder'}) );
        prop.name = 'signal_SchroederReverberator';
        for i = 1:N;
            dim = round( d/2 );
            allpassGain = rand( 1, dim )*0.2 + 0.6;
            combGain = ones( 1, dim );
            M{i} = SchroederReverberator( allpassGain, combGain, ones(dim, 1), ones(1, dim), 1 ); end;
        prop.random = true( size(M) );
        
    elseif( check(what, {12,'signal_AndersonMatrix','s_AndersonMatrix','sAndersonMatrix','signal_Anderson','s_Anderson','sAnderson'}) );
        prop.name = 'signal_AndersonMatrix';
        for i = 1:N;
            M{i} = AndersonMatrix( d ); end;

    elseif( check(what, {13,'signal_designfilt_0','signal_designfilt'}) );
        prop.name = 'signal_designfilt_0';
        for i = 1:N;
            switch randi( [1 8] );
            case 1; df = designfilt( 'bandpassiir', 'FilterOrder',randi([5 40]), 'HalfPowerFrequency1',400, 'HalfPowerFrequency2',450, 'SampleRate',450 + 450*rand() );
            case 2; df = designfilt( 'bandpassiir', 'FilterOrder',randi([4 12]), 'HalfPowerFrequency1',0.179620342112772, 'HalfPowerFrequency2',0.28807127207179, 'SampleRate',1, 'DesignMethod','butter' );
            case 3; df = designfilt( 'highpassiir', 'FilterOrder',randi([3 14]), 'HalfPowerFrequency',rand(), 'SampleRate',1, 'DesignMethod','butter' );
            case 4; df = designfilt( 'highpassiir', 'FilterOrder',randi([3 12]), 'StopbandFrequency',rand(), 'StopbandAttenuation',randi([1 120]), 'SampleRate',1, 'DesignMethod','cheby2' );
            otherwise; fatal_error;
            end;
            M{i} = ss( df ); end;
        prop.random = true( size(M) );

    elseif( check(what, {14,'signal_designfilt_1'}) );
        prop.name = 'signal_designfilt_1';
        for i = 1:N;
            f0 = rand/2;
            f1 = f0 + (1/2 - f0)*rand;
            d = designfilt( rand_oneof('lowpassiir','highpassiir'), 'PassbandFrequency',f0, 'StopbandFrequency',f1, 'PassbandRipple', randi( [1 5] ), 'StopbandAttenuation', randi( [1 100] ), 'DesignMethod',rand_oneof( 'butter','cheby1','cheby2','ellip' ), 'MatchExactly','passband', 'SampleRate',1 );
            M{i} = ss( d ); end;
        prop.random = true( size(M) );

    elseif( check(what, {15,'signal_designfilt_2'}) );
        prop.name = 'signal_designfilt_2';
        for i = 1:N;
            pbf1 = rand/4;
            sbf1 = pbf1 + (1/4 - pbf1)*rand;
            sbf2 = sbf1 + (1/3 - sbf1)*rand;
            pbf2 = sbf2 + (1/2 - sbf2)*rand;
            d = designfilt('bandstopiir','PassbandFrequency1',pbf1,'StopbandFrequency1',sbf1,'StopbandFrequency2',sbf2,'PassbandFrequency2',pbf2,'PassbandRipple1',1,'StopbandAttenuation',randi([1 50]),'PassbandRipple2',1,'DesignMethod',rand_oneof( 'butter','cheby1','cheby2','ellip' ),'SampleRate',1);
            M{i} = ss( d ); end;
        prop.random = true( size(M) );

    elseif( check(what, {16,'signal_designfilt_3'}) );
        prop.name = 'signal_designfilt_3';
        for i = 1:N;
            b_r = randi( [1 20] );
            b_c = randi( [1 20]);
            a_c = randi( [1 b_c] );
            b = randn( b_r, b_c );
            a = randn( 1, a_c );
            if( a_c < b_c );
                a(end+1:b_c) = 0; end;            
            M{i} = tf2ss( b, a ); end;
        prop.random = true( size(M) );
    end
end

function [ M, G, prop_ ] = getmatrix_graph( M, filter, opt );
    maybe_unused( filter );
    prop_ = [];
    if( isempty(opt.graph) );
        G = [];
        return; end;
    
    randflag =  isempty(opt.what);
    
    what = opt.graph;
    if( isempty(opt.graph) );
        what = randi( 1 ); end;
    try;
        idx = what == '_';
        what(idx) = []; 
    catch me;  %#ok<NASGU>
        end;
    
    try;
        J = numel( M );
        if( ischar(what) || isstring(what) );
            % do nothing
            n = min( J, 2 );
        elseif( iscell(what) );
            n = what{2};
            what = what{1};
        else;
            what = 'default';
            n = min( J, 2 ); end;
        
        if( false );
        elseif( isanyof( what, {'block','biblock'} ) );            
            G = graph_block( n, J );

        elseif( isequal( what, 'default' ) || ...
                isanyof( what, {'cyclic','cycle','bicyclic','bicycle'} ) ...
              );
            G = graph_cycle( n, J );

        elseif( isanyof( what, {'cyclic_unbd','cycle_unbd','bicyclic_unbd','bicycle_unbd'} ) );
            G = graph_cycle_unbd( n, J );
            
        elseif( isanyof( what, {'rand','random','birand','birandom'} ) );
            G = graph_random( n, J );
            
        elseif( isanyof( what, {'perm','permute','permutation','biperm','bipermute','bipermutation'} ) );
            G = graph_permute( n, J );
            end;
            
     if( numel(what) >= 2 && isequal(what(1:2), 'bi') );
        G = ( G + G' ) ~= 0; end;
    catch me;
        if( ~randflag );
            rethrow( me ); end; end;
end

%% generating functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ T ] = matrix_jordan( r, c );
    d = max( r, c );
    T = [[zeros(d-1,1) eye(d-1) ];
          zeros(1,d)];
    T = T(1:r,1:c); 
end

function [ T ] = matrix_3( c0, c1, c2 );
    T{1} = [c0 0; c2 c1];
    T{2} = [c1 c0; 0 c2];
end

function [ T ] = matrix_euler2( varargin );
    % Euler binary partition function
    % length of smp == 2 if dim has even number of ones in binary expansion
    % length of smp == 1 if dim has odd number of ones in binary expansion
    if( numel(varargin) == 0 || isempty(varargin{1}) );
        r = 7;
    elseif( numel(varargin) == 1 );
        r = varargin{1}; 
    else
        error( 'gallery_matrixset:euler2', 'Wrong options given.' ); end;
    r = r - 1;
    T{1} = zeros( r, r );
    T{2} = T{1};
    for i = 1:r
        for j = 1:r
            T{1}(i,j) = tif( 1<= 2*j-i && 2*j-i <= r+1, 1, 0 );
            T{2}(i,j) = tif( 0<= 2*j-i && 2*j-i <= r,   1, 0 ); end; end;
end

function [ TU ] = matrix_TU( filter, opt );
    [ssdim, opt.arg] = parsem( {'ssdim','subdivisiondimension','subdivdim'}, opt.arg, 1 );
    N = condget( filter, 'N' );
    [length, opt.arg] = parsem( {'length','len'}, opt.arg, [] );
    assert( N > 0, 'gallery_matrixset:subdivision', 'number of matrices ''J'' must be at least 2.' );
    try;
        while( true );
            % make matrix M
            switch ssdim;
                case 1;
                    M = rand_oneof( -1, 1 )*N;
                otherwise;
                    while( true );
                        M = randi( [-4 4], ssdim );
                        val = abs( eig(M) );
                        if( min(val) <= 1 );
                            continue; end;
                        if( abs(det(M)) ~= N );
                            continue; end;
                        break; end; end;

            % make sequence
            if( isempty(length) );
                length = randi( [N+1 N+1+2*ssdim+5] ); end;
            supp = [length*ones(1, ssdim) 1];
            a = randi( [-5 5], supp );
            a = sequence( a );
            S = getS( 'a',a, 'M',M, 'verbose',-1 );
            S = normalizeS( S, 'verbose',-1, 'gauss' );         
            if( ~isempty(S) ); 
                break; end; end;
        
        % restrict
        T = transitionmatrix( S );
        TU = [];
        for k = 0:length^ssdim;
            U = constructU( T, k, 'verbose',-1 );
            [TU_new, TT, ~, ~, ~, flag] = restrictmatrix( T, U, 'verbose',-1 );
            maybe_unused( TT );
            if( ~flag );
                break;
            else;
                TU = TU_new; end; end;
        
    catch me;
        disp( me );
        fprintf( 2, 'Error while generating transition matrix.\n' );
        TU = []; end;
end

function [ TV ] = matrix_TV( filter, opt );
    N = condget( filter, 'N', rand_oneof( -1, 1 )*randi( [2 4] ) );
    [ss.dim, opt.arg]      = parsem( {'ssdim','subdivisiondimension','subdivdim'}, opt.arg, 1 );
    [ss.pos, opt.arg]      = parsem( {'sspos','subdivisionpositive','subdivnonnegative'}, opt.arg );
    [ss.M, opt.arg]        = parsem( {'ssM','ssdilation','dilationmatrix','dilation'}, opt.arg, [] );
    [ss.length, opt.arg]   = parsem( {'length','len'}, opt.arg, [] );
    assert( abs(N) > 0, 'gallery_matrixset:subdivision', 'number of matrices ''J'' must be at least 2.' );
    try;
        while( true );
            % make dilation matrix
            if( isempty(ss.M) );
                if( ss.dim == 1 );
                    ss.M = N;
                else;
                    while( true );
                        M = randi( [-4 4], ss.dim );
                        val = abs( eig(M) );
                        if( min(val) <= 1 );
                            continue; end;
                        if( det( M ) ~= N );
                            continue; end;
                        break; end; end; end;

            % make sequence
            if( isempty(ss.length) );
                l = randi( [N+1 N+1+2*ss.dim+5] );
            else;
                l = ss.length; end;
            supp = [l*ones(1, ss.dim) 1];
            if( ss.pos );
                a = randi( [0 5], supp );
            else;
                a = randi( [-5 5], supp ); end;
            a = sequence( a );
            S = getS( 'a',a, 'M',ss.M, 'verbose',-1 );
            S = normalizeS( S, 'verbose',-1, 'gauss' );         
            if( ~isempty(S) ); 
                break; end; end;
        
        % restrict
        [T, Om] = transitionmatrix( S );
        TV = [];
        for k = 0:ss.length^ss.dim;
            V = constructVt( Om, k, 'verbose',-1 );
            [TV_new, TT, ~, ~, ~, flag] = restrictmatrix( T, V, 'verbose',-1 );
            maybe_unused( TT );
            if( ~flag );
                break;
            else;
                TV = TV_new; end; end;
        
    catch me;
        disp( me );
        fprintf( 2, 'Error while generating transition matrix.\n' );
        TV = []; end;         
end

function [ T ] = matrix_stochastic( dim, N );
    T = cell( 1, N );
    for i = 1:N
        T{i} = normalizematrix( rand( dim ), 'colsum' ); end;
end

function [ T ] = matrix_doublestochastic( dim, N );
    T = cell(1,N);
    for i = 1:N
        T{i} = zeros( dim, dim );
        c = rand( 1, dim );
        c = c/sum( c );
        for k = 1:dim
            [~,idx] = sort( randperm(dim) ); 
            idx = idx + (0:dim-1)*dim;     
            T{i}(idx) = T{i}(idx) + c(k); end; end;
end

function [ T ] = matrix_stochastic_neg( dim, N );
    T = cell( 1, N );
    for i = 1:N
        T{i} = normalizematrix( rand( dim )-.5, 'colsum' ); end;
end

function [ T ] = matrix_doublestochastic_neg( dim, N );
    T = cell( 1, N );
    for i = 1:N
        T{i} = zeros( dim, dim );
        c = rand( 1, dim ) - .5;
        c = c/sum( c );
        for k = 1:dim
            [~,idx] = sort( randperm(dim) ); 
            idx = idx + (0:dim-1)*dim;     
            T{i}(idx) = T{i}(idx) + c(k); end; end;
end

function [ T ] = matrix_stochastic_pm( filter, opt );
    maybe_unused( opt );
    d = condget( filter, 'dim' );
    T = cell( 1, condget( filter, 'N' ) );
    for i = 1:numel( T );
        T{i} = rand( [d d] )*2 - 1;
        nrm = sum( abs( T{i} ), 1 );
        T{i} = T{i}./nrm; end;
end

function [ val ] = matrix_unitary( dim, J );
    % this function generates a random unitary matrix of order 'n' and verifies
    val = cell( 1, J );
    for j = 1:J
       X = randn( dim ); % generate a random matrix 
       [Q,R] = qr( X ); % factorize the matrix
       R = diag( diag(R)./abs(diag(R)) );
       val{j} = Q*R; end; % unitary matrix         
end



function [ T ] = matrix_zerospectralradius( dim, J, verbose );

    if( dim == 0 );
        if( verbose >= 0 );
            warning( 'gallery_matrixset:zero', 'Empty matrices with zero spectral radius are not defined. I return the empty matrix.' ); end;
        T = repcellm( [], [1 J] );
        return; 
    elseif( dim == 1 )
        T = repcellm( 0, [1 J] );
        return; end;

    T = cell( 1, J );   
    for j = 1:J
        %method = randi( [1 3] );
        method = 6;
        switch method;
            case 1 ;
                % generates nilpotent rank 1 matrices
                v = randn( dim, 1 );
                w = randn( dim, 1 );
                w(1) = 0;
                v(1) = 1;
                w(1) = -v'*w;
                T{j} = v*w';
            case 2;
                N = 3;
                while( true );
                    A = randi( N, dim ) - floor( N/2 );
                    while( rhot(A) ~= 0 );
                        idx = find( A );
                        n = max( min( ceil(numel(A)/9), ceil(numel(idx)/9)), 1 );
                        A( idx(randperm( numel(idx), n )) ) = 0; end;
                    if( anym(A) ~= 0 );
                        T{j} = A; end; end;

            case 3;
                % This is slower, and does not have better numerical properties
                jordan = [[zeros(dim-1,1) eye(dim-1) ];zeros(1,dim)];
                U = matrix_unitary( dim, 1 );
                U = U{1};
                T{j} = U*jordan*U';
                
            case 4;
                % this seems to be the best method
                blocks = randi( [1 dim] );
                len = rand( [1 blocks] );
                len = round( len/sum(len)*dim );
                
                while( sum(len)<dim );
                    idx = find( len, 1 );
                    len(idx) = len(idx) + 1; end;
                while( sum(len)>dim );
                    idx = find( len, 1 );
                    len(idx) = len(idx) - 1; end;
                
                jordan = cell( 1, blocks );
                for b = 1:blocks;
                    jordan{b} = [[zeros(len(b)-1,1) eye(len(b)-1) ];zeros(1,len(b))]; end;
                jordan = blkdiag( jordan{:} );
                
                range_ub = 1;
                range_lb = 0;
                while( true );
                    B = randi( [-round(range_lb) round(range_ub)], [dim dim] );
                    if( rank(B ) == dim );
                        break; 
                    else;
                        range_ub = range_ub * 1.1; 
                        range_lb = range_lb * 1.1; end; end;
                if( dim<10 );
                    T{j} = round(adjoint(B))*jordan*B;
                else;
                    T{j} = B\jordan*B; end;
                    
            case 5;
                    B = rand( dim );
                    T{j} = B\triu( rand(dim), 1 )*B;
                
            case 6;
                % generates adjancy matrix of an acyclic graph
                T{j} = triu( rand(dim)<rand, 1 ) .* randn( [dim dim] );
                p = randperm( dim );
                T{j} = T{j}(p,p);
                %B = randn( dim );
                %T{j} = B\T{j}*B;
                

            otherwise;
                fatal_error; end; end;




end

function [ T ] = matrix_hurwitz( dim, J, ~ );
    T = cell( 1, J );
    for j = 1:J;
        X = randn( dim );
        [v,d] = eig( X );
        d2 = diag( d );
        idx = real(d2) > 0;
        d2(idx) = -d2(idx);
        T{j} = real( v  * diag( d2 ) / v );
        
        
%         D = diag( -rand(1, dim) * rand );
%         while( true );
%             V = randn( dim );  % random invertible matrix
%             if( cond(V)<dim^2 );
%                 break; end; end;
%         T{j} = V*D/V; 
        end;
end

function [ T ] = matrix_circulant( dim, J, ~ );
    T = cell( 1, J );
    for j = 1:J;
        v = randn( 1, dim );
        if( dim == 1 );
            T{j} = v;
        else;
            T{j} = gallery( 'circul', v ); end; end;
end

function [ T ] = matrix_toeplitz( filter, opt );
    N = condget( filter, 'N' );
    dim = condget( filter, 'dim' );
    T = cell( 1, N );

    for j = 1:N;
        if( ~isempty( opt.arg ) && numel( opt.arg ) == 1 || ...
            ~isempty( opt.arg ) && numel( opt.arg ) == 2 && isequal( opt.arg{1}(1), opt.arg{2}(1) ) ...
          )
            T{j} = toeplitz( opt.arg{:} );
        elseif( isempty(opt.arg) && ~isempty(dim) );
            r = randn( 1, dim );
            c = randn( 1, dim );
            c(1) = r(1);
            T{j} = toeplitz( r, c );
        else;
            fatal_error; end; end;
end

function [ T ] = matrix_hankel( dim, J, ~ );
    T = cell( 1, J );
    for j = 1:J;
        switch randi( [0 1] );
            case 0;
                r = randn( 1, dim );
                c = randn( 1, dim );
                c(1) = r(end);
                T{j} = hankel( r, c );
            case 1;
                r = randn( 1, dim );
                T{j} = hankel( r );
            otherwise;
                fatal_error; end; end;
end

function [ T ] = matrix_normal( dim, J, ~ );
    T = cell( 1, J );
    for j = 1:J;
        switch randi( [0 1] );
            case 0;
                d = rand( 1, dim );
                
            case 1;
                d = randn( 1, dim );
            otherwise;
                fatal_error; end; 
        switch randi( [0 1] );
            case 0;
                O = rand( dim, dim );
            case 1;
                O = randn( dim, dim );                
            otherwise;
                fatal_error; end; 
       [O, ~] = qr( O );
       T{j} = O'*diag( d )*O; end;            
end

function [ T ] = matrix_orthogonal( dim, J, ~ );
    T = cell( 1, J );
    for j = 1:J;
        switch randi( [0 1] );
            case 0;
                O = rand( dim, dim );
            case 1;
                O = randn( dim, dim );                
            otherwise;
                fatal_error; end; 
       [O, ~] = qr( O );
       T{j} = O; end;            
end

function [ T ] = matrix_orthonormal( dim, J, ~ );
    T = matrix_orthogonal( dim, J );
    for j = 1:numel( T );
        for i = 1:size( T{j}, 2 );
            nrm = sum(T{j}(:,i).^2 );
            T{j}(:,i) = T{j}(:,i)/nrm; end; end;
end


function [ T ] = matrix_house( filter, opt );
    maybe_unused( opt );
    N = condget( filter, 'N' );
    d = condget( filter, 'dim' );
    if( d == 0 );
        T = repcellm( [], [1 N] );
    else;    
        T = cell( 1, N );
        E = eye( d );
        for n = 1:N
            [v, beta] = gallery( 'house', randn(d, 1), randi(3)-1 );
            T{n} =  E - beta*(v*v'); end; end;
end

function [ val ] = matrix_complex( dim, N, verbose, type );

    if( nargin <= 3 ||isempty(type) );
        type = randi( [-12, 10] ); end;
    if( nargin <= 1 || isempty(N) );
        N = randi( [2, 5] ); end;
    if( dim < 0 );
        choice = dim;
    elseif( isstring(dim) || ischar(dim) );
        choice = dim;
    elseif( isstring(type) || ischar(type) );
        choice = type;
    elseif( iscell(type) );
        choice = type{1};
    else
        choice = type(1); end;

    if( dim < 2 );
        if( verbose >= 0 );
            warning( 'gallery_matrixset:zero', 'Matrices with complex eigenvalues must have at least dimension 2. I return the empty matrices.' ); end;
        val = repcellm( [], [1 N] );
        return; 
    end;

    %J=2, dim=2; %XX
    j2d2 = {};
    j2d2{end+1} = {[0 1;0 0],[0 -1;1 0]};
    j2d2{end+1} = {[0 1;0 0],[1 -1;1 1]};
    j2d2{end+1} = {[0 1;0 0],[1 1;-1 1]};
    j2d2{end+1} = {[0 1;0 1],[1 -1;1 1]};
    j2d2{end+1} = {[0 1;0 -1],[1 -1;1 1]};
    j2d2{end+1} = {[0 1;0 -1],[1 1;-1 1]};
    j2d2{end+1} = {[1 -1;1 1],[1 0;-1 0]};  %#ok<NASGU> % XX

    if( false );  % first do negative things
    elseif( check(choice, -11) );
        A=[ 0  0  0  0; 0  0 -3 -2; -3 -2  3 -5;  3 -5  0  0];
        B=[ 0  0  0 -3; 0 -3 -2  3; -2  3 -6  0; -5  0  0  0];  
        val = {A,B};

    elseif( check(choice, -10) );
        a = 1/210*[ 189, 90, 63, 245, 168, 21, 120, 147, -35, 42 ];
        M = [1 3;-2 -1];
        D =  [0 0;1 -1;2 -2;2 -1;3 -2]';
        Sd = getS({a,M,D});
        [T,Om] = transitionmatrix( Sd );
        V0 = constructVt( Om, 0 );
        val = restrictmatrix( T, V0 );

    elseif( check(choice, -9) );
        a = 1/9*[6 3 -3 0 3 6 3].';
        %a = 1/9*[6 2 -3 1 3 6 3].'; %has smp of length 2 !
        %a = 1/78*[ 39, -6, 26, 18, 65, 6, -13, -6, 26, 0, -39, 48, -39, -6, 39, 24, -26 ];
        %a = 1/78*[ 1/2, 9/13, -1/3, -3/13, 1, 8/13, -1/6, -2/13, 1/2, 1/13, -1/2];
        %a = 1/288*[ 72, 128, 72, -16, 63, 144, 0, 48, 81, -16];
        M = -2;
        D = [-1 0];
        Sd = getS({a,M,D});
        [T, Om] = transitionmatrix( Sd );
        V0 = constructVt( Om, 0 );
        val = restrictmatrix( T, V0 );

    elseif( check(choice, -8) );  % something goes wrong with type 'c2' and 'c3'
        val{1} = [-1 -6;23 -20]./[7 7;24 17];
        val{2} = [-13 3;-1 1]./[19 5 1 91];

    elseif( check(choice, -7) );  % something goes wrong with type 'c3'
        val{1} = [-0.149986651394040  -0.859119052376630; 0.958228924322373  -1.178553377472830];
        val{2} = [-0.684696254498354   0.605656859173540; -0.999878041051203   0.010982365741844];

    elseif( check(choice, -6) );  % example 6.1 from "Guglielmi, Wirth, Zennaro - 2005 - COMPLEX POLYTOPE EXTREMALITY RESULTS FOR FAMILIES OF MATRICES
        val{1} = [[cos(1) 0;0 cos(1)] [-sin(1) 0;0 -sin(1)];[sin(1) 0;0 sin(1)] [cos(1) 0;0 cos(1)]];
        val{2} = [[1/2 0;0 1/2] [0 1/2;-1/2 0];[0 1/2;-1/2 0] [1/2 0;0 1/2]];

    elseif( check(choice, -5) );  % example 4.1 from "Guglielmi, Zennaro - 2009 - Finding Extremal Complex Polytope Norms for Families of Real Matrices
        val{1} = [-3 -2 1 2;-2 0 -2 1;1 3 -1 -5;-3 -3 -2 -1];
        val{2} = [1 0 -3 -1;-4 -2 -1 -4;-1 0 -1 2;-1 -2 -1 2];
        
    elseif( check(choice, -4) );  % example 3.2 from "Guglielmi, Zennaro - 2009 - Finding Extremal Complex Polytope Norms for Families of Real Matrices
        val{1} = [-1/2 -sqrt(3)/2;sqrt(3)/2 -1/2];
        val{2} = [11/20 11/20;-11/20 -11/20];

    elseif( check(choice, -3) );  % example 3.1 from "Guglielmi, Zennaro - 2009 - Finding Extremal Complex Polytope Norms for Families of Real Matrices
        if( numel(type) == 1 );
            type(2) = 1; end;  % type(2) = beta; 0 < beta <= 1
        val{1} = [cos(1) -sin(1);sin(1) cos(1)]; 
        val{2} = type(2)*[sqrt(2)/2 sqrt(2)/2;0 0];

    elseif( check(choice, -2) );  % example from Protasov, The Barabanov norm is generically unique, simple, and easily computed, 2020, Equation(12)
        val{1} = [0 1;-1 0];
        val{2} = [.89 .646;-.129 -.178];

    elseif( check(choice, -1) );  % example from Protasov, The Barabanov norm is generically unique, simple, and easily computed, 2020, Equation(12)
        val{1} = [0 1;-1 0];
        val{2} = [0.34 1.046;-.523 .170]; 
    end;

    if( choice <= 0 || round(choice) ~= choice || choice > 10 );
        choice = randi( 4 ) + 3; end;        
        
    vprintf( 'Matrix_complex Choice: %i\n', choice, 'imp',[1 verbose] );
    if( false );
    elseif( check(choice, {0,1,2,'1','1rat'}) );
        % matrices with complex leading eigenvalue and findsmp reports all of them to be s.m.p.s
        val = {};
        i = 0;
        while( numel(val) < N && i < 2000);
            j = 0;
            while( true );  % search for set all of which s.m.p.s
                i = i + 1;
                j = j + 1;
                if( i > 2000 );
                    if( verbose >= 0 );
                        warning( 'tallery:complex', 'Failed to find set of matrices.' ); end;
                    return; end;
                if( j > 300 );
                    vprintf( '\n!', 'imp',[1 verbose] );
                    val(end) = [];
                    break; end;
                vprintf( '.', 'imp',[1 verbose] );
                switch choice;
                    case {1,'1'}; A = complexleadingmatrix( dim );
                    otherwise; A = complexleadingmatrix( dim, true ); end;  % '1rat'
                A = A / rhot( A );
                c = findsmp( [val A], 'v',-1 );
                if( isequal( c, num2cell(1:numel(val)+1)) );
                    val = [val A];
                    vprintf( '%i/%i', numel(val), N, 'imp',[1 verbose] );
                    break; end; end; end;
        vprintf( '\n', 'imp',[1 verbose] );

    elseif( check(choice, {3,4,'3','3rat'}) );
        % matrices where one of them has a complex leading eigenvalue and it is the s.m.p.
        val = {};
        i = 0;
        while( numel(val)<N && i<1000 );
            i = i + 1;
            vprintf( '.', 'imp',[1 verbose] );
            switch choice;
                case {3,'3'}; A = complexleadingmatrix( dim ); A = A/rhot( A );
                otherwise;    A = complexleadingmatrix( dim, true ); end;  % '3rat'
            c = findsmp( [val A], 'v',-1 );
            if( isequal(c,{[1]}) );  %#ok<NBRAK>
                val = [val A];
                vprintf( '%i/%i',numel(val), N, 'imp',[1 verbose] ); end; end;
        vprintf( '\n', 'imp',[1 verbose] );

    elseif( check(choice, {5,6,'5','5rat'}) );
        % matrices, the first has complex leading eigenvalue with rho=1, 
        % the others are scaled such that that the first is the smp-candidate
        %     type 4, the others have arbitrary leading eigenvector
        %     type 5, the others have complex leading eigenvector
        ACCURACY = 0.999999; %minimum accuracy
        val = {};
        i = 0;
        while( numel(val)<N && i<3000);
            l = .8;  % lower factor
            u = 1/l;  % upper factor
            switch type;
                case {5,'5'}; B = complexleadingmatrix( dim ); B = B/rhot( B );
                otherwise; B = complexleadingmatrix( dim, true ); end;  % '5rat'
            while( true && i<3000 );
                i = i+1;
                c = findsmp( [val B], 'v',-1 );
                if( ~numel(val) );
                    val = {B};
                    break;
                elseif( ~all(1 == cellfun('prodofsize',c)) || ~isequal(c{1},1) );
                    if( ~any(vertcat(c{:} ) == numel(val)+1) )
                        if( verbose >= 0 );
                            warning( 'tallery:complex', 'Failed to find set of matrices.' ); end;
                        return; end;
                    vprintf( '-', 'imp',[1 verbose] );
                    B = B*l;
                    u = (u+0.2)/1.2;
                    u = max( u, 1/((1+ACCURACY)/2) );
                else;
                    vprintf( '+', 'imp',[1 verbose] );
                    if( l/u>ACCURACY ); 
                        val = [val B];
                        break; end;
                    B = B*u;
                    l = (l+0.2)/1.2; 
                    l = min( l, (1+ACCURACY)/2 );
                end; end;
            vprintf( '%i/%i',numel(val), N, 'imp',[1 verbose] ); end;
        vprintf( '\n', 'imp',[1 verbose] );

    elseif( check(choice, {7,'7','7rat'}) );
        % matrices, the first has complex leading eigenvalue with rho=1, 
        % the others are modified entrywise such that the first is the smp-candidate
        % type 6, the others have arbitrary leading eigenvector
        % type 7, the others may have complex leading eigenvector
        val = {};
        i = 0;
        while( numel(val)<N && i<1000 );
            NUMLOOPS = 14;
            f = 1.4;
            switch type;
                case {7,'7'};
                    B = complexleadingmatrix( dim ); B = B/rhot( B );
                otherwise;
                    B = complexleadingmatrix( dim, true ); end;  % '7rat'
            while( true && i<1000 );
                i = i+1;
                c = findsmp( [val B], 'v',-1 );
                idx = randperm( numel(B), floor((dim^2)/4) );
                if( ~numel(val) );
                    val = {B};
                    break;                
                elseif( ~all(1 == cellfun('prodofsize',c)) || ~isequal(c{1},1) );
                    vprintf( '-', 'imp',[1 verbose] );
                    B(idx) = B(idx)/f;
                else;
                    if( i>NUMLOOPS ); 
                        val = [val B];
                        break; end;
                    vprintf( '+', 'imp',[1 verbose] );
                    B(idx) = B(idx)*f;
                    f = (f+0.1)/1.1; end; end;
            vprintf( '%i/%i', numel(val), N, 'imp',[1 verbose] ); end;
        vprintf('\n', 'imp',[1 verbose] );

    elseif( check(choice, {8,'8'}) );
        % matrices where the product of two matrices has complex leading eigenvector
        val = {};
        i = 0;
        if( verbose >= 0 );
            expect( isempty(N), 'gallery_matrixset:complex', '\nFor type 8 the number of matrices is random.\nSet N = [] to disable this warning.' ); end;
        vprintf( 'subtype: ', 'imp',[1 verbose] );
        j = 0;
        while( true && i<100000 );
            i = i+1;
            A = complexleadingmatrix( dim ); 
            A = A/rhot( A );
            %A = randn( dim );
            while( true );  % try all matrix factorizations
                j = j + 1;
                switch j;
                    case 1; continue; %T = cell(1,2); [T{:}] = balance( A ); %[T{:}] = lu( A );
                    case 2; continue; %T = cell(1,3); [T{:}] = equilibrate( A ); %qr( A );
                    case 3; continue; %T = cell(1,2); [T{:}] = schur( A ); T{3} = T{1}';
                    case 4; continue; %T = cell(1,3); [T{:}] = svd( A );
                    case 5; continue; %T = cell(1,3); [T{:}] = poldecomp( A ); T = T([1,2]);
                    case 6; continue; %T = cell(1,3); [T{:}] = poldecomp( A ); T = T([1,3]);
                    case 7; continue; %T = cell(1,2); [T{:}] = frd( A );
                    case 8; 
                        T = cell(1,2); 
                        T{1} = complexleadingmatrix( dim ); 
                        T{1}=T{1}/rhot(T{1}); 
                        T{2} = T{1}\A;
                    case 9; continue;
%                         T = cell(1,2); 
%                         [T{:}] = deal( A ); 
%                         idx = randi( numel(T{1}) );
%                         T{1}(idx) = T{1}(idx)-abs(randn(1)*0.01); 
%                         idx = randi( numel(T{1}) );
%                         T{2}(idx) = T{2}(idx)-abs(randn*.01);
                    otherwise; j = 0; break; end;

                if( any( rhot(T).^numel(T) > rhot(A) ) );
                    continue; end;
                %vprintf( '%i ', j, 'imp',[1 verbose] ); 
                %rhot( T )
                c = findsmp( T, 'v',-1, 'bound',[1+10*eps inf] );   
                %vdisp( c );
                v0 = leadingeigenvector( T, c, 'complexeigenvector',0, 'cycle',0, 'rep',0, 'multipleleadingeigenvector',0 ); 
                if( any(matrixinfo(v0,'real')) );
                    continue; end;
                c = findsmp( T, 'v',-1 );   
                %vdisp( c );
                v0 = leadingeigenvector( T, c, 'complexeigenvector',0, 'cycle',0, 'rep',0, 'multipleleadingeigenvector',0 ); 
                if( any(matrixinfo(v0,'real')) );
                    continue; end;                
                val = T;
                break; end;
            if( ~isempty(val) );
                vprintf( '%i/2 ', j, 'imp',[1 verbose] ); 
                break; end; end;
        vprintf( '\n', 'imp',[1 verbose] );

    elseif( check(choice, {9,'prot1'}) );
        if( numel(type) == 3 );
            al = type(2);
            be = type(3);
        else;
            al = 1;
            be = .5; end;
        if( iscell(al) );
            al = al{1};
            be = be{1}; end;
        Pxy = [1 0 0;0 1 0;0 0 0].';
        Pyz = [0 0 0;0 1 0;0 0 1].';
        Sxz = [0 0 1;0 1 0;-1 0 0].';
        Szx = [0 0 -1;0 1 0;1 0 0].';
        Rxa = [1 0 0;0 cos(al) sin(al);0 -sin(al) cos(al)].';
        Rzb = [cos(be) sin(be) 0;-sin(be) cos(be) 0;0 0 1].';
        val{1} = Rxa*Sxz*Pxy;
        val{2} = Rzb*Szx*Pyz;

    elseif( check(choice, {10,'prot2'}) );
        if( numel(type) >= 3 );
            al = type(2);
            be = type(3);
        else;
            al = sym(sqrt(2))*sym(pi);
            be = sym(sqrt(3))*sym(pi); end;
        if( numel(type) >= 4 );
            a = type(4);
        else;
            a = sym(9/10); end;
        if( iscell(al) );
            al = al{1};
            be = be{1}; end;  
        if( iscell(a) );
            a = a{1}; end;
        Rxa = [cos(al) sin(al);-sin(al) cos(al)].';
        Rxb = [cos(be) sin(be);-sin(be) cos(be)].';
        val{1} = Rxa;
        val{2} = Rxb*[a 0;0 1]; end;
    
     if( isempty(val) );
         warning( 'gallery_matrixset:complex', 'Failed to generator example.' ); end;
end

function [ val ] = nearlycomplex();
   val{1} = [
        0.260779273117590   0.321746900633509   0.430143562965100
        0.332728106032852   0.165237727924909   0.246741880562854
        0.435544012664021   0.460173223125738   0.331865771472727
        ];
   
   val{2} = [
        0.682274614607416  -0.373006400071818   0.529326054171797
        0.741899861356640   0.551196958575390  -0.263494595696068
       -0.371216501721239   1.002783855858494   0.524985866808493
       ];
end

function [ R, U, V ] = poldecomp( F );  %#ok<DEFNU>
    %POLDECOMP  Performs the polar decomposition of a regular square matrix.
    %   [R U V] = POLDECOMP(F) factorizes a non-singular square matrix F such
    %   that F=R*U and F=V*R, where
    %   U and V are symmetric, positive definite matrices and
    %   R is a rotational matrix
    C = F'*F;
    [Q0, lambdasquare] = eig( C );
    lambda = sqrt(diag((lambdasquare))); % extract the components
    Uinv = repmat(1./lambda',size(F,1),1).*Q0*Q0';
    R = F*Uinv;
    U = R'*F;
    V = F*R';
end

function [ K, L ] = frd( X );  %#ok<DEFNU>
% FRD - Full rank factorization of input matrix X. 
% [K L] = frd( X ) will write X as the product of two matrices X = KL where both K and
% L have the same rank as X
% https://de.mathworks.com/matlabcentral/fileexchange/19197-full-rank-factorization
    [U, S, V] = svd(X) ;
    [n,p] = size(S);
    D1 = zeros(n,p);
    D2 = zeros(p,p);
    a = diag(S);
    sqrt_a = a.^(.5) ;
    for i = 1:length(a)
        D1(i,i) = sqrt_a(i);
        D2(i,i) = sqrt_a(i); end;
    K = U*D1 ;
    L = D2*V' ;
end

%% signal matrices
%%%%%%%%%%%%%%%%%%%%

% Code taken from fdnToolbox, (c) Sebastian Jiro Schlecht
function [ Q ] = randomOrthogonal( n );
% Generates a random n x n orthogonal real matrix
%
% Matrix distributed according to the Haar measure over the group of
% orthogonal matrices. The Haar measure provides a uniform distribution
% over the orthogonal matrices.
%
% Based on Nick Higham
% https://nhigham.com/2020/04/22/what-is-a-random-orthogonal-matrix/

    [Q, R] = qr( randn(n) );
    Q = Q*diag( sign(diag(R)) );
end

function [ r ] = randSgn( varargin );
%randSgn - Generate random sign (-1/+1), wrapper for randi
%
% Syntax:  r = randSgn(varargin)
%
% Inputs:
%    varargin - same as randi
%
% Outputs:
%    r - random sign value
%
% Example: 
%    r = randSgn(3)
%    r = randSgn(4,2)
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: 
% Author: Dr.-Ing. Sebastian Jiro Schlecht, 
% Aalto University, Finland
% email address: sebastian.schlecht@aalto.fi
% Website: sebastianjiroschlecht.com
% 28 December 2019; Last revision: 28 December 2019

    r = randi( [0 1], varargin{:} )*2 - 1;
end

function [ H ] = householderMatrix( u );
%householderMatrix - Create Householder matrix from vector
%
% Syntax:  H = householderMatrix(u)
%
% Inputs:
%    u - Vector u which is orthogonal to the reflection hyperplane
%
% Outputs:
%    H - Householder transformation matrix
%
% Example: 
%    H = householderMatrix(randn(4,1))
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: 
% Author: Dr.-Ing. Sebastian Jiro Schlecht, 
% Aalto University, Finland
% email address: sebastian.schlecht@aalto.fi
% Website: sebastianjiroschlecht.com
% 28 December 2019; Last revision: 28 December 2019

    u = u(:);
    u = u / norm(u);

    N = length(u);
    H = eye(N) - 2*(u*u');
end

function [ rotationMatrix ] = tinyRotationMatrix( N, delta, varargin );
%tinyRotationMatrix - orthogonal matrix with a small eigenvalue angles
%
% Syntax:  rotationMatrix = tinyRotationMatrix(N,delta)
%
% Inputs:
%    N - Matrix size 
%    delta - Mean normalized eigenvalue angle
%    spread - (optional) Spreading of eigenvalue angle 
%    logMatrix - (optional) Initial logarithm matrix
%
% Outputs:
%    rotationMatrix - output matrix
%
% Example: 
%    rotationMatrix = tinyRotationMatrix(4,1/48000,0)
%    rotationMatrix^48000 is identity
%
% Author: Dr.-Ing. Sebastian Jiro Schlecht, 
% Aalto University, Finland
% email address: sebastian.schlecht@aalto.fi
% Website: sebastianjiroschlecht.com
% 17. January 2020; Last revision: 17. January 2020

    % Input Parser
    p = inputParser;
    p.addOptional('spread',0.1);
    p.addOptional('logMatrix',randn(N));
    parse(p, varargin{:});

    spread = p.Results.spread;
    x = p.Results.logMatrix;

    % Generate skew symmetric matrix
    skewSymmetric = (x - x')/2;

    [v,e] = eig(skewSymmetric);

    % Make complex conjugate
    IDX = nearestneighbour(v,conj(v));
    frequencySpread = 2*(rand(N,1)-0.5) * spread + 1;
    frequencySpread = (frequencySpread(IDX) + frequencySpread) / 2;
    frequencySpread( IDX - 1:N == 0 ) = 0; % non-complex eigenvalue 

    % Scale and spread
    E = diag(e);
    nE = E ./ abs(E) .* frequencySpread * delta * pi;

    % Create orthogonal matrix
    skewSymmetric = real(v* diag(nE)*v');
    skewSymmetric = (skewSymmetric - skewSymmetric') / 2;

    rotationMatrix = expm(skewSymmetric);

    % Alternative
    % rotationMatrix = v*diag(exp(nE))*v';
    % nearestOrthogonal

end

function [ matrix, inputGain, outputGain, direct ] = allpassInFDN( g, A, b, c, d );
%allpassInFDN - Create allpass in FDN of size [2N,2N]
% see Schlecht, S. (2017). Feedback delay networks in artificial
% reverberation and reverberation enhancement
%
% Syntax:  [matrix, inputGain, outputGain, direct] = allpassInFDN(g, A)
%
% Inputs:
%    g - Array of feedforward/back gains
%    A - feedback matrix 
%    b - input gains 
%    c - output gains
%    d - direct gains
%
% Outputs:
%   matrix - FDN feedback matrix
%   inputGain - FDN input gains
%   outputGain - FDN output gains
%   direct - FDN direct gain
%
% Example: 
%    allpassInFDN(randn(3,1),randomOrthogonal(3))
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: isAllpassFDN
% Author: Dr.-Ing. Sebastian Jiro Schlecht, 
% Aalto University, Finland
% email address: sebastian.schlecht@aalto.fi
% Website: sebastianjiroschlecht.com
% 28 December 2019; Last revision: 28 December 2019

    G = diag(g);
    I = eye(size(G));

    matrix = [-A*G, A; I - G^2, G];
    inputGain = [b; 0*b];
    outputGain = [g; c];
    direct = d;

end

function [ matrix, inputGain, outputGain, direct ] = nestedAllpass( g );
%nestedAllpass - Create nested allpass as FDN 
%
% Syntax:  [matrix, inputGain, outputGain, direct] = nestedAllpass(g)
%
% Inputs:
%    g - Array of feedforward/back gains
%
% Outputs:
%   matrix - FDN feedback matrix
%   inputGain - FDN input gains
%   outputGain - FDN output gains
%   direct - FDN direct gain
%
% Example: 
%    nestedAllpass(randn(3,1))
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: isAllpassFDN
% Author: Dr.-Ing. Sebastian Jiro Schlecht, 
% Aalto University, Finland
% email address: sebastian.schlecht@aalto.fi
% Website: sebastianjiroschlecht.com
% 28 December 2019; Last revision: 28 December 2019

    % Initialize feedforward/back allpass filter 
    matrix = g(1);
    inputGain = 1-g(1)^2;
    outputGain = 1;
    direct = -g(1);

    % Iterative nesting of feedforward/back allpass filters
    for it = 2:length(g)
        [matrix, inputGain, outputGain, direct] = nestingFDNinAllpass(g(it), matrix, inputGain, outputGain, direct);
    end
end

function [ N_matrix, N_inputGain, N_outputGain, N_direct ] = nestingFDNinAllpass( allpassGain, matrix, inputGain, outputGain, direct );
    % Nest a FDN within an feedforward/back allpass filter 

    N_matrix = [matrix, inputGain; outputGain*allpassGain, direct*allpassGain];

    N_inputGain = [ 0*inputGain; 1 - allpassGain^2];

    N_outputGain = [ outputGain, direct];

    N_direct = -allpassGain;

end

function [ matrix, inputGain, outputGain, direct ] = SchroederReverberator( allpassGain, combGain, b, c, d );
%SchroederReverberator - Create combs and allpass as FDN
% see Schlecht, S. (2017). Feedback delay networks in artificial
% reverberation and reverberation enhancement
%
% Syntax:  [matrix, inputGain, outputGain, direct] = SchroederReverberator(g, A)
%
% Inputs:
%    allpassGain - Array of feedforward/back gains for allpass
%    combGain - feedback gains for parallel comb filters
%    b - input gains of comb filters
%    c - output gains of comb filters
%    d - direct gains of comb filters
%
% Outputs:
%   matrix - FDN feedback matrix
%   inputGain - FDN input gains
%   outputGain - FDN output gains
%   direct - FDN direct gain
%
% Example: 
%    
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: isAllpassFDN
% Author: Dr.-Ing. Sebastian Jiro Schlecht, 
% Aalto University, Finland
% email address: sebastian.schlecht@aalto.fi
% Website: sebastianjiroschlecht.com
% 28 December 2019; Last revision: 28 December 2019

    [APmatrix, APinputGain, APoutputGain, APdirect] = seriesAllpass(allpassGain);

    P = diag(combGain);

    S = APinputGain * c;

    matrix = [P, 0*P; S, APmatrix];
    inputGain = [b; 0*APinputGain];
    outputGain = [APdirect*c, APoutputGain];
    direct = d * APdirect;

end

function [ A ] = AndersonMatrix(N, varargin);
%AndersonMatrix - Generates block circulant orthogonal matrix
% NxN block circulant matrix (only first off-diagonal) consisting of KxK
% matrices of type. 
%
% see Anderson, H., Lin, K., So, C., Lui, S. (2015). Flatter Frequency
% Response from Feedback Delay Network Reverbs Proc. Int. Comput. Music
% Conf. 2015(), 238 - 241.
%
% Syntax:  [A] = AndersonMatrix(N, varargin)
%
% Inputs:
%    N - Size of generated matrix A
%    K - (Optional) size of matrix blocks 
%    type - (Optional) matrix type of blocks
%
% Outputs:
%    A - Generated Matrix
%
% Example: 
%    A = AndersonMatrix(9,3,'circulant')
%    A = AndersonMatrix(8)
%
% Other m-files required: fdnMatrixGallery
%
% See also: fdnMatrixGallery
% Author: Dr.-Ing. Sebastian Jiro Schlecht, 
% Aalto University, Finland
% email address: sebastian.schlecht@aalto.fi
% Website: sebastianjiroschlecht.com
% 19. April 2020; Last revision: 19. April 2020

    % parse
    f = factor(N);
    p = inputParser;
    p.addOptional('K', f(1), @(x) isnumeric(x) && isscalar(x) && x >= 1 );
    p.addOptional('type','Hadamard',@(x) ischar(x) );
    parse(p, varargin{:});

    K = p.Results.K;
    type = p.Results.type;

    assert( mod(N, K) == 0, 'N needs to be dividable by K.');

    % generate
    numberOfBlocks = N/K;

    for it = 1:numberOfBlocks
        M{it} = fdnMatrixGallery( K, type, varargin ); end;

    A = blkdiag( M{:} );
    A = circshift( A, K );
end


%% generating graph functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ G ] = graph_block( n, J );
    [len, n] = make_block_lengths( n, J, 1 );
    assert( sum(len) == J );
    blk = cell( 1, n );
    for i = 1:n;
        blk{i} = ones( len(i) ); end;
    G = ~blkdiag( blk{:} ); 
end

function [ G ] = graph_cycle( n, J );
    if( isscalar(n) );
        [len, n] = make_block_lengths( n, J, 1 );
    else;
        len = n; 
        n = numel( len ); end;
    assert( sum(len) == J );
    G = zeros( J, J );
    for i = 1:n;
        if( i<n );
            nr_z1 = sum(len(1:i));
            nr_o = len(i+1);
        else;
            nr_z1 = 0;
            nr_o = len(1); end;
        nr_z2 = J - nr_z1 - nr_o;
        nc = len(i);
        idxc_G = (1+sum(len(1:i-1))):sum(len(1:i));
        G(:,idxc_G) = [zeros( nr_z1,nc) 
                         ones( nr_o, nc )
                         zeros( nr_z2, nc)]; end;
end

function [ G ] = graph_cycle_unbd( n, J );
    if( isscalar(n) );
        [len, n] = make_block_lengths( n, J, 2 );
    else;
        len = n;
        n = numel( n ); end;
    assert( sum(len) == J );
    G = zeros( J, J );
    for i = 1:n;
        idx_row = (sum( len(1:i-1) ) + 1):(sum( len(1:i) ) - 1);
        if( i < n );
            idx_col = (sum( len(1:i) ) + 1):sum( len(1:i+1) );
        else;
            idx_col = 1:len(1); end; 
        G(idx_row,idx_col) = 1;

        idx_row = idx_row(end) + 1;
        idx_col = (sum( len(1:i-1) ) + 1):sum( len(1:i) );
        G(idx_row,idx_col) = 1;
    end;
end

function [ G ] = graph_random( n, J );
    while( true );
    %     f = factors( J );
    %     n = f( randi(numel(f)) );
        Go = rand( n, J ) < rand/log(J);
        G = Go;
        for i = 1:(J/n)-1;
            G = [G; circshift(Go, i*n, 2)]; end;
        ft = graph_precomponent( G, [], 1 );
        if( max(ft) == n );
            break; end; end;
end

function [ G ] = graph_permute( n, J );
    if( n == 2 && J == 2 );
        G = [0 1;1 0];
        return; end;
    
    len = make_block_lengths( n, J, 1 );
    E = cell( 1, numel(len) );
    for i = 1:numel( len );
        E{i} = ones( len(i) ); end;
    G = blkdiag( E{:} );
    G = G(:, randperm( J ) );   
end



%% utility functions
%%%%%%%%%%%%%%%%%%%%%%%%%

function [ len, n ] = make_block_lengths( n, J, minsze );
    J = J - (minsze-1)*n;
    if( ~isscalar(n) );
        len = n;
        assert( sum(len) == J );
        n = numel( n ); 
        return; end;
    assert( n<=J );
    while( true );
        len = rand( 1, n );
        len = floor( len/sum(len)*J );
        while( sum(len)<J );
            idx = randi( [1 n] );
            len(idx) = len(idx) + 1; end;
        if( nnz(len) == n );
            break; end; end;
    len = len + (minsze-1);
end

function [ ret ] = randx( varargin );
    switch randi( 5 );
        case 1;
            ret = randi( dist+1, varargin{:} );
        case 2;
            ret = randi( dist+1, varargin{:} ) - randi( dist+1, varargin{:} );
        case 3;
            ret = randn( varargin{:} );
        case 4;
            ret = dist( rand(varargin{:}), 1, inf ); 
        case 5; 
            ret = dist( rand(varargin{:}), 1, inf ) - dist( rand(varargin{:}), 1, inf );
        otherwise;
            fatal_error; end;
    
%     switch randi(3);
%         case 1;
%             val = randi(9);
%             ret = round( ret/9 )*val;
%         case 2;
%             h = @(x) exp(exp(x)) - exp(exp(-x));
%             ret = h( ret );
%         case 3;
%             % do nothing
%         otherwise;
%             fatal_error; end; 

end

function [ ret ] = dist( varargin );
    if( nargin == 0 );
        x = rand;
        m = 0;
        M = inf;
    elseif( nargin == 2 );
        x = rand;
        m = varargin{1};
        M = varargin{2};
    else;
        x = varargin{1};
        m = varargin{2};
        M = varargin{3}; end;
        
    % function which gives random integers between m and M, 0<=m<=M<=inf
    k = .5;  % the higher k, the smaller the returned values
    e = .5;  % offset is necessary so that function works for m=0 too
    ret = round((m+e)./(1 + (m+e)./M.*x.^k - x.^k)-e/2);
end

function [ check ] = invariant( M, G, filter, opt );  %#ok<INUSD>
    % things which must always be true
    sze = matrixinfo( M, 'size' );
    assert( ~anym( diff(vertcat(sze{:}), 1, 1) ), 'ttoolboxes:fatal', 'Sizes of returned matrices not the same. This is a programming error.' );
    assert( isempty(sze) || numel(sze{1}) == 2 && sze{1}(1) == sze{1}(2), 'ttoolboxes:fatal', 'Returned array is not a square matrix. This is a programming error.' );

    check = true;
    check = do_check( check, filter.N,              M,  @numel );
    check = do_check( check, filter.dim,            M,  @(x) size( x{1}, 1 ) );
    check = do_check( check, G,                     [],  @() size(G, 1) == numel(M) && issquare(G) && ~isstring(G) && ~ischar(G) );
    check = do_check( check, filter.different,      [], @() numel( uniquecell(M) ) == numel( M ) );
    check = do_check( check, filter.poslead,        [], @() allm( matrixinfo( M, 'poslead' ) ) );
    check = do_check( check, filter.invertible,     [], @() allm( matrixinfo( M, 'cond' ) < filter.invertible ) );
    check = do_check( check, filter.nonnegative,         [], @() allm( matrixinfo( M, 'nonneg' ) ) );
    check = do_check( check, filter.pos,            [], @() allm( matrixinfo( M, 'pos' ) ) );
    check = do_check( check, filter.rat,            [], @() allm( matrixinfo( M, 'rat' ) ) );
    check = do_check( check, filter.bool,           [], @() allm( matrixinfo( M, 'bool' ) ) );
    check = do_check( check, filter.finite,         [], @() allm( matrixinfo( M, 'finite' ) ) );
    check = do_check( check, filter.irreducible,    [], @() numel( invariantsubspace( M, 'all', 'complex',1 ) ) == 1 );
    check = do_check( check, filter.int,            [], @() allm( matrixinfo( M, 'int' ) ) );
    check = do_check( check, filter.norm,           M,  @norm );
    check = do_check( check, filter.real,           [], @() allm( matrixinfo( M, 'real' ) ) );
    i = 1;
    while( check && i <= numel(filter.filter) );
        fi = filter.filter{i};
        if( i < numel( filter.filter ) && ...
            isanyof( filter.filter{i+1}, {0, 1,'m','yes','must','all', -1,'n','no','not','mustnot', 'some','any', 'notall','nall','mandatory'} ) ...
          );
            switch filter.filter{i+1};
                case {0}; flag = 0;
                case {1,'m','yes','must','all','mandatory'}; flag = 'all';
                case {-1, 'n','no','not','mustnot','none','forbidden','never'}; flag = 'none';
                case {'some','any','optional'}; flag = 'any';
                case {'notall','nall'}; flag = 'nall';
                otherwise; error( 'gallery:matrixset', 'I cannot parse how to apply the filter. I do not understand the word: %s', filter.filter{i+1} ); end;
            i = i + 1;
        else;
            flag = 'all'; end;
        if( flag ) == 0;
            i = i + 1;
            continue; end;
        if( isa( fi, 'function_handle' ) );
            checki = true;
            for i = 1:numel( M );
                checki = checki && fi( M{i} ); end;
            if( flag == -1 );
                checki = ~checki; end;
            check = check && checki;
        elseif( ischar(fi) || isstring(fi) );
            checki = matrixinfo( M, fi, 'verbose',-1 );
            if( isempty(checki) );
                i = i + 1;
                continue; end;
            switch flag;
                case 0; fatal_error;
                case 'all'; checki = all( checki(:) );
                case 'any'; checki = any( checki(:) );
                case 'none'; checki = ~any( checki(:) );
                case 'nall'; checki = ~all( checki(:) );
                otherwise; fatal_error; end;
            check = check && checki;
        else;
            error( 'gallery_matrixset:filter', 'Wrong value for ''filter'' is given. Cannot parse input: ''%s''', dispc( fi ) ); end;
        i = i + 1; end;
end

function [ check ] = do_check( check, filter, M, handle );
    if( ~check );
        return; end;
    if( ~isempty(filter) );
        if( islogical(filter) || ischar(filter) || isequal(filter, -1) );
            assert( isempty(M) );
            switch lower( filter );
                case {0};  % I dont mind
                    % do nothing
                case {1,'m','yes','must'};  % must-have/mandatory
                    check = handle();
                case {-1,'n','no','not','mustnot'};  % must-not-have
                    check = ~handle();
                otherwise; fatal_error; end;
        elseif( isvector(filter) && isnumeric(filter) && numel(filter) <= 2 );
            check = handle( M ) >= min( filter ) && handle( M ) <= max( filter );
        else;
            check = handle(); end; end;
end

function [ ret ] = condget( st, name, varargin );
    if( isempty(st.(name)) );
        if( isempty(varargin) );
            switch name;
                case 'dim'; func = randi( [2, 25] );
                case {'N','J'};   func = randi( [2, 10] );
                case 'pos'; func = randi( [0 1] );
                otherwise;  fatal_error; end;
        else;
            func = varargin{1}; end;
        ret = func(); 
    else;
        switch name;
            case {'dim','N','J'};   ret = randi( [floor(min(st.(name))) ceil(max(st.(name)))] );
            case {'pos'};           ret = st.(name);
            otherwise; fatal_error; end; end;

end

function [ ret ] = check( what, names );
    if( isempty(what) );
        ret = false;
        return; end;
    names = tocell( names );
    for i = 1:numel( names );
        names{i}(names{i} == '_') = [];
        names{i} = lower( names{i} ); end;
    %what(what == '_') = [];
    %what = lower( what );
    ret = false;
    for i = 1:numel( names );
        if( isequal( names{i}, what ) );
            ret = true;
            break;
        elseif( ~ischar(what) || ~ischar(names{i}) );
            ret = false;
        else;
            [a, b] = regexpi( names{i}, regexptranslate('wildcard', what), 'once' );
            if( isequal(a, 1) && isequal(b, numel(names{i})) );
                ret = true;
                break; end; end; end;
end

function test_what_for_randomize_matrixset_options( opt );
    if( isanyof( opt.what, {'metzler'} ) );
        fprintf( 2, 'Passed matrix type is not supported by this function.\n  But matrices of these type can be generated using the function randomize_matrixset.\n' );
        end;
end



%% playground
%%%%%%%%%%%%%%%%%%%%

function playground;  %#ok<DEFNU>

    % nice bad example, funktioniert in Matlab2016a, aber nicht mehr in Matlab2017a
    % funktioniert in L'Aquila auch mit Matlab2017a
    M = [2 1; 0 3]; a0=[1 0 0; 1 1 1; 0 1 1];D=[     0     1     1     1     2     2;     0     0     1     2     1     2 ];
    S = getS( {1/6*convm(a0,a0),M,D}, 'bigcheck' );
    [T, ~] = transitionmatrix( S );
    U2 = double([1 0 0 -1 0 0 0 0; 0 0 1 0 0 -1 0 0; 0 0 0 0 1 0 0 -1; 1 0 -2 0 1 0 0 0; 0 1 0 -2 0 1 0 0; 0 0 1 0 -2 0 1 0; 0 0 0 1 0 -2 0 1]');
    TU2 = restrictmatrix( T, U2, 'outputdouble' );
    ipa( TU2, 'verbose',2, 'autoextravertex',0.01 )

    
    
    ipa( gallery_matrixset('cex2'),'plot','tree','verbose',2,'autoextravertex',0)
    ipa({[2 1; 0 -2],[2 0; -1 -2]},'invariantsubspace','none','maxsmpdepth',2);  % gut um selectordering zu testen
    ipa( {[2 1; 0 2],[2 0; -1 2]} ); % alle orderings mit Laenge kleiner gleich 7 haben rho == 2, darüber steigt er an auf 2.36007056204988
    ipa( gallery_matrixset('ani',5),'maxsmpdepth',1,'verbose',2,'invariantsubspace','none','plot','norm','autoextravertex',0,'clc','waitafterbalancing');  % Multivariates Subdivision Bsp das nur mit Balancing funktioniert. Gegenbeispiel zur Vermutung Protasovs.


    %ipa(gallery_matrixset('nondominantsmp'),'addtranspose','plot','polytope','nobalancing','verbose',2,'simplepolytope',1e-10,'testeigenplane',0,'testspectralradius',0,'findsmp_N',1000,'maxsmpdepth',500)
    ipa( gallery_matrixset('vale'),'maxsmpdepth',1,'autoextravertex',1,'plot','norm','clc','verbose',2,'simplepolytope',1,'clc','balancingdepth',3,'ordering',{[1]},'waitafterbalancing',1)  %#ok<NBRAK>
    
    
    % balancing problem
    A =[    -3     2;    -4    -3]; B =[     3     2;     0     0];
    ipa( {A,B}, 'plot','polytope' )
    
    % set which seems to have infinetly many smps with infinetly many leading eigenvectors
    A = [0 0 1;0 1 1;1 0 1]; B=[0 1 0;1 0 1;1 1 1];
    ipa( {A,B} );
    %
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
