function [ oo ] = findsmp_pattern( M, pattern, varargin );
% experimental
% [ oo ] = findsmp_pattern( M, pattern, [options] );
% searches for an smp with some pattern
%

    % parse input
    [verbose, varargin] =       parsem( {'verbose','v'}, varargin, 1 );  %#ok<ASGLU>
    [toa, varargin] =           parsem( {'timeofaction','time_of_action','toa'}, varargin, [] );
    [maxsmpdepth, varargin] =   parsem( {'maxsmpdepth','maxd','maxdepth','depth'}, varargin, 100 );
    parsem( varargin, 'test' );
    pattern = tocell( pattern );
    
    for i = 1:numel( pattern );
        if( size(pattern{i}, 1) == 1 );
            pattern{i} = [1:numel( M ); pattern{i}]; end;
        %idx = pattern{i} == -inf;
        %pattern{i}(idx) = floor( -maxsmpdepth^(1/nnz(idx)) ); 
        end;
    
    if( isempty(toa) );
        toa = ones( size(M) ); end;
    
    % main part
    la_max = 0;
    oo = {};
    for i = 1:numel( pattern );
        % generate start
        pi = pattern{i};
        oi2 = pi;
        idx = oi2(2,:) < 0;
        oi2(2,idx) = 0;
        if( ~any(oi2(2,:)) );
            oi2 = next_pattern( oi2, pi, inf ); end;

        
        while( true );
            % compute spectral radius
            oo_ =  oo2_to_oo1(oi2);
            oo_ = reducelength( oo_ );
            P_ = buildproduct_fast( M, oo_ );
            toa_ = buildsum_fast( toa, oo_ );
            la_ = rho( P_ )^(1/toa_);
            if( la_ > la_max );
                la_max = la_;
                oo = {oo_};
            elseif( la_ > la_max - 4*eps(la_max) );
                oo = [oo oo_];  %#ok<AGROW>
                end;
                
            
            % compute next pattern
            [oi2, flag] = next_pattern( oi2, pi, maxsmpdepth );
           
            if( flag );
                break; end; end;
    end
    
    % post process
    oo = uniquecell( oo );
end

function [ oi2, flag ] = next_pattern( oi2, pi, maxsmpdepth );
    flag = false;
    j = 1;
    while( true );
        if( pi(2,j) >= 0 );
            j = j + 1;
        elseif( oi2(2,j) < -pi(2,j) && sum( oi2(2,:) ) < maxsmpdepth );
            oi2(2,j) = oi2(2,j) + 1;
            break;
        else;
            oi2(2,j) = 0;
            j = j + 1; end;
        if( j > size(pi, 2) );
            flag = true;
            break; end; end;
end

function [ oo1 ] = oo2_to_oo1( oo2 );
    idx = 0;
    oo1 = zeros( 1, sum(oo2(2,:)) );
    for i = 1:size( oo2, 2 );
        len = oo2(2,i);
        oo1(idx+1:idx+len) = oo2(1,i);
        idx = idx + len;
    end
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
