function [ P ] = reducelength( P, flag );
% [ P ] = reducelength( P, [flag] )
% Takes a pattern and removes repetitions and cycles it such that it has smallest lexicographic value.
% Zeros are ignored
%
% Input:
%   P       row/column vectors, OR
%           cell array of row/column vectors
%   flag    integer, default = 0, what to do:
%               0   (default) removes periodics and cycles
%               1   removes only periodics
%               2   cycles only
%
% Output:
%   P     (in the same format as the Input)
%
% E.g.:   vdisp( reducelength( {[2 1 2 2 1 2], [3 1]} ) )
%         vdisp( reducelength( {[2 1 2 2 1 2], [3 1]} ,1 ) )
%
% See also: lexicographic 
%
% Written by: tommsch, 2018

%               tommsch, 2022-02-15, Bugfix for really long orderings
% Changelog:

%#ok<*ALIGN>

    if( nargin == 1 );
        flag = 0; end;
    
    if( iscell(P) );
        for i = 1:size( P, 2 ) ;
            if( isempty(P{i}) );
                continue; end;
            P{i} = reducelength_worker( P{i}, flag ); end;
    else;
        if( isempty(P) );
            P = [];
        else;
            P = reducelength_worker( P, flag ); end; end;

end

function [ P ] = reducelength_worker( P, flag )

    if( isempty(P) ); 
        P = zeros( 0, 1 );
        return; end;

    P = double( P );
    P(P == 0) = [];  % remove all zeros in P
    if( isempty(P) );
        return; end;

    isrowvector = size(P,1) > 1;
    if( isrowvector );
        P = P.'; end;

    if( flag == 0 || flag == 1 );
        P = findperiodics( P );end;

    if( flag == 0 || flag == 2 );
        P = cyclelexicographic( P ); end;


    if( isrowvector );
        P = P.'; end;

end

function P = findperiodics( P )
    if( true );
        % kmp based algorithm
%       idx = kmp_search( [P P], P );  % using kmp
        idx = strfind( [P P], P );  % using matlab intrinsics
        len = idx(2) - idx(1);
        P = P(1:len);
    else;
        % tommsch algorithms
        n = numel( P );  %#ok<UNRCH>
        f = factor( n );

        % find all divisors of n
        f = nchoosek( [ones(size(f)) f], length(f) );
        f = cumprod( f, 2 );
        f = f(:,end)';
        f = unique( f );
        f = f(1:end-1);

        % find periodics
        for i = 1:length( f );
            canshorten = 1;
            test = P(1:f(i));
            for j = 2:numel( P )/f(i)
                if( sum(P((j-1)*f(i)+1:(j)*f(i)) ~= test) );
                    canshorten = 0;
                    break; end; end;
            if( canshorten == 1 );
                P = test;
                break; end; end; 
    end;
end

function P = cyclelexicographic( P );
    % cycle lexicographic
    MAX = ones( 1, size(P,2) )*max(P);
    MAX = MAX.^(length( P )-1:-1:0);  %#ok<NASGU>

    if( true );
        % Duval algorithm
        P = [P P];
        n = numel( P );
        i = 0;
        anss = 0;
        while( i < n / 2 );
            anss = i;
            j = i + 1;
            k = i;
            while ( j < n && P(k+1) <= P(j+1) );
                if ( P(k+1) < P(j+1) );
                    k = i;
                else;
                    k = k + 1; end;
                j = j + 1; end;
            while ( i <= k );
                i = i + j - k; end; end;
        P = P( (anss+1):(anss + n/2) );
        return;
        
    elseif( false && max(MAX) > 1e15 );  %#ok<UNRCH>
        % this method uses double values, and thus cannot be used for really long orderings
        % I did not profile it, but the other algorithm "looks" faster too
        Pnew = P;
        val = sum( P.*MAX );
        for i = 1:length( P )-1;
            Ptest = circshift( P, [0 i] );
            valnew = sum( Ptest.*MAX );
            if( valnew<val );
                val = sum( Ptest.*MAX );
                Pnew = Ptest; end; end;
        P = Pnew;
        
    else;
        % this method is brute force, and needs too much memory
        rowflag = isrow( P );
        P = circulant( P );
        P = sortrows( P' )'; 
        P = P(:,1); 
        if( rowflag );
            P = P'; end; end;
end


function C = circulant( vec )  %#ok<DEFNU>
    n = numel( vec );
    cind = (1:n)';
    rind =  n + 2 - cind';
    rind(rind == (n+1)) = 1;
    C = vec(toeplitz( cind, rind ));
end

function [ P, nP ] = kmp_search( S, W );  %#ok<DEFNU>
%     input:
%         an array of characters, S (the text to be searched)
%         an array of characters, W (the word sought)
%     output:
%         an array of integers, P (positions in S at which W is found)
%         an integer, nP (number of positions)

    j = 0;  % the position of the current character in S
    k = 0;  % the position of the current character in W
    T = kmp_table( W );  % the table
    nP = 0;
    while( j < length(S) );
        if( W(k+1) == S(j+1) );
            j = j + 1;
            k = k + 1;
            if k == numel( W );
                % (occurrence found, if only first occurrence is needed, m ← j - k  may be returned here)
                P(nP + 1) = j - k;  %#ok<AGROW>
                nP = nP + 1;
                k = T(k+1); end; % (T[length(W)] can't be -1)
        else;
            k = T(k+1);
            if( k < 0 );
                j = j + 1;
                k = k + 1; end; end; end;
end

function T = kmp_table( W );
%     input:
%         an array of characters, W (the word to be analyzed)
%     output:
%         an array of integers, T (the table to be filled)
    pos = 1;  % the current position we are computing in T
    cnd = 0;  % the zero-based index in W of the next character of the current candidate substring

    T(0+1) = -1;

    while( pos < length(W) );
        if( W(pos+1) == W(cnd+1) );
            T(pos+1) = T(cnd+1);
        else;
            T(pos+1) = cnd;
            while ( cnd >= 0 && W(pos+1) ~= W(cnd+1) );
                cnd = T(cnd+1); end; end;
        pos = pos + 1;
        cnd = cnd + 1; end;

    T(pos+1) = cnd;  % only needed when all word occurrences are searched
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

