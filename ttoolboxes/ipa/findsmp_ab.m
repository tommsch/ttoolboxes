function [ oo, na, mb, rr_max ] = findsmp_ab( varargin );
% searches for an smp, generated by concatening two repeteated sequences,
% i.e. searches for smps of the form a^n * b^m
% [ oo, n, m ] = findsmp_ab( M, [G], a, b, [options] )
% If the eigenplane criterion of the ipa fails, then this function is guaranteed to find a better candidate
%
% Input:
%   M       cell array of square matrices
%   a       column vector
%   b       column vector
%
% Options:
%   'verbose',val
%   'ia',value       1, 2, or 3 element integer vector, default=[0 400 2000], 
%   'ib',value       1, 2, or 3 element integer vector, default=[0 400 2000], determines [minimum, maximum and hard-maximum values of n
%
% Note:
%   The 'a'/'b' vectore determines [minimum, maximum and hard-maximum] values of n.
%   The function may search for sequences longer than 'maximum' if some conditions are met,
%   but it always stops when the sequence is longer than hard-maximum
%
% Output:
%   oo      smp-candidate
%   n       the value `n` from above 
%   m       the value `m` from above
%   r       the averaged spectral radius of the candidate
%
% Written by: tommsch, 2022-02-16
    
    % parse input
    [ M, G, aa, bb, ia, ib, opt ] = parse_input( varargin{:} );
    

    % find smp    
    rr_max = -inf;
    n = 0;
    while( true );
        n = n + 1;
        if( n > max( min( ia.max, ia.max_max ), min( ib.max, ib.max_max ) ) );
            break; end;
        for m = 0:n;
            for s = 1:2;
                switch s;
                    case 1; na = n; mb = m;
                    case 2; na = m; mb = n;
                    otherwise; fatal_error; end;
                if( n == m && s == 2 || ...
                    na > min( ia.max, ia.max_max ) || ...
                    mb > min( ib.max, ib.max_max ) || ...
                    na == 0 && mb > 1 || ...
                    na > 1 && mb == 0 ...
                  );
                    continue; end;
                na = na + ia.offset;
                mb = mb + ib.offset;
                %if( ~mod(ia, 100) );
                %    vprintf( '\nia: %i ', ia, 'imp',[1 verbose] ); end;
                oo = [repmat( aa, [1 na] ) repmat( bb, [1 mb] )];
                oo = reducelength( oo, 1 );
                if( ~isvalidordering( G, oo ) );
                    continue; end;
                if( opt.verbose >= 2 );
                    fprintf( '.' ); end;
                MM = buildproduct_fast( M, oo );
                scale = 1;
                %if( any(~isfinite(MM(:))) );
                %    [ ~, MM, scale ] = buildproduct( M, oo, 'scale',true ); end;
                rr = (scale*rho( MM ))^(1/numel( oo ));
                if( isfinite(rr) && rr > rr_max );
                    printcand( rr, na, mb, aa, bb, opt.verbose );
                    na_max = na;
                    mb_max = mb;
                    oo_max = oo';
                    rr_max = rr;
                    ia.max = max( ia.max, ceil(1.1*na + 5) );
                    ib.max = max( ib.max, ceil(1.1*mb + 5) );
                    end; end; end; end;
    
    
    % post process
    oo = oo_max;
    na = na_max;
    mb = mb_max;
    if( opt.verbose >= 1 );
        fprintf( '\n' ); end;
    %printcand( rr_max, na, mb, aa, bb, verbose );

end

function [ M, G, aa, bb, ia, ib, opt ] = parse_input( varargin );
    [ opt.verbose, varargin ] = parsem( {'verbose','v'}, varargin, 1 );
    [ opt.ia, varargin ] = parsem( {'a','ia'}, varargin, [0 100 200] );
    [ opt.ib, varargin ] = parsem( {'b','ib'}, varargin, [0 100 200] );
    
    assert( numel(opt.ia) >= 1 && numel(opt.ib) >= 1, 'findsmp:input', 'Arguments ''ia'' and ''ib'' must be vectors of 1 to 3 elements.' );
    
    ia_start = opt.ia(1);
    ib_start = opt.ib(1);
    assert( ia_start  >= 0 && ib_start  >= 0, 'findsmp:input', 'First element of ''ia'' and ''ib'' must be greater equal 0.' );
    ia.max = tifh( numel(opt.ia) >= 2, @() opt.ia(2), @() 2*ia_start + 5 );
    ib.max = tifh( numel(opt.ib) >= 2, @() opt.ib(2), @() 2*ib_start + 5 );
    assert( ia.max >= ia_start && ib.max >= ib_start, 'findsmp:input', 'Second element of ''ia'' and ''ib'' must be greater equal the first element.' );
    
    ia.max_max = tifh( numel(opt.ia) >= 3, @() opt.ia(3), @() 2*ia.max + 5 );
    ib.max_max = tifh( numel(opt.ib) >= 3, @() opt.ib(3), @() 2*ib.max + 5 );
    assert( ia.max_max >= ia.max && ib.max_max >= ib.max, 'findsmp:input', 'Third element of ''ia'' and ''ib'' must be greater equal the second element.' );

    aa = varargin{end-1};
    bb = varargin{end};
    varargin(end-1:end) = [];

    M = varargin{1};
    varargin(1) = [];

    G = [];
    if( numel(varargin) >= 1 )
        if( ismatrix(varargin{1}) && issquare(varargin{1}) && size(varargin{1}, 1) == numel(M) || ...
                isempty(varargin{1}) ...
          );
            G = varargin{1};
            varargin(1) = []; end; end;

    parsem( varargin, 'test' );

    assert( isvector(aa) && isvector(bb), 'findsmp:input', 'Inputs oa and ob must be vectors' );
    assert( max([aa(:);bb(:)]) <= numel( M ) && ...
            min([aa(:);bb(:)]) >= 0, 'findsmp:input', 'Inputs oa or ob out of range.' ); 
    ia.offset = ia_start;
    ia.max = ia.max - ia.offset;
    ia.max_max = ia.max_max - ia.offset;
    ib.offset = ib_start;
    ib.max = ib.max - ib.offset;
    ib.max_max = ib.max_max - ib.offset;

    if( iscolumn(aa) );
        aa = aa.'; end;
    if( iscolumn(bb) );
        bb = bb.'; end;
    aa(aa == 0) = [];
    bb(bb == 0) = [];
end

function [] = printcand( rr, ia, ib, oa, ob, verbose );
    if( verbose >= 1 );
        vprintf( '\nCandidate: rho = %17.15g, oo = [a^%i b^%i] = %r  ', ...
                    rr, ia, ib, ...
                    [repmat( oa, [1 ia] ) repmat( ob, [1 ib] )] ); end;
end

function [ ret ] = isvalidordering( GG, oo )
% returns true   if ordering is admissible
    if( isempty(GG) );
        ret = true;
        return; end    
    ret = true;
    for i = 1:numel( oo ) - 1;
        if( ~GG(oo(i), oo(i+1)) );
            ret = false;
            break; end; end;
    if( ~GG(oo(end), oo(1)) );
        ret = false; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
