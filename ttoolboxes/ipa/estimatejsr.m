function [ JSR ] = estimatejsr( MM, GG, minmax );  
% [ JSR ] = estimatejsr( M, G, minmax );
% Rough estimate of the joint spectral radius.
% Input
%   M       cell array of matrices
%   G       graph of admissible products
%   minmax  eiter 'min' or 'max', default = 'max', Whether to estimate jsr or lsr
%
% Output
%   JSR     interval containing the JSR
%
% E.g.: estimatejsr( {2, 3} )
%
% See also: ipa, findsmp
%
% Written by tommsch, 2018

    if( nargin <= 2 || isempty(minmax) );
        minmax = 'max'; end;
    if( nargin <= 1 );
        GG = []; end;

    d = clamp( ceil(log(1000)/log(size(MM, 2))), 3, 10 );
    if( ~ipa_isgraph(GG) );
        GG = []; end;
    if( ~isempty(GG) );
        d = numel( GG ); end;
    [~, ~, info] = findsmp( MM, 'graph',GG, minmax, 'maxsmpdepth',d, 'delta',.8, 'verbose',-1, 'maxtime',0, 'sym',0 );
    JSR = info.jsrbound;
    if( isempty(GG) && isanyof(minmax, {'max','jsr'}) );
        % XX missing
        val = max( cellfun( @norm, MM ) ); 
        JSR( 2 ) = min( JSR(2), val ); end;

end 

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
