function [ ret ] = isgraph_yields_invariant_polytope( G );
% tests whether the ipa algorithm will yield an invariant polytope w.r.t this grpah
% [ ret ] = isgraph_yields_invariant_polytope( G );
% 
% The graph must have the property, that: 
% If a matrix A going to component C, then each matrix starting from C must be admissible after matrix A
%
% Example: 
%   isgraph_yields_invariant_polytope( [0 1 0 0;1 0 0 1;0 0 0 1;0 1 1 0] );  % yields false
%
% Written by: tommsch, 2023-05-28

    ret = true;
    fromto = graph_precomponent( G );
    J = size( G, 1 );
    for j = 1:J;
        to = fromto( 2, j );  % where Aj goes to
        all_in_to = fromto(1,:) == to;
        if( any(xor( G(j,:), all_in_to )) );
            ret = false;
            return; end; end;
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
