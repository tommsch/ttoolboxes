classdef ipa_errorcode_enum < int16
    % Enum class for errorcodes of ipa algorithmus
    % Since Octave does not support enums, functions are used instead

    % negative values mean successfull termination
    % positive values mean bad termination
    % zero means unset errorcode
    
    enumeration
        NOWORKER                              ( ipa_errorcode.NOWORKER )
        VALIDATEUPPERBOUND                    ( ipa_errorcode.VALIDATEUPPERBOUND );
        VALIDATEUPPERBOUNDNORM                ( ipa_errorcode.VALIDATEUPPERBOUNDNORM );
        VALIDATELOWERBOUND                    ( ipa_errorcode.VALIDATELOWERBOUND );
        EXACTVALUEFOUNDDURINGBALANCING        ( ipa_errorcode.EXACTVALUEFOUNDDURINGBALANCING );
        NOERROR_INVARIANTSUBSPACE             ( ipa_errorcode.NOERROR_INVARIANTSUBSPACE );
        NOERROR                               ( ipa_errorcode.NOERROR );
        NOERROR_APPROX                        ( ipa_errorcode.NOERROR_APPROX );
        ZEROJSR                               ( ipa_errorcode.ZEROJSR );
        NOERROR_TRIVIAL                       ( ipa_errorcode.NOERROR_TRIVIAL );
        UNSETERROR                            ( ipa_errorcode.UNSETERROR );
        
        DRYRUN                                ( ipa_errorcode.DRYRUN );
        NOCANDIDATEFOUND                      ( ipa_errorcode.NOCANDIDATEFOUND );
        CANDIDATEISNOSMP                      ( ipa_errorcode.CANDIDATEISNOSMP );
        NOBALANCINGVECTORFOUND                ( ipa_errorcode.NOBALANCINGVECTORFOUND );
        BETTERORDERINGFOUND                   ( ipa_errorcode.BETTERORDERINGFOUND );
        MAXTIMEREACHED                        ( ipa_errorcode.MAXTIMEREACHED );
        MAXTREETIMEREACHED                    ( ipa_errorcode.MAXTREETIMEREACHED );
        MAXSTEPNUMBERREACHED                  ( ipa_errorcode.MAXSTEPNUMBERREACHED );
        MAXNUM_VERTEXREACHED                  ( ipa_errorcode.MAXNUM_VERTEXREACHED );
        MAXREMAININGVERTEXREACHED             ( ipa_errorcode.MAXREMAININGVERTEXREACHED );
        MAXTREEDEPTHREACHED                   ( ipa_errorcode.MAXTREEDEPTHREACHED );
        TESTEIGENPLANE                        ( ipa_errorcode.TESTEIGENPLANE );
        MAXITERATION                          ( ipa_errorcode.MAXITERATION );
        IS_NOT_A_KONE                         ( ipa_errorcode.IS_NOT_A_KONE );
        NOINVARIANTKONE                       ( ipa_errorcode.NOINVARIANTKONE );
        LIKELYNOINVARIANTKONE                 ( ipa_errorcode.LIKELYNOINVARIANTKONE );
        COMPLEXKONE                           ( ipa_errorcode.COMPLEXKONE );
        NOINVARIANTKONE_DUALTEST              ( ipa_errorcode.NOINVARIANTKONE_DUALTEST );
        
        TOOMUCHCANDIDATE                      ( ipa_errorcode.TOOMUCHCANDIDATE );
        ERROR_INVARIANTSUBSPACE               ( ipa_errorcode.ERROR_INVARIANTSUBSPACE );
        NONZEROJSR                            ( ipa_errorcode.NONZEROJSR );
        OUTOFMEMORY                           ( ipa_errorcode.OUTOFMEMORY );
        DUPLICATELEADINGEIGENVECTOR           ( ipa_errorcode.DUPLICATELEADINGEIGENVECTOR );
        MAXONENORM                            ( ipa_errorcode.MAXONENORM );
        ADMISSIBLEALGORITHM                   ( ipa_errorcode.ADMISSIBLEALGORITHM );
        MAXNORMERR                            ( ipa_errorcode.MAXNORMERR );
        STALENORM                             ( ipa_errorcode.STALENORM );
        UNEXPECTED_COMPLEX_EIGENVECTOR        ( ipa_errorcode.UNEXPECTED_COMPLEX_EIGENVECTOR );
        UNEXPECTED_NONNONNEGATIVE_VECTOR      ( ipa_errorcode.UNEXPECTED_NONNONNEGATIVE_VECTOR );
        NONNEGLECTABLE_COMPLEX_PART           ( ipa_errorcode.NONNEGLECTABLE_COMPLEX_PART );
        
        UNDERFLOW                             ( ipa_errorcode.UNDERFLOW );
        UNKOWNERROR                           ( ipa_errorcode.UNKOWNERROR );
        INPUTERROR                            ( ipa_errorcode.INPUTERROR );
    end
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
