function [ vv, oo, idx ] = ipa_get_newly_added_vertices( bv );
% Returns all vertices which were added to the cyclictree in the last iteration
% [ vv, oo, idx ] = ipa_get_newly_added_vertices( bv );
%
% Input:
%   bv      the blockvar struct
%
% Output:
%   vv      cell array of matrices of column vectors
%   oo      cell array of column vectors, orderings for vectors in vv
%   idx     cell array of logical vectors
%
% See also: merge_oo
%
% Written by: tommsch, 2024-10-17

    num_trees = numel( bv.cyclictree.smpflag );
    idx = cell( 1, num_trees );
    vv = cell( 1, num_trees );
    oo = cell( 1, num_trees );
    last_iter = numel( bv.cyclictree.num_added{1} );
    for tree_idx = 1:num_trees;
        idx{tree_idx} = bv.cyclictree.added_in{tree_idx} == last_iter;
        oo{tree_idx} = bv.cyclictree.oo{tree_idx}(:,idx{tree_idx});
        vv{tree_idx} = bv.cyclictree.VV{tree_idx}(:,idx{tree_idx}); end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

