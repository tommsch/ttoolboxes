function [ varargout ] = gallery_matrix( varargin );
% wrapper function for gallery_matrixset with 'N' == 1
    varargout = cell( 1, max(1, nargout) );
    if( nargin == 1 && isscalar(varargin{1}) && isnumeric(varargin{1}) );
        [varargout{:}] = gallery_matrixset( 'N',1, 'nocell',1, 'dim', varargin{1} );
    else;
        [varargout{:}] = gallery_matrixset( 'N',1, 'nocell',1, varargin{:} ); end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
