function [ varargout ] = tgallery( varargin );
% wrapper function for gallery_matrixset
    n = nargout( 'gallery_matrixset' );
    varargout = cell( 1, max(nargout, n) );
    [varargout{:}] = gallery_matrixset( varargin{:} );
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
