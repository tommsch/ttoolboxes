function [ v0, v0s, mult, cpflag, Mret_oret, ind, d ] = leadingeigenvector( varargin );
% [ v0, v0s, multiplicity, cpflag, Mret, ind, d ] = leadingeigenvector ( M, [options] );    
% [ v0, v0s, multiplicity, cpflag, oret, ind, d ] = leadingeigenvector ( M, oo, [options] );    
% [ v0, v0s, multiplicity, cpflag, oret, ind, d ] = leadingeigenvector ( v, d, [options] );    
% Returns all leading eigenvectors of matrices.
% The leading eigenvectors of a matrix are those, which correspond to a eigenvalue with highest absolute value. 
% There can be more leading eigenvectors to one matrix.
%
% Input:
%   M                   matrix or row-cell of matrices
%   oo                  cell array of orderings, The leading eigenvectors of all products M{oo{1}}(end)*...*M{oo{1}}(1), M{oo{end}}(end)*...*M{oo{end}}(1) are computed
%   v                   matrix of eigenvectors, it is not possible to pass a cell array
%   d                   vector of eigenvalues, it is not possible to pass a cell array
%
% Options:
%   'sym',val                           casts input prior computation
%                                          -1: no change
%                                           0: cast to double
%                                           1|3: cast to vpa
%                                           2|4: cast to sym
%   'double'                            bool, if given, output is cast to double
%   'epsequal',val                      if |x-y| < epsequal*max{x,y} then x == y. Default=automatically determined
%   'multipleeigenvector',bool          default = true, determines how multiple leading eigenvalues are handled.
%                                           false: returns only one leading eigenvector per matrix, complex conjugate eigenvectors are handled separetly by option 'simpleconjugate'
%                                           true: returns all leading eigenvectors per matrix, but with same index
%   'complexeigenvector',int            discards all complex eigenvectors depending on val
%                                           0: complex eigenvectors are kept
%                                           1: complex eigenvectors are removed, if there is also a real eigenvector corresponding to the same product
%                                           2: (default) complex leading eigenvectors are removed, if there is at least one real leading eigenvector among all products
%                                           3: same as 2
%                                           4: complex eigenvectors are removed always
%   'simpleconjugate'                   if given, complex conjugate eigenvector/eigenvalue pairs are counted with multiplicity one
%   'verbose',val                       sets the verbose level
%   'cycle',val                         default=1, Only applicable if oo is given. Also returns the eigenvalues of all cycles.
%   'repetition',val                    default=0, Only applicable if oo is given. Also returns the eigenvalues of powers of the orderings, if lcm(lengths)<val.
%                                       Sensible value (apart from 0): 50
%   'positive_eigenvector'               if given, only fully positive eigenvectors are returned
%   'positive_eigenvalue'                if given, only eigenvectors to positive eigenvalues are returned
%   'nocell'                            (experimental) if given, only one leading eigenvector is returned (as ac vector, instead of a cell array). An error is thrown when not exactly one leading eigenvector was computed
%   'normalization',val                 something, default = 2, determines how the returned vectors are normalized
%                                           1/'1': 1-norm
%                                           2/'2'/'max'/'min': 2-norm
%                                           inf/'inf': inf-norm
%                                           'kone': inf-norm, and v0s will not get computed
%   'nonnegative_eigenvector'               if given, only fully nonnegative are returned are returned
%   'nonnegative_eigenvalue'                if given, only eigenvectors to nonnegative eigenvalues
%   'left'|'right'                      (experimental), default = 'right', if given, the left/right eigenvectors are returned
%   'row'|'column'                      (experimental), if given, the output vectors are always row|columns vectors
%   
% Output:
%   v0                  cell array of column vectors, leading eigenvector. v0{i} is normalized to norm(v0, 2) == 1, v0(find( v0 )) > 0
%   v0s{i}              cell array of column vectors, leading eigenvector of complex transposed M{i}/product (eigenplane of M{i}/product). v0s{i} is normalized to <v0{i},v0s{i} >= 1, where v0 is the standard scalar product.
%   multiplicity        row vector, multiplicity of the leading eigenvalue. This is the number of eigenvalues equal in modulus to the largest eigenvalue
%   cpflag              row vector of bools, 0 if v0 is real, 1 if all v0 are complex, 2 if some v0 are complex (this is helpful since matlab has problems with symbolics)
%   Mret                cell array of matrices, matrix or product of matrices with ordering oret{i}
%   oret                cell array of orderings, ordering of matrix product
%   ind                 row vector of indices, tells to which original ordering/matrix the entry corresponds, i.e. reducelength(oret(i)) = oo(ind(i)) 
%   d                   row vector, corresponding eigenvalues
%
% Remark:
%   Function throws an error if option 3 is used
%
%  E.g.: [M,v0,v0s,mult,ind] = leadingeigenvector( {[1 1 ; 0 1],[0 0; 1 0]},{[1 2]',[1]'} )
%
% Written by: tommsch, 2018

%               tommsch, 2019-05-27,    Option '3' implemented
%               tommsch, 2019-06-29,    Fixed bug in option '3'
%               tommsch, 2020-04-19,    Behaviour change of option 'cycle'
%               tommsch, 2020-06-22,    Instead of oorig, the index to oorig is returned as fifth argument
%               tommsch, 2020-07-01,    Improvement in the identification of linearly dependendent eigenvectors
%               tommsch, 2020-07-15,    removed option '3' since it constitutes a bug in the interface. The user expects that the function returns eigenvectors, which is not the case with option 3 in general.
%                                       Added option 'simpleconjugate'
%                                       Removed option 'double', added option 'sym'
%               tommsch, 2021-06-17,    Bugfix due to behaviourchange in Matlab R2020a
%               tommsch, 2021-10-08,    Added output argument cpflag
%               tommsch, 2021-12-03,    Behaviour change: Added order of return arguments
%                                       if only ordering is passed, it can also be passed as a column vector
%               tommsch, 2022-02-10,    Added options 'nonnegative' and 'positive'
%               tommsch, 2023-04-11,    If input matrix is non-negative, the returned leading eigenvector is always real and non-negative
%               tommsch, 2023-09-04,    Better handling of nearly non-negative and non-real eigenvectors
%               tommsch, 2024-10-07,    Added option: 'normalization', 'complexeigenvector'==3 now has same meaning as 'complexeigenvector'==2
%               tommsch, 2024-10-22,    Added output `d`, New option 'positive_leading_eigenvalue'
% Changelog:    tommsch, 2024-11-17,    More information returned via `cpflag`

    [M, oo, opt] = parse_input( varargin{:} );
    
    [Mret_oret, v0, v0s] = deal( {} );
    ind = [];
    mult = [];
    cpflag = [];
    d = [];
    
    for i = 1:opt.noo;
        switch opt.mode;
            case 'ordering';
                Mi = buildproduct_fast( M, oo{1,i} );
                if( any( abs(Mi(:)) > 1e12 ) || ...
                    any( ~isfinite(Mi(:)) ) ...
                  );
                    [~, Mi, scalei] = buildproduct( M, oo{1,i}, 'scale',1 ); end;  %#ok<ASGLU>
                [v0new, v0snew, multnew, cpflagnew, dnew] = leadingeigenvector_worker( Mi, [], opt );
                Mret_oret = [Mret_oret repcell( oo{1,i}, size(v0new) )];  %#ok<AGROW>
                ind = [ind repmat(oo{2,i}, size(v0new))];  %#ok<AGROW>
            case 'vec_ev';
                [v0new, v0snew, multnew, cpflagnew, dnew] = leadingeigenvector_worker( M{i}, oo, opt );
                Mret_oret = [Mret_oret repcell( M{i}, size(v0new) )];  %#ok<AGROW>
                ind = [ind repmat(i, size(v0new) )];  %#ok<AGROW>
            case 'matrix';
                [v0new, v0snew, multnew, cpflagnew, dnew] = leadingeigenvector_worker( M{i}, [], opt );
                Mret_oret = [Mret_oret repcell( M{i}, size(v0new) )];  %#ok<AGROW>
                ind = [ind repmat(i, size(v0new) )];  %#ok<AGROW>
            otherwise;
                fatal_error; end;
        if( ~isequal( opt.mode, 'vec_ev') && ...
            ~isempty( v0snew ) && ...
            ( anym( isnan([v0snew{:}]) ) || anym( isAlways(abs([v0snew{:}]) > 1e14, 'Unknown','false') ) ) ...
          );
            vprintf( 'Orthogonal eigenplane and eigenvector (left and right eigenvector) occured.\n', 'imp',[1 opt.verbose], 'once',1 ); end;
        
        v0 = [v0 v0new];  %#ok<AGROW> 
        v0s = [v0s v0snew];  %#ok<AGROW>
        mult = [mult multnew];  %#ok<AGROW>
        cpflag = [cpflag cpflagnew];  %#ok<AGROW>
        d = [d dnew]; end;  %#ok<AGROW>
    
    [v0, v0s, mult, cpflag, Mret_oret, ind, d] = postprocess( v0, v0s, mult, cpflag, Mret_oret, ind, d, opt );

end


function [ M, oo, opt ] = parse_input( varargin );

    [opt.leftright, varargin] =              parsem( {'left-right'}, varargin, 'r' );
    [opt.complexeigenvector, varargin] =     parsem( {'complex_eigenvector','cp','cpflag'}, varargin, 2 );
    if( isequal(opt.complexeigenvector, 3) );
        opt.complexeigenvector = 2; end;
    [opt.cycle, varargin] =                  parsem( {'cycle','c'}, varargin, 'default', [] );
    [opt.epsilon, varargin] =                parsem( {'epsequal','epsilon','eps'}, varargin, [] );
    [opt.multipleeigenvector, varargin] =    parsem( {'multiple_eigenvector','multiple_leading_eigenvector','multiple','mult'}, varargin, [] );
    [opt.nonnegativeeigenvector, varargin] = parsem( {'nonnegative_eigenvector','nonnegative_leading_eigenvector','nonnegative','nonneg'}, varargin );
    [opt.nonnegativeeigenvalue, varargin] =  parsem( {'nonnegative_eigenvalue','nonnegative_leading_eigenvalue'}, varargin );
    [opt.normalization, varargin] =          parsem( {'normalization'}, varargin, 2 );
    [opt.positiveeigenvector, varargin] =    parsem( {'positive_eigenvector','positive_leading_eigenvector','positive','pos'}, varargin );
    [opt.positiveeigenvalue, varargin] =     parsem( {'positive_eigenvalue','positive_leading_eigenvalue'}, varargin );
    [opt.repetition, varargin] =             parsem( {'repetition','rep','r','power'}, varargin, 0 );
    [opt.simpleconjugate, varargin] =        parsem( {'simple_conjugate','conjugate','conj'}, varargin );
    [opt.symflag, varargin] =                parsem( {'sym','symflag'}, varargin, -1 );
    [opt.double, varargin] =                 parsem( {'double','double_output','output_double','output_as_double'}, varargin );
    [opt.verbose, varargin] =                parsem( {'verbose','v'}, varargin, 1 );
    [opt.nocell, varargin] =                 parsem( {'nocell'}, varargin );
    [~, varargin, opt.leftright] =           parsem( {'right','left'}, varargin );
    [~, varargin, opt.rowcol] =              parsem( {'rowcolauto','column','columnvector','row','rowvector'}, varargin );

    if( numel(varargin) == 1 );
        % only Matrix given
        opt.mode = 'matrix';
        M = varargin{1};
        oo = [];
        opt.noo = tif( iscell(M), numel( M ), 1 );
        varargin(1) = [];
    elseif( numel(varargin) == 2 && ...
            ~iscell(varargin{1}) && ismatrix(varargin{1}) &&...
            ~iscell(varargin{2}) && (isvector(varargin{2}) || isdiag(varargin{2})) ...
          );
        % eigenvector/eigenvalue pair given
        opt.mode = 'vec_ev';;
        M = varargin{1};
        oo = varargin{2};
        opt.noo = 1; 
        varargin(1:2) = [];
    elseif( numel(varargin) == 2 );
        % set of matrices and ordering given, for computing the matrices in question
        opt.mode = 'ordering';
        M = varargin{1};
        oo = varargin{2}; 
        if( ~iscell(oo) && size(oo, 2) == 1 );
            oo = {oo}; end;
        oo = [oo; num2cell( 1:numel(oo) )];  % copy the original ordering to the second line
        if( opt.cycle );
            oo = makecycle( oo ); end;
        if( opt.repetition );
            oo = makerepetition( oo, opt.repetition ); end;
        opt.noo = size( oo, 2 );
        varargin(1:2) = [];
    else;
        error( 'leadingeigenvector:input', 'Cannot parse input' ); end;
    
    M = tocell( M );
    if( opt.symflag == 0 );
        for j = 1:numel( M );
            try;
                M{j} = double( M{j} );
            catch me;  %#ok<NASGU>
                warning( 'leadingeigenvector:sym', 'cast to double failed for matrix %i', j ); end; end;
    elseif( opt.symflag == 1 || opt.symflag == 3 );
        for j = 1:numel( M )
            try;
                M{j} = vpa( M{j} );
            catch me;  %#ok<NASGU>
                warning( 'leadingeigenvector:sym', 'cast to vpa failed for matrix %i', j ); end; end;
    elseif( opt.symflag == 2 || opt.symflag == 4 );
        for j = 1:numel( M )
             try;
                 M{j} = simplify( sym( M{j} ),'Seconds',5 );
             catch me;  %#ok<NASGU>
                warning( 'leadingeigenvector:sym', 'cast to sym failed for matrix %i', j ); end; end; end;
    
    switch opt.leftright;
        case {'right','r'};
            % do nothing
        case {'left','l'};
            for j = 1:numel( M );
                M{j} = M{j}.'; end;
        otherwise;
            error( 'leadingeigenvector:args', 'Allowed values for option ''leftright'' are ''l'' and ''r''.' ); end;
       
    if( isempty(opt.normalization) );
        opt.normalization = 2; end;

    switch lower( opt.normalization );
        case {1,'1'};
            opt.normalization = 1;
            opt.computev0s = true;
        case {2,'2','min','max'};
            opt.normalization = 2;
            opt.computev0s = true;
        case {inf,'inf'};
            opt.normalization = inf;
            opt.computev0s = true;
        case {'kone','k','cone'};
            opt.normalization = inf;
            opt.computev0s = false;
        otherwise;
            error( 'leadingeigenvector:normalization', 'Wrong value for option ''normalization'' is given.' ); end;
        
    if( isempty(opt.cycle) ); opt.cycle = false; end;
    if( isempty(opt.multipleeigenvector) ); opt.multipleeigenvector = true; end;

    parsem( varargin, 'test' ); 

end

function [ v0, v0s, mult, cpflag, Mret_oret, ind, d ] = postprocess( v0, v0s, mult, cpflag, Mret_oret, ind, d, opt );
    % sort out solutions, corresponding to given options
    [v0, d, badidx] = sort_out( v0, d, opt );
    v0(badidx)        = [];
    v0s(badidx)       = []; 
    cpflag(badidx)    = [];
    mult(badidx)      = [];
    Mret_oret(badidx) = []; 
    ind(:,badidx)     = [];
    d(badidx)         = [];
    
    if( opt.double );
        for i = 1:numel( v0 );
            v0{i} = double( v0{i} ); end;
        for i = 1:numel( v0s );
            v0s{i} = double( v0s{i} ); end;        
        for i = 1:numel( Mret_oret );
            Mret_oret{i} = double( Mret_oret{i} ); end; end;

    if( opt.nocell );
        assert( numel(v0) == 1, 'leadingeigenvector:nocell', 'Not exactly one leading eigenvector was computed. Number of eigenvectors: %i', numel(v0) );
        v0 = v0{1};
        v0s = v0s{1};
        Mret_oret = Mret_oret{1};
        ind = ind(:,1); end;

    if( opt.verbose >= 2 );
        vprintf( 'v0: \n%v\n', v0 );
        vprintf( 'v0s: \n%v\n', v0s ); end;
end

function [ v0, d, badidx ] = sort_out( v0, d, opt );
    badidx = [];
    if( opt.complexeigenvector == 2 && any(cellfun(@(x) all(isAlways( imag(x) == 0, 'Unknown','false' )), v0)) );  % using `isreal` does not work for symbolic root expressions
        for i = 1:numel( v0 );
            val = abs( imag( v0{i} ) );
            idx = isAlways( val > 0 );
            mx = max( val );
            if( isAlways( mx > 1000*eps ) );
                badidx = [badidx i];  %#ok<AGROW>
            elseif( isAlways( mx > 4*eps ) );
                 warning( 'leadingeigenvector:complex', 'Complex eigenvector has values with imaginary part as large as %i. I set them to zero.', mx );
                 v0{i}(idx) = real( v0{i}(idx) ); end; end; end;
    
    if( opt.positiveeigenvector );
        for i = 1:numel( v0 );
            if( isAlways( any(v0{i} <= 0 ) ) );
                badidx = [badidx i]; end; end; end;  %#ok<AGROW>

    if( opt.positiveeigenvalue );
        for i = 1:numel( d );
            idx = isAlways( any( d <= 0 ) );
            badidx = [badidx find( idx )]; end; end;  %#ok<AGROW>

    if( opt.nonnegativeeigenvector )
        for i = 1:numel( v0 );
            idx = isAlways( v0{i} < 0 );
            mn = min( v0{i}(idx) );
            if( isAlways( mn < -1000*eps ) );
                badidx = [badidx i];  %#ok<AGROW>
            elseif( isAlways( mn < -4*eps ) );
                warning( 'leadingeigenvector:nonnegative', 'Eigenvector has values as low as %i. I set them to zero.', mn );
                v0{i}(idx) = 0; end; end; end;

    if( opt.nonnegativeeigenvalue )
        idx_nonneg = isAlways( d < -1000*eps );
        badidx = [badidx find( idx_nonneg )];
        idx_pos = isAlways( d(~idx_nonneg) >= 0 );
        if( ~all(idx_pos) );
            d(~idx_pos) = [];
            warning( 'leadingeigenvector:nonnegative', 'Eigenvalue has values as low as -1000*eps. I set them to zero.' ); end; end;
        

    badidx = unique( badidx );
end

%%

function [ v0, v0s, mult, cpflag, d ] = leadingeigenvector_worker( M, d, opt );
% [M, v0, v0s] = leadingeigenvector_worker (M);      
% returns all leading eigenvectors of the matrices in M
% computes v0 und v0s for one matrix M
    if( isempty(d) );
        [v, d, vs] = compute_ev( M, opt );
    else;
        v = M;
        vs = zeros( size(v) );
        if( ismatrix(d) );
            d = diag( d ).'; end; end;
    opt.epsilon = set_epsilon( v, opt );
    [v, d, vs] = select_largest_eigenvalues( v, d, vs, opt.epsilon );
    [v, d, vs, cpflag] = remove_complex_conjugates( M, v, d, vs, opt.simpleconjugate );
    [v, d, vs, cpflag, mult] = remove_linearly_dependent_eigenvectors( v, d, vs, cpflag, opt.epsilon );
    [v, d, vs, cpflag, mult] = handle_complex_case( v, d, vs, cpflag, mult, opt.complexeigenvector, opt.multipleeigenvector );
    v0 = num2cell( v, 1 );
    v0s = num2cell( vs, 1 );
    [v0, v0s] = normalize_vectors( v0, v0s, opt );
    %[v0] = realify_v0( v0 );  % I have no idea what this shall do.
    if( ~isequal(opt.mode, 'vec_ev') );
        test_eigenvectors( M, v0, d, opt ); end;
    [v0, v0s] = transpose_eigenvectors( v0, v0s, opt.leftright, opt.rowcol );
    d = d(:).';
end

function [ v, d, vs ] = compute_ev( M, opt );
    if( issym(M) );
        eig_m = memoize( @eig );
        switch opt.leftright;
            case 'right';
                [v, d, p] = eig_m( M );
                [vs, ds, ps] = eig_m( M' );
            case 'left';
                [v, d, p] = eig_m( M' );
                [vs, ds, ps] = eig_m( M );
            otherwise;
                fatal_error; end;
        
        d = diag( d );
        ds = diag( ds' );
        expect( isAlways( diag(d) - diag(ds) == 0 ), 'leadingeigenvector:leftright', 'Attention: Order of left and right eigenvectors is mixed up.' )
        rep = [diff(p) size(M, 1)-p(end)+1];
        
        %v_old = arrayfun( @(i) repmat(v(:,i),[1 rep(i)]), 1:numel(rep), 'UniformOutput',false );
        %vs_old = arrayfun( @(i) repmat(vs(:,i),[1 reps(i)]), 1:numel(reps), 'UniformOutput',false );
        val_v = cell( 1, numel(rep) );
        for i = 1:numel( rep );
            val_v{i} = repmat( v(:,i), [1 rep(i)] ); end;
        v = [val_v{:}];
        if( opt.computev0s );
            reps = [diff(ps) size(M, 1)-ps(end)+1];
            val_vs = cell( 1, numel(reps) );
            for i = 1:numel( reps );
                val_vs{i} = repmat( vs(:,i), [1 reps(i)] ); end;
            vs = [val_vs{:}];
        else;
            vs = nan( size(v) ); end;
        [~, idx]  = sort( double(d),  'descend', 'ComparisonMethod','abs' );
        [~, idxs] = sort( double(ds), 'descend', 'ComparisonMethod','abs' );
        d = d(idx);
        ds = ds(idxs);
        if( issym(d) );
            vpaflag = false;
            try;
                if( any(isSymType(d, 'vpa')) );
                    vpaflag = true; end;
            catch me;  %#ok<NASGU>
                if( isvpa(d) || isvpa(ds) );
                    vpaflag = true; end; end;
            val = norm( d - ds );
            if( vpaflag );
                expect( isAlways( val <= 4*10^-(vpa(digits)) ), 'leadingeigenvector:fatal', 'Error in eigenvalue computation. Difference of leading left and right eigenvalue (should be near zero) but is: %g', val );
            else;
                expect( isAlways( val == 0 ), 'leadingeigenvector:fatal', 'Error in eigenvalue computation. Difference of leading left and right eigenvalue (should be exactly zero) but is: %f', val ); end; end;
        v = v(:,idx);
        v = simplify( v, 'Second',5 );
        vs = vs(:,idxs);
        vs = simplify( vs, 'Second',5 );
        
    else;
        switch opt.leftright;
            case 'right';
                [v, d, vs] = eig( M );
            case 'left';
                [vs, d, v] = eig( M );
            otherwise;
                fatal_error; end;

        if( ~opt.computev0s );
            vs = nan( size(v) ); end; end; % each row is one vector
        
    if( ~isvector(d) );
        d = diag( d );  end;
    d = d(:).';
    
    % sanity check
    M_complexflag = any( isAlways( abs( imag(M(:) ) ) > 0 ) );
    if( opt.verbose >= 1 && ~M_complexflag );
        d_realflag  = isAlways( abs( imag( d ) ) < 1e-15 );
        v_realflag  = all( isAlways( abs( imag( v ) ) < 1e-15 ), 1 );
        vs_realflag = all( isAlways( abs( imag( vs ) ) < 1e-15 ), 1 );    
        if( ~all(isAlways(d_realflag == v_realflag)) || ...
            opt.computev0s && ~all(d_realflag == vs_realflag) );
            warning( 'leadingeigenvector:complex', 'Computed eigenvectors are complex, although correspdoning eigenvalues are not.\n  This may lead to wrong results (but not necessarily).\n  You may try to compute it symbolically.' ); end; end;
    assert( ~anym(isnan(v)), 'leadingeigenvector:nan', 'NaNs occured during computation of eigenvectors.' );        
end

function [ epsilon ] = set_epsilon( v, opt );
    if( ~isempty(opt.epsilon) );
        epsilon = opt.epsilon;
    elseif( ~issym(v) );
        epsilon = min( cond(v)*10e-12, 1e-7 ); 
    elseif( issym(v) );
        epsilon = 0; end;
end

function [ v, d, vs ] = select_largest_eigenvalues( v, d, vs, epsilon );
    % select largest eigenvalues
    val = max( abs(d) );
    if( val == 0 );
        idx = true( 1, numel(d) );
    else;
        idx1 = abs( d/val ) >= 1 - epsilon; 
        idx1 = find( isAlways( idx1 ) );
        [~, idx2] = max( abs(d) );
        idx = unique( [idx1(:); idx2(:)] )'; end;
    
    v = v(:,idx); 
    vs = vs(:,idx); 
    d = d(idx);
end

function [ v, d, vs, cpflag ] = remove_complex_conjugates( M, v, d, vs, simpleconjugate );
    vv = num2cell( v, 1 );
    nfo = matrixinfo( vv, 'real', 'cmirror','conjugate' );
    cpflag = any( nfo.conjugate, 1 ) & ~nfo.real | ~isreal( M );
    if( ~all(cpflag) );
        cpflag = cpflag*2; end;  % make cpflags with value 1 to flags with value 2
    if( simpleconjugate );
        badidx = cpflag & any( triu(nfo.cmirror & nfo.conjugate, 1), 1 );
        v(:,badidx) = [];
        vs(:,badidx) = [];
        d(badidx) = [];; end;
end

function [ v, d, vs, cpflag, mult ] = remove_linearly_dependent_eigenvectors( v, d, vs, cpflag, epsilon );
    idx = abs( null(v) ) > epsilon;  % get linearly dependent vectors
    if( isempty(idx) );
        idx = true( size(v,2), 0 ); 
    else;
        idx = isAlways( idx ); end;
    mult = sum( idx, 1 );  % get multiplicity of them
    idx_indep = find( ~any(idx,2) )';  % find linearly independent vectors
    [~, idx_dep] = max( idx, [], 1 );  % find first linearly dependent vector of each group
    idx = [idx_indep idx_dep];
    mult(1:numel( d )) = numel( d );
    cpflag = cpflag(idx);
    v = v(:,idx); 
    vs = vs(:,idx); 
    d = d(idx);
end

function [ v, d, vs, cpflag, mult ] = handle_complex_case( v, d, vs, cpflag, mult, complexeigenvector, multipleeigenvector );
    idx = true( size(d) );
    complexidx = any( imag(v) ~= 0 | imag(vs) ~= 0, 1 );
    if( complexeigenvector == 0 );
        % do not remove anything
    elseif( complexeigenvector == 1 || ...
            complexeigenvector == 2 ...
          );
        % remove if there are also real eigenvectors
        if( ~isequal(complexidx & idx, idx) );  % do not remove everything
            idx(complexidx) = false; end;

    elseif( complexeigenvector == 3 );
        error( 'leadingeigenvector:option3', 'Option <''complexeigenvector'', 3> is removed.' );
        
    elseif( complexeigenvector == 4 );  % remove all complex eigenvectors
        idx(complexidx) = false; end;
    
    switch( multipleeigenvector );
        case 0;
            idx(find( idx, 1 )+1:end) = 0; % if we only want to return one candidate, we choose the first
        case {1,2};
            % do nothing            
        otherwise;
            error( 'leadingeigenvector:input', 'wrong input' ); end;
    
    % choose leading eigenvectors
    v = v(:,idx);
    vs = vs(:,idx);
    d = d(idx);
    cpflag = cpflag(idx);
    mult = mult(idx);
end

function [ v, vs ] = normalize_vectors( v, vs, opt );
% I do not know why I added this intermediate computation with vp here. I disabled it for now.
% I also do not know why `tol` is not a constant value
    %vp = v;
    if( opt.positiveeigenvector || opt.nonnegativeeigenvector );
        tol = 4*eps;
    else;
        tol = 0; end;
    for i = 1:size( v, 2 );
        v{i} = makepositive( v{i}, tol );
        vp{i} = makepositive( v{i}, tol );  %#ok<AGROW>
        num_v = numel( evalc( 'v{i}' ) );
        num_vp = numel( evalc( 'vp{i}' ) );
        if( num_vp <= 4*num_v );
            v{i} = vp{i}; end;
        v{i} = normalizematrix( v{i}, 'colnorm', opt.normalization );
        v{i} = simplify( v{i}, 'Seconds',5 );
        end;

    if( opt.computev0s );
        vs = normalizematrix( vs, 'dotprod',v );
        vs = simplify( vs, 'Seconds',5 ); end;
    
end

function [ v0 ] = realify_v0( v0 );
%    % I have no idea what this shall do.
%    for i = 1:numel( v0 );
%        if( ~cpflag(i) );
%            v0{i} = real( v0{i} ); end; end;  % unfortunately, this does not help, Matlab does not apply real reliable
end

function test_eigenvectors( M, v0, d, opt );
    try;
        for i = 1:numel( v0 );
            err = norm( M*v0{i} - v0{i}*d(i), 1 );
            nM = max( 1, norm( double(M), 2 ) );
            if( double(err) > 1e-12 * nM );
                warning( 'leadingeigenvector:numerical', 'Computed eigenvector is not a leading eigenvector: || M*v0 - la*v0 || = %s, || M || = %s', err, norm(M) );
            elseif( ~isAlways(err <= nM*opt.epsilon) );
                warning( 'leadingeigenvector:numerical_maybe', 'Computed eigenvector may not be a leading eigenvector: || M*v0 - la*v0 || = %s, || M || = %s', err, norm(M) ); end; end;
    catch me;
        warning( 'leadingeigenvector:sym', 'Could not test whether computed vectors are indeed eigenvectors.\n This is most likely a Matlab bug.\n Thrown error id: %s', me.identifier ); end;
end

function [ v0, v0s ] = transpose_eigenvectors( v0, v0s, leftright, rowcol );
    % make right eigenvectors again to row vectors
    for i = 1:numel( v0 );
        switch leftright;
            case 'right';
                % do nothing
            case 'left';
                v0{i} = v0{i}';
                v0s{i} = v0s{i}';
            otherwise;
                error( 'leadingeigenvector:leftright', 'Wrong value for option ''leftright'' given.' ); end; end;

    for i = 1:numel( v0 );
        switch rowcol;
            case {'rowcolauto'};
                % do nothing
            case {'row','rowvector'};
                v0{i} = v0{i}(:).';
                v0s{i} = v0s{i}(:).';
            case {'col','column','columnvector'};
                v0{i} = v0{i}(:);
                v0s{i} = v0s{i}(:);
            otherwise;
                fatal_error; end; end;
end

function [ oout ] = makecycle( oin );
    oout = {};
    for i = 1:size( oin, 2 )
        for j = 0:(length( oin{1,i} )-1)
            oout{1,end+1} = circshift( oin{1,i}, j );  %#ok<AGROW>
            oout{2,end} = oin{2,i}; end; end;
end

function [ oout ] = makerepetition( oin, maxlength );
    if( isempty(oin) ); 
        oout = cell(2,0); 
        return; end;
    L = lcmm( unique(cellfun(@length,oin(1,:))) );
    if( L > maxlength ); 
        oout = oin; 
        return; end;
    oout = {};
    for i = 1:size( oin, 2 )
        o = oin{1,i};
        for j = 1:L
            if( j*length(o) <= L ); 
                if( isrow(o) ); 
                    oout{1,end+1} = repmat( o, [1,j] ).';  %#ok<AGROW>
                else; 
                    oout{1,end+1} = repmat( o, [j,1] ); end;  %#ok<AGROW>
                oout{2,end} = oin{2,i}; end; end; end;
end


function dummy; end  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
