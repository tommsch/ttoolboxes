function A = complexleadingmatrix( dim, ratflag )
% [ A ] = complexleadingmatrix( dim, ratflag )
% Constructs a matrix with complex leading eigenvalue
% Input:
%   dim         integer, non-negative dimension of the matrix
%   ratflag     logical, if given, a matrix with rational numbers is returned
% Output:
%   A           square matrix, matrix with complex leading eigenvalue
%
% Written by: tommsch, 2020-11-13
    if( nargin <= 1 || isempty(ratflag) );
        ratflag = false; end;
    assert( dim >= 2, 'complexleadingmatrix:dim', 'Value of `dim` must be greater equal 2.' );
    sz = [2 randi( 2, [1 dim-2 1] )];
    idx = find( cumsum(sz) >= dim, 1 );
    sz = sz(1:idx);
    if( sum(sz)>dim );
        sz(end) = 1; end;
    assert( sum(sz ) == dim, 'complexleadingmatrix:fatal', 'Programming error.' );
    block = cell( 1, numel(sz) );
    
    x = tif( ratflag, (randi(9000,1,2)-5000)/1000, randn(1,2) );
    block{1} = [x(1) x(2);-x(2) x(1)];
    nrm = norm(x);
    for i = 2:numel( sz );
        x = tif( ratflag, (randi(9000,1,sz(i))-5000)/1000, randn(1,sz(i)) );
        if( norm(x) >= nrm );
            x = x/norm(x)*nrm*round(rand*5040)/5040; end;
        if( sz(i) == 1 );
            block{i} = x;
        else;
            block{i} = [x(1) x(2);-x(2) x(1)]; end; end;
    A = blkdiag( block{:} );
    V = randn( dim );
    A = V\A*V;
    
%     [~,d] = eig( A );
%     if( ~isvector(d) );
%         d = diag( d ); end;
%     [~,idx] = max( abs(d) );
%     assert( ~isreal(d(idx)) && isreal(A),  'complexleadingmatrix:fatal', 'Programming error.' );
    
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
