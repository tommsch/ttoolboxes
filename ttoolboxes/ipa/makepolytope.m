function [ P, alg ] = makepolytope( M, varargin );
% constructs a polytope which approximates the unit ball of the set of matrices M
% [ P, alg ] = makepolytope( M, [options] );
%
% Input: 
%   M    set of matrices
%
% Options:
%   same options as for ipa()
%
% Output:
%   P       matrix, column vectors of the polytope
%   alg     ipa-ENUM, the corresponding type of the polytope
%
% Info: This is just a convenience wrapper for ipa
%
% Example: makepolytope( {[1 2;3 4],[2 3; 1 4]} )
%
% Written by: tommsch, 2021-10-12

%               2024-05-15, tommsch, Function is deprecated
% Changelog:


    warning( 'ttoolboxes:deprecated', 'This function is depracted and will be removed in a future release.' );
    [verbose, varargin] = parsem( {'verbose','v'}, varargin, 1 );
    if( ~iscell(M) );
        M = squeeze( num2cell(M,[1 2]) )'; end;
    
    [~, nfo] = ipa( M, 'maxiteration',1, 'balancing',-1, 'epssym',nan, 'invariantsubspace','none', 'v',verbose-1, varargin{:} );
    P = nfo.blockvar{1}.cyclictree.VV;
    P = [P{:}];
    alg = nfo.blockvar{1}.cyclictree.algorithm;
    nrm = polytopenorm( P, P, alg, 'v',verbose-1 );
    idx = nrm(2,:) > 1-5e-5;
    P = P(:,idx);

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
