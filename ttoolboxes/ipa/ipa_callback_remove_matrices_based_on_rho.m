function [ bv ] = ipa_callback_remove_matrices_based_on_rho( bv, rho_val );
    % rhocomp: 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % remove all vertices whos matrix products 
    % have spectral radius greater/less equal 1+10*eps  
    % If this callback is used, the ipa does not yield correct results anymore

    if( ~isfield(bv.cyclictree, 'ipa_callback_remove_matrices_based_on_rho' ) );
        bv.cyclictree.ipa_callback_remove_matrices_based_on_rho.warning_printed = true;
        warning( 'ipa:wrong_result', 'The callback ''ipa_callback_add_candidate'' is used.\n  The ipa cannot return reliable results anymore.\n  Even if the algorithm says it terminated correctly,\n  the algorithm may have failed.' );
        end;

    
    switch bv.param.minmax;
        case 'max'; idx = bv.cyclictree.rho{tree_idx} >= 1 + bv.param.epssym;
        case 'min'; idx = bv.cyclictree.rho{tree_idx} <= 1 - bv.param.epssym;
        case 'kone'; error( 'ipa:rhotest', 'The debug option ''rhocomp'' is not applicable to the kone-case.' );
        otherwise; fatal_error; end;
    if( any(idx) );
        bv.cyclictree.VV{tree_idx}(:,idx) = 0;
        bv.cyclictree.norm{tree_idx}(:,idx) = [ipa_constant.USELESS_NORMVALUE;ipa_constant.USELESS_NORMVALUE;ipa_constant.USELESS_NORMVALUE];  % set to a value which can be recognized
        bv.cyclictree.norm_est{tree_idx}(:,idx) = ipa_constant.USELESS_NORMVALUE;
        vprintf( 'Removed %i vertices from matrix products due to option ''rhocomp''.\n', nnz(idx), 'cpr','err', 'imp',[1 bv.param.verbose] ); end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
