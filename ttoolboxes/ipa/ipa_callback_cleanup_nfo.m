function [ var ] = ipa_callback_cleanup_nfo( varargin );
% [ var ] = ipa_callback_cleanup_nfo( var )
% Removes a lot of stuff from var, so that it has a smaller memory footprint
% This function is to be executed at position 'callback_end'
% This function can also be used to specify how much data shall be removed from the struct var
%       ipa_callback_cleanup_nfo( 1 )    ...   returns handle which removes some data
%       ipa_callback_cleanup_nfo( 2 )    ...   returns handle which removes more data
%
% Removes in particular: symbollic stuff, 
%                        VV/norm/has_children whenever the algorithm did not terminate successfully
%
% See also: ipa_execute_callback
%
% Written by: tommsch, 2024-10-01

    if( numel(varargin) == 0 );
        var = @(var_in) ipa_callback_cleanup_nfo( var_in, 1 );
        return;
    elseif( numel(varargin) == 1 && ~isstruct(varargin{1}) && isreal(varargin{1}) && isscalar(varargin{1}) );
        var = @(var_in) ipa_callback_cleanup_nfo( var_in, varargin{1} );
        return; end;

    
    if( numel(varargin) <= 1 || isempty(varargin{2}) );
        tidyness = 1;
    else;
        tidyness = varargin{2}; end;
    var = varargin{1};

    for i = 1:numel( var.blockvar );
        if( isanyof(var.blockvar{i}.log.errorcode, {ipa_errorcode.MAXNUM_VERTEXREACHED, ipa_errorcode.MAXTIMEREACHED, ipa_errorcode.MAXITERATION}) );
            try; var.blockvar{i}.cyclictree.VV = []; end;
            try; var.blockvar{i}.cyclictree.norm = []; end;
            try; var.blockvar{i}.cyclictree.has_children = []; end;
            try; var.blockvar{i}.cyclictree.livingvertex = []; end;
            end;
        if( tidyness >= 2 );
            try; var.blockvar{i}.cyclictree = rmfield( var.blockvar{i}.cyclictree, 'epssym' ); end;
            try; var.blockvar{i}.cyclictree = rmfield( var.blockvar{i}.cyclictree, 'Mt_sym' ); end;
            try; var.blockvar{i}.cyclictree = rmfield( var.blockvar{i}.cyclictree, 'v0_sym' ); end;
            try; var.blockvar{i}.cyclictree = rmfield( var.blockvar{i}.cyclictree, 'v0s_sym' ); end;
            try; var.blockvar{i} = rmfield( var.blockvar{i}, 'param' ); end;
            end;
        
        try; var.blockvar{i}.cyclictree = rmfield( var.blockvar{i}.cyclictree, 'added_in' ); end;
        try; var.blockvar{i}.cyclictree = rmfield( var.blockvar{i}.cyclictree, 'balancingalpha' ); end;
        try; var.blockvar{i}.cyclictree = rmfield( var.blockvar{i}.cyclictree, 'balancingvector' ); end;
        %try; var.blockvar{i}.cyclictree = rmfield( var.blockvar{i}.cyclictree, 'component_idx' ); end;
        try; var.blockvar{i}.cyclictree = rmfield( var.blockvar{i}.cyclictree, 'd' ); end;
        try; var.blockvar{i}.cyclictree = rmfield( var.blockvar{i}.cyclictree, 'delta' ); end;
        try; var.blockvar{i}.cyclictree = rmfield( var.blockvar{i}.cyclictree, 'graph' ); end;
        try; var.blockvar{i}.cyclictree = rmfield( var.blockvar{i}.cyclictree, 'ipa_callback_add_candidate' ); end;
        try; var.blockvar{i}.cyclictree = rmfield( var.blockvar{i}.cyclictree, 'norm_est' ); end;
        try; var.blockvar{i}.cyclictree = rmfield( var.blockvar{i}.cyclictree, 'norm_lvl' ); end;
        try; var.blockvar{i}.cyclictree = rmfield( var.blockvar{i}.cyclictree, 'norm_parent' ); end;
        try; var.blockvar{i}.cyclictree = rmfield( var.blockvar{i}.cyclictree, 'num_added' ); end;
        %try; var.blockvar{i}.cyclictree = rmfield( var.blockvar{i}.cyclictree, 'num_component' ); end;
        try; var.blockvar{i}.cyclictree = rmfield( var.blockvar{i}.cyclictree, 'num_matrix' ); end;
        try; var.blockvar{i}.cyclictree = rmfield( var.blockvar{i}.cyclictree, 'oo' ); end;
        try; var.blockvar{i}.cyclictree = rmfield( var.blockvar{i}.cyclictree, 'oclass' ); end;
        try; var.blockvar{i}.cyclictree = rmfield( var.blockvar{i}.cyclictree, 'parent' ); end;
        try; var.blockvar{i}.cyclictree = rmfield( var.blockvar{i}.cyclictree, 'prebalancing' ); end;
        try; var.blockvar{i}.cyclictree = rmfield( var.blockvar{i}.cyclictree, 'rho' ); end;
        try; var.blockvar{i}.cyclictree = rmfield( var.blockvar{i}.cyclictree, 'rho_ordering' ); end;
        try; var.blockvar{i}.cyclictree = rmfield( var.blockvar{i}.cyclictree, 'time_lvl' ); end;
        try; var.blockvar{i}.cyclictree = rmfield( var.blockvar{i}.cyclictree, 'toa_ordering' ); end;
        
        try; var.blockvar{i}.log = rmfield( var.blockvar{i}.log, 'info' ); end;
        end;
        
    if( tidyness >= 2 );
        try; var = rmfield( var, 'M_sym' ); end;    
        end;
    try; var = rmfield( var, 'JSR' ); end;
    
    try; var.log = rmfield( var.log, 'error' ); end;
    try; var.log = rmfield( var.log, 'errorinformation' ); end;
    try; var.log = rmfield( var.log, 'info' ); end;
    try; var.log = rmfield( var.log, 'starttime' ); end;
    try; var.log = rmfield( var.log, 'varargin' ); end;
    
    %try; var = rmfield( var, 'param' ); end;
        

end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*TRYNC>
