function [ M, x ] = makepositive( M, abstol, rho_flag ); 
% [ M ] = makepositive( M, [abstol], [rho_flag] ); 
% Multiplies arrays such that its first non-zero entry is positive and its norm is preserved.
% i.e. if M is real, then M is multiplied by -1, if the first nonzero entry is negative.
% If M is real, then the output is also real.
%
% Input:
%   M               array, the thing to make positive
%   abstol          double, default = 0, elements of M which are less than abstol in magnitude are set to zero beforehand
%   rho_flag        bool, default = 'el'
%                   'el', 'element'     makes first non-negative element positive
%                   'rho'               the leading eigenvalue is used to make the matrix positive:
%                                           If leading eigenvalue of M is real and simple, then Matrix is multiplied with scalar such that spectral radius is an eigenvalue;
%                                           If M is complex and leading eigenvalue is simple, then Matrix is multiplied with scalar such that spectral radius is an eigenvalue;
%                                           Otherwise, method rho_flag == false is used.
%                   'rho_strict'        similar to 'rho', but always makes one leading eigenvalue positive
%
% Output:
%   M               array, the thing made positive
%   x               scalar, factor with which input matrix was multiplied
%
% E.g.: makepositive( [2+1i i; 0 1] )
%
% See also: normalizematrix
%
% Written by: tommsch, 2018

%               tommsch, 2021-11-22: Multiplication factor is returned too
%               tommsch, 2023-04-12: Added option 'rho_flag'
% Changelog:
    

    if( nargin <= 1 || isempty(abstol) );
        abstol = 0; end;
    if( nargin <= 2 || isempty(rho_flag) );
        rho_flag = false; end;
    assert( ~ischar(abstol) && ~isstring(abstol), 'makepositive:input', 'Second input must be empty or a double. Third input can be a string.' );
    if( abstol > 0 );
        M(abs( M ) < abstol) = 0; end;
    
    try;
        M2 = double( M );
    catch me;  %#ok<NASGU>
        M2 = M; end;

    
    if( isanyof(rho_flag, {true,'rho','rho_strict'}) );
        assert( issquare(M), 'makepositive:input', 'Option <''rho''> is only possible for square matrices.' );
        try;
            ev = eig( M2 );
            m = max( abs(ev) );
            idx = abs( m - abs( ev ) ) < 1e-12;
            leading = ev(idx);
            if( isequal(rho_flag, 'rho_strict') );
                x = exp( -1i * angle(leading(1)) );
                if( isreal(leading) );
                    x = round( x ); end;
            elseif( numel(leading) >= 2 );
                rho_flag = false;
            elseif( isreal(leading) && isreal(M) );
                x = exp( -1i * angle(leading) ); 
                x = round( x );  % in this case x can only be `-1` or `1`
            elseif( ~isreal(M) );
                x = exp( -1i * angle(leading) );  % if the matrix is complex, the output can be complex too
            else;
                rho_flag = false; end; 
        catch me;  %#ok<NASGU>
            rho_flag = false; end; end;
    
    if( isanyof(rho_flag, {false,'el','element'}) );
        idx = find( M2, 1 );
        complex_flag = ~isAlways( norm(imag(M2), 1) == 0, 'Unknown','false' );
        if( complex_flag );
            % matrix is complex and we need to multiply with angle of first entry
            x = exp( -1i * angle(M(idx)) );
        elseif( isAlways( M2(idx) < 0, 'Unknown','false' ) );
            % first entry is negative, and we need to multiple with -1
            x = -1;
        elseif( isAlways( M2(idx) > 0, 'Unknown','false' ) );
            % first entry is positive, and we do not need to change the matrix
            x = 1;
        else;
            x = sign( M2(idx) );
             end; end;
    if( ~isempty(x) );
        M = M * x; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
