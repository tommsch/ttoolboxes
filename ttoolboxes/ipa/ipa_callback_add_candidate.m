function [ bv ] = ipa_callback_add_candidate( bv );
% [ bv ] = ipa_callback_add_candidate( bv )
% Adds a new candidate to the cyclictree
% Must be executed at position: "generatenewvertex_begin"
%
% Conditions: Currently only applicable for case (K)
%             Does not work if a graph is given
%
% See also: ipa_execute_callback
%
% Written by: tommsch, 2024-10-01

    assert( ~ipa_isgraph(bv.cyclictree.graph), 'ipa:callback_add_candidate', 'Currently this callback does not work with graphs' );
    assert( strcmpi(bv.cyclictree.algorithm(1), 'k'), 'ipa:callback_add_candidate', 'This callback only works for the ''kone'' funct.' );

    bv = make_struct( bv );

    % check whether to add candidate in this iterations
    last_iter = bv.cyclictree.ipa_callback_add_candidate.last_iter;
    cur_iter = bv.log.iteration;
    if( last_iter + 2 >= cur_iter );
        return; end;

    bv.cyclictree.ipa_callback_add_candidate.last_iter = cur_iter;
    [o_idx, len, bv] = get_last_ordering( bv );
    VV_before = make_VV_before( bv );

    len_msg = 0;
    if( bv.param.verbose >= 1 );
        msg = '[Search new candidate to add]  ';
        fprintf( '%s', msg );
        len_msg = numel( msg ); end;

    [oo, v0, retflag, bv] = get_new_ordering( o_idx, len, VV_before, len_msg, bv );  
    if( retflag || bv.log.errorcode );
        return; end;
    bv = add_to_cyclic_tree( oo, v0, VV_before, len_msg, bv );
    
    VV_after = [bv.cyclictree.VV{:}];
    if( ~iskone(VV_after) );
        bv.log.errorcode = ipa_errorcode.IS_NOT_A_KONE;
        end;

end

function [ bv ] = make_struct( bv );
    J = numel( bv.cyclictree.Mt );

    if( ~isfield( bv.cyclictree, 'ipa_callback_add_candidate' ) );
        bv.cyclictree.ipa_callback_add_candidate = struct;
        bv.cyclictree.ipa_callback_add_candidate.last_iter = 0;
        [oidx, len] = last_ordering( bv.cyclictree.ordering, J );
        bv.cyclictree.ipa_callback_add_candidate.last_ordering = [oidx len];
        end;
end

function [ o_idx, len, bv ] = get_last_ordering( bv );
    J = numel( bv.cyclictree.Mt );
    
    if( isfield( bv.cyclictree.ipa_callback_add_candidate, 'last_ordering' ) );
        lo = bv.cyclictree.ipa_callback_add_candidate.last_ordering;
        o_idx = lo(1);
        len = lo(2);
    else;
        [o_idx, len] = last_ordering( bv.cyclictree.ordering, J ); end;
end

function [ VV_before ] = make_VV_before( bv );
    VV_before = [bv.cyclictree.VV{:}];
    idx_zero = ~any( VV_before );
    VV_before(:,idx_zero) = [];
    VV_before = unique( VV_before', 'rows' )';
end

function [ oo, v0, retflag, bv ] = get_new_ordering( o_idx, len, VV_before, len_msg, bv );
    retflag = false;
    J = numel( bv.cyclictree.Mt );
    iter_max = 100;
    for iter = 1:iter_max;
        if( iter == iter_max );
            if( bv.param.verbose >= 1 );
                fprintf( repmat( '\b', [1 len_msg] ) );
                msg = '[Could not find new candidate] ';
                fprintf( '%s', msg );
                retflag = true;
                return; end;
            break; end;
        if( bv.param.verbose >= 1 );
            fprintf( '\b%s', nif( mod(iter, 4), '-','\','|','/' ) ); end;
        if( o_idx >= size( necklaces(len, J), 2 ) );
            o_idx = 1;
            len = len + 1;
        else;
            o_idx = o_idx + 1; end;
        bv.cyclictree.ipa_callback_add_candidate.last_ordering = [o_idx len];
        oo = necklaces( len, J, o_idx );
        Pi = buildproduct_fast( tifh(bv.param.epssym >= 0, @() bv.cyclictree.Mt_sym, bv.cyclictree.Mt), oo );

        [v0, d, bv.log.errorcode] = test_product( VV_before, Pi, 'num', bv );  %#ok<ASGLU>
        if( bv.log.errorcode );
            return; end;
        if( ~isempty(v0) && bv.param.epssym >= 0 );
            [v0, d, bv.log.errorcode] = test_product( VV_before, Pi, 'sym', bv );  %#ok<ASGLU>
            if( bv.log.errorcode );
                return; end; end;
        if( ~isempty(v0) );
            break; end; end;
    if( bv.param.verbose >= 1 );
        fprintf( '\b' ); end;
end

function [ v0, d, errorcode ] = test_product( VV_before, Pi, num_sym, bv );
    param = bv.param;
    cycle = tif( param.epssym >= 0 || param.cycle, 0, 1 );
    switch num_sym
        case 'num';
            VV_before = real( double( VV_before ) );
            Pi = double( Pi );
            leadingeigenvector_sym = 0;
            epsilon = 1e-12;
        case 'sym';
            leadingeigenvector_sym = param.leadingeigenvector.sym;
            epsilon = 0;
        otherwise;
            fatal_error; end;

    [ v0, ~, ~, ~, ~, ~, d ] = leadingeigenvector( Pi, 'multipleeigenvector',param.multipleeigenvector, 'v',param.verbose-1, 'complexeigenvector',3, 'simpleconjugate', 'cycle',cycle, 'sym',leadingeigenvector_sym, 'normalization',param.minmax );
    
    for i = numel( v0 ):-1:1;
        [v0{i}, errorcode] = orientate_eigenvector( v0{i}, epsilon, bv );
        if( isempty(v0{i}) );
            v0(i) = []; end;
        if( errorcode );
            return; end; end;
    if( isempty(v0) );
        errorcode = ipa_errorcode.NOINVARIANTKONE;
        return; end;

    nrm_v0 = polytopenorm( real(double([v0{:}])), VV_before, bv.cyclictree.algorithm, 'output','ub', 'v',param.verbose-1 );
    idx = isAlways( nrm_v0 < 1, 'Unknown','true' );
    v0(:,idx) = [];
    d(idx) = [];
end

function [ v0, errorcode ] = orientate_eigenvector( v0, epsilon, bv )
    errorcode = ipa_errorcode.UNSETERROR;
    if( isAlways( norm( imag(v0), 1 ) > epsilon*numel( v0 ) ) );
        errorcode = ipa_errorcode.UNEXPECTED_COMPLEX_EIGENVECTOR;
        return; end;
    v0 = real( v0 );
    dotprod = sum( v0 .* bv.cyclictree.polytopenorm.Kt, 1 );
    if( all( isAlways( dotprod <= 0, 'Unknown','false' ) ) );
        % do nothing
    elseif( all( isAlways( dotprod >= 0, 'Unknown','false' ) ) );
        v0 = -v0; 
    else;
        v0 = [];
        end;
end

function [ bv ] = add_to_cyclic_tree( oo, v0, VV_before, len_msg, bv );
    if( bv.param.verbose >= 1 );
        fprintf( repmat( '\b', [1 len_msg-1] ) ); end;
    vprintf( '[Added new candidate: %r', oo.', 'imp',[1 bv.param.verbose] );
    vprintf( '(new)', 'imp',[2 bv.param.verbose] );
    for i = 1:numel( v0 );
        bv.cyclictree = ipa_cyclictree( bv, ipa_constant.candidate, oo, v0{i}, [] );
        nrm = polytopenorm( bv.cyclictree.VV{end}, VV_before, bv.cyclictree.polytopenorm.auxiliary_data, bv.cyclictree.algorithm, 'verbose', bv.param.verbose-1 );
        bv.cyclictree.norm{end} = double( nrm );
        %vprintf( ' ', 'imp',[1 bv.param.verbose] );
        end;
    vprintf( '] ', 'imp',[1 bv.param.verbose] );
end

function [ o_idx, len ] = last_ordering( ordering, J );

    % get candidates of maximal length
   len_o = zeros( size(ordering) );
   for i = 1:numel( ordering );
       len_o(i) = size( ordering{i}, 1 ); end;
   len = max( len_o );
   if( len == 0 );
       o_idx = 0;
       len = 0;
       return; end;
   idx = len_o < len;
   ordering( idx ) = [];
   ordering = [ordering{:}];
   ordering = simplify_ordering( ordering );

   % find last candidate
   nl = necklaces( len, J );
   found = false;
   for o_idx = size( nl, 2 ):-1:1
       for m = 1:size( ordering, 2 );
           if( any( all( ordering == nl(:,o_idx), 1 ) ) );
               found = true;
               break; end; end;
        if( found );
            break; end; end;
    assert( found );

end

function [ nl ] = necklaces( len, J, idx );
    persistent nl_
    if( isequal(nl_, []) );
        nl_ = {}; end;
    if( len == 0 );
        nl = [];
        return; end;
    % check if we need to generate it
    if( any( size(nl_) < [len J] ) || ...
        isempty( nl_{len, J} ) ...
      );
        nl = genNecklaces( len, J )' + 1;
        nl = simplify_ordering( nl );
        len_nl = sum( nl ~= 0, 1 );
        nl(:,len_nl ~= max(len_nl)) = [];
        nl_{len,J} = nl; end;

    if( nargin == 2 );
        nl = nl_{len,J};
    elseif( nargin == 3);
        nl = nl_{len,J}(:,idx);
    else;
        fatal_error; end;
end

%%

function [ value ] = nif( idx, varargin );
    if( idx == 0 );
        idx = numel( varargin ); end;
    
    value = varargin{idx};
end



function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
