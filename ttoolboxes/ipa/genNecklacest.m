function [ X ] = genNecklacest( varargin );
% [ X ] = genNecklacest( n, k, [G], [periodizable] )
% genNecklacest generates all short n-bead necklaces with k colors
%    i.e., all equivalence classes of binary n-tuples under 
%           1) cyclic permutation (rotation) and
%           2) powers.
%    Each equivalence class being represented by the lexicographically first tuple in the class.
%
% Input:
%   n               length of necklace
%   k               number of colours
%   G               adjancy matrix of directed graph, default=[], specifies admissible products
%   periodizable    bool, default = true, whether to generate only peridizable necklaces, or also non-periodizable (has only a meaning if G is non-empty)
%
% Output:
%   X     n x L vector, all short n-bead necklaces
%
% Example:
%   genNecklacest( 3, 3 )
%
% See also: genNecklaces, simplify_ordering
%
% 
%
% Original version: October 2018
% Modified by: tommsch, 2019-02

%               tommsch, March 2019           Behaviour change
% Changelog: 

[n, k, G, algorithm, period] = parse_input( varargin{:} );

switch algorithm;
    case {'old','j','jungers'};
        X = genNecklaces_jungers( n, k );
    case {'new','rs','crsms'};
        X = genNecklaces_crsms( n, k ); 
    case {'fkm'};
        X = genNecklaces_fkm( n, k, G, period ); 
    otherwise;
        error( 'genNecklacest:opt', 'Wrong value for ''mode''.' ); end;

end

function [ n, k, G, algorithm, period ] = parse_input( varargin );
    n = varargin{1};
    arg2 = varargin{2};
    varargin(1:2) = [];
    
    algorithm = 'fkm';  % default value
    if( issquare(arg2) && numel(arg2) > 1 );
        G = arg2;
        k = size( G, 1 );
    else;
        k = arg2;
        G = []; end; % default value
    
    for i = 1:numel( varargin );
        if( isempty(varargin{1}) );
            varargin(1) = [];
        elseif( ischar(varargin{1}) || isstring(varargin{1}) );
            algorithm = varargin{1};
            varargin(1) = [];
        elseif( issquare(varargin{1}) && isempty(G) && ...  % varargin{1} is non-empty, due to first if-branch
                implies( ~isempty(k), size(varargin{1}, 1) == k ) );
            G = varargin{1};
            varargin(1) = []; end; end;

    if( numel(varargin) >= 1 && isscalar(varargin{1}) );
        period = varargin{1}; 
        varargin(1) = []; 
    else;
        period = true; end;

    assert( isempty(varargin) );

    if( isempty(k) && ~isempty(G) );
        k = size( G, 1 ); end;
    
    if( ~isempty(G) );
        expect( isequal(algorithm, 'fkm'), 'genNecklacest:graph', 'Passing a graph only works for algorithm ''fkm''.  I change the algorithm.' );
        assert( size(G, 1) == k, 'genNecklacest:graph', 'Size of graph and value of ''n'' are unequal (but should be equal).' );
        algorithm = 'fkm'; end;
end

%% Fredericksen, Kessler, Maiorana (FKM) algorithm for generating necklaces.
function [ X ] = genNecklaces_fkm( n, k, G, period );
    if( n == 0 );
        X = []; 
        return;
    elseif( k == 0 );
        X = zeros( n, 0 );
        return; 
    elseif( k == 1 );
        if( isempty(G) || G(1,1) );
            X = ones( n, 1 );
        else;
            X = ones( n, 0 ); end;
        return; end;
    
    [X, ~, ct] = genNecklaces_fkm_worker( n, k, G, period );
    X(:,ct+1:end) = [];
end


function [ X, f, ct ] = genNecklaces_fkm_worker( n, k, G, period );
    j = 1;
    f = zeros( 1, n + 1 );
    ct = 0;
    X = zeros( n, 0 );
    while( true );
        if(  mod( n, j ) == 0 ) ;
            if( ~isempty(G) );
                [check, idx_] = is_admissible( f(2:end), G, period);
            else;
                check = true; end;
            if( isempty(G) || check );
                ct = ct + 1;
                if( ct > size(X, 2) );
                    X(1, ceil( ct*1.5 )) = 0; end;
                if( isempty(G) || period || idx_==-1 );
                    X(:,ct) = f(2:end)+1; 
                else;
                    X(:,ct) = circshift( f(2:end)+1, -idx_ ); end; end; end;
        
        % next
        j = n;
        while( f(j+1) == k-1 );
            j = j - 1; end;
        if( j == 0 );
            break; end;

        f(j+1) = f(j+1) + 1; 
        
        i = 0;
        for t = j+1:n;
            i = i + 1;
            f(t+1) = f(i+1); end; end;

end

%% Ruskey, Sawada - A Fast Algorithm to Generate Unlabeled Necklace
function [ X ] = genNecklaces_crsms( N, K );
    if( N == 0 );
        X = []; 
        return; end;
    if( K == 0 );
        X = zeros( N, 0 );
        return; end;
    
    f = zeros( 1, N );
    X = zeros( N, 0 );
    ct = 0;
    [X, ~, ct] = crsms_gen( X, f, ct, 1, 1, N, K-1 );
    
    X(:,ct+1:end) = [];
end

function [ X, f, ct ] = crsms_gen( X, f, ct, n, j, N, K );
    if ( n > N );
        if( mod(N, j) == 0 );
            ct = ct + 1;
            if( ct > size(X, 2) );
                X(1, ceil( ct*1.5 )) = 0; end;
            X(:,ct) = f(2:end)+1; end;
    else;
        f(n+1) = f(n-j+1);
        [X, f, ct] = crsms_gen( X, f, ct, n+1, j, N, K );
        for i = f(n-j+1)+1:K-1+1;
            f(n+1) = i;
            [X, f, ct] = crsms_gen( X, f, ct, n+1, n, N, K ); end; end;
end



%% jungers
% brute force algorithm
function [ X ] = genNecklaces_jungers( n, k )
    % Generate all k-ary N-tuples
    A = zeros( k^n, n );
    for idx = 1:n;
        ww = k^(n-idx);
        for i = 0:(k^idx)-1;
            A(i*ww+1:(i+1)*ww, idx) = mod(i, k); end; end;

    % Remove all non-necklaces
    scaler = k.^(n-1:-1:0)';
    ok = true( k^n, 1 );
    shifter = zeros( n-1, n );
    for i = 1:n-1;
        shifter(i,:) = [(i+1):n, 1:i]; end;
    for scan = 1:k^n;
        if( ok(scan) );
            base = A(scan, :);
            ok(1+base(shifter)*scaler) = 0;
            ok(scan) = 1; end; end;
    X = A(ok,:).' + 1;

    idx = true( 1, size(X,2) );
    for i = 1:size(X,2);
        if( ~isequal(reducelength(X(:,i), 2), X(:,i)) ); 
            idx(i) = false; end; end;
    X = X(:,idx);

end

%% helper functions

function [ check, idx ] = is_admissible( f, G, period );
    idx = -1;
    if( ~period );
        errors = 0;
        for i = 1:numel( f )-1;
            if( ~G(f(i)+1, f(i+1)+1) );
                idx = i;
                errors = errors + 1;
                end; end;
        if( ~G(f(end)+1, f(1)+1) );
            idx = numel( f );
            errors = errors + 1; 
            end;
        check = errors <= 1;
    else;
        for i = 1:numel( f )-1;
            if( ~G(f(i)+1, f(i+1)+1) );
                check = false; 
                return; end; end;
        if( ~G(f(end)+1, f(1)+1) );
            check = false; 
            return; end;
        check = true; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
