classdef ipa_constant
    % Enum class for constants used in ipa algorithmus
    % Since Octave does not support enums, functions are used instead
    methods(Static)
        function x = GUROBI_EPSILON;                    x =      1e-9;          end

        function x = INSIDE_SYMBOLIC;                   x =      0.123456789;   end  % norm constant for a vertex which is symbolically proven to be strictly inside
        function x = DUPLICATE_SYMBOLIC;                x =      0.11223344;    end  % norm constant for a vertex which is proven to be equal to another vertex already contained in the tree
        function x = INSIDE;                            x =      0.54321;       end  % norm constant for a vertex for which an estimate proofed that it is inside
        function x = OUTSIDE;                           x =  12345.;            end  % norm constant for a vertex for which an estimate proofed that it is outside
        function x = CUTOFF;                            x =      1.05;          end
        function x = USELESS_NORMVALUE;                 x =      -.112233;      end
        function x = UNSET_BALANCING_ALPHA;             x = 112233; end

        function x = candidate;                         x = 0; end  % 0
        function x = nearlycandidate;                   x = 1; end  % 1
        function x = extravertex;                       x = 2'; end  % 2
        function x = realvertex;                        x = 3; end  % 3
        %function x = limitvertex;                       x = 4; end  % 4
    end
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
