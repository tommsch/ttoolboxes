classdef ipa_errorcode
% Enum class for errorcodes of ipa algorithmus
%
% negative values mean successfull termination
% positive values mean bad termination
% zero means unset errorcode
%
% Example: ipa_errorcode( -10 )  % yields: ipa_errorcode.NOERROR = -10
%
% Note: Since Octave does not support enums, functions are used instead
%
% Written by: tommsch, 2024-09

% Changelog:    tommsch,    2024-10-28,     Added staticfunction which takes error number and gives back error-string

    methods
        function [ varargout ] = ipa_errorcode( e );
            varargout = {};
            assert( nargin == 1, 'ipa_errorcode:input', 'This function must be called with an error code.' );
            assert( nargout == 0, 'ipa_errorcode:ctor', 'This function does not return anything.' );
            meta = ?ipa_errorcode;
            methods = meta.MethodList;
            found_count = 0;
            for i = 1:numel( methods )
                if( ~isequal( methods(i).Name, upper(methods(i).Name) ) );
                    continue; end;
                [~, value] = evalc( ['ipa_errorcode.' methods(i).Name] );
                if( isequal( value, e ) );
                    if( nargout <= 1 );
                        fprintf( 2, 'ipa_errorcode.%s = %i\n', methods(i).Name, value ); end;
                    found_count = found_count + 1; end; end
            if( found_count == 0 );
                if( nargout <= 1 );
                    fprintf( 2, 'Error code could not be found\n.' ); end;
            elseif( found_count == 1 );
                % do nothing
            else;
                if( nargout <= 1 );
                    fprintf( 2, 'Error code is found multiple times. This is a programming error.' ); end; end;
        end
    end
    
    methods(Static)
        function [ str ] = get( e );  %#ok<INUSD>
            str = strtrim( evalc( 'ipa_errorcode( e )' ) );
            idx = strfind( str, ' ' );
            if( ~isempty(idx) );
                str = str(1:idx(1)-1); end;
            
        end

        function x = COMPLEXKONE;                       x =        -128; end
        function x = NOINVARIANTKONE_DUALTEST;          x =        -129; end
        function x = IS_NOT_A_KONE;                     x =        -125; end
        function x = NOINVARIANTKONE;                   x =        -126; end

        function x = NOWORKER;                          x =         -80; end
        function x = VALIDATEUPPERBOUND;                x =         -60; end
        function x = VALIDATEUPPERBOUNDNORM;            x =         -55; end
        function x = VALIDATELOWERBOUND;                x =         -50; end
        function x = EXACTVALUEFOUNDDURINGBALANCING;    x =         -40; end
        function x = NOERROR_INVARIANTSUBSPACE;         x =         -20; end
        function x = NOERROR;                           x =         -10; end
        function x = NOERROR_APPROX;                    x =          -5; end
        function x = ZEROJSR;                           x =          -4; end
        function x = NOERROR_TRIVIAL;                   x =          -3; end

        function x = UNSETERROR;                        x =           0; end
        
        function x = DRYRUN;                            x =           1; end

        function x = NOCANDIDATEFOUND;                  x =          10; end
        function x = CANDIDATEISNOSMP;                  x =          20; end
        function x = NOBALANCINGVECTORFOUND;            x =          30; end
        function x = BETTERORDERINGFOUND;               x =          60; end
        function x = MAXTIMEREACHED;                    x =          70; end
        function x = MAXTREETIMEREACHED;                x =          75; end
        function x = MAXSTEPNUMBERREACHED;              x =          80; end
        function x = MAXNUM_VERTEXREACHED;              x =          90; end
        function x = MAXREMAININGVERTEXREACHED;         x =          95; end
        function x = MAXTREEDEPTHREACHED;               x =         100; end
        function x = TESTEIGENPLANE;                    x =         110; end
        function x = MAXITERATION;                      x =         120; end

        function x = KONE_ERROR;                        x =         124; end  % for general kone related errors
        function x = LIKELYNOINVARIANTKONE;             x =         127; end
        
        function x = TOOMUCHCANDIDATE;                  x =         130; end
        function x = ERROR_INVARIANTSUBSPACE;           x =         170; end
        function x = NONZEROJSR;                        x =         180; end
        function x = OUTOFMEMORY;                       x =         190; end
        function x = DUPLICATELEADINGEIGENVECTOR;       x =         230; end
        function x = MAXONENORM;                        x =         240; end
        function x = ADMISSIBLEALGORITHM;               x =         250; end
        function x = MAXNORMERR;                        x =         260; end
        function x = STALENORM;                         x =         270; end
        function x = UNEXPECTED_COMPLEX_EIGENVECTOR;    x =         280; end
        function x = UNEXPECTED_NONNONNEGATIVE_VECTOR;  x =         290; end
        function x = NONNEGLECTABLE_COMPLEX_PART;       x =         300; end
        function x = NONNEGLECTABLE_NONNEGATIVE_PART;   x =         310; end
        
        function x = UNDERFLOW;                         x =         500; end

        function x = BAD_OPTION;                        x =        1000; end
        function x = UNKOWNERROR;                       x =         666; end
        function x = INPUTERROR;                        x =      999999; end
        
    end
end

function dummy; end  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
