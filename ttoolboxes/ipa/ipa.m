function [ JSR, var ] = ipa( varargin );
% [ JSR, nfo ] = ipa( M, [G], [options] ) 
% Computes the joint spectral radius of the set of matrices M.
%
% Input:
%   M       the set of matrices
%   G       adjacency matrix of a graph, determining the allowed products
%   
% Options:
%   see: ipa_option
%
% Data Output:
%   JSR             Interval containing the JSR or exact value of the JSR
%   var             a struct containing nearly all data which was generated during the computation
%
% E.g.: ipa( gallery_matrixset('rand_gauss', 'dim',3, 'N',2, 'rho'), 'plot','polytope', 'verbose',3 )
%
% See also: ipa>fullhelp, ipa_option
%
% Written by: tommsch, 2018
% For more information write to: <a href="tommsch@gmx.at">tommsch@gmx.at</a>

%               tommsch,    2019-05-27,     added option 'maxmemory'
%                                           less output for verbose level 2
%               tommsch,    2019-11-08,     Behaviour change of option 'nobalancing'. Argument now is mandatory. If nobalancing=-1, balancing is done whenever a balancing vector was found.
%                                           Behaviour change of option 'delta'. Option now does not disable balancing. Instead it sets <'nobalancing',-1>
%                                           If delta == -1 and validateupperbound > 0, then min( blockvar.param.lambda/var.param.validateupperbound*1.02, 1 );
%               tommsch,    2020-06-21,     Options removed: pauseonreset, proof, save, waitafterbalancing, diary, profile, clc, alltype, maxmemory, balancing, multiplicity
%                                           Behaviour change: Function throws an error when called without proper first argument
%                                           Removed option: 'smpflag'. Whether an ordering is a candidate or a nearly-candidate is now computed. As a side-effect, extra-vertices cannot be given via 'v0' anymore
%                                           struct 'type' split up into two structs 'var' and 'blockvar'
%                                           Options 'testspectralradius' and 'testeigenplane' renamed to 'epsspectralradius' and 'epseigenplane' and default sign changed to +
%                                           Options 'nobalancing' and 'balancingvector' merged to option 'balancing'
%                                           cyclictree.V0s is not computed anymore since it is not necessary
%                                           Added complex case
%               tommsch,    2020-08-28,     Implemented symbolical comparisons of points,
%                                           Added option 'epssym', 'lambda'
%               tommsch,    2021-01-18,     Added debug option 'prebalancing'
%                                           Added option 'addlimitsmp'
%               tommsch,    2021-06-02,     Bugfix, removed option 'epslinprog'
%               tommsch,    2021-11-28,     Added option 'balancing','rand', 
%                                           Deprecated option 'nocycle'
%               tommsch,    2022-02-18,     Interface partly rewritten, added support for interval arithmetic
%               tommsch,    2023-01,        Added graph option, renamed function to `ipa`
%               tommsch,    2024-01,        Added experimental 'kone' case
%                                           Added experimental 'extrapath' feature
%               tommsch,    2024-09-06,     Added rudimentary support for Matlab autocompletion 
%               tommsch,    2024-10-04,     Replaced magic numbers with (Octave-compatible) enums.
%                                           Added callbacks - options 'iter_begin', 'iter_end'
%               tommsch,    2024-10-08,     Removed code for limitvertices and limitmatrices
%               tommsch,    2024-10-22,     Made case (K) much more robust
%               tommsch,    2025-01-13,     Removed field .log.info from nfo struct
%                                           Added (experimental) field .log.tau


% Things to be done
% YY fastnorm: Vertices die ausserhalb sind erst hinzufuegen, wenn sie auch getestet worden waeren.
% XX Alle matrices-of-column-vectors transponieren, damit ich sparse arrays machen kann
% YY Test if tree corresponding to a candidate gets vertices added, and not those of nearly-candidates or extra-vertices. Otherwise restart and make nearly-candidates and extra-vertices smaller
% XX implement termination-criteria with eigenplanes, and its search for new smp-candidates
% XX When delta < 1, we do not need complex case

    ipa_persistent_var( 'clear' );
    if( nargin == 0 );
        help ipa;  %#ok<MCHLP>
        return; end;
    [M, var] = ipa_option( varargin{:} );
    var = ipa_execute_callback( var, var.param.callback.begin ); if( var.log.errorcode ); return; end;

    [JSR, var] = ipa_smoking_tests( M, var );
    if( var.log.errorcode );
        return; end;

    var.log = ipa_check_var( var.log, var );  % make tests if options are sensible

    [MM, var] = ipa_split_in_invariant_subspaces( var );
    [bv, var] = ipa_construct_blockvar( MM, var );

    for m = 1:var.param.numblock;
        [bv, var] = ipa_preprocess_blockvar( m, bv, var );

        while( true );
            bv{m} = ipa_prepare_data( bv{m} );
            if( isequal(bv{m}.log.errorcode, ipa_errorcode.UNSETERROR ) );
                bv{m} = ipa_worker( bv{m} ); end;
            [bv{m}, breakflag] = ipa_interpret_result( bv{m} );  % parse error code and errors which can happen in this function
            if( breakflag );
                break; end;

            blockstr = tif( numel(bv) > 1, sprintf( '(of block %i/%i)', m, numel(bv) ), '' );
            vprintf( 'JSR %s: %v\n', blockstr, bv{m}.JSR, 'imp',[1 var.param.verbose] );

            bv{m}.log.num_restart = bv{m}.log.num_restart + 1;
            if( bv{m}.log.num_restart > var.param.maxnum_restart ); 
                vprintf( 'Number of maximal restarts ( currently %i) reached.\n  Use option ''maxnum_restart'' to increase this value.\n', var.param.maxnum_restart, 'cpr','err', 'imp',[1 var.param.verbose] ); 
                break; end; end; end;

    % Post-processing
    var.log.totaltime = toc( var.log.starttime );
    var = ipa_combinevar( var, bv );
    ipa_printerrortext( var );
    JSR = var.JSR;
    
    var = ipa_execute_callback( var, var.param.callback.end ); if( var.log.errorcode ); return; end;

end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         ipa_prepare_data
% sets everything up for ipa_worker
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ bv ] = ipa_prepare_data( bv );
    bv = ipa_reset_blockvar( bv );  % parse input and set blockvar
    bv = ipa_basictest( bv );
    if( bv.log.errorcode && bv.param.norestart < 2 );
        return; end;

    [ bv.cyclictree, bv.log.errorcode ] = makecandidate( tif(bv.param.epssym >= 0, bv.param.M_sym, bv.param.M), bv.param );  % also normalizes matrices
    if( bv.log.errorcode  );
        return; end;
    
    bv.log.errorcode = ipa_zerojsr( bv.cyclictree, bv.param );
    if( bv.log.errorcode );
        return; end;

    [ bv.cyclictree, ...
      bv.log.errorcode ] = ipa_balance( bv.cyclictree, bv.param );
    if( bv.log.errorcode );
        return; end;

    bv.cyclictree = ipa_cyclictree( bv, bv.cyclictree );  % makes the root of the trees. Sets V, Vs, o, normval, Rho, child

    bv.cyclictree = ipa_matrix_deltascaling( bv.cyclictree, bv );

    if( numel( bv.cyclictree.smpflag ) > 1 );
        bv = ipa_recomputeroot( bv ); end;

    switch bv.param.minmax;
        case {'max','kone'};
            if( bv.cyclictree.lambda > bv.JSR(1) );
                bv.JSR(1) = bv.cyclictree.lambda; end;
        case 'min';
            if( bv.cyclictree.lambda < bv.JSR(2) );
                bv.JSR(2) = bv.cyclictree.lambda; end;
        otherwise; fatal_error; end;    
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         ipa_worker
% does all the hard work
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [ bv ] = ipa_worker( bv );
    % Worker uses only value .lambda for computations
    bv = ipa_execute_callback( bv, bv.param.callback.worker_begin ); if( bv.log.errorcode ); return; end;
    bv.log.starttreetime = tic;
    vprintf( 'Start constructing the polytope.\n\n', 'imp',[3 bv.param.verbose] ); 
    vprintf( '   0: _SR = [ %15.12g, %15.12g ], norm= %13.12g, ', min(bv.JSR), max(bv.JSR), bv.cyclictree.norm_lvl(end), 'imp',[1 bv.param.verbose] );
    
    while( true );
        bv = ipa_execute_callback( bv, bv.param.callback.iter_begin ); if( bv.log.errorcode ); break; end;

        leveltime = tic;  % temporary variable for the clock-value

        bv = ipa_plotoutput( bv );

        bv = ipa_terminate_check( bv );
        if( bv.log.errorcode );
            break; end;

        bv = ipa_generatenewvertex( bv );  % generate all valid new vertices and select the ones whose norms we compute
        if( bv.log.errorcode );
            break; end;
        [bv, ct_est, str_est] = ipa_estimatenorm( bv );

        [pts_idx, pts_component, num_selected_pts, ...
         VV, VV_idx, VV_component, nVVbig] = ipa_selectvertex( bv );  % select vertices of which we compute the exact norm, choose vertices whose norm shall be computed. It is allowed to select parents and children.

        if( num_selected_pts(1) == 0 && num_selected_pts(2) == 0 );  % test if there are no vertices to test left
            bv.JSR = bv.cyclictree.lambda;
            bv.log.errorcode = ipa_errorcode.NOERROR;
            break; end;

        vprintf( '#test: %i/%i, #V:%i/%i | ', num_selected_pts(1)+ct_est, sum(num_selected_pts)+ct_est, size(VV, 2), nVVbig, 'imp',[1 bv.param.verbose] );          
        if( numel(str_est) > 0 );
            vprintf( '(est) %s | ', str_est, 'imp',[1 bv.param.verbose] ); end;
        [normval, iteration, ptstotest, num_normerr] = ipa_compute_norms( bv, pts_idx, pts_component, VV, VV_idx, VV_component );
        
        bv = ipa_save_to_bv( bv, pts_idx, VV_idx, normval, num_selected_pts, leveltime, iteration, ptstotest, num_normerr ); 

        bv = ipa_execute_callback( bv, bv.param.callback.iter_end ); if( bv.log.errorcode ); break; end;
        end;
    bv.log = ipa_write_logs( bv );
    bv = ipa_execute_callback( bv, bv.param.callback.worker_end );
end





function fullhelp  %#ok<DEFNU>
% Screen Output
% ================
% <strong>Output written in red must be read!</strong>
%
% For some options or input matrices, the algorithm delivers wrong results and these messages warn in these cases.
% These messages are printed again after the termination of the algorithm.
%
% Output in front of the progress bar:
% =========================================
% Time: x/y       Time needed for the last iteration/total time needed for building the tree.
% JSR = [ x, y ]  Interval in which the JSR lies.
% norm = x        Current minimal computed norm of the polytope.
% In: x, Out: y   Number of points which lie inside or outside the polytope, checked by estimating the Minkowski-norm.
% \#test: x/y      Number of points to test in this iteration/number of points to check in total
% \#V: x/y         Number of points in simplified polytope/number of points in polytope in total
% 'Test old vertex'  Old vertices of polytope get estimated again.
% 
% Output in the progress-bar:
% ==============================
% i  Vertex is proofed to be inside of the polytope, but norm is unknown
% x  Vertex is proofed to be outside of the polytope, but norm is unknown
% _  Vertex is inside of the polytope
% .  Vertex is machine-epsilon-near to the polytope 
% ,  Vertex is 1000*machine-epsilon-near to the polytope
% o  Vertex is slightly outside
% O  Vertex is far outside
% m  Negative value occurred during computation of norm, vertex is added
% 8  Inf occurred during the computation of the norm, vertex is added
% ?  NaN or Inf occurred during the computation of the norm, vertex is added
% E  Some error occurred during the computation of the norm, vertex is added
%
% Data Output
% =====================
% [ JSR, nfo ] = ipa( M, [options] ) 
% The function ipa returns two variables
% 
% JSR is the value of the joint spectral radius,
% nfo contains nearly all intermediate computed data, in particular the
% vertices AND how the vertices are to be computed.
% 
% a) nfo.blockvar is a cell array where each element corresponds to one
% invariant subspace of the input matrices. If the matrices have no
% invariant subspace, then there clearly only exists nfo.blockvar{1}
% 
% (In very trivial cases it can happen that nfo.blockvar is empty).
% 
% b) The tree which is built up by the algorithm is stored under
% nfo.blockvar{i}.cyclictree
% This field has the following (for your task important) fields:
%      .norm      % norm of vertices
%      .oo        % ordering of matrix products of computed vertices
%      .VV        % computed vertices
%      .v0        % starting vectors
%      .lambda    % the averaged spectral radius of the smp candidate
% All of those are cell arrays, where each entry corresponds to one
% starting vector of the cyclic tree.
% In most cases, there is one leading eigenvector and some extra vertices
% (The type of the starting vector is stored in .smpflag, 0 means leading
% eigenvector).
% The starting vectors are stored in .v0
% 
% each entry in .norm is an array of column vectors, storing the computed
% norms of the vertices.
%      First row: Estimated norm
%      Second row: Upper bound
%      Third row: Lower bound
% You are only interested in the second row.
% Now, to get the vertices of the invariant polytope, you first have to
% get those which were mapped outside, i.e. whose norm was greater than 1
%
% ```matlab
% nrm = [nfo.blockvar{1}.cyclictree.norm{:}];
% idx = nrm(2,:) > 1-5e-9;  % 5e-9 is the standard safety bound used
% 
% V = [nfo.blockvar{1}.cyclictree.VV{:}];
% P = V(:,idx)  % [P -P] is the invariant polytope (for case R, for case P and C it is slightly different)
% ```
%
% Now P contains the vertices (plus seome redundant ones).
% 
% If you want to compute them by hand, its slightly more difficult,
% because the arrays which hold the information are not of the same size
% in general.
%      .v0 contains the starting vectors
%      .oo the index sequence (as column vectors) of how the corresponding entry in .VV is computed.
% E.g.:
%        o{2}(:,3) == [1 2 2 2 0 0]';
%        MM = nfo.blockvar{1}.M;
%        Then V{2}(:,3) == MM{2}^2*MM{1}*v0{2}*nfo.blockvar{1}.cyclictree.lambda^4
% I think one should get the pattern
% 
% If your input is integer valued, and you used the option `'sym',1`
% (or the algorithm decided that it should use this option) then the
% following fields contain symbolic expressions
%        nfo.blockvar{1}.M_sym
%        nfo.blockvar{1}.cyclictree.v0_sym
%        nfo.blockvar{1}.cyclictree.lambda_sym
%        nfo.blockvar{1}.cyclictree.v0_sym


end



function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
