function [ varargout ] = tjsr( varargin );
% wrapper function for ipa
    n = nargout( 'ipa' );
    varargout = cell( 1, max(nargout, n) );
    [varargout{:}] = ipa( varargin{:} );
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
