function [ o, i ] = merge_oo( varargin );
% [ o, i ] = merge_oo( o1, ..., on );
% takes a cell array of matrices of column vectors (e.g. orderings),
% and concatenates it, and returns it as one big matrix.
% Missing entries are padded with zeros.
%
% Input:
%   oi      matrices of column vectors of integers, the orderings
%   
% Output:
%   o       matrix of column vectors of integers, the orderings merged into one big matrix, padded with zero at the end if necessary
%   i       the index i of oi
%
% Written by: tommsch, 2022

%               tommsch, 2024-10-07,    Renamed from `ipa_merge_o` to `merge_oo`
%                                       Bugfix
% Changelog:

    if( numel(varargin) == 1 && ...
        iscell( varargin{1} ) ...
      );
        varargin = varargin{1}; end;
    
    len = cellfun( 'size', varargin, 1 );
    sze = cellfun( 'size', varargin, 2 );
    maxlen = max( len );
    i = [];

    % zero-pad if necessary
    for k = 1:numel( varargin );
        i(end+1:end+sze(k)) = k;
        if( len(k) < maxlen );            
            varargin{k}(maxlen,1) = 0; end; end;

    % concatenate
    o = [varargin{:}]; 
    

end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
