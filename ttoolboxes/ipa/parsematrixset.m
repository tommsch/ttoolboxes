function [ M, G, varargin ] = parsematrixset( varargin );
% takes input and returns a cell array of matrices or an error if input is bad
% possible inputs for M
%    cell array of square matrices
%    3d array of dimension N x N x J
%    matrix of dimension N x NJ
%    matrix of dimension NJ x N

    M = varargin{1};
    varargin(1) = [];
    if( ischar(M) );
        M = gallery_matrixset( M ); end;

    if( ~iscell(M) );
        if( isequal(M, []) );
            M = {[]};
        elseif( ndims(M) == 3 );
            M = squeeze( num2cell(M,[1 2]) )';
        elseif( ndims( M ) == 2 && ...
                ~mod( size(M, 2), size(M, 1 ) ) ...
              );  %#ok<ISMAT>
            M = mat2cell( M, size(M, 1), repmat(size(M, 1), [1 size(M, 2)/size(M, 1)]) );
        elseif( ndims( M ) == 2 && ...
                ~mod( size(M, 1), size(M, 2) ) ...
              );  %#ok<ISMAT>
            M = mat2cell( M, repmat(size(M, 2), [1 size(M, 1)/size(M, 2)]), size(M, 2)  )';
        else;
            error( 'parsematrixset:input', 'Input matrices given in wrong format.' ); end; end;

    assert( all( cellfun(@issquare, M) ), 'parsematrixset:input', 'First input to ipa must be a cell array of *square* matrices.' );
    assert( allm( isAlways(imag([M{:}]) == 0) ), 'parsematrixset:input', 'Only real valued input is possible.' );
    

    G = [];
    if( numel( varargin ) >= 1 && ...
        issquare( varargin{1} ) && ...
        isnumeric( varargin{1} ) && ...
        matrixinfo( varargin{1}, 'bool' ) ...
      );
        G = varargin{1};
        assert( ispathcomplete(G), 'parsematrixset:graph', 'Passed graph must be pathcomplete, i.e. there must exists a path from each vertex to each other vertex.' );
        assert( numel(M) == size(G, 1), 'parsematrixset:graph', 'Size of graph and number of matrices are unequal (but should be equal).' );
        varargin(1) = []; end;
    [G, varargin] = parsem( {'graph','G'}, varargin, G );  % Specfies admissible products.

    for j = 1:numel( M );
        if( islogical(M{j}) );
            M{j} = double( M{j} ); end; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
