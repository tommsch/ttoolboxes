function [ bv ] = ipa_callback_eigenplane_criteria( bv ); 
% Checks whether the eigenplane criteria is fulfiled
% 
% See also: ipa_execute_callback
%
% Written by: tommsch, 2024-10-01
    
    if( ~isfinite( bv.param.epseigenplane ) || ...
        bv.param.norestart ...
      );
        return; end;

    if( ~isanyof( lower(bv.cyclictree.algorithm(1)), {'p','r','c','l'} ) );
        bv.log.errorcode = ipa_errorcode.BAD_OPTION;
        bv.log.errorinformation = 'The callback ''ipa_callback_eigenplane_criteria'' can not be used with case (K).'; end;

    [vv, oo, new_idx] = ipa_get_newly_added_vertices( bv );
    oo = merge_oo( oo );
    %vv = [vv{:}];
    num_trees = numel( bv.cyclictree.smpflag );
    for tree_idx = 1:num_trees;
        if( bv.cyclictree.smpflag(tree_idx) ~= ipa_constant.candidate || ...
            isempty(bv.cyclictree.v0s{tree_idx}) || ...
            ~any( bv.cyclictree.v0s{tree_idx} ) ...
          );
            continue; end;
        v0_idx= bv.cyclictree.v0idx(tree_idx);
        comp_idx = [bv.cyclictree.component_idx{:}] == v0_idx;
        if( isempty(comp_idx) );
            vv_idx = ones( 1, size(vv{tree_idx}, 2) );
        else;
            vv_idx = comp_idx(new_idx{tree_idx}); end;
        err_eigenplane = abs( sum( bv.cyclictree.v0s{tree_idx} .* vv{tree_idx}(:,vv_idx), 1 ) );  % 'abs' is necessary for case (C)
        switch bv.param.minmax;
            case 'max';
                [err_eigenplane, idx_eigenplane] = max( err_eigenplane );
                check = err_eigenplane > 1 + bv.param.epseigenplane;
            case 'min';
                [err_eigenplane, idx_eigenplane] = min( err_eigenplane );
                check = err_eigenplane < bv.cyclictree.delta - bv.param.epseigenplane;
            otherwise; fatal_error; end;
        if( check );
            bv.log.errorcode = ipa_errorcode.TESTEIGENPLANE;
            bv.log.errorinformation = {err_eigenplane, oo(:,idx_eigenplane(1)), tree_idx};
            end; end;
end



function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
