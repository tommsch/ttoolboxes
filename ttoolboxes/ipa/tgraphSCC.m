function [scnt,C] = tgraphSCC(G)
% tgraphSCC  Finds the strongly connected components of graph 
%           G using Tarjan's algorithm.
%
% [SCNT,C] = tgraphSCC(G)
%
%  G must be the adjency matrix of a directed graph, i.e. if entry (i,j) is
%    non-zero there is an edge going from i to j
%
%  SCNT is the number of strongly connected components 
%
%  C is a vector indicating to which component each node belongs
%
%
% REFERENCES: 
%    R.Tarjan, 
%      "Depth first search and linear graph algorithms", 
%      SIAM J. on Computing, 1(2):146-160, 1972 
%    R.Sedgewick, 
%      "Algorithms in Java, Third Edition, Part 5: Graph Algorithms",
%      Section 19.8, Addison-Wesley, 2003
%
% Written by: Jungers, The JSR Toolbox
% Modified by: tommsch, 2020

    tgraph_S = struct;


    n = length(G);
    Adj = cell(n,1);

    for ivert = 1:n
        Adj{ivert} = find(G(ivert,:) > 0); end;

    scnt=0;
    cnt = 0;

    tgraph_S.nel=0;
    tgraph_S.stack = {};
    tgraph_S.isIn = zeros(1,n);

    low = -ones(1,n);
    number = -ones(1,n);
    C = -ones(1,n);

    for iv = 1:n
        if (number(iv) == -1)
            [ number, C, cnt, low, Adj, scnt, tgraph_S ] = strongConnect( iv, number, C, cnt, low, Adj, scnt, tgraph_S ); end; end;
end

function [ number, C, cnt, low, Adj, scnt, tgraph_S ] = strongConnect( v, number, C, cnt, low, Adj, scnt, tgraph_S )
    cnt = cnt+1;
    low(v)=cnt;
    number(v)=cnt;
    tgraph_S = add( v, tgraph_S );

    for w = Adj{v}
        if (number(w) == -1)
            [ number, C, cnt, low, Adj, scnt, tgraph_S ] = strongConnect(w, number, C, cnt, low, Adj, scnt, tgraph_S );
            low(v) = min(low(v),low(w));
        elseif (number(w)<number(v))
            if (tgraph_S.isIn(w))
                low(v) = min(low(v),number(w)); end; end; end;
    if (low(v) == number(v))
        cont = 1;
        scnt = scnt+1;

        while (tgraph_S.nel ~= 0 && cont)
            top = tgraph_S.stack{1};
            if (number(top) >= number(v) )
                [w, tgraph_S] = pop( tgraph_S );
                C(w) = scnt;
            else 
                cont = 0; end; end; end;
end

function [ tgraph_S ] = add( v, tgraph_S );
  tgraph_S.nel = tgraph_S.nel+1;
  tgraph_S.stack = [{v} tgraph_S.stack]; 
  tgraph_S.isIn(v)=1;
end

function  [ v, tgraph_S ] = pop( tgraph_S );
  if (tgraph_S.nel ~= 0)
      v = tgraph_S.stack{1};
      tgraph_S.nel = tgraph_S.nel-1;
      tgraph_S.stack(1) = [];
      tgraph_S.isIn(v)=0;
  else
      v = []; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

