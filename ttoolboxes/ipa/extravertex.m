function [ Vextra, VVVextra ] = extravertex( varargin );
% [ Ve, VVe ] = extravertex( V, [options] );
% [ Ve, VVe ] = extravertex( M, oo, v0, [options] );
% [ Ve, VVe ] = extravertex( V, M, oo, v0, [options] );
% Computes vertices such that a polytope augmented with those vertices has non-empty interior
%
% Input:
% ======
%   V                       matrix of column vectors, the polytope
%   M                       1xJ cell array of square matrices
%   oo                      column vector, OR
%                           1xN cell array of column vectors, orderings of matrix products
%   v0                      column vector, OR
%                           1xN cell array of column vectors, 
%                               Computes the polytope VV from M, oo, and v0, precisely
%                               given ordering oo{1} and vector v0{1}, the algorithm computes the vectors 
%                                   M{oo{1}(1)}*v0, M{oo{1}(1)}*M{oo{1}(2)}*v0, M{oo{1}(1)}*M{oo{1}(2)}*...*M{oo{1}(end)}*v0
%
% Options:
% ==========
%   'verbose',val           default=1, verbose level
%   'threshold',val         default=.01, influences the number of added vectors. The smaller the number, the less vectors are added.
%                           If threshold=0, VVe still is a basis for R^n. If threshold is big, VVe will have more vectors than its dimension
%   'scale',val             double, default=1, factor with which the matrices M are scaled before the vectors of the polytope VV are computed
%   'polytopetype',val      string, default='r', The polytope which shall be considered, possible values = 'p','r','c'                           
%                           Note: A method for complex polytope is not yet implemented
%   'celloutput'            Function returns the vectors as a row cell array of column vectors
%   'sym',val               integer, default = -1, Method how to compute
%                               -1: same as input
%                                0: double
%
% Output:
% =======
%   Ve      matrix of column vectors, extra vectors
%   VVe     matrix of column vectors, input vectors plus extra vectors [V Vextra]
%
% Written by: tommsch, 2020 

%               2022-02-17, changed api from using CONEFUNCT etc to using strings
%                           removed some abbreviations of options
% Changelog:

    [opt, varargin] = parse_options( varargin{:} );
    [VV, M] = parse_VV( opt, varargin{:} );
    [Vextra, VVVextra] =  compute_extravertex( VV, M, opt );
    check_result( VVVextra, opt );
    [Vextra, VVVextra] = postprocess( Vextra, VVVextra, opt );
end

function [ opt, varargin ] = parse_options( varargin );
    [opt.verbose, varargin] =       parsem( {'verbose','v'}, varargin, 1 );
    [opt.threshold, varargin] =     parsem( {'threshold','t'}, varargin, 0.01 ); 
    [opt.scale, varargin] =         parsem( {'scale','s'}, varargin, 1 ); 
    [opt.func, varargin] =          parsem( {'case','polytopetype','problemtype','type','func','funct'}, varargin, 'r' );
    [opt.celloutput, varargin] =    parsem( {'celloutput','cell'}, varargin );
    [opt.symflag, varargin] =       parsem( {'sym'}, varargin, 0 );
    [opt.checkflag, varargin] =     parsem( {'check'}, varargin, 0 );
end

function [ VV, M ] = parse_VV( opt, varargin );
    assert( nargout == 2 );
    VV = [];
    M = [];
    oo = [];
    v0 = [];
    switch numel( varargin );
        case 1;
            VV = varargin{1};
        case 2;
            VV = varargin{1};
            M  = varargin{2};
        case 3;
            M  = varargin{1};
            oo = varargin{2};
            v0 = varargin{3};
        case 4;
            VV = varargin{1};
            M  = varargin{2};
            oo = varargin{3};
            v0 = varargin{4};
        otherwise;
            error( 'extravertex:option', 'Could not parse input.' ); end;

    if( ~isempty(oo) && ~iscell(oo) )
        oo = {oo}; end;
    if( ~isempty(v0) && ~iscell(v0) );
        v0 = {v0}; end;

    VV = to_sym( VV, opt.symflag );
    M =  to_sym( M,  opt.symflag );
    oo = to_sym( oo, opt.symflag );
    v0 = to_sym( v0, opt.symflag );

    if( ~isempty(oo) );
        % compute whole cycle, but I don't want to change the whole program.
        for i = 1:length( M )
            M{i} = M{i} .* opt.scale; end;
        VV2 = cell( 1, length(oo) );
    
        for i = 1:length( oo )
            oo_temp = arrayfun( @(x) oo{i}(1:x),  1:length(oo{i}), 'UniformOutput',0 );
            %val = cellfun( @(x) buildproduct_fast(M, x, v0{i}), oo_temp, 'UniformOutput',0 );
            val = cellfun( @(x) buildproduct(M, x)*v0{i}, oo_temp, 'UniformOutput',0 );
            VV2{i} = [val{:}]; end;
        VV = [VV VV2{:}]; end;

    VV = unique( VV.', 'rows' ).';


end

function [ X ] = to_sym( X, flag );
    assert( nargout == 1 );
    cellflag = iscell( X );
    if( ~cellflag );
        X = {X}; end;
    for i = 1:numel( X );
        try;
            switch flag;
                case -1;  % do nothing
                case 0; X{i} = double( X{i} );
                case 1; X{i} = vpa( X{i} );
                case 2; X{i} = sym( X{i} );
                otherwise; error( 'extravertex:option', 'Wrong value for ''sym''.' ); end;
            catch me;
                warning( 'extravertex:sym', 'Cast to chosen representation (double/vpa/sym) failed.\n Thrown error:' );
                disp( me ); end; end;
    if( ~cellflag );
        X = X{1}; end;
end

function [ Vextra, VVVextra ] = compute_extravertex( VV, M, opt );
    dim = size( VV,1 );
    switch opt.func(1);
        case 'l';
            if( isempty(VV) );
                Vextra = [ones( dim, 1 ), ones( dim )-eye( dim )];
            else;
                mx = 1.2 * max( abs(VV(:)) );            
                idx = isAlways( max( VV, [], 2 ) < opt.threshold, 'Unknown','true' );
                Vextra = mx * eye( dim );
                Vextra = Vextra(:,idx); end;
            if( issym(VV) );
                Vextra = sym( Vextra ); end;        
            
        case {'k','K'};
            Vextra = [];
            breakflag = false;
            if( rank(VV, 1e-5) == dim );
                % do nothing
            elseif( ~isempty(M) );
                J = numel( M );
                if( J == 1 );
                    len_max = 1;
                else;
                    len_max = 1:ceil( log( dim )/log( J ) ); end;
                for len = 1:len_max;
                    oo = mixvector( 1:J, len );
                    rank_old = rank( VV );
                    for i = 1:size( oo, 2 );
                        v0 = leadingeigenvector( buildproduct_fast( M, oo(:,i) ), 'nonnegative_eigenvalue', 'normalization','kone' );
                        v0 = real( [v0{:}] );
                        rank_new = rank([VV Vextra v0], 1e-5);
                        if( rank_new == rank_old );
                            continue; end;
                        Vextra = [Vextra v0]; %#ok<AGROW>
                        rank_old = rank_new;
                        if( rank([VV Vextra], 1e-5) == dim );
                            Vextra = [Vextra v0];  %#ok<AGROW>
                            breakflag = true; end;
                        if( breakflag );
                            break; end; end;
                    if( breakflag );
                        break;  end; end; end;
             if( ~breakflag && rank([VV Vextra], 1e-5) < dim );
                % do something desperate and hope for the best
                warning( 'extravertex:k', 'Could not compute valid extra vertices. I try my best, and guess some.' );
                nullspace = null( VV', 1e-5 );
                v0 = center_kone( VV, 'verbose',opt.verbose );
                for i = 1:size( nullspace, 2 )
                    Vextra = [Vextra v0+opt.threshold*nullspace(:,i) v0-opt.threshold*nullspace(:,i)];  %#ok<AGROW>
                    if( rank([VV Vextra], 1e-5) == dim );
                        break; end; end;
                 for i = 1:size( Vextra, 2 );
                     dotp = sum( Vextra(:,i).*VV, 1 );
                     direction = dotp < 0;
                     if( all( direction ) );
                        Vextra(:,i) = -Vextra(:,i);  %#ok<AGROW>
                     elseif( ~any(direction) )
                         % do nothing
                     else;
                         warning( 'extravertex:k', 'I even give up with guessing eigenvectors.' ); end; end;
             end;
            
            
        case 'p';
            if( isempty(VV) );
                Vextra = eye( dim );
            else;
                idx = isAlways( max( VV, [], 2 ) < opt.threshold, 'Unknown','true' );
                Vextra = eye( dim );
                Vextra = Vextra(:,idx); end;
            if( issym(VV) );
                Vextra = sym( Vextra ); end;
        
        case {'r','c'};
            if( isempty(VV) );
                try;
                    sym(0);
                    Vextra = tif( issym(VV), sym( eye(dim) ), eye(dim) );
                catch me;  %#ok<NASGU>
                    Vextra = eye(dim); end;
            else;
                [U, SVD, ~] = svd( [real(VV) imag(VV)] );
                if( issym(VV) );
                    U = ratt( double(U), [], 1 );
                    SVD = ratt( double(SVD), [], 1 ); end;
                if( ~isvector(SVD) );
                    SVD = diag( SVD ); end;  % make singular values to a vector
                if( length(SVD) < dim ); 
                    SVD(dim) = 0; end;  % fill up with zeros
                ratnum = SVD/max( SVD ); 
                idx = isAlways( ratnum < opt.threshold, 'Unknown','true' );
                idx( size(U,2)+1:end) = [];
                Vextra = normalizematrix( U(:,idx), 'positive',1 ) * max( SVD ); 
                Vextra = simplify( Vextra ); end;
        otherwise;
            error( 'extravertex:type', 'Unkown polytopetype given.' ); end;
    
    switch opt.symflag;
        case -1;  % do nothing
        case 0;
            Vextra = ratt( Vextra ); end;
    
    VVVextra = [VV Vextra];
end

function check_result( VVVextra, opt );
    assert( nargout == 0 );
    % check result
    if( opt.checkflag || opt.verbose >= 3 );
        switch opt.func(1);
            case 'l';
                warning( 'extravertex:type', 'No check can be made, since no automatic extra vertices are computed for case ''l''.' );
            case 'k';
                warning( 'extravertex:type', 'No check can be made, since no automatic extra vertices are computed for case ''k''.' );
            case 'p';
                expect( all(VVVextra(:) >= 0) && all(any(VVVextra > 0, 2)), 'extravertex:badcomputation', 'The computation yielded a result with a polytope with empty interior.' );
            case {'c','r'};
                expect( rank(VVVextra) == size(VVVextra, 1), 'extravertex:badcomputation', 'The computation yielded a result with a polytope with empty interior.' );
            otherwise;
                fatal_error; end; end;
end

function [Vextra, VVVextra] = postprocess( Vextra, VVVextra, opt );
    if( opt.celloutput )
        Vextra = num2cell( Vextra, 1 ); 
        VVVextra = num2cell( VVVextra, 1 ); end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
