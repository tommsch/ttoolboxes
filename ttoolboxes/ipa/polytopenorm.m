 function [ normval, iv, iter, time, proofed ] = polytopenorm( varargin );
% [ normval, iv, iteration, time, proofed ] = polytopenorm( pts, VV, [aux_data], algspec, [options] )
% Computes the Minkowski-norm of a set of points with respect to the polytope co_* VV, where co_* is some convex hull defined by <algorithm>
%
% The function contains one method using MATLAB linprog, and one method using GUROBI solver
% To change from one to the other, one has to comment out/in some lines in the first function
%
% Input:
%   pts             dim x N1 array of column vectors, the pts to be tested. 
%   VV              dim x M array of column vectors, the vertices of the polytope. Each vertex is one column of the matrix
%                           pts and VV can be given as: 'double', 'sym'
%                           If 'sym' type is passed, the results are usually rigorous (see output variable: proofed)
%                   If a whole column of pts/VV is nan, and additional inputs are given via a function handle, then these missing columns are computed lazily
%               
%   algspec         string, combination of char identifiers, in the following order ( See also: parse_algspec )
%                       'a','p','r','c','l','k'   type of norm to use: 
%                           'a'   (experimental) auto (old: AUTOFUNCT ), chooses one of 'p','r','c'
%                           'k'   (experimental) infinite kone norm w.r.t, the function does not produce a norm. 0 if inside, greater-than one if outside
%                                 The vertices of VV must produce a proper koner (i.e. pointed and solid), otherwise UB
%                               'k0': computes whether a vertex is inside or outside of the kone
%                               'k1': measures how far a point is inside/outside. This ist most likely not even a metric. inside: 0-1, outside: 1-inf
%                               'k2': measures how far a point is inside/outside. This ist most likely not even a metric. inside: 0-1, outside: 1-2
%                               'k3','k4','k5': a true kone-vector-norm
%                               'k6': measures how far a point is inside/outside. This ist maybe a kone-vector-norm.
%                               'k*1': computes whether a vertex is inside or outside of the dual kone
%                           'l'   cone-anti-norm for positive orthant
%                           'p'   cone-norm for positive orthant (for 'auto': chosen when both pts and VV only contain non-negative entries) (old: CONEFUNCT)
%                           'r'   polytope norm (for 'auto': chosen when nothing else applies) (old: MINKFUNCT)
%                           'c'   elliptic polytope norm (for 'auto': chosen when pts or VV have at least one complex entry) (old: COMPLEXFUNC)
%                                 not supported on Octave currently
%                               for case 'c' various algorithms can be chosen, which can be specified using numbers
%                               'c1': bad approximate elliptic-polytope (complex) (absco) (old: 'ca', COMPLEXFUNCT_A)%version from Guglielmi, Protasov 2013  
%                               'c2': approximate elliptic-polytope (complexification of MINKFUNCT) (absco)  (old: 'cb', COMPLEXFUNCT_B)  % version from Protasov 2021
%                               'c3': approximate elliptic-polytope (regular polytope in ellipses) (absco)   (old: 'cc', COMPLEXFUNCT_C) 
%                               'c4': approximate elliptic-polytope (irregular polytope in ellipses) (absco) (old: 'cd', COMPLEXFUNCT_D)  % version from Mejstrik, Protasov 2022
%                               'c5': approximate elliptic-polytope (irregular polytope in polytope) (absco) (old: 'ce', COMPLEXFUNCT_E)  % version from Mejstrik, Protasov 2022
%                               'c' : calls the best elliptic-norm function available so far, used algorithm may change in future versions
%
%
%                        'e','n','s','E','N','S'   estimate / numerical / symbolic
%                           'e': computes a fast estimate. Which algorithm to use can be specified by using numbers
%                                    for type 'p':    'e'   (only one estimate algorithm available)
%                                   for type 'r':    'e1'   using a projection on a guessed vertex (old: MINKEST_PROJ)
%                                                    'e2'   using a difference-"metric", gives no upper and lower bounds but still nice results (old: MINKEST_DIFF)
%                                                    'e3'   not-implemented anymore (old: MINKEST_ELL)
%                                                    'e'    default
%                                   for type 'c':    'c','c1','c2,'c3'   similar to type 'r'
%                                   Note that the behaviour of the default estimates is subject to be changed from release to release
%                           'n': computes the norm using LP/CP
%                            's': computes the norm symbolically
%                            For 's': These algorithms behave differently when LP/CP (option 'n') is executed beforhand. To disable automatic execution of an LP solver beforehand, use 'S' instead of 's'
%                           In this case, 's' only compute strict (i.e. rigorous) upper bounds of the norm and the computation is medium slow
%                            Otherwise, also strict lower bounds are computed, but then the algorithm may be very slow, whenever the polytope VV has more than a few vertices
%                            Furthermore, currently the implementation of the 'n'/'s' positive-orthant-norm computation is far from optimal and VERY VERY slow, especially in higher dimensions
%                           If the upper case versions 'E','N','S' are used, then the algorithm follows strictly the input.
%
% Options:
%   'pts','VV'              same type as 'pts'/'VV', default = [], Additional representations of the input can be passed. These are used for faster/exact computations
%                           OR
%                           cell array, default = [], 
%                           OR
%                           function handle, which takes an index an returns vertices (see: eval_oMvs)
% Additional representations of the input can be supplied for lazy evaluation.
%                           Format of cell array: {o,M,v,[s]}, where
%                           o    cell array of index vectors OR matrix of column vectors of indices (DANGER!, see below)
%                           M    cell array of square matrices
%                           v    cell array of column vectors OR matrix of column vectors
%                           s    optional, default = 1, scalar
%                           Roughly speaking, a vertex is computed as follows: vertex = s^(-numel(o-1)) * buildproduct( M, o(2:end), v{o(1)} )
%
%                           `o`: The format of `o` is not the usually used in the ttoolboxes (and thus may change in future releases)
%                                The first entry of each column is negative and refers to the vector `v` which shall be used for the computation
%                                Only when there is only one vertex `v`, this entry can be ommitted.
%                           
%               
%   'aux',val               default = [], additional options, are used for some algorithms
%                           'c3': integer, greater-equal 2, number of vertices of approximation ellipse
%   'vector'/'matrix'       (currently broken) (experimental) default = 'vector' when input `pts` is an array, 'matrix' when input `pts` is a cell array, whether to compute the vector or matrix norm
%   'bd'',val               default = [], 1x2 vector, when given, computation stops, after norm is inside of specified interval
%                           Specifying an interval may speed up LP/CP computations too (currently 'c4' and 'c5' use this option)
%   'a'/'g'/'m'/'o'/'s'     default = 'a', which solver to use, automatic/gurobi/matlab/octave/sedumi
%                               Currently: 'm' and 'o' both use linprog (from optiomatization toolbox on Matlab or optim toolbox on Octave)
%                               For algorithm 'c', Solver 'm' changes automatically to 's'
%                               On Octave, algorithm 's' is currently not supported
%   'solver',val            default 'a', where val = 'a'/'g'/'m'/'s', different way how to specify the solver
%   'epsilon_display',val   default = 0, only affects the logging
%   'epsilon_points',val    default = 5e-9 for double, 0 for symstrict, accuracy of given points
%   'verbose',val           default = 1,  verbose level
%   'num_core',val           default = matlab default, number of threads used for computation. If num_core == 1, then computation is done in main thread (for gurobi solver)
%   'estimate',val          (deprecated), default = 0, when given, same as specifying 'e' as algorithm 
%   'sym',val               casts input prior computations
%                               0  ... cast to double
%                               1  ... cast to vpa
%                               2  ... cast to sym
%   'output',val            (experimental) default = [1 2 3], vector of integers, Specifies which values to output
%                               1  ... estimated norm
%                               2  ... upper bound of norm
%                               3  ... lower bound of norm
%   
% Output:
%   normval         1xN1xN2 or 2xN1xN2 or 3xN1xN2 array of column vectors, the norms of the points as a vector, 
%                       first row: estimate, 
%                       second row: true upper bound, i.e. norm is less equal than the number in the second row
%                       third row: true lower bound, o.e. norm is greater equal then the number in the third row
%   iv              cell array of index vectors, indices of potential vertices which define the plane used to compute the norm. may be empty
%   iteration       The number of iterations the solver has done, 
%                       if no solver was used, iteration is zero
%   time            wall clock time needed
%   proofed         logical array, when true, the corresponding bound in normval is rigorous
%
% Info:
%   a) This function is optimized to compute the norms of many points wrt to the same polytope
%      If the norm of only one point is computed, this function is very slow.
%   b) epsequal only has an effect on the text-output of this function. For the computation a fixed accuracy is used, depending on the algorithm.
%   c) The functions text-output does not end with a newline-character.
%
% E.g.: [nrm_p,iv,iter,time,proofed] = polytopenorm( [1 1; 1 0], [1 0; 0 1], 'p' )
%       double( nrm_p )
%       [nrm_ps,iv,iter,time,proofed] = polytopenorm( [1 1; 1 0], [1 0; 0 1], 'ps' )
%
% See also: parse_algspec, ipa
%   
% Written by tommsch, 2018

% More indepth explanations of the algorithms
%   'k0':   computes whether a vertex is inside or outside of the kone
%   'k1':   measures how far a point is inside/outside. This ist most likely not even a metric. inside: 0-1, outside: 1-inf. FAILS FOR POINTS ON THE BOUNDARY
%   'k2':   Computes a center of the kone
%           projects all points onto the halfplane defined the center
%           moves all points such that the projected vertices of the kone are around the origin
%           then computes norm of the projected points w.r.t to the projected kone
%   'k3':   || p ||_K = \inf\{ t > 0 : pt \in E_t K \}, where E_t is the multiplicative kone scaling function E_t(K) = [c c^\perp] * diag( 1, t, ... t ) * [c c^\perp]^T * K. This is a non-convex quadratic problem, use 'k7' instead
%   'k4':   || p ||_K = \inf\{ t > 0 : pt \in F_t K \}, where F_t is the additive kone scaling function F_t(K) = K - (t-1)/t c, c \in K^\circ, c\in\RR^s
%   'k5':   || p ||_K = \inf\{ t > 0 : pt \in M_t K \}, where M_t is the minkowski kone scaling function M_t(K) = K \ominus (t-1) c, c \in K^\circ, c\in\RR^s
%   'k6':   || p ||_K = \sup\{ t > 0 : x + (t-1)c \in K \}^{-1}
%   'k7':   || p ||_K = \inf\{ t > 0 : E_{1/t} pt \in K \}. Thus k7 == k3, but k7 can be stated as an LP problem.
%   

%               tommsch, 2020-07-07:    Dropped support for matlab solver for Matlab versions prior to Matlab R2014b
%                                       Added complex-functional norm
%                                       Renamed to polytopenorm
%               tommsch, 2020-07-22:    Behaviour change: Value of 0 is not allowed for num_core anymore
%               tommsch, 2020-09-13:    Functionality of estimatepolytopenorm included in this function now
%                                       Interface change to name-value pairs
%                                       Return values changed
%               tommsch, 2020-09-22:    Added exact computation of CONE- fund MINKFUNCT, added experimental option lowerbound
%               tommsch, 2020-10-23:    Removed prefix IPA_ from algorithm types (e.g. CONEFUNCT becomes CONEFUNCT)
%               tommsch, 2021-01-21:    Added exact computation of MINKFUNCT for degenerate polytopes 
%               tommsch, 2021-02-28:    Added COMPLEXFUNCT_D algorithm
%               tommsch, 2021-10-29:    Behaviour change: Non-finite points now do not lead to a total fail of the computation
%               tommsch, 2022-01-26:    Refactored symbolic computation, interface and added interval arithmetic (needs INTLAB)
%               tommsch, 2022-03-28:    Defect fix, returned `time` is now a scalar. Before it was a vector of 0 to size( pts, 2 ) elements
%               tommsch, 2023-03-29:    Added computation of kone norm, rename old 'conenorm' to 'positive-orthant-norm'
%               tommsch, 2023-06-07:    Added support for non-positive vertices with algorithm 'p'
%               tommsch, 2023-07-06:    Bugfix: scaling of input values did not work correctly
%               tommsch, 2024-05-30:    Behaviour change: Removed interval arithmetic, thus algspecs 'i' and 'I' are no longer valid
%                                       Behaviour change: Option 'S' (instead of 's') must be used more often now to disable automatic execution of LP/CP solver
%               tommsch, 2024-09-04:    Added some support for symbolic computation of kone norm
%               tommsch, 2024-10-14:    Bugfix: Sometimes the auxiliary data got parsed wrongly.
%                                       Behaviour change: The function now errors more often when the input cannot be parsed
% Changelog:    tommsch, 2024-11-06:    Bugfix for case (L) computation
%               tommsch, 2024-11-13:    Bugfix for case (L) computation (Returned norm was inverse)
%               tommsch, 2024-12-04:    fixed wrong display output for symbolic case (K), improvements for symbolic case (K) computation
%               tommsch, 2025-01-09:    Bugfix in norm estimation for case (R)
%               tommsch, 2025-01-17:    Behaviour change: Value of 0 means program shall run in main thread, Value of 1 means program uses one worker

    [ pV, opt, varargin ] = polytopenorm_parse( varargin{:} );
    
    if( ~basic_test( pV, opt ) );
        [normval, iv, iter, time, proofed] = polytopenorm_failed_preworker( opt ); 
    elseif( isequal(opt.vecmat, 'v') );
        [normval, iv, iter, time, proofed] = polytopenorm_vector_preworker( pV, opt, varargin{:} );
    elseif( isequal(opt.vecmat, 'm') );
        [normval, iv, iter, time, proofed ] = polytopenorm_matrix_preworker( pV, opt, varargin{:} );
    else;
        error( 'polytopenorm:fatal', 'Programming error' ); end;
    
    normval(1,:) = clamp( normval(1,:), normval(3,:), normval(2,:) );
    if( isempty(time) );
        time = toc( opt.starttime ); end;
    time = sum( time );
    
    normval = normval(opt.output,:);
    
    %<TTEST:END>

end


%% %%%%%%%%%%%%%%%%%%%%%%%
% parse
%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ pV, opt, varargin ] = polytopenorm_parse( varargin );
    assert( nargin >= 3, 'polytopenorm:option', 'Too less arguments given. Format: polytopenorm( pts, VV, [aux_data], algspec )' );

    persistent installed;
    if( isempty(installed) );
        installed = struct;
        installed.optim  = isequal( exist('optimset.m', 'file'), 2 ) && isequal( exist('linprog.m', 'file'), 2 );
        installed.gurobi = isequal( exist('gurobi_setup.m', 'file'), 2 ) && isequal( exist('gurobi.m', 'file'), 2 );
        installed.matlab = isequal( exist('linprog', 'file'), 2 );
        installed.sedumi = isequal( exist('sedumi_linprog', 'file'), 2 ); end;
    opt.installed = installed;
    
    % parse simple options
    %%%%%%%%%%%%%%%%%%%%%%
    opt.starttime = tic;
    [opt.maxtime, varargin]                  = parsem( {'maxtime'}, varargin, inf );
    [opt.func, varargin]                     = parsem( {'func','funct','algorithm','alg','case'}, varargin, [] );
    [~, varargin, retname, ~, contained]     = parsem( {'jsr','lsr','jssr','kone','cone','sub','minkowski','complex'}, varargin );
    if( contained && isempty(opt.func) );
        opt.func = retname; end;
    [opt.debug, varargin]                    = parsem( {'debug'}, varargin );  % for skipping certain lines which are only here for debugging
    [opt.sym, varargin]                      = parsem( {'sym'}, varargin, [] );
    % [opt.target_type, varargin]              = parsem( {'target_type'}, varargin, [] );  % option not meant to be set by the user
    [opt.output, varargin]                   = parsem( {'output'}, varargin, [1 2 3] );  % must be parsed before 'lowerbound'
    [estimate_, varargin]                    = parsem( {'estimate','est'}, varargin, 0 );
    [opt.verbose, varargin]                  = parsem( {'verbose','v'}, varargin, 1 );
    [~, varargin, opt.vecmat]                = parsem( {'default_vecmat','vector','matrix','vec','mat'}, varargin );
    [opt.epsilon_display, varargin, retname] = parsem( {'epsdisplay','epsilon_display','epsilondisplay','displayepsilon','display_epsilon','epsdisp','eps_disp','epsilon','eps'}, varargin, 0 );
        expect( ~isequal(retname, 'epsilon') && ~isequal(retname, 'eps'), 'polytopenorm:deprecated', 'The options ''epsilon''/''eps'' is renamed to ''epsilon_display''. The old name may get removed in a future release.' );
    [opt.epsilon_pts, varargin]              = parsem( {'epspt','eps_pt','epsilon_points','epsilonpoints','epsilon_point','epsilonpoint','pointepsilon','point_epsilon','pointsepsilon','points_epsilon'}, varargin, [] );
    [opt.num_core, varargin]                 = parsem( {'numcore','nc','numthread','threads','thread','t'}, varargin, number_of_cores );
    [lb, varargin]                           = parsem( {'lowerbound','lb'}, varargin, [] );
    [opt.bound, varargin]                    = parsem( {'bound','bd'}, varargin, [] );    
    assert( implies(~isempty(opt.bound), isempty(lb)), 'polytopenorm:bd', 'When ''bound'' is given, ''lowerbound'' must not be given.' );
    if( isempty(opt.bound) && ~isempty(lb) );
        opt.bound = [lb cast(inf, class(lb))]; end;
        
    [opt.solver, varargin]                   = parsem( {'solver'}, varargin, [] );
    [~, varargin, sol2]                      = parsem( {'auto','gurobi','matlab','sedumi','a','g','m','s','gu','ma','se'}, varargin );
    if( isempty(opt.solver) );
        opt.solver = sol2; end;

    % process default values
    %%%%%%%%%%%%%%%%%%%%%%%%
    switch lower( opt.solver );
        case {'a','auto'};              opt.solver = 'auto';
        case {'g','gu','gurobi'};       opt.solver = 'gurobi';
        case {'m','ma','mat','matlab'}; opt.solver = 'matlab'; 
        case {'s','se','sedumi'};       opt.solver = 'sedumi';
        otherwise; fatal_error; end;

    if( ~isempty(opt.sym) );
        switch opt.sym;
            case {-1,'auto'};   opt.sym = [];
            case { 0,'double'}; opt.sym = 'double';
            case { 1,'vpa'};    opt.sym = 'vpa';
            case { 2,'sym'};    opt.sym = 'sym';
            otherwise; error( 'polytopenorm:option', 'Wrong value for option ''sym''.' ); end; end;

    % parse pts/VV/aux
    %%%%%%%%%%%%%%%%%%
    pts_ = varargin{1};
    VV_ = varargin{2};
    assert( iscell( pts_ )   && ismatrix( VV_ ) && size( pts_{1}, 1 ) == size( VV_, 1 ) || ...  % matrix-norm case
            ismatrix( pts_ ) && ismatrix( VV_ ) && size( pts_, 1 )    == size( VV_, 1 ), ...
            'polytopenorm:input', 'The points ''pts'' and the vertices of the polytope ''VV'' must be given as a matrix of column vectors.' );
    varargin(1:2) = [];
    if( numel( varargin ) >= 1 && ...
        ~(ischar( varargin{1} ) || isstring( varargin{1} )) ...
      );
        aux_ = varargin{1};
        varargin(1) = [];
    else;
        aux_ = []; end;

    opt.vecmat = opt.vecmat(1);  % eiter 'd'efault_vecmat, 'v'ector, or 'm'atrix.
    if( opt.vecmat(1) == 'd' );
        if( iscell( pts_ ) );
            pts_ = cat( 3, pts_{:} );
            opt.vecmat = 'm';
        elseif( ndims(pts_) == 3 && size(pts_, 1) == size(pts_, 2) );
            opt.vecmat = 'm';
        else;
            opt.vecmat = 'v'; end; end;
    if( iscell( pts_ ) );
        pts_ = cat( 3, pts_{:} );
        assert( opt.vecmat == 'm' ); end;

    [~, varargin, ~, pts_arg]   = parsem( {'pts','pt'}, varargin, [] );
    [~, varargin, ~, VV_arg]    = parsem( {'VV','V'}, varargin, [] );
    [~, varargin, ~, aux_arg]   = parsem( {'aux','auxiliary_data','auxiliary'}, varargin, [] );

    if( ~isempty(opt.sym) );
        switch opt.sym;
            case 'auto'; % do nothing
            case 'double'; [pts_, pts_arg, VV_, VV_arg, aux_, aux_arg] = cast_to( pts_, pts_arg, VV_, VV_arg, aux_, aux_arg, 'double' );
            case 'vpa'; [pts_, pts_arg, VV_, VV_arg, aux_, aux_arg] = cast_to( pts_, pts_arg, VV_, VV_arg, aux_, aux_arg, 'vpa' );
            case 'sym'; [pts_, pts_arg, VV_, VV_arg, aux_, aux_arg] = cast_to( pts_, pts_arg, VV_, VV_arg, aux_, aux_arg, 'sym' );
            otherwise; fatal_error; end; end;
    
        
    
    pV = parse_pV( pts_, pts_arg, VV_, VV_arg, aux_, aux_arg );
    pV = compute_num_data( pV );
    least_accurate = pV.least_accurate;
    if( ~( isempty(opt.bound) || issym(opt.bound) || ~strcmp(pV.least_accurate, 'sym') ) );
        [opt.bound, ~] = anycast( opt.bound, 'symrat' );
        assert( issym(opt.bound), 'polytopenorm:bound', '''bound'' should be given as a symbolic if ''pts'' or ''VV'' is a symbolic.'  ); end;
    opt.dim = size( pV.pt.(least_accurate), 1 );
    opt.np = size( pV.pt.(least_accurate), tif( opt.vecmat == 'v', 2, 3 ) );
    
    % parse algorithm
    %%%%%%%%%%%%%%%%%
    [opt.algorithm, opt.method, varargin] = polytopenorm_parse_algspec( pV, opt, estimate_, varargin{:} );
    switch opt.algorithm;
        case {'k3','k4','k5','k6'};
            if( opt.verbose >= 0 );
                warning( 'polytopenorm:kone_method', 'Chosen method is not tested, and thus the returned results are not reliable.' ); end;
        otherwise; end;  % do nothing

    % compute missing algorithmic specific data
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    switch lower( opt.algorithm(1) );
        case 'k';
            if( ~issize0(pV.aux.num) && ~issize0(pV.aux.sym) );
                assert( norm( pV.aux.num - double(pV.aux.sym) ) < 1e-12 );
            elseif( issize0(pV.aux.num) && ~issize0(pV.aux.sym) );
                pV.aux.num = double( pV.aux.sym );
            elseif( issize0(pV.aux.sym) && ~issize0(pV.aux.num) );
                pV.aux.sym = sym( pV.aux.num );
                pV.aux.num = double( pV.aux.sym );
            elseif( ~issize0(pV.VV.num) );
                pV.aux.sym = sym( center_kone( pV.VV.num, 'verbose',opt.verbose - 1 ) );
                pV.aux.num = double( pV.aux.sym );
            elseif( ~issize0(pV.VV.sym) );
                pV.aux.sym = sym( center_kone( double(pV.VV.sym), 'verbose',opt.verbose - 1 ) );
                pV.aux.num = double( pV.aux.sym );
            else;
                error( 'polytopenorm:kone_center', 'Computation of the center of the kone is not implemented for this sets of inputs.' ); end;
        case {'l','p','r','c'};
            % do nothing
        otherwise;
            fatal_error; end;

    
    % set missing data to nan
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if( any( size(pV.pt.num) < size(pV.pt.sym) ) );
        pV.pt.num = nan( size(pV.pt.sym) );
    elseif( any( size(pV.pt.sym) < size(pV.pt.num) ) && ...
            (~isempty(pV.pt.handle) || nnz( opt.method' == 'sS' ) >= 1) ...
          );
        pV.pt.sym = sym( nan( size(pV.pt.num) ) ); end;
    if( any( size(pV.VV.num) < size(pV.VV.sym) ) );
        pV.VV.num = nan( size(pV.VV.sym) );
    elseif( any( size(pV.VV.sym) < size(pV.VV.num) ) && ...
            (~isempty(pV.VV.handle) || nnz( opt.method' == 'sS' ) >= 1) ...
          )
        pV.VV.sym = sym( nan( size(pV.VV.num) ) ); end;
    
    % sanity checks
    %%%%%%%%%%%%%%%%%%%
    % mostly type checks
    switch opt.solver;
        case 'auto'; % do nothing
        case 'matlab'; assert( opt.installed.optim,  'polytopenorm:optim',  'The matlab solver is chosen, but the optimization toolbox does not seem to be installed.' );
        case 'gurobi'; assert( opt.installed.gurobi, 'polytopenorm:gurobi', 'Gurobi is chosen as solver, but Gurobi does not seem to be installed.' );
        case 'sedumi'; assert( opt.installed.sedumi, 'polytopenorm:sedumi', 'Sedumi is chosen as solver, but Sedumi does not seem to be installed.' );
        otherwise; fatal_error; end;

    assert( ~issize0(pV.pt.num) || ~issize0(pV.pt.sym), 'polytopenorm:pts', 'No points given, or points could not be parsed.' );
    assert( ~issize0(pV.VV.num) || ~issize0(pV.VV.sym), 'polytopenorm:VV',  'No polytope given, or polytope could not be parsed.' );
    assert( ~xor(issize0(pV.pt.sym), issize0(pV.VV.sym)), 'polytopenorm:sym',  'If either of pt/VV is given as symbolic, the other must be given too as symbolic.' );
    
    assert( isequal( size(pV.pt.num, 1), size(pV.VV.num, 1) ), 'polytopenorm:dim', 'Dimensions of input data differ.' );
    assert( isequal( size(pV.pt.sym, 1), size(pV.VV.sym, 1) ), 'polytopenorm:dim', 'Dimensions of input data differ.' );
    
    expect( implies(nnz(opt.method' == 'esS') > 1, ~isempty(opt.bound)), 'polytopenorm:bound', 'You most likely forgot to give a ''bound''.' );
    
    check_magnitudes( pV, opt );
    
    if( ischar(opt.output) || isstring(opt.output) );
        switch opt.output;
            case {'ub','upper'};     opt.output = 2;
            case {'lb','lower'};     opt.output = 3;
            case {'est','estimate'}; opt.output = 1;
            otherwise; fatal_error; end; end;
    
end

function [ pV ] = parse_pV( pts_, pts_arg, VV_, VV_arg, aux_, aux_arg );

    persistent pV_base;
    if( isempty(pV_base) );
        pV_base = struct;
        pV_base.pt.num = [];
        pV_base.pt.sym = [];
        pV_base.pt.handle = [];
        pV_base.VV.num = [];
        pV_base.VV.sym = [];
        pV_base.VV.handle = [];
        pV_base.aux.num = [];
        pV_base.aux.sym = [];
        pV_base.aux.handle = [];
        end;
    pV = pV_base;
    
    if( isequal(pts_arg, {[]}) );
        pts_arg = {}; end;
    if( isequal(VV_arg, {[]}) );
        VV_arg = {}; end;
    if( isequal(aux_arg, {[]}) );
        aux_arg = {}; end;
    
    % copy input data to pV struct
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    for name_ = {'pt','VV','aux'}; name = name_{1};
        switch name;
            case 'pt';  dataset = [{pts_} pts_arg(:)'];
            case 'VV';  dataset = [{VV_} VV_arg(:)'];
            case 'aux'; dataset = [{aux_} aux_arg(:)'];
            otherwise; fatal_error; end;
        for data_ = dataset; data = data_{1};
            if( issize0(data) );
                continue;
            elseif( isnumeric(data) );
                pV.(name).num = data;
            elseif( iscell(data) && numel(data) == 1 && isnumeric(data{1}) );
                pV.(name).num = data{1};
            elseif( issym(data) );
                pV.(name).sym = data;
            elseif( iscell(data) && numel(data) == 1 && issym(data{1}) );
                pV.(name).sym = data{1};
            elseif( isa(data, 'function_handle') );
                pV.(name).handle = data;
            elseif( iscell(data) && numel(data) == 1 && isa(data{1}, 'function_handle') );
                pV.(name).handle = data;
            elseif( iscell(data) && numel(data) == 4 && iscell(data{2}) );
                pV.(name).handle = oMvs_to_handle( data{:} );
            elseif( iscell(data) && isempty(data) );
                % do nothing
            else;
                error( 'polytopenorm:input', 'Could not parse input.' ); end; end; end;
end

function [ pV ] = compute_num_data( pV );
    for name_ = {'pt','VV','aux'}; name = name_{1};
        if( ~isempty(pV.(name).sym) && isempty(pV.(name).num) );
            pV.(name).num = double( pV.(name).sym ); end; end;
    if( ~isempty(pV.aux.handle) && isempty(pV.aux.sym) );
        pV.aux.sym = pV.aux.handle(); end;
    if( ~isempty(pV.aux.sym) && isempty(pV.aux.num) );
        pV.aux.num = double( pV.aux.sym ); end;

    % set .least_accurate
    %%%%%%%%%%%%%%%%%%%%%%%%
    % .least_accurate is the least accurate passed type,
    % is used for sanity checks etc.
    % pV.(name).(pV.least_accurate) is never empty, but may be unevaluated (i.e. all column elements set to nan)
    
    if( ~issize0(pV.pt.num) );
        pV.least_accurate = 'num';
    elseif( ~issize0(pV.pt.sym) );
        pV.least_accurate = 'sym';
    else;
        error( 'polytopenorm:input', 'No points given.' ); end; 
end

function [ algorithm, method, varargin ] = polytopenorm_parse_algspec( pV, opt, estimate_, varargin );
    least_accurate = pV.least_accurate;
    if( ~isempty(opt.func) );
        % translate algspec shorthands
        switch lower( opt.func );
            case {'jsr'}; opt.func = 'a';
            case {'mink','minkowski'}; opt.func = 'r';
            case {'complex','elliptic'}; opt.func = 'c';
            case {'lsr','jssr','sub',}; opt.func = 'l';
            case {'kone','cone'}; opt.func = 'k';
            otherwise;  % do nothing
            end;
        algspec = opt.func;
    else;
        assert( numel(varargin) >= 1, 'polytopenorm:funct', 'Could not parse input - could not find which algorithm to use.' );
        algspec = varargin{1};
        varargin(1) = []; end;
    
    [algorithm, method] = parse_algspec( algspec, pV.pt.(least_accurate), pV.VV.(least_accurate) );

    % translate_algspec
    switch algorithm;
        case 'k';   algorithm = 'k7';
        case 'k*1'; method = 'E';
        otherwise;  % do nothing
        end;
    
    switch estimate_;
        case -1; method = 'S';
        case  0; % do nothing
        case  1; method = 'E';
        case  2; warning( 'polytopenorm:deprecated', 'Option `estimate` with value `2` is removed and has no effect.' );
        otherwise; error( 'polytopenorm:option', 'Wrong value for ''estimate'' passed.' ); end;
        
    % if a bound is given, then we do an estimate and a numeric LP/CP computation
    % (unless uppercase letters are given)
    if( ~isempty(opt.bound) && ~anym( method' == 'ENIS' ) );
        method = ['en' method]; end;
    
    if( strcmpi(method, 'e') );
        capsflag = ~isequal( lower(method), method );
        switch least_accurate;
            case 'num'; method = 'e';
            case 'sym'; method = 'g';
            otherwise; error( 'polytopenorm:fatal', 'Programming error.' ); end;
        method = tif( capsflag, upper(method), method ); end;
       
    
    if( ~anym( method' == 'gEGNS' ) );
        if( ~isempty(pV.pt.num) );
            method = [method 'n']; end;
        if( ~isempty(pV.pt.sym) || ~isempty(pV.pt.handle) );
            method = [method 's']; end; end;
    
    method = unique( method, 'stable' );        
end

function check_magnitudes( pV, opt );
    % check magnitudes of input data, and give a warning if input data is badly scaled
    if( opt.verbose < 0 || ...
        isempty( pV.pt.num ) || isempty( pV.VV.num ) ...
      );
        return; end;
    
    idx1 = abs( pV.pt.num(:) ) > 0;
    idx2 = abs( pV.VV.num(:) ) > 0;
    [~, mx1] = minmax1( abs(pV.pt.num(idx1)) );
    [~, mx2] = minmax1( abs(pV.VV.num(idx2)) );
    %mn = min( [mn1 mn2] );
    mx = min( [mx1 mx2] );
    
    %expect( mx/mn < 1e12, 'polytopenorm:range', 'The magnitudes of the input data are of a very wide range. The result may be plainly wrong.' );
    expect( mx < 1e12, 'polytopenorm:range', 'The input data is of very large magnitude. The result may be plainly wrong.\n  You can try to scale the input by hand.' );
end

function [ flag ] = basic_test( pV, opt );
    least_accurate = pV.least_accurate;
    if( anym(~isfinite(pV.VV.(least_accurate))) );
        vprintf( 'Nonfinite values in data. Results are nonfinite.\n', 'imp',[1 opt.verbose] );
        flag = false;
        return; end;
    if( isempty(pV.pt.(least_accurate)) || isempty(pV.VV.(least_accurate)) );
        vprintf( 'No data given, or data could not be parsed. Results are nonfinite.\n', 'imp',[2 opt.verbose] );
        flag = false;
        return; end;
    if( any(opt.algorithm(1) == 'rc') && ~any(lower(opt.method) == 's') );
        try;
            rnk = inf;
            rnk = rank( [real(pV.VV.(least_accurate)) imag(pV.VV.(least_accurate))] );
        catch me;  %#ok<NASGU>
            end;
        if( rnk < opt.dim );
            vprintf( 'Rank of input data is too small. Results are nonfinite.\n', 'imp',[2 opt.verbose] );
            flag = false;
            return; end; end;
    
    switch opt.algorithm(1);
        case 'p';
            if( ~(isempty(pV.pt.num) || all(isnan(pV.VV.num(:))) || isAlways(all(pV.VV.num(:) >= 0))) || ...
                ~(isempty(pV.pt.sym) || all(isnan(pV.VV.sym(:))) || isAlways(all(pV.VV.sym(:) >= 0))) ...
              );
                vprintf( 'For algorithm ''p'', the vertices ''VV'' must only have non-negative entries. Results are nonfinite.\n', 'imp',[2 opt.verbose] );
                flag = false;
                return; end;
        case {'k','l','r','c'};
            % do nothing     
        otherwise;
            fatal_error; end;
        
    flag = true;
        
end
%%
% eval_oMvs
%%%%%%%%%%%%%%%%

function [ h ] = oMvs_to_handle( o, M, v, s );
    if( nargin == 3 );
        s = 1; end;
    v = [v{:}];
    h = @( idx ) eval_oMvs( o, M, v, s, idx );
end

function [ data ] = eval_oMvs( o, M, v, s, idx );
    dim = size( M{1}, 1 );
    np = nnz( idx );
    data = anycast( zeros(dim, np), class(M{1}) );
    j = 0;
    for i = find( idx );
        j = j + 1;
        len = nnz( o(2:end,i) );
        data(:,j) = s^(-len) * buildproduct_fast( M, o(2:end,i), v(:,-o(1,i)) );
        end;
end

function [ pV ] = compute_missing( pV, idx, type, opt );

    if( isequal(idx, 'all') );
        idx = true( 1, size(pV.num, 2) ); end;

    switch type;
        case {'num','double'};
            idx = all( isnan(pV.num), 1 ) & idx;
            if( any(idx) );
                if( ~isempty(pV.sym) );
                    pV.num(:,idx) = anycast( pV.sym(:,idx), 'double' );
                elseif( ~isempty(pV.handle) );
                    pV.num(:,idx) = anycast( pV.handle( idx ), 'double' );
                else;
                    error( 'polytopenorm:data', 'Given input data is not sufficient. Cannot compute numeric data.' ); end; end;

        case 'sym';
            idx = all( isnan(pV.sym), 1 ) & idx;
            if( any(idx) );
                if( ~isempty(pV.sym) );
                    [pV_num_casted, err] = anycast( pV.num(:,idx), 'symrat' );
                    if( ~err );
                        pV.sym(:,idx) = pV_num_casted;
                        % do nothing, everything good
                    elseif( err && ~isempty(pV.handle) );
                        pV.sym(:,idx) = pV.handle( idx );
                    else;
                        error( 'polytopenorm:data', 'Given input data is not sufficient. Cannot compute symbolic data.' ); end;
                elseif( ~isempty(pV.handle) );
                    pV.sym(:,idx) = anycast( pV.handle( idx ) );
                else;
                    error( 'polytopenorm:data', 'Given input data is not sufficient. Cannot compute symbolic data.' ); end; end;

            if( ~isempty(pV.num) && all(isfinite(pV.num(:))) );
                switch lower( opt.algorithm(1) );
                    case {'p','r','c','l'};
                        nrm_diff = norm( pV.num(:,idx) - double(pV.sym(:,idx)) );
                        assert( nrm_diff < 1e-12, 'polytopenorm:symbolic', 'Computation of vertices from given symbolic data, differs significantly from given numeric values.\n  Norm of difference: %f', nrm_diff );
                    case {'k'};
                        % % This is really! slow
                        % pV_num = vpa( pV.num(:,idx) );
                        % pV_sym = vpa( pV.sym(:,idx) );
                        % nrm_diff = norm( normalizematrix(pV_num, 'colnorm',1) - normalizematrix(pV_sym, 'colnorm',1) );
                        % assert( isAlways(nrm_diff < 1e-12, 'Unknown','true'), 'polytopenorm:symbolic', 'Computation of vertices from given symbolic data, differs significantly from given numeric values.\n  Norm of difference: %f', nrm_diff );
                    otherwise;
                        fatal_error; end; end;

        otherwise;
            fatal_error; end;
end

%%
% pre worker
%%%%%%%%%%%%%%%%%%%%%%%%
function [normval , iv, iter, time, proofed ] = polytopenorm_failed_preworker( opt ); 
    time = 0;
    iv = cell( 1, opt.np );
    iter = 0;
    normval = [ones(1,opt.np); inf(1,opt.np); zeros(1,opt.np) ];
    proofed = false( 3, opt.np );
end

function [ normval, iv, iter, time, proofed ] = polytopenorm_vector_preworker( pV, opt, varargin );

    switch opt.algorithm(1);
        case 'p';
            if( ~isempty(pV.pt.num) );
                assert( all(isnan(pV.VV.num(:))) || isAlways(all(pV.VV.num(:) >= 0)), 'polytopenorm:p', 'For algorithm ''p'', the polytope must only have non-negative entries.' );
                expect( all(isnan(pV.pt.num(:))) || isAlways(all(pV.pt.num(:) >= 0)), 'polytopenorm:p', 'For algorithm ''p'', the points only should have non-negative entries. I take the modulus.' );
                pV.pt.num = abs( pV.pt.num ); end;
            if( ~isempty(pV.pt.sym) );
                assert( all(isnan(pV.VV.sym(:))) || isAlways(all(pV.VV.sym(:) >= 0)), 'polytopenorm:p', 'For algorithm ''p'', the polytope must only have non-negative entries.' );
                expect( all(isnan(pV.pt.sym(:))) || isAlways(all(pV.pt.sym(:) >= 0)), 'polytopenorm:p', 'For algorithm ''p'', the points only should have non-negative entries. I take the modulus.' );
                pV.pt.sym = abs( pV.pt.sym ); end;
        case {'r','c','k','l'};
            % do nothing        
        otherwise;
            fatal_error; end;  
    
    [normval, iv, iter, time, proofed] = polytopenorm_preworker( pV, opt, varargin{:} );
end

function [ normval, iv, iter, time, proofed ] = polytopenorm_matrix_preworker( pV, opt, varargin );
    % XX this is far from optimized
    normval = [ones(1,opt.np); inf(1,opt.np); zeros(1,opt.np) ];
    
    iv = [];
    proofed = [];
    iter = 0;
    time = 0;
    
    np = opt.np;
    
    least_accurate = pV.least_accurate;
    opti = opt;
    opti.np = size( pV.VV.(least_accurate), 2 );
    vprintf( [repmat( '|', [1 opti.np] ) ' (number of matrices: %i)\n'], np, 'imp',[1 opt.verbose] );
    for i = 1:np;
        if( toc(opt.starttime) > opt.maxtime );
            break; end;
        pVi = pV;
        % generate data for matrix norm
        if( ~issize0(pVi.pt.num) && ~allm(isnan(pVi.pt.num(:,:,i))) && ~issize0(pVi.VV.num) && ~allm(isnan(pVi.VV.num)) );
            pVi.pt.num = pV.pt.num(:,:,i) * pV.VV.num; end;
        if( ~issize0(pVi.pt.sym) && ~allm(isnan(pVi.pt.sym(:,:,i))) && ~issize0(pVi.VV.sym) && ~allm(isnan(pVi.VV.sym)) );
            pVi.pt.sym = pV.pt.sym(:,:,i) * pV.VV.sym; end;
        [normval_i, ~, iter_i, time_i] = polytopenorm_preworker( pVi, opti, varargin{:} );
        vprintf( '\n', 'imp',[1 opt.verbose] );
        iter = iter + iter_i;
        time = time + time_i;
        normval(:,i) = max( normval_i, [], 2 );
    end;
end

function [ normval, iv, iter, time, proofed ] = polytopenorm_preworker( pV, opt, varargin );

    iv = cell( 1, opt.np );
    time = 0;
    iter = 0;
    proofed = false( 3, opt.np );  % true where upper and lower bound are mathematically rigorous
    least_accurate = pV.least_accurate;
    normval = cast( [ones(1, opt.np); inf(1, opt.np); zeros(1, opt.np) ], class(pV.pt.(least_accurate)) );

    % function handle, non-finite points
    idx_finite = all( isfinite(pV.pt.(least_accurate)), 1 );

    % function handle, points which are uncomputed so far
    idx_uncomputed = @(normval) isAlways( normval(1,:) == 1 & normval(2,:) == inf & normval(3,:) == 0 ...  % uncomputed yet
                                        );
    % function handle, get points not crossing any boundary of the interval
    idx_nonbordercrossing = @(normval) isAlways( normval(3,:) >= max(opt.bound) | ...
                                                 normval(3,:) >= min(opt.bound) & normval(2,:) <= max(opt.bound) | ...
                                                 normval(2,:) <= min(opt.bound) ...
                                               );

    % estimates
    %%%%%%%%%%%%%%%%%%%%
    % numerical estimate
    if( toc(opt.starttime) > opt.maxtime );
        return; end;
    if( anym(opt.method' == 'egEG') && ~anym(opt.method' == 'S') && ~issize0(pV.pt.num) );
        %<TTEST:NUM_ESTIMATE>
        if( any(idx_finite) );
            pV.pt = compute_missing( pV.pt, idx_finite, 'double', opt );
            pV.VV = compute_missing( pV.VV, 'all', 'double', opt );
            
            % it can happen, that numeric approximations are non-finite although symbolic ones are
            if( anym(~isfinite(pV.pt.num(:,idx_finite))) || ...
                anym(~isfinite(pV.VV.num)) ...
              );
                % do not compute anything
            else;
                [ nrm_est ] = polytopenorm_estimate( pV.pt.num(:,idx_finite), pV.VV.num, pV.aux.num, opt );
                nrm_est(3,isnan( nrm_est(3,:) )) = 0;
                nrm_est(2,isnan( nrm_est(2,:) )) = inf;
                assert( ~isempty(nrm_est), 'polytopenorm:fatal', 'Programming error' );
                normval(:,idx_finite) = nrm_est; end; end; end;

    % symbolic estimate
    % we only check results for which we have numerical indication that its worth it
    if( ~isempty(opt.bound) && ~issize0(pV.pt.sym) || ...
        anym(opt.method' == 'gG') ...
      );
        if( any(opt.method' == 'gG') );
            idx = idx_finite;
        else;
            idx = idx_finite & (idx_nonbordercrossing (normval ) | idx_uncomputed( normval )); end;
        if( any(idx) );
            pV.pt = compute_missing( pV.pt, idx, 'sym', opt );
            pV.VV = compute_missing( pV.VV, 'all', 'sym', opt );
            [ normval_sym ] = polytopenorm_estimate( pV.pt.sym(:,idx), pV.VV.sym, pV.aux.sym, opt );
            normval = anycast( normval, class(normval_sym) );
            idx_better_ub = isAlways( normval_sym(2,:) < normval(2,idx), 'Unknown','false' );
            idx_better_lb = isAlways( normval_sym(3,:) > normval(3,idx), 'Unknown','false' );
            proofed(2:3,idx_better_ub & idx_better_lb) = true;
            idx_set_lb = idx;
            idx_set_lb(idx_set_lb) = idx_better_lb;
            normval(:,idx_set_lb) = normval_sym(:,idx_better_lb);

            idx_set_ub = idx;
            idx_set_ub(idx_set_ub) = idx_better_ub;
            normval(:,idx_set_ub) = normval_sym(:,idx_better_ub);
        
        end; end;

    % LP/CP
    %%%%%%%%%%%%
    if( toc(opt.starttime) > opt.maxtime );
        return; end;
    if( anym( opt.method' == 'N' ) || ...
        ~anym( opt.method' == 'S' ) && anym( opt.method' == 'n' ) || ...
        anym( opt.method' == 's' ) && ~anym( opt.method' == 'S' ) && ~issize0( pV.pt.num ) && ~isempty( opt.bound ) ...
      );
        if( ~isempty(opt.bound) );
            idx = idx_finite & ...
                  (~idx_nonbordercrossing( normval ) | idx_uncomputed( normval ));
        else;
            idx = idx_finite; end;
        nVV = size( pV.VV.num, 2 );
        idx_nonzero = any( pV.VV.num, 1 );
        [~, idx_unique, ~] = unique( pV.VV.num', 'rows' );
        idx_unique = unfind( idx_unique, nVV )';
        idx_VV = idx_nonzero & idx_unique;
        if( any(idx) );
            if( any(idx_nonzero) );
                pV.pt = compute_missing( pV.pt, idx, 'double', opt );
                pV.VV = compute_missing( pV.VV, 'all', 'double', opt );
                %<TTEST:LPCP>
                [ nrm_lpcp, iv_lpcp, iter, time ] = polytopenorm_lpcp( pV.pt.num(:,idx), pV.VV.num(:,idx_VV), pV.aux.num, opt, varargin{:} );
                normval(:,idx) = nrm_lpcp;
                fi = find( idx );
                for i = 1:nnz( idx )
                    if( ~isempty(iv_lpcp{i}) );
                        iv{fi(i)} = false( 1, nVV );
                        iv{fi(i)}(idx_VV) = iv_lpcp{i}; end; end;
            else;
                normval(:,idx) = inf( 3, nnz(idx) ); end; end; 
        assert( all(isAlways(normval(3,idx) <= normval(2,idx), 'Unknown','true')), 'polytopenorm:fatal', 'Fatal error. Lower bound is greater than upper bound.' ); end;

    % check corner cases and proof again with symbolic computation
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if( toc(opt.starttime) > opt.maxtime );
        return; end;
    if( ~isempty(opt.bound) && anym(opt.method' == 's') || ...
        anym(opt.method' == 'S') ...
      );
        if( ~isempty(opt.bound) );
            idx = idx_finite & ...
                  ~idx_nonbordercrossing( normval ) & ...
                  ~idx_uncomputed( normval );
        else;
            idx = idx_finite; end;
        if( any(idx) );
            try;
                if( ~issym(normval) );
                    normval = sym( normval, 'f' ); end;
                pV.pt = compute_missing( pV.pt, idx, 'sym', opt );
                pV.VV = compute_missing( pV.VV, 'all', 'sym', opt );

                %<TTEST:SYM>
                nrm_sym = polytopenorm_exact( pV.pt.sym(:,idx), pV.VV.sym, pV.aux.sym, normval(:,idx), iv(idx), opt );
                idx_ub = false( size(idx) );
                idx_lb = false( size(idx) );
                idx_est = false( size(idx) );
                idx_ub(idx) = isAlways( nrm_sym(2,:) <= normval(2,idx), 'Unknown','false' );
                idx_lb(idx) = isAlways( nrm_sym(3,:) >= normval(3,idx), 'Unknown','false' );
                idx_est(idx) = isAlways( nrm_sym(1,:) <= normval(2,idx) & nrm_sym(2,:) >= normval(3,idx), 'Unknown','false' );
                %idx_ub_lb = idx_ub & idx_lb;
                proofed(2,idx_ub) = true;
                proofed(3,idx_lb) = true;
                normval(2,idx) = simplify( min( nrm_sym(2,:), normval(2,idx) ) );
                normval(3,idx) = simplify( max( nrm_sym(3,:), normval(3,idx) ) );
                %normval(3,idx) = min( normval(2,idx), normval(3,idx) );
                %normval(1,idx) = clamp( normval(1,idx), normval(3,idx), normval(2,idx) );
                %normval(2:3,idx_ub_lb) = [nrm_sym(2:3,idx_ub_lb)];
                normval(1,idx_est) = nrm_sym(1,idx_est(idx_est));
                assert( all(isAlways(normval(3,idx) <= normval(2,idx), 'Unknown','true')), 'polytopenorm:fatal', 'Fatal error. Lower bound is greater than upper bound.' );
            catch me;
                switch me.identifier;
                    case 'symbolic:kernel:DivisionByZero';
                        vprintf( '\nDivision through zero in symbolic expression. This is most likely a Matlab bug. Returned values may be wrong.\n', 'cpr','err', 'imp',[0 opt.verbose] );
                    otherwise;
                        rethrow( me ); end; end; end; end;
end

%% %%%%%%%%%%%%%%%%%%%%
% estimates
%%%%%%%%%%%%%%%%%%%%%%%%%

function [ normval ] = polytopenorm_estimate( pts, VV, aux, opt );
    switch opt.algorithm(1);
        case 'l';
            normval = polytopenorm_antiposest( pts, VV, aux, opt );
        case 'k';
             normval = polytopenorm_koneest( pts, VV, aux, opt );
        case 'p';
            normval = polytopenorm_posest( pts, VV, aux, opt );
        case 'r';
            normval = polytopenorm_minkest( pts, VV, aux, opt );
        case 'c';
            normval = polytopenorm_complexest( pts, VV, aux, opt );
        otherwise;
            error( 'polytopenorm:algorithm', 'wrong algorithm given' ); end;
end

function normval = polytopenorm_antiposest( pts, ~, ~, ~ );
    normval = repmat( [1; inf; 0], [1 size(pts, 2)] );
end

function [ normval ] = polytopenorm_koneest( pts, VV, aux, opt );
    switch opt.algorithm;
        case {'k0','k1','k2','k3','k4','k5','k6','k7'};
            normval = polytopenorm_koneest_pinv( pts, VV, aux, opt );
        case  {'k*1','k*'};
            normval = polytopenorm_koneest_dual( pts, VV, aux, opt );
        otherwise;
            fatal_error; end;
end

function [ normval ] = polytopenorm_koneest_pinv( pts, VV, ~, ~ );
    normval = repmat( [1;inf;0], [1 size(pts, 2)] );
    if( issym(VV) || issym(pts) );
        return; end;

    reset_lastwarn = onCleanup_lazy( @(x,y) lastwarn(x, y), @lastwarn );  %#ok<NASGU>
    reset_warnings1 = onCleanup_lazy( @(x) warning(x), warning('off', 'MATLAB:rankDeficientMatrix') );  %#ok<NASGU>
    reset_warnings2 = onCleanup_lazy( @(x) warning(x), warning('off', 'symbolic:mldivide:RankDeficientSystem') );  %#ok<NASGU>
    reset_warnings3 = onCleanup_lazy( @(x) warning(x), warning('off', 'symbolic:mldivide:InconsistentSystem') );  %#ok<NASGU>

    dim = size( pts, 1 );
    %pts = pts./(sum( abs(pts.^2), 1 ).^(1/2));
    %VV = VV./(sum( abs(VV.^2), 1 ).^(1/2));
    
    eps = tif( issym(pts) && issym(VV), 0, 5e-12 );
    for i = 1:size( pts, 2 );
        [~, co, resnorm] = evalc( 'lsqnonneg( VV, pts(:,1) );' );
        if( isAlways( resnorm > eps, 'Unknown','true' ) );
            continue; end;
        inside = all( isAlways( co >= eps ), 1 );
        boundary =  sum( isAlways( co >= eps ) < dim, 1 ) && all( isAlways( co >= 0 ), 1 );
        if( boundary );
            normval(:,i) = [1;1+eps;0];
        elseif( inside );
            normval(:,i) = [0.5;1;0]; end; end;
end

function [ normval ] = polytopenorm_koneest_dual( pts, VV, aux, opt );  %#ok<INUSD>
    epsilon = 5e-9;
    normval = zeros( 1, size(pts, 2) );
    for i = 1:size( pts, 2 );
        normval(i) = min( sum( pts(:,i) .* VV ) ); end;
    normval = max( 0, 1 - normval );
    normval = [normval; normval + epsilon; normval - epsilon];
end

function [ normval ] = polytopenorm_posest( pts, VV, aux, opt );  %#ok<INUSD>
    % norm values for inside/outside
    assert( all( isAlways( VV(:) >= 0, 'Unknown','true' ) ), 'polytopenorm:pE', 'The estimate for the norm for case (P) works only if all vertices coordinates are nonnegative.' );
    VV(VV == 0) = 0;  % take care of '-0'
    if( all(issymstrict(pts(:))) && all(issymstrict(VV(:))) );
        epsilon = 0;
    elseif( isnumeric(pts) || isnumeric(VV) );
        epsilon = tif( isempty(opt.epsilon_pts), 5e-9, opt.epsilon_pts );
    else;
        epsilon = sym( 10^(-digits+6) ); end;
    np = size( pts, 2 );
    est_in1 = zeros( 1, np );
    est_out1 = zeros( 1, np );
    if( ~isa(pts, 'double') );
        est_in1 = anycast( zeros( 1, np ), class(pts) );
        est_out1 = anycast( zeros( 1, np ), class(pts) ); end;
    normpt = sum( pts.^2, 1 );
    for i = 1:np;
        pt = pts(:,i);
        est_in1(i) = min( max(pt./VV, [], 1) );  % Compute taxi-difference of pt to hypercubes defined by VV, this is a true upper bound
        %est_out4(i) = max( min(pt./VV, [], 1) );  % look in how many anti-cubes the point lies, this is only a true characterization if there are no interior points in VV
        est_out1(i) = min( normpt(i)./(pt.'*VV) ); end;  % Project all vertexpoints onto the vector defined by p. this is a true lower bound
    
    est_out1( ~isfinite(est_out1) ) = 0;
    assert( isAlways( min(est_in1 - est_out1) >= -epsilon, 'Unknown','true' ) );
    est_in1 = max( est_in1, est_out1 );
    normval = [(est_in1+est_out1)/2;
               est_in1 + epsilon;
               max(est_out1 - epsilon,0) ];
end

function [ normval ] = polytopenorm_minkest( pts, VV, aux, opt );
    if( contains(opt.method, {'e','e1'}) ); normval = polytopenorm_minkest_proj( pts, VV, aux, opt );
    elseif( contains(opt.method, {'e2'}) ); normval = polytopenorm_minkest_diff( pts, VV, aux, opt );
    elseif( contains(opt.method, {'e3'}) ); error( 'polytopenorm:fatal', 'This method is currently not implemented.' );
    else;                                   normval = polytopenorm_minkest_1norm( pts, VV, aux, opt ); end;
end

function [ normval ] = polytopenorm_complexest( pts, VV, aux, opt );
    VV = [real( VV ) imag( VV )];
    ptsr = real( pts );
    ptsi = imag( pts );

    if( contains(opt.method,'e1') );
        nrmr = polytopenorm_minkest_proj( ptsr, VV, aux, opt );
        nrmi = polytopenorm_minkest( ptsi, VV, aux, opt );
    elseif( contains(opt.method,'e2') );
        nrmr = polytopenorm_minkest_diff( ptsr, VV, aux, opt );
        nrmi = polytopenorm_minkest_diff( ptsi, VV, aux, opt );
    elseif( contains(opt.method,'e3') );
        error( 'polytopenorm:fatal', 'This method is currently not implemented.' );
    else;
        nrmr = polytopenorm_minkest_1norm( ptsr, VV, aux, opt );
        nrmi = polytopenorm_minkest_1norm( ptsi, VV, aux, opt ); end;

    normval(2,:) = sqrt(2) * max( nrmr(2,:), nrmi(2,:) );
    normval(3,:) = max( 0, 1/sqrt(2) * min( nrmr(3,:), nrmi(3,:) ) );
    normval(1,:) = mean( normval(2:3,:), 1 );
            
end

function [ normval ] = polytopenorm_minkest_1norm( pts, VV, aux, opt );  %#ok<INUSD>
        clean_warn = onCleanup_lazy( @warning, warning('off','symbolic:mldivide:RankDeficientSystem') );  %#ok<NASGU>
        nVV = size( VV, 2 );
        dim = size( VV, 1 );
        np = size( pts, 2 );
        est_in1 = inf( 1, np );
        idx = randperm( nVV );
        [w_msg, w_id] = lastwarn();
        try;
            for i = 1:2*dim:nVV
                VV_rand = VV(:,idx(i:min( nVV, i + 2*dim )));
                if( rank(VV_rand) == dim );
                    co_rand = VV_rand\pts;
                    est_in_rand = sum( abs(co_rand), 1 );
                    err_rand = 10^(-floor(15-log10(cond(VV_rand)) - 1));
%                    if( ~issymstrict(est_in_rand) );
%                        est_in_rand = est_in_rand + max( sum( VV_rand.^2, 1 ).^(1/2) )*norm( VV_rand*co_rand - pts, 1 ); end;
                    est_in1 = min( est_in1, est_in_rand+err_rand ); end; end;  % norm of the point corresponding to an inscribed rectangle
            if( rank(VV) == dim );
                co_full = VV\pts;
                err_full = 10^(-floor(15-log10(cond(VV)) - 1));
                est_in_full = sum( abs(co_full), 1 );
%                if( ~issymstrict(est_in_full) );
%                    est_in_full = est_in_full +  max( sum( VV_rand.^2, 1 ).^(1/2) )*norm( VV*co_full - pts, 1 ); end;
                est_in1 = min( est_in1, est_in_full+err_full ); end;
        catch me;  %#ok<NASGU>
            end;  % do nothing
        lastwarn( w_msg, w_id );;

        normpts = sum( pts.^2, 1 );
        try;
            est_out1 = min( abs((normpts.')./(pts.'*[VV -VV])), [], 2 ).';  % projection onto pts
            double( est_out1 );  % there is a matlab bug, resulting in senseless symbolic expression
        catch me;  %#ok<NASGU>
            est_out1 = 0; end;
        
        if( isa(VV,'numeric') );
            pinvVV = pinv( [VV -VV] ); end;
        try;
            if( ~isa(VV,'sym') );
                est_out2 = sum( abs(pinvVV*pts).^2, 1 ).^(1/2);  % exponent must be 2
            else;
                est_out2 = zeros( 1, np ); end;
            double( est_out2 );
        catch me;  %#ok<NASGU>
            est_out2 = 0; end;
        normlb = max( est_out1, est_out2 );
        
        al = 160*(dim).^(-2)+.7;
        normval = [(normlb+al*est_in1)/(al+1); est_in1; normlb];

end

function [ normval ] = polytopenorm_minkest_proj( pts, VV, aux, opt );  %#ok<INUSD>
        % searches a good vector to project on,
        % should work for any dimension
        %VV = [VV -VV];
        
        np = size( pts, 2 );
        VVt = VV.';
        R = 5*max( sum(VV.^2, 1) )^(1/2);  % radius on sphere where to search
        pn_est = zeros( 1, np );
        normpts = sum(pts.^2, 1).^(1/2);
        
        optmin.TolFun = 1e-1;
        optmin.TolX = 1e-1;
        optmin.MaxFunEvals = 20;
        optmin.Display = 'off';
        if( opt.num_core >= 1 );
            parfor k = 1:np;
                pn_est(k) = polytopenorm_minkest_proj_for_loop( pts(:,k), R, VV, VVt, optmin, normpts(k) ); end;
        else;
            for k = 1:np;
                pn_est(k) = polytopenorm_minkest_proj_for_loop( pts(:,k), R, VV, VVt, optmin, normpts(k) ); end; end;
        normval = [pn_est; inf( 1, np ); pn_est];
end

function  [ pn_est ] = polytopenorm_minkest_proj_for_loop( pt, R, VV, VVt, optmin, normpt );
    SCOx0 = cart2sphm2( pt );
    fun = @(x) diffdistance_worker( (sph2cartm2(x)*R).', VVt );
    
    SCOx = fminsearch( fun, SCOx0, optmin );
    if( isnan(SCOx) );
        SCOx = SCOx0; end;
    dir = sph2cartm2( SCOx );
    pn_est = min( abs((pt.'*dir)./(pt.'*[VV -VV])), [], 2 ).'.*normpt;
end

function [ normval ] = polytopenorm_minkest_diff( pts, VV, aux, opt );  %#ok<INUSD>
    np = size( pts, 2 );
    normval = zeros( 1, np );
    pts = pts.';
    VV = VV.';
    parfor i = 1:size( np, 2 );
        normval(i) = diffdistance_worker( pts(i,:), VV, aux ); end;
    normval = [normval; inf( 1, np ); zeros( 1, np )];
end

function [ dm ] = diffdistance_worker( p, VV, ~ );
    % expects p and VV as row vectors
    p = double( p );
    VV = double( VV );
    d1 = pdist2( p, VV ).';
    d2 = pdist2( p, -VV ).';

    NORM = sum( VV.^2, 2 ).^(1/2);

    % % misst Abstand 
    %d = sort( min(d1,d2) );
    %if( length(d) >= 1 ); 
    %   d = d(1); 
    %else; 
    %   d = inf; end;

    % findet Punkte die weit weg von anderen Punkten sind UND die innerhalb sind
    d1 = d1./NORM;
    d2 = d2./NORM;
    dm = max( abs(d1 - d2) );
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%
% LP/CP computations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ normval, iv, iter, time ] = polytopenorm_lpcp( pts, VV, aux, opt, varargin );
    % since not all methods are implemented for all solvers and
    % some algorithms need special care, there is an intermediate layer
    iv = cell( 1, size(pts, 2) );
    iter = 0;
    time = 0;
    N = size( pts, 2 );
    normval = repmat( [1;inf;0], [1 N] );

    if( isequal( opt.solver, 'auto' ) );    
        switch opt.algorithm;
            case 'c4';
                assert( opt.installed.sedumi, 'polytopenorm:sedumi_c4', 'Case ''c4'' is only possible with Sedumi solver, but it is not installed.' );
                opt.solver = 'sedumi';
            case 'c5';
                assert( opt.installed.gurobi, 'polytopenorm:gurobi_c5', 'Case ''c5'' is only possible with Gurobi solver, but it is not installed.' );
                opt.solver = 'gurobi';
            case {'p','r','l'};
                if( opt.installed.gurobi );
                    opt.solver = 'gurobi';
                elseif( opt.installed.optim );
                    opt.solver = 'matlab'; 
                elseif( opt.installed.sedumi );
                    opt.solver = 'sedumi'; 
                else;
                    error( 'polytopenorm:solver', 'No capable solver is installed. Install either Sedumi, Gurobi, or the optimization toolbox.' ); end;
            case {'k0','k*1','k1','k2','k3','k4','k5','k6','k7'};
                assert( opt.installed.gurobi, 'polytopenorm:gurobi_c5', 'Kone cases are only possible with Gurobi solver, but it is not installed.' );
                opt.solver = 'gurobi';
            case {'c','c1','c2','c3'};
                if( opt.installed.gurobi );
                    opt.solver = 'gurobi';
                elseif( opt.installed.sedumi );
                    opt.solver = 'sedumi';
                else;
                    error( 'polytopenorm:solver', 'No capable solver is installed. Install either Sedumi or Gurobi.' ); end;
            otherwise;
                fatal_error; end; end;

    switch opt.algorithm;
        case {'k0','k*1','k1','k2','k3','k4','k5','k6','k7'};  % do nothing
        case {'c','c1','c2','c3','p','r','l'};  % do nothing
        case 'c4'; assert( isequal( opt.solver, 'sedumi' ), 'polytopenorm:sedumi_c4', 'Case ''c4'' is only possible with Sedumi solver.' );
        case 'c5'; assert( isequal( opt.solver, 'gurobi' ), 'polytopenorm:gurobi_c5', 'Case ''c5'' is only possible with Gurobi solver.' );
        otherwise; fatal_error; end;

    % pre-processings for different, otherwise unrelated algorithms
    if( isanyof(opt.algorithm, {'k0','k*1','k1','k2','k3','k4','k5','k6','k7'}) );
        [pts, VV] = normalize_for_kone( pts, VV );
        tolerance = 1e3*max( size(VV) ) * eps( norm([VV pts]) );
        rank_VV = rank( VV, tolerance );
        if( rank_VV < size( VV, 1 ) );
            normval = inf( 3, N );
            return; end;
        if( ~iskone(VV) );
            tolerance = 1e3*max( size(VV) ) * eps( norm([VV pts]) );
            rank_VV_pts = rank( [VV pts], tolerance );
            if( rank_VV == size( VV, 1 ) );
                normval = repmat( [0;5e-9;0], [1 N] );
            elseif( rank_VV_pts == rank_VV );
                normval = repmat( [0;1+5e-9;0], [1 N] );
            else;
                normval = inf( 3, N ); end;
                return; end; end;

    if( isanyof(opt.algorithm, {'k2','k3','k4','k6','k7'}) );
        center_provided = ~isempty( aux );
        if( center_provided );
            center = aux/norm( aux );
        else;
            center = center_kone( VV, 'deterministic' );
            end;
        if( any( sum(center .* VV, 1) < 0 ) || ...  % this tests whether it is on the correct side. 
            ~in_kone( center, VV ) ||  ...          % this tests whether the center is inside of the kone
            rank( VV ) ~= rank( [VV center] ) ...   % this is a safety test, not clear whether it is needed actually or not - the kone case is not optimized for speed yet
          );
            if( ~center_provided );
                warning( 'polytopenorm:kone:center', '\nCould not compute a valid center.' );
                return;
            else;
                center_test = center_kone( VV, 'deterministic', 'verbose',opt.verbose - 1 );  % try a new center,  if given center is bad
                if( any( sum(center_test .* VV, 1) < 0 ) || ...
                    ~in_kone( center_test, VV ) ||  ...
                    rank( VV ) ~= rank( [VV center_test] ) ...
                  );
                    warning( 'polytopenorm:kone:center', '\nCould not compute a valid center.' );
                    return;
                else;
                    warning( 'polytopenorm:kone:center', '\nProvided center vector is not valid. I choose another one.' );
                    center = center_test; end; end; end;
                
        center = center/norm( center );
        nullspace = nullspace_kone( center );
        aux = center;
        end;

    if( isanyof(opt.algorithm, {'k7'}) );
        L = [center nullspace];
        pts = L' * pts;
        end;

    switch opt.algorithm;
        case {'k0','k*1','k1','k3'};
            [normval, iv, iter, time] = polytopenorm_lpcp_preworker( pts, VV, aux, opt, varargin{:} );

        case {'k2'};
            normval = repmat( [1;inf;0], [1 N] );
            %[center, nullspace, iter] = center_kone( VV );

            % identify points on the other side of the halfplane
            nrm_otherside = sum( center.*pts );
            idx_otherside = nrm_otherside <= 0;
            normval(:,idx_otherside) = repmat( 2 + abs( nrm_otherside(idx_otherside) ), [3 1] );

            % identify zero points
            idx_pts0 = ~any( pts, 1 );
            normval(:,idx_pts0) = 0;
            
            % project onto affine plane
            nrm_pts = sum( pts.*center, 1 );
            idx_orth_pts = ~any( nrm_pts, 1 );
            normval(:,idx_orth_pts) = 2;
            pts = pts./nrm_pts;

            nrm_VV = sum( VV.*center, 1 );
            idx_orth_VV = ~any( nrm_VV, 1 );
            VV = VV./nrm_VV;
            VV(:,idx_orth_VV) = [];
            
            % project onto subspace
            VV = nullspace\VV;
            pts = nullspace\pts;
            
            % compute remaining norms
            idx = ~idx_otherside & ~idx_orth_pts & ~idx_pts0;
            if( any(idx) );
                [normval(:,idx), iv(idx), iter2, time2] = polytopenorm_lpcp_preworker( pts(:,idx), VV, aux, opt, varargin{:} );
                iter = iter + iter2;
                time = time + time2; end;

        case {'c'};
            opt_first = opt;
            opt_first.algorithm = 'c2';
            opt_second = opt;
            if( strcmp(opt.solver,'gurobi') ); 
                opt_second.algorithm = 'c5';
            elseif( strcmp(opt.solver,'sedumi') || strcmp(opt.solver,'matlab') );
                opt_second.algorithm = 'c4';
            else;
                error( 'polytopenorm:fatal', 'programming error' ); end;
            
            if( isempty(opt.bound) );
                [normval, iv, iter, time] = polytopenorm_lpcp_preworker( pts, VV, aux, opt_second, varargin{:} );
            else
                [normval, iv, iter, time] = polytopenorm_lpcp_preworker( pts, VV, aux, opt_first, varargin{:} );
                idx_throwaway = isAlways( normval(2,:) < opt.bound(1) | ...  % for sure below
                                          normval(3,:) > opt.bound(2) | ...  % for sure above
                                          normval(3,:) >= opt.bound(1) & normval(2,:) <= opt.bound(2) | ...  % for sure inside
                                          normval(3,:)*sqrt(2) > opt.bound(2) | ...  % very likely to be above
                                          normval(2,:)/opt.bound(2) < 1+4e-4, ...  % very likely that exact computation yields the same result
                                          'Unknown','false' ...
                                        );
                idx = ~idx_throwaway;
                %idx = normval(2,:) > opt.bound(2)+2e-4 & normval(3,:) < opt.bound(1)-2e-4 & normval(2,:)/sqrt(2) < opt.bound(2) & normval(3,:)*sqrt(2) > opt.bound(1);
                if( nnz(idx) );
                    pts_second = pts(:,idx);
                    if( opt.verbose >= 1 );
                        fprintf( '(' ); end;
                    [normval_second, ~, iter_second, time_second] = polytopenorm_lpcp_preworker( pts_second, VV, aux, opt_second, varargin{:} );
                    normval(:,idx) = normval_second;
                    normval(1:2,idx) = min( normval(1:2,idx), normval_second(1:2,:) );
                    normval(3,idx) = max( normval(3,idx), normval_second(3,:) );
                    time(idx) = time(idx) + time_second;
                    iter = iter + iter_second;
                    if( opt.verbose >= 1 );
                        fprintf( ')|' ); end;
                    %iv(:,idx) = iv_second;  % currently COMPLEXFUNCT_D/E cannot report iv values
                    end; end;
                
        otherwise;
            [normval, iv, iter, time] = polytopenorm_lpcp_preworker( pts, VV, aux, opt, varargin{:} ); end;
   
end

function [ ret, iv, iter, time ] = polytopenorm_lpcp_preworker( pts, VV, aux, opt, varargin );

        if( issym(pts) );
            pts = double( pts ); end;
        if( issym(VV) );
            VV = double( VV ); end;


        switch opt.solver;
            case {'gurobi','g'};
                [ret, iv, iter, time] = polytopenorm_gurobi( pts, VV, aux, opt, varargin{:} );
            case {'matlab','m','sedumi','s'};
                [ret, iv, iter, time] = polytopenorm_matlab( pts, VV, aux, opt, varargin{:} );
            otherwise;
                fatal_error; end;

end

function [ normval, iv, iter, time ] = polytopenorm_matlab( pts, VV, aux, opt, varargin );
%test all new points
% pts                   points to test
% VV                    polytope for the norm
% algorithm             type of algorithm
% epsilon               epsilon used in the algorithm
% verbose               verbose level
% norm_lvl              if computed norm is larger than norm_lvl, then tries to compute it exact. 
% removevertexflag      only possible if pts == V. Then, the point pts(:,i) is removed from the polytope prior to computation

    N = size( VV, 2 );  % number of vertices of polytpe
    dim = size( VV, 1 );  % dimension

    num_pts = size( pts, 2 );
    normval = inf*ones( 3, num_pts );  % infinity means outside
    iter = zeros( 1, num_pts );
    iv = cell( 1, num_pts );
    time = zeros( 1, num_pts );
        
    if( isempty(VV) );
        normval = inf( 3, num_pts );
        iter = 0; 
        time = [];
        iv = [];
        return; end;
    
    % prepare LP / CP
    %%%%%%%%%%%%%%%%%%%%%
    switch opt.algorithm;
        case {'l'};
            error( 'polytopenorm:matlab', 'Case (L) is not implemented for the matlab linprog solver. Use Gurobi instead.' );

        case {'p'};
            model.f = [zeros( 1, N ) -1];
            model.A = spalloc( dim+1+N, N+1, dim*N+N+N+dim );
            model.A(:,1:N) = [-VV; sparse( ones(1,N) ); -speye( N )];
            model.b = [sparse( dim, 1 ); 1; sparse( N, 1 )];
            sze = size( model.A );
            model.Aeq = sparse( 1, sze(2) );
            model.beq = 0;
            
        case {'r'};
            % format of variables (with notation from "Thomas Mejstrik, Thesis, University of Vienna, 2019": 
            % x = [t q t_0].'
            % A = [V 0 -p]
            % t0 = norm{p}^-1
            
            model.f = [zeros( 1, 2*N ) -1];
            model.Aeq = [VV zeros( dim, N ) zeros( dim, 1 )];  % last column is -p
            model.beq = zeros( dim, 1 );
            model.A = vertcat( [sparse(1,N) sparse(ones(1, N)) 0], ...
                         [-speye(N) -speye(N) sparse(N, 1)], ...
                         [speye(N) -speye(N) sparse(N, 1)], ...
                         [sparse(N,N) -speye(N) sparse(N, 1)] );
            model.b = vertcat( 1, sparse(3*N, 1) );            
            
        case {'c1'};  % version from Guglielmi, Protasov 2013
            % format of variables (with notation from "Thomas Mejstrik. Thesis, University of Vienna, v2.0, 2020"
            % x = [t0 a_1 r_1 i_1 ... a_N r_N i_N ].';
            %   where r_v = Re(t_v), i_v = Im(t_v);
            % A = [-p 0 v_1 -w_1 ... 0 v_N -w_N 
            %      -q 0 w_1  v_1 ... 0 w_N  v_N
            %       0 1  0    0  ... 1  0    0];
            %   where v_v = Re(v_v), w_v = Im(v_v), p = Re(p), q = Im(p)
            %   and a1 + ... + a_N = 1
            %   and an <= norm([vn;wn]).
            % Thus, 
            %   b = [0 ... 0 1].'
            %   c = [-1 0 ... 0];
            %   t0 = norm{p}^-1 >= 0
            %
            % Solving
            %   min c'x, where Ax = b
            
            model.c = [-1; zeros( 3*N, 1 )];
            idx = reshape( [ones(1, N); (2:2+N-1); (2+N:2+N+N-1)], 1, [] );
            A1 = [zeros( dim, 1 ) real( VV ) -imag( VV )];
            A2 = [zeros( dim, 1 ) imag( VV ) real( VV )];
            model.A = [zeros( dim, 1 ) A1(:,idx); zeros( dim, 1 ) A2(:,idx); 0 repmat( [1 0 0], 1, N )]; %first column is real(-p);imag(-p);0
            model.b = [zeros( 2*dim, 1 ); 1];
            model.K.f = 0;
            model.K.l = 1;
            model.K.q = repmat( 3, 1, N );
            
        case {'c2'};  % version from Protasov, barab7.pdf
            % format of variables (with notation from barab7.pdf)
            % rest is similar to COMPLEXFUNCT_A
            % x = [t0 s_1 t_1 u_1 ... s_2l t_2l u_2l ].';
            model.c = [-1; zeros( 2*3*N, 1 )];
            val1 = 2:N+1;
            val2 = 2+N:2+N+N-1;
            idx1 = reshape( [ones(1, N); val1; val2+N; ones(1, N); val1;   val2],   1, [] );
            idx2 = reshape( [ones(1, N); val2; val1;   ones(1, N); val2+N; val1], 1, [] );
            model.A = [ zeros( dim, 1 ) real( VV ) imag( VV ) -imag( VV )];
            model.A = [ zeros( dim, 1 ) model.A(:,idx1); zeros( dim, 1 ) model.A(:,idx2); 0 repmat( [1 0 0], 1, 2*N )];  % first column is real(-p);imag(-p);0
            model.b = [zeros( 2*dim, 1 ); 1];
            model.K.f = 0;
            model.K.l = 1;
            model.K.q = repmat( 3, 1, 2*N );   
            
        case {'c3'};
            % we solve the problem:
            % norm{p}_{co* VV} = max_j of
            %    max (pj,e), e = [e1,...,eD] \in \RR^D
            %    s.t. sn^2 >= sna^2 + snb^2  for all n
            %         sn = 1
            %         san = (an,e) (i.e. san = an1*e1 + ... + anD*eD )
            %         sbn = (bn,e) (i.e. sbn = bn1*e1 + ... + bnD*eD )
            %         an = real( VVn )
            %         bn = imag( VVn )
            %
            % x = [e1 ... eD s1 sa1 sb1 ... sN saN sbN]
            % 
            % sbn = bn1*e1 + ... + bnD*eD
            if( ~isempty(aux) );
                model.J = aux;
            else;
                model.J = C3_AUX(); end;
            A1 = reshape( permute( cat( dim+1, real(VV)', imag(VV)' ), [dim+1 1:dim] ), [2*N dim] );
            A2 = repmat( {[0 -1 0;0 0 -1]}, [1 N] );
            A2 = blkdiag( A2{:} );
            A3 = repmat( {[1 0 0]}, [1 N] );
            A3 = blkdiag( A3{:} );
            model.A = [A1 A2;zeros(N,dim ) A3];
            model.b = [ zeros( 2*N, 1 );  ones( N, 1 ) ];
            model.c = [ zeros( 1, dim ) zeros( 1, 3*N ) ]; %first entries are -p
            model.K.f = dim;
            model.K.l = 0;
            model.K.q = 3*ones( 1, N );
            
        case {'c4','c'};
            % parse additional stuff
            [model.relaccuracy, varargin] = parsem( {'relativeaccuracy','relaccuracy','rel'}, varargin, .99995, 'expecte', @isscalar );
            [model.absaccuracy, varargin] = parsem( {'absativeaccuracy','absaccuracy','abs'}, varargin, .00005, 'expecte', @isscalar );
            [model.maxvertex, varargin] = parsem( {'maxvertex'}, varargin, 50, 'expect', @(x) 2<x && x<inf );
            model.bound = opt.bound;
            %we solve the problem: 
            % norm{p}_{co* VV} = max_j of
            %    max (pj,e), e = [e1,...,eD] \in \RR^D
            %    s.t. sn^2 >= sna^2 + snb^2  for all n
            %         sn = 1
            %         san = (an,e) (i.e. san = an1*e1 + ... + anD*eD )
            %         sbn = (bn,e) (i.e. sbn = bn1*e1 + ... + bnD*eD )
            %         an = real( VVn )
            %         bn = imag( VVn )
            %
            %
            % x = [e1 ... eD s1 sa1 sb1 ... sN saN sbN]
            % 
            % sbn = bn1*e1 + ... + bnD*eD
            A1 = reshape( permute( cat( dim+1, real(VV)', imag(VV)' ), [dim+1 1:dim] ), [2*N dim] );
            A2 = repmat( {[0 -1 0;0 0 -1]}, [1 N] );
            A2 = blkdiag( A2{:} );
            A3 = repmat( {[1 0 0]}, [1 N] );
            A3 = blkdiag( A3{:} );
            model.A = [A1 A2;zeros(N,dim ) A3];
            model.b = [ zeros( 2*N, 1 );  ones( N, 1 ) ];
            model.c = [ zeros( 1, dim ) zeros( 1, 3*N ) ]; %first entries are -p
            model.K.f = dim;
            model.K.l = 0;
            model.K.q = 3*ones( 1, N );    
            
        case {'c5'};
            error( 'polytopenorm:unimplemented', 'Only implemented for Gurobi solver yet.' );
            
        otherwise;
            error( 'polytopenorm:fatal', 'Unkown value for ''algorithm''.'); end;    
        
    parsem( varargin, 'test' );
    
    if( opt.verbose >= 1 ); 
        fprintf( ' ' ); end; %is removed later on
    if( opt.num_core >= 1 && ~opt.debug );
        epsilon_display = opt.epsilon_display;
        verbose = opt.verbose;
        algorithm = opt.algorithm;
        solver = opt.solver;
        parfor( i_pts = 1:num_pts, opt.num_core );
             [val, iv{i_pts}, iter(i_pts), time(i_pts), epsilonout] = polytopenorm_matlab_worker( pts(:,i_pts), model, epsilon_display, verbose, algorithm, solver );
             normval(:,i_pts) = val;
             if( verbose >= 1 );
                 polytopenorm_matlab_print( val, epsilonout ); end; end;
    else;
        for i_pts = 1:num_pts;
             [val, iv{i_pts}, iter(i_pts), time(i_pts), epsilonout] = polytopenorm_matlab_worker( pts(:,i_pts), model, opt.epsilon_display, opt.verbose, opt.algorithm, opt.solver );
             normval(:,i_pts) = val;
            if( opt.verbose >= 1 );
                 polytopenorm_matlab_print( val, epsilonout ); end; end; end;
        
    
    iter = sum( iter );
    
    if( opt.verbose >= 1 ); 
        fprintf( '\b' ); end;  % removes last line break
    
end

function polytopenorm_matlab_print( normval, epsilon_display );
    % prints '-'/number, whether vertex is inside or outside of the polytope
    if( any(isnan(normval)) );              
        fprintf( '\bE\n' );  % normval(i,2) = inf;
    elseif( any(~isfinite(normval)) );      
        fprintf( '\b8\n' );  % normval(i,2) = inf; 
    elseif( max(normval) < 0 );                         fprintf( '\bm\n' );  % normval(i,2) = inf; 
    elseif( max(normval) <= 1 );                        fprintf( '\b_\n' ); 
    elseif( max(normval) <= 1+4*epsilon_display);       fprintf( '\b.\n' ); 
    elseif( max(normval) <= 1+1000*epsilon_display );   fprintf( '\b,\n' ); 
    elseif( max(normval) <= 2 );                        fprintf( '\bo\n' ); 
    else;                                               fprintf( '\bO\n' ); end;

end

function [ x, iv, iter, time, epsilon_out ] = polytopenorm_matlab_worker( p, model, ~, verbose, algorithm, solver );
% this function must return a non-negative value, +inf is allowed
% Output:
%   x               norm of point in order: [estimate; upper-bound; lower-bound]
%   iv              candidates of vertices of polytope which generate face correspdoning to point
%   iter            number of iterations in LP/CP
%   epsilon_out     accuracy of LP/CP solver
    
    switch solver;
        case 'matlab';
            accuracy = 1;  % factor for accuracy relative to Matlabs linprog
            h_linprog = @linprog;
        case 'sedumi';
            accuracy = 1000;  % higher values mean less accurate
            h_linprog = @sedumi_linprog;
        otherwise;
            fatal_error; end;
        

    dim = size( p, 1 );

    if( isequal(p, zeros(dim, 1)) );
        x = [0;0;0];
        iter = 0; 
        iv = [];
        epsilon_out = 0;
        time = 0;
        return; end;
    
    switch algorithm;
        case {'p','r','c1','c2','c3'};
            scale = norm( p, 1 );
        case {'c','c4'};
            scale = 1; 
        otherwise;
            error( 'polytopenorm:fatal', 'programming error' ); end;
    %scale = 1;
    p = p/scale;  % scale the problem
    
    starttime = tic;
    
    % run solver
    %%%%%%%%%%%%%%%%%%
    switch algorithm;
        case {'p'};
            model.A(1:dim,end) = p(:);
            if( isequal(solver,'matlab') );
                opts = optimoptions( @linprog, 'Display','off', 'Algorithm','dual-simplex' ); 
            else;
                opts.Display = 'off'; end;  % choose algorithm, interior point algorithm does not work well
            %opts = [];  % for debugging if necessary
            [x, ~, exitflag, info] = h_linprog( model.f, model.A, model.b, model.Aeq, model.beq, [], [], opts );  % Solve the problem
            iter = info.iterations;
            iv = any( abs(x(1:end-1)) > 1e-9, 2 ).';
            if( ~isempty(x) );
                x = scale/x(end);
                x = [x; x + accuracy*5e-9; max(0, x - accuracy*5e-9)]; end;
            errcode = exitflag;
            epsilon_out = 5e-9;
            
        case {'r'};
            model.Aeq(:,end) = -p;
            if( isequal(solver,'matlab') );
                opts = optimoptions( @linprog, 'Display','off', 'Algorithm','dual-simplex' ); 
            else;
                opts.Display = 'off'; end;  % choose algorithm, interior point algorithm does not work well
            %opts = [];  % for debugging if necessary
            [x, ~, exitflag, info] = h_linprog( model.f, model.A, model.b, model.Aeq, model.beq, [], [],  opts );  % Solve the problem
            iter = info.iterations;
            iv = any( abs(reshape(x(1:end-1), [], 2)) > 1e-9, 2 ).';
            if( ~isempty(x) );
                x = scale/x(end); 
                x = [x; x + accuracy*5e-9; max(0, x - accuracy*5e-9)]; end;
            errcode = exitflag;    
            epsilon_out = 5e-9;
            
        case {'c1'};
            pars.eps = 1e-9;
            pars.fid = 0;
            %pars.vplot = 1;  % for Debugging
            %pars.errors = 1;  % for Debugging
            model.A(:,1) = -[real( p ); imag( p ); 0];
            [x, ~, info] = sedumi( model.A, model.b, model.c, model.K, pars );
            iter = info.iter;
            iv = any( abs(reshape(x(2:end), 3, [])) > 5e-5, 1 );
            if( ~isempty(x) );
                x = scale/x(1);
                x = [x; x + 5e-5; 0]; end;
            errcode = ~(info.pinf | info.dinf | info.numerr > 1 );
            epsilon_out = 5e-9;
            
        case {'c2'};
            pars.eps = 1e-9;
            pars.fid = 0;
            %pars.vplot = 1;  % for Debugging
            %pars.errors = 1;  % for Debugging
            model.A(:,1) = -[real( p ); imag( p ); 0];
            [x, ~, info] = sedumi( model.A, model.b, model.c, model.K, pars );
            if( isfield(info, 'iter') ); iter = info.iter; else; iter = 0; end;
            iv = any( abs(reshape(x(2:end), 6, [])) > 5e-5, 1 );
            if( ~isempty(x) );
                x = scale/x(1);
                x = [x; x+5e-5; max(0, (x-5e-5)/2)]; end;  % accuracy seems to be at most 1e-5
            if( isfield(info, 'dinf') ); dinf = info.dinf; else; dinf = 1; end;
            if( isfield(info, 'numerr') ); numerr = info.numerr; else; numerr = 2; end;
            errcode = ~(info.pinf | dinf | numerr > 1 );
            epsilon_out = 5e-5;
            
         case {'c3'};
            beta = 0:pi/model.J:pi-pi/model.J;  % angle-stepwidth of approximated ellipse
            p = real( p )*cos( beta ) + imag( p )*sin( beta );
            expandfactor_C = 1/(cos(pi/(2*model.J)));  % expand factor to get outer polytope
            pars.eps = 1e-9;
            pars.fid = 0;
            x = zeros( size(model.A, 2), model.J );
            t = zeros( 1, model.J );
            [pinf,dinf,numerr,iter] = deal( 0 );
            feasratio = 1;
            for j = 1:model.J
                pj = p(:,j);
                model.c(1:dim) = -pj';
                [x(:,j), ~, info] = sedumi( model.A, model.b, model.c, model.K, pars ); 
                iter = iter + info.iter;
                pinf = max( pinf, info.pinf );
                dinf = max( dinf, info.dinf );
                numerr = max( numerr,info.numerr );
                feasratio = min( feasratio, info.feasratio );
                t(j) = abs( x(1:dim,j).'*pj ); end;
            [t, idx] = max( t );  %#ok<ASGLU>
            %x = x(:,idx);
            errcode = ~(pinf | dinf | numerr > 1 | feasratio < 0.8 );
            iv = [];
            if( errcode );
                x = t * scale; 
            else;
                x = nan; end;
            x = [(expandfactor_C+1)/2*x; expandfactor_C*(x+5e-5); max(0,x-5e-5) ];
            epsilon_out = 5e-5;
            
            x(end,:) = max( x(end,:), 0 );
            
        case {'c4','c'};
            errcode = 0;  % error handling is done internally
            iter = 0;
            pars.eps = 1e-9;
            pars.fid = 0;
            epsilon_out = 5e-5;
            [pg, pg1, pgsj, nrm_pg] = deal( cast([], class(p)) );  %#ok<ASGLU> 
            while( true );
                [pg, pgsj, nrm_pg, oldpg, oldnrm] = complexfunct_d_splitvertex( p, pg, pgsj, nrm_pg );
                idx = any( isnan(nrm_pg), 1 );
                for i = find( idx );
                    model.c(1:dim) = -pg(:,i).';
                    [x, ~, info] = sedumi( model.A, model.b, model.c, model.K, pars ); 
                    if( isfield(info, 'iter') ); iter = iter + info.iter; end;
                    if( isfield(info, 'pinf') ); pinf = info.pinf; else; pinf = 1; end;
                    if( isfield(info, 'dinf') ); dinf = info.dinf; else; dinf = 1; end;
                    if( isfield(info, 'numerr') ); numerr = info.numerr; else; numerr = 2; end;
                    if( isfield(info, 'feasratio') ); feasratio = info.feasratio; else; feasratio = -1; end;
                    badcomputation = pinf | dinf | numerr > 1 | feasratio < 0.8;
                    if( ~badcomputation );
                        factor = 1/cos( pi/(2*2^floor(log2(pgsj(i)))) );
                        val = abs( x(1:dim).'*pg(:,i) );
                        nrm_pg(:,i) = [val;val/factor]; 
                        errcode = 1;
                    else;
                        idx = any( isnan(nrm_pg), 1 );
                        pg(:,idx) = repmat( oldpg, [1 nnz(idx)] );
                        nrm_pg(:,idx) = repmat( oldnrm, [1 nnz(idx)] ); 
                        break; end; end;

                mx = max( nrm_pg(1,:) );
                mn = max( nrm_pg(2,:) );
                if( badcomputation );
                    break; end;                
                if( ~isempty(model.bound) && (mn >= model.bound(2) || ...
                                              mx <= model.bound(1) || ...
                                              mn >= model.bound(1) && mx <= model.bound(2) || ...
                                              mx - mn < epsilon_out) ...
                  );
                    break; end;
                if( mn/mx > model.relaccuracy );
                    break; end;
                if( mx-mn < model.absaccuracy );
                    break; end;
                if( size(pg, 2) >= model.maxvertex );
                    break; end; end;
            if( verbose >= 3 );
                plotm( pg/mx, 'g.-', 'funct','r', 'hold','on' ); end; 
            x = [mx; mx + 5e-5; max(0, mn - 5e-5)];
            iv = [];
        otherwise;
            fatal_error; end;

    time = toc( starttime );

    % interpret error codes and return value. Output Debug Information
    if( errcode <= 0 || isnan(x(1)) || isempty(x) );
        if( verbose >= 1 ); fprintf( '\bE\n' ); end;
        if( verbose >= 2 ); fprintf( '\brror (polytopenorm_matlab_worker)\n' ); end;
        if( verbose >= 3 ); display(errcode); fprintf('\n'); end;  %#ok<DISPLAYPROG>
        x = [1;inf;0];
    elseif( x(1) < -5e-9 );
        if( verbose >= 1 ); fprintf( '\bL\n' ); end;
        if( verbose >= 2 ); fprintf( '\bess than zero (polytopenorm_matlab_worker).\n' ); end; 
        x(:) = [1;inf;0]; end;
    
end

function [ ret, iv, iter, time ] = polytopenorm_gurobi( pts, VV, aux, opt, varargin );

    
    N = size( VV, 2 );  % number of vertices of polytpe
    dim = size( VV, 1 );  % dimension
    ret = inf( 3, size(pts, 2) );  %#ok<NASGU>
    iv = cell( 1, size(pts, 2) );  %#ok<PREALL>
    iter = 0;                      %#ok<NASGU>
    time = 0;                      %#ok<NASGU>

    if( opt.num_core >= 1 && ~opt.debug );
        try;
            err = lasterror();  %#ok<LERR>
            p = gcp();
            num_worker = p.NumWorkers; 
        catch me;  %#ok<NASGU>
            lasterror( err );  %#ok<LERR>
            opt.num_core = 0;
            num_worker = 0; end;
    else;
        num_worker = 0; end;

    switch opt.algorithm;  % stuff which needs to be done to the coordinates of the input points
        case {'k4'};
            center = aux;
        case {'k3','k7'};
            center = aux;
            nullspace = nullspace_kone( center );
            L = [center nullspace];
            
        otherwise;
            end; % do nothing
    
    % search for minimal spanning tree
    [chain, MST] = partitionatepolytope( pts, ceil(log2(num_worker) + 1), opt.verbose - 1 );
    num_chains = size( chain, 2 );
    
    % Initialize variables and construct model/params
    num = cellfun( @(x) x.numnodes, MST );
    nearpointidx = cell( 1, num_chains );
    if( isreal(VV) && isreal(pts) );
        for w = 1:num_chains; 
            try;
                val_d = pdist2( MST{w}.Nodes.co, VV.' ).';
            catch me;  %#ok<NASGU>
                % fallback if pdist2 is not available
                val_d = sqrt( sqdist(  MST{w}.Nodes.co.', VV ) ).'; end;
            [~, nearpointidx{w}] = min( val_d, [], 1 ); end;
    else;
        VVval = [real(VV); imag(VV)].';
        for w = 1:num_chains; 
            try;
                val = MST{w}.Nodes.co;
                val = pdist2( [real(val) imag(val)], VVval ).';
%                 val = MST{w}.Nodes.co.';
%                 val = pdist2( [real(val).' imag(val).'], VVval ).';                
            catch me;  %#ok<NASGU>
                val = MST{w}.Nodes.co;
                val = sqrt( sqdist( [real(val) imag(val)].', VVval.' ).' ); end;
            [~, nearpointidx{w}] = min( val, [], 1 ); end; end;
        
    
    switch opt.algorithm;
        
        case 'l';
            model.A = [spzeros( dim, 1 ) VV;
                       0                 ones( 1, N )];
            model.rhs = [zeros( dim, 1 ); 1];
            model.sense = [repmat( '<', [dim, 1] ); '>'];
            model.obj = [1; zeros( N, 1)];
            params.Method = 0;

        case {'k0'};
            model.A = sparse( VV );
            model.rhs = zeros( dim, 1 );
            model.sense = repmat( '=', [size(pts, 1) 1] );
            params.Method = 0;
%            model.A = sparse( [VV -VV] );
%            model.sense = repmat( '=', [1 size(pts, 1)] );
%            model.rhs = zeros( size(pts, 1), 1 );
%            model.obj = [zeros( 1, N ) ones( 1, N )];
%            params.FeasibilityTol = ipa_constant.GUROBI_EPSILON;
%            params.OptimalityTol = ipa_constant.GUROBI_EPSILON;
%            params.Method = 0;

        case {'k1'};
            model.A = [zeros( dim, 1 ) sparse( VV );
                       -ones( N, 1 )   eye( N )];
            model.rhs = zeros( dim + N, 1 );
            model.sense = [repmat( '=', [dim 1] ); repmat( '>', [N 1] )];
            model.obj = [1 zeros( 1, N )];
            model.lb = -inf( N + 1, 1 );
            model.ub = inf( N + 1, 1 );
            model.modelsense = 'max';

        case {'k2'};
            model.A = [spzeros( dim, 1 ) VV;
                       0                 sparse( ones(1, N ) )];
            model.rhs = [zeros( dim, 1 ); 1 ];
            model.sense = [repmat( '=', [dim 1] ); '<' ];
            model.ub = [inf; inf( N, 1 )];  % Danger: model is unbounded if result would be zero
            model.modelsense = 'max';
            model.obj = [1; zeros( N, 1 )];
            params.Method = 0;

        case {'k3'};
            I_alpha   = zeros( dim ); I_alpha(1) = 1;
            I_alpha_t = eye( dim );   I_alpha_t(1) = 0;
            alpha_coeff   = L * I_alpha * L' * VV;
            alpha_t_coeff = L * I_alpha_t * L' * VV;
            
            model.obj = zeros( 1, N + 1 );
            model.obj(end) = 1;
            model.A = sparse(zeros(0, N + 1));
            model.rhs = [];
            model.sense = '';
            for s = 1:dim;
                model.quadcon(s).Qc = sparse( [zeros( N ) alpha_t_coeff(s,:)'/2; alpha_t_coeff(s,:)/2 0] );
                model.quadcon(s).q = [alpha_coeff(s,:) 0];
                model.quadcon(s).sense = '='; end;
    
            model.lb = zeros( N + 1, 1 );
            model.ub = inf( N + 1, 1 );
    
            model.modelsense = 'min';
            
            params.Method = 0;
            params.NonConvex = 2;
            params.TimeLimit = 5;

        case {'k4'};
            model.A = spzeros( 0, 1 + N );
            model.obj = [1 zeros( 1, N )];
            model.lb = [-inf zeros( 1, N )];
            model.ub = [1 inf( 1, N )];
            model.modelsense = 'min';
            
            %varname = repcell( 'al', [1 N+1] );
            %varname = genvarname( varname );
            %model.varnames = [{'t', varname{2:end}}];

            for i = 1:dim;
                vec = repmat( center(i)/2, [1 N] );
                model.quadcon(i).Qc = [0     -vec ;
                                       -vec' spzeros( N, N )];
                model.quadcon(i).q = [0 VV(i,:)];
                %model.quadcon(i).rhs = p(i);
                model.quadcon(i).sense = '=';
            end

            params.Method = 0;
            params.NonConvex = 2;

        case {'k5'};
            model.A = spzeros( 0, 1 + N );
            model.obj = [1 zeros( 1, N )];
            model.lb = [-inf zeros( 1, N )];
            model.ub = [1 inf( 1, N )];  % XX upper bound of t: 1 or +inf?
            model.modelsense = 'min';
            
            %varname = repcell( 'al', [1 N+1] );
            %varname = genvarname( varname );
            %model.varnames = {'t', varname{2:end}};
            
            for i = 1:dim;
                vec = repmat( sum(VV(i,:)), [1 N] );
                model.quadcon(i).Qc = [0     -vec ;
                                       -vec' spzeros( N, N )];
                model.quadcon(i).q = [0 N * VV(i,:)];
                %model.quadcon(i).rhs = p(i);
                model.quadcon(i).sense = '='; end;
            
            params.Method = 0;
            params.NonConvex = 2;

        case {'k6'};
            model.A = sparse( [center VV] );
            model.rhs = zeros( dim, 1 );
            model.obj = [1 zeros( 1, N )];
            model.sense = '=';
            model.modelsense = 'max';
            model.lb = [-inf zeros( 1, N )];

            params.Method = 0;

        case {'k7'};
            VVt = L' * VV;  % L' = inv(L)
            model.A = sparse( [ones( dim, 1 ) VVt] );
            model.rhs = zeros( dim, 1 );
            model.obj = [1 zeros( 1, N )];
            model.sense = '=';
            model.modelsense = 'max';
            model.lb = [0 zeros( 1, N )];  % XX -inf for t t?
            
            params.Method = 0;

            
        case {'p'};
            model.obj = [zeros( 1, N ) -1];  % in matlab terminology: f, in gurobi terminolgy: c
            A = spalloc( dim + 1 + N, N + 1, dim*N + N + N + dim );
            A(:,1:N) = [-VV; sparse( ones(1, N) ); -speye( N )];
            %A(1:dim,end) = p(:);  % is done in polytopenorm_gurobi_worker
            sze = size( A );
            Aeq = zeros( 1, sze(2) );
            model.A = [A; Aeq];  % lhs must be sparse
            
            b = [zeros( dim, 1 ); 1; zeros( N, 1 )];
            beq = 0;
            model.rhs = [b; beq];  % in matlab terminology: b
            model.sense = [repmat( '<', size( A, 1 ), 1 );
                           repmat( '=', size( Aeq, 1 ), 1 )];
            params.FeasibilityTol = ipa_constant.GUROBI_EPSILON;  % smaller epsilon is faster??
            params.OptimalityTol = ipa_constant.GUROBI_EPSILON;
            params.Method = 0;  % 0 = primal simplex (fastest?), 1 = dual simplex, 2 = barrier (interior point) (slowest?)
            model.ub = 1000*ones( size(model.A, 2), 1 );  % if point to be computed is very small, this prevents the solution from becoming unbounded
            %params.Presolve = 2; %slower?, useless
            %params.DualReductions = 0; %useless
            
            clear A b beq;        
        
        case {'r'};
            model.obj = zeros( 1, 2*N+1 ); 
            model.obj(end) = 1;  % in matlab terminology: f
            val = vertcat( [sparse( 1,N )   sparse( ones( 1, N ) ) 0], ...
                           [-speye( N )    -speye( N ) sparse( N, 1 )], ...
                           [speye( N )     -speye( N ) sparse( N, 1 )], ...
                           [sparse( N, N ) -speye( N ) sparse( N, 1 )] );
            A_without_p = [VV sparse( dim, N ) zeros( dim, 1 )];  % Aeq = [V sparse(dim,n) -p]; 
            model.A = [val; A_without_p];  % lhs must be sparse
            b = vertcat( 1, zeros( 3*N, 1 ) );  % rhs must be dense
            beq = zeros( dim, 1 );
            model.rhs = [b; beq];  % in matlab terminology: b
            model.sense = [repmat( '<', size(val, 1), 1 ); 
                           repmat( '=', size(A_without_p, 1), 1 )];

            model.lb = -inf*ones( size(model.A, 2), 1 );
            %model.ub =  IPA_CUTOFF*ones( size(model.A, 2), 1 ); 
            model.ub = 1000*ones( size(model.A, 2), 1 );  % if point to be computed is very small, this prevents the solution from becoming unbounded
            %model.ub(end) = 2;  % this does not help  % we are only interested for norm-values of points near to the boundary 
            model.modelsense = 'max';
            
            params.FeasibilityTol = ipa_constant.GUROBI_EPSILON;  % smaller epsilon is faster??
            params.OptimalityTol = ipa_constant.GUROBI_EPSILON;
            params.Method = 0;  % 0 = primal simplex (fastest?), 1 = dual simplex, 2 = barrier (interior point) (slowest?)
            %params.Presolve = 2;  % slower?, useless
            %params.DualReductions = 0;  % useless            
            
            clear val A_without_p b beq;
            
        case {'c1'};
            model.obj = [-1; zeros( 3*N, 1 )];  % in matlab terminology: f, in gurobi terminolgy: c
            model.rhs = [zeros( 2*dim, 1 ); 1];  % in matlab/gurobi terminology: b
            model.A = [zeros(dim,1)  real(VV)   -imag(VV)     zeros(dim,N);  % A without p, but with column of p set to zero
                       zeros(dim,1)  imag(VV)    real(VV)     zeros(dim,N);
                       0             zeros(1,N)  zeros(1,N)   ones(1,N)   ];
            model.A = sparse( model.A );
            model.sense = '=';
            model.lb = [-inf -inf( 1, 2*N ) zeros( 1, N ) ];
            model.ub = 1000*ones( 3*N+1, 1 );  % prevents the solution to become unbounded
            %model.lb(1) = .5;
            val = zeros( 1, 1+3*N );
            for n = 1:N;
                model.quadcon(n).sense = '<';
                model.quadcon(n).q = val;
                model.quadcon(n).rhs = 0;
                model.quadcon(n).Qc = sparse( [1+n 1+n+N 1+n+2*N], [1+n 1+n+N 1+n+2*N], [1 1 -1], 1+3*N, 1+3*N ); end;
            
            params.Method = 2;
            params.FeasibilityTol = ipa_constant.GUROBI_EPSILON;
            params.OptimalityTol = ipa_constant.GUROBI_EPSILON;
            params.BarConvTol = ipa_constant.GUROBI_EPSILON;
            
        case {'c2'};
            model.obj = [-1; zeros( 6*N, 1)];
            model.rhs = [zeros( 2*dim, 1 ); 1];
            model.A = [zeros( dim, 1 ) real( VV )     -imag( VV )  real( VV ) imag( VV ) zeros( dim, 2*N );
                       zeros( dim, 1 ) imag( VV )      real( VV ) -imag( VV ) real( VV ) zeros( dim, 2*N );
                       0               zeros( 1, 4*N )                                   ones( 1, 2*N )   ];
            model.A = sparse( model.A );
            model.sense = '=';
            model.lb = [-inf -inf( 1, 4*N ) zeros( 1, 2*N) ];
            model.ub = 1000*ones( 1+6*N, 1 );  % prevents the solution to become unbounded
            %model.ub(1) = 2;
            %model.lb(1) = .5;
            val = zeros( 1, 1+6*N );
            for n = 1:N;
                model.quadcon(2*n-1).sense = '<';
                model.quadcon(2*n-1).q = val;
                model.quadcon(2*n-1).rhs = 0;
                model.quadcon(2*n-1).Qc = sparse( [n+1 n+N+1 4*N+2*n], [n+1 n+N+1 4*N+2*n], [1 1 -1], 1+6*N, 1+6*N );
                model.quadcon(2*n).sense = '<';
                model.quadcon(2*n).q = val;
                model.quadcon(2*n).rhs = 0;
                model.quadcon(2*n).Qc = sparse( [n+2*N+1 n+3*N+1 4*N+2*n+1], [n+2*N+1 n+3*N+1 4*N+2*n+1], [1 1 -1], 1+6*N, 1+6*N ); end;
            params.Method = 2;
            params.FeasibilityTol = ipa_constant.GUROBI_EPSILON;
            params.OptimalityTol = ipa_constant.GUROBI_EPSILON;
            params.BarConvTol = ipa_constant.GUROBI_EPSILON;
            
        case {'c3'};
            % we solve the problem:
            % norm{p}_{co* VV} = t of
            %   max i1.t1 + ... + iJ.tJ
            %   s.t. tj = (pj,e), e = [e1,...,eD]
            %        1 >= sna^2 + snb^2  for all n
            %        san = (an,e) (i.e. san = an1*e1 + ... + anD*eD )
            %        sbn = (bn,e) (i.e. sbn = bn1*e1 + ... + bnD*eD )
            %        ij \in \set{0,1}
            %        \sum ij = 1
            %        an = real( VVn )
            %        bn = imag( VVn )
            %
            % 
            % x = [e1 ... eD  i1 ... iJ t1 ... tJ   sa1 sb1 ... saN sbN]            
            if( ~isempty(aux) );
                model.J = aux;
            else;
                model.J = C3_AUX(); end;
            J = model.J;
            D = dim;
            a = real( VV );
            b = imag( VV );
            model.expandfactor = 1/(cos(pi/(2*J)));  % expand factor to get outer polytope
            
            num_x = D + 2*J + 2*N;
            model.obj = zeros( 1, num_x );  % c
            model.Q = sparse( [D+J+1:D+J+J D+1:D+J], [D+1:D+J D+J+1:D+J+J], ones(1, 2*J), num_x, num_x );
            Ap = nan( J, D );  % Ap has to be overwritten by the worker
            A1a = reshape( permute( cat( D+1, a', b' ), [D+1 1:D] ), [2*N D] );
            A = [ [Ap;A1a] zeros( J+2*N, J ) -eye( J+2*N ) ];
            model.A = sparse( A );
            model.sense = '=';
            model.rhs = zeros( J + 2*N, 1 );
            model.modelsense = 'max';
            model.vtype = [repmat( 'C', [D 1] ); repmat( 'B', [J 1] ); repmat( 'C', [J+2*N 1] ) ];
            model.lb = [repmat( -inf, [D 1] ); repmat( 0, [J 1] ); repmat( -inf, [J+2*N 1] )];  %#ok<REPMAT>
            for n = 1:N;
                %model.quadcon(n).sense = '<'; %default
                model.quadcon(n).q = zeros( num_x, 1 );  % is needed although documentation says its optional
                model.quadcon(n).rhs = 1;
                val = D + J + J + [2*n-1 2*n];
                model.quadcon(n).Qc = sparse( val, val, [1 1], num_x, num_x ); end;
            model.sos(1).type = 1;
            model.sos(1).index = [D+1:D+J];  %#ok<NBRAK>
            
            params.Method = 2;
            params.FeasibilityTol = ipa_constant.GUROBI_EPSILON;
            params.OptimalityTol = ipa_constant.GUROBI_EPSILON;
            params.BarConvTol = ipa_constant.GUROBI_EPSILON;
            
        case {'c4'};
            error( 'polytopenorm:unimplemented', 'Only implemented for Matlab/SeDuMi solver yet.' );
            
        case {'c5','c'};
            % S : determines approximation factor, integer larger than 1
            % J : number of ellipse-vertices Ej = E(aj,bj)
            % w : ellipse-point
            %
            % x : r, r^1, ..., r^J, x_1^1, ..., x_{2S+2}^1, ..., x_1^J, ..., x_{2S+2}^J
            %
            % r = r^1 + ... + r^J
            
            
            model.bound = opt.bound;
            [model.relaccuracy, varargin] = parsem( {'relativeaccuracy','relaccuracy','rel'}, varargin, 1-5e-9, 'expecte',@(x) x > 0.7 && x < 1 );
            [model.absaccuracy, varargin] = parsem( {'absoluteaccuracy','absaccuracy','abs'}, varargin, 5e-9, 'expecte',@(x) x > 0 );
            [model.maxvertex, varargin] = parsem( {'maxvertex'}, varargin, 50, 'expect', @(x) 2 < x && x < inf );
            default_S = max( 3, ceil( -log2( acos(sqrt(model.relaccuracy))/pi ) ) );
            [model.S, varargin] = parsem( {'S','aux'}, varargin, default_S, 'expect',{'clop',[3 inf]} );
            S = model.S;
            model.expandfactor = cos( 2^(-S)*pi )^(-1);  % XX constant by tommsch
            %model.expandfactor = cos( 2^(-S)*pi )^(-2);  % constant by protasov
            assert( 1/model.expandfactor >= model.relaccuracy, 'polytopenorm:option', 'number of vertices of polytopes ''S'' and ''relativeaccuracy'' not compatible.\nRemove ''S'', increase ''S'' or decrease ''relativeaccuracy''.' );
            expect( model.relaccuracy > 0.7 && model.relaccuracy < 1, 'polytopenorm:option', '''relativeaccuracy'' is usually a number smaller and nearly to one.' );
            
            al = @(m) 2^(-m)*pi;
            J = N;
            
            L = 1 + J + J*(2*S+2);  % length of coefficient matrix A
            model.obj = [1; zeros( L-1, 1 )];  % c vector
            model.lb = -inf( L, 1 );
            
            A0 = [-1 ones( 1, J ) zeros( 1, L-J-1 )];
            A1a = zeros( J, L );
            for j = 1:J;
                A1a(j,1+J+(j-1)*(2*S+2)+2) = -1; end;
            A1b = zeros( J, L );
            talSm1 = tan( al(S-1) );
            for j = 1:J;
                A1b(j,1+J+(j-1)*(2*S+2)+1) = -talSm1; 
                A1b(j,1+J+(j-1)*(2*S+2)+2) = 1; end;
            A2 = zeros( J, L );
            talS = tan( al(S) );
            for j = 1:J;
                A2(j,1+j) = -1;
                A2(j,1+J+(j-1)*(2*S+2)+1) = 1; 
                A2(j,1+J+(j-1)*(2*S+2)+2) = talS; ; end;
            A3 = zeros( J*S, L );
            A4a = zeros( J*S, L );
            A4b = zeros( J*S, L );
            for s = 1:S;
                calSms = cos( al(S-s) );
                salSms = sin( al(S-s) );
                for j = 1:J;
                    idx1 = j+(s-1)*J;
                    idx2 = 1+J+(j-1)*(2*S+2)+(s-1)*2+1:1+J+(j-1)*(2*S+2)+(s-1)*2+4;
                    A3(idx1,idx2) = [-calSms -salSms +calSms +salSms]; 
                    A4a(idx1,idx2) = [-salSms +calSms -salSms +calSms]; 
                    A4b(idx1,idx2) = [-salSms +calSms +salSms -calSms]; end; end;
            A5 = [zeros( J, 1 ) -eye( J ) zeros( J, L-J-1 )];
            
            a = real( VV );
            b = imag( VV );
            A6 = zeros( dim, L );
            for j = 1:J;
                A6(:,1+J+(j-1)*(2*S+2)+2*S+1:1+J+(j-1)*(2*S+2)+2*S+2) = [a(:,j) b(:,j)]; end;
            
    
            model.A = sparse( [A0;A1a;A1b;A2;A3;A4a;A4b;A5;A6] );
            model.sense = ['='; 
                          repmat( '<', [J+J+J 1] ); 
                          repmat( '=', [J*S 1 ]); 
                          repmat( '<', [2*J*S+J 1] ); 
                          repmat( '=', [dim, 1] )]; 
            L2 = size( model.sense, 1 );
            
            model.rhs = zeros( L2, 1 );  % b-vector, last entries are the ellipse points
            model.modelsense = 'min';
            
            params.FeasibilityTol = ipa_constant.GUROBI_EPSILON;  % smaller epsilon is faster??
            params.OptimalityTol = ipa_constant.GUROBI_EPSILON;
            params.Method = 3;  % 0=primal simplex, 1=dual simplex, 2=barrier (interior point)
            
        otherwise;
            error( 'polytopenorm:unimplemented', 'Chosen algorithm not implemented.' ); end;
        
    parsem( varargin, 'test' );

    params.OutputFlag = tif( opt.verbose >= 4, 1, 0 );

    % parfeval
    %%%%%%%%%%%%
    [~, idx] = sort( num, 'descend' );  % sort in descending order, such that the ones which take hopefully the longest start at first. We hope that this leads to a 100% CPU load most of the time.
    MSTresults = cell( 1, num_chains );
    if( opt.num_core >= 1 && ~opt.debug );
        algorithm = opt.algorithm;
        epsilon_display = opt.epsilon_display;
        verbose = opt.verbose;
        for w = idx;
            MSTpool(w) = parfeval( p, @polytopenorm_gurobi_worker, 1, N, chain{w}, model, params, MST{w}, nearpointidx{w}, algorithm, epsilon_display, verbose ); end;  %#ok<AGROW>

        % Collect the results as they become available.
        for w = num_chains:-1:1;
            [idx, value] = fetchNext( MSTpool );  % fetchNext blocks until next results are available.
            MSTresults{idx} = value;
            if( verbose >= 1 );
                fprintf( '%c', (MSTresults{idx}.Nodes.char).' ); end; end;
    else;
        for w = 1:num_chains;
            try;
                if( toc(opt.starttime) < opt.maxtime );
                    MSTresults{w} = polytopenorm_gurobi_worker( N, chain{w}, model, params, MST{w}, nearpointidx{w}, opt.algorithm, opt.epsilon_display, opt.verbose );
                    end;
            catch me;
                %fprintf( 2, ['Error happend. This should not happen, but it does sometimes (out of memory, licence expired, ... ).\n' ...
                %             'To prevent loss of hard work, the debugger is started unconditionally.\n'] );
                disp( me );
                disp( me.stack(1) );
                %keyboard;
                end;
            if( opt.verbose >= 1 );
                fprintf( '%c', (MSTresults{w}.Nodes.char).' ); end; end; end;
    
    MST = MSTresults;
    clear MSTresults;

    val = cell( 1, numel(MST) );
    for i_MST = 1:numel( MST );
        val{i_MST} = MST{i_MST}.Nodes; end;
    val = vertcat( val{:} );
        
    val = sortrows( val, 'idx' );
    iter = sum( val.iterations );
    ret = val.ret.';
    iv = val.iv.';
    time = val.time.';
    
    if( opt.verbose >= 4 );  % Plot MST
        clf; 
        hold on;
        h = cell( 1, num_chains );
        for w = 1:num_chains
            h{w} = plot( MST{w}, 'k', 'LineWidth',1, 'Layout','layered' );
            highlight( h{w}, 1 );
            drawnow;
            h{w}.XData = MST{w}.Nodes.co(:,1);
            h{w}.YData = MST{w}.Nodes.co(:,2);
            if(dim >= 3); 
                h{w}.ZData = MST{w}.Nodes.co(:,3); end;
            %h{w}.NodeLabel = MST{w}.Nodes.norm;
            h{w}.NodeLabel = MST{w}.Nodes.iterations; end;
        axis equal
        drawnow; end;

end

function [ MST ] = polytopenorm_gurobi_worker( N, chain, model, params, MST, nearpointidx, algorithm, epsilon_display, verbose ); 

    num = size( chain, 1 );
    dim = size( MST.Nodes.co, 2 );
    ret = zeros( 1, num );
    iteration = zeros( num, 1 );
    timeval = zeros( 1, num );
    iv = cell( num, 1 );
    co = MST.Nodes.co;
    
    switch algorithm;
        case {'l'};
            sze = size( model.A );
            cbasis = int8( zeros(num, sze(1)) );
            vbasis = int8( zeros(num, sze(2)) ); 
        case {'k0','k1','k2','k6','k7'};
            sze = size( model.A );
            cbasis = int8( zeros(num, sze(1)) );
            vbasis = int8( zeros(num, sze(2)) ); 
        case {'k3','k4','k5'};
            % do nothing
        case {'p'}; 
            sze = size( model.A );
            cbasis = int8( zeros(num, sze(1)) );
            vbasis = int8( zeros(num, sze(2)) ); 
        case {'r'}; 
            cbasis = int8( zeros(num, dim+3*N+1) );
            % what about vbasis ? % Old comment: vbasis not needed, since it is always zero??
        case {'c','c1','c2','c3','c4','c5'};
            % do nothing
        otherwise;
            fatal_error; end;

    for i = 1:num;
        starti = tic;
        idxnew = chain(i,2);  % idx of point to be processed
        idxold = chain(i,1);  % idx of parent vertex
        switch algorithm;
            case 'l';
                model.A(1:dim,1) = -co(idxnew,:);
                if( idxold );
                    model.cbasis = double( cbasis(idxold,:).' );
                    model.vbasis = double( vbasis(idxold,:).' ); end;
                result = gurobi( model, params );

            case {'k0','k1','k6'};
                model.rhs(1:dim) = co(idxnew,:);
                if( idxold );
                    model.cbasis = double( cbasis(idxold,:).' );
                    model.vbasis = double( vbasis(idxold,:).' ); end; 
                result = gurobi( model, params );

            case {'k2'};
                model.A(1:dim,1) = -co(idxnew,:).';
                if( idxold );
                    model.cbasis = double( cbasis(idxold,:).' );
                    model.vbasis = double( vbasis(idxold,:).' ); end; 
                result = gurobi( model, params );
                if( strcmp(result.status, 'INF_OR_UNBD') || ...
                    strcmp(result.status, 'UNBOUNDED') ...
                  );
                    model.ub = [1e5; inf( N, 1 )];  % Danger: model is unbounded if result would be zero
                    result2 = gurobi( model, params );
                    if( strcmp(result2.status, 'OPTIMAL') );
                        result = result2;
                        result2.x(1) = 1e12; end; end;  % infinity would be interpreted as an error

            case {'k3'};
                for s = 1:dim;
                    model.quadcon(s).rhs = co(idxnew,s); end
                result = gurobi( model, params );

            case {'k4'};
                for s = 1:dim;
                    model.quadcon(s).rhs = co(idxnew,s); end;
                result = gurobi( model, params );
                if( strcmp(result.status, 'INF_OR_UNBD') || ...
                    strcmp(result.status, 'UNBOUNDED') ...
                  );
                    model.lb(1) = -1e5;
                    result2 = gurobi( model, params );
                    if( strcmp(result2.status, 'OPTIMAL') );
                        result = result2;
                        result2.x(1) = -1e12; end; end;  % infinity would be interpreted as an error

            case {'k5'};
                for s = 1:dim;
                    model.quadcon(s).rhs = co(idxnew,s); end;
                result = gurobi( model, params );
                
            case {'k7'};
                model.A(:,1) = [0;-co(idxnew,2:end)'];
                model.rhs(1) = co(idxnew,1);
                if( idxold );
                    model.cbasis = double( cbasis(idxold,:).' );
                    model.vbasis = double( vbasis(idxold,:).' ); end;
                result = gurobi( model, params );
                if( strcmp(result.status, 'INF_OR_UNBD') || ...
                    strcmp(result.status, 'UNBOUNDED') ...
                  );
                    model.ub = inf( size(model.lb) );  % Danger: model is unbounded if result would be zero
                    model.ub(1) = 1e5;
                    result2 = gurobi( model, params );
                    if( strcmp(result2.status, 'OPTIMAL') );
                        result = result2;
                        result2.x(1) = 1e12; end; end;  % infinity would be interpreted as an error
                %repr( model )
                %full(model.A)
                
            case {'p'}; 
                model.A(1:dim,end) = co(idxnew,:);
                if( idxold ); 
                    % if point is near to a point which has already been computed
                    % It seems that a basis from a solution from a real vertex is always better than a manually basis from a vertex-point from the polytope.
                    % Thus we do not need the distances between the points in the chain for the linear-programming part
                    model.cbasis = double( cbasis(idxold,:).' );
                    model.vbasis = double( vbasis(idxold,:).' ); end; 
                result = gurobi( model, params );
                
            case {'r'}; 
                model.A(end-dim+1:end,end) = -MST.Nodes.co(idxnew,:);
                % warm start
                if( idxold ); 
                    % if point is near to a point which has already been computed
                    % It seems that a basis from a solution from a real vertex is always better than a manually basis from a vertex-point from the polytope.
                    % Thus we do not need the distances between the points in the chain for the linear-programming part
                     model.cbasis = double( cbasis(idxold,:).' );
                     model.vbasis = zeros( 2*N+1, 1 );
                else
                    % if no point near to this point has been computed so far, we make a manual basis stemming from a solution of a vertex point of the polytope
                    idx = nearpointidx(idxnew);
                    cbasisVV = zeros( 3*N+1+dim, 1 );
                    cbasisVV(1) = -1; 
                    cbasisVV(end-dim+1:end) = -1;
                    cbasisVV(1+idx+N) = -1; 
                    cbasisVV(2+2*N:idx+2*N) = -1; 
                    cbasisVV(2+2*N+idx:end-dim) = -1;
                    missing = 2*N+1-nnz( cbasisVV );
                    if( missing > 0 ); 
                        val = min( idx-1, missing );
                        cbasisVV(2+N:val+2+N-1) = -6; end;
                    missing = 2*N+1-nnz( cbasisVV );
                    if( missing > 0 ); 
                         val = min( N-idx, missing );
                         cbasisVV(2+N+idx:val+2+N+idx-1) = -7; end;
                    model.cbasis = cbasisVV;
                    model.vbasis = zeros( 2*N+1, 1 ); end;
                result = gurobi( model, params );
                
            case {'c1','c2'};
                model.A(1:2*dim,1) = [-real( co(idxnew,:) ) -imag( co(idxnew,:) )]; 
                result = gurobi( model, params );
                
            case {'c3'};
                J = model.J;  % number of vertices of approximating polytope
                pj = co(idxnew,:);
                pa = real( pj );
                pb = imag( pj );
                beta = 0:pi/J:pi-pi/J;  % angle-stepwidth of approximated ellipse
                pjapprox = pa.*(cos( beta ).') + pb.*(sin( beta ).');  % approximated ellipse
                model.A(1:J,1:dim) = pjapprox;
                result = gurobi( model, params );

            case {'c5','c'};
                % XX missing warm start
                pj = co(idxnew,:).';
                iter = 0;
                [pg, pg1, pgsj, nrm_pg] = deal( cast([], class(pj)) );  %#ok<ASGLU> 
                model.cbasis = zeros( 1, size(model.A, 1) );
                model.vbasis = zeros( 1, size(model.A, 2) );
                while( true );
                    [pg, pgsj, nrm_pg, oldpg, oldnrm] = complexfunct_d_splitvertex( pj, pg, pgsj, nrm_pg );
                    idx = any( isnan(nrm_pg), 1 );
                    for ii = find( idx );
                        model.rhs(end-dim+1:end) = -pg(:,ii).';
                        result = gurobi( model, params );
                        model.cbasis = result.cbasis;
                        model.vbasis = result.vbasis;
                        badcomputation = ~strcmp( result.status, 'OPTIMAL' );
                        if( ~badcomputation );
                            iter = iter + result.itercount;
                            factor = model.expandfactor * 1/cos( pi/(2*2^floor(log2(pgsj(ii)))) );
                            val = result.x(1);
                            nrm_pg(:,ii) = [val;val/factor];
                        else;
                            idx = any( isnan(nrm_pg), 1 );pg(:,idx) = repmat( oldpg, [1 nnz(idx)] );
                            nrm_pg(:,idx) = repmat( oldnrm, [1 nnz(idx)] ); 
                            break; end; end;
                    mx = max( nrm_pg(1,:) );
                    mn = max( nrm_pg(2,:) );
                    if( badcomputation );
                        break; end;
                    epsilonout = 5e-9;
                    if( ~isempty(model.bound) && (mn >= model.bound(2) || ...
                                                  mx <= model.bound(1) || ...
                                                  mn >= model.bound(1) && mx <= model.bound(2) || ...
                                                  mx - mn < epsilonout ...
                                                 ) ...
                      );
                        break; end;
                    if( mn/mx > model.relaccuracy );
                        break; end;
                    if( mx-mn < model.absaccuracy );
                        break; end;
                    if( size(pg, 2) >= model.maxvertex );
                        break; end; end;
                if( verbose >= 3 );
                    plotm( pg/mx, 'g.-', 'funct','r', 'hold','on' ); end; 
                x = [mx;mx + 5e-9;max( 0, mn - 5e-9 )];
                result.itercount = iter;

            otherwise;
                fatal_error; end;


            iteration(idxnew) = result.itercount;
            if( ~isequal(result.status, 'OPTIMAL') );
                ret(idxnew) = NaN;
            else;
                switch algorithm;
                    case {'l'};
                        cbasis(idxnew,:) = int8( result.cbasis.' );
                        vbasis(idxnew,:) = int8( result.vbasis.' );
                        ret(idxnew) = result.x(1);
                        iv{idxnew} = (abs(result.x(2:end)) > 5e-5).';

                    case {'k0'};
                        % if the lp is feasible, than the point is inside
                       idx = abs(result.x) > 1e-5;
                       rnk = rank( full(model.A(:,idx)), 1e-5 );
                       if( rnk < dim );
                           % vector is either on the boundary (in which case we set the norm to 1), or
                           % completely outside, in which the norm is set to something larger than 1
                           ret(idxnew) = 1;
                           end;
%                        else;
%                            result.x( (end/2)+1:end ) = -result.x( (end/2)+1:end ) ;
%                            val = result.x;
%                            idx = val < 1-5e-9; val(idx) = val(idx) * (1+5e-9);
%                            idx = val == 0;     val(idx) = 5e-9;
%                            idx = val > 5e-9;   val(idx) = 0;
%                            idx = val < 0;      val(idx) = -val(idx) + 1;
%                            ret(idxnew) = sum( val ); end;
                        cbasis(idxnew,:) = int8( result.cbasis.' );
                        iv{idxnew} = result.x ~= 0;

                    case {'k1'};
                        %ret(idxnew) = max( 0, 1 - result.x(1) );
                        if( any(result.x < 0) );
                            ret(idxnew) = 1 + sum( abs(result.x(2:end)) );
                        else;
                            ret(idxnew) = sum( clamp( result.x(2:end), 0, 1/dim ) );
                            %ret(idxnew) = max( result.x(2:end) );
                            %ret(idxnew) = 1 - min( result.x(2:end) );
                            end;
                    case {'k2'};
                        ret(idxnew) = result.x(1);
                        iv{idxnew} = result.x(2:end) ~= 0;
                    case {'k3'};
                        ret(idxnew) = result.x(end);
                    case {'k4'};
                        ret(idxnew) = 1/(1 - result.x(1));
                    case {'k5'};
                        ret(idxnew) = 1/(1 - result.x(1));  % XX here is a seemingly important! scaling. Why?
                    case {'k6'};
                        ret(idxnew) = 1/(1 + result.x(1));
                        if( ret(idxnew) < 0 );
                            ret(idxnew) = inf; end;
                    case {'k7'};
                        cbasis(idxnew,:) = int8( result.cbasis.' );
                        vbasis(idxnew,:) = int8( result.vbasis.' );
                        ret(idxnew) = 1/result.x(1);
                        if( ret(idxnew) < 0 );
                            ret(idxnew) = inf; end;
                        iv{idxnew} = (abs(result.x(2:end)) > 5e-5).';
                    case {'p'};
                        cbasis(idxnew,:) = int8( result.cbasis.' );
                        vbasis(idxnew,:) = int8( result.vbasis.' );
                        ret(idxnew) = abs( 1/result.x(end) );
                        iv{idxnew} = (abs(result.x(1:end-1)) > 5e-9).';
                    case {'r'};
                        cbasis(idxnew,:) = int8( result.cbasis.' );
                        vbasis(idxnew,:) = int8( result.vbasis.' );
                        ret(1,idxnew) = abs( 1/result.x(end) );
                        iv{idxnew} = any( abs(reshape(result.x(1:end-1), [], 2)) > 5e-9, 2 ).';
                    case {'c1'};
                        ret(1,idxnew) = abs( 1/result.x(1) );
                        iv{idxnew} = any( abs(reshape(result.x(2:end), [], 3)) > 5e-5, 2 ).';
                    case {'c2'};
                        if( result.x(1) >= model.ub(1) );
                            ret(1,idxnew) = 0;
                        else;
                            ret(1,idxnew) = abs( 1/result.x(1) ); end;
                        iv{idxnew} = any( abs(reshape(result.x(2:4*N+1), [], 4)) > 5e-5, 2 ).';     
                    case {'c3'};
                        ret(1,idxnew) = abs( max( result.x(dim+J+1:dim+J+J) ) );
                        iv{idxnew} = []; 
                    case {'c5','c'};
                        ret(1:3,idxnew) = x;
                        iv{idxnew} = [];
                    otherwise;
                        fatal_error; end; end; 
    timeval(i) = toc( starti ); end;  % do nothing
    
    
    % process ret
    switch algorithm;
        otherwise;
            ret(:,any( ret < -5e-9, 1 )) = inf; end;
        
    switch algorithm;
        case {'l'};
            ret = [1./ret; 1./ret + 5e-9; 1./ret - 5e-9];
        case {'k0'};
            ret = [ret; ret + 5e-9; ret - 5e-9];  % The returned value is either 0, 1, or inf
        case {'k1','k3','k4','k5','k6'};
            ret = [ret; ret+5e-9; ret-5e-9];
        case {'k2'};
            ret = 4./pi.*[atan( 1./ret );
                          atan( 1./ret ) + 1e-6;
                          atan( 1./ret ) - 1e-6];
            ret(ret < 0) = 2;
        case {'k7'};
            ret = [ret; ret+1e-6; ret-1e-6];
        case {'p','r'};
            if( ret < 2/model.ub(end) );
                ret = [zeros( size(ret) ); ret+5e-9; zeros( size(ret) )];
            else;
                ret = [ret; ret+5e-9; ret-5e-9]; end;
        case {'c1'};
            ret = [ret; ret+5e-5; zeros( 1, numel(ret) )];
        case {'c2'};
            ret = [ret; ret+5e-5; (ret-5e-5)/2]; 
        case {'c3'};
            ret = [(model.expandfactor+1)/2*ret; model.expandfactor*(ret+5e-5); ret-5e-5 ]; 
        case {'c5','c'};
            % do nothing, logic is above
        otherwise;
            fatal_error; end;
        
    switch algorithm;
        otherwise;
            ret(ret < 0) = 0;
            ret(~isfinite( ret )) = inf;
            ret(end,:) = max( ret(end,:), 0 ); end;
        
    % Text Output 
    %%%%%%%%%%%%%%%
    switch algorithm;
        otherwise;
            strval = 79*ones( 1,size(ret, 2) );       % O
            %strval(isnan( ret(1,:) )) = 78;           % N  % slow
            strval(~isfinite( ret(1,:) )) = 56;       % 8
            strval(ret(2,:) < 2) = 111;               % o
            strval(ret(2,:) < 1+epsilon_display) = 46;         % .
            strval(ret(2,:) < 1+1000*epsilon_display) = 44;    % ,
            strval(ret(2,:) < 1-epsilon_display) = 95;         % _
            strval(ret(2,:) < 0) = 109;                % m
            end;
                
    % Save Output
    %%%%%%%%%%%%%%%
    MST.Nodes.ret = ret.';
    MST.Nodes.char = strval.';
    MST.Nodes.iterations = iteration;
    MST.Nodes.iv = iv;
    MST.Nodes.time = timeval.'; 

end

function [ pg, pgsj, nrm_pg, oldpg, oldnrm ] = complexfunct_d_splitvertex( pt, pg, pgsj, nrm_pg );
% Cuts away the vertex with he largest norm
% [ pg, pgsj, nrm_pg, oldpg, oldnrm ] = complexfunct_d_splitvertex( pt, pg, pgsj, nrm_pg );
%
% Input:
%   pt      dim x 1 complex vector, complex point, starting ellipse, is only used if pg is empty
%   pg      dim x J array of real column vectors, coordinates of the polygon approximating the ellipse
%   pgsj    1 x J vector of integers, vertices of the polygon approximating the unit circle encoded as integers
%               the points pg are computed by: converting the integers to vertices, and then mapping these to the ellipse
%               Start: 2, 3: [1 -1].', [1 1]-';
%               Vertics (2i, 2i+1) are produced by cutting away vertex i
%   nrm_pg  2 x J vector of doubles, max/min bound of the norm of the vertices
%
% Output:
%   pg      dim x (J+1) array of real column vectors, like input-pg, but vertex with the largest norm, is replaces by two vertices arising from cutting
%   pgsj    resp.
%   nrm_pg  resp.
%   oldpg   dim x 1 real vector, coordinates of the cutted vertex
%   oldnrm  2 x 1 vector, max/min bound of the cutted vertex
%
% Written by: tommsch, 2021-03-07

    if( isempty(pg) );
        pgsj = [2 3];
        pg = [real(pt) imag(pt)]*[1 -1; 1 1].';
        nrm_pg = cast( nan( 2, size(pg, 2) ), class(pt) );
        oldpg = nan( size(pg, 1), 1 );
        oldnrm = [inf;0];
    else;
        [~,idx] = max( nrm_pg(1,:), [], 2 );
        sj = 2*pgsj(:,idx(1));
        s = 2^floor( log2(sj) );
        j = sj - s;
        pgsj = [pgsj(1:idx-1) sj sj+1 pgsj(idx+1:end)];
        oldpg = pg(:,idx);
        oldnrm = nrm_pg(:,idx);
        pg = [pg(:,1:idx-1) [real(pt) imag(pt)]*[cos(pi*[j j+1]./s-pi/2+pi/(2*s)); sin(pi*[j j+1]./s-pi/2+pi/(2*s))]/cos(pi/(2*s)) pg(:,idx+1:end)]; 
        nrm_pg = [nrm_pg(:,1:idx-1) nan(2, 2) nrm_pg(:,idx+1:end)]; end;
end


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% symbolic computations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ normval ] = polytopenorm_exact( pts, VV, aux, normval, iv, opt );

    wold = warning;
    if( ismatlab );
        warning( 'off', 'symbolic:mldivide:InconsistentSystem' );
        warning( 'off', 'symbolic:mldivide:RankDeficientSystem' ); end;
    cleanwarn = onCleanup( @() warning(wold) );
    vprintf( '(%s)', class( pts ), 'imp',[1 opt.verbose] );

    switch opt.algorithm(1);
        case {'l'};
            error( 'polytopenorm:algorithm', 'Symbolic computation for case ''l'' is not implemented yet.' );
        case {'k'};
            [normval] = polytopenorm_koneexact( pts, VV, aux, iv, opt );
        case {'p'};
            [normval] = polytopenorm_posexact( pts, VV, aux, normval, iv, opt );
        case {'r'};
            [normval] = polytopenorm_minkexact( pts, VV, aux, iv, opt );
        case {'c'};
            [normval] = polytopenorm_complexexact( pts, VV, aux, iv, opt );
        otherwise;
            error( 'polytopenorm:algorithm', 'wrong algorithm given' ); end;
end

function [ normval ] = polytopenorm_koneexact( pts, VV, aux, iv, opt );
    nVV = size( VV, 2 );
    dim = size( pts, 1 );
    npts = size( pts, 2 );

    normval = repmat( sym([1;inf;0]), [1 npts] );
    center_provided = ~isempty( aux );
    if( center_provided );
        center = aux/norm( aux );
    else;
        center = center_kone( VV, 'deterministic' );
        end;
    ns = nullspace_kone( center );
    C = [center ns];

    verbose = opt.verbose;
    bound = opt.bound;
    if( isempty(bound) );
        bound_max = 1;
    else;
        bound_max = sym( max( bound ) ); end;
    idx_sym = false( 1, npts );
    for rounds_ = {'vpa','sym'}; rounds = rounds_{1};  % I do not know whether a round with 'num' is faster
        switch rounds;
            case 'num';
                ptsr = double( pts );
                VVr = double( VV );
                Cr = double( C );
                epsilon = 10^(-15+2*dim);
            case 'vpa';
                ptsr = vpa( pts );
                VVr = vpa( VV );
                Cr = vpa( C );
                epsilon = vpa( 10^(-digits + 2*dim) );
            case 'sym';
                ptsr = vpa( pts );
                VVr = vpa( VV );
                Cr = vpa( C );
                epsilon = vpa( 10^(-digits + 2*dim) );
            otherwise;
                fatal_error; end;
        ptsr = Cr'*ptsr;
        VVr = Cr'*VVr; 
        
        parfor i = 1:npts;
            if( ismatlab );  % needed for parfor loop
                warning( 'off', 'symbolic:mldivide:InconsistentSystem' );
                warning( 'off', 'symbolic:mldivide:RankDeficientSystem' ); end;
            if( isempty(iv{i}) );
                continue; end;
            if( isequal(rounds, 'sym') && ~idx_sym(i) );
                continue; end;
            
            % first check whether the point is part of the set of vertices in VV
            if( nnz(iv{i}) <= dim );
                res = normalizematrix( VVr(:,iv{i}), 'colnorm',inf ) - normalizematrix( ptsr(:,i), 'colnorm',inf );  %#ok<PFBNS>
                if( any( all( isAlways( abs(res) <= epsilon, 'Unknown','false' ), 1 ) ) );
                    if( isanyof(rounds, {'num','vpa'}) );
                        idx_sym(i) = true;
                    else;
                        normval(2,i) = 1; end;
                    continue; end; end;
            
            % compute upper bound
            assert( numel(iv{i}) == nVV, 'polytopenorm:fatal', 'Number of entries in iv does not equal number of vertices in nVV.' );
            ptsri = ptsr(:,i);  % copy is needed for better parfor performance
            At = [VVr(:,iv{i}) [0;-ptsri(2:end,1)]];
            bt = [ptsri(1,1); zeros( dim-1, 1 )];
            xt = At\bt;
            if( isanyof(rounds, {'vpa','num'}) );
                if( norm( imag(xt) ) <= epsilon && ...
                    all( isAlways( real(xt) >= 0, 'Unknown','true' ) ) && ...
                    isAlways( norm( At*real(xt) - bt ) <= epsilon, 'Unknown','false' ) && ...
                    abs( 1/real(xt(end)) ) - bound_max < epsilon ...
                  );
                    idx_sym(i) = true; end;
            else;
                if( ~all( isAlways( xt >= 0, 'Unknown','true' ) ) && ...
                    ~all( isAlways( At*xt == bt ) ) ...
                   );
                    continue; end;
                if( isAlways(xt(end) > 1/normval(2,i)) );  % xt = 1/r
                    if( isAlways( xt(end) == 1/bound_max ) );  % this is typically the expected result and faster then 'simplify( r )'
                        normval(2,i) = 1/bound_max;
                    else;
                        normval(2,i) = real(1/xt(end)); end; end; end; end; end;

    if( verbose >= 1 );
        for i = 1:npts;
            if( ~isfinite(normval(2,i)) );
                fprintf( 'o' );
            elseif( isAlways( normval(2,i) <= bound_max, 'Unknown','false' ) );
                fprintf( '_' );
            else;
                fprintf( 'o' ); end;
            drawnow; end; end;
end

function [ normval ] = polytopenorm_posexact( pts, VV, aux, normval, iv, opt );
%     normval = polytopenorm_posexact_new( pts, VV, aux, normval, iv, opt );
    normval = polytopenorm_posexact_old( pts, VV, aux, normval, iv, opt );
end

function [ normval ] = polytopenorm_posexact_new( pts, VV, ~, normval, iv, opt );    
    for i = 1:size( pts, 2 );
        dim = size( pts, 1 );
        if( isempty(iv{i}) );
            continue; end;
        ptsi = pts(:,i);
        ivi = iv{i};
        Vi = VV(:,ivi);
        if( nnz( ivi ) < dim );
            idx = all( isAlways( pts(:,i)/normval(1,i) < Vi - 1e-7 ), 2 );
            Ve = Vi;
            assert( nnz(idx) + nnz(ivi) == dim );
            Ve(idx,:) = 0;
            Vi = [Vi Ve]; end;  %#ok<AGROW>
        coi = Vi\ptsi;
        assert(all(isfinite(coi)));  % XX DEBUG
        success = any( isAlways(Vi*coi - ptsi == 0, 'Unknown','false') ); 
        nrm = sum( coi );
        if( success );
            normval(:,i) = [nrm;nrm;0];
        else;
            normval(:,i) = [1;inf;0]; end;
    end
end

function [ normval ] = polytopenorm_posexact_old( pts, VV, aux, normval, iv, opt );
    dim = size( VV, 1 );
    if( dim > 13 );
        normval = repmat( [nan;inf;0], [1 size(pts, 2)] );
        return; end;
    VVout = [];
    C = nmultichoosek( 1:dim, dim-1 );
    for i_C = 1:size( C, 1 );
        c = C(i_C,:);
        val = VV;
        val(c,:) = 0;
        VVout = [VVout val]; end;  %#ok<AGROW>
    VVout(:,~any( VVout, 1 )) = [];  % delete zero vector
    VVout = unique( VVout.', 'rows' ).';
    VVout = setdiff( VVout.', VV.', 'rows' ).';
    num = size( VVout, 2 );
    for i_iv = 1:numel( iv )
        if( ~isempty(iv{i_iv}) );
            iv{i_iv} = [iv{i_iv} true(1,num)]; end; end;
    VVout = [VV VVout];
    normval = polytopenorm_minkexact( pts, VVout, aux, iv, opt );
end

function [ combs ] = nmultichoosek( values, k );
    % Return number of multisubsets or actual multisubsets.
    if( numel(values) == 1 );
        n = values;
        combs = nchoosek( n+k-1, k );
    else;
        n = numel( values );
        combs = bsxfun( @minus, nchoosek(1:n+k-1,k), 0:k-1 );
        combs = reshape( values(combs), [], k ); end;
end

function [ normval ] = polytopenorm_minkexact( pts, VV, ~, iv, opt ); 

    % nVV = size( VV, 2 );
    dim = size( pts, 1 );
    npts = size( pts, 2 );
    
    normval = repmat( [1;inf;0], [1 npts] );
    if( issym(pts) );
        normval = sym( normval ); end;
    
    VV_d = anycast( VV, 'double', 'unsafe' );
    pts_d = anycast( pts, 'double', 'unsafe' );
    
    compute_all = all( cellfun( 'prodofsize', iv ) == 0 );
    
    for i_pts = 1:npts;  % XX speed up this part
        normj_max = inf;
        normj_min = inf;

        pt = pts(:,i_pts);
        pt_d = pts_d(:,i_pts);

        diff_d = sum( abs(pt_d - VV_d), 1 );
        if( any(diff_d == 0) );
            idx = diff_d < 1e-12;
            diff = sum( abs(pt - VV(:,idx)), 1 );
            if( any(isAlways( diff == 0, 'Unknown','false') ) );
                normj_max = 1; end; end;
        
        if( ~isempty(iv{i_pts}) );
%             assert( numel(iv{i_pts}) == nVV );  % XX DEBUG
            VVi = VV(:,iv{i_pts});
            VVi_d = VV_d(:,iv{i_pts});
        elseif( compute_all );
            VVi = VV; 
            VVi_d = VV_d;
        else;
            continue; end;
        nVVi = size( VVi, 2 );
        if( nVVi < dim );
            VVi( dim, dim ) = 0;
            VVi_d( dim, dim ) = 0;
            nVVi = dim; end;
        P = nchoosek( 1:nVVi, dim );
        % first compute it purely numerically, then sort permutation vector wrt. obtained norm, then compute symbolically
        normvalesti = inf( 1, size(P, 1) );
        for j = 1:size( P, 1 );
            VVidj = VVi_d(:,P(j,:));
            if( rank(VVidj) == dim );
                normvalesti(j) = sum( abs(VVidj\pt_d), 1 ); 
            else;
                normvalesti(j) = inf;
                end; end;
        idx_finite = isfinite( normvalesti );
        P = P(idx_finite,:);
        normvalesti = normvalesti(idx_finite);
        [normvalesti, idx] = sort( normvalesti );
        P = P(idx,:);

        [w_msg, w_id] = lastwarn();
        for j = 1:size( P, 1 );
            VVij = VVi(:,P(j,:));
            if( normvalesti(j) < inf && ...
                rank( VVij ) == dim ...    
              );
                val = sum( abs(VVij\pt), 1 ); 
                normj_max = min( val, normj_max );
                normj_min = normj_max;
                if( abs( double(normj_max) - double(normj_min) ) < 1e-12 &&...
                    isAlways( normj_max == normj_min, 'Unknown','false' ) ...
                  );
                    break; end;
                if( ~isempty(opt.bound) && min(opt.bound) == 0 && ...  % XX this can be improved, currently a lower bound is computed, but not used
                    isAlways( normj_max <= max(opt.bound), 'Unknown','false' ) ...
                  );
                    break; end; end; end;
        lastwarn( w_msg, w_id );
        if( opt.verbose >= 1 );
            if( isempty(opt.bound) );
                bound = 1;
            else;
                bound = max( opt.bound ); end;
            if( ~isfinite(normj_max) && ~isfinite(normj_min) );
                fprintf( 'E' );
            elseif( isAlways( normj_max <= bound, 'Unknown','false' ) );
                fprintf( '_' );
            else;
                fprintf( 'o' ); end; end;
        if( isempty(iv{i_pts}) );
            normval(:,i_pts) = [normj_max;normj_max;normj_min];
        else;
            normval(:,i_pts) = [normj_max;normj_max;0]; end; end;
    
end

function [ normval ] = polytopenorm_complexexact( pts, VV, aux, iv, opt );  %#ok<INUSL>

    nVV = size( VV, 2 );
    npts = size( pts, 2 );
    normval = sym( [ones(1,npts); inf(1,npts); zeros(1,npts)] );
    
    for i = 1:npts;
        an = real( pts(:,i) );
        bn = imag( pts(:,i) );
        Kn = [an bn]*[an bn].';
        normj = inf;
        for j = 1:nVV
            a = real( VV(:,j) );
            b = imag( VV(:,j) );
            try;
                if( rank([a b an bn]) ~= 2 );
                    continue; end;
            catch me;  %#ok<NASGU>  % rank throws sometimes when there is operator real/imag in the symbolic expression
                continue; end;
            K = [a b]*[a b].';
            [w_msg, w_id] = lastwarn;
            K = K\Kn;
            lastwarn( w_msg, w_id );
            if( any(~isfinite(K(:))) );
                continue; end;
            r = rhot( K );
            normj = min( normj, sqrt( r ) ); end; 
        normval(1:2,i) = normj; 
        if( opt.verbose >= 1 );
            if( ~isfinite(normj) );
                fprintf( 'E' );
            elseif( isAlways(normj < max(opt.bound)) );
                fprintf( '_' ); end; end;
    
    end;
        
end

function [ pts,  VV] = normalize_for_kone( pts, VV );
    idx_remove = ~any( VV, 1 );
    VV(:,idx_remove) = [];
    VV = VV./(sum( abs(VV).^2, 1 ).^(1/2));
    idx_zero = ~any( pts, 1 );
    pts(:,~idx_zero) = pts(:,~idx_zero)./(sum( abs(pts(:,~idx_zero)).^2, 1 ).^(1/2));
end

function [ varargout ] = cast_to( varargin );
% [ varargout ] = cast_to( varargin, target_type );
% casts all inputs to target_type
    assert( nargout == nargin - 1 );
    if( ~ischar(varargin{1}) && ischar(varargin{end}) );
         target_type = varargin{end};
         varargin(end) = [];
     else;
        fatal_error; end;
    varargout = cell( size(varargin) );
    for i = 1:numel( varargin );
        if( iscell(varargin{i}) );
            varargout{i} = cell( size(varargin{i}) );
            for j = 1:numel( varargin{i} );
                varargout{i}{j} = eval( [target_type '( varargin{i}{j} )'] ); end;
        else;
            varargout{i} = eval( [target_type '( varargin{i} )'] ); end; end;
end


%% %%%%%%%%%%%%%%%%%
% Constants
%%%%%%%%%%%%%%%%%%%%%%

function [ ret ] = C3_AUX();
% half the number of vertices of the polytope approximating the ellipse
    ret = 8;
end

function dummy; end  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>