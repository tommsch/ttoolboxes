function [ la ] = leadingeigenvalue( M, tol_factor )
% [ la ] = leadingeigenvalue( M, [eps] )
% [ la ] = leadingeigenvalue( vec, [eps] )
% returns all leading eigenvalues, i.e. eigenvalues which differ relatively in magnitude not by more than eps compared to the eigenvalue with largest magnitude
%
% Input:
%   M       matrix, OR cell array of matrices
%   vec     vector of eigenvalues, OR cell array of vector of eigenvalues
%
% Output:
%   la      all eigenvalues largest in magnitude
%
% E.g.: leading_eigenvalue( diag( [100 101 99 4 2 100] ), .01 )
%
% Written by: 2024-03-15, tommsch

%               tommsch,    2024-03-15,     New function
% Changelog:    tommsch,    2024-10-28,     Behaviour change: Changed `eps` to `tol_factor`
%               tommsch,    2024-11-06,     New input option: Can directly pass vector of eigenvalues

    if( nargin <= 1 || isempty(tol_factor) );
        tol_factor = 4; end;

    [M, cellflag] = tocell( M );

    la = cell( size(M) );
    for j = 1:numel( M );
        if( ~issym(M{j}) );
            tolerance = max( size(M{j}) ) * eps( norm(M{j}) );
        elseif( issymstrict(M{j}) );
            tolerance = 0;
        elseif( any( isvpa(M{j}) ) );
            tolerance = max( size(M{j}) ) * 10^(-digits);
        else;
            fatal_error; end;
        tolerance = tolerance * tol_factor;
        if( ~issparse(M{j}) );
            if( isvector(M{j}) || isdiag(M{j}) );
                la{j} = M{j};
            else;
                la{j} = eig( M{j} ); end;
            if( ~isvector(la{j}) );
                la{j} = diag( la{j} ); end;
            [mx, idx] = max( abs(la{j}) );
            if( isAlways(abs(mx) > tolerance, 'Unknown','false') );
                idx2 = isAlways( abs(1 - abs(la{j}./la{j}(idx))) <= tolerance );
                la{j} = la{j}( idx2 );
                la{j} = sort( la{j} ); end;
        else;
            opts.disp = 0;
            la{j} = eigs( M{j}, 1, 'LM', opts ); end; end;

    if( ~cellflag );
        la = la{1}; end;
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
