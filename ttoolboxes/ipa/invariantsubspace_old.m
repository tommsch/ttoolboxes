function [ M_ret, B, warningflag ] = invariantsubspace_old( varargin )
% [ M_ret, B ] = invariantsubspace_old( M, ['type'], [options] )
% Searches for invariant subspaces of matrices M.
%
% Input:
%   M       Cell array of matrices
%   'type'  A string of the following
%               'none'      Nothing happens,  M_ret={M}, B=eye(dim)
%               'auto'      (default): 'perm' for positive matrices, everything for the rest
%               'perm'      Tries to find a permutation such that M is in block-diagonal form, using permTriangult, originally from the JSR-toolbox by Jungers
%               'basis'     Tries to find a basis such that M is in block-diagonal form, using jointTriangult, originally from the JSR-toolbox by Jungers
%               'diff'      If M are transition matrices, tries to find subspaces of differences U, as described in <'Smoothness of anisotropic wavelets, frames and subdivision schemes', M. Charina and V.Yu. Protasov> 
%                           First tries numerically, then symbolically, then with vpa.
%   If type is not given, then the algorithm tries in the following order: 'perm', 'basis', 'trans' (only numerically)
%
% Options:
%   'verbose',val           Verbose level, Default=0
%   'sym',val               Determines how to compute
%                               -1,'auto': (default) same as input matrices
%                                0,'double': everything in double (same as old option 'double')
%                                1,'vpa': recompute basis in vpa after computation in double
%                                2,'sym': recompute basis in sym after computation in double
%                                3,'vpastrict': compute everything in vpa (same as old option 'vpa')
%                                4,'symstrict': compute everything in sym (same as old option 'sym')
%   'double'                Casts matrices to double before computing subspaces
%   'vpa                    Casts matrices to vpa before computing subspaces
%   'sym'                   Casts matrices to sym before computing subspaces
%   'epsilon',val           Epsilon used in algorithm 'basis'
%   'V0',val                Subspace used in algorithm 'basis'
%   'outputtype'            debug option: returns as M_ret the output type of the first matrix as a string. 
%                           The algorithm may try to compute the invariant subspaces or not.
%
% Output:
%   M_ret           cell array of the blocks in the block diagonal. 
%                   The output type is determined by 'sym'. 
%                       -1: Equal to input type
%                        0: double
%                        1-4: sym or vpa (currently we cannot do better)
%   B               square matrix, change of basis such that B\M{i}*B has upper block triangular form
%   warningflag     logical, Is set when to result seems inaccuracte
%
% E.g.: [M,B] = invariantsubspace({[1 1 1; 1 0 1; 0 1 0], [1 -1 0; 0 2 0; 1 1 2]},'verbose',2)
%       invariantsubspace( {[1]} );  %yields {{[1]}}
%
% See also: jointTriangult, permTriangult, constructU, restrictmatrix
%
% Written by: tommsch, 2018, and Jungers (JSR-toolbox)

%               tommsch,    2020-06-19, renamed option 'trans' to 'diff'. But old name still works
%               tommsch,    2020-09-25, Behaviour change for option sym, now has similar behaviour as findsmp
%                                       added return value warningflag
%               tommsch,    2021-04-29, added debug option 'outputtype'
%               tommsch,    2021-09-15, output type is at least input type or higher whenever B == eye
% Changelog:

[M, opt] = invariantsubspace_parse( varargin{:} );
if( isempty(M) );
    M_ret = {cell(1,0)};
    B = [];
    warningflag = false;
    return; end;

[M, M_sym, opt] = invariantsubspace_preprocess( M, opt );

[M_ret, B] = invariantsubspace_worker( M, opt );

[M_ret, B, warningflag ] = invariantsubspace_recompute( M, M_sym, M_ret, B, opt );


vprintf( '\nSizes of matrices in invariant subspaces: %r\n', cellfun(@(x) size(x{1},2),M_ret), 'imp',[2 opt.verbose] );
if( opt.outputtype );
    M_ret = class( M_ret{1}{1} ); end;
        
end

function [ M, opt ] = invariantsubspace_parse( varargin );
    [opt.V0, varargin]           = parsem( {'V0'}, varargin, [] );
    [opt.epsilon, varargin]      = parsem( {'epsilon','eps','e'}, varargin, 1e-12 );
    [opt.verbose, varargin]      = parsem( {'verbose','v'}, varargin, 1 );
    [opt.symflag, varargin]      = parsem( {'sym'}, varargin, -1 );
    [val, varargin]              = parsem( {'double','d'}, varargin );
    [opt.simplifytime, varargin] = parsem( {'simplify','simplifytime'}, varargin, 0 );
    [opt.outputtype, varargin]   = parsem( {'outputtype'}, varargin );
    if( val );
        opt.symflag = 0; end;
    [val, varargin]    = parsem( {'vpa'}, varargin );
    if( val );
        opt.symflag = 1; end;
    switch opt.symflag;
        case {-1,0,1,2,3,4};  % do nothing
        case {'auto'}; opt.symflag = -1;
        case {'double','doublestrict','strictdouble','doubledouble'}; opt.symflag = 0;
        case {'vpa','doublevpa'}; opt.symflag = 1;
        case {'sym','doublesym'}; opt.symflag = 2;
        case {'vpastrict','strictvpa','vpavpa'}; opt.symflag = 3;
        case {'symstrict','strictsym','symsym'}; opt.symflag = 4;
        otherwise; error( 'ipa:invariantsubspace', 'Wrong input for ''sym''.' ); end;

    M = varargin{1};
    varargin(1) = [];
    if( ~iscell(M) );
        M = squeeze( num2cell(M,[1 2]) )'; end;
    opt.M_in = M;
    
    if( isempty(M) );
        return; end;
    
    opt.dim = size( M{1}, 2 );
    opt.J = numel( M );
    if( isempty(varargin) ); 
        opt.type = 'auto'; 
    else; 
        opt.type = varargin{1}; 
        varargin(1) = []; end;

    parsem( varargin, 'test' );
end

function [ M, M_sym, opt ] = invariantsubspace_preprocess( M, opt );

    M_sym = cell( size(M) );
    if( opt.symflag==-1 );
        if( isfloat(M{1}) );
            opt.symflag = 0;
        elseif( isvpa(M{1}) );
            opt.symflag = 3;
        elseif( issym(M{1}) );
            opt.symflag = 4; end; end;
    if( opt.symflag == 0 || opt.symflag == 1 || opt.symflag == 2 );
        for j = 1:opt.J;
            M_sym{j} = sym( M{j} );
            M{j} = double( M{j} ); end; end;

    if( opt.symflag == 3 );
        for j = 1:opt.J;
            M{j} = vpa( M{j} ); end; end;

    if( opt.symflag == 4 );
        for j = 1:opt.J;
            M{j} = sym( M{j} ); 
            expect( ~isvpa(M{j}), 'invariantsubspace:cast', 'Cannot convert matrix %i to sym.\n' ); end; end;

    nonneg = isAlways( allm(isAlways([M{:}]  >= 0)), 'Unknown','false' );
    if( isequal(opt.type,'auto') && nonneg ); 
        vprintf( 'Since the matrices are nonnegative, we only search for triagnulating permutations. \n', 'imp',[2 opt.verbose] );
        opt.type = 'perm'; end;
end

function [ M_ret, B ] = invariantsubspace_worker( M, opt );
    opt.EYE = eye( opt.dim );
    vprintf( 'Search for common invariant subspaces: ', 'imp',[2 opt.verbose] )

    switch opt.type;
        case {'n','none'};
            B = cast( opt.EYE, 'like', M{1} );
            M_ret = {M};    

        case {'p','perm','permutation'}
            [ ~, M_ret, B ] = searchperm( M, opt );

        case {'b','basis'}
            [ ~, M_ret, B ] = searchbasis( M, opt );

        case {'trans','transition','transitionmatrix','diff'}
            [ ~, M_ret, B ] = searchdiff( M, opt, 1 );  %flag says: symbolically

        otherwise;
            [ triang, M_ret, B ] = searchperm( M, opt );
            if( ~triang ); 
                [ triang, M_ret, B ] = searchbasis( M, opt); end
            if( ~triang ); 
                s = warning( 'off', 'restrictmatrix:notinvariant' );
                cleanwarning = onCleanup( @() warning(s) );
                [ ~, M_ret, B ] = searchdiff( M, opt, 0 );  %flag says: not symbolically
                end; end; 
end

function [ M_ret, B, warningflag ] = invariantsubspace_recompute( M, M_sym, M_ret, B, opt );
    M_test = cell( size(M) );
    warningflag = false;
    
     if( iseye(B) );
         nfo = matrixinfo( opt.M_in, 'vpa', 'sym' );
         if( any(nfo.sym) || isequal(opt.symflag,2) || isequal(opt.symflag,4) );
             M_cast  = cellfun( @(x) sym(x), opt.M_in, 'UniformOutput',false );
         elseif( any(nfo.vpa) || isequal(opt.symflag,1) || isequal(opt.symflag,3) );
             M_cast  = cellfun( @(x) vpa(x), opt.M_in, 'UniformOutput',false );
         else;
             M_cast = cellfun( @(x) double(x), opt.M_in, 'UniformOutput',false ); end;
         sze = zeros( 1, numel(M_ret) );
         for i = 1:numel( M_ret );
             sze(i) = size( M_ret{i}{1}, 1 );
             for j = 1:opt.J;
                 idx = (sum(sze(1:i-1))+1):sum(sze(1:i));
                 M_ret{i}{j} = M_cast{j}( idx, idx ); end; end;
         B = cast( eye(opt.dim), class(M_ret{1}{1}) );
         return; end;

        
        if( opt.symflag == 0 );
            B_test = B;
            M_test = M;
            epsilon = 1e-12;
        elseif( opt.symflag == 1 );
            for i = 1:numel( M );
                M_test{i} = vpa( M_sym{i} ); end;
            B_test = vpa( B );
            epsilon = vpa( 10^(sym(digits)-3) );
        elseif( opt.symflag == 2 );
            epsilon = sym(0);
            for i = 1:numel( M );
                M_test{i} = sym( M_sym{i} ); end;    
            if( ~all(matrixinfo(M_test,'symstrict')) );
                vprintf( 'Recomputation in sym not possible. Cast of input matrices to sym (not to vpa!) failed.\n', 'cpr','err', 'imp',[0 verbose] );
                opt.symflag = 0; end; 
            B_test = B;
            idx = abs( B_test ) < 5e-12;
            B_test(idx) = 0;
            B_test = sym( B_test );
            if( ~all(matrixinfo( B_test, 'symstrict' )) );
                vprintf( 'Recomputation in sym not possible. Cast of basis to sym (not to vpa!) failed.\n', 'cpr','err', 'imp',[0 verbose] );
                opt.symflag = 0; end; 
        elseif( opt.symflag == 3 );
            for i = 1:numel( M );
                M_test{i} = sym( double(M{i}) ); end;
            B_test = double( B );
            idx = abs( B_test ) < 5e-12;
            B_test(idx) = 0;
            B_test = sym( B_test );
            epsilon = sym( 0 ); end;

        if( opt.symflag ~= 4  );
            sze = cellfun( @(x) size(x{1},1), M_ret );
            blk = arrayfun( @(x) ones(x), sze, 'UniformOutput',false );  % mask where the output matrices must be zero
            blk = blkdiag(blk{:}) | triu( ones(size(M{1},1)) );
            blk = ~blk;
            if( any(blk(:)) );
                sze = [cumsum([1 sze(1:end-1)]); cumsum( sze ) ];  % is needed later
                flag = true;
                M_ret_test = cell( size(M_ret) );
                for i = 1:numel( M_sym );
                    val2 = B_test\M_test{i}*B_test;
                    val = val2.*blk;
                    val = max( abs(val(:)) );
                    if( ~isAlways( val <= 1e-12, 'Unknown','false' ) );
                        warningflag = true;
                        if( opt.verbose  >= 0 );
                            warning( 'invariantsubspace:badscaled', 'Matrices are badly scaled. Results are very inaccurate.' ); end;
                        flag = false;
                        break; 
                    elseif( ~isAlways( val<=epsilon, 'Unknown','false' ) && opt.symflag ~= 0 );
                        flag = false;
                        break;  
                    elseif( opt.symflag == 0 );
                        flag = false;
                    else  % if( symflag ~= 3 );
                        for j = 1:size( sze, 2 );
                            M_ret_test{j}{i} = val2(sze(1,j):sze(2,j),sze(1,j):sze(2,j)); end; end; end;
            elseif( opt.symflag == 0 )
                flag = false;
            else;
                flag = true;
                M_ret_test = {nestedcellfun( @sym, M_test, 'UniformOutput',false )};
                B_test = nestedcellfun( @sym, B_test, 'UniformOutput',false ); end;
            if( flag );
                M_ret = M_ret_test;
                B = B_test; end;  end;
    %end;
end


function [ triang, M_ret, B ] = searchbasis( M, opt );
    vprintf( 'Search for basis: ', 'imp',[2 opt.verbose] )
    [~,M_ret,B] = jointTriangult( M, opt.V0, opt.epsilon, 'verbose',opt.verbose, 'simplify',opt.simplifytime );
    if( ~isequal(B,eye(opt.dim)) );
        vprintf( 'basis found. ', 'imp',[2 opt.verbose] )
        triang = true;
    else
        vprintf( 'no basis found. ', 'imp',[2 opt.verbose] )
        triang = false; end; 
end

function [ triang, M_ret, B ] = searchperm( M, opt );
    vprintf( 'Search for permutation: ', 'imp',[2 opt.verbose] )
    [triang,M_ret,B] = permTriangult( M );
    if( ~triang );
        vprintf('no permutation found. ', 'imp',[2 opt.verbose] )
        B = opt.EYE;
        M_ret = {M};
        triang = false;
    else;
        vprintf( 'permutation found. ', 'imp',[2 opt.verbose] )
        B  = opt.EYE(:,B);
        triang = true; end; 
    B = cast( B, 'like',M{1} );
end


function [ triang, M_ret, B ] = searchdiff( M, opt, symflag );
    try;
        vprintf( 'Search for difference subspace ', 'imp',[2 opt.verbose] );
        if( symflag ); 
            vprintf('(symbolically): ', 'imp',[2 opt.verbose] ); 
        else; 
            vprintf( '(numerically): ', 'imp',[2 opt.verbose] ); end;
        U = constructU( M, 1, 'verbose',opt.verbose-1 );
        if( matrixinfo(M,'real') );
            U = real( U ); end;
        [MU, ~, MR, NN, BASIS] = restrictmatrix( M, U, 'verbose',opt.verbose-2 );
        if( symflag );
            if( max(max([NN{:}])) > 10e-12 ); 
                U = constructU( M, 1, 'sym', 'verbose',opt.verbose-2 );
                [MU, ~, MR, NN, BASIS] = restrictmatrix( M, U, 'verbose',opt.verbose-2 ); end;
            if(max(max([NN{:}])) > 10e-12); 
                U = constructU( M, 1, 'vpa',200 );
                [MU, ~, MR, NN, BASIS] = restrictmatrix( M, U, 'verbose',opt.verbose-2 ); end; end;
        if( max(max([NN{:}]))<10e-12 );
            vprintf( 'difference subspace found. \n', 'imp',[2 opt.verbose] );
            M_ret{1} = MU; 
            M_ret{2} = MR; 
            B = BASIS;
            triang = true;
        else;
            vprintf( 'no difference subspace found. \n', 'imp',[2 opt.verbose] );
            B = opt.EYE;
            M_ret = {M};
            triang = false; end;
    catch;
        vprintf( 'Could not search for difference subspace. \n', 'imp',[2 opt.verbose] );
        B = opt.EYE;
        M_ret = {M};
        triang = false; end;
end
    
function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
