function [ P ] = liftproductt( M, k, exact_k_flag )
% Computes all products of length less or equal k.
% [ P ] = liftproductt( M, k, [flag] ) 
%
% Input:
%   M       cell array of matrices
%   k       integer, maximal length of products
%   flag    default=false, when given only products of length k are returned
%
% Output:
%   P       all products of length less or equal k
%
% Note:
%   The same matrix can appear several times in the returned cell array.
%   Also returns the empty product eye()
%
% E.g.: liftproductt( {2 3 5}, 3 )
%
% Written for JSR-toolbox, Jungers et al,
% Modified by tommsch, 2018

%            2021-01-18, tommsch,   Added option [flag]
%            2021-09-30, tommsch,   Improved option [flag]   
% Changelog:

    if( nargin <= 2 );
        exact_k_flag = 0; end;

    n = length( M );
    m = size( M{1}, 1 );
    P = cell( k+1, 1 );
    dim = size( M{1}, 1 );
    P{1} = eye( dim );
    for i = 1:k
        P{i+1} = liftproductt_recursive( cell(n^i, 1), M, n, eye(m), i-1, 0 ); end;
    if( exact_k_flag == 0 );
        P = vertcat( P{:} ).';
    elseif( exact_k_flag == 1 );
        P = P{end}.';
    else;
        end; % do nothing
     


end

function [ P ] = liftproductt_recursive( P, M, n, prod, k, shift );

    if( k <= 0 );
        for i = 1:n;
            P{shift+i, 1} = prod*M{i}; end;
        return; end;

    for i = 1:n;
        P = liftproductt_recursive( P, M, n, prod*M{i}, k-1, shift+(i-1)*n^k ); end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
