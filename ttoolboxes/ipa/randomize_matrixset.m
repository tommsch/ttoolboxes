function [ M ] = randomize_matrixset( varargin );
% Applies random transformations to matrices
% [ M ] = randomize_matrixset( M, [options] )
%
% Input:
%   M   matrix or cell-array of matrices
%
% Options:
%   See sourcode
%
% Output:
%   M   matrix or cell-array of matrices
%


    [M, prop, opt] = parse_input( varargin{:} );
    [prop] = set_random_properties( prop, opt );
    [M, opt] = randomize_worker( M, prop, opt );

    if( ~opt.cellflag );
        M = M{1}; end;
end

function [ M, prop, opt ] = parse_input( varargin );
    to_empty = @(x, ~, ~, ~, contained) tif( contained, x, [] );
    prop = struct;
    
    [prop.int, varargin]                 = parsem( 'int', varargin, 'postprocess',to_empty );
    [prop.antisymmetric, varargin]       = parsem( 'antisymmetric', varargin, 'postprocess',to_empty );
    [prop.bool, varargin]                = parsem( {'bool','boolean'}, varargin, 'postprocess',to_empty );
    %[prop.continuous, varargin]          = parsem( {'continuous','cont'}, varargin, 'postprocess',to_empty );
    [prop.flip, varargin]                = parsem( {'flip'}, varargin, 'postprocess',to_empty );
    [prop.fliplr, varargin]              = parsem( {'fliplr'}, varargin, 'postprocess',to_empty );
    [prop.flipud, varargin]              = parsem( {'flipud'}, varargin, 'postprocess',to_empty );
    if( prop.flip );
        assert( isempty(prop.fliplr) && isempty(prop.flipud) );
        val = rand;
        prop.flipud = val < 0.5;
        prop.fliplr = val >= 0.5; end;
    [prop.irreducible, varargin]         = parsem( {'irreducible','noinvariantsubspace' }, varargin, 'postprocess',to_empty );
    [prop.invariantsubspace, varargin]    = parsem( {'invariantsubspace','subspace'}, varargin, 'postprocess',to_empty );
    if( ~isempty(prop.irreducible) );
        prop.invariantsubspace = ~prop.irreducible;
    elseif( ~isempty(prop.invariantsubspace) );
        prop.irreducible = ~prop.invariantsubspace; end;
    [prop.noise, varargin]               = parsem( {'noise','perturbate'}, varargin, 'postprocess',to_empty );
    [prop.nonnegative, varargin]         = parsem( {'nonneg','nonnegative'}, varargin, 'postprocess',to_empty );
    [prop.pinv, varargin]                = parsem( {'pinv','invert','inv'}, varargin, 'postprocess',to_empty );
    [prop.pos, varargin]                 = parsem( 'pos', varargin, 'postprocess',to_empty );
    [prop.positive, varargin]            = parsem( {'pos','positive'}, varargin, 'postprocess',to_empty );
    if( prop.positive );
        prop.nonnegative = true; end;
    [prop.poslead, varargin]             = parsem( {'poslead','vandergraft'}, varargin, 'postprocess',to_empty );
    [prop.metzler, varargin]             = parsem( {'metzler','quasipositive','essentiallynonnegative'}, varargin, 'postprocess',to_empty );
    [prop.norm, varargin]                = parsem( 'norm', varargin, 'postprocess',to_empty );
    [prop.rat, varargin]                 = parsem( {'rat','rational'}, varargin, 0, 'postprocess',to_empty );
    [prop.rank, varargin]                = parsem( 'rank', varargin, 'postprocess',to_empty );
    [rankone, varargin]                 = parsem( {'rankone','rank1'}, varargin );
    if( isempty(prop.rank) && rankone == 1 );
        prop.rank = 1; end;
    [prop.rho, varargin]                 = parsem( 'rho', varargin, 'postprocess',to_empty );
    [prop.singular, varargin]            = parsem( {'singular'}, varargin, 'postprocess',to_empty );
    [prop.sparsity, varargin]            = parsem( {'sparsity','sparse'}, varargin, 'postprocess',to_empty );
    [prop.symmetric, varargin]           = parsem( 'symmetric', varargin, 'postprocess',to_empty );
    [prop.transpose, varargin]           = parsem( {'transpose'}, varargin, 'postprocess',to_empty );
    [prop.tril, varargin]                = parsem( {'tril','lowertriangular','triangularlower'}, varargin, 'postprocess',to_empty );
    [prop.triu, varargin]                = parsem( {'triangular','triu','triangularupper','uppertriangular'}, varargin, 'postprocess',to_empty );

    [opt.verbose, varargin]              = parsem( {'verbose','v','verbosity'}, varargin, 'postprocess',@str2numt );
    assert( numel(varargin) >= 1, 'randomize_matrixset:input', 'Could not parse input. Could not find a set of matrices.' );
    if( iscell(varargin{1}) || ...
        isnumeric(varargin{1}) && ismatrix(varargin{1}) ...
      );
        M = varargin{1};
        varargin(1) = []; end;
    [M, opt.cellflag] = tocell( M );
    if( isempty(M) );
        return; end;
    opt.col = size( M{1}, 1 );
    opt.row = size( M{1}, 2 );

    parsem( varargin, 'test' );

end

function [ prop ] = set_random_properties( prop, opt );  %#ok<INUSD>
    opt_empty = all( structfun( @isempty, prop ) );
    if( ~opt_empty );
        return; end;

    names = fieldnames( prop );
    N = randi( [1 5] );
    idx = randperm( numel(names), N );
    for i = idx;
        prop.(names{i}) = 1; end;

    if( ~isempty(prop.sparsity) && ~isequal(prop.sparsity, 0) && ~isequal(prop.sparsity, 1) );
        prop.sparsity = rand; end;

end

function [ M, opt ] = randomize_worker( M, prop, opt );

    if( numel( M ) == 0 );
        return; end;

    if( prop.antisymmetric );
        for j = 1:numel( M );
            dim = min( size(M{j}) );
            opt.row = dim;
            opt.col = dim;
            M{j} = M{j}(1:dim,1:dim);
            M{j} = (M{j} - M{j}'); end; end;  

    if( prop.bool );
        for j = 1:numel( M );
            idx = isfinite( M{j} );
            M{j}(~idx) = 0;
            M{j} = double( logical(M{j}) ); end; end;
    
    if( ~isempty(prop.fliplr) );
        for j = 1:numel( M );
            if( rand < prop.fliplr );
                M{j} = fliplr( M{j} ); end; end; end;
    
    if( ~isempty(prop.flipud) );
        for j = 1:numel( M );
            if( rand < prop.flipud );
                M{j} = flipud( M{j} ); end; end; end;

    if( prop.int );
        all_values = cellfun( @(x) x(:)', M, 'UniformOutput',false );
        all_values = [all_values{:}];
        num_values = numel( unique( all_values ) );
        delta = 1;
        while( numel( unique( round(delta*all_values) ) ) ~= num_values );
            delta = delta * randi( [1 9] ); end;
        if( delta ~= 1 );
            for j = 1:numel( M );
                M{j} = round( delta*M{j} ); end; end; end;

    if( ~isempty(prop.invariantsubspace) && ~isequal(prop.invariantsubspace, false) );
        M = randomize_worker_invariantsubspace( M, prop, opt ); end;
    
    % if( ~isempty(prop.col) || ~isempty(prop.row) );
    %     if( isempty(prop.row) );
    %         r = size( M{1}, 1 ); 
    %     else;
    %         r = min( size(M{1}, 1), prop.row ); end;
    %     if( isempty(prop.col) );
    %         c = size( M{1}, 2 );
    %     else;
    %         c = min( size(M{1}, 2), prop.col ); end;
    %     for j = 1:J;
    %         M{j} = M{j}(1:r,1:c); end; end;

    if( prop.metzler );
        for j = 1:numel( M );
            dim = min( size(M{j}) );
            opt.row = dim;
            opt.col = dim;
            M{j} = M{j}(1:dim,1:dim);
            d = diag( diag(M{j}) );
            M{j} = abs( M{j} - d ) + d; end; end;
    
    if( prop.noise );
        for j = 1:numel( M );
            M{j} = M{j} +  norm( M{j}, 1 )*prop.noise*randn( size(M{j}) ); end; end;

    if( prop.norm ); 
        for j = 1:numel( M );
            M{j} = M{j}/norm( M{j} ); end; end;

    if( prop.nonnegative );
        for j = 1:numel( M );
            M{j} = abs( M{j} ); end; end;

    if( prop.pos ); 
        for j = 1:J;
            M{j} = abs( M{j} ); end; end;

    if( prop.poslead );
        M = randomize_worker_poslead( M, prop, opt ); end;

    if( prop.rat );
        M = randomize_worker_rat( M, prop, opt ); end;

    if( ~isempty(prop.pinv) && issquare(M{1}) );
        for j = 1:numel( M );
            if( rand < prop.pinv );
                try;
                    M{j} = pinv( M{j} ); 
                catch me;  %#ok<NASGU>
                    end; end; end; end;

    if( ~isempty(prop.rank) && prop.rank < min(size(M{1})) );
        for j = 1:numel( M );
            [U, S, V] = svd( M{j} );
            M{j} = zeros( size(M{j}) );
            for r = 1:prop.rank;
                M{j} = M{j} + U(:,r) * S(r,r) * (V(:,r)'); end; end; end;    

    if( prop.rho ); 
        for j = 1:numel( M );
            M{j} = M{j}/rhot( M{j} ); end; end;

    if( prop.singular );
        for j = 1:numel( M );
            [U, S, V] = svd( M{j} );
            [~, idx] = min( diag(S) );
            S(idx,idx) = 0;
            M{j} = U*S*V'; end; end;

    if( prop.sparsity );
        n = opt.col * opt.row;
        for j = 1:numel( M );
            idx = randperm( n, round(n*prop.sparsity) );
            M{j}(idx) = 0; end; end;
    
    if( prop.symmetric );
        for j = 1:numel( M );
            dim = min( size(M{j}) );
            opt.row = dim;
            opt.col = dim;
            M{j} = M{j}(1:dim,1:dim);
            M{j} = (M{j} + M{j}'); end; end;

    if( ~isempty(prop.transpose) && issquare(M{1}) );
        for j = 1:numel( M );
            if( rand < prop.transpose );
                M{j} = M{j}.'; end; end; end;
    
    if( ~isempty(prop.tril) );
        for j = 1:numel( M );
            M{j} = tril( M{j}, prop.tril ); end; end;

    if( ~isempty(prop.triu) );
        for j = 1:numel( M );
            M{j} = triu( M{j}, prop.triu ); end; end;

    % if( ~isequal(prop.continuous, 0)  );
    %     [ M, G ] = make_continuous_set( M, G, prop ); end;

end

%% post process functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ M ] = randomize_worker_invariantsubspace( M, prop, opt )
    J = numel( M );
    if( isequal(prop.invariantsubspace, 1) );
        lengths = randsum( size(M{1}, 1) );
    elseif( isscalar(prop.invariantsubspace) );
        lengths = randsum( size(M{1}, 1), prop.invariantsubspace );
    elseif( isvector(prop.invariantsubspace) );
        lengths = prop.invariantsubspace; end;
    clengths = cumsum( lengths );
    if( clengths(end) == size(M{1}, 2) );
        mask = ones( size(M{1}, 2) );
        for l = 1:numel( lengths ) - 1;
            mask((clengths(l) + 1):clengths(l+1),1:clengths(l)) = 0; end;
        Mt = cell( 1, J );
        while( true );
            B = gallery_matrix( 'dim', size(M{1}, 1), 'randn' );
            masked_sum = zeros( size(M{1}) );
            for j = 1:J;
                masked_sum = masked_sum + abs( M{j} );
                Mt{j} = B\(mask.*M{j})*B; end;
            masked_sum = masked_sum.*mask;
            norm_test = 0;
            for j = 1:J;
                norm_test = norm_test + norm( (B*Mt{j}/B).*(~mask), 1 ); end;
            bound = 1e-12 * max( cellfun( @(x) norm(x, 1), Mt ) );
            if( ~isfinite(norm_test) );
                break;
            elseif( norm_test ) <= bound;
                M = Mt;
                break; end; end;
        vprintf( 'Error: %g, Sizes of subspaces: %v\n', norm_test, lengths, 'imp',[1 opt.verbose] );
        if( opt.verbose >= 2 );
            imagesc( masked_sum ); end; end;
end

function [ M ] = randomize_worker_poslead( M, prop, opt );  %#ok<INUSD>
    J = numel( M );
    for j = 1:J;
        ev = leadingeigenvalue( M{j} );
        assert( all( isreal(ev) ), 'randomize_matrices:poslead', 'Could not apply option ''poslead''. At least one leading eigenvalue is complex.' );
        flag = isAlways( all(ev < 0 ) );
        if( all(flag) );
            M{j} = -M{j};
        elseif( any(flag) );
            error( 'randomize_matrices:poslead', 'Could not apply option ''poslead''. Leading eigenvalues have mixed signs.' );
        else;
            end; end; % do nothing
end

function [ M ] = randomize_worker_rat( M, prop, opt );  %#ok<INUSD>
    J = numel( M );
    for j = 1:J;
        M{j} = ratt( M{j}, prop.rat );
        end; % do nothing
end

function [ Tout, G ] = make_continuous_set( T, G, opt );  %#ok<DEFNU>
    assert( isempty(G), 'gallery_matrixset:graph', 'If a continuous set shall be generated, no graph must be generated.' );
    M = [];
    m = [];
    if( iscell(opt.continuous) && ischar(opt.continuous{1}) );
        args = opt.continuous;
        [ M, args ] = parsem( {'M','max','maxtime'}, args, [] );
        [ m, args ] = parsem( {'m','min','mintime','dwelltime','dwell'}, args, [] );
        [ N, args ] = parsem( {'N'}, args, 10 );
        [ selfflag, args ] = parsem( 'self', args, 1 );
        parsem( args, 'test' );
    elseif( isequal(opt.continuous, 1 ) );
        selfflag = 1;
        N = 10;
    else;
        selfflag = 1;
        N = opt.continuous; end;
    
    if( isempty(M) && isempty(m) );
        m = 1;
        M = 2;
    elseif( isempty(M) );
        M = 2*m;
    elseif( isempty(m) );
        m = M/2; end;
    
    if( selfflag );
        Tau = linspace( m, M, N-1 );
        assert( N >= 2, 'gallery_matrixset:graph', '''N'' must be at least 2 when ''self'' option is given.' );
    else;
        assert( N >= 1, 'gallery_matrixset:graph', '''N'' must be at least 1.' );
        Tau = linspace( m, M, N ); end;
    
    J = numel( T );
    
    Tout = {};
    for j = 1:J
        for tau = Tau;
            Tout{end+1} = expm( tau * T{j} ); end;  %#ok<AGROW>
        if( selfflag );
            Tout{end+1} = Tout{end}; end; end;  %#ok<AGROW>
    
    if( selfflag );
        G = graph_cycle_unbd( repmat( N, [1 J] ), numel(Tout) );
    else;
        G = graph_cycle( repmat( N, [1 J] ), numel(Tout) ); end;
    
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
