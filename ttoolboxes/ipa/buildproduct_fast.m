function [ M ] = buildproduct_fast( A, oo, M );
% [ M ] = buildproduct_fast( A, oo, [v] )
% Constructs the product of matrices in the cell A, corresponding to the sequence in prod.
% M = A{prod(end)} * A{prod(end-1)} * ... * A{prod(1)}
%
% Input:
%   A       cell array of matrices
%   oo      ordering in which to multiply them
%   v       optional. If given, the function computes A*v, which is faster and more accurate in general
%
% Output:
%   M       the product
%
% Info:
%   if prod(i) = 0, A{oo(i)} is replaced by the identity
%
% E.g.: buildproduct_fast( {2 4 5}, [1 0 3] )
%
% See also: buildproduct, buildsum_fast
% 
% Written by: Jungers, Taken from: JSR-Toolbox
% Changed by: tommsch, 2020

    if( nargin == 2 );
        M = eye( size(A{1}, 1) ); end;
    
    if( issym(M) );  %#ok<IFBDUP>
        for t = 1:length( oo );
            if( oo(t) > 0 );
                M = A{oo(t)} * M; 
                %M = simplify( M, 1 ); 
                end; end;
    else;
        for t = 1:length( oo );
            if( oo(t) > 0 );
                M = A{oo(t)} * M; end; end; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
