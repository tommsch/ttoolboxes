function I = intersectinterval( varargin );
% [ K ] = intersectinterval2( I_1, I_2, ..., I_n, [options] );
% Intersects intervals.
%
% Input: 
%   I_i,I2      matrix of 2-element row vectors, the intervals to be intersected
%               each row is one set of intervals
%
% Options:
%   'failsafe',val      default=0, If intersection would be empty, val determines how to handle this case.
%                           0,'default'     no failsafe
%                           'min'           I(,1)=min, I(,2)=min
%                           'max'           I(,1)=max, I(,2)=max
%                           'minmax'        I(,1)=min, I(,2)=max
%                           'maxmin'        I(,1)=max(min), I(,2)=min(max)
%                           'nan'           I = [nan nan]
%                           'inf'           I = [inf -inf]
%
% Output:
%   I       matrix of 2-element row-vector, the intersections of I1 \cap ... \cap In
%
% Note: Input must not contain NaN.
%
% Eg: intersectinterval( [3 5], [1 4], [-inf 4] )
%
% See also: blockjsr
%
% Written by: tommsch, 2018

% Changelog:    tommsch,    2025-01-09,     Added failsafe options 'nan','inf','min','max','default'

    [failsafe, varargin] = parsem( 'failsafe', varargin, [] );
    if( isempty( failsafe) && ...
        numel(varargin) >= 1 && ...
        (ischar( varargin{end} ) || isstring( varargin{end} ) ) ...
      );
        failsafe = varargin{end};
        varargin(end) = []; end;

    if( size(varargin, 2) == 0 ); 
        I = [-inf inf]; 
        return; end;

    if( isempty(failsafe) );
        failsafe = 'default';
        end;

    MIN = min( varargin{1}, [], 2 );
    MAX = max( varargin{1}, [], 2 );
    for i = 2:numel( varargin );
        MIN = max( MIN, min(varargin{i}, [], 2) );
        MAX = min( MAX, max(varargin{i}, [], 2) ); end;

    I = zeros( size(varargin{1}) );
    for r = 1:size( I, 1 );
        if( MIN(r,:) > MAX(r,:) );
            switch failsafe;
                case {'none','def','default'};
                    I(r,:) = [MIN(r,:) MAX(r,:)];
                case 'min';
                    I(r,:) = [MIN(r,:) MIN(r,:)];
                case 'max';
                    I(r,:) = [MAX(r,:) MAX(r,:)];
                case 'maxmin'; 
                    I(r,1) = MAX; 
                    I(r,2) = MIN;
                case 'minmax'; 
                    I(r,1) = min( cellfun(@min, varargin) );
                    I(r,2) = max( cellfun(@max, varargin) );
                case {'inf'};
                    I(r,:) = [inf -inf];
                case {'nan'};
                    I(r,:) = [nan nan];
                otherwise; 
                    error( 'intersectinterval:option', 'Unkown failsafe option' ); end;
        else;
            I(r,:) = [MIN(r,:) MAX(r,:)]; end; end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
