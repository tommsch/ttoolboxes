function [ vec ] = sum_of_primes( N, include_last );
% (experimental) gives a list of prime numbers (including 1), which sum up to p
% [ vec ] = sum_of_primes( N, include_last );
%
% Input:
%   N               the number to sum up to, must be less than 1e8
%   include_last    default=false,  If true, then N itself is allowed as a summand
%
% Output:
%   vec             vector of integers, summing up to N
%
% Written by: tommsch, 2024-10-30

% Changelog:    tommsch,    2024-10-30,     New function
%               tommsch,    2024-11-06,     Better implementation

    assert( N <= 1e8, 'sum_of_primes:input', 'The current implementation is too slow for inputs larger than 1e8' );  
    if( nargin <= 1 || isempty(include_last) );
        include_last = false; end;
    if( N <= 1 );
        vec = N;
    elseif( N == 2 );
        vec = tif( include_last, 2, [1 1] );
    elseif( include_last && isprime(N) );
        vec = N;
    elseif( mod( N, 2 ) == 0 );
        vec = sum_of_primes_even( N, include_last );
    elseif( N >= 2 && isprime(N - 2) );
        vec = [2 N-2];
    else;
        vec = sum_of_primes_odd( N, include_last ); end;
    vec = sort( vec );
    
end

function [ vec ] = sum_of_primes_even( N, include_last );
    ps = tif( include_last, primes(N), primes(N-1) );
    [~, idx] = sort( abs( ps - ps(end)/2 ) );
    ps = [ps(idx) 1];
    for p = ps;
        if( isprime( N-p ) );
            vec = [N-p p];
            return; end; end
    assert( N > 4e18, 'sum_of_primes:goldbach', 'Goldbachs conjecture tells us there must be a solution.' );
    vec = sum_of_primes_fallback( N, include_last );
end

function [ vec ] = sum_of_primes_odd( N, include_last );
    ps = tif( include_last, primes(N), primes(N-1) );
    [~, idx] = sort( abs( ps - ps(end)/3 ) );
    ps = [ps(idx) 1];
    vec = [ps(1) sum_of_primes(N-ps(1), true)];
end

function [ vec ] = sum_of_primes_fallback( N, include_last );
    po = N;
    vec = [];
    if( include_last );
        pmax = [1 primes( N )];
    else;
        pmax = [1 1 primes( N-1 )]; end;  % The two ones at the beginning are needed for the case p==2
    while( sum(vec) < po && ~isempty(pmax) )
        idx = find( N >= pmax );
        idx = idx(end);
        vec(end+1) = pmax(idx);  %#ok<AGROW>
        N = N - pmax(idx);
        pmax(idx:end) = [];
    end
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

