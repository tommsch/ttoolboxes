function [ x, fs ] = gallery_signal( varargin );
    [opt, varargin] = parse_input( varargin{:} );
    [x, fs] = getsignal( opt, varargin{:} );
    
    [x, fs] = postprocess( x, fs, opt );
end

function [ opt, varargin ] = parse_input( varargin );
    
    [opt.verbose, varargin] =           parsem( {'verbse','v'}, varargin, 1 );
    
    [opt.length, varargin] =            parsem( {'length','len'}, varargin, [] );
    [opt.clamp, varargin] =             parsem( {'clamp'}, varargin, [] );
    [opt.scale, varargin] =             parsem( {'scale'}, varargin, [] );
    [opt.revert, varargin] =            parsem( {'revert'}, varargin, [] );
    [opt.fs, varargin] =                parsem( {'fs','samplingrate','sr', 'sampling'}, varargin, [] );
    
    [opt.plot, varargin] =              parsem( {'plot'}, varargin );
    [opt.play, varargin] =              parsem( {'play'}, varargin );
    [opt.what, varargin] =              parsem( 'what', varargin, [] );
    
    if( ~isempty(opt.what) );
    elseif( numel(varargin) >= 1 );
        opt.what = varargin{1}; 
        varargin(1) = []; end;
    
    if( isempty(opt.clamp) && isempty(opt.scale) );
        opt.scale = 1; end;
    
    if( isempty(what) );
        if( isempty(opt.revert) );
            opt.revert = randi( [0 1] ); end;
    else;
        if( isempty(opt.revert) );
            opt.revert = 0; end;        
        end;        
    
end

function [ x, fs ] = postprocess( x, fs, opt );

    if( iscolumn(x) );
        x = x.'; end;

    if( ~isempty(opt.clamp) && ~isequal(opt.clamp, 0) );
        x = clamp( x, -opt.clamp, opt.clamp ); end;
    
    if( ~isempty(opt.scale) && ~isequal(opt.scale, 0) );
        mag = max( abs(x(:)) );
        if( mag > 0 );
            x = x./mag*opt.scale; end; end;
    
    if( ~isempty(opt.fs) && ~isequal(opt.fs, fs) );
        [P, Q] = rat(opt.fs/fs);
        x = resample( x, P, Q );
    elseif( isempty(opt.fs) );
        opt.fs = fs;
    end;
    
    if( ~isempty(opt.length) );
        len = opt.length*opt.fs;
        if( numel(x) < len );
            warning( 'gallery_signal:length', 'Signal is too short to be cropped. Signal will be repeated' );
            x = repmat( x, [1 ceil(len/numel(x))] ); end;
        x = x(1:len); end;
    

    
    if( opt.plot );
        plot( x ); end;
    if( opt.play );
        soundsc( x, opt.fs ); end;
    
end

%%

function [ x, fs ] = getsignal( opt, varargin );

    randflag = isempty( opt.what ) * randi( [1 2] );
    x = [];
    fs = [];
    if( randflag == 0 && isempty(x) || randflag == 1 );
        [x, fs] = getsignal_basic( opt, varargin{:} ); end;
    if( randflag == 0 && isempty(x) || randflag == 2 );
        [x, fs] = getsignal_mat( opt, varargin{:} ); end;    
end

function [ x, fs ] = getsignal_basic( opt, varargin );

    if( isempty(opt.fs) );
        opt.fs = 44100;
        fs = opt.fs; 
    else;
        fs = opt.fs; end;
    
    if( check(opt.what, {'random'}) );
        opt.what = []; end;
    if( isempty(opt.what) ); 
        opt.what = randi( [0 5] ); end;
    
    x = [];
    if( false );
    elseif( check(opt.what, {0,'zero'}) );
        x = signal_zero( opt, varargin{:} );
    elseif( check(opt.what, {1,'sin'}) );
        x = signal_sin( opt, varargin{:} );
    elseif( check(opt.what, {2,'sweep'}) );
        x = signal_sweep( opt, varargin{:} );    
    elseif( check(opt.what, {3,'click'}) );
        x = signal_click( opt, varargin{:} );
    elseif( check(opt.what, {4,'fm'}) );
        x = signal_fm( opt, varargin{:} );
    elseif( check(opt.what, {5,'fm2'}) );
        x = signal_fm2( opt, varargin{:} );        
        
    end;
end

function [ x, fs ] = getsignal_mat( opt, varargin );

    if( check(opt.what, {'mat'}) );
        opt.what = []; end;
    if( isempty(opt.what) ); 
        opt.what = randi( [1 7] ); end;
    
    fs = [];
    x = [];
    if( false );
    elseif( check(opt.what, {1,'chirp'}) );
        y = load( 'chirp' );
        x = y.y;
        fs = y.Fs;
    elseif( check(opt.what, {2,'gong'}) );
        y = load( 'gong' );
        x = y.y;
        fs = y.Fs;
    elseif( check(opt.what, {3,'handel','halleluja'}) );
        y = load( 'handel' );
        x = y.y;
        fs = y.Fs;
    elseif( check(opt.what, {4,'laugh','laughter'}) );
        y = load( 'laughter' );
        x = y.y;
        fs = y.Fs;
    elseif( check(opt.what, {5,'mtlb','matlab'}) );
        y = load( 'mtlb' );
        x = y.mtlb;
        fs = y.Fs;
    elseif( check(opt.what, {6,'splat'}) );
        y = load( 'splat' );
        x = y.y;
        fs = y.Fs;
    elseif( check(opt.what, {7,'train'}) );
        y = load( 'train' );
        x = y.y;
        fs = y.Fs;
    end;

end

%%

function [ x ] = signal_zero( opt );
    len = opt.length;
    if( isempty(len) );
        len = 1; end;
    x = zeros( 1, len*opt.fs );
end

function [ x ] = signal_sin( opt, f );
    if( nargin <= 1 );
        f = 2.^randbd( log2([20 opt.fs/2]) ); end;
    len = opt.length;
    if( isempty(len) );
        len = 1; end;
    x = sin( 2*pi*f*[0:1/opt.fs:len] );  %#ok<NBRAK>
    x = x(1:len*opt.fs);
end

function [ x ] = signal_sweep( opt, f, method, phi );

    len = opt.length;
    if( isempty(len) );
        len = 1; end;
    
    if( nargin <= 1 );
        f = 2.^randbd( log2([20 opt.fs/2]), [1 2] ); end;
    if( nargin <= 2 );
        method = randi(3); end;
    
    switch method
        case {1,'lin','linear'};       method = 'linear';
        case {2,'quad','quadratic'};   method = 'quadratic';
        case {3, 'log','logarithmic'}; method = 'logarithmic';
        otherwise;
            error( 'gallery_signal:sweep', 'Wrong value for ''method''.' ); end;
        
    if( nargin <= 3 );
        phi = randi( [0 360] ); end;

    if( nargin <= 4 );
        shape = randi(2); end;

    if( ~isequal(method, 'quadratic') );
        shape = {};
    elseif( isequal(shape, 1) );
        shape = {'convex'};
    else;
        shape = {'concave'}; end;

    x = chirp( [0:1/opt.fs:len], f(1), 1, f(2), method, phi, shape{:} );  %#ok<NBRAK>
    x = x(1:len*opt.fs);
    if( opt.verbose >= 1 );
        fprintf( '  f = [%i, %i]\n', f(1), f(2) );
        fprintf( '  shape = %s\n', shape{:} );
        fprintf( '  method = %s\n', method );
    end;
end

function [ x ] = signal_click( opt, varargin );
    train = parsem( 'train', varargin, 0 );
    len = opt.length;
    if( isempty(len) );
        len = 1; end;
    x = zeros( 1, len*opt.fs );
    if( ~isempty(train) && train > 1);
        idx = randperm( numel(x), train );
        x(idx) = 1;
    elseif( isempty(train) );
        train = randi( numel(x) );
        idx = randperm( numel(x), train );
        x(idx) = 1;
    elseif( ~isempty(train) && train > 0 && train<1 );
        d = round( 1/train );
        x(1:d:end) = 1;
    else;
        x(1) = 1; end;
end

function [ x ] = signal_fm( opt, varargin );
    len = opt.length;
    if( isempty(len) );
        len = 1; end;
    if( numel(varargin) == 0 );
        f = [2.^randbd( log2([20 opt.fs/2]) ) 
             2.^randbd( log2([20 opt.fs/2]) )];
    else;
        f = [varargin{:}]; end;
    
    t = 0:1/opt.fs:len;
    t(end) = [];
    x = sin( f(1).*t );
    for i = 2:numel( f );
        x = sin( f(i).*t + x ); end;
        
end

function [ x ] = signal_fm2( opt, varargin );
    len = opt.length;
    if( isempty(len) );
        len = 1; end;
    if( numel(varargin) == 0 );
        f = [2.^randbd( log2([1 100]) ) 
             2.^randbd( log2([100 opt.fs/2]) )];
    else;
        f = [varargin{:}]; end;
    
    t = 0:1/opt.fs:len;
    t(end) = [];
    x = sin( f(1).*t );
    for i = 2:numel( f );
        x = sin( f(i).*x ); end;
        
end

%% helper

function [ ret ] = randbd( ab, varargin );
    ret = rand( varargin{:} );
    ret = ret.*( ab(2) - ab(1) ) + ab(1);
end

function [ ret ] = check( what, names, opt )
    if( nargin == 2 );
        verbose = 1;
    else;
        verbose = opt.verbose; end;
    if( isempty(what) );
        ret = false;
        return; end;
    switch lower( what );
        case names;
            if( isnumeric(what) && verbose >= 1 );
                for i = 1:numel( names )
                    if( ischar(names{i}) || isstring(names{i}) );
                        fprintf( 'Choice: %s\n', names{i} ); 
                        break; end; end; end;
            ret = true;
        otherwise;
            ret = false; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
