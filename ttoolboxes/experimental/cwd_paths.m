function [ p ] = cwd_paths();
% function which has paths to folders where cwd shall loook for results
    p = {
        './kone/';
        './perron/';
        './signal/';
        './filter/';
        };
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
