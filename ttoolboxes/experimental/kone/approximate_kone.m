function [ K, opt ] = approximate_kone( varargin );
% constructs an approximation of a common minimal invariant kone
% [ K ] = approximate_kone( M, [v0], varargin );
%
% Input:
%   M       set of matrices
%   v0      matrix of column vectors, set of starting vectors to try to start from
%
% Options:
%   'iter_max',val              Algorithm stops, after iter_max iterations
%   'time_max',value            Algorithm stops, when it takes longer than time_max, this is an approximate bound
%   'num_vertex_max',value      Algorithm stops, when K has more than num_vertex_max vertices
%   'norm_diff_min',value       Algorithm stops, when newly added vertices have norm less than norm_diff_min
%   'verbose', value            Verbos level
%   'dual'                      If given, algorithm constructs the dual kone
%   'override'                  If given, the no checks during the computation are made, whether the constructed stuff is sensible or not. The user has to check the result thus.
%
% Output:
%   K                           matrix of column vectors, the kone
%                               is empty if matrices do not construct a kone
%                               if K is non-empty, this does not mean that the matrices have a common invariant kone
%   opt                         some statistical data about the algorithm
%
% Notes:
%   This functions always computes K numerically, even if the input is symbolically.
%   Though, the output K has the same type as M{1} always 
%
% Written by: tommsch, 2024-10-08

%               tommsch,    2024-10-08,     Added new function
% Changelog:    tommsch,    2024-10-28,     Added support for 1d case

    [ M, v0, opt ] = parse_input( varargin{:} );

    if( ~opt.override && isempty(v0) );
        nfo = matrixinfo( M, 'vandergraft' );
        if( ~all(nfo) );
            K = [];
            return; end; end;
    
    K = starting_kone( M, v0, opt );
    if( ~opt.override && ~isreal(K) );
        vprintf( 'Computed leading eigenvector is complex. I have to quit.\n', 'imp',[1 opt.verbose] );
        K = [];
        return; end;

    [K, iter] = compute_kone( M, K, opt );
    if( ~opt.override && ...
        isempty(K) ...
      );
        return; end;

    if( ~opt.override && ...
        rank(K) < size(K, 1) ...
      );
        vprintf( 'Rank of vertices of computed kone is less than dimension. I have to quit.\n', 'imp', [1 opt.verbose] );
        return;  end;

    K = tidy_up_kone( K, iter, opt );
    
    if( ~strcmp(opt.class, 'double') );
        K = anycast( K, opt.class ); end;

end

function [ M, v0, opt ] = parse_input( varargin );
    opt.iter = 0;
    [opt.iter_max, varargin] =      parsem( {'iter_max','max_iter','max_round','max_rounds','max_depth','rounds'}, varargin, 15 );
    opt.time = 0;
    [opt.time_max, varargin] =      parsem( {'time_max','max_time','time'}, varargin, 2 );
    opt.num_vertex = 0;
    [opt.num_vertex_max, varargin] = parsem( {'num_vertex_max','vertex_max','vertex'}, varargin, [] );
    opt.norm_diff = inf;
    [opt.norm_diff_min, varargin] = parsem( {'norm_diff_min','norm_diff','norm'}, varargin, 1.01 );
    [opt.verbose, varargin] =       parsem( {'verbose','v'}, varargin, 1 );
    [opt.dual, varargin] =          parsem( {'dual','dual_kone'}, varargin );
    [opt.override, varargin] =      parsem( {'override','force'}, varargin );
    [opt.fuzzy, varargin] =         parsem( {'fuzzy'}, varargin );
    [opt.random, varargin] =        parsem( {'random','rand'}, varargin );

    M = varargin{1};
    varargin(1) = [];
    M = tocell( M );

    v0 = [];
    if( opt.random );
        assert( numel(varargin) == 0, 'approximate_kone:input', 'Bad input given.' );
        v0 = randn( size(M{1}, 1), 1 );
        vprintf( 'Starting vector: %v\n', v0 ); end;
    if( numel(varargin) >= 1 );
        if( iscell( varargin{1} ) && all( cellfun(@isvector, varargin{1}) ) || ...
            isnumeric( varargin{1} ) && ismatrix( varargin{1} ) ...
          );
            v0 = varargin{1};
            varargin(1) = []; end; end;

    parsem( varargin, 'test' );

    dim = size( M{1}, 1 );

    if( isempty(opt.num_vertex_max) );
        opt.num_vertex_max = dim*1000; end;

    if( opt.dual );
        for j = 1:numel( M );
            M{j} = M{j}'; end; end;

    opt.class = class( M{1} );
    if( ~strcmp(opt.class, 'double') );
        for j = 1:numel( M );
            M{j} = double( M{j} );
            r = rho( M{j} );
            if( r > 1e-9 );
                M{j} = M{j}/r;
            else;
                n = norm( M{j} );
                if( n > 1e-9 );
                    M{j} = M{j}/n; end; end; end; end;

end

function [ K ] = starting_kone( M, v0, opt );

    r = rho( M );
    [r_max, r_idx] = max( r );
    if( r_max < 1e-12 );
        if( opt.verbose >= 0 );
            warning( 'approximate_kone:numerical', 'The matrices seem to have zero spectral radius. The result is most likely wrong.' ); end; end;

    if( isempty(v0) );
        v0 = leadingeigenvector( M{r_idx}, 'nonnegativeeigenvalue', 'normalization','kone' );
        if( isreal(M{r_idx}) );
            v0{1} = real( v0{1} ); end;
        [v0_1, ~, err] = leadingeigenvector_one( M{r_idx}, v0{1}, 'verbose',opt.verbose-1 );
        if( err < 1e-12 );
            v0{1} = v0_1; end; 

        K = v0{1};  % we do this, to make sure that v0 is not in K, otherwise v0 would get mapped onto itself in each iteration
        if( numel(M) >= 2 );
            if( r_idx == 1 );
                K_ = M{2}*K; 
            else;
                K_ = M{1}*K; end;
            if( norm(K_, 1) > 1e-12 );
                K = K_; end; end;   
    else;
        K = v0; end;

    if( opt.fuzzy );
        K = K + opt.fuzzy*randn( size( K ) ); end;

end

function [ K, iter ] = compute_kone( M, K, opt );
    switch size( M{1}, 1 );
        case 0;     error( 'approximate_kone:zerod', 'computation of 0-dimensional kone is not implemented yet.' );
        case 1;     [K, iter] = compute_kone_nd( M, K, opt );
        case 2;     [K, iter] = compute_kone_2d( M, K, opt );
        otherwise;  [K, iter] = compute_kone_nd( M, K, opt ); end
end

function [ K, iter ] = compute_kone_2d( M, K, opt );

    for j = 1:numel( M );
        M{j} = prime_powers( M{j}, 20 );
        for k = 1:numel( M{j} );
            rk = rho( M{j}{k} );
            if( rk > 1e-12 );
                M{j}{k} = M{j}{k}/rk; end; end; end;
    M = [M{:}];
    M = uniquecell( M );
    for iter = 1:opt.iter_max;
        for j = 1:numel( M );
            if( opt.verbose >= 3 );
                clf;
                hold on;
                axis( [-1.1 1.1 -1.1 1.1] );
                axis equal; end;
            
            K = [K M{j}*K];  %#ok<AGROW>
            center = center_kone( K, 'verbose',opt.verbose );
            if( opt.verbose >= 3 );
                plotm( K, 'center',center, 'Kone', 'hull', 'k', 'linewidth',2 );
                plotm( normalizematrix( K ), 'c.' ); end;
            
            
            if( ~opt.override && ~iskone(K) );
                vprintf( 'Computed vertices do not construct a kone after iteration: %i\n  I quit.\n', (iter-1)*(j-1) + (j-1) + 1, 'imp',[1 opt.verbose] );
                K = [];
                return; end;
            
            %nrm = sum( K.^2, 1 ).^2;
            %K = K./nrm;
            if( opt.verbose >= 3 );
                plotm( center, 'arrow', 1.1, 'k', 'linewidth',2 ); end;

            R = rotationmatrix( -atan2( center(2), center(1) ) );
            Ks = R*K;
            if( opt.verbose >= 3 );
                plotm( Ks, 'center',center, 'Kone', 'hull', 'k:' ); end;
            al = atan2( Ks(2,:), Ks(1,:) );
            [~, idx_min] = min( al );
            [~, idx_max] = max( al );
            K = K(:,[idx_min idx_max]);
            if( opt.verbose >= 3 );
                plotm( K, 'center',center, 'Kone', 'hull', 'r', 'linewidth',1 ); end;
        end; end;
end

function [ K, iter ] = compute_kone_nd( M, K, opt );
    Kj = cell( size(M) );
    time_start = tic;
    iter = 0;
    while( true );
        
        if( opt.verbose >= 2 );
            clf; hold on;
            %axis( [-1.1 1.1 -1.1 1.1 -1.1 1.1] );
            axis equal
            end;
        K_old = K;
        while( true )
            iter = iter + 1;
            for j = 1:numel( M );
                Kj{j} = M{j}*K;
                if( opt.verbose >= 3 );
                    
                    %plotm( Kj{j}, 'center',center, 'Kone', 'hull' );
                    %plotm( normalizematrix( Kj{j} ), 'center',center, 'Kone', 'c.', .1 );
                    %axis( [-1.1 1.1 -1.1 1.1 -1.1 1.1] );
                    drawnow; end; end;
            K = [K normalizematrix( [Kj{:}], 'colnorm',inf )];  %#ok<AGROW>
            K = unique( K.', 'rows' ).';
            center = center_kone( K, 'v',opt.verbose );
            plotm( K, 'center',center, 'Kone', 'hull', 'imp',[3 opt.verbose] );
            if( size(K, 2) > 100 || iter > opt.iter_max );
                break; end; end;
    if( ~opt.override && ...
        rank(K) == size( K, 1 ) && ~iskone(K) ...
      );
        K = [];
        return; end;
    if( ~opt.override );
        center = center_kone( K, 'verbose',0 );
        nrm = polytopenorm( K, K_old, 'Kone', 'output','ub', 'verbose',0 );
        idx_in = nrm < 1;
        if( ~isempty(idx_in) && all(idx_in) );
            idx_in(1) = false; end;
        K(:,idx_in) = [];
        opt.norm_diff = max( nrm );
        end;
    plotm( K, 'center',center, 'Kone', 'hull', 'k', 'imp',[2 opt.verbose] );
    opt.time = toc( time_start );
    opt.iter = iter;
    opt.num_vertex = size( K, 2 );
    if( opt.iter > opt.iter_max || ...
        opt.time > opt.time_max*.8 || ...
        opt.norm_diff < opt.norm_diff_min || ...
        opt.num_vertex > opt.num_vertex_max ...
      );
        break; end; end;
end

function [ K ] = tidy_up_kone( K, iter, opt );
    if( opt.override );
        return; end;
    center = center_kone( K, 'verbose',0 );
    nrm_K = polytopenorm( K, K, center, 'kone', 'output','ub', 'verbose',0 );
    idx_in = nrm_K < 1;
    if( ~isempty(idx_in) && all(idx_in) );
        idx_in(1) = false; end;
    K(:,idx_in) = [];
    opt.iter = iter;

    idx_zero = ~any( K, 1 );
    K(:,idx_zero) = [];
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
