function [ nullspace ] = nullspace_kone( center, method )
% computes a basis for the orthogonal complement for a given vector
% [ nullspace ] = nullspace_kone( center, [method] )
%
% Input:
%   center      dim x 1 vector
%   method      string, default = 'null', Defines the method used to compute the nullspace
%                   'null': Use Matlabs `null` function
%                   'gs': Use Gram Schmidt method
%
% Output:
%   nullspace   dim x (dim - 1) matrix of column vectors, the basis vectors for the space orthogonal to center
%
% See also: center_kone, Et_kone, scale_kone
%
% Written by: tommsch, 2024-04

    if( nargin <= 1 || isempty(method) );
        method = 'null'; end;
    if( any(~isfinite(center)) );
        nullspace = nan( size(center, 1), size(center, 1) - 1 );
        return; end;
    eps = tif( issym(center), 0, 1e-12 );
    assert( isAlways(abs(norm(center) - 1) <= eps), 'nullspace_kone:normalize', 'Vector is not normalized, but should be (center-cector must have length 1)' );
    switch method;
        case {'null'};
            nullspace = nullspace_kone_null( center );
        case {'gs','gramschmidt','rand','random'};
            nullspace = nullspace_kone_gs( center );
        otherwise;
            error( 'kone:input', 'Wrong method given.' ); end;
    nullspace = normalizematrix( nullspace, 'colnorm',2 );

end

function [ nullspace ] = nullspace_kone_null( center );
    dim = size( center, 1 );
    center = center/norm(center);
    nullspace = null( center' );
    V = [center nullspace];
    for i = 1:dim;
        if( abs(V(1,i) - V(i,1)) > abs(V(1,i)) );
            V(:,i) = -V(:,i); end; end;
    nullspace = V(:,2:end);
end

function [ nullspace ] = nullspace_kone_gs( center );
    dim = size( center, 1 );
    center = center/norm(center);
    while( true );
        V = [center randn( dim, dim - 1 )];
        if( rank(V) == dim );
            break; end; end;
    V = gramschmidt( V );
    for i = 1:dim;
        if( abs(V(1,i) - V(i,1)) > abs(V(1,i)) );
            V(:,i) = -V(:,i); end; end;
    nullspace = V(:,2:end);
end

%%

function [ v ] = gramschmidt( v );
% Stabilized Gram Schmidt method for orthonormalization
% Input is a matrix including k column vector k = 2,3,...
% Output is the same matrix but the vectors are replaced with orthonormal vectors
% code by: Reza Ahmadzadeh (reza.ahmadzadeh@iit.it
    k = size( v, 2 );
    assert( k >= 2, 'gramschmidt:input', 'The input matrix must include more than one vector.' );
    for ii = 1:1:k;
        v(:,ii) = v(:,ii)/norm( v(:,ii) );
        for jj = ii+1:1:k
            v(:,jj) = v(:,jj) - proj( v(:,ii), v(:,jj) ); end; end;

end

function [ w ] = proj( u, v );
    % This function projects vector v on vector u
    w = (dot( u, v )/dot( u, u )) * u;   % bugfix for complex matrices
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
