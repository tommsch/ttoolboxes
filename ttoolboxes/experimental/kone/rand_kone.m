function [ K, center, nullspace ] = rand_kone( dim, N, center, verbose );
% generates a random kone in dimension `dim`
% WARNING: The generated kone is very not-random
% [ K, center, nullspace ] = rand_kone( dim, N, center, verbose );
%
% Input:
%   dim         integer, the dimension
%   N           2-element vector, minimum and maximum number of elements of the returned kone
%   center      optional, if not given it is random. If given, `center` is contained in the kone
%   verbose     default = 1, verbose level
%
% Output:
%   K           vertices of kone
%   center      vector which is somehow in the center of the kone
%   nullspace   vectors orthogonal to center
%
% Written by: tommsch, 2024

    N_given = nargin > 1 && ~isempty( N );
    if( ~N_given );
        N = [dim + 1 2*dim]; end;
    center_given = nargin >= 3 && ~isempty(center);
    if( nargin <= 3 || isempty(verbose) );
        verbose = 1; end;
    if( isscalar(N) );
        N = [N N]; end;

    while( true );
        K = make_random_kone( dim, N, N_given );
        if( ~center_given );
            [center, nullspace] = center_kone( K, 'verbose',-1 );
        else;
            if( ~in_kone( center, K ) );
                continue; end; end;
        if( all(sum(center .* K, 1) > 0) );
            break; end; end;

    K = normalizematrix( K, 'colnorm',2 );

    if( dim <= 3 && verbose >= 2 );
        plotm( K, 'funct','kone', 'hull',1 );
        plotm( K, 'funct','kone', 'linewidth',2, 'k', 'hold','on' );
        plotm( 1.25*center, 'r', 'arrow',1, 'linewidth',2, 'hold','on' );
        plotm( nullspace, 'b', 'arrow',1, 'linewidth',2, 'hold','on' );
        alpha 0.5
        axis equal
    end;

end

function [ K ] = make_random_kone( dim, N, N_given );

    while( true );
        K = randn( dim, randi( N ) );
        if( iskone(K) );
            break; end; end;

    if( ~N_given );
        max_N = max( N(2), N(1)^5 );
    else;
        max_N = N(2); end;

    rand_threshold = (1 + rand)/2;  % some number in [0 1]
    %rand_threshold = 1;  % some number in [0 1]
    idx = 1:size( K, 2 );
    Ko = K;
    method = randi( [1 4] );
    switch method;
        case 1;
            for k = idx( rand(size(idx)) < rand_threshold );
                K = [K Ko(:,k) + 0.01*randn(dim, randi(N))]; end;  %#ok<AGROW>
        case 2;
            %al = rand;
            
            for k1 = idx( rand(size(idx)) < rand_threshold );
                for round = 1:3;
                    al = rand;
                    K = [K al*Ko+(1-al)*Ko(:,k1)];  %#ok<AGROW>

                    %KK = normalizematrix( K );
                    %plotm( KK, '.' );
                
                    end; end;
        case 3;
            al = rand;
            K = [K al*K(:,idx( rand(size(idx)) < rand_threshold ))];
        case 4;
            % do nothing
        otherwise;
            fatal_error; end;

    
    idx = randperm( size(K, 2), min(size(K, 2), max_N) );
    K = K(:,idx);

end
function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
