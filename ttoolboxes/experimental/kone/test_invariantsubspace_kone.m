clc
m = mixvector( [0 5 7 8], 4 );
for i = 1:size( m, 2 )
    if( numel(unique(m(:,i))) ~= 4 )
        continue; end
    vprintf( '%v\n', m(:,i) )
    end

%%

% does not work with first eigenvector of T
%T={[-1 -1 2 5;-1 1 6 -2;0 0 0.15263259513510383 0.38956457960955304;0 0 0.070831705073982926 0.5979251280123784],[1 -1 -8 7;-1 1 -4 -1;0 0 1.1149718235617572 0.83523893179497544;0 0 0.39546255223177529 -0.82012959714299238]};

% maybe a counterexample
%T={[0 0 6 -6;0 1 0 4;0 0 0.19742156091164884 -0.15058016463683119;0 0 -0.42237945504391805 -0.8318988673362061],[0 1 6 6;1 0 0 -2;0 0 0.81348957296305935 0.41097913385215123;0 0 -0.38537134887846186 0.80101876961130347]};

condset( 'ct', 0 );
while( true );
    ct = ct + 1;
    fprintf( ' ct = %5i, ', ct );
    rng( ct );
    delta = 0.9;
    A = gallery_matrixset( 'dim',2, 'J',2, 'filter',{'vandergraft','irreducible'} );
    [flagA, dextremeA, KA, KAs] = has_invariant_kone_approximate( A, 'verbose',0 );
    if( flagA <= 0 );
        continue; end;

    Bo = {randn(2) randn(2)};
    [flagBo, dextremeBo, KBo, KBos] = has_invariant_kone_approximate( Bo, 'verbose',0 );
    if( flagBo > 0 );
        continue; end;
    S = {randi([-9 9], size(A{1}, 1), size(Bo{1}, 2)) randi([-9 9], size(A{1}, 1), size(Bo{1}, 2))};
    
    [rA, nfoA] = ipa( A, 'v',0, 'basictest',0 );
    %rBo = ipa( Bo, 'v',0 );
    %B = Bo./max(rBo).*min(rA).*delta;
    if( min(rho(A)) < 1e-12 );
        continue; end;
    B = Bo./max( rho(Bo) ).*min( rho(A) ).*delta;
    [rB, nfoB] = ipa( B, 'v',0 );
    %assert( abs( max(rB) - min(rA)*delta ) < 1e-12 );
    
    T = {[A{1} S{1}; zeros( size(B{1}, 1), size(A{1}, 2) ) B{1}], ...
         [A{2} S{2}; zeros( size(B{2}, 1), size(A{2}, 2) ) B{2}]};
    
    rT = ipa( T, 'v',0 );
    fprintf( 'rA = %5.5f, rB = %5.5f, rT = %5.5f, ', max(rA), max(rB), max(rT) );
    i_max = 20;
    
    levA = leadingeigenvalue( A );
    levT = leadingeigenvalue( T );
    tt.assert_eq( numel(levA), numel(levT) );
    for i = 1:numel( levA );
        tt.assert_almost_eq( levA{i}, levT{i} ); end;
    v0o = leadingeigenvector( T );
    v0o = mean( [v0o{1}], 2 );
    %v0o = mean( [v0o{3}], 2 );
    %v0o = [ones( size(A{1}, 1), 1 ); zeros( size(B{1}, 1), 1)];
    eo = randn( size(T{1}, 1), 1 );
    eo = eo/norm( eo, 1 )*(1 - delta);

    delta_e = 2;
    while( delta_e > 1e-12 );
        fprintf( 'd' );
        delta_e = delta_e/2;
        e = eo*delta_e;
        v0 = v0o + e;
        K = approximate_kone( T, v0, 'verbose',-1, 'maxiter',10, 'override' );
        if( iskone(K) );
            break; end; end;
    
    fprintf( 'delta_e = %5.5g, ', delta_e );
    fprintf( 'iskone: %1i\n', iskone(K) );
    if( ~iskone(K) );
        break; end; end;
clf; hold on;

v0p = stereographic_projection( v0, v0o, -2.1, true );
Kp = stereographic_projection( K, v0o, -2.1, true );
plotm( v0, 'Kone', 'center',v0o, 'arrow',1, 'c', 'linewidth',2 )
plotm( K, 'Kone', 'center',v0o, 'hull' )
%alpha .5
%axis equal

%%
J = 2;
dim1 = 1;
dim2 = 2;
%ct = 1;
use_sym = false;
delta = 0.9;
while( true );
    clc;
    ct = ct + 1  %#ok<NOPTS>
    rng( ct )
    while( true );
        [A, ~, mnfo1] = gallery_matrixset( 'randi', [-2 2], 'maxiter',500, 'dim',dim1, 'J',J, 'filter',{'vandergraft','mandatory', 'normal','never', 'irreducible'}, 'different' );
        if( isempty( A ) );
            continue; end;        
        if( numel( invariantsubspace(A, 'all') ) > 1 );
            continue; end;
        flag_A = has_invariant_kone_approximate( A );
        if( ~flag_A );
            continue; end;
        rhoA = ipa( A );
        fprintf( 'Found matrix A\n' );
        
        while( true );
            [B, ~, mnfo2] = gallery_matrixset( 'randi',[-2 2], 'maxiter',500, 'dim',dim2, 'J',J, 'filter',{'vandergraft','mandatory', 'normal','never', 'irreducible'}, 'different' );
            if( isempty( B ) );
                continue; end;
            B = randomize_matrixset( B, 'rho' );
            if( numel( invariantsubspace(B, 'all') ) > 1 );
                continue; end;
            flag_B = has_invariant_kone_approximate( B );
            if( ~flag_B );
                continue; end;
            rhoB = ipa( B );
            B = B.*(1/rhoB.*rhoA./delta);
            fprintf( 'Found matrix B\n' );
            break; end;
        break; end;
    
    clc
    S = {};
    S{1} = A{1} - eye(size(A{1}));
    S{2} = A{2} - eye(size(A{2}));
    N = zeros( dim2, dim1 );
    T1 = [A{1}  S{1};N B{1}];
    T2 = [A{2} S{2};N B{2}];
    T = {T1 T2};
    ipa( T, 'kone', 'delta',(1/delta + 1)/2 )
    flag_T = has_invariant_kone_approximate( T );
    if( flag_T == 1 );
        continue;
    elseif( flag_T == 0 );
        break;
    elseif( flag_T == -1 );
        break; end;  % This should not happen
    
    %v0 = zeros( dim1 + dim2, 1 );
    %for i = 1:numel( A );
    %    v0i = leadingeigenvector( A{i} );
    %    for j = 1:numel( v0i );
    %        v0 = v0 + [v0i{j}; zeros(dim2, 1)]; end;
    %    v0i = leadingeigenvector( B{i} );
    %    for j = 1:numel( v0i );
    %        v0 = v0 + [zeros(dim1, 1); v0i{j}]; end; end;
    %[~, nfoT_et] = ipa( T, 'Kone', 'v0',v0, 'smpflag',ipa_constant.extravertex, 'delta',1.01, 'recomputenorm',3, 'maxiter',30, 'invariantsubspace','none', 'sym',0, 'maxvertexnum',5000 );
    %if( isanyof( nfoT_et.log.errorcode, {ipa_errorcode.MAXITERATION,ipa_errorcode.MAXNUM_VERTEXREACHED, ipa_errorcode.STALENORM,ipa_errorcode.MAXNUM_VERTEXREACHED} ) );
    %    continue; end;
    %if( ~isequal(nfoT_et.log.errorcode, ipa_errorcode.VALIDATEUPPERBOUNDNORM) );
    %    break; end;
    end;
        

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*NBRAK2>
