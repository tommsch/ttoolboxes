function [ ret, value ] = iskone( VV, solver, eps );
% [ ret, value ] = iskone( VV, [solver], [eps] );
% Checks whether the conic convex hull of a set of vertices defines a pointed kone.
%
% Input:
%   VV      matrix of column vectors, the vertices of the kone
%   solver  optional, default='a' (auto). Possible values: 'a'/'auto','g'/'gurobi','m'/'matlab'
%   eps     optional, double, default = 1e-12, threshold until when a kone still counts as a kone.
%
% Output:
%   ret     bool, true if VV is a kone
%   value   something, depending on the method this value tells something about how much kone-like the kone is
%
% Notes:
% - It is not checked whether the kone has non-empty interior.
%   To do this, one best checks whether VV has full rank.
%
% Example: iskone( [0 1;1 1]' )  % yields true
%
% Written by: tommsch, 2024-06-03
    persistent sym_warning_printed;
    
    if( nargin <= 1 || isempty(solver) );
        solver = 'a'; end;
    if( nargin <= 2 || isempty(solver) );
        eps = 1e-12; end;
    if( issym(VV) );
        VV = double( VV );
        
        if( isempty(sym_warning_printed) );
            sym_warning_printed = true; 
            warning( 'iskone:sym', 'The function `iskone` works purely numerically currently. The result may be wrong for symbolic input.\n  This warning is only printed once.' ); end;
        nrm_imag = norm( imag(VV) );
        if( nrm_imag < 1e-12 );
            VV = real( VV );
        else;
            error( 'iskone:sym', 'Casting to double led to a complex number. `iskone` cannot handle this case.' ); end; end;
    if( ~isreal(VV) );
        warning( 'iskone:complex', 'Input is complex. This function unconditionally returns ''false'' for complex input.' );
        ret = false;
        value = 0;
        return; end;
    VV = double( VV );
    nrm = sum( abs(VV), 1 );
    VV = VV./nrm;
    VV(:,nrm < 1e-12) = [];
    N = size( VV, 2 );
    dim = size( VV, 1 );
    switch solver;
        case {'a','auto'};
            value = do_lp( VV, N, dim );
        case {'g','gurobi'};
            value = do_lp_gurobi( VV, N, dim );
        case {'m','matlab','sedumi','octave'};
            value = do_lp_matlab( VV, N, dim );
        otherwise;
            error( 'iskone:solver', 'Bad ''solver'' passed. See the help for the possible solvers.' ), end;
    ret = value > eps;
    

end

function [ ret ] = do_lp( VV, N, dim );
    if( has_gurobi() );
        ret = do_lp_gurobi( VV, N, dim );
    else;
        ret = do_lp_matlab( VV, N, dim ); end;
end

function [ ret ] = do_lp_matlab( VV, N, dim );
    f = [zeros( dim, 1 ); -1];
    A = [-VV' ones( N, 1 )];
    b = zeros( N, 1 );
    Aeq = [];
    beq = [];
    lb = [-inf( dim, 1 ); -1];
    ub = [inf( dim, 1 ); 1];

    
    installed_matlab = isequal( exist('linprog', 'file'), 2 );
    installed_sedumi = isequal( exist('sedumi_linprog', 'file'), 2 );
    if( installed_matlab );
        options = optimoptions( 'linprog', 'Display','off', 'Algorithm','dual-simplex' );
        [x, fval, exitflag, output] = linprog( f, A, b, Aeq, beq, lb, ub, options );  %#ok<ASGLU>
    elseif( installed_sedumi );
        [x, fval, exitflag, output] = sedumi_linprog( f, A, b, Aeq, beq, lb, ub );  %#ok<ASGLU>
    else;
        error( 'iskone:solver', 'No LP solver installed. Install optimization toolbox or Sedumi.' ); end;
    if( ~isequal(exitflag, 1) );
        ret = nan;
    elseif( norm(x(1:dim), 1) < 1e-9 );
        ret = false;
    else;
        ret = -fval > 1e-9; end;
end

function [ ret ] = do_lp_gurobi( VV, N, dim );
    % maximizes the smallest "angle" from the normal plane to any vector of the kone
    % does not normalize the normal-vector, and thus the resulting normal may be outside of the kone
    VV = double( VV );
    model.A = [sparse( VV' ) -sparse( ones( N, 1 ) )];
    model.rhs = zeros( N, 1 );
    model.sense = repmat( '>', [N 1] );

    model.lb = [-inf( dim, 1 ); -1];
    model.ub = [inf( dim, 1 ); 1];
    model.modelsense = 'max';
    model.obj = [zeros( dim, 1 ); 1];
    params.Method = 0;
    params.OutputFlag = 0;
    
    result = gurobi( model, params );
    if( ~isequal(result.status, 'OPTIMAL') );
        ret = false;
    elseif( norm(result.x(1:dim), 1) < 1e-9 );
        ret = false;
    elseif( result.x(end) <= 1e-9 );
        ret = false;
    else;
        normal = result.x(1:dim);
        normal = normal/norm( normal );
        ret = min( sum(normal .* VV, 1) );
        if( ret <= 0 );
            ret = 0; end; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
