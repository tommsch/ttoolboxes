
plotm( VV, 'Kone', 'center',bv.cyclictree.polytopenorm.auxiliary_data{1}, 'hull' )
plotm( VV(:,idx_pn), 'Kone', 'center',bv.cyclictree.polytopenorm.auxiliary_data{1}, 'or', 'hold','on' )
%%
oo = bv.cyclictree.oo;
for i = 1:numel( bv.cyclictree.oo );
    idx = ~any( bv.cyclictree.oo{i}, 1 );
    assert( nnz(idx) == 1 && idx(1) == true );
    ordering = bv.cyclictree.ordering{i};
    oo{i}(1:numel(ordering),1) = ordering;
end
oo = merge_oo( oo{:} );
oo(:,idx_pn)
%oo(:,idx_pn)



%%
condset( 'ct', 0 );
condset( 'No', [] );
condset( 'Yes', [] );
condset( 'TT', {} );
condset( 'nfo', {} );
condset( 'ordering', {} );
% ct = 181; Yes = []; No = []; i = 0; TT = {}; nfo = {}; ordering = {};
while( true );
    %Yes=[344 397 414 474 480 511 576 698];
    %No=[188 191 202 210 218 223 239 253 266 269 279 281 298 303 304 315 318 321 322 323 324 331 337 343 350 363 369 384 388 394 404 407 409 420 438 440 444 464 472 473 478 486 514 515 517 518 526 532 543 544 557 561 563 568 590 621 626 627 630 637 644 645 648 649 659 660 684 701 719 735 737 762 771 781 785 790 816 818 819 820 830 838 839 859];
    %[TT_, ~, prop] = gallery_matrixset( 'ternary', 'dim',3, 'J',2, 'different', 'filter',{'vandergraft', 'finite', 'nonneg','nall','irreducible', 'orthogonal','none'} );
    %[nfo_, ordering_] = invariant_kone( TT_, 'method','iter', 'plot','none', 'stale_norm',[12 20], 'maxvertexnum',6000, 'maxsmpdepth',1, 'maxonenorm',inf, 'sym',0, 'maxiter',30 );
    
    %Yes=[182 210 227 244 264 277 289 297 299 305 324 359 360 369 387 391 397 414 420 452 473 477 484 512 534 544 582 614 662 745 747 776 785 799 811 823 825 832 862 871 879 897 914 927 935 945 994 995 998 1003 1024 1045 1046 1052 1055 1066 1086 1088 1092 1095 1130 1131 1133 1164 1174 1178 1186 1190 1240 1264 1308 1339 1345 1379 1418 1420 1436 1443 1460 1471 1481 1483 1493 1500 1506 1515 1523 1526 1578 1590 1593 1604 1632 1637 1639 1645 1646 1654 1669 1698 1715 1725 1746 1751 1769];
    %No=[190 198 203 211 217 231 237 238 239 246 253 275 279 281 283 285 302 307 311 317 323 329 330 343 356 376 383 384 385 390 394 396 400 409 431 439 441 442 446 447 449 466 470 475 478 504 505 506 514 521 522 533 541 543 548 551 562 564 583 588 591 601 607 609 615 622 626 636 655 657 660 666 668 669 675 678 682 684 693 704 705 708 749 751 773 777 781 786 793 797 805 817 818 828 835 843 847 854 856 885 888 893 900 915 923 924 928 934 941 942 957 960 962 963 964 973 980 981 983 1004 1020 1022 1033 1036 1039 1051 1060 1061 1067 1068 1076 1080 1081 1084 1091 1104 1116 1122 1138 1139 1142 1166 1169 1170 1185 1200 1203 1205 1209 1212 1220 1222 1223 1225 1248 1261 1274 1277 1283 1304 1305 1319 1321 1342 1359 1362 1364 1371 1390 1393 1396 1397 1400 1405 1406 1410 1413 1415 1417 1424 1425 1431 1434 1439 1441 1445 1448 1449 1450 1472 1488 1490 1491 1495 1507 1511 1516 1521 1529 1537 1542 1548 1559 1563 1573 1579 1592 1598 1599 1624 1630 1631 1655 1658 1659 1678 1682 1687 1693 1700 1708 1714 1717 1724 1729 1733 1745 1766 1776 1778 1785];
    %#smps = [1 1 1 1 1 1 2(1) 1 1 2(!) 1 2 1 1 1 1 1 1 1 1 2 1 2 2 1 1 11 2 3 2 1 1 7 1 1 1 1 3 1 2 1 1 1 2 1 1 1 2 1 1 2 3 1 1 1 1 1 2 1 1 1 1 1 1 2 2 1 1 1 2 1 4 1 3 1 1 2 1 7 1 1 1 1 4 2 1 1 1 2 1 1 1 1 1 1 1 1 2 1 1 1 1 1 1 1];
    %[TT_, ~, prop] = gallery_matrixset( 'randn', 'dim',3, 'J',2, 'different', 'filter',{'vandergraft', 'finite', 'nonneg','nall','irreducible', 'orthogonal','none'} );
    %[nfo_, ordering_] = invariant_kone( TT_, 'method','iter', 'plot','none', 'stale_norm',[12 20], 'maxvertexnum',6000, 'maxsmpdepth',1, 'maxonenorm',inf, 'sym',0, 'maxiter',30 );

    %Yes=[340 447 539({[1],[2], 554 642 654 813 907 1226 1515 2069 2105 2166 2238 2423 2611 2638 2929 3080 3090 3408 3414 3670 3916 4227 4294];
    %No=[310 323 325 337 343 369 419 429 431 438 439 458 460 461 462 487 501 514 515 518 525 582 587 591 614 615 627 647 674 699 720 721 723 725 731 752 756 757 780 795 796 797 800 833 836 839 843 859 867 878 885 917 922 937 956 973 991 1009 1030 1041 1046 1047 1075 1081 1088 1096 1124 1128 1129 1145 1153 1154 1170 1203 1208 1209 1212 1238 1242 1253 1259 1290 1325 1329 1332 1359 1363 1399 1404 1423 1438 1440 1445 1450 1466 1470 1478 1538 1562 1573 1600 1610 1625 1626 1651 1656 1678 1683 1691 1711 1716 1752 1759 1768 1786 1796 1808 1812 1816 1857 1872 1886 1900 1907 1937 1970 1988 1997 2003 2011 2039 2054 2072 2089 2090 2092 2106 2165 2205 2210 2211 2216 2239 2261 2263 2268 2269 2340 2348 2350 2381 2394 2396 2408 2414 2417 2424 2425 2435 2446 2474 2506 2511 2533 2550 2569 2578 2597 2630 2632 2647 2680 2719 2749 2762 2778 2791 2792 2817 2840 2844 2854 2864 2877 2894 2904 2908 2912 2917 2967 2968 2982 3021 3026 3030 3036 3043 3053 3056 3065 3068 3075 3127 3156 3159 3193 3200 3228 3230 3241 3247 3267 3270 3283 3297 3328 3337 3348 3370 3375 3382 3384 3386 3391 3399 3419 3420 3439 3442 3448 3460 3496 3498 3501 3503 3522 3525 3528 3549 3619 3621 3655 3666 3677 3697 3721 3722 3734 3738 3788 3789 3791 3824 3825 3846 3850 3854 3895 3897 3900 3901 3905 3915 3924 3934 3954 3955 3985 4007 4024 4037 4051 4097 4098 4122 4156 4167 4200 4206 4228 4237 4329 4330 4334 4351 4354 4358 4372 4373 4402 4403 4404 4410 4422 4431];
    %[TT_, ~, prop] = gallery_matrixset( 'ternary', 'dim',4, 'J',2, 'different', 'filter',{'vandergraft', 'finite', 'nonneg','nall','irreducible', 'orthogonal','none'} );
    %[nfo_, ordering_] = invariant_kone( TT_, 'method','iter', 'plot','none', 'stale_norm',[12 20], 'maxvertexnum',6000, 'maxsmpdepth',1, 'maxonenorm',inf, 'sym',0, 'maxiter',30 );

    clc
    i = i + 1  %#ok<NOPTS>
    ct = ct + 1  %#ok<NOPTS>
    % ct = No4b5(i);
    % Yes3a2=[182   210   227   231   289   297   299   305   343   387   420   452   473   484   512   534   544   582   662   811]
    % No3a=[190 198 211 217 237 238 239 244 246 253 264 275 277 279 281 283 285 302 307 317 323 324 329 359 360 369 376 383 384 385 390 391 396 397 400 409 414 431 442 446 447 449 466 470 475 477 478 505 506 514 521 522 533 541 543 548 551 562 564 583 591 601 607 614 615 622 626 636 657 660 666 668 669 678 682 684 693 704 705 708 745 747 749 751 776 781 785 786 793 797 799 805]
    % Yes3b5=[264   277   324   360   369   391   414   477   614   747   776   799]
    % No3b5=[190 198 211 217 237 238 239 244 246 253 264 275 277 279 281 283 285 302 307 317 323 324 329 359 360 369 376 383 384 385 390 391 396 397 400 409 414 431 442 446 447 449 466 470 475 477 478 505 506 514 521 522 533 541 543 548 551 562 564 583 591 601 607 614 615 622 626 636 657 660 666 668 669 678 682 684 693 704 705 708 745 747 749 751 776 781 785 786 793 797 799 805];
    % Yes3c7
    % No3c7=[190 198 211 217 237 238 239 244 246 253 275 279 281 283 285 302 307 323 329 359 376 383 384 385 390 396 397 400 409 431 442 446 447 449 466 470 475 478 505 506 514 521 522 533 541 543 548 551 562 564 583 591 601 607 615 622 626 636 657 660 666 668 669 678 682 684 693 704 705 708 745 749 751 781 785 786 805];

    % [T, ~, info] = gallery_matrixset( 'randn', 'dim',3, 'J',2, 'different', 'filter',{'vandergraft', 'finite', 'nonneg','nall','irreducible', 'orthogonal','none'} );
    % nfo = invariant_kone( T, 'method','all', 'plot','none', 'stale_norm',10 );

    % Yes4a2=[838 1026 1166 1245 1298 1304 1341 1580 1672 1680 1745 1845 1979 2190 2330 2583 2606];
    % No4a2=[830 840 841 844 847 857 861 884 885 896 920 921 945 948 955 975 981 986 998 1016 1027 1029 1063 1070 1088 1093 1097 1115 1123 1136 1139 1140 1142 1162 1163 1165 1186 1190 1194 1205 1208 1212 1220 1224 1234 1262 1266 1275 1278 1284 1290 1301 1331 1333 1356 1358 1360 1370 1406 1431 1434 1447 1465 1477 1499 1525 1530 1544 1546 1547 1563 1593 1598 1600 1612 1624 1645 1651 1653 1666 1674 1682 1701 1718 1758 1777 1779 1793 1823 1838 1860 1868 1880 1891 1901 1904 1916 1935 1951 1952 1955 1958 1985 2011 2014 2016 2041 2045 2046 2054 2056 2065 2069 2086 2112 2115 2125 2143 2146 2151 2161 2164 2169 2178 2189 2216 2232 2236 2237 2241 2260 2266 2271 2290 2294 2309 2311 2322 2325 2341 2344 2345 2351 2364 2377 2396 2445 2452 2462 2485 2487 2533 2547 2558 2562 2563 2565 2568 2569 2587 2598 2612 2618 2636 2646 2674 2678 2689 2700 2704 2712];
    % Yes4b5=[885 1016 1142 1212 1612 1653 1868 1935 1955 2056 2086 2112 2341 2377 2569 2618 2674];
    % No4b5=[830 840 841 844 847 857 861 884 896 920 921 945 948 955 975 981 986 1027 1029 1063 1070 1088 1093 1097 1115 1123 1139 1140 1162 1163 1165 1186 1190 1194 1205 1208 1220 1224 1234 1262 1266 1275 1278 1284 1290 1301 1331 1333 1356 1358 1360 1370 1406 1431 1434 1447 1465 1477 1525 1530 1544 1546 1547 1563 1593 1598 1600 1624 1645 1651 1666 1674 1682 1701 1718 1758 1777 1779 1793 1823 1838 1880 1891 1901 1904 1951 1952 1958 1985 2011 2014 2016 2041 2045 2046 2054 2065 2069 2115 2125 2143 2151 2161 2164 2169 2178 2189 2216 2232 2236 2237 2241 2260 2266 2271 2290 2294 2309 2311 2322 2325 2344 2345 2351 2364 2396 2445 2452 2462 2485 2487 2533 2547 2562 2563 2565 2568 2587 2598 2612 2636 2646 2678 2689 2700 2704 2712];
    % Yes4c7=[1823 2345];
    % No4c7=[830 840 841 844 847 857 861 884 896 920 921 945 948 955 975 981 986 1027 1063 1070 1088 1093 1097 1115 1123 1139 1140 1162 1163 1165 1186 1190 1194 1205 1208 1220 1224 1234 1262 1266 1275 1284 1290 1301 1331 1333 1356 1358 1360 1370 1406 1431 1434 1447 1465 1477 1525 1530 1546 1547 1563 1593 1598 1600 1624 1645 1651 1666 1682 1701 1718 1758 1777 1779 1793 1838 1880 1891 1901 1904 1951 1952 1958 1985 2011 2014 2016 2041 2045 2046 2054 2065 2069 2115 2125 2143 2151 2161 2164 2169 2178 2189 2216 2232 2236 2237 2241 2260 2266 2271 2290 2294 2309 2311 2322 2325 2344 2351 2364 2396 2445 2452 2462 2485 2487 2533 2547 2562 2563 2565 2568 2587 2598 2612 2636 2646 2678 2689 2700 2704 2712];

    % [T, ~, info] = gallery_matrixset( 'randn', 'dim',4, 'J',2, 'different', 'filter',{'vandergraft', 'finite', 'nonneg','nall','irreducible', 'orthogonal','none'} );
    % nfo = invariant_kone( T, 'method','all', 'plot','none', 'stale_norm',10, 'maxvertexnum',6000 );
    % Yes5a3 = [2779 3457 3917 4015 4697 4977 5016 5133 5948 5960 6905 7028 7185 7865 7877 8046 8184 8589 8723 8792 9114 9177 9232 9460 9577 9690 9734 10450 10515 10579 10838 10988 11149 11238 11570 11895 12120 12164 12538 13240 13694 14161 14277 14353 14492 14860 15448 15915];
    % No5a3 =[2725 2735 2749 2750 2772 2784 2785 2796 2843 2852 2857 2873 2946 2947 2980 3016 3021 3029 3053 3054 3057 3096 3161 3190 3200 3229 3329 3339 3404 3408 3427 3429 3462 3465 3493 3513 3517 3536 3581 3592 3629 3651 3654 3657 3682 3701 3734 3735 3754 3780 3794 3804 3808 3839 3883 3909 3910 3920 3933 3967 3973 3974 4005 4042 4058 4079 4081 4095 4100 4127 4152 4194 4210 4270 4300 4305 4349 4408 4435 4449 4450 4480 4539 4652 4734 4745 4799 4830 4852 4853 4856 4860 4875 4900 4937 5000 5073 5076 5077 5088 5154 5165 5173 5182 5199 5225 5249 5260 5275 5299 5360 5394 5478 5568 5575 5580 5601 5722 5765 5777 5790 5815 5847 5876 5889 5902 5939 5959 5970 6048 6054 6066 6119 6125 6149 6233 6237 6256 6308 6340 6355 6428 6500 6515 6534 6535 6552 6612 6700 6710 6719 6730 6741 6766 6783 6790 6817 6822 6832 6869 6896 6920 6936 6974 7021 7035 7049 7075 7093 7114 7161 7212 7241 7250 7276 7381 7384 7427 7430 7450 7464 7469 7481 7506 7547 7692 7727 7755 7759 7797 7838 7848 7867 7884 7885 7913 7914 7920 8044 8076 8093 8123 8125 8198 8225 8242 8306 8372 8451 8456 8471 8490 8513 8517 8533 8544 8585 8588 8601 8609 8621 8677 8739 8748 8756 8837 8845 8896 8908 8910 8967 8985 9018 9052 9084 9095 9097 9105 9109 9159 9160 9164 9183 9189 9200 9228 9242 9249 9280 9300 9311 9348 9400 9405 9474 9479 9509 9517 9527 9575 9576 9602 9621 9635 9663 9665 9678 9708 9709 9711 9714 9767 9832 9874 9899 9927 9941 9944 9980 9988 9995 10019 10023 10030 10035 10047 10114 10127 10155 10193 10236 10245 10250 10417 10424 10494 10495 10527 10529 10548 10560 10590 10612 10659 10726 10781 10792 10867 10872 10873 10876 10924 10926 10935 10998 11000 11002 11017 11031 11059 11089 11118 11152 11161 11297 11299 11352 11354 11358 11360 11378 11431 11472 11529 11543 11592 11658 11664 11708 11717 11735 11746 11759 11771 11788 11868 11872 11892 11938 11958 11973 11989 12000 12023 12078 12081 12149 12155 12191 12199 12206 12210 12236 12247 12267 12333 12340 12346 12373 12412 12414 12417 12431 12463 12467 12469 12554 12557 12598 12654 12656 12780 12796 12801 12803 12818 12834 12843 12872 12897 12925 12938 12940 12941 12974 13030 13032 13077 13136 13169 13199 13249 13251 13305 13326 13350 13359 13369 13404 13415 13521 13569 13591 13645 13655 13666 13758 13785 13788 13799 13810 13876 13925 13948 14070 14127 14179 14181 14226 14278 14294 14350 14354 14376 14380 14391 14414 14440 14444 14453 14487 14536 14628 14650 14699 14758 14761 14780 14807 14847 14865 14902 14906 14914 14942 14946 15098 15162 15232 15265 15284 15303 15308 15356 15387 15394 15451 15476 15512 15531 15598 15602 15617 15644 15747 15774 15777 15780 15830 15844 15868 15925 15941 15963 15991];

    %Yes=[180 275 397 418 517 733];
    %No=[12 15 23 35 41 42 46 53 62 81 93 97 100 101 109 121 123 126 142 143 152 153 154 164 165 168 171 174 177 192 199 206 215 229 232 234 241 242 248 253 254 255 260 264 271 278 280 281 287 291 296 299 304 309 320 321 341 342 353 356 363 380 382 384 411 416 417 420 423 426 427 430 454 464 482 492 509 510 526 534 544 545 546 550 558 561 573 578 580 583 585 588 602 610 614 618 620 624 638 646 664 665 676 681 699 711 726 729 735 739 744 745 754 758];
    %[T, ~, prop] = gallery_matrixset( 'randn', 'dim',5, 'J',2, 'different', 'filter',{'vandergraft', 'finite', 'nonneg','nall','irreducible', 'orthogonal','none'} );
    %nfo = invariant_kone( T, 'method','iter', 'plot','none', 'stale_norm',[12 20], 'maxvertexnum',6000, 'maxsmpdepth',1, 'maxonenorm',inf, 'sym',0, 'maxiter',30 );
    display( Yes )
    display( No )
    rng( ct );
    [TT_, ~, prop] = gallery_matrixset( 'ternary', 'dim',3, 'J',2, 'different', 'filter',{'vandergraft', 'finite', 'nonneg','nall','irreducible', 'orthogonal','none'} );
    if( isempty(TT_) );
        continue; end;
    matrixinfo( prop, 'compact' )
    
    %TT_={[0 -1 -1;-1 1 0;-1 1 1],[1 0 0;0 0 1;-1 1 1]};
    %TT_={[-0.9896677828672189 1.0214498508517063 -0.26031003556420201;0.11985399866001065 1.6264019487299897 0.28827653741162967;0.031114842226374999 0.95733776222695832 -0.24338267231659486],[-0.66068847233203543 -0.2816679799445268 -0.035289788459247518;-0.2172156576309531 1.9984482749391432 0.61320861399065618;-0.39614642030961961 0.55570480782772003 0.97252535758884462]};

    [nfo_, ordering_] = invariant_kone( TT_, 'method','iter', 'plot','none', 'stale_norm',[12 20], 'maxvertexnum',6000, 'maxsmpdepth',1, 'sym','auto', 'maxiter',30 );
    err_ = nfo_.log.errorcode;
    if( isanyof( err_, {ipa_errorcode.IS_NOT_A_KONE, ipa_errorcode.NOINVARIANTKONE, ipa_errorcode.LIKELYNOINVARIANTKONE, ipa_errorcode.COMPLEXKONE} ) );
        continue;
    elseif( err_ < 0 );
        nfo{end+1} = nfo_;
        ordering{end+1} = ordering_;
        TT{end+1} = TT_;
        Yes = [Yes ct];
    elseif( err_ >= 0 );
        No = [No ct]; end;
end


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dim = 3;  %#ok<UNRCH>
Ao_124123 = gallery_matrixset( 'randn', 'dim',dim, 'poslead', 'J',2 )
Ao = Ao_124123
A = Ao;
M = A;


%A=cell([1 2]);
%A{1}=[1.577891385239691 -0.11128940048835978 -0.73548422169418526;-0.8917372947836224 -0.36888610050495352 0.32038333576177597;-0.40890387223135832 1.0781518440116757 -0.61725352607424955];
%A{2}=[0.84596229212909391 -0.098574961336713354 -1.3326806657276766;-0.40282002693231311 -0.20284326758999577 0.30783206008212527;-0.91780647408330962 0.22822871341669998 0.1983602984009262];
%center = [0.84131228915286083;-0.31972967386739354;-0.41602350188576714];
% smp = [1;2]

% A=cell([1 2]);
% al1 = .1; A{1} = [2 0 0;0 sin(al1) cos(al1);0 -cos(al1) sin(al1)];
% al2 = .1; A{2} = [1.5 0 0;0 sin(al2) cos(al2);0 -cos(al2) sin(al2)];


dim = size( Ao{1}, 1 );
J = numel( Ao );


; %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[K_Et,  flag_Et] =  invariant_kone_Et( Ao );
[K_all, flag_all] = invariant_kone_allproducts2( Ao );

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dim = size( Ao{1}, 1 );
J = numel( Ao );

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
A = Ao;
K = zeros( dim, 0 );
for len_v0 = 1:5;
    oo_v0 = mixvector( 1:J, len_v0 );
    for idx = 1:size( oo_v0, 2 );
        oo_v0_i = reducelength( oo_v0(:,idx), 1 );
        if( numel(oo_v0_i) < len_v0 );
            continue; end;
        Pi = buildproduct_fast( A, oo_v0_i );
        v_new = leadingeigenvector( A{1} );
        
        for n = 1:numel( v_new );
            dots = sum( v_new{n} .* K ) > 0;  % this test is too restrictive but simple to implement. For the time being we try whether it works.
            if( all(dots) );
                % do nothing
            elseif( all(~dots) );
                v_new{n} = -v_new{n};
            else;
                error( 'ipa:kone', 'Set of matrices most likely has no invariant kone.' ); end; end;
        v_new = [v_new{:}];

        oo{end+1} = oo_v0_i;
        K = [K v_new];
        
        assert( isreal(K) );

        plotm( K, 'funct','kone', 'hull',1 );

        for rounds = 1:4;
            nrm_old = polytopenorm( K, K, 'k7', 'output','ub' );
            idx_old = nrm_old < .9999;
            K(:,idx_old) = [];
            K_new = zeros( dim, 0 );
            for j = 1:J;
                K_new = [K_new A{j}*K]; end;
            K_new = normalizematrix( K_new, 'colnorm',2 );
            dist_new = pdist2( K_new', K', 'squaredeuclidean' );
            idx_dist_new = any( dist_new < 1e-12, 2)';
            K_new(:,idx_dist_new) = [];
    
            nrm_new = polytopenorm( K_new, K, 'k7', 'output','ub' );
            if( all(nrms < .9999) );
                break; end;
            idx_new = nrm_new < .999;
            K_new(:,idx_new) = [];
            K = [K K_new];
        end
    end
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Algorithm all vertices

A = Ao;
J = numel( A );
dim = size( A{1}, 1 );


for len_v0 = 1:10;
    if( len_v0 == 1 );
        Ko = zeros( dim, 0 );
        oo_v0 = [];
        r = [];
        rk = []; end;
    oo_len = mixvector( 1:J, len_v0 );
    v0i = {};
    rki = [];
    ri = [];
    for i = 1:size( oo_len, 2 );
        P = buildproduct_fast( A, oo_len(:,i) );
        ri(i) = rho( P )^(1/len_v0);
        rki(i) = rho_kone( P )^(1/len_v0);
        v0i{i} = leadingeigenvector( P, 'nocell' );
    end
    Ko = [Ko v0i{:}];
    r = [r ri];
    rk = [rk rki];
    oo_v0(1:len_v0,end+1:end+size(oo_len, 2)) = oo_len; 
    
    nrm = polytopenorm( Ko, Ko, 'k7', 'output','ub' );
    idx = nrm < 0.999999;
    nrm(idx) = [];
    Ko(:,idx) = [];
    oo_v0(:,idx) = [];
    r(idx) = [];
    rk(idx) = [];
    [~, sort_idx] = sort( nrm, 'descend' );
    oo_v0(:,sort_idx)

    clf; hold on;
    plotm( Ko, 'funct','kone', 'hull',1 )
    plotm( Ko, 'arrow',1.1, 'r' )
    axis equal
    center = mean( Ko, 2 )
    view( center )
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Algorithm 3

dim = 3;
A = Ao
[rt, nfot] = ipa( A, 'algorithm','k7', 'plot','polytope', 'smp',{[1],[2]}, 'validateupperbound_norm',1.01  );

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Algorithm B to generate an invariant kone
% tries to approximate from inside and from outside
clc

X = Ao;
J = numel( X );
dim = size( Ao{1}, 1 );
v0 = {};
for j = 1:J;
    v0 = [v0 leadingeigenvector( X{j} )]; end;
v0 = [v0{:}];

vdisp( X{1} )
vdisp( X{2} )
%X = X ./ rho( X );

almin = 0;
for j2 = 1:size( v0, 2 );
    for k2 = j2+1:size( v0, 2 );
        almin = max( [almin acosd( sum( v0(:,j2) .* v0(:,k2), 1 ) )] ); end; end;
fprintf( 'al_min = %f\n', almin );
Ko = randn( dim, 100*dim^2 );
Ko = normalizematrix( Ko, 'colnorm',2 );
Ko = [Ko v0];
K_min = Ko;
K_max = Ko;
al1 = almin + 10;
for j2 = 1:size(v0, 2 );
    clf;
    plotm( 1.1*K_min, 'funct','kone', 'r.', 'hull',1, 'hold','on')
    plotm( 1.2*K_max, 'funct','kone', 'g.', 'hull',1, 'hold','on')
    for k2 = 1:size(v0, 2 );
        plotm( v0(:,k2),  'r', 'arrow',1, 'hold','on' ); end;
    idx_min = acosd( sum( K_min .* v0(:,j2), 1 ) ) > almin;
    idx_max = acosd( sum( K_max .* v0(:,j2), 1 ) ) > al1;
    K_min(:,idx_min) = [];
    K_max(:,idx_max) = [];
    plotm( 1.1*K_min, 'funct','kone', 'ro', 'hull',1, 'hold','on')
    plotm( 1.2*K_max, 'funct','kone', 'go', 'hull',1, 'hold','on')
end;

%pause

K_max_o = K_max;
K_min_o = K_min;
axis equal

clf;
K_min = K_min_o;
K_max = K_max_o;
for it = 1:10;
   
    %plotm( Ko,  'k', 'funct','kone', 'hull',1 );    
    clf;
    plotm( K_min, 'r', 'funct','kone', 'hull',1, 'hold','on' );
    plotm( K_max, 'g', 'funct','kone', 'hull',1, 'hold','on' );
    K_min_idx = in_kone( K_min, K_min ) < dim;
    K_min = K_min(:,K_min_idx);
    K_min_old = K_min;
    K_min_1 = X{1}*K_min;
    K_min_2 = X{2}*K_min;
    K_min = [K_min K_min_1 K_min_2];
    K_min_idx = in_kone( K_min, K_min ) < dim;
    K_min = K_min(:,K_min_idx);
    
    
    K_max_idx = in_kone( K_max, K_max ) < dim;
    K_max = K_max(:,K_max_idx);
    K_max_old = K_max;
    K_max_1 = X{1}*K_max;
    K_max_2 = X{2}*K_max;
    K_max = [K_max K_max_1 K_max_2];
    K_max_idx = in_kone( K_max, K_max ) < dim;
    K_max = K_max(:,K_max_idx);
    

    test_min_max_old = all( in_kone( K_min_old, K_max_old ) );
    test_min = all( in_kone( K_min_old, K_min ) );
    test_max = all( in_kone( K_max, K_max_old ) );
    test_min_max = all( in_kone( K_min, K_max ) );
    fprintf( '%i - %i - %i - %i\n', test_min_max_old, test_min, test_max, test_min_max );
    %min_check = iskone( K_min );
    %max_check = iskone( K_max );
    %if( ~min_check && ~max_check );
    %    fprintf( 'No a kone anymore.' );
    %    break; end;
    switch dim;
        case 3; view( mean(v0,2) );
        alpha .5
        end;
    
    %pause; 
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Algorithm C to generate an invariant kone
% tries to approximate from inside and from outside
% is similar to Algorithm B

T_1 = Ao;
v0 = {};
for j = 1:J;
    v0{j} = leadingeigenvector( T_1{j} ); end;
v0 = [v0{:}]
v0 = [v0{:}]
if( ~isreal(v0) );
    v0 = [real(v0) imag(v0)]; end;
al_min = acosd( -max( pdist( v0', 'cosine' ) ) + 1);
assert( al_min < 45 );
fprintf( 'al_min = %f\n', al_min );
vdisp( T_1 )

data = zeros( 2, 0 );
for al1 = 1:90;
    
    % make kone
    Ko = [];
    while( true );
        K_new = randn( dim, 1000 );
        K_new = normalizematrix( K_new, 'colnorm',2 );
        idx_ok = true( 1, size(K_new, 2) );
        for k = 1:size( v0, 2 );
            idx_ok = idx_ok & acosd( sum( K_new .* v0(:,k) ) ) < al1+al_min; end;
        
        K_new(:,~idx_ok) = [];
        Ko = [Ko K_new];
        if( size(Ko, 2) > 25*dim );
            break; end; end;
    Ko = Ko;
%     figure(2);
%     plotm( K, '.', 'funct','kone', 'hull',1, 'hold','on' );

    % compute behaviour
    it_max = 200;
    
    it = 0;
    while( true );
        it = it + 1;
        K_old = Ko;
        Ko = [];
        for j = 1:J;
            Ko = [Ko T_1{j}*K_old]; end;
        Ko = normalizematrix( Ko, 'colnorm',2 );
        iskone_check = iskone( Ko );
        if( ~iskone_check );
            flag = 'out';
            break; end;
        in_kone_check = in_kone( Ko, K_old );
        if( all(in_kone_check) );
            flag = 'in';
            break; end;
        for k = size( Ko, 2 ):-1:1;
            dist_k = sum( abs( Ko(:,1:k-1) - Ko(:,k) ), 1 );
            if( any(dist_k < 1e-12) );
                Ko(:,k) = []; end; end;
        nrm_K = polytopenorm( Ko, Ko, 'k', 'v',0 );  % fprintf( '\n' );
        idx_keep =  nrm_K(2,:) > .999;
        idx_rand = rand( 1, size(Ko,2) ) > 1;
        Ko = Ko(:,idx_keep | idx_rand);
        v0_dist = zeros( 1, size(Ko, 2) );
        for k = 1:size( v0, 2 );
            v0_dist = max( v0_dist, acosd(sum(v0(:,k) .* Ko, 1)) ); end;
        
        % if( all(v0_dist < al_min+1) );
        %     it = -it;
        %     flag = 'in';
        %     break;
        if( it == it_max );
             it = max(v0_dist);
             flag = '?';
             break; end;

%         figure(2);
%         clf;
%         vprintf( 'al = %f, flag = %s, it = %i\n',al, flag, it );
%         plotm( K, '.', 'funct','kone', 'hull',1 );
%         for k = 1:size( v0, 2 );
%             plotm( v0(:,k), 'r', 'arrow',1, 'hold','on' ); end;
%         axis equal
%         drawnow;

        end;



    % store result
    figure(1); clf;
    data(:,end+1) = [al1, it];
    plotm( data, '.' );
    drawnow;

    figure(2);
    clf;
    vprintf( 'al = %f, flag = %s, it = %i\n',al1, flag, it );
    plotm( Ko, 'r.-', 'funct','kone', 'hull',1, 'hold','on' );
    plotm( Ko, 'k.-', 'funct','kone', 'hull',1, 'hold','on' );
    %plotm( K, 'ro', 'funct','kone', 'hull',1, 'hold','on' );
    for k = 1:size( v0, 2 );
        plotm( v0(:,k), 'r', 'arrow',1, 'hold','on' ); end;
    axis equal
    alpha .5
    drawnow;

    if( isequal(flag, 'out') );
        break; end;

    end;
fprintf( '\nDone.\n' );



%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*AGROW,*SAGROW>
%#ok<*NBRAK,*NBRAK1,*NBRAK2>