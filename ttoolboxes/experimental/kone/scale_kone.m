function [ K ] = scale_kone( varargin );
% [ K ] = scale_kone( K, delta, [aux], [method] ) 
% Scales a kone, i.e. makes it wider/narrower
% Input:
%   K           the kone
%   delta       double, scaling factor, delta < 1 makes the kone narrows, delta > 1 makes the kone wider
%   method      string, the chosen method, default = 'default'
%   aux         auxiliary data, optionally used by some methods
%
% Output
%   K           a kone
%
% Methods:
%   'Et'            multiplies vertices of kone by matrix Et. This method is invertible
%   'minkowski'     takes a scaled minkowski sum. This method is not invertible
%   'additive'      adds a fixed vector to all vertrices of the kone. This method may be invertible
%                       aux-data: vector which to be added. If not given, it is calculated by `center_kone`
%
% See also: Et_kone, mult_kone
%
% Written by: tommsch, 2024-05-28

    idx = cellfun( 'isclass', varargin, 'char' ) | cellfun( 'isclass', varargin, 'string' );
    if( any(idx) );
        method = varargin{find(idx, 1)};
        varargin(idx) = [];
    else;
        method = 0; end;
    K = varargin{1};
    delta = varargin{2};
    varargin(1:2) = [];
    if( numel(varargin) == 1 );
        aux = varargin{1};
        varargin(1) = [];
    else;
        aux = []; end;

    assert( isempty(varargin) );
    method(method == '_') = [];
    method(method == '-') = [];
    switch lower( method );
        case {0,'et','multiplicative','mult'};
            if( isempty(aux) );
                [center, nullspace] = center_kone( K );
            else;
                center = aux;
                nullspace = nullspace_kone( center ); end;
            Et = Et_kone( delta, center, nullspace );
            K = Et*K;

        case {1,'default','inflate','deflate','minkowski'};
            K = normalizematrix( K, 'colnorm',2 );
            nrm = polytopenorm( K, K, 'k1', 'v',0 );
            idx = nrm(2,:) < 0.999999;
            K(:,idx) = [];
        
            if( delta > 0 );
                K = scale_kone_inflate( K, delta );
            else;
                K = scale_kone_deflate( K, delta); end;

        case {2,'additive','add','protasov'};
                K = scale_kone_additive( K, delta, aux );

        case {3,'additiverandom','addrand','protasovrand'};
                K = scale_kone_additive_random( K, delta, aux );
        otherwise;
            fatal_error; end;
    
        
end

function [ K_new ] = scale_kone_inflate( K, delta);
    [dim, N] = size( K );
    K_new = zeros( dim, N^2 );
    for i = 1:N
        K_new(:,(i-1)*N+1:i*N) = K(:,i) - (delta-1)*K;
    end
    K_new(:,~any(K_new,1)) = [];
    nrm_new = polytopenorm( K_new, K_new, 'k1', 'v',0 );
    idx_new = nrm_new(2,:) < 0.999999;
    K_new(:,idx_new) = [];
end       
        
function [ K_new ] = scale_kone_deflate( K, delta );
    assert( delta < 1 );
    [dim, N] = size( K );
    K_new = zeros( dim, N^2 );
    for i = 1:N
        K_i = K(:,i) - (delta-1)*K;
        %clf; hold on;
        %plotm( K, 'arrow',1 );
        %plotm( K_i, 'rx' );
        nrm_i = polytopenorm( K_i, K, 'k1', 'v',0 );
        [~,idx] = min( nrm_i(2,:) );
        K_new(:,i) = K_i(:,idx);
    end
    K_new(:,~any(K_new,1)) = [];
end

function [ K ] = scale_kone_additive( K, delta, center );
    K = normalizematrix( K, 'colnorm',2 );
    nrm = polytopenorm( K, K, 'k1', 'v',0 );
    idx = nrm(2,:) < 0.999999;
    K(:,idx) = [];
    if( isempty(center) );
        center = center_kone( K ); end;
    K = K - (delta-1)*center;
    K = normalizematrix( K, 'colnorm',2 );
end

function [ K ] = scale_kone_additive_random( K, delta, center );
    K = normalizematrix( K, 'colnorm',2 );
    
    nrm = polytopenorm( K, K, 'k1', 'v',0 );
    idx = nrm(2,:) < 0.999999;
    K(:,idx) = [];
    N = size( K, 2 );
    if( isempty(center) );
        center = center_kone( K ); end;
    center = repmat( center, [1 N] );
    delta = abs(randn( 1, N ))*delta/20 + (1+delta)/2;
    K =  K - (delta-1).*center;
    K = normalizematrix( K, 'colnorm',2 );
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
