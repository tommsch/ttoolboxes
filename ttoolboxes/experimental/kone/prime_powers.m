function [ P ] = prime_powers( M, n );
% (experimental) computes all prime powers (including 1) of M
% [ P ] = prime_powers( M, n );
%
% Note:
% - The set {M^1 M^2 M^3 M^5 M^7 M^11 ...} is computed up to power n
% - This function uses sum_of_primes which is not optimized for large values.
%   Thus this function works only for values of n up to ~1e6
%
% Written by: tommsch, 2024-10-30

% Changelog:    tommsch,    2024-10-30,     New function

    P = {M};
    for i = primes( n );  % 2:n;
        sop = sum_of_primes( i, false );
        P{i} = buildproduct_fast( P, sop );
    end
    idx = cellfun( 'isempty', P );
    P(idx) = [];
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
