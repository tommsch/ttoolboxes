fprintf( 'Test scripts for invariant kone algorithms' )

%%
% Initialize variables
condset( 'counter', 0 );
res = struct( [] );  % syntax to create an empty struct
dim = 3;
%%

% counter = max( 0, counter - 1 );
while( true );
    if( dim > 8 );
        dim = dim - 4; end;
    dim = dim + 1/(4*dim);
    counter = counter + 1  %#ok<NOPTS>
    rng( counter )
    [T, ~, T_nfo] = gallery_matrixset( 'rand', 'num_try',inf, 'dim',floor(dim), 'J',2, 'different', 'filter',{'vandergraft',true, 'unitary','no', 'finite','all'} );
    T = randomize_matrixset( T, 'sparse',.5 );
    T = randomize_matrixset( T, 'symmetric' );
    if( ~matrixinfo( T, 'primitive' ) );
        continue; end;
    if( isanyof(T_nfo.name, {'iota'}) );
        continue; end;
    % if( all(T_nfo.int) );
        % continue; end;

    res(end+1).counter = counter;  %#ok<SAGROW>
    idx = numel(res);

    res(idx).T = T;
    res(end).nfo_T = T_nfo;
    
    tic;
    [~, exact_nfo] = ipa( T, 'Kone', 'add','prune', 'recomputenorm',3, 'maxiter',100, 'maxvertexnum',5000, 'sym',0, 'end',ipa_callback_cleanup_nfo(2) );
    try; if( isanyof(exact_nfo.log.errorcode, {ipa_errorcode.MAXNUM_VERTEXREACHED, ipa_errorcode.MAXTIMEREACHED}) );  %#ok<TRYNC>
        exact_nfo.blockvar{1}.cyclictree.VV = [];
        exact_nfo.blockvar{1}.cyclictree.norm = [];
        exact_nfo.blockvar{1}.cyclictree.has_children = [];
        end; end;
    res(idx).exact_duration = toc;
    res(idx).exact_nfo = exact_nfo;
    
    if( isanyof( exact_nfo.log.errorcode, {ipa_errorcode.COMPLEXKONE,ipa_errorcode.NOINVARIANTKONE,ipa_errorcode.NOINVARIANTKONE_DUALTEST,ipa_errorcode.IS_NOT_A_KONE} ) );
        continue; end;
    idxi = cell( size(res(idx).exact_nfo.blockvar{1}.cyclictree.norm) );
    for i = 1:numel( idxi );
        idxi{i} = res(idx).exact_nfo.blockvar{1}.cyclictree.norm{i}(2,:) >= 1; end;
    [res(idx).exact_nfo.VV_bd, ~, exact_nrm_bd] = boundary_vertices( res(idx).exact_nfo.blockvar{1}.cyclictree.VV, 'k', idxi );



    tic;
    [~, et_nfo] = ipa( T, 'Kone', 'delta',1.01, 'recomputenorm',3, 'maxiter',30, 'invariantsubspace','none', 'maxvertexnum',3000, 'sym',0, 'end',@ipa_callback_cleanup_nfo );
    try; if( isanyof(et_nfo.log.errorcode, {ipa_errorcode.MAXNUM_VERTEXREACHED, ipa_errorcode.MAXTIMEREACHED}) );  %#ok<TRYNC>
        et_nfo.blockvar{1}.cyclictree.VV = [];
        et_nfo.blockvar{1}.cyclictree.norm = [];
        et_nfo.blockvar{1}.cyclictree.has_children = [];
        end; end;  
    res(idx).et_duration = toc;
    res(idx).et_nfo = et_nfo;
    res(idx).finished = true;


    idxi = cell( size(res(idx).et_nfo.blockvar{1}.cyclictree.norm) );
    for i = 1:numel( idxi );
        idxi{i} = res(idx).et_nfo.blockvar{1}.cyclictree.norm{i}(2,:) >= 1; end;
    [res(idx).et_nfo.VV_bd, ~, et_nrm_bd] = boundary_vertices( res(idx).et_nfo.blockvar{1}.cyclictree.VV, 'k', idxi );
  
        
    end;
%%
% Assess results
clc  %#ok<UNRCH>
tbl = cell( 0, 1 );
header = cell( 1, 0 );
header{end+1} = 'idx';                      idx_idx = numel( header );
header{end+1} = 'counter';                  idx_counter = numel( header );
header{end+1} = 'mat_name';                 idx_mat_name = numel( header );
header{end+1} = 'dim';                      idx_dim = numel( header );

header{end+1} = 'exact_duration';           idx_exact_duration = numel( header );
header{end+1} = 'exact_errorcode';          idx_exact_errorcode = numel( header );
header{end+1} = 'exact_errorstr';           idx_exact_errorstr = numel( header );
header{end+1} = 'exact_success';            idx_exact_success = numel( header );
header{end+1} = 'exact_nVV';                idx_exact_nVV = numel( header );
header{end+1} = 'et_duration';              idx_et_duration = numel( header );
header{end+1} = 'et_errorcode';             idx_et_errorcode = numel( header );
header{end+1} = 'et_errorstr';              idx_et_errorstr = numel( header );
header{end+1} = 'et_success';               idx_et_success = numel( header );
header{end+1} = 'et_nVV';                   idx_et_nVV = numel( header );

for idx = 1:numel( res )
    % fprintf( '%i/%i\n', idx, numel(res) );
    try;
        exact_errorcode = res(idx).exact_nfo.log.errorcode;  % .nfo_exact, .nfo_iter, .exact_nfo, 
        exact_errorstr = ipa_errorcode.get( exact_errorcode );
    catch me;
        exact_errorcode = nan;
        exact_errorstr = 'FAIL';
        end;
    try;
        et_errorcode = res(idx).et_nfo.log.errorcode;  % .et_nfo, .nfo_et
        et_errorstr = ipa_errorcode.get( et_errorcode );
    catch me;
        et_errorcode = nan;
        et_errorstr = 'FAIL';
        end;
    
    tbl{end+1,1} = [];  % increase size

    tbl{end,idx_idx} = idx;
    tbl{end,idx_counter} = res(idx).counter;
    tbl{end,idx_mat_name} = res(idx).nfo_T.name;
    tbl{end,idx_dim} = size( res(idx).T{1}, 1 );
    
    tbl{end,idx_exact_duration} =   res(idx).exact_duration;  % .exact_duration, iter_duration, duration_exact
    tbl{end,idx_exact_errorcode} =  exact_errorcode;
    tbl{end,idx_exact_errorstr} =   exact_errorstr;
    tbl{end,idx_exact_success} = tif( isanyof( {exact_errorcode}, {ipa_errorcode.VALIDATEUPPERBOUNDNORM, ipa_errorcode.NOERROR, ipa_errorcode.NOERROR_INVARIANTSUBSPACE} ), 'Y', ...
                                      isanyof( {exact_errorcode}, {ipa_errorcode.COMPLEXKONE, ipa_errorcode.NOINVARIANTKONE_DUALTEST, ipa_errorcode.IS_NOT_A_KONE, ipa_errorcode.NOINVARIANTKONE}), 'Z', ...
                                      isanyof( {exact_errorcode}, {ipa_errorcode.MAXNUM_VERTEXREACHED, ipa_errorcode.MAXTIMEREACHED, ipa_errorcode.MAXITERATION, ipa_errorcode.MAXTREETIMEREACHED}), 'N', ...
                                      '?' ...
                                    );

    % if( isanyof(tbl{end,idx_exact_success}, 'Y') );
    %     if( ~isfield(res(idx).exact_nfo, 'VV_bd') );
    %         idxi = cell( size(res(idx).exact_nfo.blockvar{1}.cyclictree.norm) );
    %         for i = 1:numel( idxi );
    %             idxi{i} = res(idx).exact_nfo.blockvar{1}.cyclictree.norm{i}(2,:) >= 1; end;
    %         [res(idx).exact_nfo.VV_bd, ~, nrm_bd] = boundary_vertices( res(idx).exact_nfo.blockvar{1}.cyclictree.VV, 'k', idxi );
    %         end;
    %     end;
    tbl{end,idx_exact_nVV} = 0;
    if( isfield( res(idx).exact_nfo, 'VV_bd' ) && ~isempty( res(idx).exact_nfo.VV_bd ) && ~isequal( res(idx).exact_nfo.VV_bd, 0 ) );
        tbl{end,idx_exact_nVV} = size( [res(idx).exact_nfo.VV_bd{:}], 2 ); end;


    if( isanyof( {et_errorcode}, {ipa_errorcode.VALIDATEUPPERBOUNDNORM, ipa_errorcode.NOERROR, ipa_errorcode.NOERROR_INVARIANTSUBSPACE}) ...
      );
        fasttest_et_correct = 'Y';
    elseif( isanyof( {et_errorcode}, {ipa_errorcode.MAXNUM_VERTEXREACHED, ipa_errorcode.MAXTIMEREACHED, ipa_errorcode.MAXITERATION, ipa_errorcode.MAXTREETIMEREACHED} ) ...
          );
        fasttest_et_correct = '?';
    else;
        fasttest_et_correct = 'N'; end;

    tbl{end,idx_et_duration} =  res(idx).et_duration;  % .duration_et, .et_duration, .nfo_et_duration
    tbl{end,idx_et_errorcode} = et_errorcode;
    tbl{end,idx_et_errorstr} =  et_errorstr;
    tbl{end,idx_et_success} = tif( isanyof( {et_errorcode}, {ipa_errorcode.VALIDATEUPPERBOUNDNORM, ipa_errorcode.NOERROR, ipa_errorcode.NOERROR_INVARIANTSUBSPACE} ), 'Y', ...
                                   isanyof( {et_errorcode}, {ipa_errorcode.COMPLEXKONE, ipa_errorcode.NOINVARIANTKONE_DUALTEST, ipa_errorcode.IS_NOT_A_KONE, ipa_errorcode.NOINVARIANTKONE}), 'Z', ...
                                   isanyof( {et_errorcode}, {ipa_errorcode.MAXNUM_VERTEXREACHED, ipa_errorcode.MAXTIMEREACHED, ipa_errorcode.MAXITERATION, ipa_errorcode.MAXTREETIMEREACHED}), 'N', ...
                                   '?' ...
                                 );
    % if( isanyof(tbl{end,idx_et_success}, 'Y') );
    %     if( ~isfield(res(idx).et_nfo, 'VV_bd') );
    %         idxi = cell( size(res(idx).et_nfo.blockvar{1}.cyclictree.norm) );
    %         for i = 1:numel( idxi );
    %             idxi{i} = res(idx).et_nfo.blockvar{1}.cyclictree.norm{i}(2,:) >= 1; end;
    %         [res(idx).et_nfo.VV_bd, ~, nrm_bd] = boundary_vertices( res(idx).et_nfo.blockvar{1}.cyclictree.VV, 'k', idxi );
    %         end;
    %     end;
    tbl{end,idx_et_nVV} = 0;
    if( isfield( res(idx).et_nfo, 'VV_bd' ) && ~isempty( res(idx).et_nfo.VV_bd ) && ~isequal( res(idx).et_nfo.VV_bd, 0 ) );
        tbl{end,idx_et_nVV} = size( [res(idx).et_nfo.VV_bd{:}], 2 ); end;
    
    
    end;

tbl2 = tbl;
% sort wrt dim
[~, dim_idx] = sort( [tbl2{:,idx_dim}] );
tbl2 = tbl2(dim_idx,:);


% reassign error code numbers
c = unique( [tbl2{:,idx_et_errorcode}] );
yt = [];
ytl = {};
for i = 1:numel( c );
    dim_idx = [tbl2{:,idx_et_errorcode}] == c(i);
    tbl2(dim_idx,idx_et_errorcode) = {i};
    yt(end+1) = i;
    ytl{end+1} = ipa_errorcode.get( c(i) );
end

tbl3 = cell2table( tbl2, 'VariableNames',header )


% plotm( cell2mat( tbl2(:,[idx_dim, idx_et_errorcode]) )', 'violin',[.1  .1], '.' )
% yticklabels( ytl )
% yticks( yt )
% axis equal

%%
% Statistics 
d = d + 1;
idx_dim_n = [tbl2{:,idx_dim}] == d;
sze = nnz( idx_dim_n );

exact_duration = median( [tbl2{idx_dim_n,idx_exact_duration}] );
exact_success = nnz( [tbl2{idx_dim_n,idx_exact_success}] == 'Y' ) / nnz( [tbl2{idx_dim_n,idx_exact_success}] ~= '?' & [tbl2{idx_dim_n,idx_exact_success}] ~= 'Z' ) * 100;
exact_nVV_val = [tbl2{idx_dim_n&[tbl2{:,idx_exact_success}] == 'Y',idx_exact_nVV}];


et_duration = median( [tbl2{idx_dim_n,idx_et_duration}] );
et_success = nnz( [tbl2{idx_dim_n,idx_et_success}] == 'Y' ) / nnz( [tbl2{idx_dim_n,idx_et_success}] ~= '?' & [tbl2{idx_dim_n,idx_et_success}] ~= 'Z' ) * 100;
et_nVV_val = [tbl2{idx_dim_n & [tbl2{:,idx_et_success}] == 'Y',idx_et_nVV}];

vprintf( ['%2i & %3i &' ...
          '%2.1f\\,s & %2.f\\%% & [$%2.f : %2.f\\pm %2.f : %2.f$] & ' ...
          '%2.1f\\,s & %2.f\\%%   & [$%2.f : %2.f\\pm %2.f : %2.f$] \\\\\n'], ...
        d,  sze, ...
        exact_duration, (exact_success),   min(exact_nVV_val, [], 'omitnan'), mean(exact_nVV_val, 'omitnan'), std(exact_nVV_val, 1, 'omitnan'), max(exact_nVV_val, [], 'omitnan'), ...
        et_duration,    round(et_success), min(et_nVV_val,    [], 'omitnan'), mean(et_nVV_val,    'omitnan'), std(et_nVV_val,    1, 'omitnan'), max(et_nVV_val,    [], 'omitnan') ...
       )


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
