fprintf( 'Test scripts for ternary 2x2 matrices' )

%%
counter = 0;
nfo_et = {};
nfo_exact = {};
res = struct( [] );  % syntax to create an empty struct
D = [0 1];
dim = 3;
J = 2;

%%

counter = max( 0, counter - 1 );
while( true );
    counter = counter + 1  %#ok<NOPTS>
    rng( counter )
    [T, num, nfo_enum] = matrixenum( dim, J, counter, D, 'kone' );
    %vdisp( T )
    if( num < counter );
        continue; end;
    if( numel(T) < 2 );
        continue; end;
    nfo_T = matrixinfo( T );
    if( ~all(nfo_T.vandergraft) );
        continue; end;
    TT = invariantsubspace( T, 'all' );
    if( numel(TT) >= 2 );
        continue; end;
    
    tic;
    [pd_ret, K, Ks, dmin] = primal_dual_kone( T );
    time_fasttest = toc;
    if( ~isfinite(pd_ret) || isequal(pd_ret, false) );
        continue; end;


    res(end+1).counter = counter;  %#ok<SAGROW>
    idx = numel(res);

    res(idx).T = T;
    res(end).nfo_T = nfo_T;
    res(idx).fasttest_duration = time_fasttest;
    res(idx).fasttest = pd_ret;
    
    tic;
    [~, nfo_exact] = ipa( T, 'Kone', 'add','prune', 'recomputenorm',3, 'maxiter',30, 'maxvertexnum',5000, 'sym',1, 'invariantsubspace',false );
    res(idx).duration_exact= toc;
    res(idx).nfo_exact = nfo_exact;
        
    end;

%%
T1old = [];  %#ok<UNRCH>
str = {};
for n = 1:numel( res )
    if( ~isfield(res(n).nfo_exact, 'log') );
        oo_str = '?';
    else;
        switch res(n).nfo_exact.log.errorcode;
            case {ipa_errorcode.NOERROR};
                oo = pruned_ordering( res(n).nfo_exact );
                if( numel(oo) == 0 );
                    'X';
                elseif( numel(oo) == 1 && ...
                    (isequal(oo, {1}) || isequal(oo, {2}) )...
                  );
                    continue; end;
                oo_str =  sprintf( '%s', latexprod(oo, 'var','', 'newline',false) );
            case {ipa_errorcode.NOINVARIANTKONE,ipa_errorcode.IS_NOT_A_KONE}
                oo_str = 'N';
                continue;
            case {ipa_errorcode.MAXITERATION,ipa_errorcode.MAXNUM_VERTEXREACHED};
                oo_str = '?';
            otherwise;
                oo_str = num2str( res(n).nfo_exact.log.errorcode ); end; end;
    if( ~isequal( T1old, res(n).T{1} ) );
        T1old = res(n).T{1};
        mat1_str = latexbmatrix( res(n).T{1}, 'begin','\tbsm{', 'end','}', 'newline',false, 'ws',false );
        mat1_str = strrep( mat1_str, '-1','\mn' );
        mat1_str = strrep( mat1_str, '0','\zr' );
        mat1_str = strrep( mat1_str, '1','\pl' );
        if( n ~= 1 );
            str{end+1} = sprintf( '\\multicolumn{2}{c|}{\\hrulefill} & ' );
            end;
        str{end+1} = sprintf( '\\multicolumn{2}{c|}{$A_1=%s$} &', mat1_str );
        end;
    mat2_str = latexbmatrix( res(n).T{2}, 'begin','\tbsm{','end','}', 'newline',false, 'ws',false );
    mat2_str = strrep( mat2_str, '-1','\mn' );
    mat2_str = strrep( mat2_str, '0','\zr' );
    mat2_str = strrep( mat2_str, '1','\pl' );
    TT = invariantsubspace( res(n).T, 'all' );
    inv_str = tif( numel(TT) == 2, '\im', '   ' );
    str{end+1} = sprintf( '$%s$%s &$%s$ &', mat2_str, inv_str, oo_str );
end

num_col = 3;
col = cell( 1, num_col );
rem = ceil( numel(str)/num_col )*num_col - numel(str);
for i = 1:rem;
    str{end+1} = '&&'; end;

col_len = numel(str)/num_col;
for i = 1:num_col;
    col{i} = str((i-1)*col_len+1:i*col_len);
    if( i == num_col );
        for j = 1:numel( col{i} );
            col{i}{j} = strrep( col{i}{j}, 'multicolumn{2}{c|}', 'multicolumn{2}{c}' );
            idx = find( col{i}{j} == '&', 1, 'last' );
            col{i}{j}(idx) = [];
            col{i}{j}(end+1:end+2) = '\\'; end; end;
    col{i} = strvcat( col{i} );
end;
tbl = [col{:}];
fprintf( '\n' )
for i = 1:size(tbl,1);
    fprintf( '%s\n', tbl(i,:) ); end
fprintf( '\n' )


function [ oo ] = pruned_ordering( nfo );
    oo = nfo.blockvar{1}.cyclictree.ordering;
    VV = [nfo.blockvar{1}.cyclictree.VV{:}];
    v0 = [nfo.blockvar{1}.cyclictree.v0{:}];
    nrm = polytopenorm( v0, VV, 'kone', 'output','ub', 'v',0 );
    idx_in = nrm < 1;
    oo(idx_in) = [];
    oo = uniquecell( oo );
end

%%
 %#ok<*NOSEL,*NOSEMI,*ALIGN>
