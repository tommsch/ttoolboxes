function [ ret, d_extreme, K, Ks ] = has_invariant_kone_approximate( M, varargin );

    [opt.verbose, varargin] = parsem( {'verbose','v'}, varargin, 1 );  % Verbose level
    parsem( varargin, 'test' );

    K = approximate_kone( M, 'verbose',opt.verbose-1 );
    Ks = approximate_kone( M, 'dual', 'verbose',opt.verbose-1 );
    d_extreme = nan;
    ret = 0;
    if( numel(K) < numel(Ks) );
        K1 = K;  % to have fewer iterations in the for loop
        K2 = Ks;
    else;
        K1 = Ks;
        K2 = K; end;

    sign = 0;
    for i = 1:size( K1, 2 );
        d = sum( K1(:,i) .* K2 );
        if( any( d > 1e-12 ) );
            d_extreme = inf;
            sign = 1;
            break;
        elseif( any( d < -1e-12 ) );
            d_extreme = -inf;
            sign = -1;
            break; end; end;

    if( sign == 0 );
        vprintf( 'Could not compute the relative orientantation of the kone and the dual kone of M.\n', 'imp',[1 opt.verbose], 'cpr','err' );
        ret = -1; 
        return; end;
    
    for i = 1:size( K1, 2 );
        d = sum( K1(:,i) .* K2, 1 );
        if( sign == 1 );
            d_extreme = min( d_extreme, min(d) );
            if( d_extreme < -1e-12 );
                return; end;
        elseif( sign == - 1 );
            d_extreme = max( d_extreme, max(d) );
            if( d_extreme > 1e-12 );
                return; end; end; end;

    ret = 1;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*NBRAK>
