function [ K, Et ] = mult_kone( t, K, center, nullspace );
% widens/shrinks a kone
% [ K, Et ] = mult_kone( t, K, center, nullspace );
%   K -> Et*K
%
% Written by: tommsch, 2024

    Et = Et_kone( t, center, nullspace );
    K = Et*K;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
