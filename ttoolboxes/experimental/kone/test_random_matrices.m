counter = 0;
nfo_et_i = {};
nfo_iter_i = {};
res = struct( [] );  % syntax to create an empty struct
D = [0 1];
dim = 3;
J = 2;
%%
counter = counter - 1;
while( rand < 0 );
    counter = counter + 1;
    %[T, num, mnfo] = matrixenum( dim, J, counter, D, 'kone' );
    %if( isempty(num) ); break; end;
    %if( ~isequal(num, couter) ); continue; end;
    [T, ~, mnfo] = gallery_matrixset( 'maxiter',500, 'dim',dim, 'J',J, 'filter',{'vandergraft','mandatory', 'normal','never', 'irreducible'}, 'different' );
    if( numel( T ) ~= 2 );
        continue; end;
    % if( all( [T{:}] >= 0 ) );
    %     continue; end;
    %TT = invariantsubspace( T, 'all', 'sym',2 );
    %if( numel(TT) > 1 );
    %    continue; end;
    
    [~, nfo_iter_i] = ipa( T, 'Kone', 'add','prune', 'recomputenorm',3, 'maxiter',30, 'maxvertexnum',5000, 'sym',0 );
    
    if( isanyof( nfo_iter_i.log.errorcode, {ipa_errorcode.COMPLEXKONE, ipa_errorcode.NOINVARIANTKONE,ipa_errorcode.NOINVARIANTKONE_DUALTEST} ) );
        continue; end;

    res(end+1).counter = counter;  %#ok<SAGROW>
    idx = numel( res );
    res(idx).T = T;
    res(idx).matrixinfo = mnfo;
    res(idx).nfo_iter = nfo_iter_i;
    if( nfo_iter_i.log.errorcode == ipa_errorcode.NOERROR );
        res(idx).minimal_polyhedral_kone = true;
        res(idx).kone_min = to_deg( nfo_iter_i ); end;

    [~, nfo_et_i] = ipa( T, 'Kone', 'delta',1.01, 'recomputenorm',3, 'maxiter',30, 'invariantsubspace','none', 'sym',0, 'maxvertexnum',5000 );
    res(idx).nfo_et = nfo_et_i;
    if( nfo_et_i.log.errorcode == ipa_errorcode.NOERROR );
        res(idx).kone_pair = true;
        res(idx).kone_et = to_deg( nfo_et_i ); end;


    
end



%%
clc
errorcode_all = [];
fprintf( '\\toprule\n' );
fprintf( '  Minimal & ordering                                        & Pair \\\\ \\hline\n' );
fprintf( '\\midrule\n' );
for n = 1:numel( res )
    
    if( ~isempty(res(n).nfo_iter) && numel(res(n).nfo_iter.blockvar) == 1 );
        errorcode = res(n).nfo_iter.log.errorcode;
        oo = latexprod( pruned_ordering(res(n).nfo_iter) );
    else;
        errorcode = ipa_errorcode.NOINVARIANTKONE;
        oo = '\{\}'; end;
    errorcode_str = evaluate_errorcode( errorcode );
    errorcode_all = [errorcode_all errorcode];  %#ok<AGROW>
    fprintf( '%5s     & ', errorcode_str );
    if( isanyof(errorcode_str, {'?'}) );
        oo = ''; end;
    str = sprintf( '%s', oo );
    fprintf( '$%45s$ & ', str );

    if( ~isempty(res(n).nfo_et) && numel(res(n).nfo_et.blockvar) == 1);
        errorcode = res(n).nfo_et.log.errorcode;
        oo = latexprod( pruned_ordering(res(n).nfo_et) );
    else;
        errorcode = ipa_errorcode.NOINVARIANTKONE;
        oo = '\{\}'; end;
    errorcode_str = evaluate_errorcode( errorcode );
    %errorcode_all = [errorcode_all errorcode];
    fprintf( '%5s   & ', errorcode_str );
    str = sprintf( '%s', oo );
    fprintf( '$%45s$ & ', str );

    fprintf( '\\\\' );
    fprintf( '\n' );
end
fprintf( '\\bottomrule\n' );
vprintf( '%% [ %v ] ', unique(errorcode_all) )

%% prune orderings
function oo = pruned_ordering( nfo );
    oo = nfo.blockvar{1}.cyclictree.ordering;
    VV = [nfo.blockvar{1}.cyclictree.VV{:}];
    v0 = [nfo.blockvar{1}.cyclictree.v0{:}];
    nrm = polytopenorm( v0, VV, 'kone', 'output','ub', 'v',0 );
    idx_in = nrm < 1;
    oo(idx_in) = [];
    oo = uniquecell( oo );
end

%%

function [ str ] = evaluate_errorcode( e );
    switch e;
        case {ipa_errorcode.VALIDATEUPPERBOUNDNORM};
            str = '$\delta$';
        case {ipa_errorcode.NOERROR};
            str = '$1$';
        case {ipa_errorcode.NOINVARIANTKONE, ipa_errorcode.COMPLEXKONE, ipa_errorcode.IS_NOT_A_KONE};
            str = '$0$';
        case {ipa_errorcode.STALENORM,ipa_errorcode.MAXITERATION,ipa_errorcode.MAXNUM_VERTEXREACHED};
            str = '?';
        otherwise;
            fatal_error; end;
end

function al = to_deg( nfo );
    pts = [nfo.blockvar{1}.cyclictree.VV{:}];
    al = rad2deg( atan2( pts(2,:), pts(1,:) ) );
    al = [min(al) max(al)];
end



%%
 %#ok<*NOSEL,*NOSEMI,*ALIGN>
