function [ r ] = rho_kone( M );
% [ r ] = rho_kone( M )
% (experimental) Computes the kone spectral radius of matrix M
%
% Note: Currently we have no theoretical background whether the returned number has anything todo with a kone-spectral-radius
%
% Written by: tommsch, 2024
    
    M = tocell( M );
    r = zeros( size(M) );
    if( issym(M{1}) );
        r = sym( r ); end;
    for j = 1:numel( M );
        if( size(M{j}, 1) == 1 );
            r(j) = 0;
        elseif( any(~isfinite(M{j}(:))) );
            r(j) = nan;
            return;
        else;
            rj = abs( eig(M{j}) );
            rj = sort( rj );
            r(j) = rj(end-1)/rj(end); end; end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>