function [ ret, K, Ks, dmin ] = primal_dual_kone( M, varargin );
% tests whether a set of matrices does not have an invariant kone
%
% Input:
%   M       cell array of matrices
%
% Output:
%   ret     false if there does not exist a common invariant kone
%           true if it could not be proven that there does not exist a common invariant kone
%           nan if something bad happend
%   K       the computed primal kone
%   Ks      the computed dual kone
%   dmin    vector of floats, the minimum value of the inner products betwen K and Ks
%           dmin(end) is less then zero if there does not exist a common invariant kone
%
% Written by: tommsch, 2024-11-22

% Changelog:    tommsch,    2024-11-22,     New function `primal_dual_kone`

    [opt.maxvertex, varargin] = parsem( {'maxvertex','maxvertex_num'}, varargin, [1e4 1e6]);
    [opt.maxiter, varargin] = parsem( {'maxiter'}, varargin, [18 22] );
    [opt.quot, varargin] = parsem( {'quot','minquot','quot_termination'}, varargin, 0.1 );
    parsem( varargin, 'test' );
    

    [v0, v0s] = leadingeigenvector( M{1} );
    if( numel(v0) > 1 );
        K = [];
        Ks = [];
        ret = nan;
        dmin = [];
        return; end;
    v0 = v0{1};
    v0 = v0/norm( v0 );

    v0s = v0s{1};
    v0s = v0s/norm( v0s );

    J = numel( M );
    K = M{2}*v0;
    if( rank(K) < rank(v0) );
        K = v0; end;
    Ks = M{2}'*v0s;
    if( rank(Ks) < rank(v0s) );
        Ks = v0s; end;
    Kj = cell( 1, J );
    Ksj = cell( 1, J );
    dmin = [];
    alpha_min = [];
    round = 0;
    while( true );
        round = round + 1;
        if( numel( K ) >= min( opt.maxvertex ) || ...
            round >= min( opt.maxiter ) ...
           );
            if( dmin(end) < 1e-12 || ...
                isempty( opt.quot ) || ...
                numel( opt.maxvertex ) == 2 && numel( K ) >= max( opt.maxvertex ) || ...
                numel( opt.maxiter ) == 2 && round >= max( opt.maxiter ) || ...
                numel( dmin ) > 2 && dmin(end)/dmin(end-1) > opt.quot ...
              );
            break; end; end;
            [ret, dmin(end+1)] = check_dual_thingy( [K v0], [Ks v0s] );  %#ok<AGROW>
            alpha_min(end+1) = acosd( dmin(end) );  %#ok<AGROW>
        
        if( ~ret );
            return; end;
        for j = 1:numel( M );
            Kj{j} = M{j}*K;
            Ksj{j} = M{j}'*Ks; end;
        K = [K [Kj{:}]];  %#ok<AGROW>
        Ks = [Ks [Ksj{:}]];  %#ok<AGROW>
        
        nrmK = sum( K.^2, 1 ).^(1/2);
        nrmKs = sum( Ks.^2, 1 ).^(1/2);
        K = K./nrmK;
        Ks = Ks./nrmKs;
        K(:,nrmK == 0) = [];
        Ks(:,nrmKs == 0) = [];
        K = unique( K.', 'rows' ).';
        Ks = unique( Ks.', 'rows' ).';
        end;  
end

function [ ret, dmin ] = check_dual_thingy( K, Ks );
    dret = zeros( 1, size(K, 2) );
    parfor i = 1:size( K, 2 );
        d = sum( K(:,i) .* Ks, 1 );
        dret(i) = min( d );
        end;
    dmin = min( inf, min( dret ) );
    ret = all( dmin >= 0 );
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
