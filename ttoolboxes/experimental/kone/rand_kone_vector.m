function [ vec ] = rand_kone_vector( K, where, verbose );
% Generates a random vector in a kone
% [ vec ] = rand_kone_vector( K, where, verbose );
%
% Input:
%   K           matrix of column vectors, the kone
%   where       string,
%                   'anywhere':     anywhere
%                   'halfspace':    a vector which is on the same side of the hyperplane as defined by center_kone( K )
%                   'inside':       a vector inside of K
%                   'outside':      a vector outside of K
%
% Output:
%   vec         column vector, some vector in K
%
% Note:
%   The algorithm can fail.
%
% Written by: tommsch, 2024

    K = normalizematrix( K, 'colnorm',2 );
    if( nargin <= 1 || isempty(where) );
        where = randi( [0 3] ); end;
    if( nargin <= 2 || isempty(verbose) );
        verbose = 1; end;
    dim = size( K , 1 );

    if( size(K, 2) == 0 );
        vec = zeros( dim, 0 );
        return; end;

    vec = randn( dim, 1 );
    vec = vec/norm( vec );
    center = center_kone( K, 'verbose',0 );
    try;
        max_rounds = 100;
        for round = 1:max_rounds;
            if( verbose >= 2 );
                clf; hold on;
                plotm( K, 'r', 'funct','kone', 'hull',1 );
                plotm( center, 'r', 'arrow',1.5 );
                plotm( vec, 'arrow',1.5 );
                alpha .5
                axis equal
                end;
                
            switch where;
                case {0,'','any','anywhere'};
                    break;

                case {1,'h','halfspace'};
                    if( dot(center, vec) >= 0 );
                        break; end;
                    angles = sum( vec .* center, 1 );
                    [~, idx] = min( angles );
                    vec = vec + rand*K(:,idx);

                case {2,'i','in','inside'};
                    angle = dot( center, vec );
                    if( angle >= 0 );
                        nrm = polytopenorm( vec, K, 'k0', 'output','lb' ); 
                    else;
                        nrm = 0; end;
                    if( angle >= 0 && nrm <= 1 );
                        break; end;
                    angles = sum( vec .* K, 1 );
                    [~, idx] = min( angles );
                    vec = vec + rand*K(:,idx);

                case {3,'o','out','outside'};
                    angle = dot( center, vec );
                    if( angle >= 0 );
                        nrm = polytopenorm( vec, K, 'k0', 'output','ub' );
                    else;
                        nrm = inf; end;
                    if( angle >= 0 && nrm >= 1 );
                        break; end;
                    angles = sum( vec .* K, 1 );
                    [~, idx] = min( angles );
                    if( angle < 0 );
                        vec = vec + rand*K(:,idx);
                    else;
                        vec = vec - rand*K(:,idx); end;

                otherwise;
                    error( 'rand_kone_vector:where', 'Value of ''where'' is not supported.' ); end; end;
            
            if( round == max_rounds );
                vec = nan( dim, 1 ); end;
            
    catch me;  %#ok<NASGU>
        warning( 'rand_kone_vector:fail', 'Computation of a vector for the given kone failed.' );
        end;  % do nothing

    vec = vec / norm( vec );
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
