function [ K ] = interpolate_kone( K, num_points, num_rounds, onlyborder );
% generates point inside a kone. Note: This algorithm is not very sophisticated
% [ K ] = interpolate_kone( K, [num_points], [num_rounds], [onlyborder] );
%
% Input:
%   K               matrix of column vectors, the kone
%   num_points      default = 100, integer, number of random points to generate
%   num_rounds      default = 5, integer, how many iterations shall be done
%   onlyborder      default = false, bool, if true uses mostly points on the border to generate new points
%
% Output:
%   K               matrix of column vectors, the same kone with vertices in the interior
%
% Written by: tommsch, 2024


    if( nargin <= 1 || isempty( num_points ) );
        num_points = 100; end;
    if( nargin <= 2 || isempty( num_rounds ) );
        num_rounds = 5; end;
    if( nargin <= 3 || isempty( onlyborder ) );
        onlyborder = false; end;

    dim = size( K, 1 );
    nrm = ( sum( abs( K ).^2, 1 ) ).^(1/2);
    K = K./nrm;
    randpoints = randn( dim, num_points );
    nrm = ( sum( abs( randpoints ).^2, 1 ) ).^(1/2);
    randpoints = randpoints./nrm;
    in_idx = in_kone_prune( randpoints, K ) > 0;
    randpoints(:,~in_idx) = [];
    K = [K randpoints];
    for rounds = 1:num_rounds;
        if( size(K, 2) > num_points*50 );
            break; end;
        al = rand;
        idx = randperm( size(K, 2) );
        K = [K (al*K + (1-al)*K(:,idx)) (K + K)/2];  %#ok<AGROW>
        if( onlyborder );
            in_nrm = polytopenorm( K, K, 'k', 'output','ub', 'v',0 );
            in_idx = in_nrm < 0.99999;
            %in_idx = in_kone_prune( K, K ) > 0;
            K(:,in_idx) = []; end;
    end;
    nrm = ( sum( abs( K ).^2, 1 ) ).^(1/2);
    K = K./nrm;

end

function [ flag ] = in_kone_prune( pts, K );
    np = size( pts, 2 );
    flag = false( 1, np );
    for i = 1:np
        Ki = K;
        while( true );
            %co = Ki\pts(:,i);
            [~, co] = evalc( 'lsqnonneg( Ki, pts(:,i) );' );
            idx = co < 0;
            if( any(idx) );
                Ki(:,idx) = [];
            else;
                break; end; end;
        residual = norm( Ki*co - pts(:,i) );
        flag(i) = residual < 1e-9; end;
end

function dummy; end  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
