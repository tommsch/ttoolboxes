%% Various ways to compute a kone-spectral-radius
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Written by: tommsch, 2024
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Generate random matrix


dim = 3;
Ao_ori142321g = gallery_matrix( 'poslead', 'dim',dim );

%al = 0.1; Ao = [1 0 0;0 sin(al) cos(al); 0 -cos(al) sin(al)];  % rho^3 = t
% A0=[1 2 2;2 5 6;2 6 9]; rho^3 = 0.071545851528384, rho^2 = 1

Ao = Ao_ori142321g;
A = Ao;

%% Computation of \rho_K^4
clc
A = Ao;
r = rho_kone( A );
v0 = leadingeigenvector( A, 'nocell' );
Et = Et_kone( 1/r, v0 )
At = Et * A;
%K = rand_kone( dim, [], v0 );
K = randn( dim, 2*dim )
for i = 1:10000;
    Ait = At^randi( [2 50] );
    K = Ait*K;
    K = normalizematrix( K, 'colnorm',2 );
end;
v0
K

%% Computation of \rho_K^3
%%%%%%%%%%%%%%%%%%%%%%%%%%
A = Ao/1000;
center = leadingeigenvector( A, 'nocell' );
Ko = center; 
Ko = extravertex( center, 'case','k', 'threshold',.5 );
idx = in_kone( Ko, Ko );
Ko = Ko(:,idx);

tmin = -1e3;
tmax = 1e3;
while( true )
    t3 = (tmin + tmax)/2;
    if( tmin/tmax > 0.99 );
        break; end;
    Et = Et_kone( t3, center );
    At = Et*A;
    K = Ko;
    while( true );
        K = At*K;
        nrms = sum( K.^2, 1 ).^(1/2);
        K = K./nrms;
        if( any( sum(center .* K, 1) < 1e-4 ) );
            tmax = t3;
            break;
        elseif( all(sum((K - center).^2, 1 ) < 1e-4) );
            tmin = t3;
            break; end;

        %         clf; hold on;
        % plotm( K, 'funct','kone', 'hull',1 );
        % plotm( center, 'arrow',1.2, 'r', 'linewidth',5 );
        % axis equal
        % title( t3 )
        % %view( center )

    end
        % clf; hold on;
        % plotm( K, 'funct','kone', 'hull',1 );
        % plotm( center, 'arrow',1.2, 'r', 'linewidth',5 );
        % axis equal
        % title( t3 )
        % %view( center )
end
rho3 = 1/t3;
rho_kone(A)
%% Computation of \rho_K^3 - wrong way
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
A = A/rho( A );

tmin = 1e-3;
tmax = 1e3;
center = leadingeigenvector( A, 'nocell' );
%center = center + 0.1*randn( dim, 1 );
center = center/norm( center );

while( true );
    t = (tmin + tmax)/2;
    if( tmin/tmax > 0.99999 );
        break; end;
    Et = Et_kone( t, center );
    [r, nfo] = ipa( Et*A, 'kone', 'v',1 );
    if( nfo.log.errorcode < 0 );
        tmin = t;
    else;
        tmax = t; end;
    disp( t )
    %1./abs(eig(A))
end

t
1./abs(eig(A))
eig( Et*A )

%% Computation of \rho_K^2
% compute kone spectral radius via singular values of matrix 
%A2 = A/rho( A )
v0 = leadingeigenvector( A ); v0 = v0{1};
%v0 = v0 + 0.1*randn( dim, 1 );
%v0 = v0/norm( v0 );
%v0 = randn( dim, 1 ); v0 = v0/norm( v0 );
ns = nullspace_kone( v0 );

T_1 = [];
T_s = [];

%s = 5; As = Et_kone( s, v0, ns )*A;
%s = 2; As = A;
for tr_2 = -3:1e-4:3;
%for tr = 7.14e-5:1e-9:7.2e-5;
%for ti = -r:r/100:r;
    %t = tr + 1i*ti;
    t = tr_2;
    Et2 = Et_kone( t, v0, ns );
    A_1t = A2 - Et2;
    %A_st = As - Et;
    sing_meas_1 = norm( A_1t )/cond( A_1t );
    T_1(:,end+1) = [t;sing_meas_1]; %#ok<SAGROW>
%    end;
    end;

eig(A)
clf; hold on;
if( isreal(T_1) );
    plotm( T_1 ,'b.', 'semilogy' );
    %plotm( T_s ,'r.', 'semilogy' );
else;
    figure( 1 );
    plotm( [real(T_1(1,:)); imag(T_1(1,:))], 'Color',T_1(2,:) )
    figure( 2 );
    plotm( [real(T_s(1,:)); imag(T_s(1,:))], 'Color',T_s(2,:) )
    colorbar; end;



%% Computation of \rho_K^1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%[K, c, n] = rand_kone( dim );
%A = A/rho( A );
A = Ao
rk = eig( A ); rk = min(abs(rk));
c = leadingeigenvector( A, 'nocell' );
ns = nullspace_kone( c );
Ko = rand_kone( dim, [], c );

krhoA = [];
krhoAt = [];
for i = 1:1:100
    ex = i;
    ext = rk^-ex;
    Et = Et_kone( ext, c, ns );
    A_1t = Et * A^ex;
    %plotm( K, 'g', 'funct','kone', 'hull',1 );
    %plotm( A*K, 'b--', 'funct','kone', 'hull',1, 'hold','on' );
    %plotm( At*K, 'r', 'funct','kone', 'hull',1, 'hold','on' );
    %plotm( A^ex*K, 'b', 'funct','kone', 'hull',1, 'hold','on' );
    %krhoA(end+1) = polytopenorm( A^ex, K, c, 'matrix', 'k7', 'output','ub', 'v',0 ).^(1/ex);  %#ok<SAGROW>
    krhoAt(end+1) = polytopenorm( A_1t^ex, Ko, c, 'matrix', 'k7', 'output','ub', 'v',0 ).^(1/ex);  %#ok<SAGROW>
    %krhoA(end+1) = norm_kone( {A^ex}, K, c, ns )^(1/ex);  %#ok<SAGROW>
    %krhoAt(end+1) = (1/ext*norm_kone( {At}, K, c, ns ))^(1/ex);  %#ok<SAGROW>
    %semilogy( [krhoA; krhoAt]' )
    semilogy( krhoAt' )
    drawnow
    %pause
end
vprintf( 'eig(A) = %v\n', sort(abs(eig(A))) )




function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*SUSENS>  % unused variable
%#ok<*ASGLU>  % unused variable
%#ok<*NASGU>  % unused variable
%#ok<*NOPTS>  % semicolon at end
%#ok<*NBRAK>
