function [ v ] = make_good_orientation( v, K );
    cellflag = iscell( v );
    if( ~cellflag );
        v = num2cell( v, 1 ); end;
    if( iscell(K) );
        K = [K{:}]; end;
    for n = 1:numel( v );
        if( isempty(K) );
            continue; end;
        dots = sum( v{n} .* K ) > 0;  % this test is too restrictive but simple to implement. For the time being we try whether it works.
        if( all(dots) );
            % do nothing
        elseif( all(~dots) );
            v{n} = -v{n};
        else;
            error( 'ipa:kone', 'Set of matrices most likely has no invariant kone.' ); end;
        K = [K v]; end;  %#ok<AGROW>
    if( ~cellflag );
        v = [v{:}]; end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
