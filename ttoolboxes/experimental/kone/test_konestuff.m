
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% does the kone norm depend also on the kone, or only on the center
clc
dim = 3;
A = gallery_matrixset( 'J',2, 'poslead', 'dim',dim, 'invariantsubspace',0 );
vdisp(A)
c = findsmp( A, 'kone' )
Pi = buildproduct_fast( A, c{1} );
v0 = leadingeigenvector( Pi );




%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% triangle inequality for vector kone norm
while( true );
    %clc
    ct = ct + 1
    rng( ct ) 
    method = ['k' rand_oneof( {'0' '2' '3' '4' '5'} )];
    method = 'k6'
    dim = randi( [2 4] );  %dim = 2;
    N = dim + randi( [0 2*dim] );
    Ko = rand_kone( dim, N );  %K = [1 0;0 1]';
    dim = size( Ko, 1 );
    %center = [20 1]'; center = center/norm( center );
    v1 = rand_kone_vector( Ko ); %v1 = [-30 1]'; v1 = v1/norm( v1 );
    v2 = rand_kone_vector( Ko );
    v = v1 + v2;
    n = polytopenorm( v,  Ko, method, 'output',2 );
    n1 = polytopenorm( v1, Ko, method, 'output',2 );
    n2 = polytopenorm( v2, Ko, method, 'output',2 );
    clc
    flag = n <= n1 + n2;
    str = sprintf( 'n = %f %s %f = %f + %f = n1 + n2\n', n, tif(flag, '<=', '>'), n1 + n2, n1, n2 );
    vprintf( ' \b\n%s\n', str, 'cpr',tif(flag, [.1 .7 .1],'red') )
    if( ~flag );
        break; end;
end

% clf; hold on;
% plotm( K, 'funct','kone', 'hull',1, 'r' );
% plotm( center, 'r', 'arrow',1 );
% plotm( v1, 'b', 'arrow',1 );
% plotm( v2, 'b', 'arrow',1 );
% plotm( [zeros(dim,1) v1 v], '-b' );
% plotm( v, 'b', 'arrow',1 );
% title( str, 'Color',tif(flag, [.1 .7 .1],'red') );
% axis equal



%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
% tests the triangle inequality for the kone norm
while( true );
    dim = 5;
    Ko = rand_kone( dim );
    while( true );
        p1 = randn( dim, 1 );
        if( all( sum(p1.*Ko, 1) > 0 ) );
            break; end; end;
    while( true );
        p2 = randn( dim, 1 );
        if( all( sum(p2.*Ko, 1) > 0 ) );
            break; end; end;

    nrm_1 = norm_kone( p1, Ko );
    nrm_2 = norm_kone( p2, Ko );
    nrm_sum = norm_kone( [p2+p1], Ko );
    flag = nrm_sum <= nrm_1 + nrm_2;
    fprintf( '%f = ||p1 + p2|| %s ||p1|| + ||p2|| = %f + %f = %f\n', nrm_sum, tif(flag, '<=', '>=' ), nrm_1, nrm_2, nrm_1+nrm_2 )
    if( ~flag || false );
        break; end; end;
drawnow;
plotm( Ko, 'funct','kone', 'hull',1 )
plotm( Ko, 'funct','kone', 'linewidth',2, 'k', 'hold','on' );
plotm( p1, 'arrow',1, 'b', 'linewidth',2, 'hold','on' )
plotm( p2, 'arrow',1, 'b', 'linewidth',2, 'hold','on' )
plotm( p1+p2, 'arrow',1, 'g', 'linewidth',2, 'hold','on' )
alpha .5


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% tests submultiplicativity of old kone matrix norm

while( true );
    dim = 2;
    [Ko, center, ns] = rand_kone( dim );
    while( true );
        A = gallery_matrix( 'dim',dim, 'poslead', 'v',0 );
        vA = leadingeigenvector( A );
        if( numel(vA) > 1 );
            continue; end;
        vA = vA{1};
        if( in_kone( Ko, vA ) );
            break; end; end;
    
    while( true );
        B = gallery_matrix( 'dim',dim, 'poslead', 'v',0 );
        vB = leadingeigenvector( B );
        if( numel(vB) > 1 );
            continue; end;
        vB = vB{1};
        if( in_kone( Ko, vB ) );
            break; end; end;
    
    nrmAB = norm_kone( {A*B}, Ko, center, ns );
    nrmA = norm_kone( {A}, Ko, center, ns );
    nrmB = norm_kone( {B}, Ko, center, ns );
    if( nrmAB < inf );
        break; end; end;
flag = nrmAB <= nrmA * nrmB;
fprintf( '%f = ||A*B|| %s ||A|| * ||B|| = %f * %f = %f\n', nrmAB, tif(flag, '<=', '>=' ), nrmA, nrmB, nrmA*nrmB )






function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*SUSENS>  % unused variable
%#ok<*ASGLU>  % unused variable
%#ok<*NASGU>  % unused variable
%#ok<*NOPTS>  % semicolon at end
%#ok<*NBRAK>
%#ok<*AGROW>
