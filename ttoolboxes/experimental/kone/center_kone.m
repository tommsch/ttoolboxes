function [ center, nullspace, iter ] = center_kone( varargin );
% [ center, nullspace, iter ] = center_kone( VV, [VVs], [options] );
% computes a vector which can serve as the center of the kone.
% Depending on the used  method, the vector has different properties
%
% Input:
%   VV          dim x N array  OR cell array thereof vertices of the kone(s)
%   VVs         dim x N array  OR cell array thereof vertices of the dual kone(s)
%
% Options:
%   method      default = 'halfone', see below for available methods
%   verbose     optional, default = 1, verbosity level
%   symflag     optional, default = issym(VV), if true, the center is returned as sym
%
% Output:
%   center      dim x 1 vector, a vector which can serve as the center of the kone.
%   nullspace   vectors orthogonal to center, making a bases of RR^dim
%   iter        integer, number of iterations spent in an lp/sdp program
%
% Notes:
%   The function currently needs the Gurobi solver
%   The function internally uses floating point computations, even when the input is symbolic
%
% Method description:
%   int           vector is in the interior of the kone 0: no, 1: on the boundary, 2: in the interior, 3: really good in the interior
%   halfpl        halfplane defined by the vector does not cut through the kone
%   toc           performance
%   fail          may totally fail sometimes
%   deter         algorithm is deterministic across runs (not releases)
%   description   brief description of the algorithm
%
%  | method        |  int  |  hp   |  toc  | fails | deter |  description                                                                          |
%  |---------------|-------|-------|-------|-------|-----------------------------------------------------------------------------------------------|
%  | 0/sdp2        |   0   |   1   | 17.5s |  no   |  yes  | solves an sdp program and tries to minimize some objective                            |
%  | 1/sdp         |   1   |   1   | 17.8s |  no   |  yes  | solves an sdp program and tries to minimize some objective                            |
%  | 2/lp          |   0   |   1   | 46.5s |  no   |  yes  | solves an lp program                                                                  |
%  | 3/mean        |   2   |   0   |  0.1s |  no   |  yes  | takes the mean of all vertices                                                        |
%  | 4/median      |   2   |   0   |  0.1s |  no   |  yes  | takes the median of all vertices                                                      |
%  | 5/random      |   1   |   0   |  0.4s |  no   |  no   | tries random normal vectors                                                           |
%  | 6/randsamp    |   0   |   0   |  inf  |  yes  |  no   | samples random points in the dual kone                                                |
%  | 7/dual        |   0   |   0   |  25   |  no   |       | computes a valid vector in the dual kone                                              |
%  | 8/distsearch  |   1?  |   1   |   0   |  no   |  no   | greedy search algorithm combined with a fix to produce valid halfplanes               | 
%  | 9/sdpiter     |   1?  |   0   |   ?   |  ?    |  no   | computes a sequences of centers of kones, each kone contained in the former one       |
%  | 10/halfone    |   1   |   1   |   ?   |  yes  |  yes  | searches a normal in a smaller kone, whose normal plane is w.r.t to the original kone |
%  | 11/multiple   |                                       | similar to lp, but accepts multiple kones and dual kones
%  | deterministic |                               |       | defaults to some deterministic method, the chosen method may change between releases  |
%  | best          |                               |       | defaults to some method, which works in most cases, the chosen method may change between releases, the method may not be deterministic |
%
% See also: nullspace_kone, Et_kone, scale_kone
%
% Written by: tommsch, 2024-05-27,

%               tommsch,    2024-09-06,     Bugfix for method 'halfone'
% Changelog:    tommsch,    2024-11-19,     New method 'multiple' acceptings multiple kones and dual kones
%               tommsch,    2025-01-10,     No plot output anymore for higher verbose levels

    [VV, VVs, opt] = parse_input( varargin{:} );
    [VV, VVs] = normalize_VV( VV, VVs, opt );
    if( isempty(VV) );
        center = [];
        return; end;

    for m_ = opt.method; m = m_{1};
        m(m == '_') = [];
        switch lower( m );
            case { 0,'sdp2'};                    [center, iter] = center_kone_sdp2( VV );
            case { 1,'sdp'};                     [center, iter] = center_kone_sdp( VV );
            case { 2,'lp'};                      [center, iter] = center_kone_lp( VV );    
            case { 3,'mean'};                    [center, iter] = center_kone_mean( VV );
            case { 4,'median'};                  [center, iter] = center_kone_median( VV );
            case { 5,'rand','random'};           [center, iter] = center_kone_random( VV );
            case { 6,'randsamp','samp'};         [center, iter] = center_kone_randsamp( VV );
            case { 7,'dual','duallp'};           [center, iter] = center_kone_dual( VV );
            case { 8,'dist','distsearch'};       [center, iter] = center_kone_distsearch( VV );
            case { 9,'sdp2iter'};                [center, iter] = center_kone_sdp2iter( VV );
            case {10,'halfone','deterministic','default','def'};
                                                 [center, iter] = center_kone_halfone( VV, opt );
            case { 11,'multiple','mult'};        [center, iter] = center_kone_multiple( VV, VVs );    
            case {'empty'};                      [center, iter] = center_empty( VV );
            otherwise;      
                error( 'center_of_input:method', 'Wrong method given.' ); end;
            
        center = normalize_center( center, opt );
        check = assess_center( center, VV );
        if( ~all(check.finite) );
            continue; end;
        if( all(check.in_kone) && all(check.normal_plane) );
            break; end; end;

    information_output( VV, check, m, opt );
    
    if( nargout >= 2 );
        nullspace = nullspace_kone( center ); end;

end

function [ VV, VVs, opt ] = parse_input( varargin );
    VV = double( varargin{1} );
    varargin(1) = [];
    
    [opt.verbose, varargin] = parsem( {'verbose','v'}, varargin, 1 );
    [opt.symflag, varargin] = parsem( {'sym','symflag'}, varargin, 'default',issym(VV) );
    [opt.normalize, varargin] = parsem( {'normalize'}, varargin, 'default',true );

    if( numel( varargin ) >= 1 && ...
        (iscell( varargin{1} ) || ~isstring( varargin{1} ) && ~ischar( varargin{1} ) && ~isscalar( varargin{1} ) ) ...
      );
        VVs = varargin{1};
        varargin(1) = [];
    else;
        VVs = {}; end;

    if( numel(varargin) == 0 || isempty(varargin{1}) );
        if( numel(varargin) >= 1 );
            varargin(1) = []; end;
        if( isempty(VV) );
            opt.method = {'empty'};
        elseif( isempty(VVs) );
            assert( has_gurobi(), 'This function currently needs the gurobi solver.' );
            opt.method = {'halfone', 'median', 'mean', 'lp', 'sdp', 'sdp2', 'dist', 'randsamp', 'randsamp', 'random', 'random'};
        else;
            assert( has_gurobi(), 'This function currently needs the gurobi solver.' );
            opt.method = {'multiple'}; end;
    else;
        opt.method = varargin{1};
        varargin(1) = []; end;

    parsem( varargin, 'test' );

    if( isequal(opt.method, 'deterministic') );
        opt.method = {'halfone', 'median', 'mean', 'lp', 'sdp', 'sdp2'}; end;

    opt.method = tocell( opt.method );
    VV = tocell( VV );
    VVs = tocell( VVs );
    opt.dim = size( VV{1}, 1 );
end


function [ VV, VVs ] = normalize_VV( VV, VVs, opt );
    if( ~opt.normalize );
        return; end;
    for n = 1:numel( VV );
        VV{n} = normalize_VV_worker( VV{n} ); end;
    for m = 1:numel( VVs );
        VVs{m} = normalize_VV_worker( VVs{m} ); end;
end

function [ K ] = normalize_VV_worker( K );
    if( norm( imag(K) ) < 1e-12*max( size(K) ) );
        K = real( K ); end;
    nrm = sqrt( sum( abs(K).^2, 1 ) );
    K = K./nrm;
    if( issym(K) );
        epsilon = 0;
    else;
        epsilon = 1e-12; end;
    K(:,isAlways( nrm <= epsilon, 'Unknown','true' )) = []; 
end

function [ center ] = normalize_center( center, opt );
    if( opt.symflag );
        center = sym( center ); end;
    center = double( center );
    center = center/norm( center );
    if( norm( imag( center ) ) < eps( norm(center) ) * max( size(center) ) );
        center = real( center ); end;
end

function [ check ] = assess_center( center, VV );
    check.in_kone = nan;
    check.normal_plane = nan;
    check.finite = isfinite( center );
    if( ~all( check.finite ) );
        return; end;    
    for n = 1:numel( VV );
        check.normal_plane(n) = all( isAlways( sum( center .* VV{n}, 1 ) >= 0 ) ); end; 
    if( ~all( check.normal_plane ) );
        return; end;
    for n = 1:numel( VV );
        check.in_kone(n) = all( in_kone( center, VV{n} ) ); end;
end

function information_output( VV, check, m, opt );
    if( opt.verbose >= 2 || ...
        opt.verbose >= 1 && ~isequal( opt.method{1}, m ) ...
      );
        vprintf( 'Used method to compute center of kone: ''%v''\n', m ); end;
    
    if( opt.verbose >= 0 );
        expect( all(check.finite), 'center_kone:fail', 'Computed center has non-finite values.' );
        expect( all(check.in_kone), 'center_kone:fail', 'Computed center is not in the interior of the kone.' );
        expect( all(check.normal_plane), 'center_kone:fail', 'Normal plane of computed center cuts through kone.' ); end;
end

%%

function [ center, iter ] = center_kone_sdp2( VV );
    % minimizes the squared sum of the distances of the normal vector to the rays of the kone
    % has additional the restriction that the normal plane must not cross the kone
    % the normal plane often touches a face of the kone in dimensions >= 10
    VV = VV{1};
    [dim, N] = size( VV );
    model.obj = [sum( VV, 2 )' 0];
    %model.objcon = 4;

    model.A = [sparse( VV' ) -sparse_ones( N, 1 )];
    model.rhs = zeros( N, 1 );
    model.sense = repmat( '>', [N 1] );

    model.quadcon(1).Qc = speye( dim + 1 );
    model.quadcon.Qc(dim+1,dim+1) = 0;
    model.quadcon(1).q = zeros( 1, dim + 1 );
    model.quadcon(1).sense = '<';
    model.quadcon(1).rhs = 1;

    model.lb = [-ones( dim, 1 ); 0];
    model.ub = [ones( dim, 1 ); inf];
    model.modelsense = 'max';

    params.Method = 0;
    params.OutputFlag = 0;

    %repr( model );
    result = gurobi( model, params );
    iter = result.itercount + result.baritercount;
    if( isequal(result.status, 'OPTIMAL') );
        center = result.x(1:dim);
    else;
        center = zeros( dim, 1 ); end;
end

function [ center, iter ] = center_kone_sdp( VV );
    % maximizes the smallest "angle" from the normal plane to any vector of the kone
    % normalizes the normal-vector, the resulting normal is inside of the kone
    % the normal plane often touches a face of the kone in dimensions >= 3
    VV = VV{1};
    [dim, N] = size( VV );
    
    model.A = [sparse( VV' ) -sparse_ones( N, 1 )];
    model.rhs = zeros( N, 1 );
    model.sense = repmat( '>', [N 1] );
    
    model.quadcon(1).Qc = speye(dim+1); model.quadcon.Qc(dim+1,dim+1) = 0;
    model.quadcon(1).q = zeros( 1, dim + 1 );
    model.quadcon(1).sense = '<';
    model.quadcon(1).rhs = 1;

    model.lb = [-ones( dim, 1 ); 0];
    model.ub = [ones( dim, 1 ); inf];
    model.modelsense = 'max';
    model.obj = [zeros( dim, 1 ); 1];
    params.Method = 0;
    params.OutputFlag = 0;
    
    result = gurobi( model, params );
    iter = result.itercount + result.baritercount;
    if( isequal(result.status, 'OPTIMAL') );
        center = result.x(1:dim);
    else;
        center = nan( dim, 1 ); end;
end

function [ center, iter ] = center_kone_lp( VV );
    % maximizes the smallest "angle" from the normal plane to any vector of the kone
    % does not normalize the normal-vector, and thus the resulting normal may be outside of the kone
    VV = VV{1};
    [dim, N] = size( VV );
    
    model.A = [sparse( VV' ) -sparse_ones( N, 1 )];
    model.rhs = zeros( N, 1 );
    model.sense = repmat( '>', [N 1] );

    model.lb = [-ones( dim, 1 ); 0];
    model.ub = [ones( dim, 1 ); inf];
%     model.modelsense = 'max';
%     model.obj = [zeros( dim, 1 ); dim];
    params.Method = 0;
    params.OutputFlag = 0;
    
    result = gurobi( model, params );
    iter = result.itercount + result.baritercount;
    if( isequal(result.status, 'OPTIMAL') );
        center = result.x(1:dim);
    else;
        center = nan( dim, 1 ); end;
end

function [ center, iter ] = center_kone_mean( VV );
    % takes the mean of the rays of the kone
    % normal plane often crosses the kone
    VV = VV{1};
    iter = 1;
    center = mean( VV, 2 );
end

function [ center, iter ] = center_kone_median( VV );
    % takes the median of the rays of the kone
    % normal plane often crosses the kone
    VV = VV{1};
    iter = 1;
    center = mean( VV, 2 );
end

function [ center, iter ] = center_kone_random( VV );
    % takes the median of the rays of the kone
    % normal plane often crosses the kone
    VV = VV{1};
    [dim, N] = size( VV );
    
    model.A = sparse( VV );
    model.sense = repmat( '=', dim, 1 );
    
    model.lb = zeros( 1, N );
    model.ub = inf( 1, N );
    params.Method = 0;
    params.OutputFlag = 0;
    iter = 0;
    for rounds = 1:10;
        
        center = randn( dim, 1 );

        % test whether normal plane is ok
        if( any(sum(center.*VV, 1) < 0) );
            continue; end;

        % test whether center is in the inside of the kone
        
        model.rhs = center;
        result = gurobi( model, params );
        iter = result.itercount + result.baritercount;
        if( strcmpi(result.status, 'optimal') );
            break; end; end;
end

function [ center, iter ] = center_kone_randsamp( VV );
    % we compute a center of the dual kone
    % by generating random points in the dual kone, and then take the mean of the points
    VV = VV{1};
    dim = size( VV, 1 );
    
    num_pts = 20 * dim^2;

    % initial points
    pts = [-VV randn( dim, num_pts )];  % 20 is a quite arbitrary number
    nrm = sqrt( sum( pts.^2, 1 ) );
    pts = pts./nrm;
    remove_idx = false( 1, size(pts, 2) );
    for i = 1:size( pts, 2 );
        angle = sum( pts(:,i) .* VV, 1 );
        if( any( angle >= 0 ) );
            remove_idx(i) = true; end; end;
    pts(:,remove_idx) = [];

    % make more points
    delta = 3;  % distance of newly generated points
    for rounds = 1:10;
        new_pts = pts + delta*randn( size(pts) );
        delta = delta/1.5;
        nrm = sqrt( sum( new_pts.^2, 1 ) );
        new_pts = new_pts./nrm;
        remove_idx = false( 1, size(new_pts, 2) );
        for i = 1:size( new_pts, 2 );
            angle = sum( new_pts(:,i) .* VV, 1 );
            if( any( angle >= 0 ) );
                remove_idx(i) = true; end; end;
        new_pts(:,remove_idx) = [];
        pts = [pts new_pts];  %#ok<AGROW>
    end

    center = -mean( new_pts, 2 );
    iter = 0;

    % plotm( VV, 'funct','kone', 'hull',1 );
    % alpha .5
    % plotm( pts, 'r.', 'funct','kone', 'hull',1, 'hold','on' );
    % plotm( center, 'arrow',1.5, 'hold','on', 'linewidth',2 )
    % plotm( -center, 'k', 'arrow',1.5, 'hold','on', 'linewidth',2 )
    
end

function [ center, iter ] = center_kone_dual( VV );
    VV = VV{1};    
    [dim, N] = size( VV );

    model.A = sparse( VV' );
    model.rhs = zeros( N, 1 );
    model.sense = repmat( '<', [N 1] );

    model.lb = -ones( dim, 1 );
    model.ub = ones( dim, 1 );
    model.modelsense = 'minimize';
    params.Method = 0;
    params.OutputFlag = 0;
    try;
        result = gurobi( model, params );
    catch me;
        flag = isequal( exist('gurobi_setup.m', 'file'), 2 ) && isequal( exist('gurobi.m', 'file'), 2 );
        if( flag );
            throw( me );
        else;
            error( 'kone:gurobi', 'This function currently needs the Gurobi solver.' ); end; end;
    iter = result.itercount + result.baritercount;
    if( isequal(result.status, 'OPTIMAL') );
        center = result.x(1:dim);
    else;
        center = nan( dim, 1 ); end;
end

function [ center, iter ] = center_kone_distsearch( VV );
    VV = VV{1};
    iter = size( VV, 2 );
    if( isempty(VV) );
        center = [];
        return; end;
    center = VV(:,1);
    VVi = VV;
    while( ~isempty(VVi) );
        dists = sum( (center - VVi).^2 );
        [~, idx] = min( dists );
        al = 1/2;
        center = al*center + (1-al)*VVi(:,idx);
        VVi(:,idx) = [];
        for rounds = 1:1000;
            %plotm( [zeros(dim, 0) VV], 'func','kone', 'hull',1 ); alpha .5;
            %plotm( center, 'arrow',1.1, 'r', 'linewidth',3, 'hold','on' );
            bad_halfplane_idx = sum( center .* VV ) < 0;
            if( any(bad_halfplane_idx) );
                center = center + sum( VV(:,bad_halfplane_idx), 2 );
            else;
                break; end; end;
        VVi(:,rand(1,size(VVi, 2)) < .5) = []; end

    center = center/norm( center );
end

function [ center, iter ] = center_kone_sdp2iter( VV );
    VV = VV{1};
    dim = size( VV, 1 );
    while( size(VV, 2) < 2*dim );
        VVo = VV;
        for i = 1:size( VVo, 2 );
            VV = [VV (VVo(:,i)+VVo)/2]; end; end;  %#ok<AGROW>
    idx_zero = ~any( VV, 1 );
    VV(:,idx_zero) = [];
    while( size(VV, 2) >= dim );
        [ center, iter ] = center_kone_sdp2( VV, size(VV, 2), dim );

        %plotm( VV, 'kone', 'hull',1 )
        %plotm( center, 'arrow',1.1, 'r', 'linewidth',2, 'hold','on' );        
        %alpha .5
        %view( center );
        %axis equal
        [~, co] = evalc( 'lsqnonneg( VV, center );' );
        center_idx = abs( co ) > 1e-9;
        if( nnz(center_idx) < 2 );
            angle = sum( center .* VV, 1 );
            [~, idx] = sort( angle );
            maxidx = min( 2, numel(idx) );
            center_idx( idx(:,1:maxidx) ) = true; end;
        VV(:,center_idx') = [];
        if( any(center) );
            VV = [VV center]; end; end;  %#ok<AGROW>
    
end

function [ center, iter ] = center_kone_halfone( VV, opt );
    VV = VV{1};    
    [dim, N] = size( VV );
    if( issym(VV) );
        assert( norm(imag(VV)) == 0, 'center_kone:imaginary', '`center_kone` can only work with real-valued data.' ); end;
    VV = real( double( VV ) );
    m = mean( VV, 2 );
    alpha = 0.5;
    while( true );
        Vh = alpha*VV + (1-alpha)*m;
        nrm = sum( Vh.^2, 1 ).^(1/2);
        Vh = Vh./nrm;
        %Vh = VV;
    
        model.A = [ -sparse_ones( N, 1 )   sparse( VV' ) spzeros( N, N )
                    spzeros( dim, 1 ) -speye( dim ) sparse( Vh )   ];
        model.rhs = zeros( N + dim, 1 );
        model.sense = [repmat( '>', [N 1] ); repmat( '=', [dim 1] )];
        model.lb = [-1; -ones( dim, 1 ); zeros( N, 1 )];
        model.ub = [1; ones( dim, 1 ); inf( N, 1 )];
        model.modelsense = 'max';
        model.obj = [1; zeros( dim, 1 ); zeros( N, 1 );];
        params.Method = 0;
        params.OutputFlag = 0;
    
        result = gurobi( model, params );
        iter = result.itercount + result.baritercount;
        if( isequal(result.status, 'OPTIMAL') );
            center = ratt( result.x(2:dim+1), 1e-4 );
        else;
            center = nan( dim, 1 ); end;
        if( alpha > 0.99 );
            if( opt.verbose >= 1 );
                warning( 'center_kone:halfone', 'Halfone algorithm failed. This should actually not happen.' ); end;
            break;
        elseif( norm(center) < .5 );
            alpha = (alpha + 1)/2;
        elseif( all( sum(center .* VV, 1) >= 0 ) );
            break;
        else;
            alpha = (alpha + 1)/2; end;
    end;
end

function [ center, iter ] = center_kone_multiple( VV, VVs );
    % searches a vector c which satisfies the following
    % (1) c \in VV{n} for all VV{n}
    % (2) c^\perp \cap VV{n} = \{0\} for all VV{n}
    % (3) c \in VVs{n}^* for all VVs{m}
    % (4) c^perp \cap VVs{m} = \{0\} for all VV{m}
    %
    % does not normalize the normal-vector, and thus the resulting normal may be outside of the kone
    
    dim = size( VV{1}, 1 );
    num_VV_coeff = sum( cellfun( 'size', VV, 2 ) );
    num_VVs_coeff = sum( cellfun( 'size', VVs, 2 ) );
    N = numel( VV );
    M = numel( VVs );
    A1 = [repmat( 0, dim*N, 1 ) repmat( -eye(dim), N, 1 ) blkdiag( VV{:} ) zeros( dim*N, num_VVs_coeff )]; %#ok<REPMAT>
    rhs1 = repmat( 0, dim*N, 1 );  %#ok<REPMAT>
    sense1 = repmat( '=', dim*N, 1 );
    
    A2 = [repmat( 1, num_VV_coeff, 1 )  [VV{:}]' zeros( num_VV_coeff, num_VV_coeff + num_VVs_coeff)];  %#ok<REPMAT>
    rhs2 = repmat( 0, num_VV_coeff, 1 );  %#ok<REPMAT>
    sense2 = repmat( '>', num_VV_coeff, 1 );
    
    A3 = [repmat( 0, num_VVs_coeff, 1 ) [VVs{:}]' zeros( num_VVs_coeff, num_VV_coeff + num_VVs_coeff)]; %#ok<REPMAT>
    rhs3 = repmat( 0, num_VVs_coeff, 1 );  %#ok<REPMAT>
    sense3 = repmat( '<', num_VVs_coeff, 1 );
    
    A4 = [repmat( 0, dim*M, 1 ) repmat( eye(dim), M, 1 ) zeros( dim*M, num_VV_coeff) blkdiag( VVs{:} )]; %#ok<REPMAT>
    rhs4 = repmat( 0, dim*M, 1 );  %#ok<REPMAT>
    sense4 = repmat( '=', dim*M, 1 );
    
    model.A = sparse( [A1;A2;A3;A4] );
    model.rhs = [rhs1;rhs2;rhs3;rhs4];
    model.sense = [sense1;sense2;sense3;sense4];
    
    model.lb = [-1 -ones( 1, dim ) zeros( 1, num_VV_coeff + num_VVs_coeff )];
    model.ub = [1 ones( 1, dim )   inf( 1, num_VV_coeff + num_VVs_coeff )];

    model.modelsense = 'max';
    model.obj = [-1 zeros( 1, dim + num_VV_coeff + num_VVs_coeff)];
    params.Method = 0;
    params.OutputFlag = 0;
    
    result = gurobi( model, params );
    iter = result.itercount + result.baritercount;
    if( isequal(result.status, 'OPTIMAL') );
        center = result.x(2:dim+1);
    else;
        center = nan( dim, 1 ); end;
end


function [ center, iter ] = center_empty( VV );
    dim = size( VV{1}, 1 );
    center = zeros( dim, 0 );
    iter = 0;
end


function dummy; end  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*INUSL>  % INUSD in R2020a
