function [ flag ] = in_kone( pts, K, method );
% [ flag ] = in_kone( pts, K, [method] );
% checks whether a vector p is inside of a kone K
%
% Input:
%   pts         matrix of column vectors, the points to be tested
%   K           matrix of column vectors, the kone
%   method      (optional), default = 0, which method to use. Possible values: 'default'/0, 'prune'/1
%
% Written by: tommsch, 2024-01

% Changelog: 
    
    if( nargin <= 2 || isempty(method) );
        method = 0; end;

    n_pts = size( pts, 2 );
    flag = zeros( 1, n_pts );

    
    switch method;
        case {0,'lp','default','def'};
            for n = 1:n_pts;
                flag(n) = in_kone_lp( pts(:,n), K ); end;
        case {1,'prune'};
            for n = 1:n_pts;
                flag(n) = in_kone_prune( pts(:,n), K ); end;
        otherwise;
            error( 'in_kone:method', 'Supplied method is not valid.' ); end;
    flag = logical( flag );
end

function [ flag ] = in_kone_lp( p, K );
    model.A = sparse( K );
    model.rhs = p;
    model.sense = repmat( '=', [1 numel(p)] );
    model.obj = ones( 1, size(K, 2) );
    params.OutputFlag = 0;
    res = gurobi( model, params );
    if( ~isequal(res.status, 'OPTIMAL') );
        flag = 0;
    else;
        res_pts = K(:,res.x > 1e-12);
        flag = rank( res_pts ); end;
end

function [ flag ] = in_kone_prune( p, K );
    p = normalizematrix( p, 'colnorm',2 );
    K = normalizematrix( K, 'colnorm',2 );
    while( true );
        [~, co, res] = evalc( 'lsqnonneg( K, p );' );
        if( res < 1e-9 );
            break; end;
        co = K\p;
        idx = co < 0;
        if( any(idx) );
            K(:,idx) = [];
        else;
            break; end; end;
    residual = norm( K*co - p );
    flag = residual < 1e-9;
end

function dummy; end  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
