function [ Et ] = Et_kone( t, center, nullspace );
% compute the kone-scaling-matrix Et
% [ Et ] = Et_kone( t, center, nullspace );
%
% See also: scale_kone, mult_kone
%
% Written by: tommsch, 2024

    if( nargin <= 2 || isempty(nullspace) );
        nullspace = nullspace_kone( center ); end;
    dim = size( center, 1 );
    V = [center nullspace];
    T = blkdiag( 1, t*eye(dim-1) );
    Et = V*T*V';
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
