function [ Ad, A, m, r, c ] = statematrix( varargin )
% [ Ad, A, m, r, c ] = statematrix( varargin )
% Generates a state space matrix Ad given a matrix A
%
% Options:
%   'm',m       vector of delay lenghts, default = random
%   'A',A       predefined matrix, default = random
%   'dim',dim   dimension of A, default = random
%   'offset'    default = 0, debug option, value which is added to all zeros in state space matrix
%   varargin    stuff passed to gallery_matrixset which defines the state space matrix
%
% Output
%   Ad          the state space matrix
%   A           the matrix
%   m           the delay lenghts
%   r, c        the row and column indices of Ad where to find A
%
% Notes:
%   - Dimension of the matrix A must be equal to the length of the vector `m`.
%
% Written by: tommsch, 2024-02-18


    [A, m, offset] = parse_input( varargin{:} );
    [U, R, P ] = make_URP( m, offset );
    [Ad, r, c] = make_Ad( m, U, R, P, A );
    
end

function [ A, m, offset ] = parse_input( varargin );
    [A, varargin]       = parsem( {'A'}, varargin, [] );
    [m, varargin]       = parsem( {'m'}, varargin, [] );
    [dim, varargin]     = parsem( {'dim','S'}, varargin, [] );
    [offset, varargin]  = parsem( {'offset'}, varargin, 0 );
    
    while( numel(varargin) >= 1 );
        if( isempty(m) && isnumeric(varargin{1}) && numel(varargin{1}) > 1 );
            m = varargin{1};
            varargin(1) = [];
            continue; end;
        if( isempty(dim) && isnumeric(varargin{1}) && numel(varargin{1}) == 1 );
            dim = varargin{1};
            varargin(1) = [];
            continue; end;
        if( isempty(A) && (iscell(varargin{1}) || issquare(varargin{1})));
            A = tocell( varargin{1} );
            varargin(1) = [];
            continue; end;
        
        break; end;
    
    if( false );
    elseif( isempty(A) && isempty(m) && isempty(dim) );
        m = 2 + cumsum( randi( [1 7], [1 randi([2 10])] ) );
        L = numel( m );
        A = gallery_matrix( 'dim',L, varargin{:} );
    elseif( ~isempty(A) && isempty(m) && isempty(dim) );
        L = size( A{1}, 1 ); 
        m = repmat( randi([3 7]), [1 L] );
    elseif( isempty(A) && ~isempty(m) && isempty(dim) );
        L = numel( m );
        A = gallery_matrix( 'dim',L, varargin{:} ); 
    elseif( isempty(A) && isempty(m) && ~isempty(dim) );
        N = max( 5, ceil(dim/100) );
        N = min( N, floor(dim/3) );
        try;
            m = randsum( dim, N, 3, 1 );
        catch me;  %#ok<NASGU>
            m = randsum( dim, N-1, 3, 1 ); end;
            
        L = numel( m );
        A = gallery_matrix( 'dim',L, varargin{:} );
    elseif( ~isempty(A) && ~isempty(m) && isempty(dim) );
        assert( numel(m) == size( A{1}, 1 ), 'statematrix:input', 'Dimension of matrices must be equal to the length of the vector `m`.' );
    elseif( isempty(A) && ~isempty(m) && ~isempty(dim) );
        assert( sum(m) == dim, 'statematrix:input', '`S` must be equal to the sum of the vector `m`.' )
        L = numel( m );
        A = gallery_matrix( 'dim',L, varargin{:} ); 
    elseif( ~isempty(A) && isempty(m) && ~isempty(dim) );
        N = size( A{1}, 1 );
        m = randsum( dim, N, 3, 1 );
    else;
        m = repmat( randi([3 7]), [1 randi([2 10])] );
        L = numel( m );
        A = gallery_matrixset( 'dim',L, 'N',1, varargin{:} ); end;
    
    assert( all(m >= 2), 'statematrix:input', 'Delay lengths in vector `m` must be at least 3.' );    
    assert( numel(m) == size( A, 1 ) );
end

function [ U, R, P ] = make_URP( m, offset );
    L = numel( m );
    U = cell( 1, L );
    R = cell( 1, L );
    P = cell( 1, L );
    
    for n = 1:L
        U{n} = [[zeros( m(n)-3, 1 ) eye( m(n)-3 ) ];
                 zeros( 1, m(n)-2 )] + 1*offset;
        R{n} = zeros( m(n)-2, L ) + 2*offset;
        if( m(n) >= 3 );
            R{n}(m(n) - 2, n) = 1; end;
        P{n} = zeros( L, m(n)-2) + 3*offset;
        if( m(n) >= 3 );
            P{n}(n,1) = 1; end; end;
end

function [ Ad, r, c ] = make_Ad( m, U, R, P, A );
    L = numel( m );
    S = sum( m );
    Ad0 = blkdiag( U{:} );
    Ad0 = [Ad0 zeros( S-L*2, L )              cat( 1, R{:} )];
    Ad0 = [Ad0;
          cat( 2, P{:} )             zeros( L, L ) zeros( L, L )];
      
    Ad = [Ad0;
          zeros( L, S-L*2 )     A          zeros( L, L )];
    r = (size( Ad0, 1 ) + 1):size( Ad, 1 );
    c = (S - L*2 + 1):(S - L*2 + 1 + size( A, 2 ) );
end

function scratchpad  %#ok<DEFNU>
    clc
    %m = randi( [3 5], 1, randi([2 6]) )
    %m = repmat( randi([2 5]), [1 randi([2 5])] )
    m = [3 4 5 6 7 8];
    sum(m)
    numel(m)
    %m = [3 3 3];
    [T{1}, A{1}] = statematrix( m );
    [T{2}, A{2}] = statematrix( m );  %#ok<NASGU>
    T = T./rho( T );

    [TI, B] = invariantsubspace( T, 'perm', 'complex',1 );
    size(B)
    [TR, TT] = restrictmatrix( T, B );  %#ok<ASGLU>
     [~, idx] = max( cellfun( @(x) size(x{1}, 1 ), TI ) );
    % TT{idx}{1}
    % TT{idx}{2}
    %A{1}
    %A{2}
    subplot_lazy( 1 )
    imagesc( abs(TI{idx}{1}) )
    drawnow
    axis equal

    subplot_lazy( 2 )
    %ipa( T, 'plot','polytope', 'delta',.95 )
    graph_precomponent(TI{idx}{1},[],3)
    %imagesc( abs(buildproduct( TI{idx}, randi([1 2  ],[1 2*randi(max(m))] ))) ~= 0 ) 
    colorbar
    drawnow
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
