function [ ret ] = ispermutationmatrix( M );
%(experimental) 
    one = isAlways( M == 1, 'Unknown','false' );
    zero = isAlways( M == 0, 'Unknown','false' );
    ret = issquare(M) && ...
          all( isAlways( sum( one, 1 ) == 1) ) && ...
          all( isAlways( sum( one, 2 ) == 1) ) && ...
          all( one(:) | zero(:) );
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
