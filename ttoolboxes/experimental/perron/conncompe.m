function [ bins, binsize ] = conncompe( G, epsilon );

    if( nargin <= 1 || isempty(epsilon) );
        epsilon = 0.8; end;
    assert( epsilon < 1 );
    
    % if( epsilon == 0 );
    %     [ bins, binsize ] = conncompe( digraph(G) );
    %     return; end;

    G = abs( G );
    sze = size( G, 1 );
    i = 1;
    idx{i} = [0];  %#ok<NBRAK2>  % needed as placeholter so that the computation of `idx` succeeds also in the first iteration
    while( true );
        idxi = any( 1:sze == [idx{:}]', 1 );
        delete_idx = find( idxi );
        Gi = G;
        Gi(:,delete_idx) = 0;
        Gi(delete_idx,:) = 0;

        normalize_idx = any( Gi > 0, 1 );
        Gi(:,normalize_idx) = Gi(:,normalize_idx)./sum( Gi(:,normalize_idx), 1 );

        first_idx = find( ~idxi, 1 );
        if( isempty(first_idx) );
            break; end;
        vi = cell( 1, sze );
        vi{1} = zeros( sze, 1 );
        vi{1}(first_idx) = 1;

        bd = 1e-14*norm( Gi, 1 );
        j = 0;
        k = @(j) mod( j, sze ) + 1;
        while( true )
            vk = vi{k(j)};
            vi_new = Gi * vk;
            s = sum( abs(vi_new(:)) );
            % if( s > 0 );
                % vi_new = vi_new/s; end;
            if( j > sze && ...
                min( sum( abs(vi_new - [vi{:}]), 1) ) <= bd ...
             );
                break; end; 
            j = j + 1;
            vi{k(j)} = vi_new; end;

        epsiloni = epsilon;
        vi = max( [vi{:}], [], 2 );
        while( true );
            idx{i} = find( vi >=  (max(vi) * (1 - norm(G, 1)*epsilon )))';
            if( isempty(idx{i}) && epsiloni < 1);
                epsiloni = epsiloni * 1.5;
            elseif( isempty(idx{i}) );
                idx{i} = first_idx;
                break;
            else;
                break;
            end; end;

        i = i + 1; end;
    
    bins = zeros( 1, sze );
    for i = 1:numel( idx );
        bins( idx{i} ) = i;
    end
    binsize = cellfun( 'prodofsize', idx );

    assert( numel(bins) == sze );
    assert( numel(bins) == unique(numel(bins)) );
    assert( sum(binsize) == sze );
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
