function [ w ] = bandweight( X, flag );
    if( nargin <= 1 || isempty(flag) );
        flag = false; end;

    if( flag )
        w = max( [bandweight_worker( X )
                  bandweight_worker( flipud(X) )
                  bandweight_worker( fliplr(X) )] );
    else;
        w = max( [bandwidth_worker( X )
                  bandwidth_worker( flipud(X) )
                  bandwidth_worker( fliplr(X) )] );
    end
end

function [ w ] = bandweight_worker( X );
    len = max( size(X) );
    w = 0;
    for i = -len+1:len-1
        Z = diag( X, i );
        w = w + sum( Z.*abs(i) );
    end
    w = w / len;
end

function [ bw ] = bandwidth_worker( X );
    [l,b] = bandwidth( X );
    bw = l + b;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>