function [ mp, X, nfo ] = gallery_mp( varargin );
% [ mp, X, nfo ] = gallery_mp( what, [options] )
% Returns sets of matrices, mostly used for ipa.
%
% Input:
%   what            string OR integer which controls the return value
%
% Options:
%   'solver',str    string, default = 'gurobi', name of solver. Currently, must be 'gurobi'
%   'dim',val       Integer, to specify dimension by name-value pair
%   'k',val         To specify k by name-value pair
%                   If any of these is specified by a name value pair, the positional argument is ignored
%
%
% Further Options:
%   'seed',val      Seed for random number generator. 
%                   If seed is set, the random number generator has the same state before and after that function 
%   'verbose',val   Verbose level
%
% Output:    
%   mp         struct, containing the mathematical programming data, can directly passed to solver
%   X          matrix, a matrix satisfying the constraints
%   nfo        struct, containing info about the generated problem

%            tommsch, 2023-07-17, Added possibility to pass threshold with option 'supp'
% Changelog: 
    
    nfo = struct;
    X = [];
    [mp_orig, opt] = parse_input( nargout, varargin{:} );

    [mp_orig, opt] = getmp_named( mp_orig, opt );



    counter = 0;
    while( counter <= opt.num_try )
        counter = counter + 1;
        mp_orig = reset_mp( mp_orig, opt );
        
        mp = getmp( mp_orig, opt );
        
        if( ~opt.solve );
            break; end;
        
        [ X, res, mp.mpgnfo ] = make_example( mp, opt );
        if( ~isempty(X) );
            break; end; 
%         vprintf( '\n', 'imp',[2 opt.verbose] ); 
    end;
    
    if( isempty(X) && opt.solve && counter>=opt.num_try );
        vprintf( '\nCould not find feasible solution --- I give up.\n', 'imp',[1 opt.verbose], 'cpr','err' ); end;
    
    vprintf( '%s\n', mp.mpgnfo, 'imp',[1 opt.verbose] );
    vprintf( 'Hash: %s\n', thash(mp.mpgnfo, [], 8*opt.verbose), 'imp',[1 opt.verbose] );
    
    if( nargout >= 3 );
        nfo.res = res;
        nfo.dim = opt.dim; end;
  
end

function [ mp, opt ] = parse_input( narg, varargin );  % n arg == nargout of original input

    [opt.random, varargin]          = parsem( {'random','rand'}, varargin, 0, 'postprocess',@str2numt );    % if given, a random model is made, the larger the value, the more constraints
    [opt.verbose, varargin]         = parsem( {'verbose','v'}, varargin, 1, 'postprocess',@str2numt );      % verbosity level
    [opt.dim, varargin]             = parsem( {'dimension','dim'}, varargin, [], 'postprocess',@str2numt ); % dimension of the matrix to be restricted
    [opt.modelname, varargin]       = parsem( {'modelname'}, varargin, 'mpgallery' );               % string, just for nicer output
    [opt.shift, varargin]           = parsem( {'shift'}, varargin, [], 'postprocess',@str2numt );   % index of the first entry of the thing to be programmed
    [mp, varargin]                  = parsem( {'model'}, varargin, {} );                            % an existing model which shall be augmented
    [opt.solve, varargin]           = parsem( {'solve'}, varargin, [] );                            % if given, it is checked whether model has a solution
    [opt.num_try, varargin]          = parsem( {'counter_max','numtry','numtries'}, varargin, [], 'postprocess',@str2numt );  % how often shall a model be generated
    [opt.ex, varargin]              = parsem( {'example','ex'}, varargin, 0 );                                               % whether to try to generate random example.
    if( isscalar(opt.ex) && opt.ex > 0 );
        oldrng = rng();
        opt.cleanrng = onCleanup( @() rng(oldrng) );
        rng( opt.seed );
    elseif( ~isequal(opt.ex, 0) && size(opt.ex,1) > 1 && issquare(opt.ex) );
        dim = size( opt.ex, 1 );
        assert( isempty(opt.dim) || isequal(opt.dim, dim), 'mpgallery:dim', 'If `dim` is given and an `example`, the dimensions must coincide.' );
        opt.dim = dim; end;
    [opt.redundantbound, varargin]  = parsem( {'redundantbound','redundantboundsconstraints'}, varargin, 0 );                % if given, constraints on elemenetwise bounds are also added to the matrix A
    
    assert( implies( ~isempty(mp), ~isempty(opt.dim) ), 'mpgallery:dim', 'If ''model'' is given, then ''dimension'' must be given too.' )
    
%     if( isempty(opt.dim) );
%         opt.dim = 1 + randp; end;  
    
    if( isempty(opt.num_try) );
        if( opt.random == 0 );
            opt.num_try = 1;
        else;
            opt.num_try = 20; end; end;
    
    random_default_threshold = 0.2;
    if( isequal(opt.random, 1 ) );
        opt.random = random_default_threshold; end;
    
    
    if( numel(varargin) >= 1 );
        opt.what = varargin;
    else;
        if( isequal(opt.random, 0) );
            opt.random = random_default_threshold; end;
        opt.what = 'random'; end;
    
    if( isempty(opt.solve) );
        opt.solve = narg >= 2 || ~isequal(opt.ex, 0); end;
    
    if( ~iscell(opt.what) );
        opt.what = {opt.what}; end;
    
    if( isempty(opt.shift) && isfield(mp, 'shift') );
        opt.shift = mp.shift; 
    elseif( isempty(opt.shift) );
        opt.shift = 0; end;
    
    assert( ~isempty(opt.dim), 'mpgallery:dim', 'Dimension of problem is not given and could not be deduced.' );
end


function [ mp, opt ] = make_empty_model( mp, opt );

    % make model
    if( isempty(mp) );
        mp.A = spzeros( 0, opt.dim^2 );
        mp.rhs = zeros( 0, 1 );
        mp.sense = char( zeros( 0, 1 ) );
        mp.lb = -inf( 1, opt.dim^2 );
        mp.ub = inf( 1, opt.dim^2 );
        mp.modelsense = 'min';
        mp.modelname = opt.modelname;
        mp.shift = 0;
        mp.mpgnfo = '';
    else;
        num_var = size( mp.A, 2 );
        if( ~isfield(mp, 'lb') );
            mp.lb = -inf( 1, num_var ); end;
        
        if( ~isfield(mp, 'ub') );
            mp.lb = inf( 1, num_var ); end;        
        
        if( ~isfield(mp, 'modelsense') );
            mp.modelsense = 'min'; end;
        
        add_string = ' (augmented by mpgallery)';
        nas = numel( add_string );
        if( ~isfield(mp, 'modelname') || ...
            numel(mp.modelname) >= nas && isequal(mp.modelname(end-nas+1:end), add_string) );
            mp.modelname = opt.modelname;
        else;
            mp.modelname = [mp.modelname add_string]; end; 
        
        if( ~isfield(mp, 'shift') );
            mp.shift = 0; end; end;

    
end

function [ mp ] = reset_mp( mp, opt );  %#ok<INUSD>
    %mp.mpgnfo = vprintf( 'dim = %i | ', opt.dim, 'imp',[1 0] );
end

function [ X, res, mpgnfo ] = make_example( mp, opt );
    X = [];
    params = struct;
    params.OutputFlag = double( opt.verbose >= 3 );
    
    % first find a solution
    res = gurobi( mp, params );
    
    if( isfield(res, 'x') && ~isequal(opt.ex, false) );
        X = reshape( res.x(mp.shift+1:mp.shift+opt.dim^2), [opt.dim opt.dim] );
        
        if( isanyof(opt.ex, {'min','max'}) );
            % generate matrix with minimum/maximum 1-norm
            nextra = size( mp.A, 2 ) - mp.shift - opt.dim^2;
            mp.obj = [zeros( 1, mp.shift ) ones( 1, opt.dim^2 ) zeros( 1, nextra )];
            switch opt.ex;
                case 'min'; mp.modelsense = 'min';
                case 'max'; mp.modelsense = 'max';
                otherwise; fatal_error; end;
            res_ex = gurobi( mp, params );
            if( isfield(res_ex,'x') );
                res = res_ex; end;
            
        else;
            % generate random matrix with random elementwise bounds
            if( isequal(opt.ex, 'time') );
                old_rng = rng();
                cleanrng = onCleanup( @()rng(old_rng) ); 
                rng( 'shuffle' );
            elseif( isanyof(opt.ex, {'min','max'}) );
                % do nothing with the rng
            elseif( isanyof(opt.ex, {'opt'}) );
                error( 'mpgallery:opt', 'For option `ex`, the value `opt` is not a valid one.' );
            elseif( isanyof(opt.ex, {'rng'}) );
                error( 'mpgallery:opt', 'For option `ex`, the value `opt` is not a valid one.' );                
            elseif( isscalar(opt.ex) );
                old_rng = rng();
                cleanrng = onCleanup( @()rng(old_rng) );
                rng( opt.ex ); end;

            lb_orig = mp.lb(opt.shift+1:opt.shift+opt.dim^2);
            ub_orig = mp.ub(opt.shift+1:opt.shift+opt.dim^2);


            % try to make random bounds on matrix entries
            minX = min( X(:) );
            maxX = max( X(:) );
            midX = (maxX-minX)/2;
            minX = midX - 2*(midX-minX);
            maxX = midX + 2*(maxX-midX);
            if( minX == maxX );
                minX = min( -100, minX );
                maxX = max( 100, maxX ); end;

            minX = max( lb_orig, minX );
            maxX = min( ub_orig, maxX );

            counter = 0;
            while( counter<=opt.num_try );
                counter = counter + 1;
                len = numel( mp.lb );
                mp.obj = randn( 1, len );

                lb = minX - counter*(midX-minX).*rand( 1, opt.dim^2 );
                ub = midX + counter*(maxX-minX).*rand( 1, opt.dim^2 );            

                lb = max( lb_orig, lb-min(ub(:)) );
                ub = min( ub_orig, ub+max(lb(:)) );
                idx = lb>=ub;
                lb(idx) = lb_orig(idx);
                ub(idx) = ub_orig(idx);

                mp.lb(opt.shift+1:opt.shift+opt.dim^2) = lb;
                mp.ub(opt.shift+1:opt.shift+opt.dim^2) = ub;

                res_ex = gurobi( mp, params );
                if( isfield(res_ex,'x') );
                    res = res_ex;
                    break; end; 
                vprintf( '.', 'imp',[1 opt.verbose] ); end; end; end;
    
    if( isfield(res, 'x') );
        X = reshape( res.x(mp.shift+1:mp.shift+opt.dim^2), [opt.dim opt.dim] ); end;
    mpgnfo = vprintf( ': %s\n', res.status, 'imp',[2 opt.verbose], 'str',mp.mpgnfo ); 
    
end

function [ mp ] = getmp( mp, opt );
    mp = getmp_property( mp, opt );
end

function [ mp, opt ] = getmp_named( mp, opt );
    [flag, opt] = check( true, opt, {'androsacealbana','androsace_albana','aalbana','a_albana','albana'} );
    if( flag );
        [mp, opt] = mp_named_albana( mp, opt ); end;
    [flag, opt] = check( true, opt, {'2023_17_07','lincolrowsum'} );
    if( flag );
        [mp, opt] = mp_named_lincolrowsum( mp, opt ); end;    
    
    if( isempty(mp) );
        [mp, opt] = make_empty_model( mp, opt ); end;
end

function [ mp ] = getmp_property( mp, opt );

    while( true );
        oldnum = numel( opt.what );
    %% symmetries

    % transpositions
        [flag, opt] = check( true, opt, {'symmetric','sym'} );
        if( flag );
            mp = mp_symmetric( mp, opt, 'symmetric' ); end;

        [flag, opt] = check( true, opt, {'antisymmetric','antisym','asym','persymmetric','persym','psym'} );
        if( flag );
            mp = mp_symmetric( mp, opt, 'antisymmetric' ); end;

        [flag, opt] = check( true, opt, {'bisymmetric','bisym',} );
        if( flag );
            mp = mp_symmetric( mp, opt, 'bisymmetric' ); end;

        [flag, opt] = check( true, opt, {'toeplitz'} );
        if( flag );
            mp = mp_symmetric( mp, opt, 'toeplitz' ); end;

        [flag, opt] = check( true, opt, {'hankel','antitoeplitz'} );
        if( flag );
            mp = mp_symmetric( mp, opt, 'hankel' ); end;

        [flag, opt] = check( true, opt, {'circulant'} );
        if( flag );
            mp = mp_symmetric( mp, opt, 'circulant' ); end;

        [flag, opt] = check( true, opt, {'anticirculant'} );
        if( flag );
            mp = mp_symmetric( mp, opt, 'anticirculant' ); end;    

    %% shapes
        [flag, opt] = check( true, opt, {'uppertriangular','triangularupper','triangularu','triangular','triu'} );
        if( flag );
            mp = mp_banded( mp, opt, [0 inf] ); end;

        [flag, opt] = check( true, opt, {'lowertriangular','triangularlower','triangularl','tril'} );
        if( flag );
            mp = mp_banded( mp, opt, [-inf 0] ); end;

        [flag, opt] = check( true, opt, {'tridiagonal','tridig'} );
        if( flag ); 
            mp = mp_banded( mp, opt, [-1 1] ); end;

        [flag, opt] = check( true, opt, {'banded','band','bandwidth'}, nan );
        if( ~isequaln(flag, nan) );
            mp = mp_banded( mp, opt, flag ); end;

        [flag, opt] = check( true, opt, {'hessenberg','upperhessenberg', 'uhessenberg', 'hessenbergupper', 'hessenbergu'}, nan );  % lower triangular + all ones on the main diagonal
        if( ~isequaln(flag, nan) );
            mp = mp_banded( mp, opt, [-1 inf] ); end;

        [flag, opt] = check( true, opt, {'lowerhessenberg', 'lhessenberg', 'hessenberglower', 'hessenbergl'}, nan );  % lower triangular + all ones on the main diagonal
        if( ~isequaln(flag, nan) );
            mp = mp_banded( mp, opt, [-inf 1] ); end;    

        [flag, opt] = check( true, opt, {'unitriangular','unitriangularupper','unitriu'}, nan );  % triangular + all ones on the main diagonal
        if( ~isequaln(flag, nan) );
            mp = mp_banded( mp, opt, [0 inf] );
            mp = mp_diagonal( mp, opt, 1, 'main', 'eq' ); end;  % XX

        [flag, opt] = check( true, opt, {'unitriangularlower','unitril'}, nan );  % lower triangular + all ones on the main diagonal
        if( ~isequaln(flag, nan) );
            mp = mp_banded( mp, opt, [-inf 0] ); 
            mp = mp_diagonal( mp, opt, 1, 'main', 'eq' ); end;



    %% sums
        [flag, opt] = check( true, opt, {'sum','sumeq'}, nan );
        if( ~isequaln(flag, nan) ); 
            mp = mp_sum( mp, opt, flag, 'eq' ); end;
        [flag, opt] = check( true, opt, {'sumlb'}, nan );
        if( ~isequaln(flag, nan) ); 
            mp = mp_sum( mp, opt, flag, 'lb' ); end;
        [flag, opt] = check( true, opt, {'sumub'}, nan );
        if( ~isequaln(flag, nan) ); 
            mp = mp_sum( mp, opt, flag, 'ub' ); end;    

    % stochastic matrices
        [flag, opt] = check( true, opt, {'columnstochastic','colstochastic','colstoch','stochastic','cstochastic','stoch','cstoch'} );
        if( flag ); 
            mp = mp_stochastic( mp, opt, 'c' ); end;

        [flag, opt] = check( true, opt, {'rowstochastic','rstochastic','rowstoch','rstoch'} );
        if( flag );
            mp  = mp_stochastic( mp, opt, 'r' ); end;

        [flag, opt] = check( true, opt, {'doublestochastic','doublestoch','dstoch','rcstoch','crstoch'} );
        if( flag ); 
            mp = mp_stochastic( mp, opt, 'c' ); 
            mp = mp_stochastic( mp, opt, 'r' ); end;

    % row/column sums
        [flag, opt] = check( true, opt, {'columnsum','colsum','csum','columsumeq','colsumeq'}, nan );
        if( ~isequaln(flag, nan) ); 
            mp = mp_colrow_sum( mp, opt, flag, 'c', 'eq' ); end;

        [flag, opt] = check( true, opt, {'columnsumlb','colsumlb','csumlb','cslb'}, nan );
        if( ~isequaln(flag, nan) ); 
            mp = mp_colrow_sum( mp, opt, flag, 'c', 'lb' ); end;

        [flag, opt] = check( true, opt, {'columnsumub','colsumub','csumub','csub'}, nan );
        if( ~isequaln(flag, nan) ); 
            mp = mp_colrow_sum( mp, opt, flag, 'c', 'ub' ); end;


        [flag, opt] = check( true, opt, {'rowsum','rsum','rowsumeq'}, nan );
        if( ~isequaln(flag, nan) ); 
            mp = mp_colrow_sum( mp, opt, flag, 'r', 'eq' ); end;    

        [flag, opt] = check( true, opt, {'rowsumlb','rsumlb','rslb'}, nan );
        if( ~isequaln(flag, nan) ); 
            mp = mp_colrow_sum( mp, opt, flag, 'r', 'lb' ); end;

        [flag, opt] = check( true, opt, {'rowsumub','rsumub','rsub'}, nan );
        if( ~isequaln(flag, nan) ); 
            mp = mp_colrow_sum( mp, opt, flag, 'r', 'ub' ); end;  

    % diagonal sums
        [flag, opt] = check( true, opt, {'maindiagonalsum','maindiagonalsumeq','maindiagsum','maindiagsumeq','mds','diagonalsum','diagsum','ds'}, nan );
        if( ~isequaln(flag, nan) ); 
            mp = mp_diagonal_sum( mp, opt, flag, 'main', 'eq' ); end;

        [flag, opt] = check( true, opt, {'maindiagonalsumlb','maindiagsumlb','mdslb','diagonalsumlb','diagsumlb','dslb'}, nan );
        if( ~isequaln(flag, nan) ); 
            mp = mp_diagonal_sum( mp, opt, flag, 'main', 'lb' ); end;

        [flag, opt] = check( true, opt, {'maindiagonalsumub','maindiagsumub','mdsub','diagonalsumub','diagsumub','dsub'}, nan );
        if( ~isequaln(flag, nan) ); 
            mp = mp_diagonal_sum( mp, opt, flag, 'main', 'ub' ); end;

        [flag, opt] = check( true, opt, {'antidiagonalsum','antidiagonalsumeq','antidiagsum','antidiagsumeq','ads'}, nan );
        if( ~isequaln(flag, nan) ); 
            mp = mp_diagonal_sum( mp, opt, flag, 'anti', 'eq' ); end;

        [flag, opt] = check( true, opt, {'antidiagonalsumlb','antidiagsumlb','adslb'}, nan );
        if( ~isequaln(flag, nan) ); 
            mp = mp_diagonal_sum( mp, opt, flag, 'anti', 'lb' ); end;

        [flag, opt] = check( true, opt, {'antidiagonalsumub','antidiagsumub','adsub'}, nan );
        if( ~isequaln(flag, nan) );
            mp = mp_diagonal_sum( mp, opt, flag, 'anti', 'ub' ); end;
        
    %% diagonal dominance

        [flag, opt] = check( true, opt, {'diagonaldominant','diagonaldominantdom','diagdominant','diagdominantdom','diagdom','dd'}, nan );
        if( ~isequaln(flag, nan) );
            error( 'gallery_mp:missing', 'Diagonal dominance is not implemented yet. You may want to use positive diagonal dominance: ''posdiagdom''.' ); end;
        
        [flag, opt] = check( true, opt, {'posdiagonaldominant','posdiagonaldominantdom','posdiagdominant','posdiagdominantdom','posdiagdom','posdd','pdd'}, nan );
        if( ~isequaln(flag, nan) );
            mp = mp_posdiagonaldominant( mp, opt, flag, 'dom' ); end;
        
        [flag, opt] = check( true, opt, {'posdiagonaldominantsubdominant','posdiagonalsubdominant','posdiagonaldominantsub','posdiagdominantsubdominant','posdiagsubdominant','posdiagdominantsub','posdiagdomsubdom','posdiagsubdom','posdiagdomsub','posdds','pdds'}, nan );
        if( ~isequaln(flag, nan) );
            mp = mp_posdiagonaldominant( mp, opt, flag, 'sub' ); end;
        
        [flag, opt] = check( true, opt, {'posdiagonaldominanteq','posdiagdominanteq','posdiagdomeq','posddeq','pddeq'}, nan );
        if( ~isequaln(flag, nan) );
            mp = mp_posdiagonaldominant( mp, opt, flag, 'eq' ); end;
        
        
        [flag, opt] = check( true, opt, {'monotone','monotoneinc'}, nan );
        if( ~isequaln(flag, nan) ); 
            mp = mp_monotone( mp, opt, flag, 'inc' ); end;
        
        [flag, opt] = check( true, opt, {'monotonedec'}, nan );
        if( ~isequaln(flag, nan) ); 
            mp = mp_monotone( mp, opt, flag, 'dec' ); end;
        
        [flag, opt] = check( true, opt, {'monotoneeq'}, nan );
        if( ~isequaln(flag, nan) ); 
            mp = mp_monotone( mp, opt, flag, 'eq' ); end;
        
        
        [flag, opt] = check( true, opt, {'diagdiff','diffdiag'}, nan );
        if( ~isequaln(flag, nan) ); 
            mp = mp_diagdiff( mp, opt, flag ); end;

    %% elementwise
    % full matrices
        [flag, opt] = check( true, opt, {'supp','support'}, nan );
        if( ~isequaln(flag, nan) );
            mp = mp_supp( mp, opt, flag ); end;
    
        [flag, opt] = check( true, opt, {'elementwise','elementwiseeq','eq','setvariables','setvariable','set_variables','set_variable','set'}, nan );
        if( ~isequaln(flag, nan) );
            mp = mp_elementwise( mp, opt, flag, 'eq' ); end;

        [flag, opt] = check( true, opt, {'elementwiselowerbound','lowerbound','elementwiselb','lb'}, nan );
        if( ~isequaln(flag, nan) ); 
            mp = mp_elementwise( mp, opt, flag, 'lb' ); end;

        [flag, opt] = check( true, opt, {'elementwiseupperbound','upperbound','elementwiseub','ub'}, nan );
        if( ~isequaln(flag, nan) ); 
            mp = mp_elementwise( mp, opt, flag, 'ub' ); end;

    % only diagonals
        [flag, opt] = check( true, opt, {'maindiagonal','maindiagonaleq','maindiag','md','diagonal','diagonaleq','diag','diageq'}, nan );
        if( ~isequaln(flag, nan) ); 
            mp = mp_diagonal( mp, opt, flag, 'main', 'eq' ); end;

        [flag, opt] = check( true, opt, {'maindiagonallowerbound','maindiaglb','mdlb','diagonallowerbound','diaglb','dlb'}, nan );
        if( ~isequaln(flag, nan) ); 
            mp = mp_diagonal( mp, opt, flag, 'main', 'lb' ); end;

        [flag, opt] = check( true, opt, {'maindiagonalupperbound','maindiagub','mdub','diagonalupperbound','diagub','dub'}, nan );
        if( ~isequaln(flag, nan) ); 
            mp = mp_diagonal( mp, opt, flag, 'main', 'ub' ); end;

        [flag, opt] = check( true, opt, {'antidiagonal','antidiagonaleq','antidiag','ad'}, nan );
        if( ~isequaln(flag, nan) ); 
            mp = mp_diagonal( mp, opt, flag, 'anti', 'eq' ); end;    

        [flag, opt] = check( true, opt, {'antidiagonallowerbound','antidiaglb','adlb'}, nan );
        if( ~isequaln(flag, nan) ); 
            mp = mp_diagonal( mp, opt, flag, 'anti', 'lb' ); end;

        [flag, opt] = check( true, opt, {'antidiagonalupperbound','antidiagub','adub'}, nan );
        if( ~isequaln(flag, nan) ); 
            mp = mp_diagonal( mp, opt, flag, 'anti', 'ub' ); end;
        

    %% misc matrix properties
        [flag, opt] = check( true, opt, {'1norm','norm1'} );  % matrix with 1-norm less-equal to 1
        if( flag ); 
            mp = mp_1norm( mp, opt ); end;

        [flag, opt] = check( true, opt, {'plane','planeq','halfspaceeq','halfplaneeq','innerproducteq'}, nan );  % matrix restriced by some arbitrary half plane
        if( ~isequaln(flag, nan) ); 
            mp = mp_halfplane( mp, opt, flag, 'eq' ); end;

        [flag, opt] = check( true, opt, {'halfspacelb','halfplanelb','innerproductlb','halfplane','innerproduct'}, nan );  % matrix restriced by some arbitrary half plane    
        if( ~isequaln(flag, nan) ); 
            mp = mp_halfplane( mp, opt, flag, 'lb' ); end;

        [flag, opt] = check( true, opt, {'halfspaceub','halfplaneub','innerproductub'}, nan );  % matrix restriced by some arbitrary half plane
        if( ~isequaln(flag, nan) ); 
            mp = mp_halfplane( mp, opt, flag, 'ub' ); end;    

        [flag, opt] = check( true, opt, {'magicsquare','magic'}, nan );  % matrix restriced by some arbitrary half plane
        if( ~isequaln(flag, nan) ); 
            mp = mp_magic( mp, opt, flag ); end;    

        [~, opt] = check( true, opt, {'random'} );  % matrix with 1-norm equal to 1
        
        
    %% mutating

        [flag, opt] = check( false, opt, {'addfront','add_front',}, nan );
        if( ~isequaln(flag, nan) );
            mp = mp_addfrontback( mp, opt, flag, 'front' ); end;  % takes character vector with entries `<` (for negative entries), `>` (for positive entries), `u` (for unbounded entries)

        [flag, opt] = check( false, opt, {'addback','add_back',}, nan );
        if( ~isequaln(flag, nan) );
            mp = mp_addfrontback( mp, opt, flag, 'back' ); end;  % takes character vector with entries `<` (for negative entries), `>` (for positive entries), `u` (for unbounded entries)
        
        [flag, opt] = check( false, opt, {'permute','perm',}, nan );
        if( ~isequaln(flag, nan) );
            mp = mp_permute( mp, opt, flag ); end;  % permutes the variables        
        
        if( numel(opt.what ) == oldnum || numel(opt.what) == 0 );
            break; end; end;
    
    parsem( opt.what, 'test' );
end

%% getmp_named workers

function [ mp, opt ] = mp_named_lincolrowsum( mp, opt );
    mp = make_empty_model( mp, opt );
    mp_colrow_sum( mp, opt, 1:opt.dim, 'c', 'eq' );
    mp_colrow_sum( mp, opt, [dim+1-(1:dim)], 'r', 'eq' );
end

% function [ mp, opt ] = mp_named_lincolrowsum_sqrt( mp, opt );
%     mp = make_empty_model( mp, opt );
%     mp_colrow_sum( mp, opt, sqrt(1:opt.dim), 'c', 'eq' );
%     mp_colrow_sum( mp, opt, sqrt([dim+1-(1:dim)]), 'r', 'eq' );
%     % optimal solution for dim = 100 is larger than 7
%     % optimal solution for dim = 50 could be sqrt(2) / 7?
% end

function [ mp, opt ] = mp_named_albana( mp, opt );
% Too big matrix:
%    a/43   b/7  c/12   d/3 0 e/3
%    23/43
%    f/43   0    0      g/3 0 h/3
%    10/43  0    7/12  
%    k/43
%    2/43   0    1/12
%
%    a + b + c + d + e = 163;

% Better matrix:
%    a/43   b/7  c/12   d/3 e/3
%    23/43
%    f/43   0    0      g/3 h/3
%    10/43  0    7/12  
%    2/43   0    1/12
%
%    a + b + c + d + e = 163;   % (Hp1)
%    f + g + h = 22;            % (Hp2);
%    a + f >= c + g;            % (Hp3)
%    c + g >= d + h;            % (Hp4)
%    d + h >= b;                % (Hp5)
%    b >= e;                    % (Hp6)
    
    opt.dim = 5;
    mp = make_empty_model( mp, opt );

    Supp = [1 1 1 1 1;
            1 0 0 0 0;
            1 0 0 1 1;
            1 0 1 0 0;
            1 0 1 0 0];

    Eq = nan( 5 );
    Eq(2,1) = 23/43;
    Eq(4,1) = 10/43;
    Eq(4,3) = 7/12;
    Eq(5,1) = 2/43;
    Eq(5,3) = 1/12;
    
    Hp1_lhs = [43 7 12 3 3; zeros(4,5)];
    Hp1_rhs = 163;
    Hp2_lhs = [zeros(2,5); 43 0 0 3 3; zeros(2,5)];
    Hp2_rhs = 22;
    Hp3_lhs = [[43;0;43;0;0] zeros(5, 4)] -                     [0 0 12 0 0; 0 0 0 0 0; 0 0 0 3 0; zeros(2, 5)];
    Hp4_lhs = [0 0 12 0 0; 0 0 0 0 0; 0 0 0 3 0; zeros(2, 5)] - [0 0  0 3 0; 0 0 0 0 0; 0 0 0 0 3; zeros(2, 5)];
    Hp5_lhs = [0 0  0 3 0; 0 0 0 0 0; 0 0 0 0 3; zeros(2, 5)] - [0 7  0 0 0; zeros(4, 5)];
    Hp6_lhs = [0 7  0 0 0; zeros(4, 5)] -                       [0 0  0 0 3; zeros(4, 5)];
    
    mp = mp_supp( mp, opt, Supp );
    
    mp = mp_elementwise( mp, opt, Eq, 'eq' );
    
    mp = mp_halfplane( mp, opt, {Hp1_lhs, Hp1_rhs}, 'eq' );
    mp = mp_halfplane( mp, opt, {Hp2_lhs, Hp2_rhs}, 'eq' );
    mp = mp_halfplane( mp, opt, {Hp3_lhs, 0}, 'lb' );
    mp = mp_halfplane( mp, opt, {Hp4_lhs, 0}, 'lb' );
    mp = mp_halfplane( mp, opt, {Hp5_lhs, 0}, 'lb' );
    mp = mp_halfplane( mp, opt, {Hp6_lhs, 0}, 'lb' );
    
    % additional restrictions, values should be in the range of the years 2009-2020
    if( 0 ~= 0 );
    lb = [106/43 1/7 30/12 1/3  1/3;
          nan    nan nan   nan  nan;
          10/43  nan nan   7/3  1/3;
          nan    nan nan   nan  nan;
          nan    nan nan   nan  nan];
    ub = [125/43 5/7 54/12 5/3  3/3;
          nan    nan nan   nan  nan;
          14/43  nan nan   10/3 2/3;
          nan    nan nan   nan  nan;
          nan    nan nan   nan  nan];
      
    mp = mp_elementwise( mp, opt, lb, 'lb' );
    mp = mp_elementwise( mp, opt, ub, 'ub' );
    end;
         
end

%% getmp_property worker

%% symmetries

% transpositions

function [ mp ] = mp_symmetric( mp, opt, diag );
    mp.mpgnfo = vprintf( '''%s'', ', diag, 'imp',[2 opt.verbose], 'str',mp.mpgnfo );
    
    switch diag;

        case {'symmetric'};
            row = size( mp.A , 1 );
            n_extrarows = opt.dim*(opt.dim-1)/2;
            n_rows_new = row + n_extrarows;
            
            idx1 = cell( 1, opt.dim-1 );
            idx2 = cell( 1, opt.dim-1 );
            row = size( mp.A, 1 );
            for i = 1:opt.dim-1;
                j = i+1:opt.dim;
                nj = numel( j );
                rows = row+1:row+nj;
                
                idx1{i} = sub2ind( [opt.dim opt.dim], repmat(i, [1 nj]), j );
                idx2{i} = sub2ind( [opt.dim opt.dim], j, repmat(i, [1 nj]) );
                idx1{i} = sub2ind( [n_rows_new opt.shift+opt.dim^2], rows, opt.shift + idx1{i} );
                idx2{i} = sub2ind( [n_rows_new opt.shift+opt.dim^2], rows, opt.shift + idx2{i} ); 
                row = row + nj; 
                end;
            
            idx1 = [idx1{:}];
            idx2 = [idx2{:}];
            mp.A(n_rows_new,opt.shift+opt.dim^2) = 0;
            mp.A([idx1 idx2]) = [ones( 1, numel(idx1) ) -ones( 1, numel(idx2) )];  % assinging to sparse arrays is slow, thus we only do it once
            mp.rhs = [mp.rhs repmat( 0, [1, numel(idx1)] )];  %#ok<REPMAT>
            mp.sense = [mp.sense repmat( '=', [1, numel(idx1)] )];
            
        case {'antisymmetric'};
            row = size( mp.A, 1 );
            for i = 1:opt.dim;
                row = row + 1;
                idx = sub2ind( [opt.dim opt.dim], i, i );
                mp.A(row, idx) = 1;
                mp.rhs(row) = 0;
                mp.sense(row) = '=';
                for j = i+1:opt.dim;
                    row = row + 1;
                    idx12 = sub2ind( [opt.dim opt.dim], [i j], [j i] );
                    mp.A(row, opt.shift + idx12) = [1 1];
                    mp.rhs(1,row) = 0;
                    mp.sense(1,row) = '='; end; end;
            
        case {'bisymmetric'};
            mp = mp_symmetric( mp, opt, 'symmetric' );
            row = size( mp.A, 1 );
            for i = 1:opt.dim;
                for j = 1:opt.dim - i;
                    row = row + 1;
                    idx12 = sub2ind( [opt.dim opt.dim], [i (opt.dim - j + 1)], [j (opt.dim - i + 1)] );
                    mp.A(row, opt.shift + idx12) = [1 -1];
                    mp.rhs(1,row) = 0;
                    mp.sense(1,row) = '='; end; end;
            
        case {'toeplitz','hankel','circulant','anticirculant'};
            row = size( mp.A, 1 );
            switch diag;
                case {'toeplitz'};
                    block1 = [eye( opt.dim - 1 ) zeros( opt.dim-1, 1 )];
                    block2 = [zeros( opt.dim-1, 1 ) -eye( opt.dim - 1 )];
                case {'hankel'};
                    block1 = [zeros( opt.dim-1, 1 ) eye( opt.dim - 1 )];
                    block2 = [-eye( opt.dim - 1 ) zeros( opt.dim-1, 1 )];
                case {'circulant'};
                    block1 = eye( opt.dim );
                    block2 = [zeros( opt.dim-1, 1 ) -eye( opt.dim-1 ); -1 zeros( 1, opt.dim-1 )];
                case {'anticirculant'};
                    block1 = eye( opt.dim );
                    block2 = [zeros( 1, opt.dim-1 ) -1; -eye( opt.dim-1 ) zeros( opt.dim-1, 1 )];
                otherwise; fatal_error; end;
            
            nrow = size( block1, 1 );
            block0 = zeros( nrow, opt.dim );
            for b = 1:opt.dim-1;
                row_idx = row+1:row+nrow;
                blockb = [repmat( block0, [1 b-1] ) block1 block2 repmat( block0, [1 opt.dim-b-1] )];
                mp.A(row_idx,1:opt.dim^2) = blockb; 
                mp.rhs(1,row_idx) = 0;
                mp.sense(1,row_idx) = '=';
                row = row + nrow; end;
            
        otherwise;
            fatal_error; end;
end

%% shapes

function [ mp ] = mp_banded( mp, opt, bandwidth );
% bandwidth is a 1- or 2-element vector, denoting on which diagonals are the non-zero entries
%  0    main diagonal
% <0    below main diagonal
%  > 0    above main diagonal
    
    if( isempty(bandwidth) );
        bandwidth = sort( randi( [-opt.dim opt.dim], [1 2] ) ); end;
    if( numel(bandwidth) == 1 );
        bandwidth = [-bandwidth bandwidth]; end;
    assert( numel(bandwidth) == 2 && bandwidth(1) <= bandwidth(2), 'mpgallery:bandwidth', 'Argument to ''bandwidth'' must be a 0-, 1- or 2-element vector.' );
    mp.mpgnfo = vprintf( '''banded'', %\[r, ',bandwidth, 'imp',[2 opt.verbose], 'str',mp.mpgnfo );

    row = size( mp.A, 1 );
    for i = 1:opt.dim;
        for j = 1:opt.dim;
            if( j-i >= bandwidth(1) && j-i<= bandwidth(2) );
                continue; end;
            row = row + 1;
            idx = sub2ind( [opt.dim opt.dim], i, j );
            mp.A(row, opt.shift + idx) = 1;
            mp.rhs(1,row) = 0;
            mp.sense(1,row) = '='; 
            mp.lb(1,opt.shift + idx) = 0;
            mp.ub(1,opt.shift + idx) = 0;
            end; end;

end

%% sums

function [ mp ] = mp_sum( mp, opt, bd, sense );
    switch sense;
        case 'lb'; sense = '>';
        case 'ub'; sense = '<';
        case 'eq'; sense = '='; 
        otherwise; fatal_error; end;
        
    bd = str2numt( bd );
    if( isempty(bd) || islogical(bd) );
        bd = randn; end;
    assert( isscalar(bd) );
    mp.mpgnfo = vprintf( '''sum%s'', %\[r, ', tif( isequal(sense,'>'),'lb', isequal(sense,'<'),'ub', 'eq' ), ...
                               bd, 'imp',[2 opt.verbose], 'str',mp.mpgnfo );
    
    if( ~isfinite(bd) );
        return; end;
    row = size( mp.A, 1 ) + 1;
    mp.A(row, opt.shift + (1:opt.dim^2)) = 1;
    mp.rhs(1,row) = bd;
    mp.sense(1,row) = sense;
end

function [ mp ] = mp_stochastic( mp, opt, cr );
    mp.mpgnfo = vprintf( '''%sstochastic'', ', cr, 'imp',[2 opt.verbose], 'str',mp.mpgnfo );
    row = size( mp.A, 1 );
    for r = 1:opt.dim;
        row = row + 1;
        switch cr
            case {'c'}; 
                idx = (1 + (r-1)*opt.dim : (r * opt.dim));
            case {'r'};
                idx = r:opt.dim:opt.dim^2; 
            otherwise; fatal_error; end;
        mp.A(row, opt.shift + idx) = 1;
        mp.rhs(1,row) = 1;
        mp.sense(1,row) = '=';
    end    
end

function [ mp ] = mp_colrow_sum( mp, opt, bd, colrow, sense );
    switch sense;
        case 'lb'; sense = '>';
        case 'ub'; sense = '<';
        case 'eq'; sense = '='; 
        otherwise; fatal_error; end;
        
    row = size( mp.A, 1 );
    bd = str2numt( bd );
    if( isempty(bd) || islogical(bd) );
        bd = randn( 1, opt.dim ); end;
    if( isscalar(bd) );
        bd = bd * ones( 1, opt.dim ); end;
    assert( numel(bd ) == opt.dim, 'optperron:sum', 'Given vector has wrong number of elements.' );
    mp.mpgnfo = vprintf( '''%ssum%s'', %\[r, ', tif( isequal(colrow,'c'), 'column', 'row' ), ...
                               tif( isequal(sense,'>'),'lb', isequal(sense,'<'),'ub', 'eq' ), ...
                               bd, 'imp',[2 opt.verbose], 'str',mp.mpgnfo );
    
    for i = 1:opt.dim
        if( ~isfinite(bd(i)) );
            continue; end;
        row = row + 1;
        switch colrow;
            case {'c'}; idx = (1 + (i-1)*opt.dim : (i * opt.dim));
            case {'r'}; idx = i:opt.dim:opt.dim^2; 
            otherwise; fatal_error; end;
        mp.A(row, opt.shift + idx) = 1;
        mp.rhs(1,row) = bd(i);
        mp.sense(1,row) = sense; end;
end

function [ mp ] = mp_diagonal_sum( mp, opt, bd, diag, sense );
    switch sense;
        case 'lb'; sense = '>';
        case 'ub'; sense = '<';
        case 'eq'; sense = '='; 
        otherwise; fatal_error; end;
        
    if( isempty(bd) );
        bd = rand; end;
    
    bd = str2numt( bd );
    
    mp.mpgnfo = vprintf( '''%sdiagsum%s'', %\[r, ', diag, ...
                                                    tif( isequal(sense,'>'),'lb', isequal(sense,'<'),'ub', 'eq' ), ...
                                                    bd, 'imp',[2 opt.verbose], 'str',mp.mpgnfo );
    if( numel(bd) == 1 );
        switch diag;
            case 'main';
                idx = sub2ind( [opt.dim opt.dim], 1:opt.dim, 1:opt.dim );
            case 'anti';
                idx = sub2ind( [opt.dim opt.dim], opt.dim:-1:1, 1:opt.dim );
            otherwise; fatal_error; end;

        Anew = spzeros( 1, opt.dim^2 );
        Anew( idx ) = 1;
        row = size( mp.A, 1 ) + 1;
        mp.A(row, (opt.shift+1):opt.shift+opt.dim^2) = Anew;
        mp.rhs(1,row) = bd;
        mp.sense(1,row) = sense;        
    elseif( numel(bd) == 1+2*(opt.dim-1) );
        for i = 1:1+2*(opt.dim-1);
            row = size( mp.A, 1 ) + 1;
            if( ~isfinite(bd(i)) );
                continue; end;
            switch diag;
                case 'main';
                    r = clamp( (opt.dim-i+1):(2*opt.dim-i), 1, opt.dim, @(x,y)x<=y, true );
                    c = clamp( (-opt.dim+i+1):(i), 1, opt.dim, @(x,y)x<=y, true );
                case 'anti';
                    r = clamp( i:-1:(-opt.dim+i+1), 1, opt.dim, @(x,y)x<=y, true );
                    c = fliplr( r );
                otherwise;
                    fatal_error; end;
            idx = sub2ind( [opt.dim opt.dim], r, c );
            mp.A(row, opt.shift + idx) = 1;
            mp.rhs(1,row) = bd(i);
            mp.sense(1,row) = sense; 
            end;
    else;
        error( 'optperron:diagsum', 'Given vector has wrong number of elements.' ); end;

end

% diagonal dominance

function [ mp ] = mp_posdiagonaldominant( mp, opt, bd, sense )
    bd = str2numt( bd );
    if( isempty(bd) );
        bd = 0; end;
    mp.mpgnfo = vprintf( '''posdiagdom%s, %\[r, ', sense, bd, 'imp',[2 opt.verbose], 'str',mp.mpgnfo );
    if( isscalar(bd) );
        bd = bd * ones( 1, opt.dim ); end;
    
    switch sense;
        case 'dom'; sense = '<';  % entries of diagonal is larger than sum of row entries
        case 'sub'; sense = '>';  % entries of diagonal is smaller than sum of row entries
        case 'eq';  sense = '=';  % entries of diagonal equals sum of row entries
        otherwise; fatal_error; end;
       
    Anew = spzeros( opt.dim, 0 );
    for i = 1:opt.dim
        blocki = speye( opt.dim );
        blocki(i,i) = -1; %#ok<SPRIX>
        Anew = [Anew blocki]; end;  %#ok<AGROW>
    rows = (size( mp.A, 1 )+1):(size( mp.A, 1 )+opt.dim);
    mp.A(rows,(opt.shift+1):(opt.shift+opt.dim^2)) = Anew;
    mp.rhs(1,rows) = -bd;
    mp.sense(1,rows) = sense;

end

function [ mp ] = mp_monotone( mp, opt, bd, sense )
    bd = str2numt( bd );
    if( isempty(bd) );
        bd = 0; end;
    mp.mpgnfo = vprintf( '''monotone%s'', %\[r, ', sense, bd, 'imp',[2 opt.verbose], 'str',mp.mpgnfo );
    assert( isscalar(bd) );
    
    switch sense;
        case 'inc'; sense = '<';  % lower left corner is smaller eq than upper right corner
        case 'dec'; sense = '>';  % lower left corner is greater eq than upper right corner
        case 'eq';  sense = '=';
        otherwise; fatal_error; end;    
        
    block1 = [speye( opt.dim ) -speye( opt.dim )];
    block2 = [spzeros( opt.dim-1,1 ) speye( opt.dim-1 )] - [ speye( opt.dim-1 ) spzeros( opt.dim-1,1 )];
    block01 = spzeros( opt.dim );
    block02 = spzeros( opt.dim-1, opt.dim );
    for i = 1:opt.dim-1;
        rows = (size( mp.A, 1 )+1):(size( mp.A, 1 )+opt.dim);
        mp.A(rows,(opt.shift+1):(opt.shift+opt.dim^2)) = [repmat( block01, 1, i-1 ) block1 repmat( block01, 1, opt.dim-i-1 )];
        mp.rhs(1,rows) = -bd;
        mp.sense(1,rows) = sense; end;
    
    for i = 1:opt.dim;
        rows = (size( mp.A, 1 )+1):(size( mp.A, 1 )+opt.dim-1);
        mp.A(rows,(opt.shift+1):(opt.shift+opt.dim^2)) = [repmat( block02, 1, i-1 ) block2 repmat( block02, 1, opt.dim-i )];
        mp.rhs(1,rows) = -bd;
        mp.sense(1,rows) = sense; end;
end

function [ mp ] = mp_diagdiff( mp, opt, args );

    [args, cellflag] = tocell( args );

    % parse name-value pairs
    [N, args] = parsem( {'random','rand','N'}, args, [] );
    [didx, args] = parsem( {'didx','diagidx','diag','diagonal','diagonalidx','idx'}, args, [] );
    [shift, args] = parsem( {'shift','band','distance','dist'}, args, [] );
    [difference, args] = parsem( {'difference','diff'}, args, [] );
    
    
    
    % parse N   ... number of conditions
    if( isempty(N) );
        if( isempty(args) || isequal(args{1},'random') || isequal(args{1},'rand') );
            N = randi( [1, ceil(opt.random*opt.dim*(opt.dim-1))+1] );
            args = [];
        elseif( ~cellflag && numel(args{1}) == 1 && isscalar(args{1}) );
            assert( args{1}  >= 0 );
            N = args{1};
            args = {[]};
        else;
            N_ = cellfun( 'prodofsize', args ); 
            N = max( N_ ); end;
        if( N == 0 );
            return; end; end;
    
    % parse `sense`
    idx = find( cellfun( @(x) (ischar(x) || isstring(x)) && all(x=='<' | x=='=' | x=='>'), args ) );
    if( numel(idx) == 1 );
        sense = args{idx};
    elseif( numel(idx) == 0 );
        sense = char( randi([60 62], [1 N]) );        
    else;
        error( 'mpgallery:diffdiag', 'Wrong arguments given.' ); end;
    if( numel(sense) == 1 );
        sense = char( sense*ones( 1, N ) ); end;
    args(idx) = [];
    
    % parse `colrow`
    idx = find( cellfun( @(x) (ischar(x) || isstring(x)) && all(x=='r' | x=='c'), args ) );
    if( numel(idx) == 1 );
        colrow = args{idx};
    elseif( numel(idx) == 0 );
        colrow = randcollection( 'cr',[1 N] );
    else;
        error( 'mpgallery:diffdiag', 'Wrong arguments given.' ); end;
    if( numel(colrow) == 1 );
        colrow = char( colrow*ones( 1, N ) ); end;
    args(idx) = [];
    
    % parse didx    ... which diagonal elements shall be restricted
    if( isempty(didx) );
        if( cellflag && numel(args) >= 1 && isnumeric(args{1}) );
            didx = args{1};
            args(1) = [];
        else;
            didx = randi( [1 opt.dim], [1 N] ); end; end;
    if( numel(didx) == 1 );
        didx = didx*ones( 1, N ); end;

    % parse shift   ... distance from diagonal-element which shall be restricted
    if( isempty(shift) );
        if( numel(args) >= 2 );
            shift = args{2};
        else;
            shift = randcollection( [ceil(-opt.dim/2):-1 1:floor(opt.dim/2)], [1 N] ); end; end;
    if( numel(shift) == 1 );
        shift = shift*ones( 1, N ); end;         
    
    % parse diff
    if( isempty(difference) );
        if( numel(args) >= 3 );
            difference = args{3};
        else;
            difference = opt.dim * randn( [1 N] ); end; end;
    if( numel(difference) == 1 );
        difference = difference*ones( 1, N ); end;
    
    assert( all( diff( [numel(sense) numel(colrow) numel(difference) numel(shift) numel(didx)] ) == 0 ), 'mpgallery:diffdiag', 'Wrong arguments given.\n  All entries must have the same number of elements, or exactly 1 element.' );
    assert( isempty(args) || isequal(args,{[]}), 'mpgallery:diffdiag', '  Wrong arguments given.\n  Some argument could not be parsed.' );
    assert( all(sense=='<' | sense=='=' | sense=='>'), 'mpgallery:diffdiag', '  Wrong arguments given.\n  `sense` entries contain a not valid character.\n  Valid: `<`,`=`,`>`.' );
    assert( all(colrow=='c' | colrow=='r'), 'mpgallery:diffdiag', '  Wrong arguments given.\n  `colrow` entries contain a not valid character.\n  Valid: `c`,`r`' );
    assert( all(shift ~= 0 & shift>=-opt.dim+1 & shift<=opt.dim-1) , 'mpgallery:diffdiag', '  Wrong arguments given.\n  `shift` entries not in a valid range.\n  Valid: dim+1 to dim-1, but not zero.' );
    assert( all(didx > 0 & didx<=opt.dim), 'mpgallery:diffdiag', '  Wrong arguments given.\n  `didx` entries not in a valid range.\n  Valid: 1 to dim' );
    
    if( N <= 3 );
        mp.mpgnfo = vprintf( '''diffdiag'', {didx: %\[r, shift: %\[r, diff: %\[r, %\[r, %\[r}), ', didx,shift,difference,sense,colrow, 'imp',[1 opt.verbose], 'str',mp.mpgnfo );
    else;
        mp.mpgnfo = vprintf( 'diffdiag'', {\n  didx: %\[r,\n  shift: %\[r,\n  diff: %\[r,\n  %\[r,\n  %\[r\n}), ', didx,shift,difference,sense,colrow, 'imp',[1 opt.verbose], 'str',mp.mpgnfo ); end;
    for n = 1:N;
        didx1 = sub2ind( [opt.dim opt.dim], didx(n), didx(n) );
        switch colrow(n);
            case 'c'; didx2 = sub2ind( [opt.dim opt.dim], clamp( didx(n)-shift(n), 1, opt.dim ), didx(n) );
            case 'r'; didx2 = sub2ind( [opt.dim opt.dim], didx(n), clamp( didx(n)-shift(n), 1, opt.dim ) );
            otherwise; fatal_error; end;
        row = size( mp.A, 1 ) + 1;
        mp.A(row,opt.shift+didx1) = 1;
        mp.A(row,opt.shift+didx2) = -1;
        mp.rhs(row) = difference(n);
        mp.sense(row) = sense(n); end;
    
        
        
end

%% elementwise
% full matrices

function [ mp ] = mp_elementwise_bd( mp, opt, bd, sense )
    idx = opt.shift+1:opt.shift+opt.dim^2;
    switch sense;
        case 'lb';
            bd(isnan( bd )) = -inf;
            mp.lb(idx) = max( mp.lb(idx), bd(:).' );
            if( opt.redundantbound );
                row = size( mp.A, 1 );
                for i = 1:numel( bd );
                    if( isfinite(bd(i)) );
                        row = row + 1;
                        mp.A(row,i) = 1;
                        mp.rhs(1,row ) = bd(i);
                        mp.sense(1,row) = '>'; end; end; end;
        case 'ub';
            bd(isnan( bd )) = inf;
            mp.ub(idx) = min( mp.ub(idx), bd(:).' );
            if( opt.redundantbound );
                row = size( mp.A, 1 );
                for i = 1:numel( bd );
                    if( isfinite(bd(i)) );
                        row = row + 1;
                        mp.A(row,i) = 1;
                        mp.rhs(1,row ) = bd(i);
                        mp.sense(1,row) = '<'; end; end; end;            
        otherwise; fatal_error; end;
        
end

function [ mp ] = mp_elementwise_eq( mp, opt, bd )
    if( numel(bd) == 1 );
        bd = bd * ones( 1, opt.dim ); end;
    bd = bd(:)';
    
    Anew = speye( opt.dim^2 );
    idx_remove = isnan( bd );
    idx_keep = opt.shift + find( ~idx_remove );
    bd(idx_remove) = [];
    Anew(idx_remove(:),:) = [];
    
    row = size( mp.A, 1 );
    rows = (row + 1):(row + size( Anew, 1 ));
    cols = opt.shift+1:opt.shift+opt.dim^2;
    mp.A(rows,cols) = Anew;
    mp.rhs(1,rows) = bd;
    mp.sense(1,rows) = '=';
    mp.lb(1,idx_keep) = bd;
    mp.ub(1,idx_keep) = bd;
end

function [ mp ] = mp_elementwise( mp, opt, bd, sense );
    bd = str2numt( bd );
    assert( isreal(bd), 'mpgallery:elementwise', 'Prescribed bounds must be real valued.' );
    bdprint = bd;
    if( isempty(bd)  || islogical(bd) );
        bd = sort( full( sprandn( 1, opt.dim^2, rand ) ), 1 ); 
        bd(bd == 0) = nan; 
        bdprint = bd; end;
    
    if( numel(bd) == 1 )
        bdprint = bd;
        bd = repmat( bd, [1 opt.dim^2] ); end;
    mp.mpgnfo = vprintf( '''elementwise%s'', %\[r, ', sense, bdprint, 'imp',[2 opt.verbose], 'str',mp.mpgnfo );
    assert( numel(bd) == opt.dim^2, 'mpgallery:elementwise', 'Given number of bounds for elementwise must be 1 or dim^2.' );

    switch sense;
        case {'lb','ub'}; 
            mp = mp_elementwise_bd( mp, opt, bd, sense );
        case {'eq'};
            mp = mp_elementwise_eq( mp, opt, bd );
        otherwise; fatal_error; end;
end

function [ mp ] = mp_supp( mp, opt, Supp );
    if( iscell(Supp) );
        if( isscalar(Supp{1}) );
            epsilon = Supp{1};
            G = Supp{2};
        else;
            epsilon = Supp{2}; 
            G = Supp{1}; end;
        assert( epsilon  >= 0, 'mpgallery:supp', 'The ''epsilon'' parameter to ''supp'' must be greater equal 0.' );
    else;
        G = Supp;
        epsilon = 0; end;
    G = double( G );
    idx = G == 0;
    G(idx) = epsilon;
    G(~idx) = nan;
    if( epsilon == 0 );
        mp = mp_elementwise( mp, opt, G, 'eq' ); 
    else;
        mp = mp_elementwise( mp, opt, G, 'ub' );
        mp = mp_elementwise( mp, opt, G, 'lb' ); end; 
end

% only diagonals

function [ mp ] = mp_diagonal( mp, opt, bd, diag, sense );
        
    row = size( mp.A, 1 );
    bd = str2numt( bd );
    if( isempty(bd)  || islogical(bd) );
        bd = randn( 1, opt.dim ); end;

    mp.mpgnfo = vprintf( '''%sdiagonal%s'', %\[r, ', diag, sense, bd, 'imp',[2 opt.verbose], 'str',mp.mpgnfo );
    
    switch sense;
        case 'lb'; sense = '>';
        case 'ub'; sense = '<';
        case 'eq'; sense = '='; 
        otherwise; fatal_error; end;    
    
    for r = 1:opt.dim
        if( numel(bd) == 1 );
            bd_ = bd;
        else;
            bd_ = bd(r); 
            if( bd_ == 0 );
                continue; end; end;
        row = row + 1;
        switch diag
            case {'main'}; idx = sub2ind( [opt.dim opt.dim], r, r );
            case {'anti'}; idx = sub2ind( [opt.dim opt.dim], opt.dim-r+1, r );
            otherwise; fatal_error; end;
        mp.A(row, opt.shift + idx) = 1;
        mp.rhs(1,row) = bd_;
        mp.sense(1,row) = sense;
    end
end

%% misc matrix properties

function [ mp ] = mp_1norm( mp, opt );
    mp.mpgnfo = vprintf( '''1norm'', ', 'imp',[2 opt.verbose], 'str',mp.mpgnfo );
    row = size( mp.A, 1 );
    current_size = size( mp.A, 2 );
    mp.lb( (opt.shift+current_size+1):(opt.shift+current_size+opt.dim^2) ) = 0;
    mp.ub( (opt.shift+current_size+1):(opt.shift+current_size+opt.dim^2) ) = inf;
    for r = 1:opt.dim^2;
        row = row + 1;
        mp.A(row, opt.shift + r) = 1;
        mp.A(row, opt.shift + r + current_size) = -1; 
        mp.rhs(1,row) = 0;
        mp.sense(1,row) = '<';
        
        row = row + 1;
        mp.A(row, opt.shift + r ) = 1;
        mp.A(row, opt.shift + r + current_size) = 1;
        mp.rhs(1,row) = 0;
        mp.sense(1,row) = '>';
    end;
    for r = 1:opt.dim;
        row = row + 1;
        idx = current_size + (1 + (r-1)*opt.dim : (r * opt.dim));
        mp.A(row, opt.shift + idx) = 1;
        mp.rhs(1,row) = 1;
        mp.sense(1,row) = '<';
    end
end


function [ mp ] = mp_halfplane( mp, opt, plane, sense );
    % (X, Aj) <= bj
    % Aj = plane{j}{1}
    % bj = plane{j}{2}
    
    switch sense;
        case {'lb','>'}; sense = '>';
        case {'ub','<'}; sense = '<';
        case {'eq','='}; sense = '='; 
        otherwise; fatal_error; end;    
    
    row = size( mp.A, 1 );
    if( ~iscell(plane) || islogical(plane) );
        A = (-1)^randi(2) * randi( 10, opt.dim );
        b = (-1)^randi(2) * randi( 10 * opt.dim );
        plane = {A,b}; end;
    if( ~iscell(plane{1}) );
        plane = {plane}; end;
    J = numel( plane );

    for j = 1:J;
        mp.mpgnfo = vprintf( '''halfplane'', {%\[r,  %\[r}, ', plane{j}{1}, plane{j}{2}, 'imp',[2 opt.verbose], 'str',mp.mpgnfo );
        row = row + 1;
        mp.A(row, opt.shift + (1:opt.dim^2)) = reshape( plane{j}{1}, 1, [] );
        mp.rhs(1,row) = plane{j}{2};
        mp.sense(1,row) = sense;
        end; 
end

function [ mp ] = mp_magic( mp, opt, sum )
    mp.mpgnfo = vprintf( '''magicsquare'', ', 'imp',[2 opt.verbose], 'str',mp.mpgnfo );
    if( isempty(sum) );
        sum = (opt.dim^3 + opt.dim)/2; end;
    mp = mp_diagonal_sum( mp, opt, sum, 'main', 'eq' );
    mp = mp_diagonal_sum( mp, opt, sum, 'anti', 'eq' );
    mp = mp_colrow_sum( mp, opt, sum, 'c', 'eq' );
    mp = mp_colrow_sum( mp, opt, sum, 'r', 'eq' );
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% mutating

function [ mp ] = mp_addfrontback( mp, opt, data, frontback )
    mp.mpgnfo = vprintf( '''add%s'', {%s}, ', frontback, data, 'imp',[2 opt.verbose], 'str',mp.mpgnfo );
    assert( ischar(data) || isstring(data), 'mpgallery:add', 'Options ''add___'' need a character vector with entries ''<'',''u'',''>''.' );
    nv = numel( data );
    switch frontback;
        case {'front','f'};
            mp.shift = mp.shift + nv;
        case {'back','b'}; 
            % do nothing
        otherwise;
            fatal_error; end; 
    
    nr = size( mp.A, 1 );
    switch frontback;
        case {'front','f'};
            mp.A = [zeros( nr, nv ) mp.A];
        case {'back','b'};
            mp.A = [mp.A zeros( nr, nv ) ];
        otherwise;
            fatal_error; end;
    
    % mp.rhs: unchanged
    % mp.sense: unchanged
    lb = double( data );
    idx1 = lb=='<' | lb == 'u' | lb == '^';
    idx2 = lb=='>';
    assert( all(idx1 | idx2), 'mpgallery:add', 'Options ''add___'' need a character vector with entries ''<'',''u'',''>''.' );
    lb(idx1) = -inf;
    lb(idx2) = 0;
    
    ub = double( data );
    idx1 = ub=='<';
    idx2 = ub=='>' | ub == 'u' | ub == '^';
    assert( all(idx1 | idx2), 'mpgallery:add', 'Options ''add___'' need a character vector with entries ''<'',''u'',''>''.' );
    ub(idx1) = 0;
    ub(idx2) = inf;
    
    switch frontback;
        case {'front','f'}; 
            mp.lb = [lb mp.lb];
            mp.ub = [ub mp.ub];
        case {'back','b'};  
            mp.lb = [mp.lb lb];
            mp.ub = [mp.ub ub];
        otherwise;
            fatal_error; end;
end

function [ mp ] = mp_permute( mp, opt, perm )
    mp.mpgnfo = vprintf( '''permute'', {%v}, ', perm, 'imp',[2 opt.verbose], 'str',mp.mpgnfo );
    
    % checks whether perm contains all numbers from 1 to n
    assert( numel(perm ) == size(mp.A,2), 'mpgallery:permute', 'Permutation vector must have equally many entries as there are variables in the mathematical program (i.e. equal to size(mp.A,2))' );
    assert( all(diff(sort(perm)) == 1), 'Permutation vector ist not a permutation; Some indices are missing.' );
    
    mp.A = mp.A(:,perm);
    mp.lb = mp.lb(perm);
    mp.ub = mp.ub(perm);
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% misc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ flag, opt ] = check( randflag, opt, names, varargin );
    % fast check first
    str_idx = cellfun( 'isclass', opt.what, 'char' );
    if( ~any(contains(opt.what(str_idx), names)) );
        if( numel(varargin) >= 1 );
            flag = varargin{1};
        else;
            flag = 0; end;
        cnt = 0;
    else;
        [flag, opt.what, ~, ~, cnt] = parsem( names, opt.what, varargin{:}, 'multiple',true );
        end;

    names_neg = add_minus( names );
    [~, opt.what, ~, ~, cnt_neg] = parsem( names_neg, opt.what, varargin{:}, 'multiple',true );
    if( ~cnt && opt.random > 0 );    
        if( ~cnt_neg );
            rand_choosen = rand < opt.random;
            if( randflag && rand_choosen );
                if( numel(varargin) >= 1 && ~isequaln(varargin{1},nan) );
                    flag = varargin{1};
                elseif( numel(varargin) >= 1 );
                    flag = [];
                else;
                    flag = true; end; end; end; end;
end

function [ names ] = add_minus( names );
    for i = 1:numel( names );
        names{i} = ['-' names{i}]; end;
end

function [ v ] = randcollection( collection, varargin );
    sz = [varargin{:}];
    N = numel( collection );
    v = randi( N, sz );
    v = collection(v);
end



function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*NBRAK2>
