function [ varargout ] = mpgallery( varargin );
% wrapper function for ipa
    n = nargout( 'gallery_mp' );
    varargout = cell( 1, max(nargout, n) );
    [varargout{:}] = gallery_mp( varargin{:} );
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
