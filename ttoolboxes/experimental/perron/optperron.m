function [ X, la, nfo ] = optperron( varargin );

%   XX The left-right method cannot work for symmetric matrices, since left and right eigenvectors are the same
%   XX Example which has admissible matrix of form [A *; 0 B], but a global minimum does not have a zero in the corner
%   maximization, matrices without zers / with zeros
%   looking for example where decreasing lower bound works better
%   XX Frank wolf: Use difference to current leading eigenvector as metric
%   XX Try hit-and-run algorithm to generate starting matrices
%   XX Try to estimate rate of convergence in small dimensions (3 to 5) only of left-right step
%   XX Only do some iterations, and then restart with a new starting vector, since the largest progress is always made in the first few steps

    [opt, varargin] = parse_input( varargin{:} );
    [Dom, X, opt] = make_Dom( opt, varargin{:} );
    nfo.start = X;
    if( opt.profile );
        clear_profile = scoped_profile(); end;  %#ok<NASGU>
    
    time_start = tic;
    [X, la] = optimize( Dom, X, opt );
    nfo.duration = toc( time_start );
    
    vprintf( 'Finished: rho = %15.15g\n', la(1,end), 'imp',[1 opt.verbose] );
end

function [ opt, varargin ] = parse_input( varargin );

    opt.hash_input = thash( varargin, [], 8 );
    
    % input options
    [opt.dim, varargin]                    = parsem( {'dim'}, varargin, [], 'postprocess',@str2numt );                      % Dimension of problem
    [opt.X, varargin]                      = parsem( {'X','start','seed_example','example','ex','seed'}, varargin, [] );    % start matrix, or seed for starting examplef
    [opt.epsX, varargin]                   = parsem( {'starteps','startepsilon','epsilonstart','epsstart','epsex','exeps','epsilonex','epsilonexample'}, varargin, [], 'postprocess',@str2numt );            % Epsilon to use to make starting matrix fully positive
    [~, varargin, opt.minmax]              = parsem( {'min','max'}, varargin );                                             % Whether to search for minimal or maximal Perron eigenvalue
    
    % worker (optimization) options
    [opt.eps, varargin]                    = parsem( {'eps','epsilon'}, varargin, [], 'postprocess',@str2numt );            % Epsilon to use to make all matrices fully positive
    [opt.find_far_vertex_mode, varargin]   = parsem( {'find_far_vertex_mode','findfarvertexmode','ffvm'}, varargin, 't' );  % Which method to use to find a new far away vertex
    [opt.optimize, varargin]               = parsem( {'optimize','opt','method'}, varargin, 'all' );                        % which optimization methods to use
    [opt.zerohandling, varargin]           = parsem( {'zerohandling'}, varargin, 'auto', 'postprocess',@(x) erase(lower(x), {'_','-'}) );  % how to handle non-strictly-positive matrices in the eigenvector setp
    [opt.gurobi_method, varargin]          = parsem( {'gurobi_method','gurobi-method','gurobimethod'}, varargin, 'auto' );  % which method shall the gurobi solver use: -1=automatic, 0=primal simplex, 1=dual simplex, 2=barrier, 3=concurrent, 4=deterministic concurrent, 5=deterministic concurrent simplex.
    [opt.maxnoprogress, varargin]          = parsem( {'maxnoprogress','extraround'}, varargin, 0 );                         % algorithm stops after maxnoprogress rounds of no progess
    [opt.num_try, varargin]                 = parsem( {'numtry'}, varargin, 20, 'postprocess',@str2numt );                   % example generation stops when faileafter num_try rounds
    
    % output options
    [opt.verbose, varargin]                = parsem( {'verbose','v'}, varargin, 1, 'postprocess',@str2numt );               % verbosity level
    [opt.figure, varargin]                 = parsem( {'plot','figure'}, varargin, [] );  % in which figure shall the plot 
    [opt.step, varargin]                   = parsem( {'step','stepping','pause','break'}, varargin );                       % whether to wait for keyboard input after each iteration
    [opt.profile, varargin]                = parsem( {'profile'}, varargin );                                               % whether to profile the optimization steps
    [opt.infeasible, varargin]             = parsem( {'infeasible'}, varargin, 'error' );                                   % behaviour when an infeasible domain is given
    [opt.epsvisual, varargin]              = parsem( {'epsvisual,visualeps'}, varargin, 1e-9 );                             % output is filtered for relative spectral radius changes which are less then epsvisual
    [opt.plotscaled, varargin]             = parsem( {'plotscaled','scaledplot','scaleplot'}, varargin, 1 );                % whether to use a special scale for plotting of rho - values
    
    if( isempty(opt.X) );
            opt.X = 'opt'; end;
    if( isequal(opt.X, 'opt') );
        opt.X = opt.minmax; end;
    if( isscalar(opt.X) );
        oldrng = rng();
        rng( opt.X );
        opt.seed = onCleanup( @() rng(oldrng) );
        opt.X = []; end;
    
    if( isempty(opt.dim) );
        if( isempty(opt.X) );
            opt.dim = 4;
        else;
            opt.dim = size( opt.X, 1 ); end; end;
    
    if( isempty(opt.figure) );
        opt.figure = 1 * (opt.verbose >= 2); end;
    if( opt.figure );
        plotm( 'clf',opt.figure ); end;
    
    if( isanyof(opt.zerohandling, {[], 'auto','def','default'}) );
        switch opt.minmax;
            case 'min'; opt.zerohandling = 'decompose';
            case 'max'; opt.zerohandling = 'off'; 
            otherwise; fatal_error; end; end;
    
    if( isempty(opt.eps) );
        if( isanyof(opt.zerohandling, {'off','decompose'}) );
            opt.eps = 0;
        else;
            opt.eps = 9.765625e-4; end; end;
    
    if( isempty(opt.epsX) );
        opt.epsX = opt.eps; end;
    
    switch opt.gurobi_method
        case {-1, 'auto','automatics','def','default'};                                                                     opt.gurobi_method = -1;
        case { 0, 'primal','primal_simplex','primal-simplex','primalsimplex','simplex'};                                    opt.gurobi_method =  0;
        case { 1, 'dual','dual_simplex','dual-simplex','dualsimplex'};                                                      opt.gurobi_method =  1;
        case { 2, 'barrier','interior_point','interior-point','interiorpoint','ip'};                                        opt.gurobi_method =  2;
        case { 3, 'concurrent'};                                                                                            opt.gurobi_method =  3;
        case { 4, 'deterministic_concurrent','deterministic-concurrent','deterministicconcurrent'};                         opt.gurobi_method =  4;
        case { 5, 'deterministic_concurrent_simplex','deterministic-concurrent-simplex','deterministicconcurrentsimplex'};  opt.gurobi_method =  5;
        otherwise;
            error( 'optperron:args', 'Wrong argument given option ''gurobi_method''.' ); end;
    
    opt.counter = 1;
    
end

function [ Dom, X, opt ] = make_Dom( opt, varargin );
% For same example start from different starting point

    for i = 1:opt.num_try;
        Dom = mpgallery( 'dim',opt.dim, 'lb',0, 'v',-1 );
        if( ~isempty(opt.X) && isanyof(size(opt.X), {[opt.dim opt.dim],[opt.dim 1]}) );
            Dom = mpgallery( 'model',Dom, 'dim',opt.dim, varargin{:}, 'v',opt.verbose-3 );
            X = opt.X;
        else;
            [ Dom, X0 ] = mpgallery( 'numtry',opt.num_try, 'model',Dom, 'dim',opt.dim, varargin{:}, 'ex',opt.X, 'v',opt.verbose-1 );
            [ ~, X, ~ ] = mpgallery( 'numtry',opt.num_try, 'model',Dom, 'dim',opt.dim, 'lb',opt.epsX, 'ex',opt.X, 'v',-1 );
            vprintf( '%s\n', Dom.mpgnfo, 'imp',[1 opt.verbose] );
            end;

        opt.hash_Dom = thash( Dom, [], 8 );
        opt.hash_X = thash( X, [], 8 );
        vprintf( 'Id (Input): %s\n', opt.hash_input, 'imp',[1 opt.verbose] );  % equivalent models can have the same id, thus we do not call it hash
        vprintf( 'Id (Dom): %s\n', opt.hash_Dom, 'imp',[1 opt.verbose] );
        if( ~isempty(X) );
            vprintf( 'Id ( X ): %s\n', opt.hash_X, 'imp',[1 opt.verbose] ); end;

        if( isempty(X) && isempty(X0) );
            switch opt.infeasible;
                case {'default','error','warning','exception'};
                    error( 'optperron:infeasible', 'Infeasible problem given.' );
                case 'repeat';
                    vprintf( '.', 'imp',[1 opt.verbose] ); 
                    continue; 
                case 'return';
                    Dom = [];
                    X = [];
                    return; 
                otherwise;
                    error( 'optperron:args', 'Wrong argument given for option ''infeasible''.' ); end;
        elseif( isempty(X) );
            switch opt.infeasible;
                case {'default','error','warning','exception'};
                    warning( 'optperron:infeasible', 'Problem given may not have a strictly positive solution.' );
                case 'repeat';
                    vprintf( '.', 'imp',[1 opt.verbose] ); 
                    continue; 
                case 'return';
                    Dom = [];
                    X = [];
                    return; 
                otherwise;
                    error( 'optperron:args', 'Wrong argument given for option ''infeasible''.' ); end;
        else;
            break; end; end;
    if( ~isvector(X) );
        assert( test_inclusion( X, Dom, opt ) ); end;
end

function [ X, la ] = optimize( Dom, X, opt );
    
    if( ~isvector(X) );
        rX = rho( X ); 
    else;
        rX = nan; end;
    vprintf( 'Start:      \trho: %15.15g\n', rX, 'imp',[1 opt.verbose] );
    la = [rX; 35];  % 35 = value of 'Start' worker. Ugly, but works.
    [X, opt, la] = optimize_worker_wrapper( X, Dom, opt, la, 'Start', 'once', @optimize_null );
    [X, opt, la] = optimize_worker_wrapper( X, Dom, opt, la, 'Eigenvector', 'multiple', @optimize_eigenvalue );
    
    no_progress_counter = 0;
    switch opt.minmax;
        case 'min'; cmp = @(l,r) l<r;
        case 'max'; cmp = @(l,r) l>r; 
        otherwise; fatal_error; end;
    while( true );
        vprintf( '---------------------------------------------\nStart optimization round:\n', 'imp',[2 opt.verbose] );
        X_old = X;
        la_old = rho( X );
        
        [X, opt, la] = optimize_worker_wrapper( X, Dom, opt, la, 'Frank-Wolfe', 'once', @optimize_Frank_Wolfe );
        
        [X, opt, la] = optimize_worker_wrapper( X, Dom, opt, la, 'Frank-Wolfe', 'once', @optimize_Frank_Wolfe );
        
        [X, opt, la] = optimize_worker_wrapper( X, Dom, opt, la, 'Deoptimize 1-norm', 'once', @deoptimize_1norm );
        
        [X, opt, la] = optimize_worker_wrapper( X, Dom, opt, la, 'Elementwise monotone', 'once', @optimize_monotone );
        
        [X, opt, la] = optimize_worker_wrapper( X, Dom, opt, la, 'Maximum acyclic graph', 'once', @optimize_mas );
        
        [X, opt, la] = optimize_worker_wrapper( X, Dom, opt, la, 'Eigenvector', 'multiple', @optimize_eigenvalue );
        
        %[X, opt, la] = optimize_worker_wrapper( X, Dom, opt, la, 'Eigenvector MAS', 'multiple', @optimize_eigenvalue_mas );
        
        
        if( abs(la(1,end) - la_old) <= 1e-9 );
            no_progress_counter = no_progress_counter + 1;
            if( opt.maxnoprogress );
                vprintf( '     (nearly no progress %i/%i)\n', no_progress_counter, opt.maxnoprogress, 'imp',[1 opt.verbose] ); end;
        elseif( cmp(la_old, la(1,end)) );
            no_progress_counter = no_progress_counter + 1;
            if( opt.maxnoprogress );
                vprintf( '     (no progress %i/%i)\n', no_progress_counter, opt.maxnoprogress, 'imp',[1 opt.verbose] ); end;
            la(1,end) = la_old;
            X = X_old;
        else;
            no_progress_counter = 0; end;
        
        if( no_progress_counter>=opt.maxnoprogress );
            opt.maxnoprogress = ceil( opt.maxnoprogress*1.5 );
            break; end;
        
        end;
        
        la = [la [rho( X ); 70]];  % 35 = value of 'End' worker. Ugly, but works.
        visualize_data( X, la, 'End', opt );
end

function [ X, opt, la, skipped ] = optimize_worker_wrapper( X, Dom, opt, la, name, onceflag, handle, varargin )
    % varargin is passed to handle as last arguments
    vprintf( 'Step: %s:\n', name, 'imp',[1 opt.verbose] );
    while( true );
        X_old = X;
        if( isvector(X) );
            la_old = nan;
        else;
            la_old = rho( X ); end;
        
        [X_new, opt_new, la_new, skipped, again] = handle( X, Dom, opt, la(1,end), varargin{:} );
        if( skipped );
            vprintf( repmat( '\b', [1 8+numel(name)]), 'imp',[1 opt.verbose] );
            break; end;
        
        X = X_new;
        opt = opt_new;
        la(1,end+1) = la_new;  %#ok<*AGROW>
        la(2,end) = mod( sum(thash(func2str(handle))), 255 );
        
        % visualize data
        visualize_data( X, la, name, opt, varargin{:} );
        
        % update variables
        opt.counter = opt.counter + 1; 
        opt.eps = opt.eps / 2;        
        
        % check termination conditions
        if( again );
            continue; end;
        [X, flag] = check_termination_conditions( X, X_old, la, la_old, onceflag, opt );
        if( flag );
            break; end; end;
        
    
end

function visualize_data( X, la, name, opt, varargin );
    if( isequal(opt.minmax, 'min') );
        % compute sanitized X
        X_san = X;
        idx = abs( X_san(:)-opt.eps)<1e-9;
        X_san(idx) = 0;
        la_san = rho( X_san );
        san_diff = abs(la_san/la(1,end)-1); end;

    rel_change = abs(la(1,end)/la(1,end-1)-1);
    vprintf( '  Iter %3i%v:   \t', opt.counter, varargin, 'imp',[1 opt.verbose] );
    if( rel_change>opt.epsvisual );
        vprintf( 'rho: %15.15g\n', la(1,end), 'imp',[1 opt.verbose] ); 
    else;
        vprintf( [repmat('\b', [1 13+ceil(log10(opt.counter+1))]) '\n'], 'imp',[1 opt.verbose] ); end;
    if( rel_change>opt.epsvisual && opt.verbose >= 1 );
        fprintf( '\b \trelative change: %3.3e\n', rel_change ); end;
    if( isequal(opt.minmax, 'min') && san_diff > 100*opt.epsvisual && opt.verbose >= 1 );
        fprintf( '\b  \t(rho sanitized: %15.15g)\n', la_san ); end;
    if( opt.verbose >= 3 );
        vprintf( '  Matrix: \n%v\n\n', X );
        vprintf( '  Evector: %v\n\n', leadingeigenvector(X, 'nonnegative')' ); end;


    title_string = ['Id: ' opt.hash_Dom ' / Input: ' opt.hash_input ' / X: ' opt.hash_X];
    if( opt.figure );
        figure_lazy( opt.figure );
        sgtitle( title_string );
        try;
            sorted = uniquetol( la(1,:) );
        catch me;  %#ok<NASGU>
            fprintf( 2, 'No visualization if start is given by vector currently.\n' );
            return; end;

        x_iter = 1:size( la, 2 );
        d = quot( la(1,:) );
        idx = [true abs(1-d)>opt.epsvisual];
        if( isanyof( opt.plotscaled, {true,1} ) );
            if( numel(sorted) >= 2 );
                yscale_shift = 2*sorted(1) - 1.5*sorted(2);
            else;
                yscale_shift = 0; end;
            if( la(1,end)/yscale_shift < (1+eps) );
                yscale_shift = yscale_shift * 0.999999; end;
            scaled = {'yscale', {@(x) log(x - yscale_shift), 1e-8, [0 1e5], [], @(x) exp(x)+yscale_shift} };
        elseif( isanyof( opt.plotscaled, {'log'} ) );
            scaled = {'logy'};
        else;
            scaled = {}; end;

        subplot_lazy( 1 );
        plotm( [x_iter(idx); la(1,idx)], 'color',mod(la(2,idx), 53), '.k-', 'markersize',10, scaled{:} );
        %caxis([0 53])
        zoom out;
        
        pos = la(2,end);
        idx = unique( la(2,:), 'stable' );
        idx = find( idx == pos );
        subplot_lazy( 1+idx );
        imagesc( X );
        title( [name ', rho = ' num2str(la(1,end))] );
        set( gca, 'ColorScale','log' );
        colorbar;
        drawnow; end;
end

function [ X, flag ] = check_termination_conditions( X, X_old, la, la_old, onceflag, opt );
    flag = false;
    if( opt.step );
        vprintf( 'Press any key to continue.', 'cpr',[0.5 0.5 0] );
        pause; 
        vprintf( repmat('\b',[1 26]) ); end;

    if( abs(la(1,end) - la_old)<1e-5 || ...
        isequal(opt.minmax, 'min') && la(1,end) == 0 ...
      );
        flag = true;
    elseif( isequal(opt.minmax, 'min') &&  la(1,end)>la_old || ...
            isequal(opt.minmax, 'max') &&  la(1,end)<la_old ...
          ) ;
        X = X_old;
        flag = true; 
    elseif( isequal(onceflag, 'once') );
        flag = true; end;
end

%%

function [ X, opt, la, skipped, again ] = optimize_null( X, Dom, opt, la );  %#ok<INUSD,INUSL>
    if( isvector(X) );
        la = nan;
    else;
        la = rho( X ); end;
    skipped = false;
    again = false;
end

function [ X, opt, la, skipped, again ] = deoptimize_1norm( X, Dom, opt, la );
    % deoptimize 1-norm
    % searches for matrix with de-optimized (e.g. maximal) 1-norm, but with same or better(e.g. smaller) spectral radius
    again = false;
    if( isempty(regexpi(opt.optimize, 'all|deopt', 'once')) || ...
        ~isempty(regexpi(opt.optimize, 'nodeopt', 'once')) ...
      );
        skipped = true;
        return; end;
    
    skipped = false;
    for left_right = {'r','l'};
        v = leadingeigenvector( X,  'nonnegativeeigenvector', 'leftright',left_right{1} );
        expect( opt.verbose  >= 0, numel(v) == 1, 'optperron:leadingeigenvector', 'Leading eigenvector is not unique. I take an arbitrary one. The algorithm may suffer and produce nonsense results.' );
        if( numel(v) == 0 );
            return; end;
        v = v{1};

        model = Dom;
        if( opt.eps > 0 );
            model = mpgallery( 'model',model, 'dim',opt.dim, 'lb',opt.eps, 'v',-1 ); end;  % X(:) >= eps
        V = spalloc( opt.dim, size( Dom.A, 2 ), opt.dim^2 );

        % X*v = la_t*v, v: fixed, la_t<>la
        nextra = size( model.A, 2 ) - opt.dim^2;
        switch left_right{1};
            case 'l';
                V = kron( v', [eye(opt.dim) zeros(opt.dim, nextra)] );
            case 'r';
                V = kron( eye(opt.dim), [v' zeros(1,nextra)] );
            otherwise; fatal_error; end;

        model.A = [model.A; V];
        switch opt.minmax;
            case 'min'; model.sense(1,end+1:end+opt.dim) = '<';
            case 'max'; model.sense(1,end+1:end+opt.dim) = '>'; 
            otherwise; fatal_error; end;

        model.rhs(end+1:end+opt.dim) = la * v;

        n_slack = size( Dom.A, 2 ) - opt.dim^2;
        model.obj = [ones( 1, opt.dim^2 ) zeros( 1, n_slack )];
        switch opt.minmax;
            case 'min'; model.modelsense = 'max';  % sic!, we deoptimize the 1-norm
            case 'max'; model.modelsense = 'min'; 
            otherwise; fatal_error; end;
            
        
            
        mag_X = max( abs(X(:)) );
        mag_ub = max( abs(model.ub( model.ub<inf )) );
        model.ub( model.ub == inf ) = 10 * max( [mag_X mag_ub] );

        params.OutputFlag = opt.verbose >= 5;
        params.Method = 2;

        try;
            res = gurobi( model, params );
        catch;
            res.status = 'INFEASIBLE'; end;
        switch res.status;
            case 'OPTIMAL';
                Xout = reshape( res.x(1:opt.dim^2), [opt.dim opt.dim] );
                Xout( Xout <= 0 ) = 0;
                diffflag = norm( X - Xout ) > 5e-4;
                la_ = rho( Xout );
                if( diffflag && isequal(opt.minmax,'min') && la_<=la || ...
                    diffflag && isequal(opt.minmax,'max') && la_<=la ...
                  );
                    X = Xout;
                    la = la_; end;
            case {'UNBOUNDED', 'INF_OR_UNBD'};
                if( opt.verbose  >= 0 );
                    warning( 'optperron:unbounded', 'Problem may be unbounded. No result is returned. Please add additional constraints.\n' ); end;
                return;
            otherwise;
                vprintf( 'No solution found. ', 'imp',[2 opt.verbose] ); end; end;

end

function [ X, opt, la, skipped, again ] = optimize_monotone( X, Dom, opt, la );
    again = false;
    if( isempty(regexpi(opt.optimize, 'monotone|all', 'once')) || ...
        ~isempty(regexpi(opt.optimize, 'nomonotone', 'once')) ...
      );
        skipped = true;
        return; end;
    
    skipped = false;
    model = Dom;
    if( opt.eps > 0 );
        model = mpgallery( 'model',model, 'dim',opt.dim, 'lb',opt.eps, 'v',-1 ); end;  % X(:) >= eps
    
    % X <> Xk
    switch opt.minmax;
        case 'min'; model = mpgallery( 'model',model, 'dim',opt.dim, 'ub', X, 'v',-1 );
        case 'max'; model = mpgallery( 'model',model, 'dim',opt.dim, 'lb', X, 'v',-1 );
        otherwise; fatal_error; end;
        
    model.obj(1:opt.dim^2) = 1;
        switch opt.minmax;
        case 'min'; model.modelsense = 'min';
        case 'max'; model.modelsense = 'max';
        otherwise; fatal_error; end;
    params.OutputFlag = opt.verbose >= 5;
    params.Method = 2;
    try;
        res = gurobi( model, params );
    catch me;  %#ok<NASGU>
        res.status = 'INFEASIBLE'; end;
    switch res.status;
        case 'OPTIMAL';
            Xout = reshape( res.x(1:opt.dim^2), [opt.dim opt.dim] );
            Xout( Xout <= 0 ) = 0;
            diffflag = norm( X - Xout ) > 5e-4;
            la_ = rho( Xout );
            if( diffflag && isequal(opt.minmax,'min') && la_<=la || ...
                diffflag && isequal(opt.minmax,'max') && la_<=la ...
              );
                X = Xout;
                la = la_; end;
        case {'UNBOUNDED', 'INF_OR_UNBD'};
            if( opt.verbose  >= 0 );
                warning( 'optperron:unbounded', 'Problem may be unbounded. No result is returned. Please add additional constraints.\n' ); end;
            return;
        otherwise;
            vprintf( 'No solution found. ', 'imp',[2 opt.verbose] ); end;
end

%%

function [ X, opt, la, skipped, again ] = optimize_eigenvalue_mas( X, Dom, opt, la )  % do not return Dom, since we mutate it here
% given an eigenvector v, searches for the mimimum la such that X v <=> la v
    again = false;
    [left_right_set, skipped] = parse_optimize_eigenvalue_mas( opt );
    if( skipped );
        return; end;    
        
    for left_right = left_right_set;
        lr = left_right{1};
        % make model
        model = Dom;
        model.obj = [ones( 1, opt.dim^2 ) zeros( 1, n_slack )];  % optimize la
        model.modelsense = 'max';
        
        if( opt.eps > 0 );
            model = mpgallery( 'model',model, 'dim',opt.dim, 'lb',opt.eps, 'v',-1 ); end;  % X(:) >= eps

        perm = [];
        if( startsWith( opt.zerohandling, 'decompose' ) || ...
            isanyof( opt.zerohandling, {'block','blocktriangular','keepnull','keepzero','ev','eigenvector'} ) || ...
            isempty( opt.zerohandling ) ...
          );
            [~, v0, sze, perm] = compute_decomposition( X, lr, opt );  % should be replaced by mas version, computation of leading eigenvector is simple here
            model = mpgallery( 'model',model, 'dim',opt.dim, 'permute',[1 expand_perm(perm)+1 opt.dim^2+2:opt.dim^2+model.n_slack+1], 'v',-1 );
            model = set_variables_to_zero( model, sze, perm, opt );
            model = set_Blockv_eq_0( model, v0, sze, lr, opt );  % XX not yet written  % for each block: Block * v <=> v * la || w * Block <=> la * w
                
        elseif( isequal( opt.zerohandling, 'off' ) );
            v = compute_leading_eigenvectors_of_whole_matrix( X, lr, opt );
            if( isempty(v) );
                return; end;
            error( 'optperron:fatal', 'This code is not yet written.' );
            model = set_Xv_R_0( model, v, opt, lr );  %#ok<UNRCH>  %% XX not yet written  % X * v <=> v * la || w * X <=> la * w
                
        else;
            error( 'optperron:zerohandling', 'Wrong argument given to option ''zerohandling''.' ); end;

        [Xout, la] = solve_model_and_evaluate_results( model, la, perm, lr, opt ); 
        if( ~isempty(Xout) );
            if( startsWith( opt.zerohandling, 'decompose' ) || ...
                isanyof( opt.zerohandling, {'block','blocktriangular','keepnull','keepzero','ev','eigenvector'} ) || ...
                isempty( opt.zerohandling ) ...
              );
                [~, v0_new] = compute_decomposition( X, lr, opt );  % should be replaced by mas version, computation of leading eigenvector is simple here
                if( numel(v0_new)>numel(v0) );
                    again = true; end; end;
            X = Xout; end; end;
end

%%

function [ X, opt, la, skipped, again ] = optimize_eigenvalue( X, Dom, opt, la );
% given an eigenvector v, searches for the mimimum la such that X v <=> la v
    again = false;
    [left_right_set, skipped] = parse_optimize_eigenvalue( opt );
    if( skipped );
        return; end;    
    
    n_slack = size( Dom.A, 2 ) - opt.dim^2;
    for left_right = left_right_set;
        lr = left_right{1};
        % make model
        model = Dom;
        model = mpgallery( 'model',model, 'dim',opt.dim, 'addfront','>', 'v',-1 );
        model.obj = [1 zeros( 1, opt.dim^2 ) zeros( 1, n_slack )];  % optimize la
        model.modelsense = opt.minmax;
        
        if( opt.eps > 0 );
            model = mpgallery( 'model',model, 'dim',opt.dim, 'lb',opt.eps, 'v',-1 ); end;  % X(:) >= eps

        perm = [];
        if( startsWith( opt.zerohandling, 'decompose' ) || ...
            isanyof( opt.zerohandling, {'block','blocktriangular','keepnull','keepzero','ev','eigenvector'} ) || ...
            isempty( opt.zerohandling ) ...
          );
            [~, v0, sze, perm] = compute_decomposition( X, lr, opt );
            model = mpgallery( 'model',model, 'dim',opt.dim, 'permute',[1 expand_perm(perm)+1 opt.dim^2+2:opt.dim^2+n_slack+1], 'v',-1 );
            model = set_variables_to_zero( model, sze, perm, opt );
            model = set_Blockv_R_vla( model, v0, sze, lr, opt );  % for each block: Block * v <=> v * la || w * Block <=> la * w
                
        elseif( isequal( opt.zerohandling, 'off' ) );
            if( isvector(X) );
                v = X;
            else;
                v = compute_leading_eigenvectors_of_whole_matrix( X, lr, opt ); end;
            if( isempty(v) );
                return; end;
            model = set_Xv_R_vla( model, v, opt, lr );  % X * v <=> v * la || w * X <=> la * w
                
        else;
            error( 'optperron:zerohandling', 'Wrong argument given to option ''zerohandling''.' ); end;

        [Xout, la] = solve_model_and_evaluate_results( model, la, perm, lr, opt ); 
        if( ~isempty(Xout) );
            if( startsWith( opt.zerohandling, 'decompose' ) || ...
                isanyof( opt.zerohandling, {'block','blocktriangular','keepnull','keepzero','ev','eigenvector'} ) || ...
                isempty( opt.zerohandling ) ...
              );
                [~, v0_new] = compute_decomposition( Xout, lr, opt );  % should be replaced by mas version, computation of leading eigenvector is simple here
                if( numel(v0_new)>numel(v0) );
                    again = true; end; end;
            X = Xout; 
        elseif( isvector(X) );
            error( 'optperron:problem', 'Infeasible problem given.' ); end; end;
end

function [ left_right_set, skipped ] = parse_optimize_eigenvalue_mas( opt );
    if( isempty(regexpi(opt.optimize, 'evmas|eigenvaluemas|leftmas|rightmas|leftrightmas|all', 'once')) || ...
        ~isempty(regexpi(opt.optimize, 'noev|noeigenvalue|noleftright|noevmas|noeigenvaluemas|noleftrightmas', 'once')) ...
      );
        skipped = true;
        return; end;    
    skipped = false;
    
    if( regexpi(opt.optimize, 'eigenvalue|ev|rightleft|all') );
        left_right_set = {'right','left'};
    elseif( regexpi(opt.optimize, 'leftright') );
        left_right_set = {'left','right'};
    elseif( regexpi(opt.optimize, 'right') );
        left_right_set = {'right'};
    elseif( regexpi(opt.optimize, 'left') );
        left_right_set = {'left'};
    else;
        error( 'optimize_eigenvalue:arg', 'Wrong string given in option `optimize`.' ); end;
    end

function [ left_right_set, skipped ] = parse_optimize_eigenvalue( opt );
    if( isempty(regexpi(opt.optimize, 'ev|eigenvalue|left|right|leftright|all', 'once')) || ...
        ~isempty(regexpi(opt.optimize, 'noevmas|noeigenvaluemas|noleftrightmas', 'once')) ...
      );
        left_right_set = '';
        skipped = true;
        return; end;    
    skipped = false;
    
    if( regexpi(opt.optimize, 'eigenvalue|ev|rightleft|all') );
        left_right_set = {'right','left'};
    elseif( regexpi(opt.optimize, 'leftright') );
        left_right_set = {'left','right'};
    elseif( regexpi(opt.optimize, 'right') );
        left_right_set = {'right'};
    elseif( regexpi(opt.optimize, 'left') );
        left_right_set = {'left'};
    else;
        error( 'optimize_eigenvalue:arg', 'Wrong string given in option `optimize`.' ); end;
end

function [ Y, vlr, sze, perm ] = compute_decomposition( X, lr, opt );
    if( contains(opt.zerohandling(10:end), {'ev','eigenvector'}) );
        [Y, perm] = compute_decomposition_using_zeros_of_eigenvectors( X, lr, opt );
    elseif( contains(opt.zerohandling, {'block','blocktriangular'}) || numel(opt.zerohandling) <= 9 );  % i.e. opt.zerohandling == 'decompose' or ''; I.e. this is the default method
        [Y, perm] = compute_decomposition_using_zeros_of_basis( X, lr, opt ); end;
    vlr = compute_leading_eigenvectors_of_blocks( Y, lr, opt );
    sze = cellfun( 'size', Y, 1 );  % size of the blocks
end

function [ Xlr, permlr ] = compute_decomposition_using_zeros_of_eigenvectors( X, left_right, opt );
    permlr = 1:opt.dim;
    Xlr = {X};
    while( true );
        old_num = numel( Xlr );
        Xlr_old = Xlr;
        Xlr = {};
        orig_idx = 0;
        for i = 1:numel( Xlr_old );
            orig_idx_start_i = max( orig_idx );
            orig_idx = (orig_idx_start_i+1):(orig_idx_start_i+size(Xlr_old{i}));
            Xlr_oldi = Xlr_old{i};
            while( true );
                vi = leadingeigenvector( Xlr_oldi, left_right, 'columnvector', 'nonnegative' );
                zero_idx_i = false( size(vi) );
                for j = 1:numel( vi );
                    zero_idx_i(j) = all( vi{j}>-eps ); end;
                vi = vi(zero_idx_i);
                if( numel(vi) >= 1 );
                    break;
                elseif( ~issym(Xlr_oldi) );
                    vprintf( 'Computation of leading eigenvector failed. I am trying it symbolically.\n', 'imp',[1 opt.verbose] );
                    Xlr_oldi = sym( Xlr_oldi );
                else;
                    error( 'optperron:fatal_error', 'Some problem occured. This should not happen.' ); end; end;

            vi = double( vi{1} );
            perm_idx_i = vi ~= 0;

            Alri = Xlr_old{i}(perm_idx_i,perm_idx_i);
            Blri = Xlr_old{i}(~perm_idx_i,~perm_idx_i);
            Xlr = [Xlr Alri Blri];
            permlr(orig_idx) = permlr([orig_idx_start_i+find(perm_idx_i)' orig_idx_start_i+find(~perm_idx_i)']); end;
        if( numel(Xlr) == old_num );
            break; end; end;
end

function [ Xlr, permlr ] = compute_decomposition_using_zeros_of_basis( X, ~, ~ );
    [Xlr, B] = invariantsubspace( X, 'permute' );
    for i = 1:numel( Xlr );                
        Xlr{i} = Xlr{i}{1}; end;
    [permlr, ~] = find( B ); 
    permlr = permlr.';
end

function [ vlr, lalr ] = compute_leading_eigenvectors_of_blocks( Xlr, left_right, opt );
    vlr = cell( 1, numel(Xlr) );
    lalr = zeros( 1, numel(Xlr) );
    for i = 1:numel( Xlr );
        [vlr{i}, lalr(i)] = compute_leading_eigenvector_of_blocks_worker( Xlr{i}, left_right, opt ); end;
end

function [ v, la ] = compute_leading_eigenvector_of_blocks_worker( X, left_right, opt );
    v = leadingeigenvector( X, 'positive', left_right, 'column' );  % XX TM Should this be 'nonnegative' for 'block' decomposition?
    if( numel(v) ~= 1 );
        if( opt.verbose  >= 0 );
            warning( 'optperron:numerical', 'Computation failure. Could not find positive eigenvector. The results may be wrong.\n  I try a non-negative eigenvector next.\n' ); end;
        v = leadingeigenvector( X, 'positive', left_right, 'column', 'sym',1 ); end;                
    if( numel(v) ~= 1 );
        if( opt.verbose  >= 0 );
            warning( 'optperron:numerical', 'Computation failure. Could not find positive eigenvector. The results may be wrong.\n  I try a non-negative eigenvector next.\n' ); end;
        v = leadingeigenvector( X, 'nonnegative', left_right, 'column' ); end;
    if( numel(v) ~= 1 );
        if( opt.verbose  >= 0 );
            warning( 'optperron:numerical', 'Computation failure. Could not find positive eigenvector. The results may be wrong.\n  I try a non-negative eigenvector next.\n' ); end;
        v = leadingeigenvector( X, 'nonnegative', left_right, 'column', 'sym',1 ); end;                
    if( numel(v) == 0 );
        warning( 'optperron:numerical', 'Computation failure. Could not find non-negative eigenvector. I give up and start the debugger\n' );
        keyboard; end;  %#ok<KEYBOARDFUN,MCKBD>
    v = double( v{1} );
    la = rho( X ); 
end

function [ model ] = set_variables_to_zero( model, sze, perm, opt );
    perm_ex = expand_perm( perm );
    if( contains(opt.zerohandling, {'keepnull','keepzero'}) );
        % set all variables to zero, which are zero in the existing matrix, does not work well
        bd = zeros( opt.dim, opt.dim );
        bd(X ~= 0) = nan;
        bd = bd(perm_ex);
        model = mpgallery( 'model',model, 'dim',opt.dim, 'eq',bd, 'v',-1 ); 
    else;
        % default, set variables in the lower left corner to zero
        bd = zeros( opt.dim, opt.dim );
        sze_ex = repelem( cumsum(sze), sze );
        for i = 1:opt.dim
            bd(1:sze_ex(i),i) = nan; end;
        model = mpgallery( 'model',model, 'dim',opt.dim, 'eq',bd, 'v',-1 ); end;
end

function [ model ] = set_Blockv_eq_0( model, v0, sze, lr, opt );
    n_slack = size( model.A, 2 ) - opt.dim^2;
    sze_pre = cumsum( sze ) - sze;
    sze_post = opt.dim - sze_pre - sze;
    block = cell( 1, numel(v0) );
    for i = 1:numel( v0 );
        objblock = -v0{i};
        startzeroblocki = zeros( sze(i), sze_pre(i)*opt.dim );
        endzeroblocki = zeros( sze(i), sze_post(i)*opt.dim );
        slackzeroblock = zeros( sze(i), n_slack );
        switch lr;
            case 'right';
                prezeroblocki = zeros( sze(i), sze_pre(i) );
                postzeroblocki = zeros( sze(i), sze_post(i) );
                mainblocki = kron( v0{i}', [prezeroblocki eye(sze(i)) postzeroblocki]);
                block{i} = [objblock startzeroblocki mainblocki endzeroblocki slackzeroblock];
            case 'left';
                prezeros = zeros( 1, sze_pre(i) );
                postzeros = zeros( 1, sze_post(i) );
                mainblocki = kron( eye(sze(i)), [prezeros v0{i}.' postzeros] );
                block{i} = [objblock startzeroblocki mainblocki endzeroblocki slackzeroblock];
            otherwise; fatal_error; end;
        % model.A = [model.A; block{i}];  % this is slow, and thus we do it at once at the end
        model.rhs = [model.rhs zeros( 1, sze(i) )]; 
        switch opt.minmax;
            case 'min'; model.sense = [model.sense repmat( '<', [1 sze(i)] )];
            case 'max'; model.sense = [model.sense repmat( '>', [1 sze(i)] )];
            otherwise; fatal_error; end; end;
    model.A = [model.A; cat( 1, block{:})];
end

function [ model ] = set_Blockv_R_vla( model, v0, sze, lr, opt );
    n_slack = size( model.A, 2 ) - opt.dim^2 - model.shift;
    sze_pre = cumsum( sze ) - sze;
    sze_post = opt.dim - sze_pre - sze;
    block = cell( 1, numel(v0) );
    for i = 1:numel( v0 );
        objblock = -v0{i};
        startzeroblocki = zeros( sze(i), sze_pre(i)*opt.dim );
        endzeroblocki = zeros( sze(i), sze_post(i)*opt.dim );
        slackzeroblock = zeros( sze(i), n_slack );
        switch lr;
            case 'right';
                prezeroblocki = zeros( sze(i), sze_pre(i) );
                postzeroblocki = zeros( sze(i), sze_post(i) );
                mainblocki = kron( v0{i}', [prezeroblocki eye(sze(i)) postzeroblocki]);
                block{i} = [objblock startzeroblocki mainblocki endzeroblocki slackzeroblock];
            case 'left';
                prezeros = zeros( 1, sze_pre(i) );
                postzeros = zeros( 1, sze_post(i) );
                mainblocki = kron( eye(sze(i)), [prezeros v0{i}.' postzeros] );
                block{i} = [objblock startzeroblocki mainblocki endzeroblocki slackzeroblock];
            otherwise; fatal_error; end;
        % model.A = [model.A; block{i}];  % this is slow, and thus we do it at once at the end
        model.rhs = [model.rhs zeros( 1, sze(i) )]; 
        switch opt.minmax;
            case 'min'; model.sense = [model.sense repmat( '<', [1 sze(i)] )];
            case 'max'; model.sense = [model.sense repmat( '>', [1 sze(i)] )];
            otherwise; fatal_error; end; end;
    model.A = [model.A; cat( 1, block{:})];
end

function [ v ] = compute_leading_eigenvectors_of_whole_matrix( X, lr, opt )
    v = leadingeigenvector( X, 'nonnegativeeigenvector', lr, 'column' );  % left eigenvector
    if( numel(v) == 1 );
        % do nothing
    elseif( numel(v) > 1 && opt.verbose  >= 0 );
        fprintf( 'Leading eigenvector is not unique. I take an arbitrary one. The algorithm may suffer and produce nonsense results.' );
    else;
        if( opt.verbose  >= 0 );
            warning( 'optimize_eigenvalue:failed', 'Optimization step with eigenvector failed.' ); end;
        v = [];
        return; end;
    v = v{1};
end

function [ model ] = set_Xv_R_vla( model, v, opt, lr );
    n_slack = size( model.A, 2 ) - opt.dim^2 - model.shift;
    slackzeroblock = zeros( opt.dim, n_slack );
    objblock = -v;
    switch lr;
        case 'right'; mainblock = kron( v', eye(opt.dim) );
        case 'left'; mainblock = kron( eye(opt.dim), v' );
        otherwise; fatal_error; end;
    block = [objblock mainblock slackzeroblock];
    model. A = [model.A; block];
    model.rhs = [model.rhs zeros( 1, opt.dim )];
    switch opt.minmax;
        case 'min'; model.sense = [model.sense repmat( '<', [1 opt.dim] )];
        case 'max'; model.sense = [model.sense repmat( '>', [1 opt.dim] )];
        otherwise; fatal_error; end;
end

function [ X, la ] = solve_model_and_evaluate_results( model, la, perm, lr, opt );
    X = [];
    params.OutputFlag = opt.verbose >= 5;
    params.Method = 2;
    try;
        res = gurobi( model, params );
    catch me;  %#ok<NASGU>
        res.status = 'INFEASIBLE'; end;
    switch res.status;
        case 'OPTIMAL'
            Xout = reshape( res.x(model.shift+1:model.shift+opt.dim^2), [opt.dim opt.dim] );
%                 Xout = zeros( size(Xout) );
            if( ~isempty(perm) );
                Xout(expand_perm(perm)) = Xout(:); end;
            Xout( Xout <= 0 ) = 0;
            switch opt.minmax;
                case 'min'; setflag = rho( Xout ) < la;
                case 'max'; setflag = rho( Xout ) > la;
                otherwise; fatal_error; end;
            if( setflag );
                X = Xout;
                la = rho( X );
                vprintf( ' (after %s-step: rho: %15.15g ) \n', lr, la, 'imp',[1 opt.verbose] ); end;

        case {'UNBOUNDED', 'INF_OR_UNBD'};
            if( opt.verbose >= 0 );
                warning( 'optperron:unbounded', 'Problem may be unbounded. No result is returned. Please add additional constraints.' ); end;
        otherwise;
            if( opt.verbose >= 0 );
                warning( 'optperron:infeasible', 'No solution found.' ); end; end;
end

function [ perm ] = expand_perm( perm );
    colflag = iscolumn( perm );
    if( colflag );
        perm = perm.'; end;
    len = numel( perm );
    perm = perm - 1;
    perm = repmat( perm', [1 len] ) + perm*len;
%     perm = perm' + 1;
    perm = perm(:) + 1;
    if( ~colflag );
        perm = perm.'; end;
end

%%

function [ X, opt, la, skipped, again ] = optimize_mas( X, Dom, opt, la );
    % uses a greedy algorithm to add edges to the matrix X, such that it is still acylic (if it was)
    % if X is not acyclic, then X is unchanged
    again = false;
    if( isempty(regexpi(opt.optimize, 'mas', 'once')) || ...
        ~isempty(regexpi(opt.optimize, 'nomas', 'once')) || ...
        ~digraph(X).isdag ...
      );
        skipped = true;
        return; end;
    skipped = false;
    
    [~, supp ] = mpgallery( 'dim',opt.dim, 'model',Dom, 'lb',0, 'ex','max', 'v',-1 );
    supp = logical( supp );

    % find sinks/sources and add all remaining possible edges such that the vertex is still a sink/source
    
    dim = size( X, 1 );
    for r = 1:dim
        in = sum( X(:,r) );
        out = sum( X(r,:) );
        idx = [1:r-1 r+1:dim];
        if( in && out );
            % do nothing
        elseif( in || ...
                ~out && nnz(supp(:,r)) >= nnz(supp(r,:))...  % if there are no in, nor outcoming edges, choose the case where we have more edges of a type
              );
            X(idx,r) = supp(idx,r);
        else;
            X(r,idx) = supp(r,idx);
        end; end;
    
    la = rho( X );
    
    assert( digraph(X).isdag );
    
end

%%

function [ Xnew, opt, la, skipped, again ] = optimize_Frank_Wolfe( X, Dom, opt, la );
% computes the gradient in the current vertex,
% selects the vertex with lowest spectral radius, in the half plane defined by the gradient
% does a global optimization of the spectral radius on the line between the current and the selected vertex
    again = false;
    [do, skipped] = parse_optimize_Frank_Wolfe( opt );
    if( skipped )
        Xnew = X;
        return; end;
    
    rp = rho_prime( X, opt.verbose );
    if( isempty(rp) || ~isreal(rp) );
        skipped = true;
        Xnew = X;
        return; end;

    if( opt.verbose >= 3 );
        X_shift =  X + 0.01 * rp;
        fprintf( '  find_far_vertex: Spectral radius of original matrix: %.15g\n', rho(X) );
        fprintf( '  find_far_vertex: Spectral radius of shift by 0.1 in direction of rho_prime: %.15g (contained: %i)\n', rho(X_shift), test_inclusion(X_shift, Dom, opt) ); end;

    
    [Xnew, la] = optimize_Frank_Wolfe_preworker( X, la, Dom, do, opt, rp );
end

function [ do, skipped ] = parse_optimize_Frank_Wolfe( opt );
    skipped = false;
    if( ~isempty(regexpi(opt.optimize, 'nofw|nofrankwolfe', 'once')) );
        skipped = true;
        return; end;
    % other shirtcurcuts are done below
    
    re.fw  = '(?<!a)fw|(?<!anti_{0,1}|a)frank_{0,1}wolfe|a_{0,1}frank_{0,1}wolfe';
    re.s   = '(?<!a)simple|(?<!anti_{0,1}|simple)simple';
    %re.t   = '(?<![anri]t)|(?<!anti_{0,1}|a)tommsch';
    re.afw = 'afw|anti_{0,1}frank_{0,1}wolfe';
    %re.at  = 'at|anti_{0,1}tommsch|a_{0,1}tommsch';
    re.all = 'all';
    
    do.fw  = any( regexpi( opt.optimize, re.fw,  'once' ) );
    do.s   = any( regexpi( opt.optimize, re.s,   'once' ) );
    %do.t   = any( regexpi( opt.optimize, re.t,   'once' ) );
    do.afw = any( regexpi( opt.optimize, re.afw, 'once' ) );
    %do.at  = any( regexpi( opt.optimize, re.at,  'once' ) );
    do.all  = any( regexpi( opt.optimize, re.all ) );
    if( any([do.fw do.s do.afw]) );
        do.all = 0; end;  % if one method for Frank-Wolf step is specialized, it overrides the option ''all''
    
    if( ~any([do.fw do.s do.afw do.all]) );
        skipped = true;
        return; end;    
end

function [ Xnew, la ] = optimize_Frank_Wolfe_preworker( X, la, Dom, do, opt, rp );
    Xopt = {X};
    alg = {'original'};
    
    if( do.all || do.fw );
        [Xopt{end+1}, ~, la(end+1), alg{end+1}] = optimize_Frank_Wolfe_worker( X, Dom, opt, rp, 'frank_wolfe' ); end;
    if( do.all || do.s );
        [Xopt{end+1}, ~, la(end+1), alg{end+1}] = optimize_Frank_Wolfe_worker( X, Dom, opt, rp, 'simple' ); end;        
%         if( do.all || do.t );
%             [Xopt{end+1}, ~, la(end+1), alg{end+1}] = optimize_simple( X, Dom, opt, rp, 'tommsch' ); end;
    if( do.afw );
        [Xopt{end+1}, ~, la(end+1), alg{end+1}] = optimize_Frank_Wolfe_worker( X, Dom, opt, rp, 'anti_frank_wolfe' ); end;
    %if( do.at );
    %    [Xopt{end+1}, ~, la(end+1), alg{end+1}] = optimize_Frank_Wolfe_worker( X, Dom, opt, rp, 'anti_tommsch' ); end; 
        
    switch opt.minmax;
        case 'min'; [la, idx_opt] = min( la );
        case 'max'; [la, idx_opt] = max( la );
        otherwise; fatal_error; end;
        
    vprintf( '\b                      (New matrix given by: %s)\n', alg{idx_opt}, 'imp',[1 opt.verbose] ); 
    Xnew = Xopt{idx_opt(1)};         
end

function [ Xopt, Xend, la_, algorithm ] = optimize_Frank_Wolfe_worker( X, Dom, opt, rp, algorithm )
    Xend = find_far_vertex( X, Dom, opt, rp, algorithm );
    if( ~isequal(Xend, X) );
        switch algorithm;
            case 'simple';
                Xopt = Xend;
            otherwise;
                Xopt = find_optimum_on_line( X, Xend, Dom, opt ); end;
        la_ = rho( Xopt );
    else;
        Xopt = X;
        Xend = X;
        la_ = rho( X ); end;
end

function [ X ] = find_far_vertex( X, Dom, opt, rp, algorithm );

if( nargin <= 4 || isempty(algorithm) );
    algorithm = opt.find_far_vertex_mode; end;
    
    ne = size( Dom.A, 2 ) - opt.dim^2;
    model = mpgallery( 'model',Dom, 'dim',opt.dim, 'addfront','>', 'v',0 );  % mode 'frank-wolfe' does not need extra variables, but we add a dummy variable
    
    switch algorithm;
        case {'bad_tommsch','t'};
            % find point on the line along rp, going through X
            % X = t * rho_prime + Xk
            % this is bad, since then the resulting matrix has the same leading eigenvector
            model.A     = [model.A; [-rp(:) speye( opt.dim^2 ) spzeros( opt.dim^2, ne )]];
            model.rhs   = [model.rhs X(:).'];
            model.sense = [model.sense repmat( '=', [1 numel(X)] )];
            model.obj   = [1 zeros( 1, opt.dim^2 + ne )];

        case {'frank_wolfe','fw','simple'};
            model.obj = [0  rp(:).' zeros( 1, ne )];
            
        case {'bad_anti_tommsch','at'};
            model.A     = [model.A; [-rp(:) speye( opt.dim^2 ) spzeros( opt.dim^2, ne )]];
            model.rhs   = [model.rhs X(:).'];
            model.sense = [model.sense repmat( '=', [1 numel(X)] )];
            model.obj   = [-1 zeros( 1, opt.dim^2 + ne )];            
            
        case {'anti_frank_wolfe','afw'}
            model.obj = [0  -rp(:).' zeros( 1, ne )];
            
        otherwise;
            error( 'optperron:find_far_vertex', 'Wrong mode.' ); end;
        
    switch opt.minmax;
        case 'min'; model.modelsense = 'min';
        case 'max'; model.modelsense = 'max';
        otherwise; fatal_error; end;
    
    % find point in the halfspace defined by rp^\perp and X
    params.OutputFlag = double( opt.verbose >= 5 );
    params.Method = 2;
    res = gurobi( model, params );
    found_solution = false;
    if( isequal(res.status, 'OPTIMAL') );
        found_solution = true;
        X = reshape( res.x(2:1+opt.dim^2), [opt.dim opt.dim] );
    elseif( isequal( res.status, 'INF_OR_UNBD') || isequal(res.status, 'UNBOUNDED') ) ;
        model.ub = min( model.ub, 100 );
        res = gurobi( model, params );
        if( isequal(res.status, 'OPTIMAL') );
            found_solution = true;
            X = reshape( res.x(2:1+opt.dim^2), [opt.dim opt.dim] );
        else;
            found_solution = false; end; end;
    if( ~found_solution );
        vprintf( '  Could not find solution (algorithm: %s). I return the input matrix.\n', algorithm, 'imp',[2 opt.verbose] ); end;
    X( X <= 0 ) = 0;
    assert( test_inclusion( X, Dom, opt ) );
    
end

function [ X ] = find_optimum_on_line( X1, X2, Dom, opt );
    switch opt.minmax;
        case 'min'; h = @(t)  rho((1-t)*X1 + t*X2);
        case 'max'; h = @(t) -rho((1-t)*X1 + t*X2);
        otherwise; fatal_error; end;
    
    plot_figure = deal( 0 );  % use deal to hide the constant value from the Matlab linter
    if( plot_figure );
        r = zeros( 2, 0 );
        N = 1000;
        vX1 = leadingeigenvector( X1, 'nonnegative' );
        vX1 = vX1{1};
        for t = linspace( 0, 1, N );
            Xt = X2*t + X1*(1-t);
            rX = rho( Xt );
            if( any(Xt(:)<0) );
                continue; end;
            vXt = leadingeigenvector( Xt, 'nonnegative' );
            if( numel(vXt) == 0 );
                continue; end;
            dXt = inf;
            for j = 1:numel( vXt );
                dXt = min( dXt, norm( vX1 - vXt{j} ) ); end;
            r = [r [rX;dXt] ]; end
        figure_lazy( plot_figure );
        subplot_lazy( 1 ); plot( r(1,:) ); 
        subplot_lazy( 2 ); plot( r(2,:) ); end;      
    
    [r, t] = ternarysearch( h, 0, 1, 5e-9 );
    X = (1-t)*X1 + t*X2;
    vprintf( '  Optimum on line: Found matrix with spectral radius: %15.15g\n', r, 'imp',[4 opt.verbose] );
    if( ~test_inclusion( X, Dom, opt ) );
        vprintf( '  Optimum on line: Found matrix on line is not in the given domain.', 'cpr','err', [0 opt.verbose] ); end;
    
end

function [ m, i ] = ternarysearch( h, a, b, epsilon );
    % searches for the minimum of a function which has one local minima
    if( nargin <= 3 );
        epsilon = 4*eps; end;
    
    % generate 4 starting points
    plot_figure = deal( 0 );  % use deal to hide the constant value from the Matlab linter
    x = linspace( a, b, 4 );
    y = zeros( 1, 4 );
    for i = 1:numel( y );
        y(i) = h(x(i)); end;
    
    if( plot_figure );
        figure_lazy( plot_figure );
        res = 0.05;
        plotm( h, a:res:b, '-k' );
        drawnow; end;
    
    % do "binary search"
    ct = 0;
    maxcounter = 100;
     while( abs(y(4)-y(1)) >= epsilon && ct < maxcounter );
         ct = ct + 1;
         if( plot_figure );
            figure_lazy( plot_figure );
            plotm( [x;y], 'rx', 'hold',2 );
            drawnow; end;
        
        [~, idx_max] = max( y );
        [~, idx_min] = mink( y, 2);
        x(idx_max) = mean( x(idx_min) );
        y(idx_max) = h(x(idx_max));
        [x, idx] = sort( x, 'ascend' );
    
        y = y(idx);
     end

     [m, i] = min( y );
     i = x(i);
end

%%

function [ check ] = test_inclusion( X, Dom, opt );
    model = mpgallery( 'model',Dom, 'dim',opt.dim, 'setvariables',X, 'verbose',0, 'numtry',0 );
    params.OutputFlag = double( opt.verbose >= 5 );
    res = gurobi( model, params );
    check = isequal( res.status, 'OPTIMAL' ) && all( X(:)  >= 0 );
end

%%

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.

function comment_area  %#ok<DEFNU>
    %% only progress gives anti_frank_wolfe, some progress would give anti_tommsch

    rng(101)
    dim = 54;
    ct = 92;
    clc;
    X = optperron( 'numtry',500, 'symmetric', 'colsum',100*rand(1,dim), 'diffdiag',{'random',0,'='}, 'v',2, 'seed',ct, 'dim',dim, 'min', 'fig_X',10, 'ex',ct, 'step',0, 'fig_rho',20, 'zerohandling','off', 'method','all,afw' );
    rho( X )
    %% same as above, but in dimension 3
    dim = 3;
    clc;
    X = optperron( 'dim',dim, 'symmetric', 'colsum',randi([1 10], [1 dim]), 'min', 'ex','time',  'v',1, 'step',0, 'fig_X',10, 'fig_rho',20 );
    rho( X )
    %% only progress gives anti_frank_wolfe, some progress would give anti_tommsch
    dim = 4;
    clc;
    ct = 94;
    ex = 101;
    rng(ct);
    X1 = optperron( 'numtry',500, 'symmetric', 'rowsum',100*rand(1,dim),'v',1, 'seed',ct, 'dim',dim, 'min', 'fig_X',2, 'ex',ex, 'step',0, 'fig_rho',12, 'method','all,anti_tommsch' );
    rng(ct);
    X2 = optperron( 'numtry',500, 'symmetric', 'rowsum',100*rand(1,dim),'v',1, 'seed',ct, 'dim',dim, 'min', 'fig_X',1, 'ex',ex, 'step',0, 'fig_rho',11, 'method','all,' );
    rho( X1 )
    rho( X2 )
    %% only one step of eigenvector
    dim = 40;
    clc;
    X = optperron( 'numtry',500, 'rowsum',100*rand(1,dim), 'diffdiag',{'N',20,'diff',1,'c','>'}, 'v',1, 'seed',ct, 'dim',dim, 'min', 'fig_X',10, 'ex','time', 'step',0, 'fig_rho',20, 'method','all,nomatrix,nomonotone' );
    rho( X )    
    %%    
    dim = 100;
    ct = 94;
    clc;
    X = optperron( 'dim',dim, 'min', 'rowsumub',300, 'columnsumlb',10, 'diffdiag',{'N',100,'>','diff',50}, 'numtry',500, 'seed',ct, 'ex','time', 'optimize','fw,afw,t,at,leftright,X', 'v',1,  'fig_X',10, 'step',0, 'fig_rho',20 );
    rho( X )
end

%#ok<*NOSEL,*NOSEMI,*ALIGN>
