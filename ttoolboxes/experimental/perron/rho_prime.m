function [ rp ] =  rho_prime( X, verbose )
    
    if( nargin <= 1 || isempty(verbose) );
        verbose = 1; end;
    
    if( isempty(X) );
        rp = [];
        return; end;
    
    method = 1;
    switch method;
        case 1;
            rp = rho_prime_eig( X, verbose );
        otherwise;
            fatal_error; end;

end

function [ rp ] = rho_prime_eig( X, verbose );
    v = leadingeigenvector( X );
    w = leadingeigenvector( X' );
    
    if( verbose  >= 0 && numel(v) ~= numel(w) );
        warning( 'rho_prime:numerical', 'Numerical issues when computing the leading eigenvectors. The result may be wrong.' ); end;
        
    threshold = 1e-9 * norm( X, 1 );
    N = min( numel(v), numel(w) );
    for ct = 0:N^2 - 1
        i = floor( ct / N );
        j = ct - i * N;
        scale = v{i+1}'* w{j+1};
        if( abs(scale)>threshold );
            break; end; end;
    
    rp = v{i+1}*w{j+1}';
    rps = rp/scale;
    
    if( abs(scale) > threshold && ...
        1000*norm(X, 1) > norm(rps, 1) );
        rp = rps;
    elseif( verbose  >= 0 );
        fprintf( 'Cannot normalize derivative of spectral radius. The computation may fail.\n' ); end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
