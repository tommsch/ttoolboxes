function [ values ] = find_borders( f, mn, mx, releps, N );

    if( nargin <= 1 || isempty(mn) );
        mn = 0; end;
    if( nargin <= 1 || isempty(mx) );
        mx = 1; end;
    if( nargin <= 3 || isempty(releps) );
        releps = 3e-2; end;
    if( nargin <= 4 || isempty(N) );
        N = [10 3]; end;
    if( numel(N) == 1 );
        N = [N N]; end;
    
    % f = @(x) round( x^2 );
    % mn = 0;
    % mx = 2;
    % eps = 3e-14;
    % N = [10 3];
    if( mn == -inf );
        mn = -realmax; end;
    if( mx == inf );
        mx = realmax; end;

    values = compute_new( f, mn, mx, N(1), true );
    
    while( true );
        values = unique_consecutives( values );
        sze = size( values, 2 );
        for i = sze:-1:2
            if( values(1,i)/values(1,i-1) < 1 + releps || ...
                values(2,i) == values(2,i-1) ...
              );
                continue; end
          
            values_new = compute_new( f, values(1,i-1), values(1,i), N(2), false );
            values = [values(:,1:i-1) values_new values(:,i:end)]; end;
        if( size( values, 2 ) == sze );
            break; end; end;
    % values
end

function [ values ] = compute_new( f, mn, mx, N, borderflag );
    if( ~borderflag );
        d = (mx-mn)/(N+1);
        mn = mn + d;
        mx = mx - d; end;

    values = [linspace( mn, mx, N ); zeros(1, N)];
    for i = 1:N;
        values(2,i) = f(values(1,i)); end;
end

function [ values ] = unique_consecutives( values );
    sze = size( values, 2 );
    remove_idx = false( 1, sze );
    for i = 2:sze-1;
        remove_idx(i) = values(2,i-1) == values(2,i+1); end;
    values(:,remove_idx) = [];
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
