function [ bins, binsize, values ] = conncompe3( G );

    G = abs( G );
    values = unique( G(:) );
    bins = cell( 1, numel(values) );
    binsize = cell( 1, numel(values) );
    for i = 1:numel( values );
        Gi = G >= values(i);
        [bins{i}, binsize{i}] = conncomp( digraph( Gi ) );
    end

    for i = numel( values ):-1:2;
        if( isequal(bins{i}, bins{i-1}) && ...
            isequal(binsize{i}, binsize{i-1}) ...
          );
            bins(i) = [];
            binsize(i) = [];
            values(i) = [];
        end;
    end
    plotm( [values'; cellfun( 'prodofsize', binsize )], '.-' );
      
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
