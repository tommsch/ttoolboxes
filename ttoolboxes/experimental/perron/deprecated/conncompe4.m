function [ bins, binsize, values ] = conncompe4( G );
    %%

    dim = 15;
    G = rand(dim) .* (rand(dim) < 0.1);

    threshold = max( G(:) );
    vertex_idx = 1:size( G, 1 );
    while( true );
        Gi = G >= threshold;
        bins = conncomp( digraph(Gi) );
        [Gi, vertex_idx] = condense( Gi, bins, vertex_idx );
        threshold = max( G(G < threshold) );
    end

    for i = numel( values ):-1:2;
        if( isequal(bins{i}, bins{i-1}) && ...
            isequal(binsize{i}, binsize{i-1}) ...
          );
            bins(i) = [];
            binsize(i) = [];
            values(i) = [];
        end;
    end
      
end

function [ G, vertex_idx ] = condense( G, bins, vertex_idx );
    
    remove_idx = [];
    for i = unique( bins );
        idx_i = find( bins == i );
        c = max( G(:,idx_i), [], 2 );
        r = max( G(idx_i,:), [], 1 );
        G(:,idx_i) = repmat( c, [1 numel(idx_i)] );
        G(idx_i,:) = repmat( r, [numel(idx_i) 1] );
        idx_i(1) = [];
        remove_idx = [remove_idx idx_i];  %#ok<AGROW>
    end

    G(remove_idx,:) = [];
    G(:,remove_idx) = [];
    %G = G .* ~eye( size(G) );  % remove self loops
    vertex_idx(remove_idx) = [];
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
