function [ bins, binsize, borders ] = conncompe2( G, eps );

    if( nargin <= 1 || isempty(eps) );
        eps = 1e-12; end;

    G = abs( G );
    borders = find_borders( @(x) f(G, x), min(G(:)), max(G(:)), eps );
    plotm( borders, '.-', 'semilogx' );
    [~,idx] = max( quot( borders(1,3:end-2) ) );
    if( isempty(idx) );
        idx = 1; end;
    [bins, binsize] = conncomp( digraph(G > borders(1,idx+2)) );

end

function [ sze ] = f( G, eps );
    [~, binsize] = conncomp( digraph(G > eps));
    sze = numel( binsize );
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
