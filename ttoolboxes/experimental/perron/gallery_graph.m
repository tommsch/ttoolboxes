function [ G, co, ph ] = gallery_graph( varargin );

    % XX: missing stuff:
    % - Galton-Watson tree
    % - d-regular tree, d-regular graph
    % - finite sections of graphs
    % - Cayley graphs
    % - random d-tree
    % - minors

    [opt, varargin] = parse_input( varargin{:} );
    [G, co] = getgraph( opt, varargin{:} );
    [G, co, ph] = postprocess( G, co, opt );
end

function [ opt, varargin ] = parse_input( varargin );

    %[~, varargin, opt.fullsparse] = parsem( {'','full','sparse'}, varargin );
    
    [opt.power, varargin] =             parsem( {'power','steps'}, varargin, [] );
    [opt.logical, varargin] =           parsem( {'logical'}, varargin );
    [opt.size, varargin] =              parsem( {'size','n','N','J'}, varargin, [] );
    [opt.stronglyconnected, varargin] = parsem( {'stronglyconnected','connected'}, varargin, [] );
    [notwocycle, varargin] =            parsem( {'no2cycle','notwocycle','no2cycles','notwocycles'}, varargin, [] );
    [opt.oriented, varargin] =          parsem( {'oriented','2cycle','twocycle','2cycles','twocycles'}, varargin, ~notwocycle );
    [opt.sparsity, varargin] =          parsem( {'sparsity','sparse'}, varargin, [] );
    [opt.asgraph, varargin] =           parsem( 'asgraph', varargin, 0 );
    [opt.what, varargin] =              parsem( 'what', varargin, [] );
    [opt.omitselfloops, varargin] =     parsem( {'omitselfloops','noselfloops'}, varargin, [] );
    [opt.symmetric, varargin] =         parsem( {'symmetric'}, varargin, [] );
    
    [noisolated, varargin] =            parsem( 'noisolated', varargin, randi([0 1]) );
    [opt.isolated, varargin] =          parsem( {'isolated'}, varargin, ~noisolated );
    
    [opt.reorder, varargin] =           parsem( {'reorder'}, varargin, [] );
    
    [opt.plot, varargin] =              parsem( {'plot'}, varargin );
    
    if( ~isempty(opt.what) );
    elseif( numel(varargin) >= 1 );
        opt.what = varargin{1}; 
        varargin(1) = []; end;
    
    if( isempty(what) );
        if( isempty(opt.reorder) );
            opt.reorder = randi( [0 1] ); end;
        if( isempty(opt.stronglyconnected) );
            opt.stronglyconnected = randi( [0 1] ); end;
        if( isempty(opt.oriented) );
            opt.oriented = randi( [0 1] ); end;
        if( isempty(opt.sparsity) );
            opt.sparsity = 1 - (randi([0 1]) * rand); end;
        if( isempty(opt.omitselfloops) );
            opt.omitselfloops = randi( [0 1] ); end;
        if( isempty(opt.symmetric) );
            opt.symmetric = randi( [0 1] ); end;
        if( isempty(opt.isolated) );
            opt.isolated = randi( [0 1] ); end;
    else;
        if( isempty(opt.reorder) );
            opt.reorder = 0; end;        
        if( isempty(opt.stronglyconnected) );
            opt.stronglyconnected = 0; end;        
        if( isempty(opt.oriented) );
            opt.oriented = 0; end;
        if( isempty(opt.sparsity) );
            opt.sparsity = 1; end;
        if( isempty(opt.omitselfloops) );
            opt.omitselfloops = 0; end;
        if( isempty(opt.symmetric) );
            opt.symmetric = 0; end;
        if( isempty(opt.isolated) );
            opt.isolated = 1; end; end;    
    
end

function [ G, co, ph ] = postprocess( G, co, opt );
    ph = [];
    if( isempty(G) );
        return; end;
    
    if( ~isempty(opt.power) );
        G_ = G;
        r = rho( G_ );
        for i = 1:(opt.power-1);
            G = G*G_/r; end; end;

    if( ~isempty(opt.size) );
        G = G(1:opt.size,1:opt.size);
        if( ~isempty(co) );
            co = co(:, 1:opt.size); end; end;

    if( opt.symmetric );
        G = (G + G')/2; end;

    if( isempty(opt.oriented) && rand<.5 || ...
        opt.oriented || opt.omitselfloops ...
      );
        % I decided, that an oriented graph has no self loops
        G(1:1+size(G, 1):end) = 0; end;
    
    if( opt.oriented );  
        [r, c] = find( G );
        idx_dupl = intersect( [r c], [c r], 'rows' );
        idx_dupl = unique( sort( idx_dupl, 2 ), 'rows' );
        idx_perm = logical( randi( [0 1], [1 size(idx_dupl, 1)] ) )';
        idx_dupl(idx_perm,:) = flip( idx_dupl(idx_perm,:), 2 );
        idx_dupl = sub2ind( size(G), idx_dupl(:,1), idx_dupl(:,2) );
        G(idx_dupl) = 0;
        end;
        
    
    if( opt.sparsity<1 );
        G = G .* (rand( size(G) ) < opt.sparsity); end;
    
    if( opt.isolated == false );
        deg = sum( G ~= 0, 1 ) + sum( G ~= 0, 2 )';
        idx = ~deg;
        G(:,idx) = [];
        G(idx,:) = [];
        if( ~isempty(co) );
            co = co(:,~idx); end; end;
    
    if( opt.stronglyconnected );
        [bins, binsizes] = conncomp( digraph(G) );
        [~, comp] = max( binsizes );
        idx = bins == comp;
        G = G(idx,idx); end;
    
    if( ~isequal(opt.reorder, false) );
            % symrcm has some bugs and cant be used
            idx = amd( G );
            if( ~isempty(co) );
                co = co(:,idx); end;
            G = G(idx,idx);
            end;
            
    if( opt.logical );
        G = logical( G ); end;            
    
    if( opt.plot );
        if( isempty(co) );
            coord = {};
        elseif( size(co, 1) == 1 );
            coord = {'XData',co(1,:)};
        elseif( size(co, 1) == 2 );
            coord = {'XData',co(1,:), 'YData',co(2,:)};
        elseif( size(co, 1) == 3 );
            coord = {'XData',co(1,:), 'YData',co(2,:), 'ZData',co(3,:)}; 
        else;
            sze = size( co, 2 );
            mag = 10*max(abs(co(:)))/numel(co);
            coord = {'XData',co(1,:)+randn(1, sze)*mag, 'YData',co(2,:)+randn(1, sze)*mag, 'ZData',co(3,:)+randn(1, sze)*mag}; 
        end;
        if( issymmetric(G) );
            ph = plot( graph(G), coord{:} );
        else;
            ph = plot( digraph(G), coord{:} ); end; end;

    if( opt.asgraph );
        if( issymmetric(G) );
            G = graph( G ); 
        else;
            G = digraph( G ); end; end;
    
end

%%

function [ G, co ] = getgraph( opt, varargin );

    randflag = isempty( opt.what ) * randi( [1 1] );
    G = [];
    if( randflag == 0 && isempty(G) || randflag == 1 );
        [G, co] = getgraph_family( opt, varargin{:} ); end;
    if( randflag == 0 && isempty(G) || randflag == 1 );
        [G, co] = getgraph_higham( opt, varargin{:} ); end;    
end

function [ G, co ] = getgraph_family( opt, varargin );

    co = [];
    
    if( check(opt.what, {'random'}) );
        opt.what = []; end;
    if( isempty(opt.what) ); 
        opt.what = randi( [1 23] ); end;
    
    G = [];
    if( false );
    elseif( check(opt.what, {1,'full','complete','kn','nclique','n-clique','k-clique','1partite','1-partite'}) );
        [G, co] = graph_complete( varargin{:} );
        
    elseif( check(opt.what, {2,'completebipartite','bipartite','bi-partite','2-partite','2partite','knm'}) );
        len = [varargin{:}];
        if( numel(len) == 0 );
            [G, co] = graph_multipartite( ceil([1+randp 1+randp]/2) );
        elseif( numel(len) == 1 );
            [G, co] = graph_multipartite( ceil([len len]/2) );
        else
            [G, co] = graph_multipartite( len ); end;
    elseif( check(opt.what, {3,'k5'}) );
        assert( numel(varargin) == 0 );
        [G, co] = graph_multipartite( 5 );        
    elseif( check(opt.what, {4,'knn'}) );
        assert( numel(varargin) == 1 );
        [G, co] = graph_multipartite( varargin{:}, varargin{:} );
    elseif( check(opt.what, {5,'k33','utility'}) );
        assert( numel(varargin) == 0 );
        [G, co] = graph_multipartite( [3, 3] );
    elseif( check(opt.what, {6,'partite','n-partite','npartite','k-partite','kpartite','multi-partite','multipartite'}) );
        [G, co] = graph_multipartite( varargin{:} );
        
    elseif( check(opt.what, {7,'circular','cn','cycle'}) );
        G = graph_cycle( varargin{:} );
        
    elseif( check(opt.what, {8,'fan','nfan','fn','friend','friendship','dutchwindmill','windmill'}) );
        G = graph_friendship( varargin{:} );        
    
    elseif( check(opt.what, {9,'hypercube','cube','qn'}) );
        G = graph_hypercube( varargin{:} );
        
    elseif( check(opt.what, {10,'wheel','wn'}) );
        G = graph_wheel( varargin{:} );

    elseif( check(opt.what, {11,'web','wnr'}) );
        G = graph_web( varargin{:} );
        
    elseif( check(opt.what, {12,'plane','planar','delaunay'}) );
        [G, co] = graph_planar( 'delaunay', varargin{:} );
        
    elseif( check(opt.what, {13,'grid','lattice','mesh','squaregrid','squarelattice','squaremesh'}) );
        G = graph_squarelattice( varargin{:} );  
        
    elseif( check(opt.what, {14,'metric','near'}) );
        [G, co] = graph_near( varargin{:} );
        
    elseif( check(opt.what, {15,'lattice2'}) );
        G = graph_lattice2( varargin{:} );

    elseif( check(opt.what, {16,'gaussian','gauss'}) );
        G = graph_gaussian( varargin{:} );

    elseif( check(opt.what, {17,'bishop'}) );
        [G, co] = graph_chess( opt, 'bishop', varargin{:} );
    elseif( check(opt.what, {18,'knight'}) );
        [G, co] = graph_chess( opt, 'knight', varargin{:} );
    elseif( check(opt.what, {19,'rook','castle'}) );
        [G, co] = graph_chess( opt, 'rook', varargin{:} );
    elseif( check(opt.what, {20,'queen'}) );
        [G, co] = graph_chess( opt, 'queen', varargin{:} );
    elseif( check(opt.what, {21,'king'}) );
        [G, co] = graph_chess( opt, 'king', varargin{:} );
    elseif( check(opt.what, {21,'pawn','whitepawn','pawnwhite'}) );
        [G, co] = graph_chess( opt, 'pawn', varargin{:} );

        
        
    end;
end

function [ G, co ] = getgraph_higham( opt, varargin );

    co = [];
    if( check(opt.what, {'higham'}) );
        opt.what = []; end;
    if( isempty(opt.what) ); 
        opt.what = randi( [1 4] ); end;
    if( numel(opt.what) >= 7 && isequal(opt.what(1:7),'higham_' ) );
        opt.what(1:7) = []; end;
    
    G = [];
    
    if( false );
    elseif( check(opt.what, {1,'erdrey','erdosrenyi','random'}) );
        G = graph_erdrey( varargin{:} );
    elseif( check(opt.what, {2,'geo','geometric,','geometricrandom'}) );
        [G, co] = graph_geo( opt, varargin{:} );
    elseif( check(opt.what, {3,'gilbert','Gnp'}) );
        G = graph_gilbert( varargin{:} );
    elseif( check(opt.what, {4,'kleinberg','smallworld'}) );
        G = graph_kleinberg( opt, varargin{:} );        
    end
end
%% families

function [ G, co ] = graph_complete( n );
    if( nargin <= 0 );
        n = 1 + randp(); end;
    G = ones( n );
    co = [];
    %t = linspace( 0, 2*pi, n + 1 );
    %co = [sin( t(1:end-1) );cos( t(1:end-1) )];
end

function [ G, co ] = graph_multipartite( len );
    if( nargin <= 0 );
        len = randi( [2 5], [1 randi([2 5])] ); end;
    dim = sum( len );
    G = zeros( dim );
    for i = 1:numel( len );
        if( i<numel( len ) );
            nr_z1 = sum(len(1:i));
            nr_o = len(i+1);
        else;
            nr_z1 = 0;
            nr_o = len(1); end;
        nr_z2 = dim - nr_z1 - nr_o;
        nc = len(i);
        idxc_G = (1+sum(len(1:i-1))):sum(len(1:i));
        G(:,idxc_G) = [zeros( nr_z1,nc) 
                       ones( nr_o, nc )
                       zeros( nr_z2, nc)]; end;
    x = repelem( (1:numel(len))-1, len );
    y = zeros( size(x) );;
    idx = 1;
    for i = 1:numel( len );
        y( idx:idx+len(i)-1 ) = linspace( 0, 1, len(i) ) + randn/(len(i)*10);
        idx = idx + len(i); end;
    co = [x;y];

%     t = linspace( 0, 2*pi, dim + 1 );
%     co = [sin( t(1:end-1) );cos( t(1:end-1) )];
end

function [ G ] = graph_cycle( n );
    if( nargin <= 0 );
        n = randi( [2 200] ); end;
    n = n - 1;
    G = [zeros( n, 1 ) eye( n );
        1              zeros(1, n)];
end

function [ G ] = graph_friendship( n );
    if( nargin <= 0 );
        n = 2 + randp(); end;
    G = [0 1;1 0];
    G = repcell( G, [1 n] );
    G = blkdiag( G{:} );
    G = [ G             ones( 2*n, 1 );
          ones(1, 2*n)  1];
end

function [ A ] = graph_hypercube( d );
    %dorig = d;
    d = ceil( log2( d ) );
    if( nargin <= 0 );
        d = randi( [2 15] ); end;
    % adjacency matrix of the d-dimensional hypercube.
    pow2 = 2.^(0:d-1);
    f2b = @(j) floor(rem(j./pow2,2)); % flint to binary
    b2f = @(b) b*pow2'; % binary to flint
    n = 2^d;
    A = zeros( n,n, 'logical' );
    for j = 0:n-1
        % Convert column index to binary.
        b = f2b(j);
        % Flip bits to get corresponding row indices.
        for i = 1:d
            b(i) = ~b(i);
            k = b2f(b);
            b(i) = ~b(i);
            A(k+1,j+1) = 1;
        end
    end
end

function [ G ] = graph_wheel( n );
    if( nargin <= 0 );
        n = 2 + randp; end;
    n = n - 2;
    G = [zeros( n, 1 ) eye( n )     ones( n, 1 );
        1              zeros(1, n)  1;
        zeros( 1, n+2 );
        ];
end

function [ G ] = graph_web( varargin );
    nr = [varargin{:}];
    if( numel(nr) <= 1 );
        nr(1) = 1 + randp(); end;
    if( numel(nr) <= 2 );
        nr(2) = 1 + randp(); end;
    G = graph_cycle( nr(1) );
    G = repcell( G, [1 nr(2)] );
    G = blkdiag( G{:} );
    rows = nr(1)*nr(2);
    G(nr(1)*rows+1:rows+1:end) = 1;
end

function [ G, co ] = graph_planar( method, varargin );
    switch method;
        case 'delaunay';
            [G, co] = graph_planar_delaunay( varargin{:} );
        otherwise;
            fatal_error; end;
end

function [ G ] = graph_squarelattice( n );
    if( nargin <= 0 );
        n = 2 + randp; end;
    
    G = delsq( numgrid('S', ceil(sqrt(n)) + 2) );
    G = G(1:n,1:n);
end

function [ G, pts ] = graph_near( n, dist, metric, dim, distribution )
    if( nargin <= 0 || isempty(n) );
        n = randp( 1, 100 ); end;
    if( nargin <= 1 || isempty(dist) );
        dist = .2; end;
    if( nargin <= 2 || isempty(metric) );
        metric = 'cityblock'; end;
    if( nargin <= 3 || isempty(dim) );
        dim = 2; end;
    if( nargin <= 4 || isempty(distribution) );
        distribution = @rand; end;
    pts = distribution( n, dim );
    G = squareform( pdist( pts, metric ) < dist );
    idx = symrcm( G );
    pts = pts(idx,:)';
    G = G(idx,idx);
end

function [ G, pts ] = graph_planar_delaunay( n );
    if( nargin <= 0 );
        n = 3 + 2*randp; end;
    pts = randn( n, 2 );
    dt = delaunayTriangulation( pts );
    dt = dt.ConnectivityList;
    G = sparse( dt, dt(:, [2:end 1]), 1);
    
    idx = symrcm( G );
    G = G(idx,idx);
    pts = pts(idx,:).';
    
%     pts = [];
    
end

function [ G ] = graph_lattice2( n, density );
    % generates a an adjancy matrix, which looks like a sparse lattice
    if( nargin <= 0 );
        n = randi( [2 10] ); end;    
    if( nargin <= 1 );
        density = 1; end;
    n = ceil( sqrt( n ) );
    co = randn( 2, n^2*density );
    co = round( co * n ) + n;
    co = clamp( co, 1, n );
    lin = sub2ind( [n n], co(1,:), co(2,:) );
    lin = unique( lin );
    
    %G = zeros( n );
    G(lin) = 1;
    G = (G + G')/2;
end

function [ G ] = graph_gaussian( n, density );
    if( nargin <= 0 );
        n = randi( [2 10] ); end;
    if( nargin <= 1 );
        density = 1; end;
    
    co = randn( 2, n^2*density ) * n/4 + n/2;
    co = round( co );
    co = clamp( co, 1, n );
    lin = sub2ind( [n n], co(1,:), co(2,:) );
    lin = unique( lin );
    
    G = zeros( n );
    G(lin) = 1; 
end

function [ G, co ] = graph_chess( opt, piece, varargin );

    [n, varargin]  = parsem( {'n','size','sze'}, varargin, randi( [2 10] ) );
    [dim, varargin]  = parsem( {'dim','dimension'}, varargin, 2 );
    
    if( ~isempty(opt.size) );
        n = opt.size; end;    
    
    if( numel(varargin) >= 1 );
        n = varargin{1};
        varargin(1) = []; end;
    if( numel(varargin) >= 1 );
        dim = varargin{1};
        varargin(1) = []; end;    
    parsem( varargin, 'test' ),

    n = ceil( n^(1/dim) );
    co = mixvector( [1:n], dim );  %#ok<NBRAK>
    G = zeros( n^dim );
    for i = 1:n^dim;
        for j = 1:n^dim;
            switch piece
                case {'rook','castle'};
                    if( i == j );
                        continue; end;
                    if( nnz( co(:,i) ~= co(:,j) ) == 1 );
                        G(i,j) = 1; end;
                case 'knight';
                    if( abs( norm( co(:,i) - co(:,j) ) - sqrt(5) ) < .1 );
                        G(i,j) = 1; end;                    
                case 'king';
                    if( i == j );
                        continue; end;
                    d = norm( co(:,i) - co(:,j), inf );
                    if( d == 1 );
                        G(i,j) = 1; end;
                case 'queen';
                    if( i == j );
                        continue; end;
                    if( nnz( co(:,i) ~= co(:,j) ) == 1 || ...
                        norm( diff(abs(co(:,i)-co(:,j))) ) < .1 ...
                      );
                        G(i,j) = 1; end;
                case 'bishop';
                    if( i == j );
                        continue; end;
                    if( all( abs( diff(abs(co(:,i)-co(:,j))) ) < .1 ) );
                        G(i,j) = 1; end;
                otherwise;
                    fatal_error; end; end; end;
        
    rc2idx = @(r,c) (r-1).*n + c;
    switch piece
        case {'pawn','whitepawn','pawnwhite'};
            assert( dim == 2, 'gallery_graph:pawn', 'pawns work only for dimension 2 yet.' );
            for r = 2:n-1;
                for c = 1:n;
                    i = rc2idx( r, c );
                    if( r == 2 );
                        if( c == 1 );
                            j = rc2idx( [r+1 r+1 r+2], [c   c+1 c] );
                        elseif( c==n );
                            j = rc2idx( [r+1 r+1 r+2], [c-1 c   c] );
                        else;
                            j = rc2idx( [r+1 r+1 r+1 r+2], [c-1 c   c+1 c ] );
                            end;
                    else;
                        if( c == 1 );
                            j = rc2idx( [r+1 r+1], [c   c+1] );
                        elseif( c==n );
                            j = rc2idx( [r+1 r+1], [c-1 c  ] );
                        else;
                            j = rc2idx( [r+1 r+1 r+1], [c-1 c   c+1] );
                            end; end;
                    j = clamp( j, 1, n^2, @(l,r) l<=r, 1 );
                    i = repmat( i, [1 numel(j)] );
                    idx = sub2ind( [n^2 n^2], i, j );
                    G(idx) = 1; end; end; end;
                    
end

%% higham
function [ G ] = graph_erdrey( n, m );
    if( nargin == 0 || isempty(n) );
        n  = randi( 1000 ); end;
    if( nargin <= 1 || isempty(m) );
        m = ceil( n*log(n)/2 ); end;
    G = erdrey( n, m );  % Copyright for erdrey: Alan Taylor and Desmond J. Higham
end

function [ G, co ] = graph_geo( opt, varargin );
    [n, varargin]  = parsem( {'n','size','sze'}, varargin, randi(1000) );
    [r, varargin]  = parsem( {'r','radius','rad'}, varargin, sqrt(1.44/n) );
    [m, varargin]  = parsem( {'m','dim'}, varargin, 2);
    [per, varargin]  = parsem( {'per','period','periodicity'}, varargin, 0 );
    [pnorm, varargin]  = parsem( {'pnorm','norm','nrm'}, varargin, 2 );
    
    if( ~isempty(opt.size) );
        n = opt.size; end;    
    
    if( numel(varargin) >= 1 );
        n = varargin{1};
        varargin(1) = []; end;
    
    if( numel(varargin) >= 1 );
        r = varargin{1};
        varargin(1) = []; end;
    
    if( numel(varargin) >= 1 );
        m = varargin{1};
        varargin(1) = []; end;
    
    if( numel(varargin) >= 1 );
        per = varargin{1};
        varargin(1) = []; end;
    
    if( numel(varargin) >= 1 );
        pnorm = varargin{1};
        varargin(1) = []; end;
    
    
    
    parsem( varargin, 'test' );
    [G, co] = geo( n, r, m, per, pnorm );  % Copyright for geo: Alan Taylor and Desmond J. Higham
    co = co.';
end

function [ G ] = graph_gilbert( n, p );
    if( nargin == 0 || isempty(n) );
        n  = randi( 1000 ); end;
    if( nargin <= 1 || isempty(p) );
        p = log(n)/n; end;
    G = gilbert( n, p );  % Copyright for erdrey: Alan Taylor and Desmond J. Higham
end

function [ G ] = graph_kleinberg( opt, varargin );
    [n, varargin]  = parsem( {'n','size','sze'}, varargin, randi(1000) );
    [p, varargin]  = parsem( {'p','maxdist','maxshortdist','manhattan'}, varargin, 1 );
    [q, varargin]  = parsem( {'q','connections','numconnections','connection','numconnection'}, varargin, 1 );
    [alpha, varargin]  = parsem( {'alpha','clustering','periodicity'}, varargin, 2 );
    
    if( ~isempty(opt.size) );
        n = opt.size; end;    
    
    if( numel(varargin) >= 1 );
        n = varargin{1};
        varargin(1) = []; end;
    
    if( numel(varargin) >= 1 );
        p = varargin{1};
        varargin(1) = []; end;
    
    if( numel(varargin) >= 1 );
        q = varargin{1};
        varargin(1) = []; end;
    
    if( numel(varargin) >= 1 );
        alpha = varargin{1};
        varargin(1) = []; end;
    
    parsem( varargin, 'test' );
    G = kleinberg( n, p, q, alpha );  % Copyright for geo: Alan Taylor and Desmond J. Higham
end

%% helper

function [ ret ] = check( what, names, opt )
    if( nargin == 2 );
        verbose = 1;
    else;
        verbose = opt.verbose; end;
    if( isempty(what) );
        ret = false;
        return; end;
    switch lower( what );
        case names;
            if( isnumeric(what) && verbose >= 1 );
                for i = 1:numel( names )
                    if( ischar(names{i}) || isstring(names{i}) );
                        fprintf( 'Choice: %s\n', names{i} ); 
                        break; end; end; end;
            ret = true;
        otherwise;
            ret = false; end;
end
    
function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
