function [ w ] = eig_nil_nonneg( M, oneflag );
% returns the eigenvectors of a non-negative, nilpotent matrix
% [ w ] = eig_nil_nonneg( M, [oneflag] );
% Input:
%   M           matrix, must be non-negative and nilpotent
%   oneflag     bool, default = false, 
%               if true, then only one eigenvector with the maximum number of nonzero entries is returned
%
% Output:
%   w           eigenvectors of M
%
% Written by: tommsch, 2023-05

    if( nargin >= 2 && oneflag );
        w = double( ~any( M, 1 )' );
    else;
        idx = ~any( M, 1 );
        w = double( diag( ~idx' ) );
        w = num2cell( w(:,idx), 1 ); end;
end
    
function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
