function threadplot( handle, fig, type )

    if( nargin <= 1  || isempty(fig) );
        fig = []; end;
    if( nargin <= 2 || isempty(type) );
        type = 1; end;

    switch type;
        case 1;  % compute plot in other thread and show it with timer
            F = parfeval( @threadplot_plot, 1, handle );
            t = timer;
            t.StartDelay = 0.1;
            t.Period = 0.2;
            t.ExecutionMode = 'fixedSpacing';
            t.TimerFcn = @(~,~) threadplot_fetch( F, t, fig );
            start( t );

        case 2;  % compute plot in other thread and show it in this function using a busy loop
            F = parfeval( @threadplot_plot, 1, handle );
            while( true );
                success = threadplot_fetch( F, [], fig );
                pause( 0.1 );
                if( success );
                    break; end; end;

        case 3;  % do everything sequentially
            st = threadplot_plot( handle );
            threadplot_show( st, fig );

        otherwise;
            error( 'threadplot:type', 'Wrong value for ''type''.' ); end;


end

function success = threadplot_fetch( F, t, fig );
    try;
        success = false;  % is needed for callers busy loop
        if( strcmp(F.State,'finished') );
            if( ~isempty(t) );
                stop( t ); % stop timer 
                delete( t ); % delete timer
                end;
            [~, st] = fetchNext( F );
            threadplot_show( st, fig );
            success = true;
            cancel( F ); % cancel future
        end;
    catch me;
        warning( 'threadplot:fetch', 'Error while fetching plot from thread.' );
        disp( F.Error.stack )
        disp( me ); end;
end

function threadplot_show( st, fig );
    %#ok<*UNRCH>
    try;
        
        % get number of figures to be plotted
        b1_f = getArrayFromByteStream( st.byte );
        num = numel( b1_f );
        close( b1_f );
        
        % make target figures where we want to plot
        if( isa(fig,'matlab.ui.Figure') );
            fig = fig.Number; 
        elseif( isempty(fig) );
            fig = gcf;
            fig = fig.Number;
        elseif( isnumeric(fig) );
            % do nothing
        else;
            error( 'threadplot:arg', 'Wrong type for ''fig'' given.' );  end;
        if( num == 1 );
            try;
                target_f{1} = figure( fig );
            catch me;  %#ok<NASGU>
                target_f{1} = figure; end;
        else;
            target_f = cell( 1, num );
            for i = 1:num;
                target_f{i} = figure( i + fig - 1); end; end;
        
        f_ax = cell( 1, num );
        for i = 1:num;
            f_ax{i} = get( target_f{i}, 'CurrentAxes' ); end;
        
        % clear figure if necessary
        for i = 1:num
            if( ~isempty(f_ax{i}) && ~ishold(f_ax{i}) );
                clf( target_f{i} ); end; end;
        
        % plot stuff from bytearray into temporary figure
        for k = 1:1;
            switch k;
                case 1; source_f = getArrayFromByteStream( st.byte );
                case 2; source_f = struct2handle( st.h2s , 0 );
                otherwise; fatal_error; end;

            source_c = get( source_f, 'Children');
            if( ~iscell(source_c) );
                source_c = {source_c}; end;
            source_cc = cell( 1, numel(source_c) );
            for i = 1:numel( source_c );
                source_cc{i} = get( source_c{i},'Children');
                if( ~iscell(source_cc{i}) );
                    source_cc{i} = {source_cc{i}}; end; end;  %#ok<CCAT1>

            for i = 1:num;
                % copy from temporary figure to target figure and close temporary figures
                for j = 1:numel( source_cc{i} );
                    copyobj( source_cc{i}{j}, gca(target_f{i}) ); end; end; 
            close( source_f );
        
        end;
            
        
    catch me;
        warning( 'threadplot:show', 'Error while displaying fetched plot.' );
        disp( me ); end;
        
    try;  %#ok<TRYNC>
        close( target_f ); end;
        
end

function [ st ] = threadplot_plot( handle );
    
    st = [];
    if( ~isempty(getCurrentWorker) );
        close all; end;  % close all figures if executed in thread
    try;
        handle();  % plot
    catch me;
        warning( 'threadplot:plot', 'Error while plotting.' );
        disp( me );
        
        % since the warning is not visible when plotting happens in thread,
        % we instead plot an error message
        clf;  
        figure;
        hold on;
        plot( [1 0 1], [0 1 2], 'r-' );
        plot( [0 1], [1 1], 'r-' );
        plot( [3 3 4 3 4], [0 2 1 1 0], 'r-' );
        plot( [6 6 7 6 7], [0 2 1 1 0], 'r-' );
        plot( [9.5 9 9.5 10 9.5], [0 1 2 1 0], 'r-' );
        plot( [12 12 13 12 13], [0 2 1 1 0], 'r-' );
        axis( [-1 14 -1 3] );
        axis equal; 
        rethrow( me );
        end;
    
    h = get( groot, 'Children' );  % get handles to all figures
    for i = 1:numel( h );
        h(i).Visible = false; end;
    st.h2s = handle2struct( h );  % make figure to struct
    st.byte = getByteStreamFromArray( h );
    close( h );  % close generated figures    
        
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

