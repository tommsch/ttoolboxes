% Class which holds information and is identifiable via class()

classdef cvector
    properties ( Access = public )
        data
        sze
        cap
    end
    
    methods
        function [ obj ] = cvector( varargin );
        % [ obj ] = cvector( double );
        % [ obj ] = cvector( double, sze, cap );
            d = varargin{1};
            switch numel( varargin );
                case 1;
                    sze = size( d );
                    cap = sze;
                    cap(end) = ceil( sze(end)*1.5 + 1 );
                    cap = num2cell( cap );
                    d(cap{:}) = 0;
                case 3;
                    sze = varargin{2};
                    cap = varargin{3};
                otherwise;
                    fatal_error; end;
            obj.data = d;
            obj.sze = sze;
            obj.cap = cap;
        end
        
        function [ obj ] = subsref( obj, s );
        % Overload subsref for indexing
            switch s(1).type;
                case '()'; % Parentheses indexing
                    switch numel( s(1).subs );
                        case 1; assert( all(s(1).subs{1} <= prod( obj.sze )) );
                        otherwise;
                            trail = numel(s(1).subs) - fliplr( find([s(1).subs{:}]-1, 1) ) + 2;
                            if( ~isempty(trail) );
                                s(1).subs(trail:end) = []; end;
                            assert( all( [s(1).subs{:}] <= [obj.cap{:}] ) );
                            end;
                    obj.data = builtin( 'subsref', obj.data, s );  % Access internal data
                otherwise;
                    error('Unsupported indexing type.');
            end
        end
        
        % Overload subsasgn for assignment
        function [ obj ] = subsasgn( obj, s, value );
            switch s(1).type;
                case '()' % Parentheses indexing
                    obj.data = builtin( 'subsasgn', obj.data, s, value );  % Access internal data
                    
                otherwise
                    error('Unsupported indexing type.');
            end
        end
        
    end
end




function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.   
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.
