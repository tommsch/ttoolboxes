function [ varargout ] = subfigure( pos );
% (experimental) Adds new axes to a figure at position fig
% [ varargout ] = subfigure( [pos] );
%
% See also: subplot_lazy
%
% Written by: tommsch, 2022

% Changelog:    tommsch,    2024-10-30,     New calling possibility without argument


    hFig = gcf;
    num = numel( hFig.Children);  % total number of figures so far
    tmpFig = figure;  % we need a new figure, because subplot deletes handles
    clean_fig = onCleanup( @() close(tmpFig) );

    if( isempty(pos) );
        pos = num + 1; end;

    if( ~isempty(num) && pos <= num );
        % just return axis of subplot
        ax = subplot_lazy( num, pos );
    else;
        setappdata( hFig, 'ttoolboxes_subfigure_num', pos );
        
        % reorder all current subplots
        hAxes = findobj( allchild(hFig), 'flat', 'Type','axes' );
        for i = 1:num;
            hSub(i) = subplot_lazy( pos, i );  %#ok<AGROW>
            set( hAxes(i), 'Position', get(hSub(i), 'Position') );
            end;
        for i = 1:num;
            delete( hSub(i) ); end;
        
        figure( hFig );
        ax = subplot_lazy( pos, pos );
        end;
    
    if( nargout >= 1 );
        varargout = cell( 1, nargout );
        [varargout{:}] = ax; end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
