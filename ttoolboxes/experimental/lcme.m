function [ y, x ] = lcme( a, b, epsilon );
% [ x, y ] = lcme( a, b, epsilon );
% approximate lcm
% 
% Input:
%   a, b        number to compute the gcd for
%   epsilon     default = 0.1
% 
% Output:
%   x       minimum lcm of all integers which are away from a,b by less than max(a,b)*epsilon (approximately)
%
% See also: gcde, gcd, lcm
%
% Written by: tommsch, 01-03-2024

    if( nargin == 2 );
        epsilon = 0.1; end;
    assert( epsilon <= 1, 'lcme:epsilon', 'Epsilon must be less than 1.' );
    epsilon = epsilon * max( a, b );
    [nom, denom] = ratt( epsilon ); 
    a_s = a * denom;
    b_s = b * denom;

    x = inf;
    y = 0;
    a_s_mn = max( 1, floor( a_s - nom/2 ) );
    a_s_mx = ceil( a_s + nom/2 );
    b_s_mn = max( 1, floor( b_s - nom/2 ) );
    b_s_mx = ceil( b_s + nom/2 );
    bs = b_s_mn:b_s_mx;
    for as = a_s_mn:a_s_mx;
        val = lcm( as, bs );
        x = min( x, min(val) );
        y = max( y, max(val) );
    end
    y = y / (denom*denom);
    x = x / (denom*denom);
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
