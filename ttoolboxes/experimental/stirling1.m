function [ s ] = stirling1( n, k );
% [ s ] = stirling1( n, [k] )
% Stirling number of first kind
%
% Output:
%   s       stirling number of first kind
%           if k is given, only the k^th number is returned, otherwise all numbers of level n
%
% See also: stirling2
%
% Written by: 2024-11-28, tommsch

% Changelog:    tommsch,    2024-11-28,     New function `stirling1`

    if( nargin == 2 );
        if( k <= 0 || k > n );
            s = 0;
            return;
        elseif( k == 1 );
            s = factorial( n - 1 )*(-1)^(n-k);
            return;
        elseif( k == n - 3 );
            s = nchoosek( n, 2 )*nchoosek( n, 4 )*(-1)^(n-k);
            return;
        elseif( k == n - 2 );
            s = (3*n-1)/4*nchoosek( n, 3 )*(-1)^(n-k);
            return;
        elseif( k == n - 1 );
            s = nchoosek( n, 2 )*(-1)^(n-k);
            return;
        elseif( k == n );
            s = 1;
            return;
        else;
            end; end;  % do the whole computation

        
    s = [1];  %#ok<NBRAK2>
    for i = 1:n-1;
        s = conv( s, [1 -i] ); end;

    s = fliplr( s );

    if( nargin == 2 );
        s = s( k ); end;
end

function dummy; end   %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
