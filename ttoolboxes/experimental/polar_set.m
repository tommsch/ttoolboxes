function [ Bs ] = polar_set( B, N );
% (experimental) computes the polar of a set
%
% Written by: tommsch, 2025-01-25

% Changelog:    tommsch,    2025-01-25,     New (experimental) function `polar_set`
    if( nargin <= 1 || isempty(N) );
        N = 2000; end;
    dim = size( B, 1 );
    Bs = randn( dim, N );
    Bss = permute( Bs, [1 3 2] );
    dt = abs( sum( Bss .* B, 1 ) );
    sp = max( dt, [], 2 );
    sp = sp(:)';
    Bs = Bs./sp;
end


function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
