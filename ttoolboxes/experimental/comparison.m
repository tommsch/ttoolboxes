function [ M ] = comparison( M );
% [ C ] = comparison( M );
% (experimental) computes the comparison matrix of C
% i.e. C(i,j) = -| M(i,j) |  if i~=j
%      C(i,i) = | M(i,i) |
%
% Written by: tommsch, 2025-01-25

% Changelog:    tommsch,    2025-01-25,     New (experimental) function comparison

    [m, n] = size( M );
    M = -abs( M );
    M(1:m+1:n*m) = -M(1:m+1:n*m);
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
