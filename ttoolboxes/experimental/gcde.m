function [ x, y ] = gcde( a, b, epsilon );
% [ x, y ] = gcde( a, b, epsilon );
% approximate gcd
% 
% Input:
%   a, b    number to compute the gcd for
% 
% Output:
%   x       maximum gcd of all integers which are away from a,b by less than max(a,b)*epsilon (approximately)
%
% See also: lcme, gcd, lcm
%
% Written by: tommsch, 01-03-2024

    if( nargin == 2 );
        epsilon = 0.1; end;
    assert( epsilon <= 1, 'lcme:epsilon', 'Epsilon must be less than 1.' );
    
    epsilon = epsilon * max( a, b );
    [nom, denom] = ratt( epsilon ); 
    a_s = a * denom;
    b_s = b * denom;

    y = inf;
    x = 0;
    a_s_mn = floor( a_s - nom/2 );
    a_s_mx = ceil( a_s + nom/2 );
    b_s_mn = floor( b_s - nom/2 );
    b_s_mx = ceil( b_s + nom/2 );
    bs = b_s_mn:b_s_mx;
    for as = a_s_mn:a_s_mx;
        val = gcd( as, bs );
        y = min( y, min(val) );
        x = max( x, max(val) );
    end;
    x = x / denom;
    y = y / denom;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
