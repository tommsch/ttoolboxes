function [ M ] = carleman( dim, kind, a )

    if( nargin <= 2 || isempty(a) );
        a = randn; end;
    switch kind;
        case {0,'const','constant'};
            M = carleman_constant( dim, a );
        case {1,'identity'};
            M = eye( dim );
        case {2, 'constadd','constantaddition'};
            M = carleman_constadd( dim, a );
        case {3, 'log1p','logp1'};
            M = carleman_log1p( dim );
        case {4, 'mlog1m','mlogm1'};
            M = abs( carleman_log1p( dim ) );
        case {5, 'expm1','exp1m'};
            M = carleman_expm1( dim );
        case {6, 'exp'};
            M = carleman_exp( dim, a );
        case {7, 'mult','multiple'};
            M = carleman_mult( dim, a );
        case {8, 'lin','functional'};
            M = carleman_functional( dim, a );
        otherwise;
            error( 'carleman:kind', 'Wrong string for ''kind''.' ); end;

end

function [ M ] = carleman_constant( dim, a );
    M = [[a.^(0:dim-1)]' zeros(dim, dim-1)]; %#ok<NBRAK1>
end

function [ M ] = carleman_constadd( dim, a );
    if( numel(a) == 1 );
        a(2) = 1; end;
    M = eye( dim );
    s = a;
    for i = 1:dim-1;
        M(i+1, 1:numel(s)) = s;
        s = conv( s, a ); end;
end

function [ M ] = carleman_log1p( dim );
    M = eye( dim );
    for k = 1:dim-1;
        for j = 1:dim-1;
            M(j+1,k+1) = stirling1( k, j )*factorial(j)/factorial(k); end; end;
end

function [ M ] = carleman_expm1( dim );
    M = eye( dim );
    for k = 1:dim-1;
        for j = 1:dim-1;
            [~, ~, m] = stirling2( k, j );
            M(j+1,k+1) = m; end; end;
end

function [ M ] = carleman_exp( dim, a );
    M = zeros( dim );
    if( issym(a) );
        M = sym( M ); end;
    M(:,1) = 1;
    for k = 1:dim-1;
        for j = 1:dim-1;
            M(j+1,k+1) = (j*a)^k/factorial(k); end; end;
end

function [ M ] = carleman_mult( dim, a );
    M = diag( a.^(0:dim-1) );
end

function [ M ] = carleman_functional( dim, a );
    if( numel( a ) == 1 );
        a(2) = randn; end;
    M = carleman_constadd( dim, a );

end


function dummy; end   %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
