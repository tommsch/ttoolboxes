function [ ret, Z, al, q ] = ismoore( X, tol );
% (experimental) checks whether a matrix is a Moore matrix
% i.e. Z = [al1^(q^1) al1^(q^1) ... al1^(q^n)
%           .                        .
%           .                        .
%           .                        .
%          alm^(q^0) alm^(q^1) ... alm^(q^n)]
%
% Written by: tommsch, 2025-01-20

% Changelog:    tommsch,    2025-01-20,     New (experimental) function: ismoore
    
    if( ~ismatrix(X) );
        ret = false;
        return; end;

    if( nargin <= 1 || isempty(tol) );
        if( issym(X) );
            tol = 0;
        else;
            tol = eps( norm(X, 1) ) * max( size(X) )^2; end; end;
    n = size( X, 2 );
    if( n == 1 );
        Z = X;
        al = X;
        q = 1;
        ret = true;
        return; end;
    
    X1 = log( X );
    rllX1 = real( log( log( X ) ) );
    k = rllX1(:,2:end) - rllX1(:,1:end-1);  % `diff` does not work for symbolic input
    k(~isfinite(k)) = [];
    k_mean = median( k(:) );
    q = exp( k_mean );
    
    X2 = X1./(q.^(1:n));
    idx = isAlways( ~isfinite(X2) | abs(X2) < tol );
    X2(idx) = nan;
    if( issym(X) );
        d = X2(:,1);
    else;
        d = median( X2, 2, 'omitnan' ); end;
    al = exp( d );    
    
    Z = al.^(q.^(1:n));
    
    ret = isAlways( norm( Z - X ) <= tol );
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>