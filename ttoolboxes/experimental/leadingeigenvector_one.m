function [ v, d, err ] = leadingeigenvector_one( M, varargin );
% (experimental) Computes the leading eigenvector using an accelerated power method
% [ v ] = leadingeigenvector_one( M, [iter_max] );
%
% Written by: tommsch, 2024-10-28

% Changelog:    tommsch,    2024-10-28,     New function: leadingeigenvector_one

    assert( ~issym(M), 'leadingeigenvector_one:sym', 'This function does not work with symbolic input.' );

    [opt.verbose, varargin] = parsem( {'verbose','v'}, varargin, 1 );  % Verbose level
    [opt.iter_max, varargin] = parsem( {'iter_max'}, varargin, 1 );

    if( numel(varargin) == 0 );
        v = randn( size(M, 1), 1 );
    else;
        v = varargin{1};
        varargin(1) = []; end;

    parsem( varargin, 'test' );
    
    for i = 1:opt.iter_max
        v = M*v;
        x_new = v/norm( v );
        if( any( ~isfinite(x_new) ) || norm( v - x_new ) < 1e-12 );
            break; end;
        v = x_new; end;
        
    idx = abs( v ) >= 1e-12;
    res = (M*v) ./ v;
    d = mean( res(idx) );
    err = norm( M*v - d*v );
    if( ~isfinite(err) );
        err = inf;
        if( opt.verbose >= 0 );
            warning( 'leadingeigenvector_one:inexact', 'Could not compute leading eigenvector.' ); end;
    elseif( err > 1e-12 );
        if( nargout <= 2 );
            warning( 'leadingeigenvector_one:inexact', 'Computed vector seems to be wrong. Error: %g', err ); end; end;

    

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
