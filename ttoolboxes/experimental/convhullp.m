function [ cl ] = convhullp( pts, p )


    switch size( pts, 1 )
        case 2;
            cl = convhull2( pts, p );
        otherwise;
            error( 'convhullp:p', 'not implemented yet.' ); end;
        
        
end

function [ cl ] = convhull2( pts, p );
    q = 1/(1-1/p);
    
    I = 10000;
    cl = zeros( 2, I );
    for i = 1:I;
        a1 = rand();
        a2 = rand();
        n = norm( [a1 a2], q );
        a1 = a1/n;
        a2 = a2/n;
        idx1 = randi( size(pts, 2) );
        idx2 = idx1;
        while( idx2 == idx1 );
            idx2 = randi( size(pts, 2) ); end;
        cl(:,i) = a1*pts(:,idx1) + a2*pts(:,idx2);
    end
end

function [ cl ] = convhull2_new( pts, p );
    [dim, npts] = size( pts );
    q = 1/(1-1/p);
    
    I = 10000;
    cl = zeros( 2, I );
    for i = 1:I;
        a1 = rand();
        a2 = rand();
        n = norm( [a1 a2], q );
        a1 = a1/n;
        a2 = a2/n;
        
        idx1 = randperm( npts, randi(npts) );
        idx2 = randperm( npts, randi(npts) );
        al1 = rand( 1, numel(idx1) );
        al2 = rand( 1, numel(idx2) );
        al1 = al1/sum( al1 );
        al2 = al2/sum( al2 );
        pt1 = sum( pts(:,idx1).*al1, 2 );
        pt2 = sum( pts(:,idx2).*al2, 2 );
        %plotm( pt1, 'cx', 'ms',5 )
        %plotm( pt2, 'c.', 'ms',5 )
        
        cl(:,i) = a1*pt1 + a2*pt2;
    end
end

function test
    %%
    eps = .00001;
    pts = [-1 1;-1 -.5;-1 -1]';
    pts = [pts -pts];
    p = 1;
    
    clf; hold on;
    %axis( [-10 10 -10 10] )
    axis equal
    title( p )

    
    cl = convhull2( pts, p );
    %cln = convhull2_new( pts, p );
    

    %plotm( cln, 'g.' )
    %plotm( cln, 'hull', 'g' )
    plotm( cl, 'b.' )
    plotm( cl, 'hull', 'b' )
    
    
    
    plotm( 'origin' )
    plotm( pts, 'hull', 'linewidth',1, 'r' )
    plotm( pts, 'linewidth',2, 'ro', 'ms',10 )
    
    %%
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
