function [ S, T, U ] = stirling2( n, k );
% (experimental) [ S, T, U ] = stirling2( n, k )
% Stirling number of second kind
%
% Input:
%   n       size of set
%   k       number of subsets
%
% Output:
%   S       stirling number of second kind
%   T       S(k,j)*j!
%   U       S(k,j)*j!/k!
%
% Note:
%   The number of ways of partitioning a set of n elements into k nonempty sets iscalled a Stirling set number.
%   For example, the set {1,2,3} can be partitioned into
%       three subsets in one way: {{1},{2},{3}};                               % stirling2(3,1) == 1
%       two subsets in three ways: {{1,2},{3}}, {{1,3},{2}}, and {{1},{2,3}};  % stirling2(3,2) == 3
%       one subset in one way: {{1,2,3}}.                                      % stirling2(3,3) == 1
%   The interface of sterling2 is currently different than that of stirling1
%
% See also: stirling1
%
% Written by: 2005, Nikolaus Correll

% Changelog:    2024-11-28,     tommsch,    New function `stirling2`

    S = 0;
    for i = 0:k
        S = S+(-1)^i*factorial( k )/(factorial( i )*factorial( k - i ))*(k - i)^n; end;
    T = S;
    S = S/factorial( k );
    U = T/factorial( n );
end

function dummy; end   %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% Copyright (c) 2005, Nikolaus Correll
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met:
% 
% * Redistributions of source code must retain the above copyright notice, this
%   list of conditions and the following disclaimer.
% 
% * Redistributions in binary form must reproduce the above copyright notice,
%   this list of conditions and the following disclaimer in the documentation
%   and/or other materials provided with the distribution
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
