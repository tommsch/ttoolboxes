function [ out ] = tswitch( varargin );
% (experimental) Ternary switch operator
% [ out ] = tswitch( value, pattern1, return1, pattern2, return2, ..., default )
%
% Changelog:    2024-12-23,     tommsch,    New experimental function tswitch
    assert( mod( numel(varargin), 2 ) == 0 );
    for i = 2:2:nargin-1;
        switch varargin{1};
            case varargin{i};
                out = varargin{i+1};
                return; end; end;
    out = varargin{end}; 
    
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
