function [ cleanup_obj ] = bprintf( varargin );
% stores the last printout, and overwrites the last printout on the next call
    assert( nargout == 1, 'bprintf', 'The return value of the function must be stored.\n  On destruction of the return value the last printed number of characters are removed from the console' );
    if( numel(varargin) >= 1 && isnumeric(varargin{1}) );
        fid = varargin{1};
        varargin(1) = [];
    else;
        fid = 1; end;

    str = sprintf( varargin{:} );
    fprintf( fid, '%s', str );
    cleanup_obj = onCleanup( @() fprintf( repmat('\b', [1 numel(str)]) ) );

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.