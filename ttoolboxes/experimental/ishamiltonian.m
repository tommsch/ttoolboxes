function [ ret ] = ishamiltonian( A, tol );
% (experimental) [ ret ] = ishamiltonian( A, tol )
    if( nargin == 1 );
        tol = norm( A, 1 ) * eps * max( size( A ) ); end;
    sz = size( A );
    if( numel(sz) ~= 2 || ...
        sz(1) ~= sz(2) || ...
        mod( sz(1), 2 ) ~= 0 ...
      );
        ret = false;
        return; end;
    n = sz(1)/2;
    J = [zeros(n) eye(n); -eye(n) zeros(n)];
    P = J*A;
    ret = isAlways( norm( P - P' ) <= tol, 'Unknown','false' );
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
