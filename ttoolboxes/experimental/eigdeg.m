function [ deg, la, deg1 ] = eigdeg( M, tol_factor, verbose );
% [ deg, la, deg1 ] = eigdeg( M, [tol], [verbose] )
% [ deg, la, deg1 ] = eigdeg( la, [tol], [verbose] )
% (experimental) Computes the algebraic multiplicity of the eigenvalues
%
% Input:
%   M               a square matrix
%   la              vector of eigenvalues
%   tol_factor      optional, default = 1
%
% Output:
%   deg     the degrees of each eigenvalue
%   la      the eigenvalues
%   deg1    number of eigenvalues equal to the spectral radius
%
% Written by: tommsch, 2024-09-26

%               tommsch,    2024-09-26, New function introduced
% Changelog:    tommsch,    2024-10-28, Behaviour change: tol changed to tol_factor  
%               tommsch,    2024-10-29, More robust computation

    if( nargin <= 1 || isempty(tol_factor) );
        tol_factor = 1; end;
    if( nargin <= 2 || isempty(verbose) );
        verbose = 1; end;

    if( isvector(M) );
        la = M;
        if( ~issym(la) );
            tolerance = max( size(M) ) * eps( norm(M) );
        elseif( issymstrict(la) );
            tolerance = 0;
        elseif( isvpa(la) );
            tolerance = max( size(M) ) * 10^(-digits); end;
        tolerance = tolerance * tol_factor;
        la = leadingeigenvalue( M, tol_factor );
    else;
        la = leadingeigenvalue( M, tol_factor );

        if( ~issym(la) );
            tolerance = max( size(M) ) * eps( norm(M) );
        elseif( issymstrict(la) );
            tolerance = 0;
        elseif( isvpa(la) );
            tolerance = max( size(M) ) * 10^(-digits); end;
        tolerance = tolerance * tol_factor;

        if( all( ~issymstrict( la ) ) && ...
            norm( imag(la) ) > tolerance ...
          );
            la_s = leadingeigenvalue( M', tol_factor );

            if( norm( imag(la_s) ) <= tolerance );
                la = la_s;
            elseif( norm(la - la_s) <= tolerance );
                % do nothing
            else;
                if( verbose >= 1 );
                    warning( 'matrixinfo:vandergraft', 'Could not reliably compute multiples of eigenvectors.' ); end; end; end; end;
    rho = max( abs(la) );
    deg1 = nnz( isAlways( abs(rho - la) <= tolerance, 'Unknown','false' ) );
    deg = zeros( size(la) );
    for i = 1:numel( la );
        deg(i) = nnz( abs(la(i) - la) <= tolerance ); end;

end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
