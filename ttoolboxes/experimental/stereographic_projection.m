function [ p, n, o, err ] = stereographic_projection( p, n, o, reduce_dim );
% stereographic projection
% [ p, n, o, err ] = stereographic_projection( p, [n], [o], [reduce_dim] );
%
% Input:
%   p               matrix of column vectors, the points to be projected
%   n               column vector, default = center_kone( p ), normal vector of tangent plane, 
%                   the tangent plane touches the unit ball
%   o               scalar, default = -2, distance of projection point to tangent plane
%                       +inf            orthographic
%                       +5.62           GEO
%                       +0.05           LEO, ISS
%                       +1              MEO
%                       -1              gnonomic/rectilinear projection
%                       -2              stereographic projection (default)
%                       -2.4            twilight (clarke)
%                       -2.5            james
%                       -2-1/sqrt(2)    la hire
%                       -inf            orthographic
%                   
%   reduce_dim      bool, default = false
%
% Output:
%   p               the projected points
%   n               the used normal vector
%   o               the used value of o
%   err             if nargout => 4, then err is set to true  whenever there are points outside the projectable domain
%                   if nargout <= 3, then a warning is raised whenever there are points outside the projectable domain
%   
% Written by: tommsch, 2024-07-26

    if( nargin <= 1 || isempty(n) );
        n = center_kone( p, 'verbose',-1 ); end;
    if( any(isnan(n)) );
        n = mean( p, 2 ); end;
    n = n/norm( n );

    err = false;

    if( nargin <= 2 || isempty(o) );
        o = -2; end;
    
    if( nargin <= 3 || isempty(reduce_dim) );
        reduce_dim = false; end;

    if( ischar(o) || isstring(o) );
        switch lower( o );
            case {'orthographic','ortho','orth','inf'}; o = inf;
            case {'geo'},                               o = +5.62;
            case {'meo'},                               o = +1;
            case {'leo'},                               o = +0.05;
            case {'gnonomic','rectilinear','recti'};    o = -1;
            case {'stereographic','stereo'},            o = -2;
            case {'twilight','clarke'};                 o = -2.4;
            case {'james'},                             o = -2.5;
            case {'lahire','la hire','la_hire','hire'}; o = -2-1/sqrt(2);
            case {'-inf'};                              o = -inf;
            otherwise; error( 'stereographic_projection:method', 'Unkown method given for `o`.' ); end; end;


        assert( isscalar(o), 'stereographic_projection:method', 'Argument `o` must be a scalar, or a valid string.' );
        pn = sum( p.*n, 1 );
        if( isinf(o) );
            la = 1 - pn;
            p = la.*n + p;
        else;
            la = o./(o + 1 - pn);
            if( ~all( isAlways( la >= 0, 'Unknown','true' ) ) );
                if( nargout == 4 );
                    err = true;
                else;
                    warning( 'stereographic_projection:outside', 'Some points are outside of the projectable domain.' ); end; end;
            z = (o+1)*n;
            p = z + (p-z).*la; end;
    
        if( reduce_dim );
            ns = null( n' );
            p = ns' * p;
            end;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
