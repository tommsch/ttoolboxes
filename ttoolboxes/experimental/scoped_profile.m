function [ cleaner ] = scoped_profile();
    % starts the profiler, and returns a variable, which when destroyed, stops the profile and shows the "profile viewer".
    cleaner = onCleanup( @() clean_profile() );
    profile clear;
    profile on;
end

function clean_profile
    profile off;
    profile viewer;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>