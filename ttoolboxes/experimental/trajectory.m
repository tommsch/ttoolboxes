function trajectory( varargin );
% plots trajectories of vectors of the matrix-vector products
% trajectory( T, [v] );
%
% Input:
%   T     cell array of matrices
%   v     starting vector, random if not given
%
% Options:
%   'G',G           adjacancy matrix of graph, determines admissible products
%   'v',verbose     int, default = 1, verbosity level
%
% Output:
%   a nice picture
%
% Example:
%   A = randn( 2 );
%   B = randn( 2 );
%   trajectory( {A,B} );
%
% Written by: tommsch, 2023-04-3

    [G, varargin] =             parsem( {'G','graph'}, varargin, [] );
    [opt.verbose, varargin] =   parsem( {'v','verbose'}, varargin, 1 );
    
    cleanwarning = onCleanup_lazy( @(x) warning(x), warning('off','MATLAB:nearlySingularMatrix') );  %#ok<NASGU>

    v = [];
    if( numel(varargin) == 0 );
        M = gallery_matrixset( 'dim',2 );
    elseif( iscell(varargin{1}) || ...
        issquare(varargin{1}) && ismatrix(varargin{1}) );
        M = varargin{1};
        if( numel(varargin) >= 2 );
            v = varargin{2}; end;
    else;
        M = varargin{2};
        v = varargin{1}; end;

    if( ~iscell(M) );
        M = {M}; end;
    
    dim = size( M{1}, 1 );
    J = numel( M );
    
    if( isempty(v) );
        v = rand( dim, 1 );
        v = v/norm( v ); end;
    
    tr_sub = v;
    tr_main = v;
    color_sub = 0;
    color_main = 0;
    
    maxcounter = 2000;
    if( isempty(maxcounter) );
        maxcounter = 1000; end;
    step = .01;
    counter = 0;
    jk = randi( J );
    while( true );
        if( ~isempty(G) );
            jk_new = find( G(jk,:) );
            jk = jk_new( randi(numel(jk_new)) );
            vprintf( '%i ', jk, 'imp',[2 opt.verbose] );
        else;
            jk = randi( J ); end;
        trk = tr_main(:,end);
        sub = linspace( 0, 1, 1/step );
        for s = sub;
            tr_sub(:,end+1) = real( mpower( M{jk}, s ) ) * trk(:,end);  %#ok<AGROW>
            color_sub(1,end+1) = jk;  %#ok<AGROW>
        end;  
        tr_main(:,end+1) = M{jk} * tr_main(:,end);  %#ok<AGROW>
        color_main(1,end+1) = jk;  %#ok<AGROW>
        counter = counter + 1;
        if( opt.verbose >= 2 );
            plot_trajectory( tr_main, tr_sub, color_main, color_sub, J ); end;
        if( norm(tr_main(:,end)) > 10 );
            vprintf( 'O', 'imp',[1 opt.verbose] );
            break;
        elseif( norm(tr_main(:,end))<1e-9 );
            vprintf( '_', 'imp',[1 opt.verbose] );
            break;
        elseif( counter>=maxcounter );
            vprintf( '.', 'imp',[1 opt.verbose] );
            break; end; end;
     
    if( opt.verbose >= 1 );
        plot_trajectory( tr_main, tr_sub, color_main, color_sub, J ); end;
    
end

function plot_trajectory( tr_main, tr_sub, color_main, color_sub, J )
    plotm( real(tr_main(:,1)), 'r.', 'MarkerSize',20 );
    if( ~isreal(tr_main) );
        plotm( real(tr_sub), 'b-', 'color',color_sub+J+1, 'hold',2 );
        plotm( real(tr_main), 'bx', 'color',color_main+J+1, 'hold',2 );
        plotm( imag(tr_sub), 'r-', 'color',color_sub, 'hold',2 );
        plotm( imag(tr_main), 'rx', 'color',color_main, 'hold',2 );
    else;
        plotm( real(tr_sub), '-', 'color',color_sub, 'hold',2 );
        plotm( tr_main, 'rx', 'color',color_main, 'hold',2 ); 
%         tr_sub2 = unique( real(tr_sub)', 'rows' )';
%         plotm( tr_sub2, 'k-', 'hull',1, 'hold',2 ); 
        
        dim = size( tr_main, 1 );
        plotm( zeros(dim, 1), 'kx', 'hold',2 ); 
    end;
    drawnow;
end

function dummy; end %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
