classdef named
% named( something );
% A class which just stores something and displays it nicely
% Input:
%   something   any type, which will get displayed nicely
%
% Written by: tommsch, 2024-09-05

%               2024-09-05, tommsch,    Introduction of new class 'named'
% Changelog: 

    properties
        value
    end

    methods
        %% ctors
        function [ obj ] = named( value_ );
            assert( nargin == 1, 'named:nargin', 'This function needs exactly on input argument of any type.' );
            obj.value = value_;
        end

        %% member functions
        function [ obj ] = eq( o1, o2 );
            obj = isequal( o1, o2 );
        end
        function [ obj ] = ne( o1, o2 );
            obj = ~isequal( o1, o2 );
        end

        %% display functions
        function [ str ] = string( obj );
            str = '';
            for i = 1:numel( obj );
                str = [str TTEST_DISP( 255, obj(i).value ) ' ']; end;  %#ok<AGROW>
            str = strtrim( str );
        end

        function disp( obj );
            fprintf( '%s ', string(obj) );
        end

        function display( obj );  %#ok<DISPLAY>
            fprintf( '%s \n', string(obj) );
        end

    end
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
