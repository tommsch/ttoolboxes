function [ y ] = upsample( x, n, varargin )
% basic implementation of upsample
    
    if( iscolumn(x) );
        dim = 1;
    elseif( isrow(x) );
        dim = 2;
    else;
        dim = 1; end;
    
    expect( numel(varargin) == 0 && nargout <= 1, 'ttoolboxes:shim', 'A shim for upsample is used, which does not understands all options. All additional options are discarded.' );
    assert( n >= 1 && floor(n) == n, 'ttoolboxes:upsample:expectedPositiveInteger', 'Upsampling factor L must be a positive integer.' );
    assert( dim >= 1 && floor(dim) == dim && dim <= ndims(x), 'ttoolboxes:upsample', 'Dimension dim must be a positive integer within the array dimensions.' );
    
    sz = size( x );  % Get the size of the input array
    sz_out = sz;  % Determine the size of the output array
    sz_out(dim) = sz(dim) * n;  % Upsample along the specified dimension
    y = zeros( sz_out, class(x) );  % Initialize the output array with zeros % Preserve the class (data type) of the input array
    indices = repmat( {':'}, 1, ndims(x) );  % Create a cell array of indices for assigning values
    indices{dim} = 1:n:sz_out(dim);  % Assign the original values to the correct positions in the output array % Specify the positions along the upsampled dimension
    y(indices{:}) = x;
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
