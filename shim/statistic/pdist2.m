function [ dis ] = pdist2( x, y, varargin );
% basic implementation of pdist2
    expect( numel(varargin) == 0 && nargout <= 1, 'ttoolboxes:shim:options', 'A shim for pdist2 is used, which does not understands all options. All additional options are discarded.' );
        
    sze_x = size( x );
    sze_y = size( y );
    assert( sze_x(2) == sze_y(2), 'ttoolboxes:pdist2:SizeMismatch', 'Dimension of input wrong.' );
    
    dis = zeros( sze_x(1), sze_y(1) );
    for i = 1:sze_x(1);
        dis(i,:) = sqrt( sum(abs(x(i,:) - y).^2, 2) );
    end
end
 
function [ dis ] = pdist2_old( x, c, varargin );  %#ok<DEFNU>
% basic implementation of pdist2
    expect( numel(varargin) == 0 && nargout <= 1, 'ttoolboxes:shim', 'A shim for pdist2 is used, which does not understands all options. All additional options are discarded.' );
        
    [ndata, dimx] = size( x ); 
    [ncentres, dimc] = size( c ); 
    assert( dimx == dimc, 'polytopenorm:fatal', 'Fatal error' );
    dis = (ones( ncentres, 1 ) * sum( x.^2, 2 ))' + ones( ndata, 1 ) * sum( c.^2, 2 ) - 2.*(x*(c'));
    idx = dis < 0;
    dis( idx ) = 0;
    dis = sqrt( diag(dis) ).';
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
