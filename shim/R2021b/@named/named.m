classdef named <  matlab.mixin.CustomCompactDisplayProvider
% named( something );
% (experimental) A class which just stores something and displays it nicely
% Input:
%   something   any type, which will get displayed nicely
%
% Note: For octave compatibility, and for compatibility with older Matlab versions there exists another shim
%
% Written by: tommsch, 2024-09-05

%               2024-09-05, tommsch,    Introduction of new class 'named'
% Changelog: 

    properties
        value
    end

    methods
        %% ctors
        function [ obj ] = named( value_ );
            assert( nargin == 1, 'named:nargin', 'This function needs exactly on input argument of any type.' );
            obj.value = value_;
        end

        %% member functions
        function [ obj ] = eq( o1, o2 );
            obj = isequal( o1, o2 );
        end
        function [ obj ] = ne( o1, o2 );
            obj = ~isequal( o1, o2 );
        end

        %% display functions

        function [ rep ] = compactRepresentationForSingleLine( obj, displayConfiguration, ~ );
            persistent warning_shown;
            v = ver( 'matlab' );  %#ok<VERMATLAB>
            show_warning = false;
            switch v.Version;
                case {'23.2','9.15','9.15.0'}  % R2023b
                    if( iswindows );
                        % This bloody hack removes the parentheses in the textual representation. It works for R2023b on Windows
                        evalat( 'in','matlab.display.PlainTextRepresentation', ...
                                'at', '% Compute the character width for display string', ...
                                'eval', 'for i = 1:numel( paddedDisplayOutput ); if( isequal(paddedDisplayOutput{i}(1), ''['') && isequal(paddedDisplayOutput{i}(end), '']'') ); paddedDisplayOutput{i}( [1 end] ) = []; end; end;', ...
                                'once', true ...
                              );
                    else;
                        show_warning = true; end;
                otherwise;
                    show_warning = true; end;
            if( show_warning && (warning_shown) );
                warning( 'named:unsupportedversion', 'A nice formatting of the output for the type ''named'' is not implemented for this Version.\n  This warning is only shown once.' );
                warning_shown = true; end;
            rep = fullDataRepresentation( obj, displayConfiguration );
        end

        function [ rep ] = compactRepresentationForColumn( obj, displayConfiguration, ~ )
            rep = fullDataRepresentation( obj, displayConfiguration );
        end

        function [ str ] = string( obj );
            str = '';
            for i = 1:numel( obj );
                str = [str TTEST_DISP( 255, obj(i).value ) ' ']; end;  %#ok<AGROW>
            str = strtrim( str );
        end

        function disp( obj );
            fprintf( '%s ', string(obj) );
        end

        function display( obj );  %#ok<DISPLAY>
            fprintf( '%s \n', string(obj) );
        end

    end
end

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
