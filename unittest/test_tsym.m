% test double

%#ok<*CTPCT>
%#ok<*ASGLU>
%#ok<*NASGU>

TTEST init testsym

ASSUME_TOOLBOX( 'symbolic', OPTION('load') );

% preconditions    

%% test isAlways
TESTCASE( 'isAlways' );
EXPECT_EQ( 1, isAlways(4) );
EXPECT_EQ( 0, isAlways(0) );
EXPECT_EQ( 1, isAlways(-1) );
err = lasterr();  %#ok<LERR>
EXPECT_EQ( 0, isAlways(NaN, [], 'false') );
EXPECT_EQ( 1, isAlways(NaN, [], 'true') );
lasterr( err );  %#ok<LERR>
EXPECT_EQ( 1, isAlways(Inf) );

EXPECT_THROW( @() isAlways(NaN), 'sym:isAlways' );
EXPECT_EQ( isAlways(NaN, [], 'false'), 0 );

%% test simplify
TESTCASE( 'simplify' );
EXPECT_EQ( 4, simplify(4));
EXPECT_PRED( @isnan, simplify(NaN));
EXPECT_EQ( inf, simplify(inf));
EXPECT_EQ( 1/0., simplify(1/0.));

%% test symmax
a = symmin( sym([0 1]) );
b = symmax( sym([0 1]) );
EXPECT_EQ( a, sym(0) );
EXPECT_EQ( b, sym(1) );