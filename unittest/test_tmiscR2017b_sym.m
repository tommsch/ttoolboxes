% test tmiscR2017b
TTEST init test_tmiscR2017b_sym

assert( EXPECT_TOOLBOX('matlab', '>=R2017b', APPLIED('onmatlab') ) );

ASSUME_TOOLBOX( 'symbolic', OPTION('load') );

% preconditions

%% test liminf
TESTCASE
EXPECT_EQ( liminf(sym([10 1 9 2 8 3 7 4 6])), sym([1 1 2 2 3 3 4 4 6]) );


%% test limsup
TESTCASE
EXPECT_EQ( limsup(sym([10 1 9 2 8 3 7 4 6])), sym([10 9 9 8 8 7 7 6 6]) );

%#ok<*NOSEL,*NOSEMI,*ALIGN>

