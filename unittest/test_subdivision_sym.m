TTEST init test_subdivision_sym
% symbolic tests

%#ok<*NASGU>
%#ok<*CTPCT>
%#ok<*ASGLU>

% preconditions

TTEST hardinit subdivision_sym

ASSUME_TOOLBOX( 'symbolic', OPTION('load') );
ASSERT_TOOLBOX( 'matlab', '>=R2016b', APPLIED('onmatlab') );

%% blf
TESTCASE
warning( 'off', 'OctSymPy:sym:rationalapprox' );
Ssym = getS('1_121');
Ssym{1} = Ssym{1}.set_c( sym(Ssym{1}.c) );
EXPECT_NTHROW( @() blf( Ssym, 'iteration',4, 'verbose',0 ), 'OctSymPy:sym:rationalapprox' );

%% constructdigit
TESTCASE
warning( 'off', 'OctSymPy:sym:rationalapprox' );
EXPECT_NTHROW( @() constructdigit( [2 0; 1 2], 'ZZ',[0 0; 1 0]','sym') );
EXPECT_NTHROW( 'constructdigit( [2 0; 1 2], ''ZZ'',[0 0; 1 0]'',''classify'',''sym'',''verbose'',0);' );
EXPECT_NTHROW( 'constructdigit( [10 9;11 9],''classify'',''ZZ'',[0 0;1 2;10 15]'',''sym'');' );
EXPECT_NTHROW( 'constructdigit( [10 9;11 9]);' );
EXPECT_NTHROW( @() constructdigit( sym([10 9;11 9]) ), 'OctSymPy:sym:rationalapprox' );

%% constructV_constructVt
TESTCASE
if( isoctave );
    warning( 'off', 'Octave:colon-nonscalar-argument' ); end;
Om = [0 1 2 3];
EXPECT_EQ( constructV(sym(Om)), constructV(Om) );
EXPECT_EQ( constructV(sym(Om)), constructV(Om) );
EXPECT_EQ( constructVt(sym(Om), 0), constructVt(Om, 0) );
EXPECT_EQ( constructVt(sym(Om), 0), constructVt(Om, 0) );

%% daubechiesmask
TESTCASE
EXPECT_PRED( @issym, daubechiesmask( 5 ) );
EXPECT_EQ( daubechiesmask( 0 ), 2 );

%differencescheme %untested

%% getS
TESTCASE
EXPECT_THROW( @() getS( 'a',1/2*[1 2 1]','M',2,'D',[0 2],'bigcheck')', 'm', 'getS:digit', 'o', 'getS:bigcheck', 'o','getS:sumrule' );
EXPECT_THROW( @() getS( 'a',1/2*[1 1]','M',1,'D',[0 2],'bigcheck'), 'getS:notexpanding', 'getS:bigcheck' );
EXPECT_NTHROW( @() getS('3_rand'), 'normalizeS:support', 'normalizeS:scaling' );
EXPECT_NTHROW( 'getS(''1_all'',''nocheck'');', 'normalizeS:scaling', 'normalizeS:support', 'constructdigit:sym' );  % 1_all also includes non-stationary schemes

%% normalizeS
TESTCASE
S = getS({[1 1 1]',2},'nocheck');
EXPECT_NTHROW( 'normalizeS(S,''scale'');' );
EXPECT_THROW( 'normalizeS({[1 0 1],2});', 'normalizeS:support' );
EXPECT_NTHROW( 'normalizeS(S,''scale'');' );
EXPECT_NTHROW( 'normalizeS(S,''gauss'');' );
Sn = normalizeS(S,'equal');
EXPECT_EQ( Sn{1,1}.c,1/2*[1 2 1]');
EXPECT_THROW( @() normalizeS(getS({[1 2 -1]',2},'nocheck')), 'normalizeS:scaling' );

%% restrictmatrix
TESTCASE
syms xxx;
EXPECT_THROW( '[TA, TT, TR, NULL, BASIS, flag] = restrictmatrix( sym([1 2;-1 -2]),[xxx -1]'' );', 'restrictmatrix:notest' );
EXPECT_TRUE( isnan(flag) || isequal(flag,0) );

%% tile
TESTCASE
S = getS( '2_butterfly' );
EXPECT_THROW( @() tile({[1 2 2 1],[]},[getS('2_rand');S],'verbose',0,'iteration',5), 'tile:oolength', 'o','normalizeS:support','normalizeS:scaling', 'MATLAB:dimagree','getS:dimM' );
EXPECT_NTHROW( 'tile( getS(''M'',3,''D'',[0 1 5]), ''interval'', ''start'',{[0 1]}, ''round'',0, ''v'',-1 );', 'tile:experimental' );
EXPECT_NTHROW( @() tile([getS('2_rand');getS('2_rand')], 'supertile',6, 'round',[1e-2 1e-4], 'verbose',0), 'normalizeS:support', 'normalizeS:scaling' );


%%
ENDTESTCASE();
%#ok<*NOSEL,*NOSEMI,*ALIGN>

