% test cell

%#ok<*CTPCT>
%#ok<*ASGLU>
%#ok<*NASGU>

TTEST init test_cell_sym

% preconditions   

ASSUME_TOOLBOX( 'symbolic', OPTION('load') );

%% double
TESTCASE;
EXPECT_EQ( double({sym(1) {sym(2)}}), {1 {2}});
EXPECT_EQ( double([sym(1) sym(2)]), [1 2]);

%%
ENDTESTCASE;

