% test sequence

%#ok<*CTPCT>
%#ok<*ASGLU>
%#ok<*NASGU>

TTEST init tt_experimental

% preconditions    

%% repr
A1 = [1 2 3];
A2 = A1;
evalc( repr( A1 ) );
EXPECT_EQ( A1, A2 );

%% leading_eigenvalue
EXPECT_EQ( leadingeigenvalue(0), 0 );
EXPECT_EQ( leadingeigenvalue(-1), -1 );
EXPECT_EQ( sort(leadingeigenvalue([1 0;0 -1])), [-1; 1] );

%#ok<*NOSEL,*NOSEMI,*ALIGN>
