% test version

%#ok<*CTPCT>
%#ok<*ASGLU>
%#ok<*NASGU>

TTEST init install;

%% install

TESTCASE install
    [~, dir_ttoolboxes] = cwd( 'ttoolboxes_install' );
    [~, dir_TTEST] = cwd( 'TTEST' );
    [~, dir_gurobi] = cwd( 'gurobi_setup' );
    restoredefaultpath
    
    cd( dir_TTEST );
    TTEST install experimental path unattended verbose 0
    
    cd( dir_gurobi );
    evalc( 'gurobi_setup' );
    
    cd( dir_ttoolboxes )
    ttoolboxes_install 0 0
    
    SECTION
        cd unittest
        runttests -*slow -ttest_install
    
    
    


ENDTESTCASE
