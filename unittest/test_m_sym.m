% test sequence

%#ok<*CTPCT>
%#ok<*ASGLU>
%#ok<*NASGU>

TTEST init m_sym

ASSUME_TOOLBOX( 'symbolic', OPTION('load') );
    

% preconditions    

%% allm
syms a;
EXPECT_NTHROW( @() allm([[1 a 0];[1 1 0]],1), 'symbolic:sym:isAlways:TruthUnknown' );
  % extra brackets are needed for Octave

%% anym
syms a;
EXPECT_NTHROW( @() anym([[a a 0];[0 0 0]],1), 'symbolic:sym:isAlways:TruthUnknown' );

%% convm
EXPECT_EQ( convm([1 2].',sym([2 1].')), sym([2 5 2].') );

seq1 = convm( sym([1 1].'), [1 2].' ); 
EXPECT_ALMOST_EQ( seq1, [1 3 2].' );
if( ismatlab )
    seq1 = convm( sym([1 1]), sym([1; 2]) ); 
    EXPECT_ALMOST_EQ( seq1, [1 1;2 2] ); 
else;
    TODO_FAIL( 'Multivariate symbolic convolution not possible in Octave 6.3' );
    end;


%% mldividem
if( ismatlab )
    EXPECT_EQ( mldividem( [2 3 4 5], [sym(1)] ), [0 0 0 sym(0.2)].' );
    % EXPECT_EQ( mldividem( [sym(2)], [1 2; 3 4] ), sym([.5 1;1.5 2]) );  % not implemented yet
    EXPECT_EQ( mldividem( [2 0;1 1;0 2].', sym([1 9].')/10 ), sym([0.05 0 0.45].') );
    end;
%XX missing tests
% A = magic(4); b = [34 34 34 34].';
% val1 = mldividem( sym(A), b );
% val2 = mldividem( A, b );
%
% A = [1 0;0 0].'; b = [1 1].'
% val1 = mldividem( sym(A), b );
% val2 = mldividem( A, b );

%% padarraym 
if( ismatlab )
    syms z
    Z = [z z^2];
    EXPECT_EQ( padarraym(Z,1), sym([0 0 0 0;0 z z^2 0;0 0 0 0]) );
else;
    TODO_FAIL( 'paddarraym for symbolics does not work in Octave currently.' ); end;

%% upsamplem

%#ok<*NOSEL,*NOSEMI,*ALIGN>
