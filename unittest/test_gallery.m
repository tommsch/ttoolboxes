%#ok<*CTPCT>
%#ok<*ASGLU>
%#ok<*NASGU>

TTEST init gallery_matrixset

%% gallery_matrixset
TESTCASE
%%
SECTION( 'missing arguments' );
    EXPECT_NEMPTY( gallery_matrixset( 'dim',10, 'rand' ) );
    EXPECT_NEMPTY( gallery_matrixset( 'v',0 ) );

%%
SECTION( 'general tests' );
    EXPECT_PRED( @(x) numel(x) == 2,                            gallery_matrixset( 'rand',                      'dim',5, 'N',2 ) );
    EXPECT_PRED( @(x) all(matrixinfo(x, 'stochastic_column')),  gallery_matrixset( 'rand_stochastic',           'dim',5, 'N',2 ) );
    EXPECT_PRED( @(x) all(matrixinfo(x, 'stochastic_double')),  gallery_matrixset( 'rand_doublestochastic',     'dim',5, 'N',2 ) );
    EXPECT_NPRED( @isempty,                                     gallery_matrixset( 'rand_stochastic_neg',       'dim',5, 'N',2 ) );
    EXPECT_NPRED( @isempty,                                     gallery_matrixset( 'rand_doublestochastic_neg', 'dim',5, 'N',2 ) );
    EXPECT_PRED( @(x) all(matrixinfo(x, 'pm1')),                gallery_matrixset( 'rand_pm1',                  'dim',5, 'N',2 ) );
    EXPECT_PRED( @(x) all(matrixinfo(x, 'bool')),               gallery_matrixset( 'rand_bool',                 'dim',5, 'N',2 ) );
    EXPECT_NPRED( @isempty,                                     gallery_matrixset( 'rand_gauss',                'dim',5, 'N',2 ) );
    %EXPECT_PRED( @(x) all(matrixinfo(x, 'sparsity')),           gallery_matrixset( 'rand_gauss',                'dim',4, 'N',2, 'sparse',.5 ) );
    %EXPECT_PRED(  @(x) all(matrixinfo(x, 'int')),               gallery_matrixset( 'rand_gauss',                'dim',5, 'N',2, 'int',1 ) );
    EXPECT_PRED( @(x) all(matrixinfo(x, 'pos')),                gallery_matrixset( 'rand',                      'dim',5, 'N',2 ) );
    EXPECT_PRED(  @(x) all(matrixinfo(x, 'pos')),               gallery_matrixset( 'rand_equal',                'dim',5, 'N',2 ) );
    EXPECT_NPRED( @isempty,                                     gallery_matrixset( 'rand_neg',                  'dim',5, 'N',2 ) );
    EXPECT_NPRED( @isempty,                                     gallery_matrixset( 'rand_equal_neg',            'dim',5, 'N',2 ) );
    EXPECT_PRED( @(x) all(matrixinfo(x, 'unitary')),            gallery_matrixset( 'rand_unitary',              'dim',5, 'N',2 ) );
    EXPECT_PRED( @(x) all(matrixinfo(x, 'rhozero')),            gallery_matrixset( 'rhozero',                   'dim',5, 'N',2 ) );
    EXPECT_NPRED( @isempty,                                     gallery_matrixset( 'rand_colu',               'dim',5, 'N',2 ) );
    EXPECT_NPRED( @isempty,                                     gallery_matrixset( 'rand_corr',               'dim',5, 'N',2 ) );
    EXPECT_PRED( @(x) all(matrixinfo(x, 'hessu')),              gallery_matrixset( 'rand_hess',                 'dim',5, 'N',2 ) );
    EXPECT_NPRED( @isempty,                                     gallery_matrixset( 'binary',                    'dim',5, 'N',2, 10, 'v',-1) );
    EXPECT_NPRED( @isempty,                                     gallery_matrixset( 'mejstrik_119',              'N',2, 10, 'v',-1 ) );
    EXPECT_NPRED( @isempty,                                     gallery_matrixset( 'mejstrik_Cn',               'N',2, 10, 'v',-1 ) );
    EXPECT_NO_THROW( @()                                        gallery_matrixset( 'rand_gauss',                'dim',5, 'N',2, 'seed',rng, 'sparse',.5, 'bool', 'v',-1 ) );
    EXPECT_PRED( @(x) all(matrixinfo(x, 'int')),                gallery_matrixset( 'integer',                   'dim',2, 'J',2, 'v',-1 ) );
    
%%
SECTION( 'properties' );
    EXPECT_NEMPTY( gallery_matrixset( 'mejstrik_Cn', 30 ) );

%%
SECTION( 'kone' );
    EXPECT_NTHROW( @() gallery_matrixset( 'kone_ab' ) );
    EXPECT_NTHROW( @() gallery_matrixset( 'kone_p10_1', 2 ) );
    EXPECT_THROW( @() gallery_matrixset( 'kone_rss10_72', [1 2 3 4] ), 'gallery_matrixset:args' );
    EXPECT_THROW( @() gallery_matrixset( 'kone_rss10_72', [1 2 3 4 5 1] ), 'gallery_matrixset:args' );
    EXPECT_THROW( @() gallery_matrixset( 'kone_rss10_72', [1 2 3 4 5 1] ), 'gallery_matrixset:args' );
    EXPECT_NEMPTY( gallery_matrixset( 'kone_rss10_76' ) );
    EXPECT_NUMEL( gallery_matrixset( 'kone_rss10_76', [.5 1 2 3] ), 3 );
    
%#ok<*NOSEL,*NOSEMI,*ALIGN>
