
TTEST init sedumi

ASSERT_TOOLBOX( 'sedumi' );

%%
TESTCASE
A = [1 1;1 1/4;1 -1;-1/4 -1;-1 -1;-1 1];
b = [2 1 2 1 -1 2];
f = [-1 -1/3];

SECTION
    x = sedumi_linprog( f, A, b );
    EXPECT_NEAR( x, [2/3 4/3]', 1e-9 );


SECTION
    Aeq = [1 1/4];
    beq = 1/2;
    x = sedumi_linprog( f, A, b, Aeq, beq );
    EXPECT_NEAR( x, [0 2]', 1e-7 );

SECTION
    Aeq = [1 1/4];
    beq = 1/2;
    lb = [-1,-0.5];
    ub = [1.5,1.25];
    x = sedumi_linprog( f, A, b, Aeq, beq, lb, ub );
    EXPECT_NEAR( x, [0.1875  1.2500]', 1e-9 );

SECTION
    if( tt.expect_toolbox( 'optim' ) );
        Aeq = [1 1/4];
        beq = 1/2;
        lb = [-inf,-0.5];
        ub = [inf,inf];
        x = sedumi_linprog( f, A, b, Aeq, beq, lb, ub );
        options = optimoptions('linprog','Display','off');
        y = linprog( f, A, b, Aeq, beq, lb, ub, options );
        EXPECT_NEAR( x, y, 1e-9 ); end;

SECTION
    Aeq = [1 1/4];
    beq = 1/2;
    lb = [-1,-0.5];
    ub = [1.5,1.25];
    if( tt.expect_toolbox( 'optim' ) );
        options = optimoptions('linprog','Algorithm','interior-point');
    else;
        options.Algorithm = 'interior'; end;
    
    EXPECT_THROW( @() sedumi_linprog( f, A, b, Aeq, beq, lb, ub, options ), 'sedumi_linprog:opt' );
    
%%    
% TESTCASE
% SECTION
%     m = randi( 9 ) + 5;
%     me = randi( 9 ) + 5;
%     n = randi( 9 ) + 5;
%     f = randn( 1, n );
%     A = rand( m, n );
%     b = -10*rand( m, 1 );
%     Ae = randn( me, n );
%     be = randn( me, 1 );
%     lb = -100*rand( n, 1 ); lb(lb<-1) = -inf;
%     ub = 100*rand( n, 1 ); ub(ub>1) = inf;
%     %options = optimoptions('linprog','Display','off');
%     se = sedumi_linprog( f, A, b, Ae, be, lb, ub, [] )';
%     ma = linprog( f, A, b, Ae, be, lb, ub, [] )';
%     gu = gurobi_linprog( f, A, b, Ae, be, lb, ub, [] )';
    
%#ok<*NOSEL,*NOSEMI,*ALIGN>
