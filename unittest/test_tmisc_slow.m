% test tmisc

%#ok<*NASGU>
%#ok<*CTPCT>
%#ok<*ASGLU>

TTEST hardinit tmisc_slow

% preconditions



%% normalizematrix
TESTCASE
MM = [1 2 3; 0 -1 2];
EXPECT_NO_THROW( 'normalizematrix( [], ''colsum'' );' );
val = normalizematrix( MM, 'rowsum' );
EXPECT_ALMOST_EQ( sum(val,2), 1 );
EXPECT_NO_THROW( 'normalizematrix( MM, ''dirsum'',3 );' );
EXPECT_ALMOST_EQ( normalizematrix(MM,'dirmax',1), [1 1 1;0 -.5 2/3] );

val = normalizematrix( MM, 'dirnorm',[2 2] );
EXPECT_ALMOST_EQ( sum(val.^2,2), [1;1] );

MM = [-1 2; 0 3];
val = normalizematrix( MM,'rho');
EXPECT_ALMOST_EQ( max(abs(eig(val))), 1 );

val = normalizematrix( MM, 'norm',inf );
EXPECT_ALMOST_EQ( norm(val,inf), 1 );
EXPECT_EQ( normalizematrix(MM,'binary'), [1 1;0 1] );
EXPECT_EQ( normalizematrix(MM,'positive',2),[1 -2;0 3] );
EXPECT_EQ( normalizematrix({[1 2],[-4 2]},'dotprod',{[1 1],[0 1]}), {[1/3 2/3],[-2 1]} );
EXPECT_EQ( normalizematrix([1 2],'dotprod',[1 1]), [1/3 2/3] );
EXPECT_THROW( 'normalizematrix([10 20 30 40],''wrongstring'');', 'normalizematrix:opt' );

v = randn( 3,1 ) + 1i*randn( 3, 1 );
vs = randn( 3, 1 ) + 1i*randn( 3, 1 );
vs = normalizematrix( vs, 'dot', v );
EXPECT_NEAR( dot(v, vs), dot(vs, v), 1, 4e-15 );

v = randn( 3,1 );
vs = randn( 3, 1 );
vs = normalizematrix( vs, 'dot', v );
EXPECT_ALMOST_EQ( dot(v, vs), dot(vs, v), 1 );
