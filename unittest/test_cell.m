% test cell

%#ok<*CTPCT>
%#ok<*ASGLU>
%#ok<*NASGU>

TTEST init test_cell

% preconditions    

%% diag
TESTCASE;
EXPECT_EQ( diag({{1 2 3;2 3 4}}), {{1 2 3;2 3 4}});
EXPECT_EQ( diag({1 2 3;2 3 4}), {1 3}');
EXPECT_EQ( diag({1 2 3;2 3 4},inf), cell(0,1));
EXPECT_EQ( diag({1 2 3;2 3 4},-inf), cell(0,1));
EXPECT_EQ( diag({1 2 3;2 3 4},3), cell(0,1));
EXPECT_EQ( diag({1 2 3;2 3 4},1), {2 4}');
EXPECT_EQ( diag({1 2 3;2 3 4},-1), {2}');
EXPECT_EQ( diag({1 2 3;2 3 4},-3), cell(0,1));
EXPECT_EQ( diag({}),{});
EXPECT_EQ( diag({1 2}), {1 [];[] 2});
EXPECT_EQ( diag({1 2}'), {1 [];[] 2});
EXPECT_EQ( diag({1 2}',1), {[] 1 [];[] [] 2; [] [] []});
EXPECT_EQ( diag({1 2}',-1), {[] [] []; 1 [] [];[] 2 []});
EXPECT_THROW( @() diag({1 2}', -inf), 'diag:input' );
EXPECT_THROW( @() diag({1 2}', inf), 'diag:input' );

%% double
TESTCASE;
EXPECT_EQ( double(1), 1);
EXPECT_EQ( double({1}), {1});

%% arithetmics
if( ismatlab );
TESTCASE
SECTION rldivide
    EXPECT_EQ( {[1 2;3 4],[5 6;7 8]}.\[1 3;3 1], {[1 2;3 4]\[1 3;3 1],[5 6;7 8]\[1 3;3 1]} );
    EXPECT_EQ( [1 3;3 1].\{[1 2;3 4],[5 6;7 8]}, {[1 3;3 1]\[1 2;3 4], [1 3;3 1]\[5 6;7 8]} );
    EXPECT_EQ( {1 2}.\[1 3;3 1], {1.\[1 3;3 1], 2.\[1 3;3 1]} );
    EXPECT_EQ( {2 3}.\{4 5}, {4/2 5/3} );

SECTION minus
    EXPECT_EQ( {1 2;3 4;5 6}-3, {-2 -1;0 1;2 3} );
    EXPECT_EQ( 3-{1 2;3 4;5 6}, {2 1;0 -1;-2 -3} );
    EXPECT_EQ( {1 2;3 4;5 6}-[3 -1], {[-2 2] [-1 3];[0 4] [1 5];[2 6] [3 7]} );
    EXPECT_EQ( [3 -1]-{1 2;3 4;5 6}, {[2 -2] [1 -3];[0 -4] [-1 -5];[-2 -6] [-3 -7]} );
    EXPECT_EQ( {2}-{3}, {-1} );
    EXPECT_EQ( {}-{}, {} );
    EXPECT_EQ( {1}-[], {[]} );
    EXPECT_EQ( {}-1, {} );

SECTION plus
    EXPECT_EQ( {1 2;3 4;5 6}+3 ,{4 5;6 7;8 9} );
    EXPECT_EQ( 3+{1 2;3 4;5 6} ,{4 5;6 7;8 9} );
    EXPECT_EQ( {1 2;3 4;5 6}+[3 -1] ,{[4 0] [5 1];[6 2] [7 3];[8 4] [9 5]} );
    EXPECT_EQ( [3 -1]+{1 2;3 4;5 6} ,{[4 0] [5 1];[6 2] [7 3];[8 4] [9 5]} );
    EXPECT_EQ( {2}+{3}, {5});
    EXPECT_EQ( {}+{}, {} );
    EXPECT_EQ( {1}+[], {[]} );
    EXPECT_EQ( {}+1, {} );

SECTION rdivide
    EXPECT_EQ( {[1 2;3 4],[5 6;7 8]}./[1 3;3 1], {[1 2;3 4]/[1 3;3 1], [5 6;7 8]/[1 3;3 1]} );
    EXPECT_EQ( [1 3;3 1]./{[1 2;3 4],[5 6;7 8]}, {[1 3;3 1]/[1 2;3 4], [1 3;3 1]/[5 6;7 8]} );
    EXPECT_EQ( [1 3;3 1]./{1 2}, {[1 3;3 1]./1, [1 3;3 1]./2} );
    EXPECT_EQ( {2 3}./{4 5}, {2/4 3/5} );

SECTION times
    % EXPECT_EQ( {2}.*{3}, {6});
    % EXPECT_EQ( {[2] [3]}.*{[1] [2]}, {2 3;4 6} );
    EXPECT_EQ( {2 3;1 2}.*2, {4 6;2 4} );
    EXPECT_EQ( 2.*{2 3;1 2}, {4 6;2 4} );
    EXPECT_EQ( [1 2].*{2 3}, {[2 4] [3 6]} );
    EXPECT_EQ( {2 3}.*[1 2], {[2 4] [3 6]} );
    
SECTION times
    EXPECT_EQ( -{2 {3}}, {-2 {-3}});
    EXPECT_EQ( -{2 [3 4]}, {-2 [-3 -4]});

SECTION uplus
    EXPECT_EQ( +{},+{});
    EXPECT_EQ( +{2},+{2});
    EXPECT_EQ( +{2 {3} [4 5]},+{2 {3} [4 5]});    
else;
    TODO_FAIL( 'On Octave 6.4 most of these functions do not work, and thus the test is disabled.' ); end;

%% tocell
EXPECT_EEQ( tocell([]), {[]} );
EXPECT_EEQ( tocell({}), {} );
EXPECT_EEQ( tocell({{}}), {{}} );
EXPECT_EEQ( tocell(@sum), {@sum} );

%% triu
TESTCASE;
EXPECT_EQ( triu({[2 3],[1 2 0];[2], [2;3];2 , 3},-5), {[2 3],[1 2 0];[2] [2;3];[2] [3]});
EXPECT_EQ( triu({[2 3],[1 2 0];[2], [2;3];2 , 3},-2), {[2 3],[1 2 0];[2] [2;3];[2] [3]});
EXPECT_EQ( triu({[2 3],[1 2 0];[2], [2;3];2 , 3},-1), {[2 3],[1 2 0];[2] [2;3];[] [3]});
EXPECT_EQ( triu({[2 3],[1 2 0];[2], [2;3];2 , 3},0), {[2 3],[1 2 0];[] [2;3];[] []});
EXPECT_EQ( triu({[2 3],[1 2 0];[2], [2;3];2 , 3}), {[2 3],[1 2 0];[] [2;3];[] []});
EXPECT_EQ( triu({[2 3],[1 2 0];[2], [2;3];2 , 3},1), {[],[1 2 0];[] [];[] []});
EXPECT_EQ( triu({[2 3],[1 2 0];[2], [2;3];2 , 3},2), {[],[];[] [];[] []});
EXPECT_EQ( triu({[2 3],[1 2 0];[2], [2;3];2 , 3},5), {[],[];[] [];[] []});
EXPECT_EQ( triu({}), {});
EXPECT_EQ( triu({{} {}}), {{} {}});




%%
ENDTESTCASE;

%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*NBRAK>