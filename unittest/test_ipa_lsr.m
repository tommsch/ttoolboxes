% testipa_lsr

%#ok<*CTPCT>
%#ok<*ASGLU>
%#ok<*NASGU>

TTEST init test_ipa_lsr

EXPECT_TOOLBOX( 'matlab', '>=R2016b', APPLIED('onmatlab') );
EXPECT_TOOLBOX( 'octave', '>=6.4', APPLIED('onoctave') );

gurobi_inst = tt.expect_toolbox( 'gurobi' );
if( ~gurobi_inst );
    ASSUME_FAIL( 'Either Gurobi, or the Optimization toolbox must be installed.' ); end;

% preconditions

TESTCASE( 'ipa' );
SECTION( 'general' );
    % A = {[1 1;0 1],[1 1;1 1]};
    % [r, nfo] = ipa( A, 'case','lsr', 'v',-1 );

    A = {[12 18;3 1],[11 5;3 15]};
    delta = 0.99;
    [r, nfo] = ipa( A, 'case','lsr', 'sym',0, 'delta',delta, 'maxnum_restart',5, 'v',-1 );
    EXPECT_ALMOST_EQ( max(r), rho(A{1}*A{2})^(1/2)/delta ); 

    SECTION pascal_rhombus
    A1 = [0 1 0 0 0;1 0 2 0 0;0 0 0 0 0;0 1 0 0 1;0 0 0 2 1];
    A2 = [1 0 2 0 0;0 0 0 2 1;1 1 0 0 0;0 0 0 0 0;0 1 0 0 0];
    A = {A1, A2};
    [~, r] = EXPECT_NTHROW( @() ipa( A, 'lsr' ), 'findsmp:numerical' );

TESTCASE( 'findsmp' );
SECTION( 'general tests' );
    M = {[2 0;0 4], [4 0;0 2]};
    [c, ~, nfo] = findsmp( M, 'min', 'depth',5, 'v',0 );
    EXPECT_ALMOST_EQ( max(nfo.jsrbound), sqrt(8) );
    EXPECT_EQ( c{1}, [1;2] );

TESTCASE( 'polytopenorm' )
SECTION( 'input parsing' );

    VV = [1 0; .5 .5;0 1;.25 .25]';
    p = [0 .5]';
    EXPECT_NEAR( polytopenorm( p, VV, 'l', 'output','ub', 'v',0 ), .5 , 5e-9 );

    VV=[0.89868888396802871 0.2021555344940992 0.54214722284077022 0.075379635650365831 0.046029709400643687 0.33121045545133687;0.33105597956821042 0.88783658768924267 0.1234439570609682 0.33105597956821053 0.88783658768924323 0.12344395706096824;0.27762949906962286 0 0 0 0.24818524389474295 0.57025473661723725;0 0.24818524389474284 0.57025473661723702 0.27762949906962314 0 0;0.075379635650365762 0.046029709400643645 0.3312104554513367 0.89868888396802882 0.20215553449409929 0.54214722284077044];
    pts=[0.88783658768924267;0.046029709400643645;0.75092958750899186;0;0.2021555344940992];
    EXPECT_NEAR( polytopenorm( pts, VV, 'l', 'output',1, 'v',0 ), 0.372879406127539, 1e-10 );


ENDTESTCASE();



%#ok<*NOSEL,*NOSEMI,*ALIGN>
