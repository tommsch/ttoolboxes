% test subdivision

%#ok<*NASGU>
%#ok<*CTPCT>
%#ok<*ASGLU>

% preconditions

TTEST hardinit subdivision
if( isoctave );
    warning( 'off', 'Octave:colon-nonscalar-argument' ); end;

%%
EXPECT_TOOLBOX( 'matlab', '>9.1', APPLIED('onmatlab') );
EXPECT_TOOLBOX( 'octave', '>=6.4', APPLIED('onoctave') );

%% blf
TESTCASE
EXPECT_NTHROW( 'S = getS( ''a'',1/2*[1 2 1]'', ''M'',2 );' );
EXPECT_NTHROW( @() blf( S, 'iteration',4 ), 'Octave:colon-nonscalar-argument' );


%% checktile
TESTCASE
M1 = [1 2; -2 -2];  D1 = [0 1; 0 -1];
M2 = [-1 1; -1 -2]; D2 = [0 0 0; -2 -1 0];
M12 = M2*M1;
D12 = setplus( M2*D1, D2 );
[f1,C1,T1] = checktile( M1, D1, 'verbose',-1, 'legacy',0 );
EXPECT_EQ( size(C1),[6 6]);
EXPECT_EQ( size(T1),[2 6]);



%% compresscoordinates
TESTCASE
cc = round( 3*compresscoordinates([10 20 ; 40 50],[2 1; -1 1]) );
EXPECT_EQ( cc, [0 1 -1 0;0 1 2 3;30 120 60 150]);

%% constructdigit
TESTCASE
EXPECT_EQ( constructdigit(2), [0 1] );


%% constructOmega
TESTCASE
S = getS( '1_1331' );
Om = constructOmega( S,'legacy' );
EXPECT_EQ( sort(Om),[0 1 2]);


%% constructordering
TESTCASE
oo = constructordering( [1 2], [3], [10 20], [30] );  %#ok<NBRAK>
EXPECT_EQ( oo, {[1 2;10 20], [3;30]} );
EXPECT_EQ( constructordering([10 20 30],[1 2 3],[],[1 2]), {[10 20 30;1 2 1],[1 2 3 1 2 3;2 1 2 1 2 1]} );

oo = constructordering( [1 2] );
EXPECT_EQ( oo{1}, [1 2] );
EXPECT_PRED( @isempty, oo{2} );

EXPECT_PRED( @isordering, constructordering(2,2,'random',6) );
EXPECT_PRED( @isordering, constructordering(2,2,5,10,'random',6) );
EXPECT_THROW( @() constructordering( oo, [1 2] ), 'constructordering:arg' );
EXPECT_THROW( @() constructordering( [1 2], [], [1 2 3], [] ), 'constructordering:emptyperiodic' );
EXPECT_THROW( @() constructordering( [1 2], [], [1 2 3], [1] ), 'constructordering:emptyperiodic' );  %#ok<NBRAK>



%constructPhi %untested

%constructU   %untested

%% constructV_constructVt
TESTCASE
EXPECT_PRED( @iscell, constructV( [1 1 1;0 1 1], '01' ) );
EXPECT_PRED( @iscell, constructVt( [1 1 1;0 1 1], '01' ) );


%% dimVVt
TESTCASE
dim = dimVVt( [1 1 0 0;1 0 1 1;0 1 0 0], '01', 'verbose',0 );
EXPECT_EQ( dim, [5 3; 3 0] );

Om = [0 0;2 2; 0 1;1 2;0 2;1 0;2 0]';
dim = dimVVt( Om, 'V', 'verbose',0 );
dimt_ = dimVVt( Om, 'Vt', 'verbose',0 );
EXPECT_EQ( dimt_, [6 3] );
EXPECT_EQ( dim, [6 4 1] );

%% findperiod
TESTCASE
[of] = findperiod( [1 2 1 2 1 2; 4 5 5 5 5 5] );

EXPECT_PRED( @isempty, findperiod( zeros(0, 5 )) );
[oo, pp, nrep] = findperiod( [] );
EXPECT_EQ( oo, [] );
EXPECT_EQ( pp, [] );
EXPECT_EQ( nrep, 0 );
EXPECT_NTHROW( 'findperiod( [1] );' );
EXPECT_NTHROW( 'findperiod( [1 2] );' );
EXPECT_NTHROW( 'findperiod( randi(2, 1, 500) );' );
[oo, pp, nrep] = findperiod([1 2 1 2 1 2; 4 5 5 5 5 5]);
EXPECT_EQ( oo, [1;4]);
EXPECT_EQ( pp, [2 1; 5 5] );
EXPECT_GE( nrep, 1 );
EXPECT_LE( nrep, 3 );
[of] = findperiod( [1 2 1 2 3 2 1 2 1 2], 'fuzzy',1 );
EXPECT_PRED( @isempty, of{1} );
EXPECT_EQ( of{2}, [1 2] );
[of] = findperiod( [1 2 1 2 1 2 1 2 2 2 ],'everywhere','verbose',0);
EXPECT_PRED( @isempty, of{1} );
EXPECT_EQ( of{2}, [1 2] );


%% getS
TESTCASE( 'getS' );
warning( 'off', 'normalizeS:support' );
EXPECT_NTHROW( 'getS( ''a'',1/2*[1 2 1]'', ''M'',2 );' );


%% isordering
TESTCASE
S1 = getS( '1_121' );
EXPECT_EQ( isordering( findperiod([1 2 1 2 1 2; 4 5 5 5 5 5]) ), true );
EXPECT_EQ( isordering( [2 3] ), false);
EXPECT_EQ( isordering( [] ), false);
EXPECT_EQ( isordering( {[1 3 2]} ), false );



%% isS
TESTCASE
S1 = getS('1_121');
EXPECT_EQ( isS({[],[1 2],[]}),false);
EXPECT_PRED( @isS, S1 );
EXPECT_NPRED( @isS, [2 3] );
EXPECT_PRED( @isS, cell( 0, 4 ) );


%% isT
TESTCASE
T = transitionmatrix( getS( '1_1133' ) );
EXPECT_PRED( @isT, T );


%% multiplyS
TESTCASE
S1 = getS('1_143');
S2 = getS('1_1133');
EXPECT_THROW( 'multiplyS();', 'getS:notexpanding' );
EXPECT_NTHROW( 'multiplyS( S1 ); ' );
EXPECT_NTHROW( @() multiplyS( S1, S2 ) );
EXPECT_NTHROW( 'multiplyS( S1, S2, S2 );' );

EXPECT_THROW( 'multiplyS( 2, 3 )', 'multiplyS:input' );
S = getS( '2_not_jointly_expanding' );
EXPECT_THROW( 'multiplyS( S(2,:),S (1,:), S(2,:) )' ,'getS:notexpanding', 'multiplyS:notexpanding' );




%% num2ordering
TESTCASE
S = getS( {[1 1]',2}, 'nocheck' );
Snum = getS( {[],[1 2; -1 2]} );
pt = [0.1875;0.375];
EXPECT_NTHROW( 'ordering2num(num2ordering(Snum,pt,''l'',50),Snum);' );


%% ordering2num
TESTCASE
num = ordering2num( {[2 1 ],[]}, getS({[],2}) );
EXPECT_EQ( num, 1/2 );

%% ordering2vector
TESTCASE
vec = ordering2vector( { [10 20 30], [1 2] }, 10 );
EXPECT_EQ( vec,[10 20 30 1 2 1 2 1 2 1]);

EXPECT_NTHROW( 'ordering2vector( {[], [1]}, 5 );' );
vec = ordering2vector( {[1 2 3 4 5],[]}, 10 );
EXPECT_EQ( vec, [1 2 3 4 5] );
EXPECT_NTHROW( 'ordering2vector( {[], []} );' );

%% peter
TESTCASE
EXPECT_NTHROW( 'peter( rand(2, 30), 4 );' );
EXPECT_NTHROW( 'peter( rand(2, 30), .4 );' );


%% restrictmatrix
TESTCASE
S = getS( '1_121' );
[T,Om,V0] = transitionmatrix( S, 'V',0, 'Omega',[0 1 2 3] );
EXPECT_NTHROW( 'restrictmatrix( T{1}, V0 );' );

S = getS('1_121');
[T,Om,V0] = transitionmatrix( S, 'V',0, 'Omega',[0 1 2 3] );
EXPECT_NTHROW( 'restrictmatrix( T{1}, V0 );' );
EXPECT_THROW( 'restrictmatrix( T, [1 -3 3 1]'' );', 'restrictmatrix:notinvariant', 'restrictmatrix:notest' );

[TA,TT,TR,NULL,BASIS] = restrictmatrix(T,[1 -1 0 0; 1 0 -1 0; 1 -1 0 0]','smallsize',1 );


%testsubdivision %this file

%% tile
TESTCASE
S = getS( '2_butterfly' );
[Q,oo] = tile( S, 'verbose',0, 'peter',1000, 'round',[1e-1 1e-5] );
EXPECT_EQ( size(Q,1), 2);
EXPECT_FALSE( any(diff(oo)) );

EXPECT_NTHROW( @() tile( S, 'verbose',0, 'peter',[.4 .8]) );
EXPECT_NTHROW( @() tile( S, 'verbose',0, 'plot',{'MarkerSize',10}) );
EXPECT_NTHROW( @() tile( S, 'verbose',0, 'digit') );
EXPECT_NTHROW( @() tile( S, 'verbose',0, 'diffdigit', 'iteration',3 ) );
EXPECT_NTHROW( @() tile( S, 'verbose',0, 'supp') );
EXPECT_NTHROW( @() tile( S, 'verbose',0, 'OmegaRR', 'iteration',4 ) );
EXPECT_NTHROW( @() tile( [getS('2_rand'); getS('2_rand')], 'supertile',6, 'iteration',8, 'verbose',0 ), 'normalizeS:support', 'normalizeS:scaling', 'constructdigit:sym' );

%% tilearea
S = getS( '2_butterfly' );
EXPECT_EQ( tilearea( tile(S, 'plot',0, 'verbose',0), 'verbose',0 ), 1);

%% tilearea_slow
TESTCASE
S = getS( '2_butterfly' );
tilearea( S, 'verbose',0 );
tilearea( getS( 'M',3, 'D',[0 1 5] ), 'v',0, 'visual' );
tilearea( tile( S, 'plot',0, 'v',0 ), 'v',0, 'visual' );


%% transitionmatrix
TESTCASE
S1 = getS( '1_121' );
S2 = getS( '1_1133' );
S = [S1;S2];
EXPECT_NTHROW( '[T,Om]=transitionmatrix(S);' );

transitionmatrix( S, 'colsum',0 );
transitionmatrix( S, 'colsum',1 );
EXPECT_THROW( '[~, Om] = transitionmatrix( S, ''colsum'',2 ); ', 'transitionmatrix:colsum' );
EXPECT_EQ( Om, [0 1 2]);
EXPECT_THROW( @() transitionmatrix( S, 'Omega',[0;0] ), 'transitionmatrix:dimOm' );
EXPECT_THROW( @() transitionmatrix( S, 'Omega',[50], 'colsum',1 ), 'transitionmatrix:colsum' );  %#ok<NBRAK>

T = transitionmatrix( S, 'noflat' );
EXPECT_PRED( @iscell, T{1} );

[T, Om, V0] = transitionmatrix( S, 'V',0 );
n = null( V0' );
n = n/n(1);
EXPECT_ALMOST_EQ( n, [1;1;1] );

[T, Om, V1] = transitionmatrix( S, 'V',3 );
EXPECT_EQ(Om,[0 1 2 3 4]);
EXPECT_NTHROW( 'transitionmatrix( S,''noflat'',''colsum'',1 );' );
EXPECT_NTHROW( 'transitionmatrix( S,''onlyindex'' );' );
EXPECT_NTHROW( 'transitionmatrix( S );' );


%% vdisp
TESTCASE
S1 = getS('1_121');
ret = EXPECT_NTHROW( 'str = vdisp( S1 );' );
if( ret );
    EXPECT_THAT( str, tt.HasSubstr('1_spline_binary_1') );
end;

%% vector2ordering
TESTCASE
[o1, o2] = vector2ordering( [1 2 1 2 1 2; 4 5 5 5 5 5] );
EXPECT_EQ( o1, [1;4] );
EXPECT_EQ( o2, [2 1; 5 5] );

%%
ENDTESTCASE();

%#ok<*NOSEL,*NOSEMI,*ALIGN>
