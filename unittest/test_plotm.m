% test plotm

%#ok<*CTPCT>    
%#ok<*ASGLU>
%#ok<*NASGU>

TTEST init test_plotm

% preconditions

ASSUME_FAIL( 'This test is disabled, since EXPECT_IMG_EQ does not work well yet.' );

%% implicit_inverse
ii = allfunctionhandle( 'plotm', 'private', 'implicit_inverse', 'verbose',0 );
EXPECT_NEAR( ii( @(x) -x, -1 ), 1 );
EXPECT_NEAR( ii( @(x) log(x), exp(1) ), 1 );

%% plotm
[w_msg, w_id] = lastwarn();
try;  %#ok<TRYNC>
    evalc( 'plot( [2] );' );  % trigger possible warning: Warning: MATLAB has disabled some advanced graphics rendering features by switching to software OpenGL....
    close all; end;
lastwarn( w_msg, w_id );

TESTCASE( 'meta' )
    EXPECT_NTHROW( @() plotm( 'clf',-1 ) );
    EXPECT_NTHROW( @() plotm( 'proj',[1 0 0 0;0 1 0 0], 'funct','r', 'hold','on' ), 'plotm:function' );

TESTCASE( '1d' )
    rng(1); clf; plotm( 'visible','off' );
    EXPECT_IMG_EQ( @() plotm( randn(1, 40) ), 'img/%v_test_1d_100_%o.png' );
    EXPECT_THROW( @() plotm( randn(1, 40), 'wrongoption' ), 'plotm:option' );
    EXPECT_IMG_EQ( @() plotm( randn(2, 40), '.-', 'box',2 ), 'img/%v_test_1d_200_%o.png' );
    EXPECT_THROW( @() plotm( num2cell(reshape(cumsum(rand(20, 1)), 2, [])', 2), 'wrongoption',1 ), 'plotm:option' );
    EXPECT_IMG_EQ( @() plotm( num2cell(reshape(rand(4, 1), 2, [])', 2), 'verbose',1 ), 'img/%v_test_1d_300_%o.png' );
    EXPECT_IMG_EQ( @() plotm( num2cell(reshape(cumsum(rand(20, 1)), 2, [])', 2), 'verbose',1 ), 'img/%v_test_1d_400_%o.png' );
    close all;

TESTCASE( '2d' )
    rng(1); clf; plotm( 'visible','off' );
    val = randn( 2, 20 ); 
    EXPECT_IMG_EQ( @() plotm(randn(1, 10)+1i*randn(1, 10),'.'), 'img/%v_test_2d_030_%o.png' );
    EXPECT_IMG_EQ( @() plotm(val, '.'), 'img/%v_test_2d_050_%o.png' );
    EXPECT_IMG_EQ( @() plotm(val, 'boundary',.5, 'Color','red'), 'img/%v_test_2d_100_%o.png' );
    EXPECT_IMG_EQ( @() plotm(val, 'boundary',-.5, 'Color','blue'), 'img/%v_test_2d_200_%o.png' );
    EXPECT_IMG_EQ( @() plotm(val, 'boundary',-1.5, 'Color','green'), 'img/%v_test_2d_300_%o.png' );
    EXPECT_IMG_EQ( @() plotm(val, 'hull',1, 'Color','black'), 'img/%v_test_2d_400_%o.png' );

TESTCASE( '3d' )
    rng(1); clf; plotm( 'visible','off' );
    val = randn( 3, 20 );
    EXPECT_IMG_EQ( @() plotm( randn(3, 40), 'resolution',0, 'MarkerSize',100 ), 'img/%v_test_3d_100_%o.png' );
    title('plotm: A pointcloud and a box.');
    EXPECT_IMG_EQ( 'img/%v_test_3d_200_%o.png', gcf );
    EXPECT_IMG_EQ( @() plotm('box', 3), 'img/%v_test_3d_300_%o.png' );
    EXPECT_IMG_EQ( @() plotm(randn(3,40),'resolution',100, 'surface',1, 'contour',1 ), 'img/%v_test_3d_400_%o.png' );
    EXPECT_IMG_EQ( @() plotm([0 0 0;1 0 0;1 1 0;0 1 0; 0 0 0;0 0 1;1 0 1;1 1 1;0 1 1; 0 0 1;0 0 0; 1 0 0;1 0 1;0 0 1; 0 0 0;0 1 0;1 1 0;1 1 1;0 1 1; 0 1 0;0 0 0;0 1 0;0 1 1;0 0 1;0  0 0;1 0 0;1 1 0;1 1 1;1 0 1;1  0 0;0 0 0]','.-'), 'img/%v_test_3d_500_%o.png' );
    EXPECT_IMG_EQ( @() plotm(val, 'boundary',.5), 'img/%v_test_3d_600_%o.png' );
    EXPECT_IMG_EQ( @() plotm(val, 'hull',1 ), 'img/%v_test_3d_700_%o.png' );

TESTCASE( 'funct' );
    K = [5 5 5 5 5 6 6 6 6 6;9 9 9 9 9 8 8 8 8 8];
    EXPECT_IMG_EQ( @() plotm( K, 'arrow',1, 'hold','on' ), 'img/%v_test_funct_arrow_1000_%o.png' );
    
    EXPECT_IMG_EQ( @() plotm( [100;7], 'r.', 'funct','', 'linewidth',2), 'img/%v_test_funct_emptyfunc_1000_%o.png' );
    

TESTCASE( '400d' )
    rng(1);
    val = randn( 400, 2 );
    EXPECT_THROW( 'plotm(val);', 'plotm:dim' );

TESTCASE( 'darkmode' );
    rng(1);
    val = randn( 3, 2 );
    plotm( val );
    plotm( 'darkmode' );
    EXPECT_FALSE( tt.expect_img_eq( @() plotm( val, 'darkmode' ), @() plotm( val ) ) );

TESTCASE( 'options' );
    rng(1);
    EXPECT_IMG_EQ( @() plotm( randn(3, 3), 'arrow',1 ), 'img/%v_test_options_1000_%o.png' );

TESTCASE( 'regression' );
    rng(1);
    EXPECT_IMG_EQ( @() plotm( randn(2, 20), 'c.-' ), 'img/%v_test_regression_cyan_%o.png' );
    EXPECT_FALSE( tt.expect_img_eq( @() plotm( [1;3], 'arrow',1, 'k-', 'linewidth',1 ), @() plotm( [1;3], 'arrow',1, 'k-', 'linewidth',20 ) ) );
    EXPECT_FALSE( tt.expect_img_eq( @() plotm( [1;2;3], 'r', 'arrow',1, 'linewidth',2 ), @() plotm( [1;2;5], 'r', 'arrow',1, 'linewidth',2 ) ) );
    EXPECT_IMG_EQ( @() plotm( [7 13;1 6;3 5], 'g', 'funct','k', 'arrow',1 ), 'img/%v_test_regression_3000_%o.png' );

    Ks = [-0.50393319428625738 -0.92911320040665368 0.85787102926516512;0.86374263278852303 0.36979543105628793 -0.5138650573326875]';
    EXPECT_NTHROW( @() plotm( Ks, 'r', 'funct','k*', 'hull',1 ), 'kone:gurobi', 'MATLAB:UndefinedFunction'  );
    
TESTCASE( 'implicit_inverse' );
    ii = allfunctionhandle( 'plotm', 'private', 'implicit_inverse', 'v',0 );
    EXPECT_NEAR( ii( @(x) -x, -1 ), 1 );
    EXPECT_NEAR( ii( @(x) log(x), exp(1) ), 1 );

%#ok<*NOSEL,*NOSEMI,*ALIGN>

