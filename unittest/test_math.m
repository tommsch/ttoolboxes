% test math

%#ok<*NASGU>
%#ok<*CTPCT>
%#ok<*ASGLU>

TTEST hardinit math

% preconditions


%% quot
TESTCASE
EXPECT_EQ( quot([1 2 4 4 2]), [2 2 1 .5] );
EXPECT_EQ( quot([2 4 6;1 8 12; 2 4 6], 2, 2), [0.75; 0.1875; 0.75] );
EXPECT_EMPTY( quot([1]) );  %#ok<NBRAK2>

%% liminf
TESTCASE
EXPECT_EQ( liminf([10 1 9 2 8 3 7 4 6]), [1 1 2 2 3 3 4 4 6] );

%% limsup
TESTCASE
EXPECT_EQ( limsup([10 1 9 2 8 3 7 4 6]), [10 9 9 8 8 7 7 6 6] );

%% mpowerinf
TESTCASE
    SECTION( 'gerneral tests' )
        A = mpowerinf( [0 1;1 0] );
        EXPECT_ALMOST_EQ( A{1},[0 1;1 0] );
        EXPECT_ALMOST_EQ( A{2},[1 0;0 1] );
        EXPECT_THROW( @() mpowerinf( ([1 0 0;1 0 0;1 1 1]), true ), 'mpowerinf:singular', 'o','MATLAB:nearlySingularMatrix' );

    SECTION( 'edgecases' )
        val = mpowerinf( [0 1;-1 0], [], 0 );
        EXPECT_EQ( numel(val), 4 );

  SECTION( 'regressiontests' )
        EXPECT_NTHROW( @() mpowerinf( [0 0 0;0 0 1;0 0 1] ) );  % this is a very easy example and should not throw a warning (it currently does)

        A = mpowerinf( [-8 -14  3  -1;0  22  0   3;-2   9 -1   6;7  10 -7 -10], true );
        EXPECT_TRUTHY( isfinite(A{1}) );
        
        EXPECT_NUMEL( mpowerinf( [0 1 0;-1 0 0;0 0 -1] ), 4 );

        C = [
          1.047189367516205 - 0.000000000000000i  0.045041607238849 + 0.000000000000000i -0.196738548829262 + 0.000000000000000i
         -0.192128944732523 - 0.000000000000000i  0.982213466583598 + 0.000000000000000i  0.591454706495801 + 0.000000000000000i
          0.229380100539453 + 0.000000000000000i -0.158201739572542 + 0.000000000000000i  0.520938404408025 - 0.000000000000000i ];
        mpowerinf( C, true );

        %Pit_inf = mpowerinf( Pi, true );
        Pi = [0 -2 0;2 -1 0;1 -2 -1];
        EXPECT_THROW( @() mpowerinf( Pi, true ), 'matrixpowerinf:resolution' );
        

%%
%#ok<*NOSEL,*NOSEMI,*ALIGN>

