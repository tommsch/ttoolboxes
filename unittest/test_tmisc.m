% test tmisc

%#ok<*NASGU>
%#ok<*CTPCT>
%#ok<*ASGLU>

TTEST hardinit tmisc

% preconditions

%% allcomb
TESTCASE
% third party function and thus not tested

%% anycast
TESTCASE
EXPECT_THROW( @() anycast( 0, 'badtype_jaksldausd219u' ), 'anycast:badcast' );
[~, ~, err] = EXPECT_NTHROW( @() anycast( 0, 'badtype_jaksldausd219u' ) );
EXPECT_TRUE( err );

EXPECT_EQ( anycast( 1.41258233152, 'int64', 'int32'), int64(1) );

if( tt.expect_toolbox( 'intlab' ) );
    EXPECT_EQ( anycast( .1, 'intvalrat' ), infsup(0.099999999999999992,0.100000000000000006) );
    EXPECT_THROW( @() anycast( .1291025819205, 'intvalrat' ), 'anycast:badcast' );
end;

%% buildproduct
TESTCASE
SECTION( 'general tests' );
    mat = {[0.8 0.2; 0.3 0.7],[0.4 0.6;0.5 0.5]};
    EXPECT_NO_THROW( @() buildproduct(mat, {[],[1 2]}) );
    
    sol = 1/21*[11 10;11 10];
    EXPECT_ALMOST_EQ( buildproduct(mat,{[],[1 2]}), sol );
    EXPECT_ALMOST_EQ( buildproduct(mat,{[],[2 1]},'reverse'), sol );
    EXPECT_NO_THROW( @() buildproduct( mat, {[],[1 2]}, 'l',1 ) );
    [m,ms,s] = buildproduct( mat, {[],[1 2]}, 'scale',1 );
    EXPECT_ALMOST_EQ( m, ms*s );
    
    sol = 1/20*[10 10;11 9];
    EXPECT_ALMOST_EQ( buildproduct(mat, [1 2]), sol );
    EXPECT_ALMOST_EQ( buildproduct(mat, {[1 2],[]}), sol );
    [m,ms,s] = buildproduct( mat, {[1 2],[]}, 'scale',1 );
    EXPECT_ALMOST_EQ( m, ms*s );
    
    EXPECT_THROW( @() buildproduct({{[10],[20]}},[1 2]), 'buildproduct:input', '''oo''' );
    EXPECT_EQ( buildproduct({{[10],[20]}},[1 1;1 2]), 200 );

SECTION( 'regression tests' );
    M={[0 -1;-1 0],[-1 1;-1 0]};
    EXPECT_EQ( buildproduct( M, [1 2]' ), [-1 1;0 1] );
    EXPECT_EQ( buildproduct_fast( M, [1 2]' ), [-1 1;0 1] );

    M = {[1 2;1 2], [2 4;2 4]};
    oo = [1;2;2];
    EXPECT_EQ( buildproduct(M, oo ), [36 72;36 72] );
    EXPECT_EQ( buildproduct_fast(M, oo ), [36 72;36 72] );

%% celldiff
TESTCASE
EXPECT_EQ( {3}, celldiff( {1 2 3}, {1 2} ) );
EXPECT_EQ( {1 2 3}, celldiff( {1 2 3}, {} ) );
EXPECT_EMPTY( celldiff( {}, {1 2} ) );
EXPECT_EMPTY( celldiff( {}, {} ) );


%% chinese
TESTCASE
EXPECT_EQ( chinese([6 0 -1],[7 5 3]), 20 );
EXPECT_EQ( chinese([2 0],[4 2]), 2 );
EXPECT_EQ( chinese([1 2 3 4 5 0],[2 3 4 5 6 7]), 119 );
EXPECT_EQ( chinese([3],[4]), 3 );
EXPECT_EQ( chinese([0],[4]), 0 );
EXPECT_EQ( chinese([2 6 10],[4 4 4]), 2 );
EXPECT_PRED( @isnan, chinese([1 2],[2 2]) );
EXPECT_PRED( @isnan, chinese([1 2],[2 4]) );
EXPECT_THROW( @() chinese([0],[1 0]), 'chinese:input' );

%% clamp
TESTCASE
EXPECT_EQ( clamp(4, 3, 5), 4 );
EXPECT_EQ( clamp(2, 3, 5), 3 );
EXPECT_EQ( clamp(6, 3, 5), 5 );

%% condeigvec
TESTCASE
SECTION
EXPECT_GE( condeigvec( [0 1 0;0 1 -1;1 -1 0] ), 1e7 );
EXPECT_LE( condeigvec([1 0;0 1]), 1+eps );

%% expect
TESTCASE
EXPECT_THROW( 'expect( false,''exp:test'',''loremipsum'')', 'exp:test' );
EXPECT_NO_THROW( 'expect( true,''exp:test'',''loremipsum'')' );

%% flatten
TESTCASE
EXPECT_EQ( flatten({{2 3 {4; 5} 6} {{{7}}}}), {2 3 4 5 6 7} );
EXPECT_EQ( {2}, flatten( {2} ) );
EXPECT_PRED( @iscell, flatten([2 3]) );

%% grCenter
TESTCASE
G = [1 2;2 5; 5 6; 2 3; 3 4; 4 7; 4 8; 8 9];
EXPECT_EQ( grCenter(G), 3 );
val = grCenter( G, 'edge' );
EXPECT_TRUE( isequal(val,[2;3]) || isequal(val,[3;2]) || isequal(val,[3;4]) || isequal(val,[4;3]) );

G = [1 2; 2 3; 4 5; 2 4];
EXPECT_EQ( sort(grCenter(G)), [2 4] );

G = [1 2;2 3;3 4;4 5;3 6;6 7;6 8;3 9;1 10;10 11;11 12;10 13];
EXPECT_EQ( grCenter(G,'edge'), [1;2] );
EXPECT_EQ( grCenter(G), [1 2] );



%% grVerCover
TESTCASE
if( ismatlab );
    G = [1 2; 1 3; 1 4; 2 5; 1 6];
    G = digraph( G(:,1), G(:,2) );
    val = grVerCover( G );
    EXPECT_NUMEL( val, 3 );
    EXPECT_TRUE( searchincellarray([1 2],val,1) );
    EXPECT_TRUE( searchincellarray([1 5],val,1) );
    EXPECT_TRUE( searchincellarray([2 3 4 6],val,1) );

    G = [1 1; 1 3; 1 2; 2 1];
    G = digraph( G(:,1), G(:,2) );
    val = grVerCover( G );
    EXPECT_EQ( val, {1} );

    G = digraph([1 4],[4 1]);
    val = grVerCover( G );
    EXPECT_EQ( val, {[1],[4]} );
else;
    EXPECT_THROW( 'graph;', 'Octave:undefined-function' ); end;

%% intersectspace
TESTCASE
EXPECT_ALMOST_EQ( intersectspace([1 -1 0 0; 1 0 -2 1; 0 1 -2 -1].',[1 1 0 0; 1 0 -1 0].'), [1 -1/3 -4/3 0].' );
EXPECT_ALMOST_EQ( intersectspace([1 0].',[0 1].',[1 0;0 1].'), [0 0].' );

[in,dim] = intersectspace( [1 1 1; 1 2 1; 1 2 2]', [1 1 1; 1 2 1]', [1 1 1]' );
EXPECT_EQ( in, [1;1;1] );
EXPECT_EQ( dim, 1 );

[in,dim] = intersectspace( [1 -1 0 0; 1 0 -2 1; 0 1 -2 -1]', [1 1 0 0; 1 0 -1 0]' );
EXPECT_EQ( rank(in), 1 );
EXPECT_EQ( dim, 1 );

[in,dim] = intersectspace( [1 0 0; 0 1 0;1 1 0]' );
EXPECT_EQ( dim, 2 );

[in,dim] = intersectspace( [1 0 0]', [0 1 0;1 -1 0]', [0 1 1;0 1 -1]', [1 1 1;1 1 -1; 1 -1 1]' );
EXPECT_EQ( dim, 0 );

EXPECT_THROW( 'intersectspace()', 'intersectspace:InvalidInput' );
EXPECT_THROW( 'intersectspace([2 3 3;1 1 1]'',[1 2]'');', 'intersectspace:InvalidInput' );


%% invblkdiag
TESTCASE
EXPECT_EQ( invblkdiag([1 0 0; 0 0 1;0 0 1]), {[1],[0 1;0 1]} );
EXPECT_EQ( invblkdiag([0 1;0 0]), {[0 1;0 0]} );
EXPECT_EQ( invblkdiag([0 0;0 0]), {[0],[0]} );
EXPECT_EQ( invblkdiag([]), {} );

EXPECT_EQ( invblkdiag( [0 1;0 0] ), {[0 1;0 0]} );
EXPECT_EQ( invblkdiag( [0 1;0 0] ), {[0 1;0 0]} );
EXPECT_EQ( invblkdiag( [0 0 0;0 1 0;0 0 0] ), {[0],[1],[0]} );

%% isaltsign
TESTCASE
EXPECT_TRUE( isaltsign([1 0 0;0 1 0;0 0 1]) );
EXPECT_TRUE( isaltsign([1 0 0;0 0 1;0 1 0]) );
EXPECT_TRUE( isaltsign([0 1 0;1 0 0;0 0 1]) );
EXPECT_TRUE( isaltsign([0 1 0;1 -1 1;0 1 0]) );
EXPECT_TRUE( isaltsign([0 0 1 0;1 0 0 0;0 1 -1 1;0 0 1 0]) );
EXPECT_FALSE( isaltsign([1 -1;-1 1]) );


%% iscirculant
TESTCASE
EXPECT_TRUE( iscirculant( [] ) );
EXPECT_TRUE( iscirculant( [1] ) );
EXPECT_FALSE( iscirculant( [1 1 1 1] ) );
EXPECT_FALSE( iscirculant( [1 1 1 1]' ) );
EXPECT_TRUE( iscirculant( circulant(rand(1, 10)) ) );
EXPECT_FALSE( iscirculant( [1 1 1;0 1 1;1 1 1] ) );
for i = 1:100;
    vec = randi( [1 9], 1, randi([2 20]) );
    M = circulant( vec );
    idx = randi( numel(M) );
    if( idx == 1 || ...  % check whether we would alter the elements in the corners
        idx == numel( M ) ...
      );
        continue; end;
    M(idx) = 0;
    EXPECT_FALSE( iscirculant( M ) ); end;

%% iseigenvector
A = randi( 99, 4 ) - 50;
[v, d, w] = eig( A );
EXPECT_TRUE( iseigenvector( A, v(:,1) ) );
EXPECT_TRUE( iseigenvector( A, v(:,2) ) );
EXPECT_FALSE( iseigenvector( A, randn(4,1) ) );

A = [0 0;0 0];
EXPECT_TRUE( iseigenvector( A, [2;3] ) );

A = [1 1;0 1];
EXPECT_TRUE( iseigenvector( A, [1;0] ) );

A = randn( 3 ) + 1i*randn( 3 );
[v,d,w] = eig( A );
EXPECT_TRUE( iseigenvector( A, v(:,1) ) );
EXPECT_TRUE( iseigenvector( w(:,1)', A ) );
EXPECT_FALSE( iseigenvector( w(:,1).', A ) );

%% ishankel
TESTCASE
EXPECT_TRUE( ishankel( [] ) );
EXPECT_TRUE( ishankel( zeros(0,3) ) );
EXPECT_TRUE( ishankel( [1] ) );
EXPECT_TRUE( ishankel( [1 1 1 1] ) );
EXPECT_TRUE( ishankel( [1 1 1 1]' ) );
EXPECT_TRUE( ishankel( hankel(rand(1, 10)) ) );
EXPECT_TRUE( ishankel( hankel([1 2 3], [3 5 7 9 11 13 15 17 19 21 23]) ) );
EXPECT_TRUE( ishankel( hankel([1 5 9 13 17 21 25 29], [29 31 33]) ) );
EXPECT_FALSE( ishankel( [1 1 1;0 1 1;1 1 1] ) );
for i = 1:100;
    vec1 = randi( [1 9], 1, randi([2 20]) );
    vec2 = randi( [1 9], 1, randi([2 20]) );
    vec2(1) = vec1(end);
    M = hankel( vec1, vec2 );
    idx = randi( numel(M) );
    if( idx == 1 || ...  % check whether we would alter the elements in the corners
        idx == numel( M ) ...
      );
        continue; end;
    M(idx) = 0;
    EXPECT_FALSE( ishankel( M ) ); end;

%% ishouseholder
TESTCASE
EXPECT_FALSE( ishouseholder( [] ) );
EXPECT_TRUE( ishouseholder( [-1] ) );
EXPECT_FALSE( ishouseholder( [1] ) );
EXPECT_FALSE( ishouseholder( [0] ) );
EXPECT_FALSE( ishouseholder( [1 1 1 1] ) );
EXPECT_FALSE( ishouseholder( [1 1 1 1]' ) );

v = randn( 2, 1 );
EXPECT_FALSE( ishouseholder( eye(2) - v*v' ) );
EXPECT_TRUE( ishouseholder( eye(2) - 2/(v'*v)*(v*v') ) );

%% issquare
TESTCASE
EXPECT_PRED( @issquare, rand(3, 3, 3) );
EXPECT_NPRED( @issquare, rand(3, 4)  );

%% issize0
EXPECT_TRUE( issize0([]) );
EXPECT_TRUE( issize0('') );
EXPECT_FALSE( issize0([0]) );
EXPECT_FALSE( issize0(zeros(0, 1, 0)) );

%% istoeplitz
TESTCASE
EXPECT_TRUE( istoeplitz( [] ) );
EXPECT_TRUE( istoeplitz( zeros(0, 3) ) );
EXPECT_TRUE( istoeplitz( [1] ) );
EXPECT_TRUE( istoeplitz( [1 1 1 1] ) );
EXPECT_TRUE( istoeplitz( [1 1 1 1]' ) );
EXPECT_TRUE( istoeplitz( toeplitz(rand(1, 10)) ) );
EXPECT_TRUE( istoeplitz( toeplitz([1 2 3], [1 5 9 13 17 21 25 29]) ) );
EXPECT_TRUE( istoeplitz( toeplitz([1 5 9 13 17 21 25 29], [1 2 3]) ) );
EXPECT_FALSE( istoeplitz( [0 1 1;1 1 1;1 1 1] ) );
for i = 1:100;
    vec1 = randi( [1 9], 1, randi([2 20]) );
    vec2 = randi( [1 9], 1, randi([2 20]) );
    vec2(1) = vec1(1);
    M = toeplitz( vec1, vec2 );
    idx = randi( numel(M) );
    if( idx == size( M, 1 ) || ...  % check whether we would alter the elements in the corners
        idx == numel( M ) - size( M, 1 ) + 1 ...
      );
        continue; end;
    M(idx) = 0;
    EXPECT_FALSE( istoeplitz( M ) ); end;

%% iswholenumber
TESTCASE
EXPECT_EQ( iswholenumber([2 2.1 inf 0]), [true false false true] );
EXPECT_EQ( iswholenumber(@sum), false );

%% lexicographic
TESTCASE
EXPECT_EQ( lexicographic( [0 1 0; 0 1 2; 3 1 1] ,2), [1 0 0;1 2 0;1 1 3] );
EXPECT_NO_THROW( 'lexicographic( [0 1 0; 0 1 2; 3 1 1]);' );
EXPECT_NO_THROW( 'lexicographic( [0 1 0; 0 1 2; 3 1 1],''inf'');' );
EXPECT_THROW( 'lexicographic( [0 1 0; 0 1 2; 3 1 1] ,@(x) x^2)', 'lexicographic:opt' );

%% makepositive
TESTCASE
EXPECT_EQ( makepositive([ 0 -1 2]), [0 1 -2] );
EXPECT_ALMOST_EQ( makepositive(1i), 1 );

%% matrixinfo
TESTCASE
SECTION( 'general' );
    EXPECT_NO_THROW( @() matrixinfo( [], 'all' ) );
    EXPECT_NO_THROW( @() matrixinfo( {}, 'all' ) );
    EXPECT_NO_THROW( @() matrixinfo( {[1 2 3; 0 2 1; 0 0 1],[2]}, 'all' ) );
    EXPECT_EQ( matrixinfo(1i, 'nan'), false );

    EXPECT_NO_THROW( @() matrixinfo( ones(0, 3), 'all' ) );
    EXPECT_NO_THROW( @() matrixinfo( ones(3, 0), 'all' ) );
    EXPECT_NO_THROW( @() matrixinfo( ones(1, 3), 'all' ) );
    EXPECT_NO_THROW( @() matrixinfo( ones(3, 1), 'all' ) );
    EXPECT_NO_THROW( @() matrixinfo( ones(2, 3), 'all' ) );
    EXPECT_NO_THROW( @() matrixinfo( ones(3, 2), 'all' ) );
    
    EXPECT_NO_THROW( @() matrixinfo( [nan], 'all' ) );
    EXPECT_NO_THROW( @() matrixinfo( [inf], 'all' ) );
    EXPECT_NO_THROW( @() matrixinfo( [nan 0], 'all' ) );
    EXPECT_NO_THROW( @() matrixinfo( [inf 0], 'all' ) );
    
    EXPECT_TRUE( matrixinfo(1,'pm1') );
    EXPECT_TRUE( matrixinfo([1 1/2;0 1/2], 'stochastic_column') );
    EXPECT_EQ( matrixinfo({[-1 -1;0 -2],[-1 0;0 -1],[-2 -2;-2 -2]}, 'commute'), [1 1 0;1 1 1;0 1 1] );
    EXPECT_EQ( matrixinfo({[-1 -1;0 -2],[-1 0;0 -1],[-2 -2;-2 -2]}, 'commuteall'), 0 );
    EXPECT_THROW( @() matrixinfo([1 2;3 4], 'wrongname'), 'MATLAB:nonExistentField', 'Octave:invalid-indexing', 'matrixinfo:unkown_what' );
    
    val = matrixinfo( {1 2;0 2} );
    EXPECT_TRUTHY( structfun( @(x) isequal(size(x),[2 2]) || isequal(size(x),[4 4]) || isequal(size(x),[1 1]), val ) );

SECTION( 'regression' );
    At=[-0.15892544259255201+0.38735466570321608i -0.53283764083845342-0.688146172329545i;-0.015207067036715698+0.46343509952122225i 0.68183292864432732-0.014745111314095448i];
    nfo = matrixinfo( At );
    EXPECT_EQ( nfo.commuteall, 1 );
    EXPECT_EQ( matrixinfo(At, 'commuteall'), 1 );

    nfo = matrixinfo( {2 3}, 'compact' );
    EXPECT_EQ( nfo.commuteall, 1 );

    nfo = matrixinfo( {[0 1]', [2 1]'}, 'conjugate' );
    EXPECT_EQ( nfo, eye(2) );

%YY more tests for matrixinfo

%% mergeinterval
TESTCASE
[lower_, upper_] = mergeinterval( [0 1 2 3 4], [1.5 1.6 3.5 3 5] );
EXPECT_EQ( lower_, [0 2 4] );
EXPECT_EQ( upper_, [1.6 3.5 5] );
EXPECT_NO_THROW( 'mergeinterval({[0 1.5];[1 1.6];[2 3.5];[3 3];[4 5]});' );
EXPECT_NO_THROW( 'mergeinterval([0 1.5; 1 1.6; 2 3.5; 3 3; 4 5]);' );
EXPECT_NO_THROW( 'mergeinterval([0 1.5; 1 1.6; 2 3.5; 3 3; 4 5],''output'',''C'');' );
EXPECT_NO_THROW( 'mergeinterval({[]});' );
EXPECT_NO_THROW( 'mergeinterval([]);' );
EXPECT_NO_THROW( 'mergeinterval([]);' );
[~,~] = mergeinterval();
EXPECT_EQ( mergeinterval([0 2; 1 -1],'output','M'), [0 2] );
EXPECT_EQ( mergeinterval([0 2; -1 1],'output','M','switch'), [-1 2] );
EXPECT_EQ( mergeinterval([-inf inf],'output','M','switch'), [-inf inf] );
EXPECT_EQ( mergeinterval([],'output','M','switch'), zeros(0,2) );

EXPECT_THROW( 'mergeinterval([0 1 2 3 4],[1.5 1.6 3.5 3 5]);', 'mergeinterval:nargout' );
EXPECT_THROW( 'mergeinterval([0 1 2 3],[1.5 1.6 3.5 3 5]);', 'mergeinterval:format' );
EXPECT_THROW( 'mergeinterval([0 1 2 3],[1.5 1.6 3.5 3 5]);', 'mergeinterval:format' );
EXPECT_THROW( 'mergeinterval([0 1 2 3 3]);', 'mergeinterval:format' );
EXPECT_THROW( 'mergeinterval([0 1 2; 3 3 3]);', 'mergeinterval:format' );
EXPECT_THROW( 'mergeinterval([0 1; 3 3],''o'',''Wrongstring'');', 'mergeinterval:format' );
EXPECT_THROW( 'mergeinterval([1 2 3;4 5 6;7 8 9]);', 'mergeinterval:format' );


%% mixvector
TESTCASE
EXPECT_EQ( sortrows(mixvector(0:1, 2)')', sortrows([0 0 1 1;0 1 0 1]')' );
EXPECT_EQ( mixvector([-1 1], 3,0), 8 );
EXPECT_EQ( mixvector([-1 1], 3,1), [-1;-1;-1] );
[~,val] = mixvector(0:1, 2);
EXPECT_EQ( val, 4 );

EXPECT_EQ( mixvector([10 20 30 40],1), [10 20 30 40] );
EXPECT_EQ( mixvector([10 20 30 40],1,1), 10 );
EXPECT_EQ( mixvector([10 20 30 40],1,0), 4 );

EXPECT_THROW( 'mixvector([10 20 30 40],1,5);', 'mixvector:index' );
EXPECT_EQ( mixvector([1 2 3],2,5), [2;2] );

%% mtry
TESTCASE
EXPECT_NTHROW( @() mtry() );
EXPECT_EQ( mtry( {@() asashaswehashasdhdf, @() asasfasfgasghf, @()1, @() jksdglajsdgkawjkasdh} ), 3 );
EXPECT_STREQ( @() mtry( @() fprintf('X') ), 'X' );
EXPECT_STREQ( @() mtry( {@() fprintf('X'), @() fprintf('Y') } ), 'X' );
EXPECT_STREQ( @() mtry( {{@() fprintf('X'), @() fprintf('Y')}} ), 'XY' );
EXPECT_THROW( @() mtry( {@() error('id:id','msg')} ), 'id:id' );


%% nestedcellfun
TESTCASE
EXPECT_EQ( nestedcellfun(@sum,{[1 2],[2 3 4]}), [3 9] );
EXPECT_EQ( nestedcellfun(@ndimsm, {{[1 2 1]},[1 ;2 ]},'UniformOutput',false), {{2},1} );
EXPECT_EQ( nestedcellfun(@plus,{{1 2},3;4 {5 6}} , {{7 8},9;10 {11 12}},'UniformOutput',true), [8 10 14 12 16 18] );
EXPECT_EQ( nestedcellfun(@plus,{{1 2},3;4 {5 6}} , {{7 8},9;10 {11 12}},'UniformOutput',false), {{8 10} 12; 14 {16 18}} );
EXPECT_EQ( nestedcellfun(@plus,[1 2 3;4 5 6] , [7 8 9;10 11 12],'UniformOutput',true), [8 10 12; 14 16 18] );
EXPECT_EQ( nestedcellfun(@plus,[1 2 3;4 5 6] , [7 8 9;10 11 12],'UniformOutput',false), [8 10 12; 14 16 18] );
EXPECT_NO_THROW( 'nestedcellfun(@eps,''UniformOutput'',false);' );


%% nondiag
TESTCASE
val = nondiag({[1 2] [2 3] ;[1] [40] },1);
EXPECT_EQ( val, {[1 2] []; 1 40} );
nondiag([1 2 3; 3 2 3; 3 4 5]);
EXPECT_NO_THROW( 'nondiag([1 2 3; 3 2 3; 3 4 5],-inf);' );
EXPECT_NO_THROW( 'nondiag([1 2 3; 3 2 3; 3 4 5],inf);' );


%% normalizematrix
TESTCASE
MM = [1 2 3; 0 -1 2];
val = normalizematrix( MM,'colsum' );
EXPECT_ALMOST_EQ( sum(val,1), 1 );


%% num2color
TESTCASE
EXPECT_NO_THROW( 'num2color(0); ');
EXPECT_EQ( num2color(4), num2color(4) );

%% rational
TESTCASE
EXPECT_EQ( 2.14, rational( 2.14 ) );
EXPECT_EQ( [], rational( [] ) );
val = 1i*0.152;
EXPECT_EQ( 1+val, rational( 1+val ) );

%% randsum
check = @(x, sum_, mn_, mx_ ) EXPECT_EQ( sum(x), sum_ ) && ...
                              EXPECT_GE( min(x), mn_ ) && ...
                              EXPECT_LE( max(x), mx_ );
EXPECT_NTHROW( @() check( randsum( 27, 5, 3, 1 ), 27, 5, 3 ) );
EXPECT_NTHROW( @() check( randsum( 27, 5, 3 ), 27, 5, 3 ) );
EXPECT_NTHROW( @() check( randsum( 27, 5 ), 27, 5, 1 ) );
EXPECT_NTHROW( @() check( randsum( 27 ), 27, 27, 1 ) );
EXPECT_EQ( randsum( 1 ), [1] );
EXPECT_EMPTY( randsum( 0 ) );

%% rand_oneof
r = []; for i = 1:20; r = [r rand_oneof( {0 1 2 3 4 5} )]; end;  %#ok<AGROW>
EXPECT_TRUE( any(r(1) ~= r) );

r = []; for i = 1:20; r = [r rand_oneof( [0 1 2 3 4 5] )]; end;  %#ok<AGROW>
EXPECT_TRUE( any(r(1) ~= r) );

r = []; for i = 1:20; r = [r rand_oneof( 0, 1, 2, 3, 4, 5 )]; end;  %#ok<AGROW>
EXPECT_TRUE( any(r(1) ~= r) );

%% realify
TESTCASE
EXPECT_EQ( imag2real( [] ), [] );
EXPECT_EQ( imag2real( -1 ), [-1 0;0 -1] );

%% removezero
TESTCASE
AA = [0 0 0 0;0 1 0 0;0 0 0 0;0 2 0 3];
EXPECT_EQ( removezero(AA,'compact',1), [0 1 0 3;0 2 0 0] );
EXPECT_EQ( removezero(AA,'compact',2), [0 0;1 0;0 0;2 3] );
EXPECT_EQ( removezero(AA,'compact',[1 2]), [1 3; 2 0] );
EXPECT_EQ( removezero(AA,[1 2]), [1 0; 2  3] );
EXPECT_EQ( removezero(AA,[]), AA );
EXPECT_EQ( removezero(AA,'border'), [1 0 0;0 0 0;2 0 3] );
EXPECT_EQ( removezero(AA,'outside'), [0 0 0 0; 0 1 0 0;0 0 0 0;0 2 0 3] );
EXPECT_EQ( removezero(AA,'inside'), [1 0 0;0 0 0;2 0 3] );
EXPECT_EQ( removezero(AA,'all'), [1;2;3] );
EXPECT_EQ( removezero(AA,'left'), [0 0 0; 1 0 0;0 0 0;2 0 3] );
EXPECT_EQ( removezero(AA,'right'), AA );
EXPECT_EQ( removezero(AA,'top'), [0 1 0 0;0 0 0 0;0 2 0 3] );
EXPECT_EQ( removezero(AA,'bottom'), AA );
AA = [0 1 0; 0 2 0];
[val,idx_] = removezero(AA,[2],'keepdim');
EXPECT_EQ( val, [1 0;2 0] );
EXPECT_EQ( idx_, [1;2] );
EXPECT_THROW( 'removezero(AA,''wrongstring'');', 'removezero:arg' );
AA = [0 0 1 0 1];
[~,idx_] = removezero( AA, 'border' );
EXPECT_EQ( idx_, [1;3] );

EXPECT_EQ( removezero( [0 0 0; 1 1 1; 0 0 0; 1 1 1], 'border', 'value',0 ), [1 1 1;0 0 0;1 1 1] );
EXPECT_EQ( removezero( [0 0 0; 1 1 1; 0 0 0; 1 1 1], 'border', 'value',1 ), [0 0 0;1 1 1;0 0 0] );
EXPECT_EQ( removezero( [0 0 0; 1 1 1; 0 0 0; 1 1 1], 'border', 'value',2 ), [0 0 0;1 1 1;0 0 0;1 1 1] );
EXPECT_EQ( removezero( [0 0 0; 1 1 1; 0 0 0; 1 1 1], 'top', 'value',0 ), [1 1 1;0 0 0;1 1 1] );
EXPECT_EQ( removezero( [0 0 0; 1 1 1; 0 0 0; 1 1 1], 'top', 'value',1 ), [0 0 0;1 1 1;0 0 0;1 1 1] );
EXPECT_EQ( removezero( [0 0 0; 1 1 1; 0 0 0; 1 1 1], 'top', 'value',2 ), [0 0 0;1 1 1;0 0 0;1 1 1] );
EXPECT_EQ( removezero( [0 0 0; 1 1 1; 0 0 0; 1 1 1], 1, 'value',0 ), [1 1 1;1 1 1] );
EXPECT_EQ( removezero( [0 0 0; 1 1 1; 0 0 0; 1 1 1], 1, 'value',1 ), [0 0 0;0 0 0] );
EXPECT_EQ( removezero( [0 0 0; 1 1 1; 0 0 0; 1 1 1], 1, 'value',2 ), [0 0 0;1 1 1;0 0 0;1 1 1] );
EXPECT_EQ( removezero( [0 0 0; 1 1 1; 0 0 0; 1 1 1], 2, 'value',0 ), [0 0 0;1 1 1;0 0 0;1 1 1] );
EXPECT_EQ( removezero( [0 0 0; 1 1 1; 0 0 0; 1 1 1], 2, 'value',1 ), [0 0 0;1 1 1;0 0 0;1 1 1] );
EXPECT_EQ( removezero( [0 0 0; 1 1 1; 0 0 0; 1 1 1], 2, 'value',2 ), [0 0 0;1 1 1;0 0 0;1 1 1] );

EXPECT_EQ( size(removezero([0 0 0],2)), [1 0] );
EXPECT_EQ( size(removezero([0 0 0],1)), [0 3] );


%% repcell
TESTCASE
EXPECT_EQ( repcell(10 ,2), {10 10;10 10} );

%% savetocellarray
TESTCASE
%%% general test
val = savetocellarray( [1 2 3], logical([1 1 0 0 1]), {[-1 -2], [-3 -4 -5]} );
EXPECT_EQ( val, {[1 2], [-3 -4 3]} );

%%% only save values in one row
EXPECT_NTHROW( 'val = savetocellarray( [-1 -2 -3], [1 0 1 0 1], {[1 2 3;1 2 3;1 2 3] [4 5;4 5;4 5]}, 3  )' );
EXPECT_EQ( val, {[1 2 3;1 2 3;-1 2 -2] [4 5;4 5;4 -3]} );

%%% save whole column
EXPECT_EQ( {[1 5;2 7;3 9]}, savetocellarray( [1;2;3], [1 0], {[4 5;6 7;8 9]} ) );
EXPECT_EQ( {[inf 5;inf 7;inf 9]}, savetocellarray( inf, [1 0], {[4 5;6 7;8 9]} ) );
EXPECT_EQ( {[4 5;inf 7;inf 9]}, savetocellarray( inf, [1 0], {[4 5;6 7;8 9]}, [2 3] ) );

%%% no values to save
EXPECT_EQ( {[1 2 3]}, savetocellarray([],[0 0 0],{[1 2 3]}) );

%% searchincellarray
TESTCASE
[found_, ioo_] = searchincellarray( [1 2 3 4], {}, 0 );
EXPECT_EQ( found_, false );
EXPECT_EQ( ioo_, 0 );
[found_, ioo_] = searchincellarray( [1 2 3 4], {[1 2; 3 4],[1 2 3 4],[2 3 1]',[5]}, 0 );
EXPECT_EQ( found_, true );
EXPECT_EQ( ioo_, 2 );


%% setplus
TESTCASE
EXPECT_NO_THROW( 'setplus( [1 0; 1 1], [10 10; 20 20; 30 30], ''rows'' );' );
EXPECT_NO_THROW( 'setplus(1);' );
val = setplus( [1 0; 1 1]', [10 10; 20 20; 30 30]' );
EXPECT_EQ( val, [11 11 21 21 31 31;10 11 20 21 30 31] );
val = setplus( [1 0], [0 1], 'stable' );
EXPECT_EQ( val, [1 0 2] );
val = setplus( [1 0], [0 1], [0], [1], 'nounique' );
EXPECT_EQ( sort(val), [1 2 2 3] );


%% smallestchoice
TESTCASE
smallestchoice( randi(5,2,30), randi(5,1,30), randi(5,1,30), @(x,y) norm(x-y)<4 );

[c,ic] = smallestchoice( [4 3;3 1;4 3;4 5;3 1;1 3;5 5;5 5;1 4;3 2].', [2 2 2 5 2 4 3 3 3 3], [3 1 5 2 5 1 1 5 4 4] );
EXPECT_EQ( c, {[3;1],[4;5],[4;3],[1;4],[4 3;3 1].'} );
EXPECT_EQ( ic, [1 2 3 4 5 9] );

[c,ic] = smallestchoice( [1 1 1 3 1], [1 1 2 2 2], [1 1 1 3 1] );
EXPECT_EQ( c, {[1],[],[3]} );
EXPECT_EQ( ic, [1 4] );

[c,ic] = smallestchoice( [5 3;5 2;4 1].', [2 2 1], [4 2 5] );
EXPECT_EQ( c, {[],[5;2],[],[5;3],[4;1]} );
EXPECT_EQ( ic, [1 2 3] );

[c,ic] = smallestchoice( [], [], [] );
EXPECT_EQ( c, {} );
EXPECT_EQ( ic, [] );

[c,ic] = smallestchoice( [10 20;10 20;11 20;20 20;10 20;20 21; 30 10]', [3 1 1 2 2 1 3], [1 1 1 2 2 2 3] );
EXPECT_EQ( c, {[10;20],[10;20],[30;10]} );
EXPECT_EQ( ic, [2 5 7] );

[c,ic] = smallestchoice( [1 2;1 2;1.1 2;2 2;1 2;2 2.1; 3 1]', [3 1 1 2 2 1 3], [1 1 1 2 2 2 3] );
EXPECT_EQ( c, {[1;2],[1;2],[3;1]} );
EXPECT_EQ( ic, [2 5 7] );

[c,ic] = smallestchoice( [1 2;1 2;1.1 2;2 2;1 2;2 2.1; 3 1]', [3 1 1 2 2 1 3], [1 1 1 2 2 2 3], .5 );
EXPECT_EQ( c, {[1;2],[1;2],[3;1]} );
EXPECT_EQ( ic, [2 5 7] );

[c,ic] = smallestchoice( [1 2 2 3 3 4], [2 1 1 1 2 1], [1 1 2 2 3 3] );
EXPECT_EQ( c, {[2],[2],[4]} );
EXPECT_EQ( ic, [2 3 6] );

%% subsco
TESTCASE
val = subsco( [1 2 3; 4 5 6], [1 2; 2 3]' );
EXPECT_EQ( val, [2 6] );
val = subsco( [1 2 3; 4 5 6], [1 2; 2 3]', [-1 -1] );
EXPECT_EQ( val, [1 -1 3;4 5 -1] );
EXPECT_THROW( 'subsco( [1 2 3; 4 5 6], [1 2; 4 3]'' );', 'ttest_idOutOfRange', 'Octave:index-out-of-bounds' );
val = subsco([1 2 3; 4 5 6],[1 2; 4 3]','save');
EXPECT_THROW( 'val = subsco( [1 2 3; 4 5 6], [1 2; 1 2]'', [1 2], ''save'' );', 'subsco:multipleindex' );

%% tavailable_memory
TESTCASE
EXPECT_NO_THROW( 'a=tavailable_memory;' );
EXPECT_GE( a, 512*1024*1024 );


%% tif
TESTCASE
EXPECT_EQ( tif(true,1,2) , 1 );
EXPECT_EQ( tif(false,1,2), 2 );


%% unflatten/flatten  %unflatten(flatten(C_),C_) );
TESTCASE
C=[1 2 3;4 5 6];    EXPECT_EQ( C, unflatten(flatten(C),C) );
C={1 {} };          EXPECT_EQ( C, unflatten(flatten(C),C) );
C={};               EXPECT_EQ( C, unflatten(flatten(C),C) );
C={{}};             EXPECT_EQ( C, unflatten(flatten(C),C) );
C=[];               EXPECT_EQ( C, unflatten(flatten(C),C) );
C={1 2 3};          EXPECT_EQ( unflatten( 10, C, 'assign' ), {10 10 10} );
C={};               EXPECT_EQ( unflatten( 10, C, 'assign' ), {} );
C={{}};             EXPECT_EQ( unflatten( 10, C, 'assign' ), {{}} );


%% unfind
TESTCASE
EXPECT_EQ( unfind( [], 3 ), [false false false]' );
EXPECT_EQ( unfind( [1 2 3], 3 ), [true true true]' );
EXPECT_EQ( unfind( [1 2 3], [1 3] ), [true true true] );
EXPECT_EQ( unfind( [], 0 ), zeros(0,1) );
EXPECT_THROW( @() unfind( [1 4], 3 ), 'unfind:indexing' );
EXPECT_THROW( @() unfind( [true false true], 3 ), 'unfind:indexing' );

%% uniquecell
TESTCASE
val = uniquecell( {1 2 1 [1 2] [1 3] [1 2]} )';
EXPECT_EQ( numel(val), 4 );
[AUC_, IDXC_ ,IDXCC_] = uniquecell({1 2 3 1 3 4 5});
[AUA_, IDXA_ ,IDXAA_] = unique(    [1 2 3 1 3 4 5]);
EXPECT_EQ( cell2mat(AUC_), AUA_ );
EXPECT_EQ( IDXC_, IDXA_ );
EXPECT_EQ( IDXCC_, IDXAA_ );


%% vdisp
TESTCASE
val = vdisp( {[1 2 3]} );
EXPECT_TRUE( ischar(val) && strfind(val,'1') && strfind(val,'2') && strfind(val,'3') ); %#ok<STRIFCND>

ST_ = struct;
ST_.There = 'should';
ST_.be = 'a';
ST_.struct = @displayed;
val = vdisp(ST_);
EXPECT_TRUE( contains(val,'struct') && contains(val,'@displayed') && contains(val,'There') );
val = vdisp({[123 234 345]},'OLD');
EXPECT_TRUE( ischar(val) && contains(val,'OLD') && contains(val,'123') && contains(val,'234') && contains(val,'345') );


%% vprintf
TESTCASE
val = vprintf('12345', 'cpr', [.8 .4 .24], 'str','', 'npr' );
EXPECT_TRUE( contains(val,'12345') );

vprintf('ERROR: vprintf() is broken.\n\n', 'imp',[3 2], 'cpr','err' );
vprintf('ERROR: vprintf() is broken.\n\n', 'cpr','err', 'noprint' );
val = vprintf('TEST','cpr','err','sze',[10 100], 'noprint' );
EXPECT_FALSE( contains(val, 'TEST') );
val = vprintf( '%v', [1 2 3;4 5 6], 'npr' );
newl = sprintf('\n'); %#ok<SPRINTFN>
EXPECT_TRUE( contains(val,'1') && contains(val,'6') && contains(val,newl) );
stru.num=6;
val = vprintf( 'Numbers from 1 to 6 : \n   %i %f %v %r %v\n', 1, 2, {{3}}, [4 5], stru, 'npr',1 );
EXPECT_TRUE( contains(val,'1') && contains(val,'2.0') && contains(val,'3') && contains(val,'4') && contains(val,'5') && contains(val,'num') && contains(val,'6') );
val = vprintf('Some text again.', 'str','OLD', 'noprint' );
EXPECT_EQ( val, 'OLDSome text again.' );
[ONE] = vprintf('Some text again.', 'str','ONE', 'noprint' );
EXPECT_EQ( ONE, 'ONESome text again.' );
val = vprintf( '%i%%',100, 'noprint' );
EXPECT_EQ( val, '100%' );
EXPECT_THROW( 'vprintf(''ERROR: vprintf() is broken. Wrong number of arguments %i\n\n'',''cpr'',''err'');', 'vprintf:nargin' );
EXPECT_EQ( evalc( 'vprintf(''imp'')' ), 'imp' );
EXPECT_EQ( evalc( 'vprintf(''cpr'')' ), 'cpr' );
EXPECT_EQ( evalc( 'vprintf(''npr'')' ), 'npr' );
EXPECT_EQ( evalc( 'vprintf(''str'')' ), 'str' );
EXPECT_EQ( evalc( 'vprintf(''once'')' ), 'once' );

EXPECT_NO_THROW( 'str = vprintf( ''ABC\n%r\nDEF'', {[1 2 3]''}, ''str'','''' );' );
EXPECT_NO_THROW( 'str = vprintf( ''ABC\n%r\nDEF'', {[1 2 3]''}, ''str'','''' );' );

EXPECT_NTHROW( @() vprintf('%v|%v%%',{[1:1000]},{[1:1000]}) );
EXPECT_NTHROW( @() vprintf('%r\n|%r%%',[1:1000],[1:1000]) );
EXPECT_NTHROW( @() vprintf('%\r%%',[1:1000]) );


%%
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*NBRAK2,*NBRAK>
