% test m

%#ok<*CTPCT>
%#ok<*ASGLU>
%#ok<*NASGU>

TTEST init test_m

ASSUME_TOOLBOX( 'signal' );

% preconditions

%% allm
AA = [1 1 2;0 1 1; 1 1 1];
EXPECT_EQ( allm(), 1);
EXPECT_EQ( allm(AA), 0 );
EXPECT_EQ( allm(AA, 1), [0 1 1] );
EXPECT_EQ( allm(AA, 2), [1;0;1] );
EXPECT_EQ( allm(AA,[1 2]), 0 );
EXPECT_EQ( allm([1 0;1 2],[]), [1 0;1 1] );

%% anym
BB = [0 1 2;0 1 1; 0 1 1];
EXPECT_EQ( anym(), 0 );
EXPECT_EQ( anym(BB), 1 );
EXPECT_EQ( anym(BB, 1), [0 1 1] );
EXPECT_EQ( anym(BB, 2), [1;1;1] );
EXPECT_EQ( anym(BB,[1 2]), 1 );
EXPECT_EQ( anym([1 0;1 2],[]), [1 0;1 1] );

%% cart2sphm
EXPECT_ALMOST_EQ( cart2sphm([1 0]'), [0 1]' );
EXPECT_ALMOST_EQ( cart2sphm2(2*[0 1 0]'), [pi/2 0]' );
val = colnorm( randn( 100, 300 ) );
EXPECT_NEAR( sph2cartm2(cart2sphm2(val)), val, 5e-10 );

%S^1
%cart2sphm, type=0
EXPECT_ALMOST_EQ( cart2sphm([1 0]'), [0 1]' );
EXPECT_ALMOST_EQ( cart2sphm([0 1]'), [pi/2 1]' );
EXPECT_ALMOST_EQ( cart2sphm([-1 0]'), [pi 1]' );
EXPECT_ALMOST_EQ( cart2sphm([0 -1]'), [-pi/2 1]' );
%cart2sphm2, type=0
EXPECT_ALMOST_EQ( cart2sphm2(2*[1 0]'), [0]' );
EXPECT_ALMOST_EQ( cart2sphm2(2*[0 1]'), [pi/2]' );
EXPECT_ALMOST_EQ( cart2sphm2(2*[-1 0]'), [pi]' );
EXPECT_ALMOST_EQ( cart2sphm2(2*[0 -1]'), [-pi/2]' );
%sph2cartm, type=0
EXPECT_ALMOST_EQ( [1 0]', sph2cartm([0 1]') );
EXPECT_ALMOST_EQ( [0 1]', sph2cartm([pi/2 1]') );
EXPECT_ALMOST_EQ( [-1 0]', sph2cartm([pi 1]') );
EXPECT_ALMOST_EQ( [0 -1]', sph2cartm([-pi/2 1]') );
%sph2cartm2, type=0
EXPECT_ALMOST_EQ( [1 0]', sph2cartm2([0]') );
EXPECT_ALMOST_EQ( [0 1]', sph2cartm2([pi/2]') );
EXPECT_ALMOST_EQ( [-1 0]', sph2cartm2([pi]') );
EXPECT_ALMOST_EQ( [0 -1]', sph2cartm2([-pi/2]') );
%sph2cartm/sph2cartm2, type=0
val = colnorm( randn( 2, 10 ) );
EXPECT_NEAR( sph2cartm(cart2sphm(val)),val,1e-10 );
val = colnorm( randn( 2, 300 ) );
EXPECT_NEAR( sph2cartm2(cart2sphm2(val)), val, 1e-10 );

%S^2
%cart2sphm
EXPECT_ALMOST_EQ( cart2sphm([1 0 0]'), [0 0 1]');
EXPECT_ALMOST_EQ( cart2sphm([0 1 0]'), [pi/2 0 1]');
EXPECT_ALMOST_EQ( cart2sphm([0 0 1]'), [pi/2 pi/2 1]');
EXPECT_ALMOST_EQ( cart2sphm([-1 0 0]'), [pi 0 1]');
EXPECT_ALMOST_EQ( cart2sphm([0 -1 0]'), [pi/2 pi 1]');
EXPECT_ALMOST_EQ( cart2sphm([0 0 -1]'), [pi/2 -pi/2 1]');
%type = 'azel', 'azinc'
EXPECT_ALMOST_EQ( cart2sphm([1 0 0]', 'azel'),  [0 0 1]');
EXPECT_ALMOST_EQ( cart2sphm([0 1 0]', 'azel'),  [pi/2 0 1]');
EXPECT_ALMOST_EQ( cart2sphm([0 0 1]', 'azel'),  [0 pi/2 1]');
EXPECT_ALMOST_EQ( cart2sphm([0 0 1]', 'azinc'), [0 0 1]');
EXPECT_ALMOST_EQ( cart2sphm([0 1 0]', 'azinc'), [pi/2 pi/2 1]');
EXPECT_ALMOST_EQ( cart2sphm([1 0 0]', 'azinc'), [0 pi/2 1]');
%cart2sphm2, type = 'h'
EXPECT_ALMOST_EQ( cart2sphm2(2*[1 0 0]'),[0 0]');
EXPECT_ALMOST_EQ( cart2sphm2(2*[0 1 0]'),[pi/2 0]');
EXPECT_ALMOST_EQ( cart2sphm2(2*[0 0 1]'),[pi/2 pi/2]');
EXPECT_ALMOST_EQ( cart2sphm2(2*[-1 0 0]'),[pi 0]');
EXPECT_ALMOST_EQ( cart2sphm2(2*[0 -1 0]'),[pi/2 pi]');
EXPECT_ALMOST_EQ( cart2sphm2(2*[0 0 -1]'),[pi/2 -pi/2]');
%sph2cartm, type = 'h'
EXPECT_ALMOST_EQ( [1 0 0]',  sph2cartm([0 0 1]') );
EXPECT_ALMOST_EQ( [0 1 0]',  sph2cartm([pi/2 0 1]') );
EXPECT_ALMOST_EQ( [0 0 1]',  sph2cartm([pi/2 pi/2 1]') );
EXPECT_ALMOST_EQ( [-1 0 0]', sph2cartm([pi 0 1]') );
EXPECT_ALMOST_EQ( [0 -1 0]', sph2cartm([pi/2 pi 1]') );
EXPECT_ALMOST_EQ( [0 0 -1]', sph2cartm([pi/2 -pi/2 1]') );
%type = 2/3
EXPECT_ALMOST_EQ( [1 0 0]', sph2cartm([0 0 1]', 'azel') );
EXPECT_ALMOST_EQ( [0 1 0]', sph2cartm([pi/2 0 1]', 'azel') );
EXPECT_ALMOST_EQ( [0 0 1]', sph2cartm([0 pi/2 1]', 'azel') );
EXPECT_ALMOST_EQ( [0 0 1]', sph2cartm([0 0 1]', 'azinc') );
EXPECT_ALMOST_EQ( [0 0 1]', sph2cartm([pi/2 0 1]', 'azinc') );
EXPECT_ALMOST_EQ( [1 0 0]', sph2cartm([0 pi/2 1]', 'azinc') );
%sph2cartm2, type = 'h'
EXPECT_ALMOST_EQ( [1 0 0]', sph2cartm2([0 0]') );
EXPECT_ALMOST_EQ( [0 1 0]', sph2cartm2([pi/2 0]') );
EXPECT_ALMOST_EQ( [0 0 1]', sph2cartm2([pi/2 pi/2]') );
EXPECT_ALMOST_EQ( [-1 0 0]', sph2cartm2([pi 0]') );
EXPECT_ALMOST_EQ( [0 -1 0]', sph2cartm2([pi/2 pi]') );
EXPECT_ALMOST_EQ( [0 0 -1]', sph2cartm2([pi/2 -pi/2]') );
%sph2cartm/sph2cartm2, type = 'h'
val = colnorm( randn( 3, 300 ) );
EXPECT_NEAR( sph2cartm(cart2sphm(val)), val, 1e-12 );
EXPECT_NEAR( sph2cartm2(cart2sphm2(val)), val, 1e-12 );
%cart2sphm, ...type = 'h', test using Matlabs cart2sph()
val = colnorm( randn(3,300) );
[val1, val2, val3] = cart2sph( val(1,:), val(2,:), val(3,:) );
EXPECT_ALMOST_EQ( cart2sphm( val, 'azel'), [val1;val2;val3] );

[val1, val2, val3] = sph2cart( val(1,:),val(2,:),val(3,:) );
EXPECT_ALMOST_EQ( sph2cartm(val, 'azel'), [val1;val2;val3] );

[val1, val2, ~] = cart2sph( val(1,:),val(2,:),val(3,:) );
EXPECT_ALMOST_EQ( cart2sphm2(val, 'azel'), [val1;val2] );

[val1,val2,val3] = sph2cart( val(1,:), val(2,:), ones(1,size(val,2)) );
EXPECT_ALMOST_EQ( sph2cartm2(val(1:2,:), 'azel'), [val1;val2;val3] );

%S^99
%sph2cartm/sph2cartm2, type = 'h'
val = colnorm( randn(100,300) );
EXPECT_NEAR( sph2cartm(cart2sphm(val)), val, 1e-11 );
val = colnorm( randn(100,300) );
EXPECT_NEAR( sph2cartm2(cart2sphm2(val)), val, 1e-11 );

%sph2sphm
val = sph2sphm( randn(100,300), 'h', 'h' );
EXPECT_NEAR( val,sph2sphm(val, 'h', 'h'),1e-9);
val = sph2sphm2(randn(100,300), 'h', 'h' );
EXPECT_NEAR( val,sph2sphm2(val, 'h', 'h'),5e-10);
%S^2, type 1
val = sph2sphm( randn(3,300), 'azel', 'azel' );
EXPECT_ALMOST_EQ( val, sph2sphm(val, 'azel', 'azel') );
val = sph2sphm2( randn(2,300), 'azel', 'azel' );
EXPECT_ALMOST_EQ( val, sph2sphm2(val, 'azel', 'azel') );
val = sph2sphm( randn(3,300), 'h', 'h' );
val1 = sph2sphm( sph2sphm(val, 'h', 'azel'), 'azel', 'h' );
EXPECT_NEAR( val, val1, 5e-12 );
val = sph2sphm( randn(3, 300), 'azel', 'azel' );
val1 = sph2sphm( sph2sphm(val, 'azel', 'h'), 'h', 'azel' );
EXPECT_NEAR( val, val1, 5e-12 );

val = sph2sphm( randn(3, 300), 'azinc', 'azinc' );
EXPECT_NEAR( val,sph2sphm(val, 'azinc', 'azinc' ), 1e-10 );
val = sph2sphm2(randn(2, 300), 'azinc', 'azinc' );
EXPECT_NEAR( val,sph2sphm2(val, 'azinc', 'azinc'), 1e-10 );
val = sph2sphm( randn(3, 300), 'h', 'h' );
val1=sph2sphm( sph2sphm(val, 'h', 'azel'), 'azel', 'h' );
EXPECT_NEAR( val, val1, 1e-10 );
val = sph2sphm( randn(3, 300), 'azinc', 'azinc' );
val1=sph2sphm( sph2sphm(val, 'azinc', 'h'), 'h', 'azinc' );
EXPECT_NEAR( val, val1, 1e-10 );
%type 2 <-> type 3
EXPECT_ALMOST_EQ( sph2sphm([0 pi/2 1]', 'azel', 'azinc'), [0 0 1]' );
EXPECT_ALMOST_EQ( sph2sphm([2*pi pi/2 1]', 'azel', 'azinc'), [0 0 1]' );
EXPECT_ALMOST_EQ( sph2sphm([0 0 1]', 'azel', 'azinc'), [0 pi/2 1]' );
EXPECT_ALMOST_EQ( sph2sphm([0 0 1]', 'azinc', 'azel'), [0 pi/2 1]' );
EXPECT_ALMOST_EQ( sph2sphm([0 pi/2 1]', 'azinc', 'azel'), [0 0 1]' );
%type 4 <-> type 2
EXPECT_ALMOST_EQ( sph2sphm([pi 0 1]', 'ant', 'azel'), [-pi/2 0 1]' );
EXPECT_ALMOST_EQ( sph2sphm([pi/2 0 1]', 'ant', 'azel'), [0 0 1]' );
EXPECT_ALMOST_EQ( sph2sphm([3*pi/2 0 1]', 'ant', 'azel'), [-pi 0 1]' );
EXPECT_ALMOST_EQ( sph2sphm([0 pi/2 1]', 'ant', 'azel'), [pi/2 -pi/2 1]' );
EXPECT_ALMOST_EQ( sph2sphm([pi/4 -pi/4 1]', 'ant', 'azel'), [pi/4 pi/4 1]' );
EXPECT_ALMOST_EQ( sph2sphm([pi/4 pi/4 1]', 'ant', 'azel'), [pi/4 -pi/4 1]' );

%type 100 tests
EXPECT_ALMOST_EQ( cart2sphm([1;0;1], 'hcyl'), [0;1;1] );
EXPECT_ALMOST_EQ( [1;0;1], sph2cartm([0;1;1], 'hcyl') );
EXPECT_ALMOST_EQ( cart2sphm2([2;0;2], 'hcyl'), [0;2] );
EXPECT_ALMOST_EQ( [1;0;1], sph2cartm2([0;1], 'hcyl') );
EXPECT_ALMOST_EQ( cart2sphm([0;1;1], 'hcyldeg'), [90;1;1] );
EXPECT_ALMOST_EQ( [0;1;1], sph2cartm([90;1;1], 'hcyldeg') );
EXPECT_ALMOST_EQ( cart2sphm2([0;2;2], 'hcyldeg'), [90;2] );
EXPECT_ALMOST_EQ( [0;1;1], sph2cartm2([90;1], 'hcyldeg') );

%dim 2 tests of type 2/3/4
val = randn(2,100);
EXPECT_NEAR( cart2sphm(val, 'h'), cart2sphm(val, 'azel'), 1e-10 );
EXPECT_NEAR( cart2sphm(val, 'h'), cart2sphm(val, 'azinc'), 1e-10 );
EXPECT_NEAR( sph2cartm(val, 'h'), sph2cartm(val, 'azel'), 1e-12 );
EXPECT_NEAR( sph2cartm(val, 'h'), sph2cartm(val, 'azinc'), 1e-12 );
EXPECT_NEAR( sph2cartm([0;1], 'ant'), [0;1], 1e-12 );
EXPECT_NEAR( cart2sphm([1;0], 'azeldeg'), [0;1], 1e-12 );
EXPECT_NEAR( cart2sphm([0;1], 'azeldeg'), [90;1], 1e-12 );
EXPECT_NEAR( cart2sphm(val, 'hd'), cart2sphm(val, 'azeldeg'), 1e-6 );
EXPECT_NEAR( cart2sphm(val, 'hd'), cart2sphm(val, 'azincdeg'), 1e-6 );
EXPECT_NEAR( sph2cartm(val, 'hd'), sph2cartm(val, 'azeldeg'), 1e-12  );
EXPECT_NEAR( sph2cartm(val, 'hd'), sph2cartm(val, 'azincdeg'), 1e-12  );
EXPECT_NEAR( sph2cartm([0;1], 'antdeg'), [0;1], 1e-12  );

%tests of type 104
EXPECT_NEAR( sph2cartm([pi/2;-1;20], 'hant'), [20;0;-1], 1e-12 );
EXPECT_NEAR( sph2cartm([90;-1;20], 'hantdeg'), [20;0;-1], 1e-12 );

%sph2sphm2 tests with TTEST_ALLTESTFLAG set missing


%% sph2cartm_rand
for type1 = {'hsph', 'azel', 'azinc', 'latlong', 'wgs', 'hcyl'}; % 'hant' not allowed
    type1 = type1{1}; %#ok<FXSET>
    for i = 1:2;
        switch type1;
            case {'hsph'}; dim = randi(10) + 1;
            case {'azel', 'azinc', 'latlong', 'wgs'}; dim = randi(2)+1;
            case {'hcyl'}; dim = randi(10) + 1;
            otherwise; dim = randi(5); end;
        if( i==2 );
            type1 = [type1 'deg']; end; %#ok<AGROW,FXSET>
        coA = randn( dim, 10 );
        coB = cart2sphm( coA, type1 );
        coC = sph2cartm( coB, type1 );
        EXPECT_NEAR( coA, coC, 5e-9 );

        coA = randn( dim, 10 );
        coB = cart2sphm2( coA, type1 );
        coC = sph2cartm2( coB, type1 );
        coD = cart2sphm2( coC, type1 );
        coE = sph2cartm2( coD, type1 );
        EXPECT_NEAR( coC, coE, 1e-8 );
    end;
end;


%% sph2sphm_rand

MESSAGE( 'type1', 'type2', 'j' );
for type1 = {'hsph', 'azel', 'azinc', 'latlong', 'hcyl', 'wgs'};
    type1 = type1{1}; %#ok<FXSET>
    for i = 1:2;
        if( i==2 );
            type1 = [type1 'deg']; end;  %#ok<AGROW,FXSET>
        for type2 = {'hsph', 'azel', 'azinc', 'latlong', 'hcyl', 'wgs'};
            type2 = type2{1};  %#ok<FXSET>
            for j = 1:2;
                if( j == 2 );
                    type2 = [type2 'deg']; end;  %#ok<FXSET,AGROW>
                dim = randi(2) + 1;
                coA = randn( dim, 10 );
                coB = sph2sphm( coA, type1, type2 );
                coC = sph2sphm( coB, type2, type1 );
                coD = sph2sphm( coC, type1, type2 );
                coE = sph2sphm( coD, type2, type1 );
                
                EXPECT_NEAR( coC, coE, 5e-9 );

                coA = randn( dim-1, 10 );
                coB = sph2sphm2( coA, type1, type2 );
                coC = sph2sphm2( coB, type2, type1 );
                coD = sph2sphm2( coC, type1, type2 );
                coE = sph2sphm2( coD, type2, type1 );
                
                % EXPECT_NEAR( coC, coE, 5e-11 );
                % EXPECT_NEAR( coC, coE, 5 );
            end;
        end;
    end;
end;
MESSAGE();

%% wgs

    pt_1 = randn(3,100) * 6378137;
    pt_2 = cart2sphm( pt_1, 'wgs84' );
    pt_3 = sph2cartm( pt_2, 'wgs84' );
    pt_4 = cart2sphm( pt_3, 'wgs84' );
    EXPECT_NEAR( pt_1, pt_3, 1e-6 );
    EXPECT_NEAR( pt_2, pt_4, 1e-6 );

EXPECT_NEAR( cart2sphm( [6371000 0 0].', 'wgsdeg' ), [0 0 0].', 1e-6 );
EXPECT_NEAR( cart2sphm( [0 6371000 0].', 'wgsdeg' ), [0 90 0].', 1e-6 );
EXPECT_NEAR( cart2sphm( [.01 -.01 6371000].', 'wgsdeg' ), [90 -45 0].', 1e-6 );

EXPECT_NEAR( cart2sphm( [6378137 0 0].', 'wgs84deg' ), [0 0 0].', 1e-6 );
EXPECT_NEAR( cart2sphm( [0 6378137 0].', 'wgs84deg' ), [0 90 0].', 1e-6 );
EXPECT_NEAR( cart2sphm( [.01 -.01 6356752.314245].', 'wgs84deg' ), [90 -45 0].', 1e-6 );

EXPECT_NEAR( sph2cartm( [0 0 0].', 'wgsdeg' ), [6371000 0 0].', 1e-6 );
EXPECT_NEAR( sph2cartm( [0 90 0].', 'wgsdeg' ), [0 6371000 0].', 1e-6 );
EXPECT_NEAR( sph2cartm( [90 -45 0].', 'wgsdeg' ), [0 0 6371000].', 1e-6 );

EXPECT_NEAR( sph2cartm( [0 0 0].', 'wgs84deg' ), [6378137 0 0].', 1e-6 );
EXPECT_NEAR( sph2cartm( [0 90 0].', 'wgs84deg' ), [0 6378137 0].', 1e-6 );
EXPECT_NEAR( sph2cartm( [90 -45 0].', 'wgs84deg' ), [0 0 6356752.314245].', 1e-6 );

%% convm
seq1 = convm( [1 2], [3 4] );

EXPECT_EQ( seq1,[3 10 8]);
seq1 = convm( [1 1],[1 2]);
EXPECT_ALMOST_EQ( seq1,[1 3 2]);
seq1 = convm( [1 1]',[1 2]');
EXPECT_ALMOST_EQ( seq1,[1 3 2]');
seq1 = convm( [1 2 1],[1; 3; 1], 'mult',[2 1], 'normalize' );
EXPECT_ALMOST_EQ( seq1,1/80*[1 4 6 4 1;3 12 18 12 3;1 4 6 4 1]);
EXPECT_EQ( convm([1 2 1],[1; 3; 1], 'mult', [2 1]), [1 4 6 4 1;3 12 18 12 3;1 4 6 4 1] );
seq1 = convm( [1 2 1],[1; 3; 1], 'outer', 'normalize' );
EXPECT_ALMOST_EQ( seq1,1/20*[1 3 1; 2 6 2; 1 3 1]);
convm( randn(2, 2, 2), randn(2, 2, 2), randn(2, 2, 2), randn(2, 2, 2) );
EXPECT_EQ( convm([], [1 2]), [] );
EXPECT_EQ( convm([1 2], []), [] );

EXPECT_SIZE( convm( [1 2], [1 2 3], [1 2 3 4], 'outer' ), [2 3 4] );

%% dec2basem
num = dec2basem( [10; 1], [1 2; -1 2] );
EXPECT_EQ( num, [1 1 2;1 1 1] );
num = dec2basem( -1, 2, 5 );
EXPECT_EQ( num(1,1:5), [1 1 1 1 1] );
num = dec2basem( 3, 2, 5 );
EXPECT_EQ( num, [0 0 0 1 1] );
dec2basem( [10;-10], [2 1;0 2], 5 );

EXPECT_THROW( 'dec2basem(10,1/2,5);', 'dec2basem:dilation' );

%% factorialm
EXPECT_EQ( factorialm([3 4]), 144 );

%% gcdm
EXPECT_EQ( gcdm(10, 15, 20), 5 );
EXPECT_EQ( gcdm(1), 1);
EXPECT_EQ( gcdm(), 0);
EXPECT_EQ( gcdm([]), 0);
EXPECT_EQ( gcdm([10, 15, 20]), 5 );

%% ind2subm');
ind2subm( [2 1 3], [1 6] );
EXPECT_THROW( @() ind2subm([2 1 3], [1 7]), 'ind2subm' );


%% isvectorm
EXPECT_TRUE( isvectorm(ones(1, 1, 3)) );
EXPECT_FALSE( isvectorm(ones(0)) );
EXPECT_FALSE( isvector(ones(1, 1, 3)) );
EXPECT_FALSE( isvector(ones(0)) );

%% kronm
val = kronm([1 2 3], [2; 3]);
EXPECT_EQ( val, [2 4 6; 3 6 9] );
val = kronm( [], [], [] );
EXPECT_EQ( val, [] );

%% lcmm
EXPECT_EQ( lcmm(10, 15, 20), 60 );
EXPECT_EQ( lcmm([10, 15, 20]), 60 );
EXPECT_EQ( lcmm(1), 1 );
EXPECT_EQ( lcmm(), 1 );
EXPECT_EQ( lcmm(-4), 4 );
EXPECT_EQ( lcmm(-4, 0), 0 );
EXPECT_EQ( lcmm(0), 0 );
EXPECT_EQ( lcmm([]), 1 );


%% maxm
EXPECT_EQ( maxm(), -Inf );
EXPECT_EQ( maxm([1 2 2; 2 3 4; 2 1 3;4 5 NaN]), 5);
EXPECT_EQ( maxm([1 2 2; 2 3 4; 2 1 3],1), [2 3 4] );
EXPECT_EQ( maxm([1 0; 1 -1],[]), [1 0;1 1] );

%% minm
EXPECT_EQ( minm(), Inf );
EXPECT_EQ( minm([1 2 2; 2 3 4; 2 1 3;4 5 NaN]), 1 );
EXPECT_EQ( minm([1 2 2; 2 3 4],2), [1;2] );
EXPECT_EQ( minm([1 0; 1 -1],[]), [1 0;1 1] );

%% mldividem
if( ismatlab )
    EXPECT_ALMOST_EQ( mldividem([2 3 4 5],[1]), [0 0 0 0.2].' ); %#ok<NBRAK2>
    EXPECT_ALMOST_EQ( mldividem([2],[1 2; 3 4]), [.5 1;1.5 2] );  %#ok<NBRAK2>
    EXPECT_ALMOST_EQ( mldividem([2 0;1 1;0 2].',[0.1 0.9].'), [0.05 0 0.45].' );
    end;
%XX missing tests
% A = magic(4); b = [34 34 34 34].';
% val1 = mldividem( sym(A), b );
% val2 = mldividem( A, b );
%
% A = [1 0;0 0].'; b = [1 1].'
% val1 = mldividem( sym(A), b );
% val2 = mldividem( A, b );

%% nchoosekm
EXPECT_EQ( nchoosekm([10 5],[5 2]),2520 );

%% ndimsm
EXPECT_EQ( ndimsm([1; 2; 3]),1);
EXPECT_EQ( ndimsm([1]),1); %#ok<NBRAK2>
EXPECT_EQ( ndimsm([1 2 3]),2);
EXPECT_EQ( ndimsm([]),0);

%% onesm
EXPECT_EQ( onesm, [] );
EXPECT_EQ( onesm([]), [] );
EXPECT_EQ( onesm(1,2), [1 1] );
EXPECT_EQ( onesm(3), [1;1;1] );

%% padarraym
TESTCASE
SECTION
    EXPECT_EQ( padarraym([1], 1), [0;1;0] );  %#ok<NBRAK2>
    EXPECT_EQ( padarraym([], 2), [0;0;0;0] );
    EXPECT_EQ( padarraym([1;2], 1), [0;1;2;0] );
    EXPECT_EQ( padarraym({1;2}, 1), {[];1;2;[]} );
    EXPECT_EQ( padarraym([1 2], 1, 'pre'), [0 0 0;0 1 2] );
    EXPECT_EQ( padarraym({1 2}, 1, 'pre'), {[] [] [];[] 1 2} );
    EXPECT_EQ( padarraym([1 2], 1, 'post'), [1 2 0;0 0 0] );
    EXPECT_EQ( padarraym({1 2}, 1, 'post'), {1 2 [];[] [] []} );
    EXPECT_EQ( padarraym([1 2], 1), [0 0 0 0;0 1 2 0;0 0 0 0] );
    EXPECT_EQ( padarraym({1 2}, 1), {[] [] [] [];[] 1 2 [];[] [] [] []} );
    EXPECT_EQ( padarraym([1 2;3 4],0), [1 2;3 4] );
    EXPECT_EQ( padarraym({1 2;3 4},0), {1 2;3 4} );
    EXPECT_EQ( padarraym({1 2;@sum 4},1), {[] [] [] [];[] 1 2 [];[] @sum 4 [];[] [] [] []} );
    EXPECT_THROW( 'padarraym([1;2],-1);', 'padarraym:argin' );
    EXPECT_EQ( size(padarraym([1 2;3 4], [1 0 1])), [4 2 3] );
    EXPECT_EQ( padarraym(zeros(0,0), [2;1;0], 1, 'post'), [1;1] );

SECTION( 'regressiontests' );
    EXPECT_EQ( padarraym( [-2 2 0 4], [0 5 0 0]', [0], 'pre' ), [0 0 0 0 0 -2 2 0 4] );  %#ok<NBRAK2>
    
%% parsem
SECTION( 'parsing tests' );
    [v,arg] = parsem( 'selftest', {'asd','selftest',2}, @setupm );
    EXPECT_EQ( v, 2 );
    EXPECT_EQ(arg, {'asd'});

    [v,arg] = parsem( 'selftest', 'selftest' );
    EXPECT_EQ( v, 1 );

    [v,arg] = parsem( 'tommsch', {'asd'} );
    EXPECT_FALSE( ~isequal( v,0) || ~isequal(arg, {'asd'}));

    [v,arg] = parsem( {'t','tommsch','t'}, {'asd'} );
    EXPECT_FALSE( ~isequal( v,0) || ~isequal(arg, {'asd'}));

    [v,arg] = parsem( {}, {'asd'} );
    EXPECT_FALSE( ~isequal( v,0) || ~isequal(arg, {'asd'}));

    [v,arg] = parsem( {},{} );
    EXPECT_FALSE( ~isequal( v,0) || ~isequal(arg, {}));

    [v,arg,ret] = parsem('st',{'selftest','st'});
    EXPECT_EQ( ret,'st');
    [v,arg,ret] = parsem({'st','selftest',},{'selftest'});
    EXPECT_EQ( ret,'selftest');
    [v,arg,ret] = parsem({'st','selftest',},{'t'});
    EXPECT_EQ( ret,'st');

    [flag, args] = parsem( 'a', {'a',2,'a',3} );
    EXPECT_EQ( flag, 3 );
    EXPECT_EMPTY( args );

    [flag, args] = parsem( 'a', {'a',2,'a',3}, 1 );
    EXPECT_EQ( flag, 3 );
    EXPECT_EMPTY( args );

    [flag, args] = parsem( 'a', {'a',2,'a',3} );
    EXPECT_EQ( flag, 3 );
    EXPECT_EMPTY( args );

    [flag, args] = parsem( 'a', {'a',2,'a',3}, 1, 'multiple',1 );
    EXPECT_EQ( flag, 2 );
    EXPECT_EQ( args, {'a',3} );

SECTION( 'throw tests' );
    EXPECT_THROW( @() parsem('asd',{'asd'},1), 'parsem:missing' );

    EXPECT_THROW( 'parsem({''2 is the oddest prime.''},''test'');', 'parsem:unknown' );

    EXPECT_THROW( 'parsem({''name''},''name'',''val'');', 'parsem:missing' );



    %warning( 'on', 'parsem:expect' );
    EXPECT_THROW( @() parsem( 't', {'t',11}, 'expect', {'clop',[0 10]} ), 'parsem:expect', '0' );
    EXPECT_THROW( @() parsem( 't', {'t',10}, 'expect', {'clop',[0 10]} ), 'parsem:expect', '< ' );
    EXPECT_THROW( @() parsem( 't', {'t',-1}, 'expect', {'clop',[0 10]} ), 'parsem:expect', '-1' );
    EXPECT_THROW( @() parsem( 't', {'t',[]}, 'expect', {'clop',[0 10]} ), 'parsem:expect', '[]' );
    EXPECT_THROW( @() parsem( 't', {'t',11}, 'expecte',{'clop',[0 10]} ), 'parsem:expect' );
    EXPECT_THROW( @() parsem( 't', {'t',10}, 'expecte',{'clop',[0 10]} ), 'parsem:expect' );
    EXPECT_THROW( @() parsem( 't', {'t',-1}, 'expecte',{'clop',[0 10]} ), 'parsem:expect' );
    EXPECT_EMPTY( parsem( 't', {'t',[]}, 'expecte',{'clop',[0 10]} ) );

    EXPECT_THROW( @() parsem( 't', {'t',11}, 'expect',{'clcl',[0 10]} ), 'parsem:expect', '0' );
    EXPECT_THROW( @() parsem( 't', {'t',-1}, 'expect',{'clcl',[0 10]} ), 'parsem:expect', '10' );
    EXPECT_THROW( @() parsem( 't', {'t',{}}, 'expect',{'clcl',[0 10]} ), 'parsem:expect', '{}' );
    EXPECT_EQ( 10, parsem( 't', {'t',10}, 'expect',{'clcl',[0 10]} ) );

    EXPECT_THROW( @() parsem( 't', {'t',11}, 'expect',{'opcl',[0 10]} ), 'parsem:expect', '< ' );
    EXPECT_THROW( @() parsem( 't', {'t',0},  'expect',{'opcl',[0 10]} ), 'parsem:expect', '0' );
    EXPECT_THROW( @() parsem( 't', {'t',-1}, 'expect',{'opcl',[0 10]} ), 'parsem:expect', '-1' );
    EXPECT_THROW( @() parsem( 't', {'t',[]}, 'expect',{'opcl',[0 10]} ), 'parsem:expect' );
    EXPECT_EQ( 9, parsem( 't', {'t',9}, 'expect',{'opcl',[0 10]} ) );

    EXPECT_THROW( @() parsem( 't', {'t',11}, 'expect',{'opop',[0 10]} ), 'parsem:expect' );
    EXPECT_THROW( @() parsem( 't', {'t',10}, 'expect',{'opop',[0 10]} ), 'parsem:expect' );
    EXPECT_THROW( @() parsem( 't', {'t',0},  'expect',{'opop',[0 10]} ), 'parsem:expect' );
    EXPECT_THROW( @() parsem( 't', {'t',-1}, 'expect',{'opop',[0 10]} ), 'parsem:expect' );
    EXPECT_THROW( @() parsem( 't', {'t',[]}, 'expect',{'opop',[0 10]} ), 'parsem:expect' );
    EXPECT_EQ( 9, parsem( 't', {'t',9}, 'expect',{'opcl',[0 10]} ) );

    EXPECT_THROW( @() parsem( 't', {'t',11}, 'expect',{0 1 10} ), 'parsem:expect', '10' );
    EXPECT_THROW( @() parsem( 't', {'t',[]}, 'expect',{0 1 10} ), 'parsem:expect', '[]' );
    EXPECT_EMPTY( parsem( 't', {'t',[]}, 'expecte',{0 1 10} ) );
    EXPECT_EQ( 10, parsem( 't', {'t',10}, 'expect',{0 1 10} ) );

    EXPECT_THROW( @() parsem( 't', {'t',@sum}, 'expect',@isnumeric ), 'parsem:expect', '@isnumeric' );
    EXPECT_NTHROW( @() parsem( 't', {'t',10}, 'expect',@isnumeric ) );

    EXPECT_THROW( @() parsem( {'s','t','u'}, {'v'}, 'expect','name' ), 'parsem:expect', 'ttest_msgu' );
    EXPECT_NTHROW( @() parsem( {'s','t','u'}, {'t'}, 'expect','name' ) );

%% repcellm
EXPECT_EQ( repcellm(10 ,2),{10;10});
EXPECT_EQ( repcellm(10 ,[2 3]),{10 10 10;10 10 10});

%% repmatm
EXPECT_EQ( repmatm(10 ,2), [10;10] );
EXPECT_EQ( repmatm(10 ,[2 3]), [10 10 10;10 10 10] );
EXPECT_EQ( repmatm([1 2] ,2, 3), [1 2 1 2 1 2;1 2 1 2 1 2] );

%% sizem
[a,b,c] = sizem( zeros(2,0,1) );  %#ok<SIZEM>
EXPECT_EQ( a, 2 );
EXPECT_EQ( b, 0 );
EXPECT_EQ( c, 1 );

EXPECT_EQ( sizem(1), 1 );  %#ok<SIZEM>
EXPECT_EQ( sizem([1;2]), 2 );  %#ok<SIZEM>
EXPECT_EQ( sizem([1 2]), [1 2] );  %#ok<SIZEM>
EXPECT_EQ( sizem([]), [] );  %#ok<SIZEM>
EXPECT_EQ( sizem([1 2;3 4;5 6],1), 3 );  

%% sph2cartm % tested above
%% sph2cart2m % tested above
%% sph2sph % tested above
%% sph2sphm % tested above

%% squeezem
EXPECT_EQ( squeezem([2 1 3]),[2;1;3]);

%% summ
EXPECT_EQ( summ([2 3; -4 -5],[], 'abs'),14);
EXPECT_EQ( summ([2 3; -4 -5]), -4);
EXPECT_EQ( summ([2 3; -4 -5], 1),[-2 -2]);

%% upsamplem

[d,dmin] = upsamplem([1 2 ; 3 4], [0;0], [2 1; 0 -2],Inf);
EXPECT_EQ( d, [inf inf 1;2 inf inf;inf inf 3;4 inf inf] );
EXPECT_EQ( dmin, [0;-2] );

[d,dmin] = upsamplem( [1 2 ; 3 4], [], [2 1; 0 -2], Inf );
EXPECT_EQ( d, [inf inf 1;2 inf inf;inf inf 3;4 inf inf] );
EXPECT_EQ( dmin, [0;-2] );

EXPECT_EQ( upsamplem([0 1], 0, 2, inf, 1), [0 1] );

[d, dmin] = upsamplem( [], 0, 2 );
EXPECT_EQ( d, [] );
EXPECT_EQ( dmin, [0] );

[d, dmin] = upsamplem( [1 2 3]', 0, -1 );
EXPECT_EQ( d,[3 2 1]' );
EXPECT_EQ( dmin, -2 );

[d, dmin] = upsamplem( [1 2 3]', 0, 1 );
EXPECT_EQ( d,[1 2 3]' );
EXPECT_EQ( dmin,0);

[d, dmin] = upsamplem( [1 2;3 4], [0;0], [2 0;0 2] );
EXPECT_EQ( removezero(d,'border'), [1 0 2;0 0 0;3 0 4] );
EXPECT_EQ( dmin,[0;0] );

[d, dmin] = upsamplem( [1 2 3], [2 0], [-1 -2] );
if( numel(d)==6 );
    EXPECT_EQ( d, [0 3 0 2 0 1] );
    EXPECT_EQ( dmin, [-2;-5] );
elseif( numel(d)==5 );
    EXPECT_EQ( d, [3 0 2 0 1] );
    EXPECT_EQ( dmin, [-2;-4] );
else
    EXPECT_FAIL(); end;

[d, dmin] = upsamplem( [1 2 3], [0 2], -2 );
if( numel(d)==12 );
    EXPECT_EQ( d, [0 0 0 0 0 0;0 3 0 2 0 1] );
    EXPECT_EQ( dmin, [-1;-9] );
else;
    EXPECT_EQ( d, [3 0 2 0 1] );
    EXPECT_EQ( dmin, [0;-8] ); end;

[d, dmin] = upsamplem( [1 2 3;4 5 6], [0],  [2], [], 1 );
if( numel(d)==12 );
    EXPECT_EQ( d, [1 2 3;0 0 0;4 5 6;0 0 0] );
    EXPECT_EQ( dmin, 0 );
else;
    EXPECT_EQ( d, [1 2 3;0 0 0;4 5 6] );
    EXPECT_EQ( dmin, 0 ); end;

[d, dmin] = upsamplem( [1 2 3;4 5 6], [0], [-2], [], 1 );
if( numel(d)==12 );
    EXPECT_EQ( d, [0 0 0;4 5 6;0 0 0;1 2 3] );
    EXPECT_EQ( dmin, -3 );
else;
    EXPECT_EQ( d, [4 5 6;0 0 0;1 2 3] );
    EXPECT_EQ( dmin, -2 ); end;

EXPECT_THROW( 'upsamplem( [1 2 3], [0;0] );', 'upsamplem.m' );
EXPECT_THROW( @() upsamplem([1 2 3],[2;2],[2 0;0 2],[1 0]), 'upsamplem.m' );

[c, idx] = upsamplem( 1, [0;0], [2 0;0 2], 0, true );
EXPECT_EQ( idx, [0;0] );
EXPECT_THAT( c, tt.NnzIs(1) );

[c, idx] = upsamplem( 1, [0;0], [2 0;0 2], 0, false );
EXPECT_EQ( idx, [0;0] );
EXPECT_THAT( c, tt.NnzIs(1) );


%% zerosm
EXPECT_EQ( zerosm(), []) ;
EXPECT_EQ( zerosm([]), [] );
EXPECT_EQ( zerosm([1,2]), [0 0] );
EXPECT_EQ( zerosm(3), [0;0;0] );
EXPECT_EQ( zerosm(1, 1, 2), permute([0; 0], [3 2 1]) );

%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*NBRAK1,*NBRAK2,*SIZEM>
