% test sequence

%#ok<*CTPCT>
%#ok<*ASGLU>
%#ok<*NASGU>

TTEST init ipa_complex_sym

ASSUME_TOOLBOX( 'symbolic', OPTION('load') );
EXPECT_TOOLBOX( 'matlab', '>=R2016b', APPLIED('onmatlab') );
EXPECT_TOOLBOX( 'octave', '>=6.4', APPLIED('onoctave') );

optim_inst = tt.expect_toolbox( 'optimization' );
sedumi_inst = tt.expect_toolbox( 'sedumi' );
gurobi_inst = tt.expect_toolbox( 'gurobi' );
assert( sedumi_inst && optim_inst || gurobi_inst, 'At least, either SeDuMi or Gurobi must be installed.' );

%% polytopenorm
TESTCASE
pt = [1;0;2];
VV = [1;0;0];
polytopenorm( pt, VV, 'c2', 'lb',sym(1), 'v',-1 ); 

EXPECT_NTHROW( @() polytopenorm( [.2;.3-.1i;.1], [0.5; .3i;.25], 'c2', 'm' ) );
[~,nrm] = EXPECT_NTHROW( @() polytopenorm( [.2;.3-.1i;.1], [0.5; .3i;.25], 'c4', 'm' ) );
EXPECT_PRED( @(x) ~isfinite(x), nrm(2) );

[~, nrm] = EXPECT_NTHROW( @() polytopenorm( [0;1i;1], [1; 1i;0], 'c2', 'm' ) );
EXPECT_STREQ( @() polytopenorm( [0;1i;1], [1; 1i;0], 'c2', 'm', 'v',-1 ), '' );
EXPECT_STREQ( @() polytopenorm( [.2;.3-.1i;.1], [0.5; .3i;.25], 'c2', 'm', 'v',-1 ), '' );

p = [-2+1i 1-1i].';
VV = [2 -1-1i;-1-2i -1].';
nrm = double( polytopenorm( p, VV, 'cns', 'v',-1 ) );
EXPECT_LE( nrm(3), nrm(1), nrm(2) );


%%% underflow tests
solver = {};
if( gurobi_inst );
    solver = [ solver 'g']; end;
if( optim_inst );
    solver = [ solver 'm']; end;
MESSAGE( 'sol', 'alg' );
for sol = solver;
    sol = sol{1};  %#ok<FXSET>
    for alg = {'c1','c2','c3','c4','c5'}
        alg = alg{1};  %#ok<FXSET>
        p = [0;0];
        VV = round( 100*randncp( 2, 5 ) );
        [check, nrm] = EXPECT_NTHROW( @() polytopenorm( p, VV, [alg 'S'], sol, 'v',-1 ), 'polytopenorm:unimplemented' );
        if( ~isempty(nrm) );
            EXPECT_EQ( nrm(3), sym(0) ); end;
        end; end;

%% ipa
TESTCASE
SECTION
    A = [ 1   -1  0; 1  1     0; 0   0  0.25];
    B = [ 0.5  0  0; 0  1    -1; 0   1  1];
    C = [-1    0 -1; 0  0.75  0; 1   0 -1];
    EXPECT_NTHROW( @() ipa( {A,B,C}, 'v',2, 'admissiblealgorithm',{'c','c1','c2','c3','c4','c5'}, 'plot','polytope', 'epssym',1e-5 ) );
    EXPECT_THROW( @() ipa( {A,B,C}, 'addlimitmatrix',1, 'epssym',1e-5, 'ncdelta',1), 'ipa:unkown_option', 'parsem:unknown' );  % When 'addlimitmatrix' is added again, this is a _NTHROW test

    A = [ -8325854551438777/18014398509481984,  -1371732554537271/2251799813685248
           7873947122517161/18014398509481984,  -2106884015594867/4503599627370496];
    B = [  5805556464020791/144115188075855872,  1836273638859193/2251799813685248
          -2999026326278621/144115188075855872,  2833225208504457/36028797018963968];
    EXPECT_THROW( @() ipa( {A,B}, 'epssym',5e-5, 'addlimitmatrix',1, 'maxiteration',2, 'maxsmpdepth',5 ), 'ipa:unkown_option', 'parsem:unknown' );  % When 'addlimitmatrix' is added again, this is a _THROW test, expecting 'makecandidate:limitmatrix'

    MESSAGE( 'dim' );
    for dim = 2:5;
        x = sym( randn(dim, 1) + 1i*randn(dim, 1) );
        y = sym( randn(dim, 1) );
        val = sym( rand );
        EXPECT_EQ( polytopenorm( x, [val*x y], 'cS', 'v',0, 'output','ub' ).*val, sym([1]) );
        EXPECT_EQ( polytopenorm( x, [val*x], 'cS', 'v',0, 'output','ub' ).*val, sym([1]) );
        EXPECT_EQ( polytopenorm( x, -x, 'cS', 'v',0, 'output','ub', 'bound',[0 1] ), sym([1]) );
    end

%%
% XX COMPLEXFUNCT_B terminates, but others do not
SECTION
    T{1} = [
      -5.889471853501820  -1.970688309111238  -5.630001102033487  -2.134107558660932  -0.938640820995035  -2.487061344257762
       5.071657210650709  -0.989430404138958   1.901900057387121  -2.216075231882621  -1.818826148702658  -0.393716026511971
       0.343739182972231  -3.887130549831923  -1.368677936849846  -3.745190294358782  -0.680550516431704  -0.570437342693295
      -2.715542946373447  -0.042246390250672   1.473973118263827   0.911008006927947   2.335541244916852   0.870511052969737
      -0.119131164442341  -2.553294528955973   0.596955146405897  -3.386121672223856  -1.144702127235217   1.894996417207507
       1.061722021065081   3.515406917240614   3.878492782459036   5.781584180102053  -3.294700233778078  -4.293725685202105];
    T{2} = [
      -0.365908284967353  -0.047691238353058   0.875959250475501   0.378018168662129  -0.333645684869110  -0.474521512990880
       1.572003625026727   0.065959441200210   0.242096657240805  -2.054660734733636  -3.107647138206332  -0.248692283983502
       0.110469146820022   0.314964264431425   0.423978874715854  -1.115277462488781  -1.101899870944965  -0.136558787313850
      -0.144322039429911  -0.202938600930496   0.505977181629081  -0.014865618118077  -1.577102317631012  -0.533003452293104
       0.505026932102296   0.871209364877234  -0.572191342079327   1.223053272310250   2.584928997760822  -0.077133318684067
       1.079901551971576   0.385709636970533   1.523364032564718  -1.503352361318295  -1.580208165770239  -0.410572593335098];

    % Missing Test

%%
SECTION
    %%% example where the pt is included in the convex hull, but there exists a small ellipse in the set of vertices which is included in the convex hull of the point
    pt = [2 2i 0].';
    VV = [4 2i 2i;4 -2i 2i; 1 1i 0].';
    % plotm( pt, 'funct','c', 'r-' ); plotm( VV, 'funct','c', 'hold',2, 'k-' );
    nrm = polytopenorm( pt, VV, 'cS', 'bd',sym([0 1]), 'v',0 );
    EXPECT_LE( nrm(2), 2 );
    if( vercmp( ttoolboxes('version'), '2.2025.03', '>' ) );  % it is known that the algorithm does not return the correct value for this example currently. Thus we disable this test, till the new version
        TODO_LE( nrm(2), 1 ); end;
    


%% endtestsuite
ENDTESTCASE();

%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*NBRAK,*NBRAK1,*NBRAK2>
