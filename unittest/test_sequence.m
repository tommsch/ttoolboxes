% test sequence

TTEST hardinit sequence
% preconditions

%% addsequence
TESTCASE
%EXPECT_EQ( addsequence([1 2],[4;5],[1;1],[1;0]), [4 1 2;5 0 0] );

%% characteristic
TESTCASE
EXPECT_NO_THROW( 'characteristic([1 2 3]'',''amin'',[-1;0;0]);' );
EXPECT_NO_THROW( 'characteristic([1 2 3]'');' );
EXPECT_EQ( characteristic([1 2 3]), [1;1;1] );
EXPECT_EQ( characteristic([1 2 3],'amin',[0]), [0;1;1;1] );
EXPECT_EQ( characteristic([1 2 3],'amin',[0],'amax',5), [0;1;1;1;0;0] );
[A,amin] = characteristic([1 0; 2 0; 1 -1]','amin',[-1; -1] );
EXPECT_EQ( A, [0 0;0 0;1 1;0 1] );
EXPECT_EQ( amin, [-1;-1] );
EXPECT_EQ( characteristic([1 0; 2 0; 1 -1]','amin',[1; -1],'amax',[2;2]), [1 1 0 0;0 1 0 0] );
D1 = [0 0;2 2; 0 1;1 2;0 2]';
[chi,amin,idx] = characteristic( D1 );
D2 = supp( chi, 2, amin ); 
D2 = D2(:,idx);
EXPECT_EQ( D1, D2 );

%% constructmu
TESTCASE
EXPECT_EQ( constructmu(2,2), [2 1 0;0 1 2] );

%% decompact
TESTCASE
% and also compact
B = sequence( [1 -1 0 1]', [0], 2 );
EXPECT_EQ( B.size, [2 2] );
EXPECT_EQ( B.sizei, 2 );

B = sequence( [1 -1 0 1]', [0], [2 1] );
EXPECT_EQ( B.size, [2 2] );
EXPECT_EQ( B.sizei, 2 );

B = sequence( [1 -1; 0 1]', [0], [1 2] );
EXPECT_EQ( B.size, [2 1 2] );
EXPECT_EQ( B.sizei, [1 2] );

C1 = [1 -1; 2 -2];
D = decompact( C1, 1, [1 2] );
EXPECT_EQ( D, cat( 3, [1;2], [-1;-2] ) );

A = [ 1    10   100     2    20   200     3    30   300     4    40   400
      5    50   500     6    60   600     7    70   700     8    80   800 ].';
dA = decompact( A, 2, 3 );
EXPECT_EQ ( compact(dA,2), A );

%% diffsequence
TESTCASE
EXPECT_NO_THROW( 'diffsequence([1 1 1]'',1);' );
val = diffsequence([1 2 3; 4 5 6],[1 0],'cell');
EXPECT_EQ( val{1}, [1 2 3; 3 3 3; -4 -5 -6] );
EXPECT_NO_THROW( 'diffsequence([1 2 3; 4 5 6],[1 0]);' );
EXPECT_NO_THROW( 'diffsequence([1 2 3; 4 5 6],1);' );
EXPECT_EQ( diffsequence([1 2 3; 4 5 6],1,'equalsize'), {[1 2 3 0;3 3 3 0;-4 -5 -6 0]; [1 1 1 -3;4 1 1 -6;0 0 0 0]} );


%% equalize
TESTCASE
[imin,imax] = equalize( [0 0],[1 2], [-1 0], [0 2] );  %#ok<*CMEQU>
EXPECT_EQ( imin, {[-1;0],[-1;0]} );
EXPECT_EQ( imax, {[1;2],[1;2]} );

[imin,imax] = equalize( [0 0],[1 2], [-1 0], [0 2], 'square' );
EXPECT_EQ( imin, {[-1;-1],[-1;-1]} );
EXPECT_EQ( imax, {[2;2],[2;2]} );

[imin,imax] = equalize( [0 0], [1 2], [-1 0], [0 2], 'D', [2 1] );
EXPECT_EQ( imin, {[0;-1],[-1;0]} );
EXPECT_EQ( imax, {[1;2],[2;2]} );

[imin,imax] = equalize( [0 0 2], [1 2 2], [1 0 1], [4 3 4], 'D', [3 1] );
EXPECT_EQ( imin, {[0;0;1],[1;0;1]} );
EXPECT_EQ( imax, {[1;2;4],[4;3;4]} );

[imin,imax] = equalize( [0 0 2], [1 2 2], [1 0 1], [4 3 4], 'D', [3 1;2 2] );
EXPECT_EQ( imin, {[0;0;1],[1;0;1]} );
EXPECT_EQ( imax, {[1;3;4],[4;3;4]} );

[imin,imax] = equalize( [0;0],[0;2],[-1],[1], 'D',[2 1] );
EXPECT_EQ( imin, {[0;-1],[-1]} );
EXPECT_EQ( imax, {[0;2],[2]} );


%% setidx
TESTCASE
EXPECT_NO_THROW( @() setidx( [1 2; 2 3], [1 1]', [0 0]' ) );
EXPECT_EQ( setidx( [1 2; 2 3], [1 1]', [0 0]', [2 3]' ), [0 0 0 0;0 1 2 0;0 2 3 0] );

%% supp
TESTCASE
EXPECT_EQ( supp([],2,[0]), zeros(2,0) );
EXPECT_EQ( supp([1 1; 0 1],2,[1;-1]), [1 1 2;-1 0 0] );
EXPECT_EQ( supp([1 2 3 4;2 3 4 5],1), [0 1] );
EXPECT_EQ( supp([1 2 3], 2, [2;0])  );
%% symbol2mask %tested above

