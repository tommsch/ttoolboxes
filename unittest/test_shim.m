% test cell

%#ok<*CTPCT>
%#ok<*ASGLU>
%#ok<*NASGU>

TTEST init test_shim

% preconditions    

%% upsample
TESTCASE( 'upsample' );
SECTION( 'general tests' );
    EXPECT_EQ( upsample( [1 2 3], 2 ), [1 0 2 0 3 0] );
    EXPECT_EQ( upsample( [1 2 3]', 2 ), [1;0;2;0;3;0] );
    EXPECT_EQ( upsample( [0], 2 ), [0;0] );
    EXPECT_EQ( upsample( [1 2 3;4 5 6], 3 ), [1 2 3;0 0 0;0 0 0;4 5 6;0 0 0;0 0 0] );
    EXPECT_EQ( upsample( reshape([1 2 3 1 2 3 1 2 3 1 2 3;4 5 6 4 5 6 4 5 6 4 5 6], [2 3 2 2]), 2 ), reshape([1 2 3 1 2 3 1 2 3 1 2 3;0 0 0 0 0 0 0 0 0 0 0 0;4 5 6 4 5 6 4 5 6 4 5 6;0 0 0 0 0 0 0 0 0 0 0 0], [4 3 2 2]) );
    EXPECT_EQ( upsample( [1, 2, 3], 1 ), [1, 2, 3] );
    EXPECT_EQ( upsample( [1, 2], 5 ), [1 0 0 0 0 2 0 0 0 0] );
    EXPECT_EQ( upsample( [1 2; 3 4], 1 ), [1 2; 3 4] );
    EXPECT_EQ( upsample( 5, 10 ), [5; zeros(9, 1)] );

    EXPECT_NTHROW( @() upsample( [], 2 ), 'signal:upsample:Nonempty' );
    EXPECT_THROW( @() upsample( [1 2 3], -1 ),     'MATLAB:upsample:expectedPositive', 'ttoolboxes:upsample:expectedPositiveInteger' );  % Negative upsampling factor is invalid
    EXPECT_THROW( @() upsample( [1 2 3], 0 ),      'MATLAB:upsample:expectedPositive', 'ttoolboxes:upsample:expectedPositiveInteger' );  % Zero upsampling factor is invalid
    EXPECT_THROW( @() upsample( [1 2 3], 2.5 ),     'MATLAB:upsample:expectedInteger', 'ttoolboxes:upsample:expectedPositiveInteger' );  % Non-integer upsampling factor is invalid
    EXPECT_THROW( @() upsample( {'a', 'b', 'c'}, 2 ), 'MATLAB:cast:UnsupportedPrototype', 'MATLAB:zeros:invalidStringInput', 'MATLAB:cast:UnsupportedClass' );  % Cell array instead of numeric
    EXPECT_THROW( @() upsample( [1 2 3], 'two' ),   'MATLAB:upsample:invalidType', 'MATLAB:nonLogicalConditional' );  % factor as a string

%% pdist2
TESTCASE( 'pdist2' );
SECTION( 'general tests' );
    EXPECT_ALMOST_EQ( pdist2( [1 2 3;2 3 4], [3 4 5;4 5 6] ), sqrt( [12 27;3 12] ) );
    EXPECT_ALMOST_EQ( pdist2( [1 2 3], [4 5 6] ), sqrt( [27] ) );
    EXPECT_ALMOST_EQ( pdist2( [1 2 3]', [4 5 6]' ), [3 4 5;2 3 4;1 2 3] );
    EXPECT_ALMOST_EQ( pdist2( 1, 1 ), 0 );

SECTION( 'chatpgpt tests' );
    EXPECT_ALMOST_EQ( pdist2( [1 2; 3, 4], [5, 6; 7, 8] ), [sqrt(32), sqrt(72); sqrt(8), sqrt(32)] );
    %EXPECT_ALMOST_EQ( pdist2( [1 2], [4, 6], 'euclidean' ), sqrt(25) );
    %EXPECT_ALMOST_EQ( pdist2( [1 2; 3 4], [5 6;  7 8], 'cityblock' ), [8, 12; 4, 8] );
    %EXPECT_ALMOST_EQ( pdist2( [1 0; 0 1], [1 1; -1 -1], 'cosine' ), [0.29289321881345254 1.7071067811865475;0.29289321881345254 1.7071067811865475] );
    %EXPECT_ALMOST_EQ( pdist2( [1 2; 3 4], [5 6;  7 8], 'chebychev' ), [4, 6; 2, 4] );
    %EXPECT_ALMOST_EQ( pdist2( [1 2; 3 4], [5 6;  7 8], 'minkowski',1 ), [8, 12; 4, 8]);  % Equivalent to 'cityblock'
    %EXPECT_ALMOST_EQ( pdist2( [1 2; 3 4], [5 6;  7 8], 'minkowski',3 ), [128 432; 16 128].^(1/3) );
    EXPECT_ALMOST_EQ( pdist2( [], [] ), [] );
    EXPECT_ALMOST_EQ( pdist2( zeros(2,0), zeros(3,0) ), [0 0 0;0 0 0] );
    EXPECT_ALMOST_EQ( pdist2( zeros(0,2), zeros(0,2) ), [] );
    
SECTION( 'chatpgpt corner cases tests' );
    EXPECT_ALMOST_EQ( pdist2( [1e10, 1e10], [-1e10, -1e10] ), sqrt( (2e10)^2 + (2e10)^2 ) );
    EXPECT_ALMOST_EQ( pdist2( [1e-10, 1e-10], [-1e-10, -1e-10] ), sqrt( (2e-10)^2 + (2e-10)^2 ) );
    EXPECT_ALMOST_EQ( pdist2( ones(1, 100), zeros(1, 100) ), sqrt( 100 ) );
    EXPECT_ALMOST_EQ( pdist2( [1 2 3], [1 2 3] ), 0 );
    EXPECT_ALMOST_EQ( pdist2( [0 0 0], [0 0 0] ), 0 );
    EXPECT_ALMOST_EQ( pdist2( [-1, -2, -3], [-4, -5, -6] ), sqrt( (3)^2 + (3)^2 + (3)^2) );
    EXPECT_ALMOST_EQ( pdist2( [1 2], [3 4; 5 6] ), [8 32].^(1/2) );
    EXPECT_ALMOST_EQ( pdist2( [3 4; 5, 6], [1 2] ), [8; 32].^(1/2) );
    EXPECT_ALMOST_EQ( pdist2( [1], [1] ), 0 );
    EXPECT_ALMOST_EQ( pdist2( [], [] ), [] );
    EXPECT_ALMOST_EQ( pdist2( [1 2], [3 4; 5 6] ), [8 32].^(1/2) );

    X = rand( 1, 1000 );
    Y = rand( 1, 1000 );
    EXPECT_NEAR( pdist2( X, Y ), sqrt( sum((X - Y).^2) ), max( eps([X Y]) ) * max(size(X)) );

SECTION( 'chatpgpt death tests' );
    EXPECT_THROW( @() pdist2( [1, 2, 3], [4, 5] ), 'stats:pdist2:SizeMismatch', 'ttoolboxes:pdist2:SizeMismatch' );  % X has 3 columns, Y has 2 columns
    EXPECT_THROW( @() pdist2( {'a', 'b'}, {'c', 'd'}), 'stats:pdist2:DataConversion', 'MATLAB:UndefinedFunction' );  % Non-numeric inputs
    EXPECT_THROW( @() pdist2( [1, 2; 3, 4], [5, 6; 7, 8], 'unsupported_metric' ), 'stats:pdist2:UnrecognizedDistance', 'ttoolboxes:shim:options' );
    EXPECT_THROW( @() pdist2( [1, 2; 3, 4], [5, 6; 7, 8], 'minkowski',-1 ), 'stats:pdist2:BadMinExp', 'ttoolboxes:shim:options' );  % Invalid exponent
    EXPECT_THROW( @() pdist2( [1, 2; 3, 4], [5, 6; 7, 8], 'minkowski','two' ), 'stats:internal:parseArgs:WrongNumberArgs', 'ttoolboxes:shim:options' );  % Non-numeric exponent
    EXPECT_THROW( @() pdist2( [1, 2; 3, 4] ), 'MATLAB:minrhs','stats:pdist2:TooFewInputs' );  % Only one argument provided
    EXPECT_THROW( @() pdist2( [1, 2; 3, 4], [5, 6; 7, 8], 'euclidean',1, 'extra-argument' ), 'stats:internal:parseArgs:IllegalParamName', 'ttoolboxes:shim:options' );  % Extra argument

%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*NBRAK2>
 
