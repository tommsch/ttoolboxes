%#ok<*CTPCT>
%#ok<*ASGLU>
%#ok<*NASGU>

TTEST init gallery_matrixset_sym

ASSUME_TOOLBOX( 'symbolic', OPTION('load') );

%% gallery_matrixset
TESTCASE

%%
SECTION( 'sym' );
    T = gallery_matrixset( 'rand', 'dim',5, 'J',2 );
    EXPECT_NPRED( @isempty, gallery_matrixset('cex') );

    
%#ok<*NOSEL,*NOSEMI,*ALIGN>
