%#ok<*CTPCT>
%#ok<*ASGLU>
%#ok<*NASGU>

TTEST init ipa


EXPECT_TOOLBOX( 'matlab', '>=R2016b',  APPLIED('onmatlab') );
EXPECT_TOOLBOX( 'octave', '>=6.4',     APPLIED('onoctave') );
EXPECT_TOOLBOX( 'sym', OPTION('load'), APPLIED('onoctave') );

sedumi_inst = tt.expect_toolbox( 'sedumi' );
gurobi_inst = tt.expect_toolbox( 'gurobi' );
optim_inst = tt.expect_toolbox( 'optimization' );
assert( optim_inst || sedumi_inst || gurobi_inst, 'At least, SeDuMi, the Optimization toolbox, or Gurobi must be installed.' );

% preconditions


%% blockjsr
TESTCASE
EXPECT_EQ( blockjsr([1 0],[.5 2]), [.5 2] );
EXPECT_EQ( blockjsr(1), 1 );
EXPECT_EQ( blockjsr(0,1), 1 );
EXPECT_EQ( blockjsr([0 1], 1), 1 );

EXPECT_THROW( 'blockjsr([1 2 3]);', 'blockjsr:input' );


%% chooseval
TESTCASE
EXPECT_EQ( [1 0 0], chooseval([inf 0 nan],1) );
EXPECT_PRED( @islogical, chooseval(randn(1,100), [10 50]) );
EXPECT_PRED( @islogical, chooseval(randn(1,100), [10 50]) );
EXPECT_EQ( chooseval([0 0 0 0 3 3]'), [0 0 0 0 1 1]' );
EXPECT_EQ( chooseval([1 1 1 4 4 4]', [0 0]), [0 0 0 0 0 0]');

%% codecapacity
TESTCASE
EXPECT_NTHROW( @() codecapacity( {[1 1],[-1 -1]}, 'v',-1 ), TODOED('onoctave') );


%% estimatejsr
TESTCASE
TT = {[1 2; 0 1],[1 2; 1 0]};
EXPECT_NO_THROW( 'val =  estimatejsr( TT );' );
EXPECT_NEAR( val(1), 2, 1e-9 );
EXPECT_LE( val(2), 2.5 );


%% extravertex
TESTCASE
V = [1 2 3;2 3 4]';
[Ve, VVe] = extravertex( V );
EXPECT_EQ( [V Ve],VVe );
EXPECT_GE( min(svd(VVe)), .01 );

[Ve, VVe] = extravertex( randn(10), 't',2000 );
EXPECT_EQ( size(Ve, 2),10 );

[Ve, VVe] = extravertex( randn(10), 't', 0 );
EXPECT_EQ( size(VVe, 2),10 );

[Ve, VVe] = extravertex( V, 'type','p' );
EXPECT_EMPTY( Ve  );

EXPECT_EQ( eye(3), extravertex( zeros(3, 0), 'type','p' ) );
EXPECT_EQ( eye(3), extravertex( zeros(3, 0), 'type','r' ) );

V2 = [1 1 0]';
[Ve,VVe] = extravertex( V2, 'type','p' );
EXPECT_EQ( Ve/max(Ve), [0;0;1] );

[Ve, VVe] = extravertex( V2, 'type','p', 'cell' );
EXPECT_PRED( @iscell, Ve );
EXPECT_PRED( @iscell, VVe );

M = {[1 2;1 2], [2 4;2 4]};
Mb = {[2 4;2 4], [4 8;4 8]};
oo = {[1 2 2 1 1 2 1]'};
v0 = {[1 0]'};
Ve1 = extravertex( M, oo, v0 );
Ve1a = extravertex( M, oo, v0 ,'s', 2 );
Ve1b = extravertex( Mb, oo, v0 );
EXPECT_EQ( Ve1a, Ve1b );
EXPECT_EQ( size(Ve1, 2), 1 );

M = {[1 2;1 2], [2 4;2 4]};
oo = [1 2 2 1 1 2 1]';
v0 = [1 0]';
Ve2 = extravertex( M, oo, v0 );
EXPECT_EQ( size(Ve2, 2), 1 );
EXPECT_EQ( Ve1, Ve2 );

VV = [10000 0 0;10000 0 1;10000 1 0;10000 1 1]';
Ve = extravertex( VV );
EXPECT_EQ( size(Ve, 2), 2 );



%% findsmp
TESTCASE
allfunctionhandle( 'findsmp', {'makePtot_idx', 'makePtot_bf', 'makePtot_nl', 'computeNtRt_base', 'computelbub_smaxp', 'computelbub_sminp', 'choosematrix_lhr', 'simplify_ordering'}, 'assignin', 'v',0 );

%%% makePtot_idx
SECTION
% [ Pt, ot ] = makePtot_idx( M, P, o, idx )
[ot, Pt] = makePtot_idx( {[2] [3]}, [], {[2] [3]}, {[1] [2]}, logical([0 1]) );
EXPECT_EQ( Pt, {[6] [9]} );

[ot, Pt] = makePtot_idx( {[2] [3]}, [], {[2] [3]}, {[1] [2]}, logical([0 0]) );
EXPECT_EMPTY( Pt );

%%% makePtot_bf( M, ~, o, ~ )
SECTION
[ot, Pt] = makePtot_bf( {[2] [3]}, [], [], [], 2, 2, [] );
EXPECT_EQ( Pt, {4 6 6 9} );

[ot, Pt] = makePtot_bf( {[2] [3]}, [], [], [], [], 1, [] );
EXPECT_EQ( Pt, {2 3} );

ot = makePtot_bf( {1 2 3 4}, [0 1 0 1;1 0 1 0;0 1 0 1;1 0 1 0], [], [], [], 4, true );
EXPECT_NUMEL( ot, 32 );

%%% makePtot_nl( M, ~, o, ~ )
SECTION
[ot, Pt] = makePtot_nl( {[2] [3]}, [], [], [], [], 2, [] );
EXPECT_EQ( Pt, {4, 6, 9} );

[ot, Pt] = makePtot_nl( {[2] [3]}, [], [], [], [], 1, [] );
EXPECT_EQ( Pt, {2 3} );

ot = makePtot_nl( {1 2 3 4}, [0 1 0 1;1 0 1 0;0 1 0 1;1 0 1 0], [], [], [], 3, true );
EXPECT_NUMEL( ot, 10 );

%%% computeNtRt_grip
SECTION
%[ Nt, Rt ] = computeNtRt_base( Pt, ot, lb, normfun, rhofun );
[Nt, Rt] = computeNtRt_base( {[2] [-2]}, {[1],[1;1]}, @norm, @rho );
EXPECT_ALMOST_EQ( Nt, [2 sqrt(2)] );
EXPECT_ALMOST_EQ( Rt, [2 sqrt(2)] );

[Nt, Rt] = computeNtRt_base( {[1 2;1 2]}, {[1]}, @(x) norm(x, 1), @(x) norm(x, inf) );
EXPECT_ALMOST_EQ( Nt, 4 );
EXPECT_ALMOST_EQ( Rt, 3 );

[Nt, Rt] = computeNtRt_base( repmat({1}, [1 1000]), repmat({1}, [1 1000]), @norm, @rho );
EXPECT_ALMOST_EQ( Nt, ones(1,1000) );

%%% computelbub_smaxp
SECTION
%[ lb, ub ] = computelbub_smaxp( lb, ub, Nt, Rt, delta_now );
[lb, ub] = computelbub_smaxp( 10, 100, [], [], [30 40 50], [0 10 20], [], 1, [] );
EXPECT_EQ( lb, 20 );
EXPECT_EQ( ub, 50 );

%%% simplify_ordering
SECTION
EXPECT_EQ( simplify_ordering( {[1],[1 1],[1]} ), {[1]} );
[~, o, idx] = EXPECT_EQ( simplify_ordering( {} ), {} );
val = simplify_ordering( {[]} );
EXPECT_TRUE( isequal(val, {[]}) || isequal(val, {zeros(0,1)}) );

%%% findsmp
SECTION
AA = {[1 -1; 3 -2], [1 3; -1 -1]};
EXPECT_NO_THROW( @() findsmp( AA, 'v',-1 ) );

AA = {[1 1;0 1], [0 0;2 0]};
[~, c] = EXPECT_NTHROW( @() findsmp( AA, 'v',-1, 'maxsmpdepth',2 ) );
EXPECT_EQ( c, {[1;2]} );


%% intersectinterval
TESTCASE
EXPECT_EQ( intersectinterval([4 5],[1 4],[-inf 4]), [4 4] );

EXPECT_EQ( intersectinterval(), [-inf inf] );
EXPECT_EQ( intersectinterval( [0 0]), [0 0] );
EXPECT_EQ( intersectinterval( [0 0], [1 1]), [1 0] );
EXPECT_EQ( intersectinterval( [-1 0], [1 2], 'failsafe','minmax' ), [-1 2] );
EXPECT_EQ( intersectinterval( [-1 0], [1 2], 'failsafe','maxmin' ), [0 1] );
EXPECT_EQ( intersectinterval( [1 2], 2), [2 2] );
EXPECT_EQ( intersectinterval( [1 2], 1), [1 1] );
EXPECT_EQ( intersectinterval( [0 5], 1, [1 3]), [1 1] );
EXPECT_EQ( intersectinterval( [0 5], 6), [6 5] );
EXPECT_EQ( intersectinterval( [0 5], [6 7]), [6 5] );
EXPECT_THROW( @() intersectinterval( [-1 0], [1 2], 'failsafe','wrongoption' ), 'intersectinterval:option' );
EXPECT_EQN( intersectinterval( [3 5;3 4], [1 9;1 2], 'nan' ), [3 5;nan nan] );

%% invariantsubspace
TESTCASE
[M, B] = invariantsubspace( {[1 1 1; 1 0 1; 0 1 0], [1 -1 0; 0 2 0; 1 1 2]}, 'v',-1 );
EXPECT_EQ( size(M,2), 3 );

T{1} = [
 0.506218339278890   0.862408901256391                   0
-0.862408901256391   0.506218339278890   0.391914359514002
                 0                   0   0.170743918280893];
T{2} = [
-0.897844141915492  -0.440286513093771  -0.032525024390464
 0.440286513093771  -0.897844141915492                   0
                 0                   0   0.916839611307475];
[TT, B] = invariantsubspace( T, 'v',0 );
EXPECT_NUMEL( TT, 3 );

%% ipa
TESTCASE

EXPECT_NO_THROW( @() ipa( {[1 1;1 0],[1 -1; 1 1]}, 'v',0 ) );

c = {[1;2;2]};
[~, ~, nfo] = EXPECT_NTHROW( @() ipa( {randn(10),randn(10)}, 'oo',[c c], 'smpflag', [2 2], 'v0',{randn(10, 1) randn(10, 1)}, 'v',1, 'dryrun', 'balancing',-1, 'JSR',[0 1000] ), ...
                             'constructU:donotuse' );
EXPECT_LE( nfo.param.complexeigenvector, 2 );

[~, r] = EXPECT_NTHROW( @() ipa( {[2 2;2 2], [1 3;0 5]}, 'admissiblealgorithm','p' ) );
EXPECT_EQ( r, 5 );

%% leadingeigenvector
TESTCASE
SECTION( 'general' );
    MM = [1 2;1 1];
    [v0, v0s, mult, cpflag, M] = leadingeigenvector( MM, 'v',-1 );
    EXPECT_EQ( cpflag, 0 );
    EXPECT_EQ( M{1}, MM );
    EXPECT_ALMOST_EQ( v0{1}, 1/sqrt(3)*[sqrt(2);1] );
    EXPECT_ALMOST_EQ( v0s{1}, [1;sqrt(2)]/dot(v0{1},[1;sqrt(2)]) );
    EXPECT_EQ( mult, 1 );

SECTION( 'regressiontests' );
    X = [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
         0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
         0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
         0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
         0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
         0 0 1 0 0 0 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 0;
         0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
         0 3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 0 0 0 0 0 0 0;
         0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
         0 1 0 0 0 0 0 0 0 0 5 0 0 0 0 0 0 0 0 0 0 0 0 0;
         0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
         0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
         0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 0 0 0 0 0 0 1 0 0;
         0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
         0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
         0 0 0 0 0 0 0 5 0 0 0 0 0 0 0 0 0 2 0 0 0 0 2 0;
         0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
         0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
         0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
         0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
         0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
         0 0 0 0 0 0 0 0 6 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
         0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 1 0 0 0 0;
         0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;];
    [~, v0] = EXPECT_NTHROW( @() leadingeigenvector( X ) );
    if( vercmp( ttoolboxes('version'), '>=2.2025.03' ) );  % it is known that the algorithm does not return the correct value for this example currently. Thus we disable this test, till the new version
        TODO_EQ( numel(v0), 17 );  end;  % 17 is correct. Checked with Mathematica
    
    
    X = zeros( 100 );
    X([1 2],:) = 1;
    X(:,[end-1 end]) = 1;
    v = leadingeigenvector( X );
    EXPECT_NUMEL( v, 1 );
    EXPECT_TRUE( isreal(v{1}) );

    A = [0.44868265413621788+0.73210641324767622i -0.25649177669999834-0.41851244512418967i;0.66467181636908834+1.0845315614119075i -0.041301388755662326-0.067390640813308644i];
    EXPECT_STREQ( @() leadingeigenvector( A ), '' );

    At = [-0.15892544259255201+0.38735466570321608i -0.53283764083845342-0.688146172329545i;-0.015207067036715698+0.46343509952122225i 0.68183292864432732-0.014745111314095448i];
    EXPECT_NTHROW( @() leadingeigenvalue( At ) );
    [~, vec] = EXPECT_NTHROW( @() leadingeigenvector( At, 'nocell' ) );
    EXPECT_NEAR( diff( At*vec./vec ), 0, 1e-12 );

%% makecandidate
TESTCASE
allfunctionhandle( 'makecandidate', 'assign', 'private',0, 'verbose',0 );
param = struct;
param.verbose = -1;
A = {[1 1 1;0 1 -1;1 -1 1],[0 0 1;1 1 0;-1 -1 1]};
param = initialize( A, param );

SECTION( 'select_smp_candidates' );
    ct = select_smp_candidates( A, param, '' );
    EXPECT_EQ( ct.ordering, {[1 1 1 1 1 1 1 1 2]'} );
    EXPECT_EQ( ct.smpflag, [0] );

    param.findsmp.nearlycanddelta = .99;
    ct = select_smp_candidates( A, param, '' );
    EXPECT_EQ( sort(cellfun( 'prodofsize', ct.ordering )), [6 7 8 9] );

SECTION( 'makecandidate' );
    param.findsmp.nearlycanddelta = .99;
    param.autoextravertex = .5;

    ct = makecandidate( A, param );
    EXPECT_EQ( sort(cellfun( 'prodofsize', ct.ordering )), [0 0 6 7 8 9] );
    EXPECT_NEAR( ct.v0{1}, [0.436502094499892  -0.379326931864834   0.815829026364727].', 6e-16 );
    EXPECT_NEAR( ct.v0s{1}, [0.713872150387829  -0.119913982090577   0.788040983416833].', 6e-16 );
    EXPECT_EQ( nnz(ct.smpflag == ipa_constant.candidate), 1 );


    param.ordering = {[1 2 2 2 2 2 2 2]'};
    param.complexeigenvector = 2;
    param.epssym = 0;
    ct = makecandidate( A, param );
    EXPECT_EQ( ct.ordering, {[1 2 2 2 2 2 2 2]',[]} );
    EXPECT_NEAR( ct.v0{1}, [0.534522483824849   0.133630620956212 + 0.517549169506766i  0.400891862868637 - 0.517549169506766i].', 1e-12 );
    EXPECT_EQ( ct.smpflag, [0 2] );

    param.v0 = {[1 0 0]'};
    EXPECT_THROW( @() makecandidate( A, param ), 'makecandidate:v0' );

    param.v0 = {3*[(15^(1/2)*1i)/6 + 1/2;  (15^(1/2)*1i)/6 - 1/2;  1]};
    ct = makecandidate( A, param );
    EXPECT_EQ( ct.v0{1}, param.v0{1} );

    param.v0s = {[1 0 0]'};
    EXPECT_THROW( @() makecandidate( A, param ), 'makecandidate:v0s' );

    param.v0s = {3*[(15^(1/2)*1i)/4 + 5/4; (15^(1/2)*1i)/2 - 1/2; 1]};
    EXPECT_THROW( @() makecandidate( A, param ), 'makecandidate:v0s' );

    param.v0s = {[(15^(1/2)*1i)/4 + 5/4; (15^(1/2)*1i)/2 - 1/2; 1]/(3*3.872983346207417)};
    ct = makecandidate( A, param );
    EXPECT_EQ( ct.v0s{1}, param.v0s{1} );


%% makeorderinggraph
TESTCASE
if( ismatlab );
    oo = [1 1 1 2 1 2 2 2 1 1;0 2 1 0 2 1 2 1 1 2;0 0 0 0 1 0 0 1 2 1];
    makeorderinggraph( oo, 'v',-1 );
    makeorderinggraph( oo, 'removeleaf', 'v',-1 );
    makeorderinggraph( zeros(10,1),  'v',-1 );
    makeorderinggraph( [],  'v',-1 );
else;
    TODO_FAIL( 'makeorderinggraph does not work on Octave currently.' ); end;

%% makepositive
TESTCASE
SECTION( 'general' );
    EXPECT_EQ( makepositive([ 0 -1 2]), [0 1 -2] );
    EXPECT_ALMOST_EQ( makepositive(1i), 1 );
    
    X = [-2 1; 1 2];
    EXPECT_EQ( makepositive(X), -X );
    EXPECT_EQ( makepositive(-X), -X );
    
    X = [-3 1; 1 2];
    EXPECT_EQ( makepositive(-X, [], 'rho'), -X );
    EXPECT_EQ( makepositive(X, [], 'rho'), -X );
    
    
    X = [-2 0; 0 1];
    EXPECT_EQ( makepositive(X, [], 'rho'), -X );
    EXPECT_EQ( makepositive(-X, [], 'rho'), -X );
    EXPECT_EQ( makepositive(-X), -X );
    EXPECT_EQ( makepositive(X), -X );
    
    X = [-1 2; -2 -1];
    EXPECT_EQ( makepositive(X, [], 'rho'), -X );
    
    X = nan;
    EXPECT_THAT( makepositive( X, [],'rho'), tt.NanSensitiveEq([nan]) );
    
    X = [1 -1;1 1];
    Y = makepositive( X, [], 'rho_strict' );
    EXPECT_SIZE( Y, [2 2] );
SECTION( 'regression' );
    A = [-0.50043724390214328 -1.4188733495932788;-0.73869326791601142 -0.3292393897343015];
    EXPECT_ALMOST_EQ( makepositive( A, [], 'rho_strict' ), -A );

%% matrixenum
TESTCASE
val = matrixenum( 2, 2, 46,'v',0 );
EXPECT_EQ( val, {[0 1; 0 0],[1 1; 1 0]} );

EXPECT_THROW( 'matrixenum( 1, 1, 2, ''v'',0 )', 'matrixenum:input' );
[~, num_t] = matrixenum( 3, 1, 433, true, 'v',0 );
EXPECT_LT( num_t, 433 );
[~,num_t] = matrixenum(3, 1, 283,true, 'v',0);
EXPECT_EQ( num_t, 283 );


%% partitionatepolytope
TESTCASE
if( ismatlab );
    EXPECT_NO_THROW( 'partitionatepolytope(randn(2,100),2,0);' );
    EXPECT_EQ( partitionatepolytope([1;0],2,0), {[0 1]} );
    EXPECT_EQ( partitionatepolytope([1 1;0 1],2,0), {[0 1;1 2]} );
    EXPECT_NO_THROW( 'partitionatepolytope( [randi(100,2,20) [0 0 0 0 0;0 0 0 0 0]],2,0);' );
    EXPECT_NO_THROW( 'partitionatepolytope( ones(2,100),2,0);' );
    EXPECT_NO_THROW( @() partitionatepolytope( zeros(3,0), 0, 0 ) );
else;
    TODO_FAIL( 'partitionatepolytope does not work on Octave currently.' ); end;

%% polytopenorm
TESTCASE
SECTION( 'general' );
    VV = [ -6    11    -8    -7     8     3     3     3    -4     5
            7   -17     3    -1   -15    10    -6    21    -3    13
           -3     8     1    13     2   -19     3    -9   -15    -8];
    pp = [-10    -3     7     9    -6    25    -7    12    -3    -7
            4    -3    -5    13     3    14     3     0    -6     8
           12    -2    -8    18   -16   -13    -8     6   -13     0];
    
    EXPECT_NO_THROW( @() polytopenorm(pp, VV, 'r') );
    if( gurobi_inst );
        EXPECT_NO_THROW( @() polytopenorm(pp, VV, 'r', 'g' ) ); end;
    if( sedumi_inst );
        EXPECT_NO_THROW( @() polytopenorm(pp, VV, 'r', 's' ) ); end;
    if( optim_inst );
        EXPECT_NO_THROW( @() polytopenorm(pp, VV, 'r', 'm' ) ); end;
    
    
    [~,val] = EXPECT_NO_THROW( @() polytopenorm( [0 3]', zeros(2,0), 'r' ) );
    EXPECT_EQ( val, [1;inf;0] );
    
    val = polytopenorm( [0 3].', [0 1;1 1].', 'r', 'v',-1 );
    EXPECT_NEAR( val(1), 3, 1e-12 );
%%
SECTION( 'estimates' );
    pts=[0.95105651629515264;0.58778525229247103];
    VV=[0.52573111211913359 0.95105651629515342 1.0633135104400497 0.58778525229247303 0.96743459101561446 0.53478468915649002 0.9534460451934843 0.52705201120729073 0.95140514386300912 0.52592382869887799 0.95110738037181441 0.52575922908918904 0.95106393726392624 0.52573521432977932 0.95105759899990527 0.52573171062360047 0.95105667425964735 0.52573119943975799 0.95105653934186163 0.5257311248590405 0.95105651965762217 0.52573111397786054 0.95105651678573033 0.52573111239031767 0.95105651636672683 0.52573111215869828 0.95105651630559485 0.52573111212490542 0.95105651629667576 0.52573111211997503 0.95105651629537458 0.52573111211925572 0.95105651629518451 0.52573111211915069 0.95105651629515664 0.52573111211913526;0.85065080835203988 0.58778525229247303 0.40614962029113288 0.81229924058226566 0.56128497072448158 0.84505539002318775 0.58391891331787327 0.84983444781984963 0.58722116103826649 0.85053170295556169 0.58770295248762838 0.85063343110888423 0.58777324491276794 0.85064827304643087 0.58778350043938321 0.85065043845593591 0.5877849967005514 0.85065075438492488 0.58778521500211378 0.85065080047834318 0.58778524685188249 0.85065080720328223 0.58778525149870109 0.85065080818443772 0.58778525217666278 0.85065080832758599 0.58778525227557588 0.85065080834847118 0.58778525229000711 0.85065080835151818 0.58778525229211265 0.85065080835196261 0.58778525229241974 0.85065080835202744 0.58778525229246448 0.85065080835203677];
    nrm_e = polytopenorm( pts, VV, 'p', 'estimate',1, 'v',0 );
    EXPECT_GT( nrm_e(2,:), 1 );
    EXPECT_LT( nrm_e(3,:), 1 );

    pts=[0.95105651629515264;0.58778525229247103];
    VV2=[0.95105651629515342 1.0633135104400497;0.58778525229247303 0.40614962029113288];
    nrm_e = polytopenorm( pts, VV2, 'r', 'estimate',1, 'v',0 );
    nrm_n = polytopenorm( pts, VV2, 'r', 'v',0 );
    EXPECT_GT( nrm_e(2,:), 1 );  % I do not know whether `1` is really the correct value. It could also be approx. 1-1.332267629550188e-15.
    EXPECT_LT( nrm_e(3,:), 1 );


%% preprocessmatrix
TESTCASE
EXPECT_NO_THROW( @() preprocessmatrix( {[-1 2; 2 3],[-1 2; 2 3]}, 'v',-1 ) );
EXPECT_EQ( preprocessmatrix({-1}, 'v',-1), {1} );

EXPECT_NO_THROW( @() preprocessmatrix({[-1 2; 2 3],[-1 2; 2 3]}, 'v',-1, 'inverse',1, 'addinverse',1, 'transpose',1, 'addtranspose',1, 'makepositive',0 ,'timestep', .9, 'perturbate',.001, 'removezero', 0, 'removeduplicateeps',0, 'basechange','random', 'exponential',1 ) );
EXPECT_NO_THROW( @() preprocessmatrix({[-1 2; 2 3],[-1 2; 2 3]}, 'v',-1, 'basechange',1 ) );
EXPECT_NO_THROW( @() preprocessmatrix({[-1 2; 2 3],[-1 2; 2 3]}, 'v',-1, 'basechange',[1 1;1 -1]) );
EXPECT_EQ( preprocessmatrix({[-1 2; 2 3],[-1 2; 2 3]}, 'v',-1, 'makepositive',1 ), {[-1 2; 2 3],[-1 2; 2 3]} );

A = 1e10*randn( 4 );
B = A + 1e10*randn( 4 )*1e-7;
C = A;
D = A + 1e10*randn( 4 )*1e-14;
EXPECT_PRED( @(x) numel(x)==1, preprocessmatrix({A,B,C,D},'removeduplicateeps',1e-3, 'v',-1 ) );
EXPECT_PRED( @(x) numel(x)==2, preprocessmatrix({A,B,C,D},'removeduplicateeps',1e-10, 'v',-1 ) );
EXPECT_PRED( @(x) numel(x)==3, preprocessmatrix({A,B,C,D},'removeduplicateeps',1e-20, 'v',-1 ) );
EXPECT_PRED( @(x) numel(x)==1, preprocessmatrix({A,B,C,D},'removeduplicateeps',-1e5, 'v',-1 ) );
EXPECT_PRED( @(x) numel(x)==2, preprocessmatrix({A,B,C,D},'removeduplicateeps',-1e-1, 'v',-1 ) );
EXPECT_PRED( @(x) numel(x)==3, preprocessmatrix({A,B,C,D},'removeduplicateeps',-1e-5, 'v',-1 ) );

%% ipa_problemtype
TESTCASE
allfunctionhandle( 'ipa', 'ipa_problemtype', 'assign', 'private',1, 'verbose',0 );
EXPECT_EQ( 'p', ipa_problemtype( [], [1 2;2 3], [], 'verbose',-1 ) );
EXPECT_EQ( 'p', ipa_problemtype( 'p', [1 2;2 3], [], 'verbose',-1 ) );
EXPECT_EQ( 'r', ipa_problemtype( 'r', [1 2;2 3], [], 'verbose',-1 ) );
EXPECT_EQ( 'r', ipa_problemtype( 'p', [-1 2;2 3], [], 'verbose',-1 ) );


%% reducelength
TESTCASE
val = reducelength( {[2 1 2 2 1 2],[3 1]} );
EXPECT_EQ( val, {[1 2 2],[1 3]} );

EXPECT_EQ( reducelength({[]}, 0), {[]} );
EXPECT_EQ( reducelength({[2 1 2 2 1 2],[3 1]}, 0), {[1 2 2],[1 3]} );
EXPECT_EQ( reducelength([2 1 2 2 1 2], 1), [2 1 2] );
EXPECT_EQ( reducelength([2 1 2 2 1 2], 2), [1 2 2 1 2 2] );

in = [ones(1, 300) 2 2 2 2 1 1 1 1];
out = reducelength( in );
EXPECT_EQ( out, [ones(1, 304) 2 2 2 2] );

EXPECT_EMPTY( reducelength(0) );


%% removecombination
TESTCASE
val = removecombination( {[1 1 2],[1]} );
EXPECT_FALSE( size(val, 2)~=2 || ~searchincellarray(2, val, 1) || ~searchincellarray(1, val, 1) );

EXPECT_EQ( removecombination({[1 1 2],[1]},'add'), {[1 1 2],[1],[1 2],[2]} );


%% buildproduct_fast
TESTCASE
EXPECT_EQ( buildproduct_fast({2 4 5},[1 0 3]), 10 );

vv = [1;1];
val1 = buildproduct_fast( {[2 1; 1 2],[2 0;0 1]}, [1 0 2] );
val2 = buildproduct_fast( {[2 1; 1 2],[2 0;0 1]}, [1 0 2], vv );
EXPECT_ALMOST_EQ( val1*vv, val2 );

%% genNecklacest

MESSAGE( 'n', 'j' );
for n = [0 1 2 3];
    for j = [0 1 2 3];
        n_j = genNecklacest( n, j, 'jungers' );
        n_rs = genNecklacest( n, j, 'rs' );
        n_fkm = genNecklacest( n, j, 'fkm' );
        EXPECT_EQ( n_j, n_rs, n_fkm ); end; end;

%% jointTriangult
TESTCASE
M = [0.785211620918197   0.341788926627757   0.074143007303344   0.356491151983806
     0.265584257738676   1.290969309204150   0.420563191227518   0.521936881263907
     0.577587606917734  -0.440097243298557   1.144471401392911  -0.312389350198254
    -0.504169282542269   0.018492281649516  -0.388594019087334   0.779347668484741];
[ triang, block, B ] = jointTriangult( {M}, [] );
val = [block{:}];
EXPECT_NEAR( [val{:}], [1 1 1 1], 1e-6 );

%% liftproductt
TESTCASE
EXPECT_NTHROW( 'val = unique( cell2mat(liftproductt({2 3 5}, 2)) );' );
EXPECT_EQ( val, [1 2 3 4 5 6 9 10 15 25] );

%% liftsemidefinitet
TESTCASE
EXPECT_NTHROW( 'val = liftsemidefinitet({[1 1;0 1] [1 0; 1 0]}, 2);' );
EXPECT_EQ( val, {[1 4 4 2 4 1;0 1 2 1 3 1;0 0 1 0 2 1;0 0 0 1 2 1;0 0 0 0 1 1;0 0 0 0 0 1]; [ones(6,1) zeros(6,5)]} );

%% rhot
TESTCASE
TESTCASE( 'rh' );
    EXPECT_NO_THROW( 'val = rhot([1 1;1 1]);' );

    EXPECT_ALMOST_EQ( rhot(2), 2 );
    EXPECT_ALMOST_EQ( rhot([1 0;0 -2]), 2 );


    EXPECT_EQ( rhot({1 -2 -3}), [1 2 3] );
    C = {[2 3 4] [5 ; 5 ; 5 ] {3 7; 4 6}; 40 50 {60  80}};
    EXPECT_EQ( C, unflatten(flatten(C),C) );

    A = randn( 10 );
    A = A/max( abs(eig(A)) );
    EXPECT_NEAR( rhot(A), 1, 1e-10 );
    EXPECT_NO_THROW( 'rhot(sparse(randi(2,2)));' );
    EXPECT_EQ( rhot({[1], [1 1;1 1], sparse([1 0;0 0]), sparse([100]) }), [1 2 1 100] );
    EXPECT_NO_THROW( 'rhot([]);' );
    EXPECT_NO_THROW( 'rhot(sparse([]));' );
    EXPECT_NO_THROW( 'rhot({1, [1 1;0 1]});' );


%% end
ENDTESTCASE();

%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*NBRAK>

