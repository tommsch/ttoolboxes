% test tmiscR2019a

TTEST init tmisc_R2019a

ASSUME_TOOLBOX( 'symbolic', OPTION('load') );
ASSERT_TOOLBOX( 'matlab', '>=R2019a', APPLIED('onmatlab') );


if( ismatlab );
    syms xx; 
    assume( xx, 'real' );
else;
    syms xx real; end;


% preconditions

%% iswholenumber
% TESTCASE
% TODO_EQ( DISABLED, iswholenumber(floor(xx)), true );
% TODO_EQ( iswholenumber(ceil(xx)), true, OPTION('exec') );  % XX Reenable when Matlab gets smarter
% TODO_EQ( @() iswholenumber(round(xx)), true, OPTION('exec') );

%#ok<*NOSEL,*NOSEMI,*ALIGN>