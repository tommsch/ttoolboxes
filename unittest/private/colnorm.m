function A = colnorm( A );
    A = A./sqrt(sum(A.^2,1));
end