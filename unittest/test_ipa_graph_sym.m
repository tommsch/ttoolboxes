% test ipa_lsr

%#ok<*CTPCT>
%#ok<*ASGLU>
%#ok<*NASGU>

TTEST init test_ipa_graph_sym

EXPECT_TOOLBOX( 'matlab', '>=R2016b', APPLIED('onmatlab') );
EXPECT_TOOLBOX( 'octave', '>=6.4', APPLIED('onoctave') );

ASSUME_TOOLBOX( 'symbolic', OPTION('load') );

%%
TESTCASE( 'findsmp' );

SECTION( 'regression tests' );
    M{1} = sym( [[     1, 0];[ 1/100, 1]] );
    M{2} = sym( [[ 1, 1/10];[ 0,    1]] );
    M{3} = sym( [[ 1, 1/100];[ 0,     1]] );
    M{4} = sym( [[    1, 0];[ 1/10, 1]] );
    G = [0 0 1 1;1 1 0 0;1 1 0 0;0 0 1 1];
    EXPECT_NTHROW( @() findsmp( M, 'graph',G, 'maxsmpdepth', 2, 'sym',2 ), 'findsmp:graph', 'findsmp:numerical' );


    

ENDTESTCASE();



%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*NBRAK2>
