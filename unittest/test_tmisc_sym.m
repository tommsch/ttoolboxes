% test tmisc

%#ok<*NASGU>
%#ok<*CTPCT>
%#ok<*ASGLU>

TTEST hardinit tmisc_sym

ASSUME_TOOLBOX( 'symbolic', OPTION('load') );

if( ismatlab );
    syms zz;
    syms xx; 
    syms yy;
    assume( xx, 'real' );
    assume( yy, 'real' );
 else;
    syms zz;
    syms xx real;
    syms yy real; end;

% preconditions

%% anycast
TESTCASE
EXPECT_ISA( anycast( 2+eps, single(2), 'sym' ), 'sym' );
EXPECT_EQ( anycast( sym([]), 2, 'double' ), 2 );
EXPECT_EQ( anycast( sym([]), single(2), 3, '' ), single(2) );

EXPECT_EQ( anycast( 1.4, 'symrat'), sym(7)/sym(5) );
EXPECT_THROW( @() anycast( randn+1.41258233152, 'symrat'), 'anycast:badcast', 'OctSymPy:sym:rationalapprox' );
[~, ~, err] = EXPECT_NTHROW( @() anycast( 1.4125823152, 'symrat'), 'OctSymPy:sym:rationalapprox' );
EXPECT_TRUE( err );

EXPECT_NTHROW( @() anycast( 1, 'symrat') );
EXPECT_EQ( anycast( 1.41258233152, 'symrat', 'int32'), int32(1) );

EXPECT_EQ( anycast( [0 1], 'symrat', 'intval' ), sym([0 1]) );

[~, ret] = EXPECT_NTHROW( @() anycast(sym(2.5),2,'sym'), 'OctSymPy:sym:rationalapprox' );
EXPECT_EQ( ret, sym(5)/2 );

%% intersectspace
TESTCASE
[in, dim] = intersectspace( sym([1 0 0]'), [1 1 0; 1 -1 0]' );
EXPECT_EQ( double(dim), 1 );  % `double` is an Octave bugfix

%% invblkdiag
TESTCASE
EXPECT_EQ( invblkdiag(sym([1 0 0; 0 0 1;0 0 1])), {sym([1]),sym([0 1;0 1])} );  %#ok<NBRAK>

%% iseigenvector
if( ismatlab );
    A = randi( 99, 4 ) - 50;
    [v, d] = eig( sym(A) );
    EXPECT_TRUE( iseigenvector( A, v(:,1) ) );
    EXPECT_TRUE( iseigenvector( A, v(:,2) ) );
    EXPECT_FALSE( iseigenvector( A, randi(9,4,1) ) );

    A = randn( 6 );
    [v, d] = eig( vpa(A) );
    EXPECT_TRUE( iseigenvector( A, v(:,1) ) );
    EXPECT_TRUE( iseigenvector( A, v(:,2) ) );
    EXPECT_FALSE( iseigenvector( A, vpa(randn(6,1)) ) ); 
else;
    TODO_FAIL( 'iseigenvector tests on Octave fail currently.' ); end;

%% issym
TESTCASE
EXPECT_PRED( @issym, sym('23.2') );
EXPECT_NPRED( @issym, 3.2 );

%% issymstrict
TESTCASE
warning( 'off', 'OctSymPy:sym:rationalapprox' );
EXPECT_EMPTY( issymstrict(sym([])) );
EXPECT_EMPTY( issymstrict([]) );
EXPECT_EQ( issymstrict( [2 3] ), logical( [0 0] ) );
if( ismatlab );
    EXPECT_EQ( issymstrict( [sym(23.2) sym('23.2') 0] ), logical( [1 0 1] ) );
else;
    EXPECT_EQ( issymstrict( [sym(23.2) sym('23.2') 0] ), logical( [1 1 1] ) ); end;
    
%% isvpa
TESTCASE
warning( 'off', 'OctSymPy:sym:rationalapprox' );
EXPECT_EMPTY( isvpa(sym([])) );
EXPECT_EMPTY( isvpa([]) );
EXPECT_EQ( isvpa( [2 3] ), logical( [0 0] ) );
if( ismatlab );
    EXPECT_EQ( isvpa( [sym(23.2) sym('23.2') 0] ), logical( [0 1 0] ) );
else;
    EXPECT_EQ( isvpa( [sym(23.2) sym('23.2') 0] ), logical( [0 0 0] ) ); end;


%% iswholenumber
TESTCASE
syms xx;
EXPECT_NTHROW( @() iswholenumber(xx), 'symbolic:sym:isAlways:TruthUnknown', 'sym:isAlways' );
EXPECT_EQ( iswholenumber(sym(2)), true );
EXPECT_EQ( iswholenumber(sym(5)/2), false );

%% makepositive
TESTCASE
if( ismatlab );
    syms rr
    assume( rr, 'real' );
else;
    syms rr real; end;

EXPECT_EQ( makepositive(sym([ 0 -1 2])), sym([0 1 -2]) );
EXPECT_EQ( makepositive( -abs(rr)-1 ), abs(rr)+1 );
EXPECT_EQ( makepositive( [rr-rr -abs(rr)-1] ), [rr-rr abs(rr)+1] );
EXPECT_EQ( makepositive( [0 -abs(rr)-1] ), [0 abs(rr)+1] );
EXPECT_NTHROW( @() makepositive( rr ) );
[ret, a] = EXPECT_NTHROW( @() makepositive( -(rr*rr)-1 ) );
EXPECT_EQ( a, rr^2+1 );


%% matrixinfo
TESTCASE
SECTION
    syms w;
    A = {[[w -w];[0 0]], [[w -w];[0 0]], [1 1;2 2], [1 1;2 2]};  % strange format due to octave bug
    EXPECT_NTHROW( @() matrixinfo(A), 'symbolic:sym:isAlways:TruthUnknown', 'symbolic:sym:max:InputsMustBeConvertibleToFloatingPointNumbers' );

SECTION
    warning( 'off', 'OctSymPy:sym:rationalapprox' );
    syms rr
    syms yy
    syms zz
    if( ismatlab );
        assume( xx, 'real' );
    else;
        syms xx real; end;

    EXPECT_FALSE( matrixinfo( xx, 'pm1' ) );
    EXPECT_FALSE( matrixinfo( zz, 'bool' ) );
    EXPECT_TRUE( matrixinfo( sym([1 2;2 1]), 'sym') );

    T = {sym([1+1i 1i]),sym([1+2i 1]),sym([1-1i -1i]),[-1-1i 1i],[-1+1i -1i]};
    EXPECT_EQ( matrixinfo( T, 'cmirror' ), [1 0 1 0 0;0 1 0 0 0;1 0 1 0 0;0 0 0 1 1;0 0 0 1 1] );

    T = {[xx+1i*1], [xx-1i*1], [xx]};  %#ok<NBRAK>
    EXPECT_EQ( matrixinfo( T, 'cmirror' ), [1 1 0;1 1 0;0 0 1] );

    T = {[0 xx], [0 -xx], [0 yy], [0 conj(-yy)], [0 zz], [0 conj(zz)]};
    EXPECT_EQ( matrixinfo( T, 'cmirror' ), [1 1 0 0 0 0;1 1 0 0 0 0;0 0 1 1 0 0;0 0 1 1 0 0;0 0 0 0 1 1;0 0 0 0 1 1] );

    EXPECT_EQ( matrixinfo( {vpa(2) 2/(sym(5)^(1/2) + 1)}, 'symstrict' ), [0 1] );

    EXPECT_FALSE( matrixinfo( sym(-1), 'vandergraft' ) );

    %YY more tests for matrixinfo

SECTION( 'regression tests' );
    vv=cell([1 2]);
    vv{1}=str2sym('[0; (6625*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 5)^2)/621108 - (155*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 5)^3)/931662 - (11*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 5)^4)/1863324 + (8848*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 5))/51759 - 118696/17253; (65279*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 5)^2)/9937728 - (5993*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 5)^3)/29813184 - (157*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 5)^4)/29813184 + (672965*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 5))/3312576 - 64841/17253; (47713*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 5)^2)/4968864 - (1447*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 5)^3)/14906592 - (107*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 5)^4)/14906592 + (130435*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 5))/1656288 - 65378/17253; root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 5)/18 - 1; 1]');
    vv{2}=str2sym('[0; (6625*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 4)^2)/621108 - (155*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 4)^3)/931662 - (11*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 4)^4)/1863324 + (8848*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 4))/51759 - 118696/17253; (65279*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 4)^2)/9937728 - (5993*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 4)^3)/29813184 - (157*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 4)^4)/29813184 + (672965*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 4))/3312576 - 64841/17253; (47713*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 4)^2)/4968864 - (1447*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 4)^3)/14906592 - (107*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 4)^4)/14906592 + (130435*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 4))/1656288 - 65378/17253; root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 4)/18 - 1; 1]');
    matrixinfo( vv, 'conjugate' );

    M=cell([1 2]);
    M{1}=str2sym('[1, 1; 0, 1]');
    M{2}=str2sym('[0.74932654633036755794396194809134, 0; 0.74932654633036755794396194809134, 0.74932654633036755794396194809134]');
    EXPECT_NTHROW( @() matrixinfo( M ) );



%% mergeinterval
TESTCASE
EXPECT_NO_THROW( 'mergeinterval(sym([0 1.5 1 1.6 2 3.5 3 3 4 5]));', DISABLED('onoctave') );
    syms z
    X_s2 = [[          (6625*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 5)^2)/621108 - (155*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 5)^3)/931662 - (11*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 5)^4)/1863324 + (8848*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 5))/51759 - 118696/17253,           (6625*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 4)^2)/621108 - (155*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 4)^3)/931662 - (11*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 4)^4)/1863324 + (8848*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 4))/51759 - 118696/17253]
           [(65279*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 5)^2)/9937728 - (5993*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 5)^3)/29813184 - (157*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 5)^4)/29813184 + (672965*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 5))/3312576 - 64841/17253, (65279*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 4)^2)/9937728 - (5993*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 4)^3)/29813184 - (157*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 4)^4)/29813184 + (672965*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 4))/3312576 - 64841/17253]
           [(47713*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 5)^2)/4968864 - (1447*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 5)^3)/14906592 - (107*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 5)^4)/14906592 + (130435*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 5))/1656288 - 65378/17253, (47713*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 4)^2)/4968864 - (1447*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 4)^3)/14906592 - (107*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 4)^4)/14906592 + (130435*root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 4))/1656288 - 65378/17253]
           [                                                                                                                                                                                                                                                                                                     root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 5)/18 - 1,                                                                                                                                                                                                                                                                                                      root(z^5 - 4*z^4 - 2286*z^3 + 24912*z^2 + 1600641*z - 28387584, z, 4)/18 - 1]
           [                                                                                                                                                                                                                                                                                                                                                                                1,                                                                                                                                                                                                                                                                                                                                                                                 1]];
    X_d=[-0.0078659149988726737+0.22001612068225429i -0.0078659149988726737-0.22001612068225429i;0.22642874523468112+0.14002411662616154i 0.22642874523468112-0.14002411662616154i;0.084349575373435071-0.62207711516902175i 0.084349575373435071+0.62207711516902175i;0.66382196252367953 0.66382196252367953;-0.21228003252122823-0.028932782392635317i -0.21228003252122823+0.028932782392635317i;0 0];


    
%% nestedcellfun
TESTCASE
EXPECT_EQ( nestedcellfun( @sym, {5^(sym(1)/2)}, 'UniformOutput',false ), {5^(sym(1)/2)} );

%% randsymbolic
TESTCASE
warning( 'off', 'OctSymPy:sym:rationalapprox' );
EXPECT_PRED( @issym, randsymbolic );
EXPECT_PRED( @isnan, randsymbolic(1,'start',sym(nan),'nonan',0,'it',1) );
EXPECT_PRED( @(x) numel(x)==9, randsymbolic(3,'it',1) );
EXPECT_PRED( @(x) numel(x)==3, randsymbolic(3,'it',1,'legacy',0) );

%% buildproduct
TESTCASE
warning( 'off', 'OctSymPy:sym:rationalapprox' );
sol = 1/21*[11 10;11 10];
mat = {[0.8 0.2; 0.3 0.7],[0.4 0.6;0.5 0.5]};
if( ismatlab );
    EXPECT_ALMOST_EQ( buildproduct( mat,{[],[1 2]}, 'sym',1 ), sol ); 
else;
    TODO_FAIL( 'buildproduct does not work for symbolic input on Octave.' ); end;

%#ok<*NOSEL,*NOSEMI,*ALIGN>
