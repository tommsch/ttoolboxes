% test sequence

TTEST init sequenceclass

%% sequence
TESTCASE
EXPECT_NO_THROW( @() sequence( [1 2 3], [1 2]' ) );
EXPECT_EQ( ndims(sequence([1 2 3])), 2 );
EXPECT_EQ( ndims(sequence([1 2 3]')), 1 );
EXPECT_THROW( @() sequence([1 2 3 4],[1]) , 'sequence:dim' ); 

%% 0d
TESTCASE
c = sequence( [], [], [], 1 );
d = sequence( [], [], [], 2 );
EXPECT_EQ( c+d, sequence([],[],[],3) );

%% scalar1d
TESTCASE
c0 = sequence( [1 2 0 4]', 0 );
c1 = sequence( [-2 2 0 4]', 1 );
EXPECT_EQ( get_c(conv(c0,c1)), conv(get_c(c0),get_c(c1)) );

EXPECT_EQ( c0.ndims, 1 );
EXPECT_EQ( c0.ndimso, 1 );
EXPECT_EQ( c0.ndimsi, 0 );
EXPECT_EQ( c0.size, [4 1] );
EXPECT_EQ( c0.sizeo, 4 );
EXPECT_EQ( c0.sizei, 1 );
EXPECT_EQ( c0.idxmin, [0;1] );
EXPECT_EQ( c0.idxmino, 0 );
EXPECT_EQ( c0.idxmini, 1 );
EXPECT_EQ( c0.idxmax, [3;1] );
EXPECT_EQ( c0.idxmaxo, 3 );
EXPECT_EQ( c0.idxmaxi, 1 );

EXPECT_EQ( c0.ref(':,::'), c0 );
EXPECT_EQ( c0.ref('::,::'), c0 );
EXPECT_EQ( c0.ref('::'), c0 );
EXPECT_EQ( c0.ref('::,1'), c0 );
EXPECT_EQ( c0.ref('::,1,1,1'), c0 );
EXPECT_THROW( 'c0.ref(''::,2'')', 'MATLAB:badsubscript' );


%% scalar2d
TESTCASE
c0 = sequence( [1 2 0 4], [0 0] );
c1 = sequence( [-2 2 0 4], [1 1] );
c2 = sequence( [10 20 30 40], [-5; 0] );
c3 = sequence( [1 2 0 4], [1 0] );
c4 = c3;
c4 = c4.asgn( 1, 100, 0 );
c5 = c0; 
c5 = c5.set_d( -1 );  % necessary for Octave

SECTION;
    while( true );
        r = EXPECT_NTHROW( 'c5.d = 0;', TODOED('onoctave') );
        if( r );
            EXPECT_EQ( c5.d, 0 ); 
        else; 
            break; end;
    
        r = EXPECT_NTHROW( 'c0.idx = [1;1];', TODOED('onoctave') );
        if( r ); 
            EXPECT_EQ( c0.idx, [1;1] ); 
        else;
            break; end;
    
        r = EXPECT_NTHROW( 'c0.c = [1 1 1 1];', TODOED('onoctave') );
        if( r );
            EXPECT_EQ( c0.c, [1 1 1 1] );
        else;
            break; end;
        
        break; end;

SECTION
    EXPECT_EQ( get_c(c1), [-2 2 0 4] );
    EXPECT_EQ( c1.idx, [1;1] );
    EXPECT_EQ( c1.ref(1,2).get_c, 2 );
    EXPECT_EQ( get_c(ref(-c1,1,2)), -2 );
    EXPECT_EQ( c1.ref('1,-4:end'), c1 );
    EXPECT_EQ( c1.ref(1,'-4:end'), c1 );
    EXPECT_EQ( c1.ref(1,'-4:10'), c1 );
    EXPECT_EQ( get_c(c5.ref(-5,4)), -1 );

SECTION
    EXPECT_EQ( +c1, c1 );
    EXPECT_EQ( -c1, -1*c1, -1.*c1, c1/-1, c1./-1, sequence([2 -2 0 -4],[1 1]) );
    EXPECT_EQ( c1*2, 2*c1, sequence([-4 4 0 8],[1 1]) );
    EXPECT_EQ( 2./c1, c1.\2, sequence([-1 1 inf .5],[1 1],[],inf) );
    EXPECT_EQ( c1/2, c1./2, 2\c1, 2.\c1, sequence([-1 1 0 2],[1 1]) );
    EXPECT_EQ( c0+c1, c1+c0, sequence([1 2 0 4 0;0 -2 2 0 4],[0 0]) );
    EXPECT_EQ( c0.*c1, c1.*c0, sequence([0],[0 0]) );
    EXPECT_PRED( @isequaln, c1./c2, c2.\c1 );
    EXPECT_EQ( c3<=c1, sequence([0 0 1 0 1],[1 0],[],1) );
    EXPECT_NO_THROW( 'c3<c1;' ); 
    EXPECT_NO_THROW( 'c3>c1;' );
    EXPECT_NO_THROW( 'c3>=c1;' );
    EXPECT_EQ( c3, c4, c3 );
    EXPECT_EQ( sum(c1,'all'), 4 );
    EXPECT_ISA( sum(c1,'all'), 'double' );
    
SECTION
    EXPECT_ALMOST_EQ( norm(c1,-inf), 0 );
    EXPECT_ALMOST_EQ( norm(c1,0), 3 );
    EXPECT_ALMOST_EQ( norm(c1,1), 8 );
    EXPECT_ALMOST_EQ( norm(c1,2), (4+4+16)^(1/2) );
    EXPECT_ALMOST_EQ( norm(c1,3), (8+8+4*4*4)^(1/3) );
    EXPECT_ALMOST_EQ( norm(c1,inf), 4 );
    EXPECT_EQ( supp(c1), [1 1 1;1 2 4] );
    EXPECT_EQ( c1.idxmin, [1;1;1] );
    EXPECT_EQ( c1.idxmax, [1;4;1] );
    EXPECT_EQ( nnz(c1), 3 );
    EXPECT_EQ( ndims(c1), 2 );
    EXPECT_NO_THROW( 'numel(c1);' );
    EXPECT_NO_THROW( 'size(c1);' );
    [cc1,cc2] = equalize( c1, c2 );
    EXPECT_NO_THROW( 'cc1+cc2;' );
    EXPECT_EQ( shrink(cc1), c1 );
    EXPECT_EQ( trace(cc1), -2 );

SECTION
    EXPECT_EQ( shrink(sequence([0 0 1 0 1],[1 0])), sequence([1 0 1],[1 2]) );

SECTION
    EXPECT_EQ( upsample(c1,[2 1; 0 2]), sequence([-2 0 0 0 0 0 0;0 0 2 0 0 0 0;0 0 0 0 0 0 0;0 0 0 0 0 0 4],[3 2]) );
    EXPECT_EQ( conv(c1,c2,c1), sequence([40 0 0 -160 -360 0 0 960 480 640],[-3 2]) );

SECTION
    EXPECT_EQ( characteristic(c1), sequence([1 1 0 1],[1 1]) );
    val = diffsequence(c1,[1 1]);
    EXPECT_EQ( val, sequence([-2 4 -2 4 -4;2 -4 2 -4 4],[1 1]) );




%% matrix1d
TESTCASE
A = {}; A{2} = [8 8; 0 6];  A{1} = [4 4; 0 3]; A{3} = A{1}; 
p = [3 1 2];
A = cat( 1, permute(A{1},p), permute(A{2},p), permute(A{3},p) );
c0 = sequence( A, -1, nan, 0 );
c1 = c0; 
c1 = c1.set_idx( 1 );
c2 = 10*c0; 
c2 = c2.set_idx( -1 );
c3 = c0;
c3 = c3.set_d( -5 );

EXPECT_EQ( c1.ref('1').get_c, cat( 3, [4 0], [4 3]) );
EXPECT_EQ( c1.ref(1), cat( 3, [4 0], [4 3]) );
EXPECT_EQ( c1.ref(':'), c1 );
EXPECT_EQ( +c1, c1 );
EXPECT_EQ( -c1, -1*c1, -1.*c1, c1/-1, c1./-1 );
EXPECT_EQ( c1*2, 2*c1 );
EXPECT_EQ( 2./c1, c1.\2 );
EXPECT_EQ( c1/2, c1./2, 2\c1, 2.\c1 );
EXPECT_EQ( c0+c1, c1+c0 );
EXPECT_EQ( c0.*c1, c1.*c0 );
EXPECT_PRED( @isequaln, c1./c2, c2.\c1 );

A= { }; A{2} = [8 8; 0 6]; A{1} = [4 4; 0 3]; A{3} = A{1}; 
p = [3 1 2];
A = cat( 1, permute(A{1},p), permute(A{2},p), permute(A{3},p) );
c0 = sequence( A, -1, nan, 0 );
EXPECT_EQ( diffsequence(c0,1), sequence(cat(3,[4 0;4 0;-4 0;-4 0],[4 3;4 3;-4 -3;-4 -3]),-1,nan) );

%% matrix2d
TESTCASE
A = {}; A{1} = [0 2 4 6;1 3 5 7]; A{2} = 2*A{1}; A{3} = 3*A{1}; A{4} = 4*A{1}; A{5} = 5*A{1}; A{6} = 6*A{1}; A{1} = 0*A{1};
p = [3 4 1 2];
A = [permute(A{1},p), permute(A{3},p), permute(A{5},p); permute(A{2},p), permute(A{4},p), permute(A{6},p)]; 
c0 = sequence( A, [-1 -2].', nan );
EXPECT_EQ( c0.ndims, 4 );
EXPECT_EQ( c0.ndimso, 2 );
EXPECT_EQ( c0.ndimsi, 2 );
EXPECT_EQ( c0.size, [2 3 2 4] );
EXPECT_EQ( c0.sizeo, [2 3] );
EXPECT_EQ( c0.sizei, [2 4] );
EXPECT_EQ( c0.idxmin, [-1 -2 1 1]' );
EXPECT_EQ( c0.idxmino, [-1 -2]' );
EXPECT_EQ( c0.idxmini, [1 1]' );
EXPECT_EQ( c0.idxmax, [0 0 2 4]' );
EXPECT_EQ( c0.idxmaxo, [0 0]' );
EXPECT_EQ( c0.idxmaxi, [2 4]' );

EXPECT_EQ( c0.nnz, 7*5 );
EXPECT_EQ( c0.nnzo, 5 );
EXPECT_EQ( c0.numel, 8*6 );
EXPECT_EQ( c0.numelo, 6 );

EXPECT_EQ( shrink(c0), c0 );
EXPECT_EQ( get_c(c0.characteristico), [0 1 1;1 1 1] );
EXPECT_EQ( c0.supp, [0 -2; -1 -1; 0 -1; -1 0; 0 0].' );

c1 = c0;
c1.c(2,1,:,:) = 0;
EXPECT_EQ( get_c(c1.characteristico), [0 1 1;0 1 1] );
EXPECT_EQ( c1.characteristic, c1~=0 );
c1 = shrink(c1);
EXPECT_EQ( c1.idx, [-1;-1] );

A = [1 10 100 2 20 200 3 30 300 4 40 400
     5 50 500 6 60 600 7 70 700 8 80 800]';
EXPECT_EQ( sequence(A,[0 0],3).compact, A );

A = [1 1 2 2 3 3;1 1 2 2 3 3;4 4 5 5 6 6;4 4 5 5 6 6];
EXPECT_EQ( sequence(A,[0 0],[2 2]).compact, A );

%% test of functions in sequence class
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
TESTCASE
%% 2-tensors
st1 = tt.value( {@sqrtm} );
st2 = tt.value( {randn(1), randn(2), randn(3), randn(4)} );
GIVEN( @(st1,st2) EXPECT_NEAR( get_c(st1(sequence(st2))),  st1(st2), 5e-14 ) );

%% conv
TESTCASE
p = [3 1 2];
A = {}; A{1} = [1 2;3 4]; A{2} = [5 6;7 8]; A{3} = [-1 -2;-3 -4];
A = cell2mat( cellfun(@(x) permute(x,p), A, 'UniformOutput', false )' );
A = sequence( A, -1, nan );
C = {}; C{1} = [1;-1];
C = cell2mat( cellfun(@(x) permute(x,p), C, 'UniformOutput', false )' );
C = sequence( C, 0, nan );
D = conv( A, C );
EXPECT_EQ( compact(D), [-1 -1 -1 -1 1 1]' );
EXPECT_EQ( D.idx, -1 );

p = [3 1 2];
A = {}; A{1} = [1 2;2 0]; A{2} = [3 4;0 2];
A = cell2mat( cellfun(@(x) permute(x,p), A, 'UniformOutput', false )' );
A = sequence( A, 0, nan );
C = {}; C{1} = [1 1;-1 -1]; C{2} = [0 1;0 1];
C = cell2mat( cellfun(@(x) permute(x,p), C, 'UniformOutput', false )' );
C = sequence( C, 0, nan );
D = conv(A,C);
EXPECT_EQ( compact(D), [-1 -1;2 2;-1 2;-2 0;0 7;0 2] );
EXPECT_EQ( D.idx, 0 );

p = [3 1 2];
A = {}; A{1} = [1 2;2 0;3 -1];
A = cell2mat( cellfun(@(x) permute(x,p), A, 'UniformOutput', false )' );
A = sequence( A, 0, nan );
C = {}; C{1} = [1 2 -1 4;-1 0 0 -2];
C = cell2mat( cellfun(@(x) permute(x,p), C, 'UniformOutput', false )' );
C = sequence( C, 0, nan );
D = conv(A,C);
EXPECT_EQ( compact(D), [-1 2 -1 0;2 4 -2 8;4 6 -3 14] );

c   = sequence( [3 3 2]', 0, [], 1 );
d   = sequence( [1 -1 2]', 1);
e   = sequence( [1 -3 2]', -1);
cd  = sequence( [4 2 5 5 4]', 1, [], 2 );
ce  = sequence( [2 -4 -1 1 2]', -1 );
cdd = sequence( [6 2 11 4 9 8 8]', 2, [], 4 );
EXPECT_EQ( conv(c,d), conv(d,c), cd );
EXPECT_EQ( conv(c,e), conv(e,c), ce );
EXPECT_EQ( conv(d,conv(c,d)), conv(d,d,c), conv(c,d,d), conv(d,c,d), cdd );

x  = sequence( [1 2;0 4], [1;1], [], 1 );
y  = sequence( [0 1;1 -1], [0;1], [], 0 );
z  = sequence( [-1 1;1 -1], [0;1], [], 0 );
xy = sequence( [1 1 2;1 1 3;0 5 -2], [1;2], [], 1 );
xz = sequence( [0 -1 1;1 -3 2;-1 4 -3], [1;2] );
xyy = [1 1 1 1;1 1 1 2;1 1 2 2;1 0 7 -4;0 6 -6 4];
EXPECT_EQ( conv(x,y), conv(y,x), xy );
EXPECT_EQ( conv(x,z), conv(z,x), xz );

%% equalize
TESTCASE
o1 = sequence( [2;3] );
o2 = sequence( [5;5;5] );
[o1,o2] = equalize( o1, o2 );  %#ok<*CMEQU>
EXPECT_EQ( o1, sequence( [2;3;0]) );

%% mtimes
TESTCASE
% scalar-valued x scalar-valued
SECTION
c1 = sequence( [3 2 1]' );
c2 = sequence( [3 2 1] );
EXPECT_EQ( c2*c2, sequence([9 6 3]) );
val = c1*c1;
EXPECT_EQ( val.c, dot(c1.c,c1.c) );

% tensor-valued x tensor-valued
SECTION
A = sequence( [1 2 -1 -2;3 4 -1 -2], [0;0], [2 2] );
B = sequence( [3 5 -1 -3]', [0], 2 );
C = A*B;
E = [1 2;3 4]*[3;5] + [-1 -2;-1 -2]*[-1;-3];
EXPECT_EQ( C.compact, E );
EXPECT_EQ( C.ndimso, 1 );
EXPECT_EQ( C.sizei, 2 );

S = [3 5;-1 -3];
As = sequence( [1 2 -1 -2;3 4 -1 -2], [0;0], [2 2], [1 1;1 1] );
EXPECT_EQ( S*As, sequence([18 26 -8 -16;-10 -14 4 8],[0;0],[2 2],[8 8;-4 -4]) );
EXPECT_EQ( As*S, sequence([1 -1 -1 1;5 3 -1 1],[0;0],[2 2],[2 2;2 2]) );
EXPECT_EQ( 2*As, As*2, sequence([2 4 -2 -4;6 8 -2 -4], [0;0],[2 2],[2 2;2 2]) );

% tensore-valued x matrix (and vice versa)
SECTION
As = sequence( [1 2 -1 -2;3 4 -1 -2; 2 2 0 0], [0;0], [3 2], [1 1;1 1;0 0] );
S = [1 2 3];
T = [-1 -2 1 2;0 1 1 0];
EXPECT_THROW( @() As*S, ':innerdim', 'Octave:nonconformant-args' );
EXPECT_EQ( S*As, sequence([13 16 -3 -6],[0;0],[1 2],[3 3]) );
EXPECT_THROW( @() T*As, ':innerdim', 'Octave:nonconformant-args' );
EXPECT_EQ( As*T, sequence([-1 0 3 2 1 0 -3 -2;-3 -2 7 6 1 0 -3 -2;-2 -2 4 4 0 0 0 0],[0;0],[3 4],[-1 -1 2 2;-1 -1 2 2;0 0 0 0]) );

% tensor-valued x scalar (and vice versa)
SECTION
A = sequence( [1 2 -1 -2 -1 0;3 4 -1 -2 3 3], [0;0], [2 2] );
B = sequence( [1;2;-2], [0;0] );
EXPECT_EQ( A*B, sequence([1 -2;-5 -6],[0;0],[2 2]) );
EXPECT_EQ( B*A, sequence([1 2 -1 -2 -1 0;3 4 -1 -2 3 3;2 4 -2 -4 -2 0;6 8 -2 -4 6 6;-2 -4 2 4 2 0;-6 -8 2 4 -6 -6],[0;0],[2 2]) );

A = sequence( [1 2 -1 -2 -1 0;3 4 -1 -2 3 3], [0;0], [2 2] );
B = sequence( [1;2;-2], [0] );
EXPECT_EQ( A*B, sequence([1 -2;-5 -6],[0],[2 2]) );
EXPECT_EQ( B*A, sequence([1 2;3 4;-1 -2;-1 -2;-1 0;3 3],[0],[2 2]) );

A = sequence( [1 2 -1 -2 -1 0;3 4 -1 -2 3 3], [0;-1], [2 2] );
B = sequence( [1;2;-2], [0;0] );
EXPECT_EQ( A*B, sequence([-3 -2;5 4],[0;0],[2 2]) );


%% plus
TESTCASE
c = sequence( [1 1 2 2;1 1 2 2], [0;0], [1 2], [3 3] );
d = sequence( [10 10 20 20], [2;2], [1 2], [4 4] );
EXPECT_EQ( c+d, sequence([5 5 6 6 7 7 7 7;5 5 6 6 7 7 7 7;7 7 7 7 13 13 23 23],[0;0],[1 2],[7 7]) );

c = sequence( [], [0], [1], [2] );
d = sequence( [], [0], [1], [3] );
EXPECT_EQ( c+d, sequence([],[0],nan,[5]) );

%scalar plus tensor
s =  sequence( [1 2 3 4 5].', [0], [1] );
t = sequence( [3 3;3 3].', [0], [2 2], [3 3;3 3] );
% XX

a = sequence( [2;3], 0, [], 0 );
s = sequence( [], 0, [], 1 );
b = sequence( [3;4], 0, [], 1 );
EXPECT_EQ( a+s, b );

%% squeeze
TESTCASE

c = sequence(randi(9, 2,1,1,3,1,2,1,2 ),[0;0;0],nan);
EXPECT_EQ( squeeze(c,3), squeezeo(c,3), squeezeo(c,-1) );
EXPECT_EQ( c, squeezeo(c,4) );
EXPECT_EQ( squeezeo(c), squeezeo(c,[2 3]), squeezeo(c,[-1 -2]) );
EXPECT_EQ( squeeze(c,-4), squeeze(c,5), squeezei(c,2), squeezei(c,-4) );
EXPECT_EQ( squeezei(c), squeezei(c,[2 4]) );
EXPECT_EQ( squeeze(c), squeeze(c,[2 3 5 7]) );

c = sequence( randi(9, 4,3,1,2 ), [0;0], nan, [1 2] );
EXPECT_EQ( get_d(squeezei(c,1)), get_d(squeezei(c,-2)), get_d(squeeze(c,3)), get_d(squeeze(c,-2)), [1;2] );
EXPECT_NE( get_d(squeezeo(c,3)), [1;2] );


%% subs
TESTCASE
c = sequence( [1 10 2 20 3 30;100 1000 200 2000 300 3000; 4 40 5 50 6 60;400 4000 500 5000 600 6000], [0;0], [2 2] );
ind = sequence( logical([1 1 0 0 0 0;1 1 0 0 0 0;0 0 0 0 0 0;0 0 0 0 1 1]), [0;0], [2 2] );
EXPECT_EQ( c.ref(ind), [1 100 600 10 1000 6000].' );
EXPECT_EQ( c.asgn(ind,-1).compact, [-1 -1 2 20 3 30;-1 -1 200 2000 300 3000; 4 40 5 50 6 60;400 4000 500 5000 -1 -1] );
EXPECT_EQ( c.asgn(ind,[-1:-1:-6]).compact, [-1 -4 2 20 3 30;-2 -5 200 2000 300 3000; 4 40 5 50 6 60;400 4000 500 5000 -3 -6] );

ind = sequence( logical([1 1 0 0 0 0;1 1 0 0 0 0;0 0 0 0 0 0;0 0 0 0 1 1]), [-1;0], [2 2] );
EXPECT_EQ( c.ref(ind), [0 0 300 0 0 3000].' );
EXPECT_EQ( c.asgn(ind,-1).compact, [-1 -1 0 0 0 0;-1 -1 0 0 0 0; 1 10 2 20 3 30;100 1000 200 2000 -1 -1; 4 40 5 50 6 60;400 4000 500 5000 600 6000] );
EXPECT_EQ( c.asgn(ind,[-1:-1:-6]).compact, [-1 -4 0 0 0 0; -2 -5 0 0 0 0; 1 10 2 20 3 30; 100 1000 200 2000 -3 -6; 4 40 5 50 6 60; 400 4000 500 5000 600 6000] );

ind = logical(decompact([1 1 0 0 0 0; 1 1 0 0 0 0; 0 0 0 0 0 0; 0 0 0 0 1 1], 2, [2 2] ));
EXPECT_EQ( c.ref(ind), [1 100 600 10 1000 6000].' );
EXPECT_EQ( c.asgn(ind,-1).compact, [-1 -1 2 20 3 30;-1 -1 200 2000 300 3000; 4 40 5 50 6 60;400 4000 500 5000 -1 -1] );
EXPECT_EQ( c.asgn(ind,[-1:-1:-6]).compact, [ -1 -4 2 20 3 30; -2 -5 200 2000 300 3000; 4 40 5 50 6 60; 400 4000 500 5000 -3 -6 ] );

ind = logical( [1 0 0;1 0 1] );
EXPECT_EQ( compact(c.ref(ind),1), [1 10;100 1000;4 40;400 4000;6 60;600 6000] );
EXPECT_EQ( c.asgn(ind,-1).compact, [-1 -1 2 20 3 30;-1 -1 200 2000 300 3000; -1 -1 5 50 -1 -1;-1 -1 500 5000 -1 -1] );
EXPECT_EQ( c.asgn(ind,[-1 -2 -3]).compact, [-1 -1 2 20 3 30;-1 -1 200 2000 300 3000; -2 -2 5 50 -3 -3;-2 -2 500 5000 -3 -3] );

ind = sequence( logical([1 0 0;1 0 1]), [-1;0] );
EXPECT_EQ( compact(c.ref(ind),1), [0 0;0 0;1 10;100 1000;3 30;300 3000] );
EXPECT_EQ( c.asgn(ind,-1).compact, [-1 -1 0 0 0 0;-1 -1 0 0 0 0;-1 -1 2 20 -1 -1;-1 -1 200 2000 -1 -1; 4 40 5 50 6 60;400 4000 500 5000 600 6000] );
EXPECT_EQ( c.asgn(ind,[-1 -2 -3]).compact, [-1 -1 0 0 0 0;-1 -1 0 0 0 0;-2 -2 2 20 -3 -3;-2 -2 200 2000 -3 -3; 4 40 5 50 6 60;400 4000 500 5000 600 6000] );

warning( 'off', 'sequence:linidx' );
ind = [0 0; 1 0]';
EXPECT_EQ( compact(c.ref(ind),1), [1 10;100 1000;4 40;400 4000] );
EXPECT_EQ( c.asgn(ind,-1).compact, [-1 -1 2 20 3 30;-1 -1 200 2000 300 3000; -1 -1 5 50 6 60;-1 -1 500 5000 600 6000] );
EXPECT_EQ( c.asgn(ind,[-1 -2]).compact, [-1 -1 2 20 3 30;-1 -1 200 2000 300 3000; -2 -2 5 50 6 60;-2 -2 500 5000 600 6000] );

ind = [0 0; -1 0]';
EXPECT_EQ( compact(c.ref(ind),1), [1 10;100 1000;0 0;0 0] );
EXPECT_EQ( c.asgn(ind,-1).compact, [-1 -1 0 0 0 0;-1 -1 0 0 0 0;-1 -1 2 20 3 30;-1 -1 200 2000 300 3000; 4 40 5 50 6 60;400 4000 500 5000 600 6000] );
EXPECT_EQ( c.asgn(ind,[-1 -2]).compact, [-2 -2 0 0 0 0;-2 -2 0 0 0 0;-1 -1 2 20 3 30;-1 -1 200 2000 300 3000; 4 40 5 50 6 60;400 4000 500 5000 600 6000] );

ind = [0 -1 1 1; 0 0 1 1;1 1 2 2]';
EXPECT_EQ( c.ref(ind), [0 1 5000] );
EXPECT_EQ( c.asgn(ind,-1).compact, [-1 0 -1 10 2 20 3 30;0 0 100 1000 200 2000 300 3000;0 0  4 40 5 50 6 60;0 0 400 4000 500 -1 600 6000] );
EXPECT_EQ( c.asgn(ind,[-1 -2 -3]).compact, [-1 0 -2 10 2 20 3 30;0 0 100 1000 200 2000 300 3000;0 0  4 40 5 50 6 60;0 0 400 4000 500 -3 600 6000] );


%% upsample
TESTCASE
SECTION
    c = sequence( 1, [0;0] );
    EXPECT_EQ( upsample(c, [2 0;0 2]), c );

    val = upsample( sequence([1 2], 0, nan), 2 );
    EXPECT_EQ( val.shrink.get_c, [1 2] );

    val = upsample( sequence([0 1], 0, nan), 2 );
    EXPECT_EQ( val.shrink.get_c, [0 1] );
    
SECTION
    A = [1 1 2 2 3 3;1 1 2 2 3 3;4 4 5 5 6 6;4 4 5 5 6 6];
    c = sequence( A, [1 1], [2 2] ) ;
    d = upsample( c, [-2 2] );
    if( numel(d) == 96 );
        dexp = [0 0 0 0 0 0 0 0 0 0 0 0;
                0 0 0 0 0 0 0 0 0 0 0 0;
                4 4 0 0 5 5 0 0 6 6 0 0;
                4 4 0 0 5 5 0 0 6 6 0 0;
                0 0 0 0 0 0 0 0 0 0 0 0;
                0 0 0 0 0 0 0 0 0 0 0 0;
                1 1 0 0 2 2 0 0 3 3 0 0;
                1 1 0 0 2 2 0 0 3 3 0 0];
        EXPECT_EQ( d.compact, dexp );
        EXPECT_EQ( d.d, 0 );
        EXPECT_EQ( d.idx, [-5;2] );
    elseif( numel(d) == 60 );
         dexp = [4 4 0 0 5 5 0 0 6 6; 
                 4 4 0 0 5 5 0 0 6 6;
                 0 0 0 0 0 0 0 0 0 0;
                 0 0 0 0 0 0 0 0 0 0;
                 1 1 0 0 2 2 0 0 3 3;
                 1 1 0 0 2 2 0 0 3 3];
        EXPECT_EQ( d.compact, dexp );
        EXPECT_EQ( d.d, 0 );
        EXPECT_EQ( d.idx, [-4;2] );   
    else;
        EXPECT_FAIL( 'upsample failed.' ); end;

SECTION
    A = [1 1 2 2;1 1 2 2;3 3 4 4;3 3 4 4];
    c = sequence(A,[0 0],[2 2]);
    d = upsample(c,[2 1; 0 -2]);
    dexp = [0 0 0 0 1 1;
            0 0 0 0 1 1;
            2 2 0 0 0 0;
            2 2 0 0 0 0;
            0 0 0 0 3 3;
            0 0 0 0 3 3;
            4 4 0 0 0 0;
            4 4 0 0 0 0];
    EXPECT_EQ( d.compact, dexp );
    EXPECT_EQ( d.d, 0 );
    EXPECT_EQ( d.idx, [0;-2] );

%% comparison
TESTCASE
EXPECT_EQ( sequence([2;3])<sequence([5;5]), sequence([1;1]) );

%%
ENDTESTCASE


%#ok<*NOSEL,*NOSEMI,*ALIGN>


