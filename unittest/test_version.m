% test version

%#ok<*CTPCT>
%#ok<*ASGLU>
%#ok<*NASGU>

TTEST init;

% preconditions    

%% test external functions / libraries

%% test version
TESTCASE
EXPECT_TOOLBOX( 'matlab', '>=R2016b', APPLIED('onmatlab') );
EXPECT_TOOLBOX( 'octave', '>=6.4', APPLIED('onoctave') );

%% test toolboxes
TESTCASE
op = tt.expect_toolbox( 'optimization' );
%pa = tt.expect_toolbox( 'parallel' );
%si = tt.expect_toolbox( 'signal' );
gu = tt.expect_toolbox( 'gurobi' );
js = tt.expect_toolbox( 'jsrlouvain' );
se = tt.expect_toolbox( 'sedumi' );

MESSAGE( 'JSR Toolbox not installed.' );
EXPECT_TRUE( js );

MESSAGE( 'LP/CP solvers not installed.' );
EXPECT_TRUE( op && gu || se );

%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*MCCD>

