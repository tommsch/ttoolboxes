% test sequence

%#ok<*CTPCT>
%#ok<*ASGLU>
%#ok<*NASGU>

TTEST init test_ipa_sym

ASSUME_TOOLBOX( 'symbolic', OPTION('load') );
EXPECT_TOOLBOX( 'matlab', '>=R2016b', APPLIED('onmatlab') );
EXPECT_TOOLBOX( 'octave', '>=6.4', APPLIED('onoctave') );

optim_inst = tt.expect_toolbox( 'optimization' );
sedumi_inst = tt.expect_toolbox( 'sedumi' );
gurobi_inst = tt.expect_toolbox( 'gurobi' );
assert( sedumi_inst || optim_inst || gurobi_inst, 'At least SeDuMi, the optimization toolbox or Gurobi must be installed.' );
% preconditions   

solver = {};
if( gurobi_inst );
    solver = [ solver 'g']; end;
if( sedumi_inst );
    solver = [ solver 's']; end;

%% daubechiesmatrix
TESTCASE;
SECTION( 'fully sym' );
    EXPECT_NO_THROW( 'daubechiesmatrix(3,''gugl'',''v'',-1,''double'');' );
    D21 = daubechiesmatrix( 2, 'gugl', 'v',-1 ); 
    val1 = max(cellfun(@(x) max(abs(eig(double(x)))), D21) );
    D22 = daubechiesmatrix( 2,'jung','v',-1); 
    val2 = max( cellfun(@(x) max(abs(eig(double(x)))), D22) );
    EXPECT_ALMOST_EQ( val1, val2 );

%% daubechiesmatrix
SECTION( 'partly sym' );
    [~, D3] =  EXPECT_NTHROW( @() daubechiesmatrix( 3, 'jung', 'v',-1, 'double' ), 'daubechiesmatrix:sym' );
    [~, D21] = EXPECT_NTHROW( @() daubechiesmatrix( 2, 'gugl', 'v',-1 ), 'daubechiesmatrix:sym', TODOED('onoctave') );
    [~, D22] = EXPECT_NTHROW( @() daubechiesmatrix( 2, 'jung', 'v',-1 ), 'daubechiesmatrix:sym' );
    if( ~isempty(D21) && ~isempty(D22) );
        val1 = max( cellfun( @(x) max( abs(eig(double(x))) ), D21 ) );
        val2 = max( cellfun( @(x) max( abs(eig(double(x))) ), D22 ) );
        EXPECT_ALMOST_EQ( val1, val2 );
        end;


%% extravertex
TESTCASE;
EXPECT_EQ( eye(3), extravertex( zeros(3,0), 'type','r' ) );


%% findsmp
TESTCASE;
allfunctionhandle( 'findsmp', {'makePtot_idx', 'makePtot_bf', 'makePtot_nl', 'computeNtRt_base', 'computelbub_smaxp', 'computelbub_sminp', 'choosematrix_lhr', 'simplify_ordering'}, 'assignin', 'v',0 );

SECTION;
    AA = {[1 -1; 3 -2], [1 3; -1 -1]};
    EXPECT_EQ( findsmp( AA, 'modgrip',  'maxsmpdepth',5, 'v',-1, 'sym',-1 ), {[1;2]} );
    EXPECT_EQ( findsmp( AA, 'modgrip',  'maxsmpdepth',5, 'v',-1, 'sym', 0 ), {[1;2]} );
    EXPECT_EQ( findsmp( AA, 'lowgrip',  'maxsmpdepth',5, 'v',-1 ),           {[1;2]} );
    EXPECT_EQ( findsmp( AA, 'lowgrip',  'maxsmpdepth',5, 'v',-1, 'sym', 0 ), {[1;2]} );
    EXPECT_EQ( findsmp( AA, 'lowgrip',  'maxsmpdepth',5, 'v',-1, 'sym',1 ),  {[1;2]} );
    EXPECT_EQ( findsmp( AA, 'randgrip', 'maxsmpdepth',5, 'v',-1 ),           {[1;2]} );
    EXPECT_EQ( findsmp( AA, 'randgrip', 'maxsmpdepth',5, 'v',-1, 'sym', 1 ), {[1;2]} );
    EXPECT_EQ( findsmp( AA, 'randgrip', 'maxsmpdepth',5, 'v',-1, 'sym', 2 ), {[1;2]} );
    
SECTION;
    EXPECT_EQ( findsmp( daubechiesmatrix(  7, 'v',-1, 'd' ), 'v',-1 ), {[1],[2]} );
    EXPECT_EQ( findsmp( daubechiesmatrix(  2, 'v',-1, 'd' ), 'v',-1 ), {[1]} );
    EXPECT_EQ( findsmp( daubechiesmatrix( 10, 'v',-1, 'd' ), 'v',-1 ), {[1;1;2;2]} );
    
SECTION;
    Msym = {sym([0 0 1;0 0 1;0 1 0]), sym([0 1 0;1 0 1;0 1 1])};
    M = {[0 0 1;0 0 1;0 1 0],[0 1 0;1 0 1;0 1 1]};
    EXPECT_EQ( findsmp( M,    'sym', 4, 'v',-1, 'maxsmpdepth',3 ), {[2]} );
    EXPECT_EQ( findsmp( M,    'sym', 3, 'v',-1, 'maxsmpdepth',3 ), {[2]} );
    EXPECT_EQ( findsmp( M,    'sym',-1, 'v',-1, 'maxsmpdepth',3 ), {[2]} );
    EXPECT_EQ( findsmp( M,    'sym', 2, 'v',-1, 'maxsmpdepth',3 ), {[2]} );
    EXPECT_EQ( findsmp( Msym, 'sym',-1, 'v',-1, 'maxsmpdepth',3 ), {[2]} );
    EXPECT_EQ( findsmp( Msym, 'sym', 2, 'v',-1, 'maxsmpdepth',3 ), {[2]} );    
    
    Msym = {sym([0 0 1;0 1 0;0 0 1]), sym([0 1 0;1 1 0;1 1 1])};  % has two s.m.p.s
    EXPECT_EQ( findsmp( Msym, 'sym',2, 'maxsmpdepth',8, 'v',-1 ), {[1 2 2 2].', [1 2 1 2 2 2 2 2].'} );

%%% computelbub_smaxp
SECTION
    [lb, ub] = computelbub_smaxp( sym(10), sym(100), [], [], sym([30 40 50]), sym([0 10 20]), [], sym(1), [] );
    EXPECT_EQ( lb, sym(20) );
    EXPECT_EQ( ub, sym(50) );

    syms xx yy
    [lb, ub] = computelbub_smaxp( sym(abs(xx)), sym(abs(xx)+2), [], [], sym(abs(xx)+1), sym(abs(xx)+1), [], sym(1), [] );
    EXPECT_EQ( lb, sym(abs(xx) + 1) );
    EXPECT_EQ( ub, sym(abs(xx) + 1) );

%% invariantsubspace
TESTCASE
SECTION
    s2 = sym(2);
    MA{1} = [[ 0, 0, 2/(5^(1/s2) + 1)];[ 0, 0, 2/(5^(1/s2) + 1)];[ 0, 0, 0]];
    MA{2} = [[ 0, 0, 2/(5^(1/s2) + 1)];[ 0, 2/(5^(1/s2) + 1), 0];[ 2/(5^(1/s2) + 1), 2/(5^(1/s2) + 1), 2/(5^(1/s2) + 1)]];
    [MM,basis,warningflag] = invariantsubspace( MA, 'auto', 'v',0, 'sym',3 );
    EXPECT_EQ( MM{1}{1}, MA{1} );

    for i = -1:4;
        [MM, B] = invariantsubspace( {sym([1 1 1; 1 0 1; 0 1 0]), sym([1 -1 0; 0 2 0; 1 1 2])}, 'sym',i, 'v',-1 );
        EXPECT_EQ( numel(MM), 3 ); end;
    
SECTION    
    M = {[1 1 1; 1 0 1; 0 1 0], [1 -1 0; 0 2 0; 1 1 2]};
    
    [MM, B] = invariantsubspace( M, 'none', 'sym',3, 'v',-1 );
    EXPECT_EQ( B, sym(eye(3)) );    
    
    [MM, B] = invariantsubspace( M, 'diff', 'sym',2, 'v',-1 );
    EXPECT_EQ( numel(MM), 2 );
    EXPECT_EQ( MM{2}{1}, sym([1 2;1 0]) );  % The result of this test may change when the implementation of invariantsubspace changes
    
    [MM, B] = invariantsubspace( M, 'basis', 'sym',2, 'v',-1 );
    EXPECT_EQ( numel(MM), 3 );
    
    [MM, B] = invariantsubspace( M, 'basis', 'sym',4, 'v',-1 );
    EXPECT_EQ( numel(MM), 3 );
    EXPECT_PRED( @issymstrict, MM{1}{1} );     
    
SECTION
    M = {[1 0 1;1 1 0;1 0 1],[1 0 2;0 1 1;2 0 2]};
    [MM, B] = invariantsubspace( M, 'v',-1, 'sym',1 );
    EXPECT_PRED( @issym, B );
    
    [MM, B] = invariantsubspace( M, 'v',-1, 'sym',2 );
    EXPECT_EQ( B, sym([0 1 0;1 0 0;0 0 1]) );
    
    [MM, B] = invariantsubspace( M, 'v',-1, 'sym',3 );
    EXPECT_EQ( B, sym([0 1 0;1 0 0;0 0 1]) );
    
    [MM, B] = invariantsubspace( M, 'v',-1, 'sym',4 );
    EXPECT_EQ( B, sym([0 1 0;1 0 0;0 0 1]) );    
    
SECTION
    M = { [0 0 1;0 0 1;0 0 0], [0 0 1;0 1 0;1 1 1] };
    [MM,basis,warningflag] = invariantsubspace( M, 'auto', 'v',-1, 'sym',2 );
    EXPECT_EQ( MM{1}, {sym(M{1}) sym(M{2})} );

SECTION    
    a = 5^(1/sym(2));
    M = {
        2/(a + 1)*[0 0 1;0 0 1;0 0 0], ...
        2/(a + 1)*[0 0 1;0 1 0;1 1 1]
        };    
    [MM,basis,warningflag] = invariantsubspace( M, 'auto', 'v',-1, 'sym',2 );
    EXPECT_EQ( MM{1}, {sym(M{1}) sym(M{2})} );    
    
SECTION( 'Output type' );
  %%% output type test
    SECTION
    M1 = {[1],[2]}; 
    M2 = {[1 0;0 1],[2 0;0 2]}; 
    M3 = {[1],sym([2])}; 
    M4 = {[1 0;0 1],sym([2 0;0 2])}; 
    M5 = {sym([1]),sym([2])}; 
    M6 = {sym([1 0;0 1]),sym([2 0;0 2])}; 
    
    EXPECT_EQ( invariantsubspace( M1, 'outputtype' ), ...
               invariantsubspace( M2, 'outputtype' ), ...
                'double' );
    EXPECT_EQ( invariantsubspace( M5, 'outputtype' ), ...
               invariantsubspace( M6, 'outputtype' ), ...
               'sym' );
    EXPECT_EQ( invariantsubspace( M1, 'outputtype', 'sym',0 ), 'double' );
    EXPECT_EQ( invariantsubspace( M3, 'outputtype', 'sym',0 ), 'sym' );
    

SECTION( 'Regressiontests' );
    A = sym( [1 0 -1 0;0 1 0 0;-1 -1 -1 0;1 -1 -1 -1] );
    invariantsubspace( {A,A}, 'v',0 );


%% ipa
TESTCASE
SECTION( 'general' );
    A = [ -8325854551438777/18014398509481984,  -1371732554537271/2251799813685248
           7873947122517161/18014398509481984,  -2106884015594867/4503599627370496];
    B = [  5805556464020791/144115188075855872,  1836273638859193/2251799813685248
          -2999026326278621/144115188075855872,  2833225208504457/36028797018963968];
    EXPECT_THROW( @() ipa( {A,B}, 'epssym',5e-5, 'addlimitmatrix',1, 'maxiteration',2, 'maxsmpdepth',5 ), 'ipa:unkown_option', 'parsem:unknown' );  % When 'addlimitmatrix' is added again: this test expects 'makecandidate:limitmatrix'
    
    A = [0 0 0;0 0 0;0 0 1];
    B = [0 1 0;0 1 -1;1 -1 0];
    EXPECT_ALMOST_EQ( ipa({A,B}, 'maxtime',30, 'v',0 ), 1 );
    
    [~, D7] = EXPECT_NTHROW( @() daubechiesmatrix( 7, 'v',0 ) );
    JSR = ipa( D7, 'balancing',[1 1.02 .01 .01 .01], 'v',-1, 'maxiteration',15, 'invariantsubspace','none' );
    EXPECT_NEAR( JSR, 0.181695060514703, 10e-12 );
    EXPECT_EQ( numel(JSR), 1 );
    JSR = ipa( D7, 'balancing',[1 2 1 1 1], 'v', -1, 'maxiteration',100, 'invariantsubspace','none', 'epssym',nan ); 
    EXPECT_GE( numel(JSR), 2 );
    
    a = 5^(1/sym(2));
    A = { 2/(a + 1)*[0 0 1;0 0 1;0 0 0], 2/(a + 1)*[0 0 1;0 1 0;1 1 1] };
    [~, r] = EXPECT_NTHROW( @() ipa( A ),  'findsmp:numerical', 'leadingeigenvector:numerical_maybe', 'symbolic:mldivide:InconsistentSystem', 'balancepolytope:numerical' );
    EXPECT_EQ( r, 1 );
    
    EXPECT_EQ( ipa( {[0 1;1 0],[0 1;0 1]}, 'epssym',0, 'v',-1, 'maxsmpdepth',2 ), 1 );
    EXPECT_ALMOST_EQ( ipa( {[0 0 1;0 1 0;0 1 0],[0 0 1;1 1 1;0 0 0]}, 'v',-1 ), 1.442249570307408 );


    for sol_ = solver; sol = sol_{1};
        T = {[1 1;0 1], 4/5*[1 0;1 1]};
        EXPECT_NUMEL( ipa( T, 'sym',0, 'maxsmpdepth',2, 'maxiteration',50, 'v',-1, 'solver',sol ), 2 );  % this must not terminate, since T has no spectral gap

        T = {[1 1;0 1], 4/5*[1 0;1 1]};
        EXPECT_NUMEL( ipa( T, 'sym',1, 'maxsmpdepth',2, 'maxiteration',10, 'v',-1, 'solver',sol ), 1 );  % this terminates, since new points are mapped onto the boundary
        end;

    

SECTION( 'regression' )
    T = {[0 0 0;0 0 1;0 0 0], [0 0 -1;0 0 1;1 1 -1]};
    [~, r, nfo] = EXPECT_NTHROW( @() ipa( T, 'v',1, 'invariantsubspace_sym',4 ), 'findsmp:numerical', 'symbolic:mldivide:InconsistentSystem' );
    EXPECT_EQ( r, 1 );

    A = {[-1 -1;1 1], [-1 -1;-1 1]};  % eigenvalues are computed very wrongly here, and algorithm should fall back to symbolic computation
    EXPECT_NTHROW( @() ipa( A ), 'findsmp:numerical' );

    [~, T] = EXPECT_NTHROW( @() codecapacity( {[1 1 0],[1 -1 0]} ) );
    r = ipa( T, 'maxtime',60, 'v',0 );
    EXPECT_ALMOST_EQ( r, sqrt(2) );

    ptstotest2=[0.48075031917066879;0.96150063834133759;0];
    VV2=[0.33333365394464937 0 0.48075031917066879 0.48074985676913606;1.000000961833948 0.6933619412490466 0.48075031917066879 0.96149971353827213;0 0.6933619412490466 0.48075031917066879 0];
    pts_sym=cell([1 4]);
    pts_sym{1}=int32([-1;1;2;0]);
    pts_sym{2}=cell([1 2]);
    pts_sym{2}{1}=str2sym('[0, 0, 1; 0, 1, 0; 0, 1, 0]');
    pts_sym{2}{2}=str2sym('[0, 0, 1; 1, 1, 1; 0, 0, 0]');
    pts_sym{3}=cell([1 2]);
    pts_sym{3}{1}=str2sym('[(721*10^(1/2))/6840; (721*10^(1/2))/2280; 0]');
    pts_sym{3}{2}=str2sym('[0; 1; 0]');
    pts_sym{4}=str2sym('3^(1/3)');
    VV_sym=cell([1 4]);
    VV_sym{1}=int32([-1 -1 -1 -2;0 1 1 1;0 0 1 2;0 0 0 0]);
    VV_sym{2}=cell([1 2]);
    VV_sym{2}{1}=str2sym('[0, 0, 1; 0, 1, 0; 0, 1, 0]');
    VV_sym{2}{2}=str2sym('[0, 0, 1; 1, 1, 1; 0, 0, 0]');
    VV_sym{3}=cell([1 2]);
    VV_sym{3}{1}=str2sym('[(721*10^(1/2))/6840; (721*10^(1/2))/2280; 0]');
    VV_sym{3}{2}=str2sym('[0; 1; 0]');
    VV_sym{4}=str2sym('3^(1/3)');
    s10 = sqrt(sym(10));
    EXPECT_EQ( polytopenorm( ptstotest2, VV2, 'p', 'pts',pts_sym, 'VV',VV_sym, 'bound',[0 1], 'debug', 'v',0 ), 721*s10/2280*ones(3, 1) );
    
    p=[1;0;1];
    V=str2sym('[2, 1, 1; 1, 2, 1; 1, 1, 2]');
    polytopenorm( p, V, 'pNS', 'output',1 );  % behaviour of this call is not yet clear to me

    % % this does not work anymore, since Matlab R2023b represents the eigenvectors as root objects, instead of vpa
    % T = cell( [1 2] );
    % T{1} = [1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0;0 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0;0 1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0;0 0 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0;0 0 1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0;0 0 0 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0;0 0 0 1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0;0 0 0 0 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0;0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0;0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0;0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 0 0 0 0;0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 0 0 0 0;0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 0 0 0;0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 0 0 0;0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 0 0;0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 0 0;0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 0;0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 0;0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1;0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1];
    % T{2} = [1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0;1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0;0 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 0;0 1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0;0 0 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0;0 0 1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0;0 0 0 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0;0 0 0 1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0;0 0 0 0 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0 0;0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0;0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 0 0 0 0 0;0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 0 0 0 0;0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 0 0 0 0;0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 0 0 0;0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 0 0 0;0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 0 0;0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 0 0;0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 0;0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 0;0 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1];
    % [r, nfo] = ipa( T, 'sym',1, 'maxsmpdepth',2, 'maxiteration',50, 'v',0 );
    % EXPECT_NUMEL( r, 1 );
    % EXPECT_ALMOST_EQ( r, double(rho(sym(T{1}))) );

SECTION( 'daubechiesmatrices' );
    [~, D7] = EXPECT_NTHROW( @() daubechiesmatrix( 7, 'v',0 ), 'daubechiesmatrix:sym' );
    [~, JSR] = EXPECT_NTHROW( @() ipa( D7, 'v',-1, 'maxiteration',15 ), 'daubechiesmatrix:sym' );
    EXPECT_NEAR( JSR, 0.181695060514689, 10e-12 );
    EXPECT_EQ( numel(JSR), 1 );

    [~, JSR] = EXPECT_NTHROW( @() ipa( D7, 'balancing',0, 'delta',.99999, 'v',-1, 'maxiteration',13 ), 'daubechiesmatrix:sym' );
    EXPECT_NEAR( JSR(1), 0.181695060514703, 6e-3 );
    EXPECT_NEAR( JSR(2), 0.181695060514703, 6e-3 );
    EXPECT_EQ( numel(JSR), 2 );
    %JSR = ipa( D7, 'alwaysout', 'maxiteration',3, 'v',-1, 'fastnorm',0 );

%% leadingeigenvector
TESTCASE
SECTION;
    [v0sym, ~, ~, ~, oosym, indsym] = leadingeigenvector( {sym([1 1 ; 0 1]), sym([0 0; 1 0])}, {[1 2]',[1]'}, 'cycle' );
    [v0, ~, ~, ~, oo, ind] = leadingeigenvector( {([1 1 ; 0 1]),([0 0; 1 0])}, {[1 2]',[1]'}, 'cycle' );
    EXPECT_EQ( oo, double(oosym), {[1;2],[2;1],[1]} );
    EXPECT_LE( norm(cell2mat(double(v0 - v0sym))), 1e-12 );
    EXPECT_EQ( ind, indsym, [1 1 2] );
    
    D7 = daubechiesmatrix( 7, 'v',-1, 'jung' );
    [~, v0s, mult] = leadingeigenvector( D7, {[1],[2]},  'v',-1, 'sym',0 );
    EXPECT_EQ( mult, [1 1] );
    EXPECT_NEAR( v0s{1}/v0s{1}(1), [0.715677698431714 0.632185219429203 0.266730863902382 -0.108126347808392 -0.024608820551088 0.067342553358251 -0.013038631543157].'/0.715677698431714, 1e-14 );
    
    [~, v0s] = leadingeigenvector( {sym([1 -1;1 1])}, {[1]},  'multipleeigenvector',1, 'v',-1, 'complexeigenvector',2, 'simpleconjugate', 'cycle',0 );
    EXPECT_ALMOST_EQ( v0s{1}, [2^(1/2)/2;-(2^(1/2)*1i)/2] ); %Depending on the implementation thr other conjugate eigenvector may be returned
    
    oin = {[2],... 
           [1 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2],...
           [1 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2],...
           [1 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2]};
    M = {sym([0 0 1;0 0 1;0 0 0]),sym([0 0 1;0 1 0;1 1 1])};
    EXPECT_NTHROW( @() leadingeigenvector( M, oin, 'v',-1 ) );
    
    X = [0 0 0 0 0 0;-12 21 -12 18 0 0;5 15 9 12 -12 21;-21 35 16 -23 5 15;18 18 -12 -6 -21 35;0 0 0 0 18 18];
    [ ~, v0_sym, v0s_sym ] = EXPECT_NTHROW( @() leadingeigenvector( X, 'multipleeigenvector',1, 'v',1, 'complexeigenvector',2, 'simpleconjugate', 'cycle',0, 'sym',2 ), 'leadingeigenvector:numerical_maybe' );
    [ ~, v0_d, v0s_d ]     = EXPECT_NTHROW( @() leadingeigenvector( X, 'multipleeigenvector',1, 'v',1, 'complexeigenvector',2, 'simpleconjugate', 'cycle',0, 'sym',0 ), 'leadingeigenvector:numerical_maybe' );
    EXPECT_NEAR( v0_sym{1}, v0_d{1}, 1e-14 );
    EXPECT_TRUE( norm(vpa(v0s_sym{1}) - conj(v0s_d{1})) < 1e-14 || norm(vpa(v0s_sym{1}) - v0s_d{1}) < 1e-14 );
    
    M = sym([
         0, -91/388,    2/195, -79/300,    5/128,  -26/89,   19/280, 146/215
         0, -11/329,  -58/181,  1/3021, -215/607,    3/88,  295/482,  19/280
         0, -85/387,   -4/281, -59/242,    3/302, 112/153,     3/88,  -26/89
         0, -13/269, -124/419, -12/625,  245/363,   3/302, -215/607,   5/128
         0, -95/464,  -11/284, 294/379,  -12/625, -59/242,   1/3021, -79/300
         0, -11/174,  671/921, -11/284, -124/419,  -4/281,  -58/181,   2/195
         0, 367/453,  -11/174, -95/464,  -13/269, -85/387,  -11/329, -91/388
     1/160,       0,        0,       0,        0,       0,        0,       0]);
    % leadingeigenvector( M, 'multipleeigenvector',1, 'complexeigenvector',0, 'simpleconjugate', 'cycle',0 )
    [~, v] = EXPECT_NTHROW( @() leadingeigenvector( M ), 'leadingeigenvector:numerical_maybe', 'symbolic:sym:isAlways:TruthUnknown' );
    expect = [
     sym(     '-160.00060584385122883840440737091')
     sym( '74947832.458553726677987371863795'     )
     sym( '57562902.084321403191698572343739'     )
     sym('-17384738.582113985540000790751182'     )
     sym( '17384580.687907236416454866621075'     )
     sym('-57564027.695766080007316484159883'     )
     sym('-74948370.362508981382141233406093'     )
     sym(        '1.0'                            )];
    EXPECT_NEAR( v{1}/v{1}(end), expect, vpa('1e-22') );

SECTION( 'regression' );
    M={sym([0 0 1 0; 0 0 1 0; 0 0 0 1; 0 0 0 1]), sym([0 0 0 0; 0 0 0 0; 0 1 0 1; 0 1 0 1])};
    v0 = leadingeigenvector( M, [1;2], 'multipleeigenvector',1, 'v',-1, 'complexeigenvector',0, 'simpleconjugate',1, 'cycle',0, 'sym',1, 'double',0 );
    EXPECT_ISA( v0{1}, 'sym' );
    v0 = leadingeigenvector( M, [1;2], 'multipleeigenvector',1, 'v',-1, 'complexeigenvector',0, 'simpleconjugate',1, 'cycle',0, 'sym',2, 'double',0 );
    EXPECT_ISA( v0{1}, 'symstrict' );

    M=str2sym('[3, -3, 1; 0, 4, 0; -1, -3, 5]');
    [~, v0] = EXPECT_NTHROW( @() leadingeigenvector( M, {[1]},       'multipleeigenvector',1, 'v',1, 'complexeigenvector',3, 'simpleconjugate', 'cycle',0, 'sym',2, 'double',0, 'normalization','kone' ) );
    expected={str2sym('[1; -1/3; 0]'), str2sym('[1; 0; 1]')};
    EXPECT_EQ( v0, expected );

%% leadingeigenvalue
M = str2sym('[2779955938640937/9007199254740992, 4633259897734895/9007199254740992, 1, 5; 4633259897734895/9007199254740992, 2779955938640937/4503599627370496, 4, -3; 0, 0, 4569187569340983/72057594037927936, 5711484461676229/9007199254740992; 0, 0, 5711484461676229/9007199254740992, 2570168007754303/4503599627370496]');
ev = leadingeigenvalue( M );
EXPECT_NUMEL( ev, 1 );

%% makecandidate
TESTCASE
allfunctionhandle( 'makecandidate', 'assign', 'private',0, 'verbose',0 );
param = struct;
param.verbose = -1;
A = {[1 1 1;0 1 -1;1 -1 1],[0 0 1;1 1 0;-1 -1 1]};
param = initialize( A, param );
    
SECTION( 'problemtype' );
    ct = struct;
    ct.complexflag = false;
    p3 = 1/sym(3);
    p2 = 1/sym(2);
    p23 = 2/sym(3);
    xx = (2^p3*(5 - 111^p2*3i)^p23 - 4*(5 - 111^p2*3i)^p3)*(111^p2 + 9i)*(640 + 2^p23*(5 + 111^p2*3i)^p23 - 2^p3*111^p2*(5 + 111^p2*3i)^p3*1i + 2^p23*111^p2*(5 + 111^p2*3i)^p23*1i - 41*2^p3*(5 + 111^p2*3i)^p3)^p2*1i;
    [algorithm, retflag, str] = set_problem_type( xx, ct, param, '' );
    
    EXPECT_EQ( algorithm, 'p' );

%% makepositive
TESTCASE
syms xx
assume( xx, 'real' );

EXPECT_EQ( makepositive(sym([ 0 -1 2])), sym([0 1 -2]) );
EXPECT_EQ( makepositive( -abs(xx)-1 ), abs(xx)+1 );
EXPECT_EQ( makepositive( [xx-xx -abs(xx)-1] ), [xx-xx abs(xx)+1] );
EXPECT_EQ( makepositive( [0 -abs(xx)-1] ), [0 abs(xx)+1] );
[ret,a] = EXPECT_NTHROW( @() makepositive( -(xx*xx)-1 ) );
EXPECT_EQ( a, xx^2+1 );

syms yy
EXPECT_NTHROW( 'makepositive( yy )' );


%% polytopenorm
TESTCASE

SECTION( 'case R' );
    %%% the norm here seems to be exactly one
    pt = [
          (sym('27189068044651495')*2^(1/sym(2)))/sym('92442129447518208')
          (sym('31720401366221015')*2^(1/sym(2)))/sym('46221064723759104')
         -(sym('45314706506710825')*2^(1/sym(2)))/sym('46221064723759104')
         ];
    VV = [
           (55*2^(1/sym(2)))/72, -(115*2^(1/sym(2)))/216,  -(505*2^(1/sym(2)))/2592
           (35*2^(1/sym(2)))/36,  -(65*2^(1/sym(2)))/144, -(1355*2^(1/sym(2)))/1728
          -(25*2^(1/sym(2)))/36,  (175*2^(1/sym(2)))/144,  (1525*2^(1/sym(2)))/1728
          ];
    n = polytopenorm( pt, VV, 'rs', 'lb',sym(1), 'v',-1 );
    EXPECT_EQ( n(2), sym(1) );

    %val = polytopenorm( sym(pt), sym(VV), 'rS', 'v',-1 );
    %EXPECT_EQ( val(1,:), sym([1 1/2 3/2]) );

    nrm = polytopenorm( [2 2].',[2 0;1 1;0 2].', 'rS', 'v',-1 );
    EXPECT_EQ( nrm(2), sym([2].') );
    EXPECT_EQ( polytopenorm([2 2].',[2 0;1 1;0 2].', 'rS', 'v',-1), sym([2 2 2].') );

    nrm = polytopenorm( [1 1].',[2 0;1 1;0 2].', 'rS','v',-1 );
    EXPECT_EQ( nrm(2), sym([1].') );
    EXPECT_EQ( polytopenorm( [1 1].',[2 0;1 1;0 2].', 'rS', 'v',-1 ), sym([1 1 1].') );

    nrm = polytopenorm( [0 0].',[2 0;1 1;0 2].', 'rS','v',-1 );
    EXPECT_EQ( nrm(2), sym([0].') );
    EXPECT_EQ( polytopenorm( [0 0].',[2 0;1 1;0 2].', 'rS', 'v',-1), sym([0 0 0].') );

SECTION( 'degenerate' )
    %EXPECT_EQ( polytopenorm( [1 2 -1]', [0 2 0;2 2 -2]', 'rS', 'v',-1 ), sym([1 1 1].') );

    [normval, iv, iter, time, proofed] = polytopenorm( [1 1; 1 0], [1 0; 0 1], 'pns', 'v',-1 );
    EXPECT_LE( normval(3,:), [2 1], normval(2,:) );

    pts = [ 2 1 ]';
    VV = [10 0]';
    nrm = polytopenorm( pts, VV, 'rns',  'v',0, 'bound',[0 1] );
    EXPECT_LE( nrm(3), nrm(1), nrm(2) );


SECTION( 'division by zero' )
    vertextotest2 = [  -0.995959313953112;  -1.611496021388163];
    VV2 = [ 0.850650808352040   0.995959313953112;  -0.525731112119134   1.611496021388163];

    s2 = sqrt( sym(2) );
    s10 = sqrt( sym(10) );
    s5 = sqrt( sym(5) );
    
    pts_sym2{1} = [-1;2;1];
    pts_sym2{2} = {sym([1 -2;-2 -1]), sym([2 -1;3 -2])};
    pts_sym2{3}{1} = [((5*s2 + s10)*(5 - s5)^(1/2))/20; -(s10*(5 - s5)^(1/2))/10];
    pts_sym2{4} = s5;
    
    VV_sym2{1} = [-1 0;-1 2]';
    VV_sym2{2} = pts_sym2{2};
    VV_sym2{3} = pts_sym2{3};
    VV_sym2{4} = pts_sym2{4};
    
    if( gurobi_inst );
        normval = polytopenorm( vertextotest2, VV2, 'r', 'g', 'pts',pts_sym2, 'VV', VV_sym2, 'numcore',1, 'epsilon_display',1e-9, 'v',0, 'bound',[0 1] );
        EXPECT_NTHROW( @() double(normval) ); end;

SECTION( 'underflow tests' )
    MESSAGE( 'sol', 'alg' );
    solver = {};
    if( gurobi_inst );
        solver = [ solver 'g']; end;
    if( optim_inst );
        solver = [ solver 'm']; end;
    for sol = solver;
        sol = sol{1};  %#ok<FXSET>
        for alg = {'p','r'}
            alg = alg{1};  %#ok<FXSET>
            
            p = [0;0];
            VV = randncp( 2, 5 );
            switch alg;
                case 'p'; VV = abs( VV );
                case 'r'; VV = real( VV ); end;
            VV = round( 100*VV );
            [check, nrm] = EXPECT_NTHROW( @() polytopenorm( p, VV, [alg 'S'], sol, 'v',-1 ), 'polytopenorm:unimplemented' );
            if( ~isempty(nrm) );
                EXPECT_EQ( nrm(3), sym(0) ); end;
            end; end;

SECTION( 'case P' );
    VV = [2 2 2; 0 4 2; 4 0 0; 4 0 0;2 2 0;2 2 2;3 1 1].';
    pt = [2 2 1;0 2 1;0 2 2].';
    val = polytopenorm( pt, VV, 'pS', 'v',-1 );
    EXPECT_EQ( val(2,:), sym([1 1/2 1]) );

    nrm = polytopenorm( [0 0].',[2 0;1 1;0 2].', 'pS', 'v',-1 );
    EXPECT_EQ( nrm(2), sym([0].') );
    EXPECT_EQ( polytopenorm( [0 0].',[2 0;1 1;0 2].','pS', 'v',-1 ), sym([0 0 0].') );

    V = [2 1 1;1 2 1;1 1 2]'; p=[321; 156; 684];
    nrm = polytopenorm( p, V, 'pNS', 'v',0 );
    TODO_FAIL( 'add this test' );
    
    V=[93, 125, 571, 473; 36, 237, 674, 110; 365, 3, 340, 310; 527, 753, 597, 242];
    p=[328; 181; 264; 333];
    nrm = polytopenorm( p, V, 'pNS', 'v',0 );
    TODO_FAIL( 'add this test' );

SECTION( 'regression tests' );
    SUBSECTION;
    pts2 = [ 0.5257311121191336                    0
             0.2008114158862273   0.7265425280053609
             0.7265425280053609   0.5257311121191337];
    VV2 =  [                  0   0.32491969623290635   0.52573111211913360
             0.8506508083520399   0.52573111211913360   0.32491969623290635
             0.5257311121191336   0.85065080835203992   0.85065080835203990];
    s2 = sqrt(sym(2));
    s5 = sqrt(sym(5));
    s10 = sqrt(sym(10));
    pts_sym2{1} = [-1 1 1 1;-1 1 1 2]';
    pts_sym2{2}{1} = sym([0 0 1;0 1 0;0 1 1]);
    pts_sym2{2}{2} = sym([0 0 0;0 1 1;1 1 0]);
    pts_sym2{3}{1} = [0;   ((5*s2 + s10)*(5 - s5)^(1/2))/20; (s10*(5 - s5)^(1/2))/10];
    pts_sym2{4} = s5/2+1/2;
    VV_sym2 = pts_sym2;
    VV_sym2{1} = [-1 0 0;-1 1 0;-1 1 1]';
    [ ret, normval, iv, iteration, normtime, proofed ] = EXPECT_NTHROW( @() polytopenorm( pts2, VV2, 'p', 'a', 'pts',pts_sym2, 'VV', VV_sym2, 'bound',[0 1] ) );

%% jointTriangult
TESTCASE( 'jointTriangult' );
%%% set with matrices not having full set of eigenvectors during computation
[~,tr] = EXPECT_NTHROW( @() jointTriangult( {sym( 1/2*[1 1;1 1] ),sym( [0 3/2;0 0] )} ), 'jointTriangult:multiplicities' );
EXPECT_FALSE( tr );

%%% another bad set
EXPECT_NTHROW( @() jointTriangult( {vpa([0 0 0;0 0 1;0 0 0]),vpa([0 0 1;1 0 0;0 1 -1])} ) );


%% rhot
TESTCASE( 'rhot' );
    syms xx
    assume( xx, 'real' );
    
    EXPECT_ALMOST_EQ( rhot(sym([1 0;0 -2])), 2 );
    EXPECT_EQ( rhot(xx), abs(xx) );
    % TODO_EQ( rhot([xx 1;0 -1]), max(1, abs(xx)) );  % maybe this works in a future Matlab Release
   

    A = sym([0 1 0;1 0 1;0 1 1]);
    res = 7/(9*(- 7/54 + (108^(sym(1)/2)*7i)/108)^(sym(1)/3)) + ((108^(sym(1)/2)*7i)/108 - 7/54)^(sym(1)/3) + sym(1)/3;
    EXPECT_EQ( rhot(A), res );
    EXPECT_EQ( rhot({A A}), [res res]);


%% end
ENDTESTCASE();

%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*NBRAK>
 