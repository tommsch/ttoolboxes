% test subdivision

%#ok<*NASGU>
%#ok<*CTPCT>
%#ok<*ASGLU>

% preconditions

TTEST init subdivision

%% blf
TESTCASE
S = getS( '2_butterfly' );
blf( S, 'iteration',3, 'wavelet', 'v',0 ); 
blf( S, 'iteration',3, 'diff',1, 'verbose',0 ); 

S = getS('1_121');
blf( S, 'plot',0, 'verbose',0 );

[c,PM,xyzv,oo] = blf( {[],1}, S, 'start',[1 -1]', 'removezero',1, 'start',[0 0 0 1 0 0 0]', 'iteration',5, 'verbose',0 );
title( 'blf -- There should be a hat function.' );
EXPECT_EQ( PM, 32 );
EXPECT_EQ( oo, {[],[1]} );
EXPECT_EQ( size(xyzv,1), 2 );

EXPECT_THROW( 'blf( [1 1 1], S, ''iteration'', 4, ''v'',0 );', 'blf:oolength' );

%% checktile
TESTCASE
M1 = [1 2; -2 -2];  D1 = [0 1; 0 -1]; 
M2 = [-1 1; -1 -2]; D2 = [0 0 0; -2 -1 0]; 
M12 = M2*M1; 
D12 = setplus( M2*D1, D2 );

f1 = checktile( M1, D1, 'verbose',-1 );
f2 = checktile( M2, D1, 'verbose',-1 );
[~, f12] = EXPECT_NTHROW( @() checktile( M12, D12, 'verbose',-1 ), 'checktile:sym' );
EXPECT_EQ( [f1 f2 f12], [true, true, false] );

%% constructdigit
TESTCASE
EXPECT_EQ( size(constructdigit( [2 0; 1 2], 'random',4)),[2,4]);
EXPECT_EQ( constructdigit( [2 0; 1 2] ),[0 0 1 1;0 1 1 2]);
EXPECT_PRED( @isempty, constructdigit( 0 ) );
EXPECT_NTHROW( 'constructdigit( 1,''random'',2);' );
EXPECT_THROW( 'constructdigit( [10 9;11 9],''classify'');', 'constructdigit:option', '''ZZ''' );
EXPECT_THROW( 'constructdigit( [10 9;11 9],''classify'',''ZZ'',[0 0;1 2;10 15]'',''random'',3);', 'constructdigit:option', '''random''' );


%% constructOmega
TESTCASE
S = getS('1_1331');
Om = constructOmega(S,'Omega',[4 3 2 1 0],'stable');
EXPECT_EQ( Om,[4 3 2 1 0]);
EXPECT_NTHROW( 'constructOmega(getS(''2_butterfly''),''plot'');' );
EXPECT_NTHROW( 'constructOmega(S);' );
EXPECT_THROW( 'constructOmega(S,''Om'',[1 1.5]);', 'constructOmega:integer' );
EXPECT_THROW( @() constructOmega(S,'Om',[1;2]), 'constructOmega:dim' );

%% constructV_constructVt
TESTCASE
EXPECT_EQ( constructV([],3), [] );
EXPECT_EQ( constructV([],3), [] );
EXPECT_EQ( constructVt([]), {[]} );
EXPECT_EQ( constructVt([]), {[]} );

Om = [0 1 2 3];
EXPECT_PRED( @isempty, constructV(Om,5) );

[V,Om] = constructVt( [1 1 0 1 1], 2, '01' );
EXPECT_PRED( @isempty, V );
EXPECT_EQ( Om, [0 0 0 0;0 1 3 4] );

[V,Om] = constructVt( [1 1 0 1 1], 2, '01' );
EXPECT_PRED( @isempty, V );
EXPECT_EQ( Om, [0 0 0 0;0 1 3 4] );

[V,Om] = constructVt( [1 1 0 1 1], 2, '01' );
EXPECT_PRED( @isempty, V );
EXPECT_EQ( Om, [0 0 0 0;0 1 3 4] );

Om = [0 0;2 2; 0 1;1 2;0 2]';
Vt = constructVt( Om, 1 ); 
V = constructV( Om, 1 );
EXPECT_EQ( rank(intersectspace(V,Vt)), 2 );

EXPECT_EQ( size(constructVt([1 1 1;1 1 1;1 1 1],4,'01')), [9 0] );

EXPECT_NTHROW( @() constructVt([0;0;0;0]) );
EXPECT_NTHROW( @() constructV([0;0;0;0]) );


%% getS
TESTCASE
SECTION
EXPECT_THROW( @() getS( 'a',1/2*[1 2 1],'M',2), 'getS:transpose', 'o','getS:dim' );
EXPECT_THROW( @() getS( 'a',[1 2 1]','M',2), 'getS:sumrule0' );
EXPECT_THROW( @() getS( 'a',1/2*[1 1]','D',[0 2]), 'getS:M', 'o','getS:dim' );

EXPECT_NTHROW( 'getS(''a'',[1 2 1],''M'',2,''name'',''tommsch'',''nocheck'');' );
S1 = getS('a',1/2*[1 2 1]','M',2);
EXPECT_EQ( S1,getS(S1) );
EXPECT_EQ( S1,getS({1/2*[1 2 1]',2}) );
EXPECT_NTHROW( 'getS(''1_121'');' );
EXPECT_NTHROW( 'getS(S1,''supp'');' );
EXPECT_NTHROW( 'getS([S1; S1],''supp'');' );
EXPECT_NTHROW( 'getS(S1,''OmegaRR'');' );
EXPECT_NTHROW( 'getS(S1,''characteristic'');' );
[~, val] = EXPECT_NTHROW( @() getS( '1_rand' ), 'constructdigit:sym', 'normalizeS:support', 'normalizeS:scaling' );
EXPECT_NTHROW( 'getS(val);' );
EXPECT_NTHROW( @() getS('2_rand'), 'normalizeS:support', 'normalizeS:scaling', 'constructdigit:sym' );
[~,S2] = EXPECT_NTHROW( @() getS('2_rand'), 'constructdigit:sym', 'normalizeS:support', 'normalizeS:scaling' );

EXPECT_THROW( 'getS([S1;S2]);', 'getS:dimM' );
S2 = getS( '1_1133' );
S = getS( [S1;S2] );

EXPECT_THROW( 'getS(100,''nocheck'');', 'getS:nodata' );

val = getS('1_143'); 
EXPECT_ALMOST_EQ( val{1}.c, 1/4*[1;4;3] );
EXPECT_NTHROW( 'getS(val);' );
EXPECT_NTHROW( 'getS(''a'',1/2*[1 2 1]'',''M'',2); ' );
EXPECT_EQ( val{3},[0 1]);
EXPECT_NTHROW( 'getS(S1);' );


%% num2ordering
TESTCASE
SECTION
[doo, doint, num, proofnum] = num2ordering( [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1], getS('M',2), .4, 'proof', 'v',0 );
EXPECT_ALMOST_EQ( proofnum, .4 );

Snum = getS( {[],[1 2; -1 2]} );
pt = [0.1875;0.375];
num = ordering2num( num2ordering(Snum,pt,'l',50,'verbose',0,'plot',1), Snum );
EXPECT_ALMOST_EQ( num, pt );
EXPECT_THROW( 'num2ordering(Snum,[1 2],''check'',1)', 'num2ordering:pointoutside' );

[doo,doint,num,proofnum] = num2ordering( Snum,pt, 'check',1, 'proof','verbose',0 );
EXPECT_EQ( doo, {[1 1;2 3],[1 1;1 2]} );
EXPECT_EQ( proofnum, num);
Sn = {[],2,[0 1]; [],2,[0 3]};

EXPECT_NTHROW( 'ordering2num( num2ordering({[],[1 2]},Sn,5/3,''plot'',1,''verbose'',0,''l'',10,''delta'',.7,''round'',0), Sn );' );

S = getS( {[1 1]',2}, 'nocheck' );
EXPECT_THROW( 'num2ordering( [1 1 1 1 1 1 1 1 1 1 1 1 1 1 1], Snum, [.4 .2], ''v'',0 );', 'num2ordering:period' );
EXPECT_THROW( 'num2ordering( [S;S], [.4 .2], ''v'',0 );', 'num2ordering:argument' );
EXPECT_THROW( 'num2ordering( getS(''M'',10), 0.1133333333333333333333333333, ''l'',4 );', 'num2ordering:period' );


%% ordering2num
TESTCASE
EXPECT_ALMOST_EQ( ordering2num({[1 2],[1 2]},10), 0.010101010101010101 );
EXPECT_NTHROW( 'ordering2num( {[1 2],[1 2]}, [1 2;2 2] );' );
EXPECT_NTHROW( 'ordering2num( {[2 1],[1 3]}, getS({[],[1 2; -1 1]}) );' );
num = ordering2num( {[],[1 2]},getS({[],2}) );
EXPECT_EQ( num, 1/3 );
num = ordering2num( {[],[]},getS({[],2}) );
EXPECT_EQ(num,0);

S = getS( {[],2} );
EXPECT_THROW( 'ordering2num( {[2 1 ]}, S );', 'ordering2num:input', '''oo''' );
EXPECT_THROW( 'ordering2num( {[1],[2 1 ]}, [S;S] );', 'ordering2num:input', '''oo''' );


%% tilearea
TESTCASE
S = getS( '2_butterfly' );
tilearea( S, 'verbose',0 );
tilearea( getS('M',3,'D',[0 1 5]), 'v',0, 'visual' );        
tilearea( tile(S,'plot',0,'v',0), 'v',0, 'visual' );

%%
ENDTESTCASE();

%#ok<*NOSEL,*NOSEMI,*ALIGN>

