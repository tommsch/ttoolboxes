% test sequence

TTEST init sequence_sym
ASSUME_TOOLBOX( 'symbolic', OPTION('load') );
% preconditions

%% mask2symbol/symbol2mask
TESTCASE
syms x y z z1 z2 a b;
[ret, mask] = EXPECT_NTHROW( @() symbol2mask( mask2symbol([1 2 3; 4 5 6]) ), TODOED('onoctave') );
if( ret );
    EXPECT_EQ( mask, [1 2 3; 4 5 6] ); end;
EXPECT_NO_THROW( 'mask2symbol([1]);' );
val = symbol2mask( mask2symbol([]) );
EXPECT_TRUE( isempty(val) || isequal(val,0) );
EXPECT_EQ( mask2symbol([1 1],'dim',1), 1 + z1 );
EXPECT_EQ( mask2symbol([1 1],'var','x'), 1 + y );
EXPECT_EQ( mask2symbol([1 1],'var',{'b','a'}), 1 + a );
val = mask2symbol([1 1],'amin',[0;1]);
EXPECT_EQ( val, z2^2+z2 );

[ret, val] = EXPECT_NTHROW( @() symbol2mask(val,'dim',2,'sym','var','z1'), TODOED('onoctave') );
if( ret );
    EXPECT_EQ( val, sym([0 1 1]) ); end;

