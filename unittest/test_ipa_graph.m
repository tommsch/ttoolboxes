% test ipa_lsr

%#ok<*CTPCT>
%#ok<*ASGLU>
%#ok<*NASGU>

TTEST init test_ipa_graph

EXPECT_TOOLBOX( 'matlab', '>=R2016b', APPLIED('onmatlab') );
EXPECT_TOOLBOX( 'octave', '>=6.4', APPLIED('onoctave') );

%%
TESTCASE( 'graph_precomponent' );
X = logical( [0 0 0 1;1 0 1 1;0 1 1 0; 1 0 0 1 ] );
EXPECT_EQ( graph_precomponent( X ), [1 1 1 1;1 1 1 1] );

EXPECT_EQ( graph_precomponent( [0 1;1 0] ), [1 2;2 1] );

EXPECT_EQ( graph_precomponent( [0] ), [1;0] );
EXPECT_EQ( graph_precomponent( [] ), zeros(2,0) );

%%
TESTCASE( 'genNecklacest' );
G = [0 0 1 1;0 0 1 1;1 1 0 0;1 1 0 0];
n_fkm41 = genNecklacest( 2, 4, G );
EXPECT_EQ( n_fkm41, [1 3;1 4;2 3;2 4]' );

n_fkm42 = genNecklacest( 3, 4, G );
EXPECT_EMPTY( n_fkm42, [] );

G = [0 1 1;1 0 0;1 0 0];
n_fkm31 = genNecklacest( 2, G, false );
EXPECT_EQ( n_fkm31, [1 2;1 3]' );

n_fkm32 = genNecklacest( 3, G, true );
EXPECT_EMPTY( n_fkm32 );

n_fkm33 = genNecklacest( 3, G, false );
EXPECT_EQ( n_fkm33, [1 2 1;1 3 1;2 1 2;3 1 2;2 1 3;3 1 3]' );  % ordering actually is allowed to be different, but for the time beeing, this suffices.

%%
TESTCASE( 'findsmp' );
SECTION( 'general' );
    MM = {1/3 2 1/3 1/3 2};
    GG = [0 1 1 0 0;0 0 0 1 1;0 0 0 1 1;1 1 1 0 0;0 0 0 1 0];
    [c, ~, info] = findsmp( MM, 'graph',GG, 'verbose',-1 );
    EXPECT_EQ( c, {[2 5 4]'} );
    EXPECT_ALMOST_EQ( min(info.jsrbound), (2*2/3)^(1/3) );

    MM = {3 2 1 4};
    weigth = [1 1/2 0 2];
    GG = [0 1 1 0;0 0 1 0;1 0 1 1;1 0 0 1];
    [c, ~, info] = findsmp( MM, 'graph',GG, 'toa',weigth, 'delta',1, 'verbose',-1 );
    EXPECT_EQ( c(1:2), {[1 2 3]' [1 2 3 3]'} );
    EXPECT_ALMOST_EQ( min(info.jsrbound), (2*3*1)^(1/(1+.5+0)) );

%% 
TESTCASE( 'graph_precomponent' );
fignum = 1212590124;
G = [1 0 0 1 0 0 0 0 0 0;0 0 0 0 1 0 0 0 1 0;0 0 0 1 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0;0 1 1 0 1 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0;0 0 0 0 0 0 0 0 0 0;0 0 0 0 1 0 0 0 0 0;0 0 0 0 1 0 0 0 0 0];
figure(fignum);
[~, fromto] = EXPECT_NTHROW( @() graph_precomponent( G, [], 2 ) );
EXPECT_EQ( fromto, [1 2 2 1 2 3 4 5 2 6;1 2 1 0 2 0 0 0 2 2] );

G = [1 1 0 0 0 0 0 0 0;0 0 0 0 1 0 0 0 0;1 1 0 0 0 0 0 0 0;0 0 1 0 0 0 0 0 0;0 0 0 0 1 0 0 0 0;1 1 0 0 0 0 0 0 0;0 0 0 0 0 0 1 0 0;0 0 0 0 0 0 1 0 0;0 0 0 0 0 0 0 1 1];
[~, fromto] = EXPECT_NTHROW( @() graph_precomponent( G, [], 2 ) );
EXPECT_EQ( fromto, [1 1 2 3 4 5 6 7 7;1 4 1 2 4 1 6 6 7]);
close( fignum )

TESTCASE( 'isgraph_yields_invariant_polytope' ); 
EXPECT_FALSE( isgraph_yields_invariant_polytope( [0 1 0 0;1 0 0 1;0 0 0 1;0 1 1 0] ) );
EXPECT_TRUE( isgraph_yields_invariant_polytope( 1 ) );
EXPECT_NTHROW( @() isgraph_yields_invariant_polytope( [] ) );


ENDTESTCASE();



%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*NBRAK2>
