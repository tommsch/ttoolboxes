% test sequence

%#ok<*CTPCT>
%#ok<*ASGLU>
%#ok<*NASGU>

TTEST init test_kone
% preconditions

ASSUME_TOOLBOX( 'gurobi' );


%% nullspace
TESTCASE( 'nullspace' );
EXPECT_EQ( nullspace_kone( [1 0] ), [0] );
EXPECT_EQ( nullspace_kone( [1 0]' ), [0 1]' );

center = [0.49851888925343141;-0.86437324307648689];
EXPECT_THROW( @() nullspace_kone( center ), 'nullspace_kone:normalize' );

function dummy; end  %#ok<DEFNU>  % Generates an error, if the 'end' of a function is missing.
%#ok<*NOSEL,*NOSEMI,*ALIGN>
%#ok<*NBRAK1,*NBRAK2,*NBRAK>
