% test sequence

%#ok<*CTPCT>
%#ok<*ASGLU>
%#ok<*NASGU>

TTEST init ipa_complex

EXPECT_TOOLBOX( 'matlab', '>=R2016b', APPLIED('onmatlab') );
EXPECT_TOOLBOX( 'octave', '>=6.4', APPLIED('onoctave') );

sedumi_inst = tt.expect_toolbox( 'sedumi' );
gurobi_inst = tt.expect_toolbox( 'gurobi' );
optim_inst  = tt.expect_toolbox( 'optimization' );
assert( sedumi_inst || gurobi_inst, 'At least, either SeDuMi or Gurobi must be installed.' );

solver = {};
if( gurobi_inst );
    solver = [ solver 'g']; end;
if( sedumi_inst );
    solver = [ solver 's']; end;

% preconditions


%% complexfunct_d_helper
TESTCASE
h = allfunctionhandle( 'makepositive', 'complexfunct_d_cut_vertex', 'private', 'v',0 );
%%% bad configurations
SECTION
    EXPECT_THROW( @() h( [2 2], [3 3] ),                  'MATLAB:narginchk:notEnoughInputs' );
    EXPECT_THROW( @() h( [0 .5 1], [2 2], [3 3] ),        'complexfunct_d_cut_vertex:dimension' );
    EXPECT_THROW( @() h( [.5 .5], [2 0], [3 -3] ),        'm','complexfunct_d_cut_vertex:position:inside', 'm', 'complexfunct_d_cut_vertex:position:intersect' );
    EXPECT_THROW( @() h( [-1.2 0], [0 1.2], [2 1.2] ),    'complexfunct_d_cut_vertex:position:intersect' );
    EXPECT_THROW( @() h( [-1.2 0.1], [0 1.2], [2 1.2] ),  'complexfunct_d_cut_vertex:position:intersect' );
    EXPECT_THROW( @() h( [-1.2 -0.1], [0 1.2], [2 1.2] ), 'complexfunct_d_cut_vertex:position:intersect' );
    EXPECT_THROW( @() h( [3 2], [0 2], [3 -2] ),          'complexfunct_d_cut_vertex:position:nonconvex','complexfunct_d_cut_vertex:position:side' );


%%% good configurations
SECTION
    [ba, bc] = h( [2 0], [0 2], [-2 0] );
    EXPECT_EQ( ba, [1;1] );
    EXPECT_EQ( bc, [-1;1] );

    [ba,bc] = h( [1 -1], [1 1], [-1 1] );
    EXPECT_ALMOST_EQ( ba, [1;sqrt(2)-1] );
    EXPECT_ALMOST_EQ( bc, [sqrt(2)-1;1] );

    [ba,bc] = h( [1 -1], [2 0], [1 1] );
    EXPECT_EQ( ba, [1;-1] );
    EXPECT_EQ( bc, [1;1] );

    [ba,bc] = h( [-sqrt(3) -1], [0 2], [sqrt(3) -1] );
    EXPECT_ALMOST_EQ( ba, [-sqrt(3)/3;1] );
    EXPECT_ALMOST_EQ( bc, [sqrt(3)/3; 1] );


%% complexleadingmatrix
TESTCASE
EXPECT_THROW( @() complexleadingmatrix(0, false), 'complexleadingmatrix:dim' );
EXPECT_THROW( @() complexleadingmatrix(1, false), 'complexleadingmatrix:dim' );
EXPECT_MAXTIME( @() complexleadingmatrix( 150, true ), 2 );



%% polytopenorm
TESTCASE
SECTION
    MESSAGE( 'sol' );
    for sol_ = solver;
        sol = sol_{1};
        
        switch sol;
            case 'g','m'; acc = 1;
            case 's'; acc = 50;
            otherwise; fatal_error;
            end;

        pts0 = zeros( 3, 0 );
        pts1 = [3 1 9].';
        pts2 = [3 1 9;2 1 3].';
        pts3 = [3 1 9;2 1 3;1 1 1].';
        pts1000 = [pts3 randi( 9, 3, 1000 )];
        VV = [9 9 6;8 2 3;7 6 18].';

        
        n0 = polytopenorm( pts0, VV, 'c1', 'v',-1, sol );
        n1 = polytopenorm( pts1, VV, 'c1', 'v',-1, sol );
        n2 = polytopenorm( pts2, VV, 'c1', 'v',-1, sol );
        n3 = polytopenorm( pts3, VV, 'c1', 'v',-1, sol );
        n1000 = polytopenorm( pts1000, VV, 'c1', 'v',-1, sol );

        
        n0 = polytopenorm( pts0, VV, 'c2', 'v',-1, sol );
        n1 = polytopenorm( pts1, VV, 'c2', 'v',-1, sol );
        n2 = polytopenorm( pts2, VV, 'c2', 'v',-1, sol );
        n3 = polytopenorm( pts3, VV, 'c2', 'v',-1, sol);
        n1000 = polytopenorm( pts1000, VV, 'c2', 'v',-1, sol );


        %%% algorithm 'c'
        
        VV = randn( 10, 100 );
        p = randn( 10, 50 );
        nca_s = polytopenorm( p, VV, 'c1', sol, 'v',-1 );
        ncb_s = polytopenorm( p, VV, 'c2', sol, 'v',-1 );
        nr_s = polytopenorm( p, VV, 'r', sol, 'v',-1 );
        if( optim_inst );
            nr_m = polytopenorm( p, VV, 'r', 'matlab', 'v',-1 ); 
        elseif( sedumi_inst );
            nr_m = polytopenorm( p, VV, 'r', 'sedumi', 'v',-1 ); 
        elseif( gurobi_inst );
            nr_m = polytopenorm( p, VV, 'r', 'gurobi', 'v',-1 ); 
        else;
            nr_m = nr_s; end;
        EXPECT_NEAR( nca_s(1,:), ncb_s(1,:), nca_s(1,:), nr_m(1,:), 3e-4 );


        %%% zeros
        
        val = polytopenorm( [0;0], [0 1;1 0]', 'c1', sol, 'v',-1 );
        EXPECT_LE( val(1,:), 0.1 );
        val = polytopenorm( [0;0], [0 1;1 0]', 'c2', sol, 'v',-1 );
        EXPECT_LE( val(1,:), 0.1 );    

        VV = randn( 10, 30 ) + 1i*randn( 10, 30 );
        p = randn( 10, 19 ) + 1i*randn( 10, 19 );
        nca1 = polytopenorm( p, VV, 'c1', 'v',-1, sol );
        nca2 = polytopenorm( 2*p, VV, 'c1', 'v',-1, sol );
        nca3 = polytopenorm( 3*p, VV, 'c1', 'v',-1, sol );
        EXPECT_NEAR( nca1, nca2/2, nca3/3, 0.00045 );

        VV = randn(10,30) + 1i*randn(10,30);
        p = randn(10,19) + 1i*randn(10,19);
        ncb1 = polytopenorm( p, VV, 'c2', 'v',-1, sol );
        ncb2 = polytopenorm( 2*p, VV, 'c2', 'v',-1, sol );
        ncb3 = polytopenorm( 3*p, VV, 'c2', 'v',-1, sol );
        EXPECT_NEAR( ncb1, ncb2/2, ncb3/3, 6e-4 );

        val = polytopenorm( 1, 1i, 'c1', 'v',-1, sol );
        EXPECT_NEAR( val(1,:), 1, 5e-5 );
        val = polytopenorm( 1, [1+1i], 'c1', 'v',-1, sol );
        EXPECT_NEAR( val(1,:), 1/sqrt(2), 5e-5 );
        val = polytopenorm( 1, 1i, 'c2', 'v',-1, sol );
        EXPECT_NEAR( val(1,:), 1, 5e-5 );
        val = polytopenorm( 1, [1+1i], 'c2', 'v',-1, sol );
        EXPECT_NEAR( val(1,:), 1/sqrt(2), 5e-5 );    

        VV = [1+2i -1+20i; 3+4i -3+40i; 5+6i -5+60i].';
        p = [1+2i -1+20i;3+4i -3+40i].';
        polytopenorm( p, VV, 'c1', 'v',-1, sol );
        polytopenorm( p, VV, 'c2', 'v',-1, sol );

        VV = [1 0; 0 1i].';
        p = [1i 1i].';
        polytopenorm( p , VV, 'c1', 'v',-1, sol );
        polytopenorm( p , VV, 'c2', 'v',-1, sol );

        val = polytopenorm( [1i 1].', [2 0;0 2i].', 'c2', 'v',-1, sol );
        EXPECT_NEAR( val(1,:), 1, 5e-5 ); %the real norm is 1/sqrt(2) but 'c2' computes it to 1

        val = polytopenorm( [1i -1].', [-2 1i;1i 2].', 'c1', 'v',-1, sol );
        EXPECT_NEAR( val(1,:), 2, 5e-5 ); %the real norm is sqrt(5/2) but 'c2' computes it to 2

        val = polytopenorm( [1i 1].', [-2 1i;1i 2].', 'c2', 'v',-1, sol );
        EXPECT_NEAR( val(1,:), 2/3, 5e-5 ); %the real norm is sqrt(5/2) but 'c2' computes it to 2/3    

        val = polytopenorm( [ 1i -1].', [ -2  1i; 1i  2].', 'c2', 'v',-1, sol );
        EXPECT_NEAR( val(1,:), 2/3, 5e-5 ); %the real norm is sqrt(2/5) but 'c2' computes it to 2/3

        % %XX test to be added if we have correct version of type 'c'
        % %norm should be approx. 6.61532
        %p = [2+6i -6-20i].'; 
        %VV = [2 -2i;1-2i -1+1i;-1+2i -3+7i].';
        %val = polytopenorm( p, VV, 'c', 1, 0, -1, 'm' );     


        VV = randn( 5, 100 ) + 1i*randn( 5, 100 );
        pp = randn( 5, 20 ) + 1i*randn( 5, 20 );
        nc = polytopenorm( pp, VV, 'c', 'v',-1, sol, 'relaccuracy',0.99999 );
        nca = polytopenorm( pp, VV, 'c1', 'v',-1, sol );
        ncb = polytopenorm( pp, VV, 'c2', 'v',-1, sol );
        nest = polytopenorm( pp, VV, 'c', 'estimate',1, 'v',-1, sol, 'relaccuracy',0.99999 );
        EXPECT_TRUE( allm( nc(1,:)   <= nc(2,:)   ) );
        EXPECT_TRUE( allm( nc(1,:)   >= nc(3,:)   ) );
        EXPECT_TRUE( allm( nca(1,:)  <= nca(2,:)  ) );
        EXPECT_TRUE( allm( nca(1,:)  >= nca(3,:)  ) );
        EXPECT_TRUE( allm( ncb(1,:)  <= ncb(2,:)  ) );
        EXPECT_TRUE( allm( ncb(1,:)  >= ncb(3,:)  ) );
        EXPECT_TRUE( allm( nest(1,:) <= nest(2,:) ) );
        EXPECT_TRUE( allm( nest(1,:) >= nest(3,:) ) );
        nc = num2cell( nc([3 2],:)', 2 );
        nca = num2cell( nca([3 2],:)', 2 );
        ncb = num2cell( ncb([3 2],:)', 2 );
        nest = num2cell( nest([3 2],:)', 2 );
        val = cellfun( @(x,a,b,e) ~isempty(intersectinterval(x, a, b, e)), nc, nca, ncb, nest )';
        EXPECT_TRUE( all(val) );

        
        pt = [1/2 + 1i*1/2; 1/2 + 1i*1/2];
        VV = [sqrt(2)/2; -1i*sqrt(2)/2];
        [ normval, iv, iterations ] = polytopenorm( pt, VV, 'c2', 'v',-1, sol );
        EXPECT_NEAR( normval(1), sqrt(2), 5e-5 );

        
        pt = [1;0;2];
        VV = [1;0;0];
        polytopenorm( pt, VV, 'c2', 'lb',1, 'v',-1 );     

    end

    SECTION( 'bugs from examples' );
        EXPECT_NEAR( polytopenorm( [0 0]', [1 1;0 1]', 'cE', 'v',-1 ), [0;0;0], 1e-12 );

    SECTION( 'underflow tests' );
    MESSAGE( 'sol', 'alg' );
    for sol = solver;
        sol = sol{1};  %#ok<FXSET>
        for alg = {'c1','c2','c3','c4','c5'}
            alg = alg{1};  %#ok<FXSET>
            
            p = [0;0];
            VV = round( 100*randncp( 2, 5 ) );
            [check, nrm] = EXPECT_NTHROW( @() polytopenorm( p, VV, [alg 'N'], sol, 'v',-1 ), 'polytopenorm:unimplemented', 'polytopenorm:sedumi_c4', 'polytopenorm:gurobi_c5' );
            if( ~isempty(nrm) );
                EXPECT_EQ( nrm(3), 0 ); end;
                end; end;


%%
TESTCASE( 'polytopenorm_inf' )
for sol = solver;
    sol = sol{1};  %#ok<FXSET>
    % commands shall not throw
    pt = [inf 0;0 inf];
    VV = [inf 0;0 inf];
    [ normval, iv, iterations ] = polytopenorm( pt, VV, 'c1', 'v',-1, sol );
    [ normval, iv, iterations ] = polytopenorm( pt, VV, 'c2', 'v',-1, sol ); end;

%% polytopenorm_complex_d
TESTCASE
MESSAGE( 'dim' );
for dim_ = {1,2,3,10}; dim = dim_{1};
    for nVV_ = {0,1,10,50};
        nVV = nVV_{1} + dim;
        pt = randncp( dim, 1 );
        VV = randncp( dim, nVV );
        nc = polytopenorm( pt, VV, 'c3', 'aux',20, 'numthread',1, 'v',0 );
        nd = polytopenorm( pt, VV, 'c4', 'numthread',1, 'v',0 );
        nd1 = polytopenorm( pt, VV, 'c4', 'numthread',1, 'v',0, 'bound',[0 1] );
        EXPECT_LE( nc(3), nc(1), nc(2) );
        EXPECT_LE( nd(3), nd(1), nd(2) );
        EXPECT_LE( nd1(3), nd1(1), nd1(2) );
        EXPECT_GE( min([nc(2) nd(2) nd1(2)]), max([nc(3) nd(3) nd1(3)]) ); end; end;

%% ipa
TESTCASE

T = {};
T{1} = [0 0 0 0 3 3;0 0 3 3 0 1;3 3 0 1 -4 6;0 1 -4 6 0 0;-4 6 0 0 0 0;0 0 0 0 0 0];
T{2} = [0 0 0 0 0 3;0 0 0 3 3 0;0 3 3 0 1 -4;3 0 1 -4 6 0;1 -4 6 0 0 0;6 0 0 0 0 0];
EXPECT_NTHROW( @() ipa( 9.*T, 'epssym',nan, 'v',3, 'algorithm','cb', 'delta',.8 ), 'leadingeigenvector:numerical' );

%%

ENDTESTCASE();

%#ok<*NOSEL,*NOSEMI,*ALIGN>
